﻿using AGE.Entities.DAO.AgeDepartamentos;
using AGE.Entities;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Utils.Paginacion;
using AGE.Utils;
using Microsoft.EntityFrameworkCore;

namespace AGE.Repositories.AgeDepartamentosRepository
{
    public class AgeDepartamentosRepository : IAgeDepartamentosRepository
    {
        private readonly DbContextAge _dbContext;

        public AgeDepartamentosRepository(DbContextAge dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Page<AgeDepartamentosDAO>> ConsultarTodos(int codigoLicenciatario,
                                                                    string? descripcion,
                                                                    Pageable pageable)
        {
            if (_dbContext.AgeDepartamentos == null)
            {
                throw new RegisterNotFoundException();
            }

            var query = _dbContext.AgeDepartamentos
            .Where(a => a.Estado != Globales.ESTADO_ANULADO &&
                    (a.AgeLicencCodigo == codigoLicenciatario) &&
                    (descripcion == null || descripcion == "" || a.Descripcion.ToLower().Contains(descripcion.ToLower())));

            return await Paginator<AgeDepartamentos, AgeDepartamentosDAO>.Paginar(query, pageable, FromEntityToDAO);
        }

        public async Task<AgeDepartamentosDAO> ConsultarPorId(AgeDepartamentosPKDAO ageDepartamentosPKDAO)
        {
            if (_dbContext.AgeDepartamentos == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeDepartamentos? ageDepartamento = await _dbContext.AgeDepartamentos
                            .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeLicencCodigo == ageDepartamentosPKDAO.AgeLicencCodigo &&
                            p.Codigo == ageDepartamentosPKDAO.Codigo).FirstOrDefaultAsync();

            return FromEntityToDAO(ageDepartamento);
        }

        public async Task<Page<AgeDepartamentosDAO>> ConsultarListaFiltro(int codigoLicenciatario, string filtro, Pageable pageable)
        {
            if (_dbContext.AgeDepartamentos == null)
            {
                throw new RegisterNotFoundException();
            }

            var query = _dbContext.AgeDepartamentos
                .Where(a => a.Estado != Globales.ESTADO_ANULADO &&
                            a.AgeLicencCodigo == codigoLicenciatario &&
                            a.Descripcion.ToUpper().Contains(filtro.ToUpper()) ||
                            a.Codigo.ToString().ToUpper().Contains(filtro.ToUpper()));

            return await Paginator<AgeDepartamentos, AgeDepartamentosDAO>.Paginar(query, pageable, FromEntityToDAO);
        }

        public async Task<AgeDepartamentosDAO> Insertar(AgeDepartamentos ageDepartamento)
        {
            _dbContext.AgeDepartamentos.Add(ageDepartamento);
            await _dbContext.SaveChangesAsync();

            AgeDepartamentosDAO ageAgeDepartamentosDAO = await ConsultarPorId(new AgeDepartamentosPKDAO
            {
                AgeLicencCodigo = ageDepartamento.AgeLicencCodigo,
                Codigo = ageDepartamento.Codigo
            });

            return ageAgeDepartamentosDAO;
        }

        public async Task<List<AgeDepartamentosDAO>> InsertarVarios(List<AgeDepartamentos> ageDepartamentosList)
        {
            _dbContext.AgeDepartamentos.AddRange(ageDepartamentosList);
            _dbContext.SaveChanges();

            return ConversorEntityToDAOList<AgeDepartamentos, AgeDepartamentosDAO>
                .FromEntityToDAOList(ageDepartamentosList, FromEntityToDAO);
        }

        public async Task<AgeDepartamentos> ConsultarCompletePorId(AgeDepartamentosPKDAO ageDepartamentosPKDAO)
        {
            if (_dbContext.AgeDepartamentos == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeDepartamentos? ageDepartamento = await _dbContext.AgeDepartamentos
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeLicencCodigo == ageDepartamentosPKDAO.AgeLicencCodigo &&
                            p.Codigo == ageDepartamentosPKDAO.Codigo)
                .FirstOrDefaultAsync();

            return ageDepartamento;
        }

        public async Task<AgeDepartamentosDAO> Actualizar(AgeDepartamentos AgeDepartamento)
        {
            _dbContext.Entry(AgeDepartamento).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();

            AgeDepartamentosDAO ageAgeDepartamentosDAO = await ConsultarPorId(new AgeDepartamentosPKDAO
            {
                AgeLicencCodigo = AgeDepartamento.AgeLicencCodigo,
                Codigo = AgeDepartamento.Codigo
            });

            return ageAgeDepartamentosDAO;
        }

        public async Task<List<AgeDepartamentosDAO>> ActualizarVarios(List<AgeDepartamentos> AgeDepartamentosList)
        {
            _dbContext.AgeDepartamentos.UpdateRange(AgeDepartamentosList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeDepartamentos, AgeDepartamentosDAO>
                .FromEntityToDAOList(AgeDepartamentosList, FromEntityToDAO);
        }

        private static AgeDepartamentosDAO? FromEntityToDAO(AgeDepartamentos ageDepartamentos)
        {
            if (ageDepartamentos == null)
            {
                return null;
            }

            return new AgeDepartamentosDAO
            {

                Id = new AgeDepartamentosPKDAO
                {
                    AgeLicencCodigo = ageDepartamentos.AgeLicencCodigo,
                    Codigo = ageDepartamentos.Codigo
                },
                Estado = ageDepartamentos.Estado,
                AgeDepartAgeLicencCodigo = ageDepartamentos.AgeDepartAgeLicencCodigo,
                AgeDepartCodigo = ageDepartamentos.AgeDepartCodigo,
                Descripcion = ageDepartamentos.Descripcion,
            };
        }
    }
}
