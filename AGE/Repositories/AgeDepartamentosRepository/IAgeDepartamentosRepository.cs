﻿using AGE.Entities.DAO.AgeDepartamentos;
using AGE.Entities;
using AGE.Utils.Paginacion;

namespace AGE.Repositories.AgeDepartamentosRepository
{
    public interface IAgeDepartamentosRepository
    {
        Task<Page<AgeDepartamentosDAO>> ConsultarTodos(
            int codigoLicenciatario,
            string? descripcion,
            Pageable pageable);

        Task<AgeDepartamentosDAO> ConsultarPorId(AgeDepartamentosPKDAO ageDepartamentosPKDAO);

        Task<Page<AgeDepartamentosDAO>> ConsultarListaFiltro(
            int codigoLicenciatario,
            string filtro,
            Pageable pageable);

        Task<AgeDepartamentos> ConsultarCompletePorId(
            AgeDepartamentosPKDAO ageDepartamentosPKDAO);

        Task<AgeDepartamentosDAO> Insertar(AgeDepartamentos AgeDepartamentos);

        Task<List<AgeDepartamentosDAO>> InsertarVarios(
            List<AgeDepartamentos> AgeDepartamentosList);

        Task<AgeDepartamentosDAO> Actualizar(AgeDepartamentos AgeDepartamentos);

        Task<List<AgeDepartamentosDAO>> ActualizarVarios(
            List<AgeDepartamentos> AgeDepartamentosList);
    }
}
