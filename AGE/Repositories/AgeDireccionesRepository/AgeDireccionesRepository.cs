﻿using AGE.Entities;
using AGE.Entities.DAO.AgeDirecciones;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Utils;
using AGE.Utils.Paginacion;
using Microsoft.EntityFrameworkCore;

namespace AGE.Repositories.AgeDireccionesRepository
{
    public class AgeDireccionesRepository : IAgeDireccionesRepository
    {
        private readonly DbContextAge _dbContext;

        public AgeDireccionesRepository(DbContextAge dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Page<AgeDireccionesDAO>> ConsultarTodos(
            int codigoLicenciatario,
            int? codigoPersona,
            int? codigo,
            int? ageTipDiCodigo,
            string? descripcion,
            Pageable pageable)
        {
            if (_dbContext.AgeDirecciones == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgeDirecciones
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeAgeTipLoAgePaisCodigo == codigoLicenciatario &&
                            (codigoPersona == 0 || p.AgePersonCodigo == codigoPersona) &&
                            (codigo == 0 || p.Codigo == codigo) &&
                            (ageTipDiCodigo == 0 || p.AgeTipDiCodigo == ageTipDiCodigo) &&
                            (descripcion.Equals("") || p.Descripcion.ToLower().Contains(descripcion.ToLower())));

            return await Paginator<AgeDirecciones, AgeDireccionesDAO>.Paginar(Query, pageable, FromEntityToDAO);
        }


        public async Task<AgeDireccionesDAO> ConsultarPorId(AgeDireccionesPKDAO ageDireccionesPKDAO)
        {
            if (_dbContext.AgeDirecciones == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeDirecciones? ageDireccione = await _dbContext.AgeDirecciones
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgePersonAgeLicencCodigo == ageDireccionesPKDAO.AgePersonAgeLicencCodigo &&
                            p.AgePersonCodigo == ageDireccionesPKDAO.AgePersonCodigo &&
                            p.Codigo == ageDireccionesPKDAO.codigo).FirstOrDefaultAsync();

            return FromEntityToDAO(ageDireccione);
        }


        public async Task<AgeDirecciones> ConsultarCompletePorId(AgeDireccionesPKDAO ageDireccionesPKDAO)
        {
            if (_dbContext.AgeDirecciones == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeDirecciones? ageDireccione = await _dbContext.AgeDirecciones
                 .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                             p.AgePersonAgeLicencCodigo == ageDireccionesPKDAO.AgePersonAgeLicencCodigo &&
                             p.AgePersonCodigo == ageDireccionesPKDAO.AgePersonCodigo &&
                             p.Codigo == ageDireccionesPKDAO.codigo).FirstOrDefaultAsync();

            return ageDireccione;
        }


        public async Task<Page<AgeDireccionesDAO>> ConsultarListaFiltro(
            int codigoLicenciatario,
            string filtro,
            Pageable pageable)
        {
            if (_dbContext.AgeDirecciones == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgeDirecciones
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgePersonAgeLicencCodigo == codigoLicenciatario && (
                            p.Codigo.ToString().ToLower().Contains(filtro) ||
                            p.Telefono1.ToLower().Contains(filtro) ||
                            p.Telefono2.ToLower().Contains(filtro) ||
                            p.NombreContacto.ToLower().Contains(filtro) ||
                            p.CorreoElectronicoContacto.ToLower().Contains(filtro) ||
                            p.CelularContacto.ToLower().Contains(filtro) ||
                            p.Latitud.ToLower().Contains(filtro) ||
                            p.Longitud.ToLower().Contains(filtro) ||
                            p.Descripcion.ToLower().Contains(filtro)));

            return await Paginator<AgeDirecciones, AgeDireccionesDAO>.Paginar(Query, pageable, FromEntityToDAO);
        }


        public async Task<AgeDireccionesDAO> Insertar(AgeDirecciones ageDireccione)
        {
            _dbContext.AgeDirecciones.Add(ageDireccione);
            await _dbContext.SaveChangesAsync();

            AgeDireccionesDAO ageDireccionesDAO = await ConsultarPorId(new AgeDireccionesPKDAO
            {
                AgePersonAgeLicencCodigo = ageDireccione.AgePersonAgeLicencCodigo,
                codigo = ageDireccione.Codigo,
                AgePersonCodigo = ageDireccione.AgePersonCodigo
            });

            return ageDireccionesDAO;
        }


        public async Task<List<AgeDireccionesDAO>> InsertarVarios(List<AgeDirecciones> ageDireccioneList)
        {
            await _dbContext.AgeDirecciones.AddRangeAsync(ageDireccioneList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeDirecciones, AgeDireccionesDAO>
                .FromEntityToDAOList(ageDireccioneList, FromEntityToDAO);
        }

        public async Task<AgeDireccionesDAO> Actualizar(AgeDirecciones ageDireccione)
        {
            _dbContext.Entry(ageDireccione).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();

            AgeDireccionesDAO ageDireccionesDAO = await ConsultarPorId(new AgeDireccionesPKDAO
            {
                AgePersonAgeLicencCodigo = ageDireccione.AgePersonAgeLicencCodigo,
                codigo = ageDireccione.Codigo,
                AgePersonCodigo = ageDireccione.AgePersonCodigo
            });

            return ageDireccionesDAO;
        }

        public async Task<List<AgeDireccionesDAO>> ActualizarVarios(List<AgeDirecciones> ageDireccioneList)
        {
            _dbContext.AgeDirecciones.UpdateRange(ageDireccioneList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeDirecciones, AgeDireccionesDAO>
                .FromEntityToDAOList(ageDireccioneList, FromEntityToDAO);
        }


        private static AgeDireccionesDAO FromEntityToDAO(AgeDirecciones entityObject)
        {

            if (entityObject == null)
            {
                return null;
            }

            return new AgeDireccionesDAO
            {
                Id = new AgeDireccionesPKDAO
                {
                    AgePersonCodigo = entityObject.AgePersonCodigo,
                    AgePersonAgeLicencCodigo = entityObject.AgePersonAgeLicencCodigo,
                    codigo = entityObject.Codigo
                },

                AgeAgeTipLoAgePaisCodigo = entityObject.AgeAgeTipLoAgePaisCodigo,
                AgeLocaliAgeTipLoCodigo = entityObject.AgeLocaliAgeTipLoCodigo,
                AgeLocaliCodigo = entityObject.AgeLocaliCodigo,
                AgeTipDiCodigo = entityObject.AgeTipDiCodigo,
                Descripcion = entityObject.Descripcion,
                Telefono1 = entityObject.Telefono1,
                Telefono2 = entityObject.Telefono2,
                NombreContacto = entityObject.NombreContacto,
                CorreoElectronicoContacto = entityObject.CorreoElectronicoContacto,
                CelularContacto = entityObject.CelularContacto,
                Latitud = entityObject.Latitud,
                Longitud = entityObject.Longitud,
                Estado = entityObject.Estado
            };
        }

    }
}
