﻿using AGE.Entities.DAO.AgeDirecciones;
using AGE.Entities;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Utils.Paginacion;
using AGE.Utils;
using Microsoft.EntityFrameworkCore;

namespace AGE.Repositories.AgeDireccionesRepository
{
    public interface IAgeDireccionesRepository
    {
        Task<Page<AgeDireccionesDAO>> ConsultarTodos(
            int codigoLicenciatario,
            int? codigoPersona,
            int? codigo,
            int? ageTipDiCodigo,
            string? descripcion,
            Pageable pageable);
        Task<AgeDireccionesDAO> ConsultarPorId(AgeDireccionesPKDAO ageDireccionesPKDAO);
        Task<AgeDirecciones> ConsultarCompletePorId(AgeDireccionesPKDAO ageDireccionesPKDAO);
        Task<Page<AgeDireccionesDAO>> ConsultarListaFiltro(
            int codigoLicenciatario,
            string filtro,
            Pageable pageable);
        Task<AgeDireccionesDAO> Insertar(AgeDirecciones ageDireccione);
        Task<List<AgeDireccionesDAO>> InsertarVarios(List<AgeDirecciones> ageDireccioneList);
        Task<AgeDireccionesDAO> Actualizar(AgeDirecciones ageDireccione);
        Task<List<AgeDireccionesDAO>> ActualizarVarios(List<AgeDirecciones> ageDireccioneList);
    }
}
