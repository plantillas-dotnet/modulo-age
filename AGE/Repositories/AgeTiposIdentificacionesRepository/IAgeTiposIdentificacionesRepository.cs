﻿using AGE.Entities.DAO.AgeTiposIdentificaciones;
using AGE.Entities;
using AGE.Utils.Paginacion;

namespace AGE.Repositories.AgeTiposIdentificacioneRepository
{
    public interface IAgeTiposIdentificacionesRepository
    {

        Task<Page<AgeTiposIdentificacioneDAO>> ConsultarTodos(
           int codigo,
           int codigoPais,
           string siglas,
           string descripcion,
           Pageable pageable);

        Task<Page<AgeTiposIdentificacioneDAO>> ConsultarListaFiltro(
            string filtro,
            Pageable pageable);

        Task<AgeTiposIdentificacioneDAO> ConsultarPorId( 
            int codigo);

        Task<AgeTiposIdentificaciones> ConsultarCompletePorId(
           int id);

        Task<AgeTiposIdentificacioneDAO> Insertar(AgeTiposIdentificaciones AgeTiposIdentificacione);

        Task<List<AgeTiposIdentificacioneDAO>> InsertarVarios(
            List<AgeTiposIdentificaciones> AgeTiposIdentificacioneList);

        Task<AgeTiposIdentificacioneDAO> Actualizar(AgeTiposIdentificaciones AgeTiposIdentificacione);

        Task<List<AgeTiposIdentificacioneDAO>> ActualizarVarios(
            List<AgeTiposIdentificaciones> AgeTiposIdentificacioneList);

}
}
