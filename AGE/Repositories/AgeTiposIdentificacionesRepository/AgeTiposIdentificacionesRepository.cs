﻿using AGE.Entities;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Utils.Paginacion;
using AGE.Utils;
using Microsoft.EntityFrameworkCore;
using AGE.Entities.DAO.AgeTiposIdentificaciones;

namespace AGE.Repositories.AgeTiposIdentificacioneRepository
{

    public class AgeTiposIdentificacionesRepository : IAgeTiposIdentificacionesRepository
    {
        private readonly DbContextAge _dbContext;

        public AgeTiposIdentificacionesRepository(DbContextAge dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Page<AgeTiposIdentificacioneDAO>> ConsultarTodos(
            int codigo,
            int codigoInstitucionControl,
            string siglas,
            string descripcion,
            Pageable pageable)
        {
            if (_dbContext.AgeTiposIdentificaciones == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgeTiposIdentificaciones
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            (codigoInstitucionControl == 0 || p.CodigoInstitucionControl == codigoInstitucionControl) &&
                            (codigo == 0 || p.Codigo == codigo) &&
                            (siglas.Equals("") || p.Siglas.ToLower().Equals(siglas.ToLower())) &&
                            (descripcion.Equals("") || p.Descripcion.ToLower().Contains(descripcion.ToLower())));

            return await Paginator<AgeTiposIdentificaciones, AgeTiposIdentificacioneDAO>.Paginar(Query, pageable, FromEntityToDAO);
        }

        public async Task<AgeTiposIdentificacioneDAO> ConsultarPorId(int codigo)
        {
            if (_dbContext.AgeTiposIdentificaciones == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeTiposIdentificaciones? ageTiposIdentificacione = await _dbContext.AgeTiposIdentificaciones
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                p.Codigo == codigo).FirstOrDefaultAsync();

            return FromEntityToDAO(ageTiposIdentificacione);
        }


        public async Task<AgeTiposIdentificaciones> ConsultarCompletePorId(int codigo)
        {
            if (_dbContext.AgeTiposIdentificaciones == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeTiposIdentificaciones? ageTiposIdentificacione = await _dbContext.AgeTiposIdentificaciones
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                p.Codigo == codigo).FirstOrDefaultAsync();

            return ageTiposIdentificacione;
        }


        public async Task<Page<AgeTiposIdentificacioneDAO>> ConsultarListaFiltro(
           string filtro,
           Pageable pageable)
        {
            if (_dbContext.AgeTiposIdentificaciones == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgeTiposIdentificaciones
                .Where(p => p.Estado != Globales.ESTADO_ANULADO && (
                            p.CodigoInstitucionControl.ToString().ToLower().Contains(filtro) || 
                            p.Codigo.ToString().ToLower().Contains(filtro) ||
                            p.Siglas.ToLower().Contains(filtro) ||
                            p.Descripcion.ToLower().Contains(filtro)));

            return await Paginator<AgeTiposIdentificaciones, AgeTiposIdentificacioneDAO>.Paginar(Query, pageable, FromEntityToDAO);
        }

        public async Task<AgeTiposIdentificacioneDAO> Insertar(AgeTiposIdentificaciones ageTiposIdentificacione)
        {
            _dbContext.AgeTiposIdentificaciones.Add(ageTiposIdentificacione);
            await _dbContext.SaveChangesAsync();

            AgeTiposIdentificacioneDAO ageTiposIdentificacioneDAO = await ConsultarPorId(ageTiposIdentificacione.Codigo);

            return ageTiposIdentificacioneDAO;
        }


        public async Task<List<AgeTiposIdentificacioneDAO>> InsertarVarios(List<AgeTiposIdentificaciones> ageTiposIdentificacioneList)
        {
            await _dbContext.AgeTiposIdentificaciones.AddRangeAsync(ageTiposIdentificacioneList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeTiposIdentificaciones, AgeTiposIdentificacioneDAO>
                .FromEntityToDAOList(ageTiposIdentificacioneList, FromEntityToDAO);
        }

        public async Task<AgeTiposIdentificacioneDAO> Actualizar(AgeTiposIdentificaciones ageTiposIdentificacione)
        {
            _dbContext.Entry(ageTiposIdentificacione).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();

            AgeTiposIdentificacioneDAO ageTiposIdentificacioneDAO = await ConsultarPorId(ageTiposIdentificacione.Codigo);

            return ageTiposIdentificacioneDAO;
        }


        public async Task<List<AgeTiposIdentificacioneDAO>> ActualizarVarios(List<AgeTiposIdentificaciones> ageTiposIdentificacioneList)
        {
            _dbContext.AgeTiposIdentificaciones.UpdateRange(ageTiposIdentificacioneList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeTiposIdentificaciones, AgeTiposIdentificacioneDAO>
                .FromEntityToDAOList(ageTiposIdentificacioneList, FromEntityToDAO);
        }


        private AgeTiposIdentificacioneDAO FromEntityToDAO(AgeTiposIdentificaciones entityObject)
        {

            if (entityObject == null)
            {
                return null;
            }

            return new AgeTiposIdentificacioneDAO
            {
                Codigo = entityObject.Codigo,
                CodigoInstitucionControl = entityObject.CodigoInstitucionControl,
                Descripcion = entityObject.Descripcion,
                Siglas = entityObject.Siglas,
                Estado = entityObject.Estado
            };
            }

    }
    
}
