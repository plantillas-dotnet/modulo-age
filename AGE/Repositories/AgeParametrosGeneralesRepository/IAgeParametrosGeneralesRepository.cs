﻿using AGE.Entities;
using AGE.Entities.DAO.AgeParametrosGenerales;
using AGE.Utils.Paginacion;

namespace AGE.Repositories.AgeParametrosGeneralesRepository
{
    public interface IAgeParametrosGeneralesRepository
    {
        Task<Page<AgeParametrosGeneralesDAO>> ConsultarTodos(
           int codigo,
           string descripcion,
           Pageable pageable);

        Task<Page<AgeParametrosGeneralesDAO>> ConsultarListaFiltro(
            string filtro,
            Pageable pageable);


        Task<AgeParametrosGeneralesDAO> ConsultarPorId(int codigo);


        Task<AgeParametrosGenerales> ConsultarCompletePorId(int codigo);


        Task<AgeParametrosGeneralesDAO> Insertar(AgeParametrosGenerales ageParametrosGenerales);

        Task<AgeParametrosGeneralesDAO> Actualizar(AgeParametrosGenerales ageParametrosGenerales);
        Task<List<AgeParametrosGeneralesDAO>> InsertarVarios(
            List<AgeParametrosGenerales> ageParametrosGeneralesList);
        Task<List<AgeParametrosGeneralesDAO>> ActualizarVarios(
            List<AgeParametrosGenerales> ageParametrosGeneralesList);
    }
}
