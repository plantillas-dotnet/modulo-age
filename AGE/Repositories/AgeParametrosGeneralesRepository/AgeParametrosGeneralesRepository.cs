﻿using AGE.Entities;
using AGE.Entities.DAO.AgeParametrosGenerales;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Utils;
using AGE.Utils.Paginacion;
using Microsoft.EntityFrameworkCore;

namespace AGE.Repositories.AgeParametrosGeneralesRepository
{
    public class AgeParametrosGeneralesRepository : IAgeParametrosGeneralesRepository
    {
        private readonly DbContextAge _dbContext;

        public AgeParametrosGeneralesRepository(DbContextAge dbContext)
        {
            _dbContext = dbContext;
        }


        public async Task<Page<AgeParametrosGeneralesDAO>> ConsultarTodos(
            int codigo,
            string descripcion,
            Pageable pageable)
        {
            if (_dbContext.AgeParametrosGenerales == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgeParametrosGenerales
                .Where(p => p.Estado != Globales.ESTADO_ANULADO && (codigo == 0 || p.Codigo == codigo) &&
                            (descripcion.Equals("") || p.Descripcion.ToLower().Contains(descripcion.ToLower())));

            return await Paginator<AgeParametrosGenerales, AgeParametrosGeneralesDAO>.Paginar(
                Query, pageable, FromEntityToDAO);
        }


        public async Task<Page<AgeParametrosGeneralesDAO>> ConsultarListaFiltro(
            string filtro,
            Pageable pageable)
        {
            if (_dbContext.AgeParametrosGenerales == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgeParametrosGenerales
                .Where(p => p.Estado != Globales.ESTADO_ANULADO && (
                            p.Codigo.ToString().ToLower().Contains(filtro) ||
                            p.Descripcion.ToLower().Contains(filtro)));

            return await Paginator<AgeParametrosGenerales, AgeParametrosGeneralesDAO>.Paginar(
                Query, pageable, FromEntityToDAO);
        }


        public async Task<AgeParametrosGeneralesDAO> ConsultarPorId(int codigo)
        {
            if (_dbContext.AgeParametrosGenerales == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeParametrosGenerales? ageParametrosGenerales = await _dbContext.AgeParametrosGenerales
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                p.Codigo == codigo).FirstOrDefaultAsync();

            return FromEntityToDAO(ageParametrosGenerales);
        }


        public async Task<AgeParametrosGenerales> ConsultarCompletePorId(int codigo)
        {
            if (_dbContext.AgeParametrosGenerales == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeParametrosGenerales? ageParametrosGenerales = await _dbContext.AgeParametrosGenerales
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                p.Codigo == codigo).FirstOrDefaultAsync();

            return ageParametrosGenerales;
        }


        public async Task<AgeParametrosGeneralesDAO> Insertar(AgeParametrosGenerales ageParametrosGenerales)
        {
            _dbContext.AgeParametrosGenerales.Add(ageParametrosGenerales);
            await _dbContext.SaveChangesAsync();

            AgeParametrosGeneralesDAO ageParametrosGeneralesDAO = await ConsultarPorId(ageParametrosGenerales.Codigo);

            return ageParametrosGeneralesDAO;
        }

        public async Task<AgeParametrosGeneralesDAO> Actualizar(AgeParametrosGenerales ageParametrosGenerales)
        {
            _dbContext.Entry(ageParametrosGenerales).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();

            AgeParametrosGeneralesDAO ageParametrosGeneralesDAO = await ConsultarPorId(ageParametrosGenerales.Codigo);

            return ageParametrosGeneralesDAO;
        }

        public async Task<List<AgeParametrosGeneralesDAO>> InsertarVarios(List<AgeParametrosGenerales> ageParametrosGeneralesList)
        {
            await _dbContext.AgeParametrosGenerales.AddRangeAsync(ageParametrosGeneralesList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeParametrosGenerales, AgeParametrosGeneralesDAO>
                .FromEntityToDAOList(ageParametrosGeneralesList, FromEntityToDAO);
        }

        public async Task<List<AgeParametrosGeneralesDAO>> ActualizarVarios(List<AgeParametrosGenerales> ageParametrosGeneralesList)
        {
            _dbContext.AgeParametrosGenerales.UpdateRange(ageParametrosGeneralesList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeParametrosGenerales, AgeParametrosGeneralesDAO>
                .FromEntityToDAOList(ageParametrosGeneralesList, FromEntityToDAO);
        }



        private AgeParametrosGeneralesDAO FromEntityToDAO(AgeParametrosGenerales entityObject)
        {
            if (entityObject == null)
            {
                return null; 
            }

            return new AgeParametrosGeneralesDAO
                {
                    Estado = entityObject.Estado,
                    Codigo = entityObject.Codigo,
                    Descripcion = entityObject.Descripcion
                };
        }
    }
}
