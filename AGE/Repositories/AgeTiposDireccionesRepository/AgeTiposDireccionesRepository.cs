﻿using AGE.Entities.DAO.AgeTiposDireccione;
using AGE.Entities;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Utils.Paginacion;
using AGE.Utils;
using Microsoft.EntityFrameworkCore;

namespace AGE.Repositories.AgeTiposDireccioneRepository
{
    public class AgeTiposDireccionesRepository : IAgeTiposDireccionesRepository
    {
        private readonly DbContextAge _dbContext;

        public AgeTiposDireccionesRepository(DbContextAge dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Page<AgeTiposDireccionesDAO>> ConsultarTodos(
            int? codigo,
            string? descripcion,
            Pageable pageable)
        {
            if (_dbContext.AgeTiposDirecciones == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgeTiposDirecciones
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            (codigo == 0 || p.Codigo == codigo) &&
                            (descripcion.Equals("") || p.Descripcion.ToLower().Contains(descripcion.ToLower())));

            return await Paginator<AgeTiposDirecciones, AgeTiposDireccionesDAO>.Paginar(Query, pageable, FromEntityToDAO);
        }


        public async Task<AgeTiposDireccionesDAO> ConsultarPorId(int codigo)
        {
            if (_dbContext.AgeTiposDirecciones == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeTiposDirecciones? ageTiposDireccione = await _dbContext.AgeTiposDirecciones
               .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                           p.Codigo == codigo).FirstOrDefaultAsync();

            return FromEntityToDAO(ageTiposDireccione);
        }

        public async Task<AgeTiposDirecciones> ConsultarCompletePorId(int codigo)
        {
            if (_dbContext.AgeTiposDirecciones == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeTiposDirecciones? ageTiposDireccione = await _dbContext.AgeTiposDirecciones
               .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                           p.Codigo == codigo).FirstOrDefaultAsync();

            return ageTiposDireccione;
        }

        public async Task<Page<AgeTiposDireccionesDAO>> ConsultarListaFiltro(string filtro, Pageable pageable)
        {
            if (_dbContext.AgeTiposDirecciones == null)
            {
                throw new RegisterNotFoundException();
            }

            IQueryable<AgeTiposDirecciones> query = _dbContext.AgeTiposDirecciones
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            (p.Codigo.ToString().ToLower().Contains(filtro) ||
                             p.Descripcion.ToLower().Contains(filtro)));

            return await Paginator<AgeTiposDirecciones, AgeTiposDireccionesDAO>.Paginar(query, pageable, FromEntityToDAO);
        }

        public async Task<AgeTiposDireccionesDAO> Insertar(AgeTiposDirecciones ageTiposDireccione)
        {
            _dbContext.AgeTiposDirecciones.Add(ageTiposDireccione);
            await _dbContext.SaveChangesAsync();


            return await ConsultarPorId(ageTiposDireccione.Codigo);
        }

        public async Task<List<AgeTiposDireccionesDAO>> InsertarVarios(List<AgeTiposDirecciones> ageTiposDireccioneList)
        {
            await _dbContext.AgeTiposDirecciones.AddRangeAsync(ageTiposDireccioneList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeTiposDirecciones, AgeTiposDireccionesDAO>.FromEntityToDAOList(ageTiposDireccioneList, FromEntityToDAO);
        }

        public async Task<AgeTiposDireccionesDAO> Actualizar(AgeTiposDirecciones ageTiposDireccione)
        {
            _dbContext.Entry(ageTiposDireccione).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();

            return await ConsultarPorId(ageTiposDireccione.Codigo);
        }

        public async Task<List<AgeTiposDireccionesDAO>> ActualizarVarios(List<AgeTiposDirecciones> ageTiposDireccioneList)
        {
            _dbContext.AgeTiposDirecciones.UpdateRange(ageTiposDireccioneList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeTiposDirecciones, AgeTiposDireccionesDAO>.FromEntityToDAOList(ageTiposDireccioneList, FromEntityToDAO);
        }

        private AgeTiposDireccionesDAO FromEntityToDAO(AgeTiposDirecciones entityObject)
        {
            if (entityObject == null)
            {
                return null;
            }

            return new AgeTiposDireccionesDAO
            {
                Codigo = entityObject.Codigo,
                Descripcion = entityObject.Descripcion,
                Estado = entityObject.Estado,

            };
        }
    }
}
