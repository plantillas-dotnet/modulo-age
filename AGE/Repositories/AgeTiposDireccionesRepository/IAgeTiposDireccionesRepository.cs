﻿using AGE.Entities;
using AGE.Entities.DAO.AgeTiposDireccione;
using AGE.Utils.Paginacion;

namespace AGE.Repositories.AgeTiposDireccioneRepository
{
    public interface IAgeTiposDireccionesRepository
    {
        Task<Page<AgeTiposDireccionesDAO>> ConsultarTodos(
            int? codigo, 
            string? descripcion, 
            Pageable pageable);

        Task<AgeTiposDireccionesDAO> ConsultarPorId(
            int codigo);

        Task<AgeTiposDirecciones> ConsultarCompletePorId(
            int codigo);

        Task<Page<AgeTiposDireccionesDAO>> ConsultarListaFiltro(
            string filtro, 
            Pageable pageable);

        Task<AgeTiposDireccionesDAO> Insertar(
            AgeTiposDirecciones ageTiposDireccione);

        Task<List<AgeTiposDireccionesDAO>> InsertarVarios(
            List<AgeTiposDirecciones> ageTiposDireccioneList);

        Task<AgeTiposDireccionesDAO> Actualizar(
            AgeTiposDirecciones ageTiposDireccione);

        Task<List<AgeTiposDireccionesDAO>> ActualizarVarios(
            List<AgeTiposDirecciones> ageTiposDireccioneList);
    }
}
