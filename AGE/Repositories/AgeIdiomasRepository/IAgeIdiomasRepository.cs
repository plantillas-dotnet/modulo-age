﻿using AGE.Entities;
using AGE.Entities.DAO.AgeIdioma;
using AGE.Utils;
using AGE.Utils.Paginacion;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AGE.Repositories.AgeIdiomaRepository
{
    public interface IAgeIdiomasRepository
    {
        Task<Page<AgeIdiomasDAO>> ConsultarTodos(
        int codigo,
        string descripcion,
        Pageable pageable);

        Task<AgeIdiomasDAO> ConsultarPorId(
            int codigo);
        Task<AgeIdiomas> ConsultarCompletePorId(
            int codigo);
        Task<Page<AgeIdiomasDAO>> ConsultarListaFiltro(
            string filtro, 
            Pageable pageable);
        Task<AgeIdiomasDAO> Insertar(
            AgeIdiomas ageIdioma);
        Task<List<AgeIdiomasDAO>> InsertarVarios(
            List<AgeIdiomas> ageIdiomaList);
        Task<AgeIdiomasDAO> Actualizar(
            AgeIdiomas ageIdioma);
        Task<List<AgeIdiomasDAO>> ActualizarVarios(
            List<AgeIdiomas> ageIdiomaList);
    }
}
