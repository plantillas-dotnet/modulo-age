﻿using AGE.Entities.DAO.AgeIdioma;
using AGE.Entities;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Repositories.AgeIdiomaRepository;
using AGE.Utils.Paginacion;
using AGE.Utils;
using Microsoft.EntityFrameworkCore;
using AGE.Entities.DAO.AgePaisesIdioma;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AGE.Entities.DAO.AgeTiposLocalidades;
using AGE.Entities.DAO.AgeTiposIdentificaciones;

namespace AGE.Repositories.AgeIdiomaRepository
{
    public class AgeIdiomasRepository : IAgeIdiomasRepository
    {
        private readonly DbContextAge _dbContext;

        public AgeIdiomasRepository(DbContextAge dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Page<AgeIdiomasDAO>> ConsultarTodos(
            int codigo,
            string descripcion,
            Pageable pageable)
        {
            if (_dbContext.AgeIdiomas == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgeIdiomas
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            codigo == 0 || p.Codigo == codigo &&
                            (descripcion.Equals("") || p.Descripcion.ToLower().Contains(descripcion.ToLower())));

            return await Paginator<AgeIdiomas, AgeIdiomasDAO>.Paginar(Query, pageable, FromEntityToDAO);
        }


        public async Task<AgeIdiomasDAO> ConsultarPorId(int codigo)
        {
            if (_dbContext.AgeIdiomas == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeIdiomas? ageIdioma = await _dbContext.AgeIdiomas
               .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                           p.Codigo == codigo).FirstOrDefaultAsync();

            return FromEntityToDAO(ageIdioma);
        }

        public async Task<AgeIdiomas> ConsultarCompletePorId(int codigo)
        {
            if (_dbContext.AgeIdiomas == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeIdiomas? ageIdioma = await _dbContext.AgeIdiomas
               .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                           p.Codigo == codigo).FirstOrDefaultAsync();

            return ageIdioma;
        }

        public async Task<Page<AgeIdiomasDAO>> ConsultarListaFiltro(string filtro, Pageable pageable)
        {
            if (_dbContext.AgeIdiomas == null)
            {
                throw new RegisterNotFoundException();
            }

            IQueryable<AgeIdiomas> query = _dbContext.AgeIdiomas
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            (p.Codigo.ToString().ToLower().Contains(filtro) ||
                             p.Descripcion.ToLower().Contains(filtro)));

            return await Paginator<AgeIdiomas, AgeIdiomasDAO>.Paginar(query, pageable, FromEntityToDAO);
        }

        public async Task<AgeIdiomasDAO> Insertar(AgeIdiomas ageIdioma)
        {
            _dbContext.AgeIdiomas.Add(ageIdioma);
            await _dbContext.SaveChangesAsync();


            return await ConsultarPorId(ageIdioma.Codigo);
        }

        public async Task<List<AgeIdiomasDAO>> InsertarVarios(List<AgeIdiomas> ageIdiomaList)
        {
            await _dbContext.AgeIdiomas.AddRangeAsync(ageIdiomaList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeIdiomas, AgeIdiomasDAO>.FromEntityToDAOList(ageIdiomaList, FromEntityToDAO);
        }

        public async Task<AgeIdiomasDAO> Actualizar(AgeIdiomas ageIdioma)
        {
            _dbContext.Entry(ageIdioma).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();

            return await ConsultarPorId(ageIdioma.Codigo);
        }

        public async Task<List<AgeIdiomasDAO>> ActualizarVarios(List<AgeIdiomas> ageIdiomaList)
        {
            _dbContext.AgeIdiomas.UpdateRange(ageIdiomaList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeIdiomas, AgeIdiomasDAO>.FromEntityToDAOList(ageIdiomaList, FromEntityToDAO);
        }

        private AgeIdiomasDAO FromEntityToDAO(AgeIdiomas entityObject)
        {
            if (entityObject == null)
            {
                return null;
            }

            return new AgeIdiomasDAO
            {
                Codigo = entityObject.Codigo,
                Descripcion = entityObject.Descripcion,
                Estado = entityObject.Estado
            };
        }
    }
}
