﻿using AGE.Entities;
using AGE.Entities.DAO.AgePerfiles;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Utils;
using AGE.Utils.Paginacion;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;

namespace AGE.Repositories.AgePerfilesRepository
{
    public class AgePerfilesRepository : IAgePerfilesRepository
    {
        private readonly DbContextAge _dbContext;

        public AgePerfilesRepository(DbContextAge dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Page<AgePerfilesDAO>> ConsultarTodos(
            int codigoLicenciatario, 
            int codigo, 
            string descripcion, 
            Pageable pageable)
        {
            if (_dbContext.AgePerfiles == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgePerfiles
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeLicencCodigo == codigoLicenciatario &&
                            (codigo == 0 || p.Codigo == codigo) &&
                            (descripcion.Equals("") || p.Descripcion.ToLower().Contains(descripcion.ToLower())));

            return await Paginator<AgePerfiles, AgePerfilesDAO>.Paginar(Query, pageable, FromEntityToDAO);
        }



        public async Task<Page<AgePerfilesDAO>> ConsultarListaFiltro(
            int codigoLicenciatario, 
            string filtro, 
            Pageable pageable)
        {
            if (_dbContext.AgePerfiles == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgePerfiles
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeLicencCodigo == codigoLicenciatario && (
                            p.Codigo.ToString().ToLower().Contains(filtro) ||
                            p.Descripcion.ToLower().Contains(filtro)));

            return await Paginator<AgePerfiles, AgePerfilesDAO>.Paginar(Query, pageable, FromEntityToDAO);
        }


        public async Task<AgePerfilesDAO> ConsultarPorId(AgePerfilesPKDAO agePerfilesPKDAO)
        {
            if (_dbContext.AgePerfiles == null)
            {
                throw new RegisterNotFoundException();
            }

            AgePerfiles? agePerfile = await _dbContext.AgePerfiles
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeLicencCodigo == agePerfilesPKDAO.ageLicencCodigo &&
                            p.Codigo == agePerfilesPKDAO.codigo).FirstOrDefaultAsync();

            return FromEntityToDAO(agePerfile);
        }


        public async Task<AgePerfiles> ConsultarCompletePorId(AgePerfilesPKDAO agePerfilesPKDAO)
        {
            if (_dbContext.AgePerfiles == null)
            {
                throw new RegisterNotFoundException();
            }

            AgePerfiles? agePerfile = await _dbContext.AgePerfiles
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeLicencCodigo == agePerfilesPKDAO.ageLicencCodigo &&
                            p.Codigo == agePerfilesPKDAO.codigo).FirstOrDefaultAsync();

            return agePerfile;
        }


        public async Task<AgePerfilesDAO> Insertar(
            AgePerfiles agePerfiles)
        {   
            _dbContext.AgePerfiles.Add(agePerfiles);
            await _dbContext.SaveChangesAsync();

            AgePerfilesDAO agePerfilesDAO = await ConsultarPorId(new AgePerfilesPKDAO
            {
                ageLicencCodigo = agePerfiles.AgeLicencCodigo,
                codigo = agePerfiles.Codigo
            });

            return agePerfilesDAO;
        }

        public async Task<List<AgePerfilesDAO>> InsertarVarios(
            List<AgePerfiles> agePerfilesList)
        {
            await _dbContext.AgePerfiles.AddRangeAsync(agePerfilesList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgePerfiles, AgePerfilesDAO>
                .FromEntityToDAOList(agePerfilesList, FromEntityToDAO);
        }


        public async Task<AgePerfilesDAO> Actualizar(AgePerfiles agePerfiles)
        {
            _dbContext.Entry(agePerfiles).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();

            AgePerfilesDAO agePerfilesDAO = await ConsultarPorId(new AgePerfilesPKDAO
            {
                ageLicencCodigo = agePerfiles.AgeLicencCodigo,
                codigo = agePerfiles.Codigo
            });

            return agePerfilesDAO;
        }


        public async Task<List<AgePerfilesDAO>> ActualizarVarios(List<AgePerfiles> agePerfilesList)
        {
            _dbContext.AgePerfiles.UpdateRange(agePerfilesList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgePerfiles, AgePerfilesDAO>
                .FromEntityToDAOList(agePerfilesList, FromEntityToDAO);
        }


        private static AgePerfilesDAO FromEntityToDAO(AgePerfiles entityObject)
        {

            if (entityObject == null)
            {
                return null;
            }

            return new AgePerfilesDAO
            {
                Id = new AgePerfilesPKDAO
                {
                    ageLicencCodigo = entityObject.AgeLicencCodigo,
                    codigo = entityObject.Codigo
                },
                Descripcion = entityObject.Descripcion,
                Estado = entityObject.Estado
            };
        }

    }
}
