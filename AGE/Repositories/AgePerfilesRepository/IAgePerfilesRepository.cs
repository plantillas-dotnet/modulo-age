﻿using AGE.Entities.DAO.AgePerfiles;
using AGE.Entities;
using AGE.Utils.Paginacion;

namespace AGE.Repositories.AgePerfilesRepository
{
    public interface IAgePerfilesRepository
    {
        Task<Page<AgePerfilesDAO>> ConsultarTodos(
            int codigoLicenciatario,
            int codigo,
            string descripcion,
            Pageable pageable);

        Task<Page<AgePerfilesDAO>> ConsultarListaFiltro(
            int codigoLicenciatario,
            string filtro,
            Pageable pageable);

        Task<AgePerfilesDAO> ConsultarPorId(
            AgePerfilesPKDAO AgePerfilesPKDAO);

        Task<AgePerfiles> ConsultarCompletePorId(
            AgePerfilesPKDAO AgePerfilesPKDAO);

        Task<AgePerfilesDAO> Insertar(
            AgePerfiles AgePerfile);

        Task<List<AgePerfilesDAO>> InsertarVarios(
            List<AgePerfiles> AgePerfilesList);

        Task<AgePerfilesDAO> Actualizar(AgePerfiles AgePerfile);

        Task<List<AgePerfilesDAO>> ActualizarVarios(
            List<AgePerfiles> AgePerfilesList);
    }
}
