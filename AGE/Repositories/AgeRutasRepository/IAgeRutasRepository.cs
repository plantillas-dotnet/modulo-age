﻿using AGE.Entities;
using AGE.Entities.DAO.AgeRutas;
using AGE.Utils.Paginacion;

namespace AGE.Repositories.AgeRutasRepository
{
    public interface IAgeRutasRepository
    {

        Task<Page<AgeRutasDAO>> ConsultarTodos(
            int codigoLicenciatario,
            int codigo,
            string ruta,
            string descripcion,
            Pageable pageable);

        Task<Page<AgeRutasDAO>> ConsultarListaFiltro(
            int codigoLicenciatario,
            string filtro,
            Pageable pageable);

        Task<AgeRutasDAO> ConsultarPorId(
            AgeRutasPKDAO AgeRutasPKDAO);

        Task<AgeRutas> ConsultarCompletePorId(
            AgeRutasPKDAO AgeRutasPKDAO);

        Task<AgeRutasDAO> Insertar(AgeRutas AgeRuta);

        Task<List<AgeRutasDAO>> InsertarVarios(
            List<AgeRutas> AgeRutaList);

        Task<AgeRutasDAO> Actualizar(AgeRutas AgeRuta);

        Task<List<AgeRutasDAO>> ActualizarVarios(
            List<AgeRutas> AgeRutaList);
    }
}
