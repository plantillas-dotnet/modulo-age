﻿using AGE.Entities;
using AGE.Entities.DAO.AgeRutas;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Utils;
using AGE.Utils.Paginacion;
using Microsoft.EntityFrameworkCore;

namespace AGE.Repositories.AgeRutasRepository
{
    public class AgeRutasRepository : IAgeRutasRepository
    {
        private readonly DbContextAge _dbContext;

        public AgeRutasRepository(DbContextAge dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Page<AgeRutasDAO>> ConsultarTodos(
            int codigoLicenciatario, 
            int codigo, 
            string ruta, 
            string descripcion, 
            Pageable pageable)
        {
            if (_dbContext.AgeRutas == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgeRutas
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeLicencCodigo == codigoLicenciatario &&
                            (codigo == 0 || p.Codigo == codigo) &&
                            (ruta.Equals("") || p.Ruta.ToLower().Equals(ruta.ToLower())) &&
                            (descripcion.Equals("") || p.Descripcion.ToLower().Contains(descripcion.ToLower())));

            return await Paginator<AgeRutas, AgeRutasDAO>.Paginar(Query, pageable, FromEntityToDAO);
        }


        public async Task<AgeRutasDAO> ConsultarPorId(AgeRutasPKDAO ageRutasPKDAO)
        {
            if (_dbContext.AgeRutas == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeRutas? ageRuta = await _dbContext.AgeRutas
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeLicencCodigo == ageRutasPKDAO.ageLicencCodigo &&
                            p.Codigo == ageRutasPKDAO.codigo).FirstOrDefaultAsync();

            return FromEntityToDAO(ageRuta);
        }


        public async Task<AgeRutas> ConsultarCompletePorId(AgeRutasPKDAO ageRutasPKDAO)
        {
            if (_dbContext.AgeRutas == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeRutas? ageRuta = await _dbContext.AgeRutas
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeLicencCodigo == ageRutasPKDAO.ageLicencCodigo &&
                            p.Codigo == ageRutasPKDAO.codigo).FirstOrDefaultAsync();

            return ageRuta;
        }


        public async Task<Page<AgeRutasDAO>> ConsultarListaFiltro(
            int codigoLicenciatario,
            string filtro, 
            Pageable pageable)
        {
            if (_dbContext.AgeRutas == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgeRutas
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeLicencCodigo == codigoLicenciatario && (
                            p.Codigo.ToString().ToLower().Contains(filtro) ||
                            p.Ruta.ToLower().Contains(filtro) ||
                            p.Descripcion.ToLower().Contains(filtro)));

            return await Paginator<AgeRutas, AgeRutasDAO>.Paginar(Query, pageable, FromEntityToDAO);
        }

        
        public async Task<AgeRutasDAO> Insertar(AgeRutas ageRuta)
        {
            _dbContext.AgeRutas.Add(ageRuta);
            await _dbContext.SaveChangesAsync();

            AgeRutasDAO ageRutasDAO = await ConsultarPorId(new AgeRutasPKDAO
            {
                ageLicencCodigo = ageRuta.AgeLicencCodigo,
                codigo = ageRuta.Codigo
            });

            return ageRutasDAO;
        }


        public async Task<List<AgeRutasDAO>> InsertarVarios(List<AgeRutas> ageRutaList)
        {
            await _dbContext.AgeRutas.AddRangeAsync(ageRutaList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeRutas, AgeRutasDAO>
                .FromEntityToDAOList(ageRutaList, FromEntityToDAO);
        }

        public async Task<AgeRutasDAO> Actualizar(AgeRutas ageRuta)
        {
            _dbContext.Entry(ageRuta).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();

            AgeRutasDAO ageRutasDAO = await ConsultarPorId(new AgeRutasPKDAO
            {
                ageLicencCodigo = ageRuta.AgeLicencCodigo,
                codigo = ageRuta.Codigo
            });

            return ageRutasDAO;
        }

        public async Task<List<AgeRutasDAO>> ActualizarVarios(List<AgeRutas> ageRutaList)
        {
            _dbContext.AgeRutas.UpdateRange(ageRutaList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeRutas, AgeRutasDAO>
                .FromEntityToDAOList(ageRutaList, FromEntityToDAO);
        }


        private static AgeRutasDAO FromEntityToDAO(AgeRutas entityObject)
        {
      
            if (entityObject == null)
            {
                return null;
            }

            return new AgeRutasDAO
            {
                Id = new AgeRutasPKDAO
                {
                    ageLicencCodigo = entityObject.AgeLicencCodigo,
                    codigo = entityObject.Codigo
                },
                Descripcion = entityObject.Descripcion,
                Ruta = entityObject.Ruta,
                Estado = entityObject.Estado
            };
        }

    }
}
