﻿using AGE.Entities;
using AGE.Entities.DAO.AgeArchivosMultimedia;
using AGE.Entities.DAO.AgeLicenciatarios;
using AGE.Entities.DAO.AgePaises;
using AGE.Entities.DAO.AgePerfiles;
using AGE.Entities.DAO.AgeUsuarios;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Repositories.AgeArchivosMultimediaRepository;
using AGE.Utils;
using AGE.Utils.Paginacion;
using Microsoft.EntityFrameworkCore;

namespace AGE.Repositories.AgeLicenciatariosRepository
{
    public class AgeLicenciatariosRepository : IAgeLicenciatariosRepository
    {
        private readonly DbContextAge _dbContext;
        private readonly IAgeArchivosMultimediaRepository _ageArchivosMultimediaRepository;
        
        public AgeLicenciatariosRepository(DbContextAge dbContext, 
            IAgeArchivosMultimediaRepository ageArchivosMultimediaRepository) {

            _dbContext = dbContext;
            _ageArchivosMultimediaRepository = ageArchivosMultimediaRepository;
        }

        public async Task<Page<AgeLicenciatarioDAO>> ConsultarTodos(
            int codigo, string descripcion, Pageable pageable)
        {
            if (_dbContext.AgeLicenciatarios == null)
            {
                throw new RegisterNotFoundException();
            }

            var query = _dbContext.AgeLicenciatarios
                .Where(p => p.Estado != Globales.ESTADO_ANULADO);

            return await Paginator<AgeLicenciatario, AgeLicenciatarioDAO>.Paginar(
                query, pageable, FromEntityToDAO);
        }

        public async Task<Page<AgeLicenciatarioDAO>> ConsultarListaFiltro(string filtro, Pageable pageable)
        {
            if (_dbContext.AgeLicenciatarios == null)
            {
                throw new RegisterNotFoundException();
            }


            var query = _dbContext.AgeLicenciatarios
                .Where(p => p.Estado != Globales.ESTADO_ANULADO && 
                    (
                    p.Codigo.ToString().ToLower().Contains(filtro) 
                    || p.Descripcion.ToLower().Contains(filtro)
                    ) 
                );

            return await Paginator<AgeLicenciatario, AgeLicenciatarioDAO>.Paginar(
                query, pageable, FromEntityToDAO);
        }

        public async Task<AgeLicenciatarioDAO> ConsultarPorId(int id)
        {
            if (_dbContext.AgeLicenciatarios == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeLicenciatario? ageLicenciatario = await _dbContext.AgeLicenciatarios
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.Codigo == id).FirstOrDefaultAsync();

            return FromEntityToDAO(ageLicenciatario);
        }

        public async Task<AgeLicenciatario> ConsultarCompletePorId(int codigo)
        {
            if (_dbContext.AgeLicenciatarios == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeLicenciatario? ageLicenciatario = await _dbContext.AgeLicenciatarios
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.Codigo == codigo).FirstOrDefaultAsync();

            return ageLicenciatario;
        }


        public async Task<AgeLicenciatarioDAO> Insertar(AgeLicenciatario ageLicenciatario)
        {
            _dbContext.AgeLicenciatarios.Add(ageLicenciatario);
            await _dbContext.SaveChangesAsync();

            AgeLicenciatarioDAO ageLicenciatariosDAO = await ConsultarPorId(ageLicenciatario.Codigo);

            return ageLicenciatariosDAO;
        }

        public async Task<List<AgeLicenciatarioDAO>> InsertarVarios(List<AgeLicenciatario> ageLicenciatariosList)
        {
            await _dbContext.AgeLicenciatarios.AddRangeAsync(ageLicenciatariosList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeLicenciatario, AgeLicenciatarioDAO>
                .FromEntityToDAOList(ageLicenciatariosList, FromEntityToDAO);

        }

        public async Task<AgeLicenciatarioDAO> Actualizar(AgeLicenciatario ageLicenciatario)
        {
            _dbContext.Entry(ageLicenciatario).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();

            AgeLicenciatarioDAO ageLicenciatarioDAO = await ConsultarPorId(ageLicenciatario.Codigo);

            return ageLicenciatarioDAO;
        }

        public async Task<List<AgeLicenciatarioDAO>> ActualizarVarios(List<AgeLicenciatario> ageLicenciatariosList)
        {
            _dbContext.AgeLicenciatarios.UpdateRange(ageLicenciatariosList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeLicenciatario, AgeLicenciatarioDAO>
                .FromEntityToDAOList(ageLicenciatariosList, FromEntityToDAO);
        }

        private AgeLicenciatarioDAO  FromEntityToDAO(AgeLicenciatario entityObject)
        {
            if (entityObject == null)
                return null;

            AgeLicenciatarioDAO daoObject = new AgeLicenciatarioDAO();

            daoObject.Codigo = entityObject.Codigo;
            daoObject.AgeClaCoCodigo = entityObject.AgeClaCoCodigo;
            daoObject.AgeTipIdCodigo = entityObject.AgeTipIdCodigo;
            daoObject.NombreCorto = entityObject.NombreCorto;
            daoObject.RepresentanteLegal = entityObject.RepresentanteLegal;
            daoObject.DireccionPrincipal = entityObject.DireccionPrincipal;
            daoObject.Telefono1 = entityObject.Telefono1;
            daoObject.EMail2 = entityObject.EMail2;
            daoObject.EsCorporacion = entityObject.EsCorporacion;
            daoObject.Observacion = entityObject.Observacion;
            daoObject.PaginaWeb = entityObject.PaginaWeb;
            daoObject.ContribuyenteEspecialResoluc = entityObject.ContribuyenteEspecialResoluc;
            daoObject.RutaLicenciatario = entityObject.RutaLicenciatario;
            daoObject.AgeLicencCodigo = entityObject.AgeLicencCodigo;
            daoObject.RazonSocial = entityObject.RazonSocial;
            daoObject.NombreComercial = entityObject.NombreComercial;
            daoObject.NumeroIdentificacion = entityObject.NumeroIdentificacion;
            daoObject.Telefono2 = entityObject.Telefono2;
            daoObject.EMail1 = entityObject.EMail1;
            daoObject.ObligadoLlevarContabilidad = entityObject.ObligadoLlevarContabilidad;
            daoObject.Descripcion = entityObject.Descripcion;
            daoObject.Estado = entityObject.Estado;

            try
            {
                List<AgeArchivosMultimedia> ageArchivosMultimediaList =
                    _ageArchivosMultimediaRepository.ConsultarTodosPorLicenciatario(entityObject.Codigo).Result;

                List<AgeArchivosMultimediaDAO> ageArchivosMultimediaDAOList = ageArchivosMultimediaList.Select(a => new AgeArchivosMultimediaDAO
                {
                    Id = new AgeArchivosMultimediaPKDAO
                    {
                        ageLicencCodigo = a.AgeLicencCodigo,
                        codigo = a.Codigo
                    },
                    Estado = a.Estado,
                    Descripcion = a.Descripcion,
                    Ruta = a.Ruta,
                    Tipo = a.Tipo,
                    Promocion = a.Promocion
                }).ToList();

                daoObject.AgeArchivosMultimediaList = ageArchivosMultimediaDAOList;
                
            }
            catch
            {
                daoObject.AgeArchivosMultimediaList = new List<AgeArchivosMultimediaDAO>();
            }

            return daoObject;

        }
    }
}
