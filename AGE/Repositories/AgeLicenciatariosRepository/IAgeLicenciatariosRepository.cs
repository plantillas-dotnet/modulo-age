﻿using AGE.Entities.DAO.AgeLicenciatarios;
using AGE.Entities;
using AGE.Utils.Paginacion;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Utils;
using Microsoft.EntityFrameworkCore;

namespace AGE.Repositories.AgeLicenciatariosRepository
{
    public interface IAgeLicenciatariosRepository
    {
        Task<Page<AgeLicenciatarioDAO>> ConsultarTodos(
            int codigo, string descripcion, Pageable pageable);

        Task<Page<AgeLicenciatarioDAO>> ConsultarListaFiltro(string filtro, Pageable pageable);

        Task<AgeLicenciatarioDAO> ConsultarPorId(int codigo);

        Task<AgeLicenciatario> ConsultarCompletePorId(int codigo);

        Task<AgeLicenciatarioDAO> Insertar(AgeLicenciatario ageLicenciatario);

        Task<List<AgeLicenciatarioDAO>> InsertarVarios(List<AgeLicenciatario> ageLicenciatariosList);

        Task<AgeLicenciatarioDAO> Actualizar(AgeLicenciatario ageLicenciatario);

        Task<List<AgeLicenciatarioDAO>> ActualizarVarios(List<AgeLicenciatario> ageLicenciatariosList);



    }
}
