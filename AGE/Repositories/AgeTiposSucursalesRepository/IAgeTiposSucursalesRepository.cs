﻿using AGE.Entities;
using AGE.Utils.Paginacion;
using AGE.Entities.DAO.AgeTiposSucursales;

namespace AGE.Repositories.AgeTiposSucursaleRepository
{
    public interface IAgeTiposSucursalesRepository
    {
        Task<Page<AgeTiposSucursaleDAO>> ConsultarTodos(
        int codigoLicenciatario,
        int codigo,
        string descripcion,
        Pageable pageable);

        Task<Page<AgeTiposSucursaleDAO>> ConsultarListaFiltro(
            int codigoLicenciatario,
            string filtro,
            Pageable pageable);

        Task<AgeTiposSucursaleDAO> ConsultarPorId(
            AgeTiposSucursalePKDAO AgeTiposSucursalePKDAO);

        Task<AgeTiposSucursales> ConsultarCompletePorId(
           AgeTiposSucursalePKDAO AgeTiposSucursalePKDAO);

        Task<AgeTiposSucursaleDAO> Insertar(AgeTiposSucursales AgeTiposSucursale);

        Task<List<AgeTiposSucursaleDAO>> InsertarVarios(
            List<AgeTiposSucursales> AgeTiposSucursaleList);

        Task<AgeTiposSucursaleDAO> Actualizar(AgeTiposSucursales AgeTiposSucursale);

        Task<List<AgeTiposSucursaleDAO>> ActualizarVarios(
            List<AgeTiposSucursales> AgeTiposSucursaleList);
    }
}
