﻿using AGE.Entities;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Utils.Paginacion;
using AGE.Utils;
using Microsoft.EntityFrameworkCore;
using AGE.Entities.DAO.AgeTiposSucursales;

namespace AGE.Repositories.AgeTiposSucursaleRepository
{
    public class AgeTiposSucursalesRepository : IAgeTiposSucursalesRepository
    {
        private readonly DbContextAge _dbContext;

        public AgeTiposSucursalesRepository(DbContextAge dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Page<AgeTiposSucursaleDAO>> ConsultarTodos(
            int codigoLicenciatario,
            int codigo,
            string descripcion,
            Pageable pageable)
        {
            if (_dbContext.AgeTiposSucursales == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgeTiposSucursales
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeLicencCodigo == codigoLicenciatario &&
                            (codigo == 0 || p.Codigo == codigo) &&
                            (descripcion.Equals("") || p.Descripcion.ToLower().Contains(descripcion.ToLower())));

            return await Paginator<AgeTiposSucursales, AgeTiposSucursaleDAO>.Paginar(Query, pageable, FromEntityToDAO);
        }


        public async Task<AgeTiposSucursaleDAO> ConsultarPorId(AgeTiposSucursalePKDAO ageTiposSucursalePKDAO)
        {
            if (_dbContext.AgeTiposSucursales == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeTiposSucursales? ageTiposSucursale = await _dbContext.AgeTiposSucursales
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeLicencCodigo == ageTiposSucursalePKDAO.ageLicencCodigo &&
                            p.Codigo == ageTiposSucursalePKDAO.codigo).FirstOrDefaultAsync();

            return FromEntityToDAO(ageTiposSucursale);
        }


        public async Task<AgeTiposSucursales> ConsultarCompletePorId(AgeTiposSucursalePKDAO ageTiposSucursalePKDAO)
        {
            if (_dbContext.AgeTiposSucursales == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeTiposSucursales? ageTiposSucursale = await _dbContext.AgeTiposSucursales
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeLicencCodigo == ageTiposSucursalePKDAO.ageLicencCodigo &&
                            p.Codigo == ageTiposSucursalePKDAO.codigo).FirstOrDefaultAsync();

            return ageTiposSucursale;
        }


        public async Task<Page<AgeTiposSucursaleDAO>> ConsultarListaFiltro(
            int codigoLicenciatario,
            string filtro,
            Pageable pageable)
        {
            if (_dbContext.AgeTiposSucursales == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgeTiposSucursales
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeLicencCodigo == codigoLicenciatario && (
                            p.Codigo.ToString().ToLower().Contains(filtro) ||
                            p.Descripcion.ToLower().Contains(filtro)));

            return await Paginator<AgeTiposSucursales, AgeTiposSucursaleDAO>.Paginar(Query, pageable, FromEntityToDAO);
        }


        public async Task<AgeTiposSucursaleDAO> Insertar(AgeTiposSucursales ageTiposSucursale)
        {
            _dbContext.AgeTiposSucursales.Add(ageTiposSucursale);
            await _dbContext.SaveChangesAsync();

            AgeTiposSucursaleDAO ageTiposSucursaleDAO = await ConsultarPorId(new AgeTiposSucursalePKDAO
            {
                ageLicencCodigo = ageTiposSucursale.AgeLicencCodigo,
                codigo = ageTiposSucursale.Codigo
            });

            return ageTiposSucursaleDAO;
        }


        public async Task<List<AgeTiposSucursaleDAO>> InsertarVarios(List<AgeTiposSucursales> ageTiposSucursaleList)
        {
            await _dbContext.AgeTiposSucursales.AddRangeAsync(ageTiposSucursaleList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeTiposSucursales, AgeTiposSucursaleDAO>
                .FromEntityToDAOList(ageTiposSucursaleList, FromEntityToDAO);
        }

        public async Task<AgeTiposSucursaleDAO> Actualizar(AgeTiposSucursales ageTiposSucursale)
        {
            _dbContext.Entry(ageTiposSucursale).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();

            AgeTiposSucursaleDAO ageTiposSucursaleDAO = await ConsultarPorId(new AgeTiposSucursalePKDAO
            {
                ageLicencCodigo = ageTiposSucursale.AgeLicencCodigo,
                codigo = ageTiposSucursale.Codigo
            });

            return ageTiposSucursaleDAO;
        }

        public async Task<List<AgeTiposSucursaleDAO>> ActualizarVarios(List<AgeTiposSucursales> ageTiposSucursaleList)
        {
            _dbContext.AgeTiposSucursales.UpdateRange(ageTiposSucursaleList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeTiposSucursales, AgeTiposSucursaleDAO>
                .FromEntityToDAOList(ageTiposSucursaleList, FromEntityToDAO);
        }


        private static AgeTiposSucursaleDAO FromEntityToDAO(AgeTiposSucursales entityObject)
        {

            if (entityObject == null)
            {
                return null;
            }

            return new AgeTiposSucursaleDAO
            {
                Id = new AgeTiposSucursalePKDAO
                {
                    ageLicencCodigo = entityObject.AgeLicencCodigo,
                    codigo = entityObject.Codigo
                },
                Descripcion = entityObject.Descripcion,
                Estado = entityObject.Estado
            };
        }

    }

}
