﻿
using AGE.Entities.DAO.AgeLocalidades;
using AGE.Entities;
using AGE.Utils.Paginacion;

namespace AGE.Repositories.AgeLocalidadesRepository
{
    public interface IAgeLocalidadesRepository
    {
        Task<Page<AgeLocalidadesDAO>> ConsultarTodos(
            string descripcion,
            int ageTipLoCodigo,
            int ageTipLoAgePaisCodigo,
            int ageLocaliCodigo,
            int ageAgeTipLoAgePaisCodigo,
            int ageLocaliAgeTipLoCodigo,
            Pageable pageable);

        Task<AgeLocalidadesDAO> ConsultarPorId(AgeLocalidadesPKDAO ageLocalidadesPKDAO);

        Task<Page<AgeLocalidadesDAO>> ConsultarListaFiltro(
            string filtro,
            Pageable pageable);

        Task<AgeLocalidades> ConsultarCompletePorId(
            AgeLocalidadesPKDAO ageLocalidadesPKDAO);

        Task<AgeLocalidadesDAO> Insertar(AgeLocalidades AgeLocalidades);

        Task<List<AgeLocalidadesDAO>> InsertarVarios(
            List<AgeLocalidades> AgeLocalidadesList);

        Task<AgeLocalidadesDAO> Actualizar(AgeLocalidades AgeLocalidades);

        Task<List<AgeLocalidadesDAO>> ActualizarVarios(
            List<AgeLocalidades> AgeLocalidadesList);
    }
}
