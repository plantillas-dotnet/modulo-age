﻿using AGE.Entities.DAO.AgeLocalidades;
using AGE.Entities;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Utils.Paginacion;
using AGE.Utils;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System.Reflection.PortableExecutable;
using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;
using AGE.Middleware.Exceptions.BadRequest;

namespace AGE.Repositories.AgeLocalidadesRepository
{
    public class AgeLocalidadesRepository : IAgeLocalidadesRepository
    {
        private readonly DbContextAge _dbContext;

        public AgeLocalidadesRepository(DbContextAge dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Page<AgeLocalidadesDAO>> ConsultarTodos(string descripcion,
                                                                int ageTipLoCodigo,
                                                                int ageTipLoAgePaisCodigo,
                                                                int ageLocaliCodigo,
                                                                int ageAgeTipLoAgePaisCodigo,
                                                                int ageLocaliAgeTipLoCodigo,
                                                                Pageable pageable)
        {
            if (_dbContext.AgeLocalidades == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgeLocalidades
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                        (descripcion == null || descripcion == "" || p.Descripcion == descripcion) &&
                        (ageTipLoCodigo == 0 || p.AgeTipLoCodigo == ageTipLoCodigo) &&
                        (ageTipLoAgePaisCodigo == 0 || p.AgeTipLoAgePaisCodigo == ageTipLoAgePaisCodigo) &&
                        (ageLocaliCodigo == 0 || p.AgeLocaliCodigo == ageLocaliCodigo) &&
                        (ageAgeTipLoAgePaisCodigo == 0 || p.AgeAgeTipLoAgePaisCodigo == ageAgeTipLoAgePaisCodigo) &&
                        (ageLocaliAgeTipLoCodigo == 0 || p.AgeLocaliAgeTipLoCodigo == ageLocaliAgeTipLoCodigo));

            return await Paginator<AgeLocalidades, AgeLocalidadesDAO>.Paginar(Query, pageable, FromEntityToDAO);
        }

        public async Task<Page<AgeLocalidadesDAO>> ConsultarListaFiltro(string filtro,
                                                                                 Pageable pageable)
        {
            if (_dbContext.AgeLocalidades == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgeLocalidades
                .Where(p => p.Estado != Globales.ESTADO_ANULADO && (
                            p.AgeTipLoCodigo.ToString().ToLower().Contains(filtro) ||
                            p.Codigo.ToString().ToLower().Contains(filtro) ||
                            p.AgeTipLoAgePaisCodigo.ToString().ToLower().Contains(filtro) ||
                            p.AgeLocaliCodigo.ToString().ToLower().Contains(filtro) ||
                            p.Descripcion.ToLower().Contains(filtro)));

            return await Paginator<AgeLocalidades, AgeLocalidadesDAO>.Paginar(Query, pageable, FromEntityToDAO);
        }


        public async Task<AgeLocalidadesDAO> ConsultarPorId(AgeLocalidadesPKDAO ageLocalidadesPKDAO)
        {
            if (_dbContext.AgeLocalidades == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeLocalidades? ageLocalidade = await _dbContext.AgeLocalidades
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeTipLoAgePaisCodigo == ageLocalidadesPKDAO.AgeTipLoAgePaisCodigo &&
                            p.AgeTipLoCodigo == ageLocalidadesPKDAO.AgeTipLoCodigo &&
                            p.Codigo == ageLocalidadesPKDAO.Codigo).FirstOrDefaultAsync();

            return FromEntityToDAO(ageLocalidade);
        }

        public async Task<AgeLocalidades> ConsultarCompletePorId(AgeLocalidadesPKDAO ageLocalidadesPKDAO)
        {
            if (_dbContext.AgeLocalidades == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeLocalidades? ageLocalidade = await _dbContext.AgeLocalidades
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeTipLoAgePaisCodigo == ageLocalidadesPKDAO.AgeTipLoAgePaisCodigo &&
                            p.AgeTipLoCodigo == ageLocalidadesPKDAO.AgeTipLoCodigo &&
                            p.Codigo == ageLocalidadesPKDAO.Codigo).FirstOrDefaultAsync();

            return ageLocalidade;
        }


        public async Task<AgeLocalidadesDAO> Insertar(
            AgeLocalidades ageLocalidades)
        {
            _dbContext.AgeLocalidades.Add(ageLocalidades);
            await _dbContext.SaveChangesAsync();

            AgeLocalidadesDAO ageLocalidadesDAO = await ConsultarPorId(new AgeLocalidadesPKDAO
            {
                AgeTipLoAgePaisCodigo = ageLocalidades.AgeTipLoAgePaisCodigo,
                AgeTipLoCodigo = ageLocalidades.AgeTipLoCodigo,
                Codigo = ageLocalidades.Codigo
            });

            return ageLocalidadesDAO;
        }

        public async Task<List<AgeLocalidadesDAO>> InsertarVarios(
            List<AgeLocalidades> ageLocalidadesList)
        {
            await _dbContext.AgeLocalidades.AddRangeAsync(ageLocalidadesList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeLocalidades, AgeLocalidadesDAO>
                .FromEntityToDAOList(ageLocalidadesList, FromEntityToDAO);
        }


        public async Task<AgeLocalidadesDAO> Actualizar(AgeLocalidades ageLocalidades)
        {
            _dbContext.Entry(ageLocalidades).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();

            AgeLocalidadesDAO ageLocalidadesDAO = await ConsultarPorId(new AgeLocalidadesPKDAO
            {
                AgeTipLoAgePaisCodigo = ageLocalidades.AgeTipLoAgePaisCodigo,
                AgeTipLoCodigo = ageLocalidades.AgeTipLoCodigo,
                Codigo = ageLocalidades.Codigo
            });

            return ageLocalidadesDAO;
        }


        public async Task<List<AgeLocalidadesDAO>> ActualizarVarios(List<AgeLocalidades> ageLocalidadesList)
        {
            _dbContext.AgeLocalidades.UpdateRange(ageLocalidadesList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeLocalidades, AgeLocalidadesDAO>
                .FromEntityToDAOList(ageLocalidadesList, FromEntityToDAO);
        }


        private static AgeLocalidadesDAO FromEntityToDAO(AgeLocalidades entityObject)
        {

            if (entityObject == null)
            {
                return null;
            }

            return new AgeLocalidadesDAO
            {
                Id = new AgeLocalidadesPKDAO
                {
                    AgeTipLoAgePaisCodigo = entityObject.AgeTipLoAgePaisCodigo,
                    AgeTipLoCodigo = entityObject.AgeTipLoCodigo,
                    Codigo = entityObject.Codigo
                },
                Estado = entityObject.Estado,
                AgeAgeTipLoAgePaisCodigo = entityObject.AgeAgeTipLoAgePaisCodigo,
                AgeIdiomaCodigo = entityObject.AgeIdiomaCodigo,
                AgeLocaliAgeTipLoCodigo = entityObject.AgeLocaliAgeTipLoCodigo,
                AgeLocaliCodigo = entityObject.AgeLocaliCodigo,
                AgeMonedaCodigo = entityObject.AgeMonedaCodigo,
                Descripcion = entityObject.Descripcion,
                LatitudCentro = entityObject.LatitudCentro,
                LongitudCentro = entityObject.LongitudCentro,
                Poligono = ConvertByteArrayToList(entityObject.Poligono)
            };
            
        }

        private static List<List<List<decimal>>> ConvertByteArrayToList(byte[] poligonoBytes)
        {
            if (poligonoBytes == null) return null;
            try
            {
                string jsonString = System.Text.Encoding.UTF8.GetString(poligonoBytes);
                return JsonConvert.DeserializeObject<List<List<List<decimal>>>>(jsonString);
            }
            catch (Exception)
            {
                throw new JsonDeserializationException("Campo polígono no se puede deserializar.");
            }
        }
    }
}
