﻿
using AGE.Entities;
using AGE.Entities.DAO.AgeFeriadosPaise;
using AGE.Utils.Paginacion;

namespace AGE.Repositories.AgeFeriadosPaiseRepository
{
    public interface IAgeFeriadosPaisesRepository
    {

        Task<Page<AgeFeriadosPaisesDAO>> ConsultarTodos(
            Pageable pageable);

        Task<Page<AgeFeriadosPaisesDAO>> ConsultarListaFiltro(
            string filtro,
            Pageable pageable);

        Task<AgeFeriadosPaisesDAO> ConsultarPorId(
            AgeFeriadosPaisesPKDAO ageFeriadosPaisePKDAO);

        Task<AgeFeriadosPaises> ConsultarCompletePorId(
            AgeFeriadosPaisesPKDAO ageFeriadosPaisePKDAO);

        Task<AgeFeriadosPaisesDAO> Insertar(
            AgeFeriadosPaises ageFeriadosPaise);

        Task<AgeFeriadosPaisesDAO> Actualizar(
            AgeFeriadosPaises ageFeriadosPaise);

        Task<List<AgeFeriadosPaisesDAO>> InsertarVarios(
            List<AgeFeriadosPaises> ageFeriadosPaiseList);

        Task<List<AgeFeriadosPaisesDAO>> ActualizarVarios(
            List<AgeFeriadosPaises> ageFeriadosPaiseList);

    }
}
