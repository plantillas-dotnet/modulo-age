﻿using AGE.Entities;
using AGE.Entities.DAO.AgeFeriadosPaise;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Utils;
using AGE.Utils.Paginacion;
using Microsoft.EntityFrameworkCore;

namespace AGE.Repositories.AgeFeriadosPaiseRepository
{
    public class AgeFeriadosPaisesRepository : IAgeFeriadosPaisesRepository
    {

        private readonly DbContextAge _dbContext;

        public AgeFeriadosPaisesRepository(DbContextAge dbContext)
        {
            _dbContext = dbContext;
        }



        public async Task<Page<AgeFeriadosPaisesDAO>> ConsultarTodos(
            Pageable pageable)
        {
            if (_dbContext.AgeFeriadosPaises == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgeFeriadosPaises
                .Where(p => p.Estado != Globales.ESTADO_ANULADO);

            return await Paginator<AgeFeriadosPaises, AgeFeriadosPaisesDAO>.Paginar(Query, pageable, FromEntityToDAO);
        }


        public async Task<AgeFeriadosPaisesDAO> ConsultarPorId(
            AgeFeriadosPaisesPKDAO ageFeriadosPaisePKDAO)
        {
            if (_dbContext.AgeFeriadosPaises == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeFeriadosPaises? ageFeriadosPaise = await _dbContext.AgeFeriadosPaises
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgePaisCodigo == ageFeriadosPaisePKDAO.AgePaisCodigo &&
                            p.AgeDiaFeCodigo == ageFeriadosPaisePKDAO.AgeDiaFeCodigo).FirstOrDefaultAsync();

            return FromEntityToDAO(ageFeriadosPaise);
        }


        public async Task<AgeFeriadosPaises> ConsultarCompletePorId(
            AgeFeriadosPaisesPKDAO ageFeriadosPaisePKDAO)
        {
            if (_dbContext.AgeFeriadosPaises == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeFeriadosPaises? ageFeriadosPaise = await _dbContext.AgeFeriadosPaises
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgePaisCodigo == ageFeriadosPaisePKDAO.AgePaisCodigo &&
                            p.AgeDiaFeCodigo == ageFeriadosPaisePKDAO.AgeDiaFeCodigo).FirstOrDefaultAsync();

            return ageFeriadosPaise;
        }


        public async Task<Page<AgeFeriadosPaisesDAO>> ConsultarListaFiltro(
            string filtro, 
            Pageable pageable)
        {
            if (_dbContext.AgeFeriadosPaises == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgeFeriadosPaises
                .Where(p => p.Estado != Globales.ESTADO_ANULADO && (
                            p.AgeDiaFeCodigo.ToString().ToLower().Contains(filtro) ||
                            p.AgePaisCodigo.ToString().ToLower().Contains(filtro)));

            return await Paginator<AgeFeriadosPaises, AgeFeriadosPaisesDAO>.Paginar(Query, pageable, FromEntityToDAO);
        }




        public async Task<AgeFeriadosPaisesDAO> Insertar(
            AgeFeriadosPaises ageFeriadosPaise)
        {
            _dbContext.AgeFeriadosPaises.Add(ageFeriadosPaise);
            await _dbContext.SaveChangesAsync();

            AgeFeriadosPaisesDAO ageFeriadosPaiseDAO = await ConsultarPorId(new AgeFeriadosPaisesPKDAO
            {
                AgeDiaFeCodigo = ageFeriadosPaise.AgeDiaFeCodigo,
                AgePaisCodigo = ageFeriadosPaise.AgePaisCodigo
            });

            return ageFeriadosPaiseDAO;
        }

        public async Task<List<AgeFeriadosPaisesDAO>> InsertarVarios(
            List<AgeFeriadosPaises> ageFeriadosPaiseList)
        {
            await _dbContext.AgeFeriadosPaises.AddRangeAsync(ageFeriadosPaiseList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeFeriadosPaises, AgeFeriadosPaisesDAO>
                .FromEntityToDAOList(ageFeriadosPaiseList, FromEntityToDAO);
        }


        public async Task<AgeFeriadosPaisesDAO> Actualizar(
            AgeFeriadosPaises ageFeriadosPaise)
        {
            _dbContext.Entry(ageFeriadosPaise).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();

            AgeFeriadosPaisesDAO ageFeriadosPaiseDAO = await ConsultarPorId(new AgeFeriadosPaisesPKDAO
            {
                AgeDiaFeCodigo = ageFeriadosPaise.AgeDiaFeCodigo,
                AgePaisCodigo = ageFeriadosPaise.AgePaisCodigo
            });

            return ageFeriadosPaiseDAO;
        }


        public async Task<List<AgeFeriadosPaisesDAO>> ActualizarVarios(
            List<AgeFeriadosPaises> ageFeriadosPaiseList)
        {
            _dbContext.AgeFeriadosPaises.UpdateRange(ageFeriadosPaiseList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeFeriadosPaises, AgeFeriadosPaisesDAO>
                .FromEntityToDAOList(ageFeriadosPaiseList, FromEntityToDAO);
        }




        private static AgeFeriadosPaisesDAO FromEntityToDAO(AgeFeriadosPaises entityObject)
        {
            if (entityObject == null)
                return null;

            return new AgeFeriadosPaisesDAO
            {
                Id = new AgeFeriadosPaisesPKDAO
                {
                    AgeDiaFeCodigo = entityObject.AgeDiaFeCodigo,
                    AgePaisCodigo = entityObject.AgePaisCodigo
                },
                FechaEjecucionDesde = entityObject.FechaEjecucionDesde,
                FechaEjecucionHasta = entityObject.FechaEjecucionHasta,
                HoraEjecucionDesde = entityObject.HoraEjecucionDesde,
                HoraEjecucionHasta = entityObject.HoraEjecucionHasta,
                Estado = entityObject.Estado
            };
        }
    }
}
