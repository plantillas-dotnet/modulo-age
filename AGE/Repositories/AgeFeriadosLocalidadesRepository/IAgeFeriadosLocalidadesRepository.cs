﻿using AGE.Entities.DAO.AgeFeriadosLocalidade;
using AGE.Entities;
using AGE.Utils.Paginacion;

namespace AGE.Repositories.AgeFeriadosLocalidadeRepository
{
    public interface IAgeFeriadosLocalidadesRepository
    {

        Task<Page<AgeFeriadosLocalidadesDAO>> ConsultarTodos(
            Pageable pageable);

        Task<Page<AgeFeriadosLocalidadesDAO>> ConsultarListaFiltro(
            string filtro,
            Pageable pageable);

        Task<AgeFeriadosLocalidadesDAO> ConsultarPorId(
            AgeFeriadosLocalidadesPKDAO ageFeriadosLocalidadePKDAO);

        Task<AgeFeriadosLocalidades> ConsultarCompletePorId(
            AgeFeriadosLocalidadesPKDAO ageFeriadosLocalidadePKDAO);

        Task<AgeFeriadosLocalidadesDAO> Insertar(
            AgeFeriadosLocalidades ageFeriadosLocalidade);

        Task<AgeFeriadosLocalidadesDAO> Actualizar(
            AgeFeriadosLocalidades ageFeriadosLocalidade);

        Task<List<AgeFeriadosLocalidadesDAO>> InsertarVarios(
            List<AgeFeriadosLocalidades> ageFeriadosLocalidadeList);

        Task<List<AgeFeriadosLocalidadesDAO>> ActualizarVarios(
            List<AgeFeriadosLocalidades> ageFeriadosLocalidadeList);

    }
}
