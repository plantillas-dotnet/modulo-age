﻿using AGE.Entities;
using AGE.Entities.DAO.AgeFeriadosLocalidade;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Utils;
using AGE.Utils.Paginacion;
using Microsoft.EntityFrameworkCore;

namespace AGE.Repositories.AgeFeriadosLocalidadeRepository
{
    public class AgeFeriadosLocalidadesRepository : IAgeFeriadosLocalidadesRepository
    {

        private readonly DbContextAge _dbContext;

        public AgeFeriadosLocalidadesRepository(DbContextAge dbContext)
        {
            _dbContext = dbContext;
        }


        public async Task<Page<AgeFeriadosLocalidadesDAO>> ConsultarTodos(Pageable pageable)
        {
            if (_dbContext.AgeFeriadosLocalidades == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgeFeriadosLocalidades
                .Where(p => p.Estado != Globales.ESTADO_ANULADO);

            return await Paginator<AgeFeriadosLocalidades, AgeFeriadosLocalidadesDAO>.Paginar(Query, pageable, FromEntityToDAO);
        }


        public async Task<AgeFeriadosLocalidadesDAO> ConsultarPorId(AgeFeriadosLocalidadesPKDAO ageFeriadosLocalidadePKDAO)
        {
            if (_dbContext.AgeFeriadosLocalidades == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeFeriadosLocalidades? ageFeriadosLocalidade = await _dbContext.AgeFeriadosLocalidades
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeAgeTipLoAgePaisCodigo == ageFeriadosLocalidadePKDAO.AgeAgeTipLoAgePaisCodigo &&
                            p.AgeLocaliAgeTipLoCodigo == ageFeriadosLocalidadePKDAO.AgeLocaliAgeTipLoCodigo &&
                            p.AgeLocaliCodigo == ageFeriadosLocalidadePKDAO.AgeLocaliCodigo &&
                            p.AgeDiaFeCodigo == ageFeriadosLocalidadePKDAO.AgeDiaFeCodigo).FirstOrDefaultAsync();

            return FromEntityToDAO(ageFeriadosLocalidade);
        }


        public async Task<AgeFeriadosLocalidades> ConsultarCompletePorId(AgeFeriadosLocalidadesPKDAO ageFeriadosLocalidadePKDAO)
        {
            if (_dbContext.AgeFeriadosLocalidades == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeFeriadosLocalidades? ageFeriadosLocalidade = await _dbContext.AgeFeriadosLocalidades
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeAgeTipLoAgePaisCodigo == ageFeriadosLocalidadePKDAO.AgeAgeTipLoAgePaisCodigo &&
                            p.AgeLocaliAgeTipLoCodigo == ageFeriadosLocalidadePKDAO.AgeLocaliAgeTipLoCodigo &&
                            p.AgeLocaliCodigo == ageFeriadosLocalidadePKDAO.AgeLocaliCodigo &&
                            p.AgeDiaFeCodigo == ageFeriadosLocalidadePKDAO.AgeDiaFeCodigo).FirstOrDefaultAsync();

            return ageFeriadosLocalidade;
        }


        public async Task<Page<AgeFeriadosLocalidadesDAO>> ConsultarListaFiltro(string filtro, Pageable pageable)
        {
            if (_dbContext.AgeFeriadosLocalidades == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgeFeriadosLocalidades
                .Where(p => p.Estado != Globales.ESTADO_ANULADO && (
                            p.AgeAgeTipLoAgePaisCodigo.ToString().ToLower().Contains(filtro) ||
                            p.AgeLocaliAgeTipLoCodigo.ToString().ToLower().Contains(filtro) ||
                            p.AgeLocaliCodigo.ToString().ToLower().Contains(filtro) ||
                            p.AgeDiaFeCodigo.ToString().ToLower().Contains(filtro)));

            return await Paginator<AgeFeriadosLocalidades, AgeFeriadosLocalidadesDAO>.Paginar(Query, pageable, FromEntityToDAO);
        }



        public async Task<AgeFeriadosLocalidadesDAO> Insertar(AgeFeriadosLocalidades ageFeriadosLocalidade)
        {
            _dbContext.AgeFeriadosLocalidades.Add(ageFeriadosLocalidade);
            await _dbContext.SaveChangesAsync();

            AgeFeriadosLocalidadesDAO ageFeriadosLocalidadeDAO = await ConsultarPorId(new AgeFeriadosLocalidadesPKDAO
            {
                AgeDiaFeCodigo = ageFeriadosLocalidade.AgeDiaFeCodigo,
                AgeAgeTipLoAgePaisCodigo = ageFeriadosLocalidade.AgeAgeTipLoAgePaisCodigo,
                AgeLocaliCodigo = ageFeriadosLocalidade.AgeLocaliCodigo,
                AgeLocaliAgeTipLoCodigo = ageFeriadosLocalidade.AgeLocaliAgeTipLoCodigo
            });

            return ageFeriadosLocalidadeDAO;
        }


        public async Task<List<AgeFeriadosLocalidadesDAO>> InsertarVarios(List<AgeFeriadosLocalidades> ageFeriadosLocalidadeList)
        {
            await _dbContext.AgeFeriadosLocalidades.AddRangeAsync(ageFeriadosLocalidadeList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeFeriadosLocalidades, AgeFeriadosLocalidadesDAO>
                .FromEntityToDAOList(ageFeriadosLocalidadeList, FromEntityToDAO);
        }



        public async Task<AgeFeriadosLocalidadesDAO> Actualizar(AgeFeriadosLocalidades ageFeriadosLocalidade)
        {
            _dbContext.Entry(ageFeriadosLocalidade).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();

            AgeFeriadosLocalidadesDAO ageFeriadosLocalidadeDAO = await ConsultarPorId(new AgeFeriadosLocalidadesPKDAO
            {
                AgeDiaFeCodigo = ageFeriadosLocalidade.AgeDiaFeCodigo,
                AgeAgeTipLoAgePaisCodigo = ageFeriadosLocalidade.AgeAgeTipLoAgePaisCodigo,
                AgeLocaliCodigo = ageFeriadosLocalidade.AgeLocaliCodigo,
                AgeLocaliAgeTipLoCodigo = ageFeriadosLocalidade.AgeLocaliAgeTipLoCodigo
            });

            return ageFeriadosLocalidadeDAO;
        }


        public async Task<List<AgeFeriadosLocalidadesDAO>> ActualizarVarios(List<AgeFeriadosLocalidades> ageFeriadosLocalidadeList)
        {
            _dbContext.AgeFeriadosLocalidades.UpdateRange(ageFeriadosLocalidadeList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeFeriadosLocalidades, AgeFeriadosLocalidadesDAO>
                .FromEntityToDAOList(ageFeriadosLocalidadeList, FromEntityToDAO);
        }



        private static AgeFeriadosLocalidadesDAO FromEntityToDAO(AgeFeriadosLocalidades entityObject)
        {
            if (entityObject == null)
                return null;

            return new AgeFeriadosLocalidadesDAO
            {
                Id = new AgeFeriadosLocalidadesPKDAO
                {
                    AgeAgeTipLoAgePaisCodigo = entityObject.AgeAgeTipLoAgePaisCodigo,
                    AgeLocaliAgeTipLoCodigo = entityObject.AgeLocaliAgeTipLoCodigo,
                    AgeLocaliCodigo = entityObject.AgeLocaliCodigo,
                    AgeDiaFeCodigo = entityObject.AgeDiaFeCodigo
                },
                FechaEjecucionDesde = entityObject.FechaEjecucionDesde,
                FechaEjecucionHasta = entityObject.FechaEjecucionHasta,
                HoraEjecucionDesde = entityObject.HoraEjecucionDesde,
                HoraEjecucionHasta = entityObject.HoraEjecucionHasta,
                Estado = entityObject.Estado
            };
        }

    }
}
