﻿using AGE.Entities.DAO.AgeLicenciatarios;
using AGE.Entities.DAO.AgePersonas;
using AGE.Entities;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Utils;
using Microsoft.EntityFrameworkCore;

namespace AGE.Repositories.AgePersonasRepository
{
    public interface IAgePersonasRepository
    {
        Task<Page<AgePersonasDAO>> ConsultarTodos(
           int codigoLicenciatario,
           int codigo,
           string apellidos,
           string nombres,
           string numeroIdentificacion,
           Pageable pageable);


        Task<AgePersonasDAO> ConsultarPorId(AgePersonasPKDAO agePersonasPKDAO);


        Task<AgePersonas> ConsultarCompletePorId(AgePersonasPKDAO agePersonasPKDAO);


        Task<Page<AgePersonasDAO>> ConsultarListaFiltro(
        int codigoLicenciatario,
            string filtro,
            Pageable pageable);

        Task<AgePersonasDAO> ConsultarLicencYNumeroIdent(
            int codigoLicenciatario,
            string identificacion);

        Task<AgePersonasDAO> ConsultarPorLicencYNumeroIdent(
            int codigoLicenciatario, 
            string numeroIdentificacion);

        Task<AgePersonas> ConsultarPorIdentificacion(
            string numeroIdentificacion);

        Task<AgePersonasDAO> Insertar(AgePersonas agePersona);


        Task<List<AgePersonasDAO>> InsertarVarios(List<AgePersonas> agePersonaList);
        Task<AgePersonasDAO> Actualizar(AgePersonas agePersona);

        Task<List<AgePersonasDAO>> ActualizarVarios(List<AgePersonas> agePersonaList);

        /*
        Task<AgePersonasDAO> ConsultarPorId(int codigo);

        Task<Page<AgePersonasDAO>> ConsultarTodos(int codigo, string descripcion, Pageable pageable);

        AgeLicenciatario ConsultarCompletePorId(
            int codigo);

        Task<Resource<AgePersonasDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor, AgeLicenciatariosSaveDAO ageLicenciatariosSaveDAO);

        Task<List<Resource<AgePersonasDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeLicenciatariosSaveDAO> ageLicenciatariosSaveDAOList
            );

        Task<AgePersonasDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeLicenciatariosSaveDAO ageLicenciatariosSaveDAO);

        Task<List<AgePersonasDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeLicenciatariosSaveDAO> ageLicenciatariosSaveDAOList);
        Task<Page<AgePersonasDAO>> ConsultarListaFiltro(string filtro, Pageable pageable);

        AgeLicenciatario ValidarInsert(
            IHttpContextAccessor _httpContextAccessor,
            AgeLicenciatariosSaveDAO ageLicenciatariosSaveDAO);*/
    }
}
