﻿using AGE.Entities.DAO.AgePersonas;
using AGE.Entities;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Utils.Paginacion;
using AGE.Utils;
using Microsoft.EntityFrameworkCore;

namespace AGE.Repositories.AgePersonasRepository
{
    public class AgePersonasRepository : IAgePersonasRepository
    {
        private readonly DbContextAge _dbContext;

        public AgePersonasRepository(DbContextAge dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Page<AgePersonasDAO>> ConsultarTodos(
            int codigoLicenciatario,
            int codigo,
            string apellidos,
            string nombres,
            string numeroIdentificacion,
            Pageable pageable)
        {
            if (_dbContext.AgePersonas == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgePersonas
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeLicencCodigo == codigoLicenciatario &&
                            (codigo == 0 || p.Codigo == codigo) &&
                            (apellidos.Equals("") || p.Apellidos.ToLower().Contains(apellidos.ToLower()) &&
                            (nombres.Equals("") || p.Nombres.ToLower().Contains(nombres.ToLower()) &&
                            (numeroIdentificacion.Equals("") || p.NumeroIdentificacion.ToLower().Contains(numeroIdentificacion.ToLower())))));

            return await Paginator<AgePersonas, AgePersonasDAO>.Paginar(Query, pageable, FromEntityToDAO);
        }


        public async Task<AgePersonasDAO> ConsultarPorId(AgePersonasPKDAO agePersonasPKDAO)
        {
            if (_dbContext.AgePersonas == null)
            {
                throw new RegisterNotFoundException();
            }

            AgePersonas? agePersona = await _dbContext.AgePersonas
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeLicencCodigo == agePersonasPKDAO.ageLicencCodigo &&
                            p.Codigo == agePersonasPKDAO.codigo).FirstOrDefaultAsync();

            return FromEntityToDAO(agePersona);
        }


        public async Task<AgePersonas> ConsultarCompletePorId(AgePersonasPKDAO agePersonasPKDAO)
        {
            if (_dbContext.AgePersonas == null)
            {
                throw new RegisterNotFoundException();
            }

            AgePersonas? agePersona = await _dbContext.AgePersonas
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeLicencCodigo == agePersonasPKDAO.ageLicencCodigo &&
                            p.Codigo == agePersonasPKDAO.codigo).FirstOrDefaultAsync();

            return agePersona;
        }


        public async Task<Page<AgePersonasDAO>> ConsultarListaFiltro(
            int codigoLicenciatario,
            string filtro,
            Pageable pageable)
        {
            if (_dbContext.AgePersonas == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgePersonas
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeLicencCodigo == codigoLicenciatario &&
                            (p.Codigo.ToString().ToLower().Contains(filtro) ||
                            p.Nombres.ToLower().Contains(filtro) ||
                            p.Apellidos.ToLower().Contains(filtro) ||
                            p.RazonSocial.ToLower().Contains(filtro)));

            return await Paginator<AgePersonas, AgePersonasDAO>.Paginar(Query, pageable, FromEntityToDAO);
        }

        public async Task<AgePersonasDAO> ConsultarLicencYNumeroIdent(
            int codigoLicenciatario,
            string identificacion)
        {
            if (_dbContext.AgePersonas == null)
            {
                throw new RegisterNotFoundException();
            }

            AgePersonas? agePersona = await _dbContext.AgePersonas
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeLicencCodigo == codigoLicenciatario &&
                            p.NumeroIdentificacion.Equals(identificacion))
                .FirstOrDefaultAsync();

            return FromEntityToDAO(agePersona);
        }


        public async Task<AgePersonasDAO> ConsultarPorLicencYNumeroIdent(
            int codigoLicenciatario, 
            string numeroIdentificacion)
        {
            if (_dbContext.AgePersonas == null)
            {
                throw new RegisterNotFoundException();
            }

            AgePersonas? agePersona = await _dbContext.AgePersonas
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeLicencCodigo == codigoLicenciatario &&
                            p.NumeroIdentificacion.Equals(numeroIdentificacion))
                            .FirstOrDefaultAsync();

            return FromEntityToDAO(agePersona);
        }


        public async Task<AgePersonas> ConsultarPorIdentificacion(
            string identificacion)
        {
            if (_dbContext.AgePersonas == null)
            {
                throw new RegisterNotFoundException();
            }

            AgePersonas? persona = await _dbContext.AgePersonas
               .Where(p => p.Estado == Globales.ESTADO_ACTIVO &&
                           p.NumeroIdentificacion.Equals(identificacion)).FirstOrDefaultAsync();

            return persona;
        }


        public async Task<AgePersonasDAO> Insertar(AgePersonas agePersona)
        {
            _dbContext.AgePersonas.Add(agePersona);
            await _dbContext.SaveChangesAsync();

            AgePersonasDAO agePersonasDAO = await ConsultarPorId(new AgePersonasPKDAO
            {
                ageLicencCodigo = agePersona.AgeLicencCodigo,
                codigo = agePersona.Codigo
            });

            return agePersonasDAO;
        }


        public async Task<List<AgePersonasDAO>> InsertarVarios(List<AgePersonas> agePersonaList)
        {
            await _dbContext.AgePersonas.AddRangeAsync(agePersonaList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgePersonas, AgePersonasDAO>
                .FromEntityToDAOList(agePersonaList, FromEntityToDAO);
        }

        public async Task<AgePersonasDAO> Actualizar(AgePersonas agePersona)
        {
            _dbContext.Entry(agePersona).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();

            AgePersonasDAO agePersonasDAO = await ConsultarPorId(new AgePersonasPKDAO
            {
                ageLicencCodigo = agePersona.AgeLicencCodigo,
                codigo = agePersona.Codigo
            });

            return agePersonasDAO;
        }

        public async Task<List<AgePersonasDAO>> ActualizarVarios(List<AgePersonas> agePersonaList)
        {
            _dbContext.AgePersonas.UpdateRange(agePersonaList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgePersonas, AgePersonasDAO>
                .FromEntityToDAOList(agePersonaList, FromEntityToDAO);
        }


        private static AgePersonasDAO FromEntityToDAO(AgePersonas entityObject)
        {

            if (entityObject == null)
            {
                return null;
            }

            return new AgePersonasDAO
            {
                Id = new AgePersonasPKDAO
                {
                    codigo = entityObject.Codigo,
                    ageLicencCodigo = entityObject.AgeLicencCodigo
                },
                AgeTipIdCodigo = entityObject.AgeTipIdCodigo,
                NumeroIdentificacion = entityObject.NumeroIdentificacion,
                Nombres = entityObject.Nombres,
                Apellidos = entityObject.Apellidos,
                AgeLocaliAgeTipLoCodigo = entityObject.AgeLocaliAgeTipLoCodigo,
                RazonSocial = entityObject.RazonSocial,
                NombreComercial = entityObject.NombreComercial,
                TelefonoCelular1 = entityObject.TelefonoCelular1,
                TelefonoCelular2 = entityObject.TelefonoCelular2,
                AgeLocaliCodigo = entityObject.AgeLocaliCodigo,
                FechaNacimiento = entityObject.FechaNacimiento,
                DireccionPrincipal = entityObject.DireccionPrincipal,
                CorreoElectronico = entityObject.CorreoElectronico,
                AgeAgeTipLoAgePaisCodigo = entityObject.AgeAgeTipLoAgePaisCodigo,
                Sexo = entityObject.Sexo,
                EstadoCivil = entityObject.EstadoCivil,
                TelefonoPrincipal = entityObject.TelefonoPrincipal,
                Estado = entityObject.Estado
            };

        }
    }
}
