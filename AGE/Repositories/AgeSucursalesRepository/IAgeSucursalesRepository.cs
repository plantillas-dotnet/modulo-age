﻿using AGE.Entities.DAO.AgeSucursales;
using AGE.Entities;
using AGE.Utils.Paginacion;

namespace AGE.Repositories.AgeSucursalesRepository
{
    public interface IAgeSucursalesRepository
    {
        Task<Page<AgeSucursalesDAO>> ConsultarTodos(
            int codigoLicenciatario,
            int? codigo,
            string? descripcion,
            string? direccion,
            Pageable pageable);

        Task<AgeSucursalesDAO> ConsultarPorId(AgeSucursalesPKDAO ageSucursalesPKDAO);

        Task<Page<AgeSucursalesDAO>> ConsultarListaFiltro(
            int codigoLicenciatario,
            string filtro,
            Pageable pageable);

        Task<AgeSucursales> ConsultarCompletePorId(
            AgeSucursalesPKDAO ageSucursalesPKDAO);

        Task<AgeSucursalesDAO> Insertar(AgeSucursales AgeSucursales);

        Task<List<AgeSucursalesDAO>> InsertarVarios(
            List<AgeSucursales> AgeSucursalesList);

        Task<AgeSucursalesDAO> Actualizar(AgeSucursales AgeSucursales);

        Task<List<AgeSucursalesDAO>> ActualizarVarios(
            List<AgeSucursales> AgeSucursalesList);
    }
}
