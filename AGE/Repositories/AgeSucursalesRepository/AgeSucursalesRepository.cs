﻿using AGE.Entities.DAO.AgeSucursales;
using AGE.Entities;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Utils.Paginacion;
using AGE.Utils;
using Microsoft.EntityFrameworkCore;

namespace AGE.Repositories.AgeSucursalesRepository
{
    public class AgeSucursalesRepository : IAgeSucursalesRepository
    {
        private readonly DbContextAge _dbContext;

        public AgeSucursalesRepository(DbContextAge dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Page<AgeSucursalesDAO>> ConsultarTodos(int codigoLicenciatario,
                                                                int? codigo,
                                                                string? descripcion,
                                                                string? direccion,
                                                                Pageable pageable)
        {
            if (_dbContext.AgeSucursales == null)
            {
                throw new RegisterNotFoundException();
            }

            var query = _dbContext.AgeSucursales
            .Where(a => a.Estado != Globales.ESTADO_ANULADO &&
                    (a.AgeLicencCodigo == codigoLicenciatario) &&
                    (codigo == null || codigo == 0 || a.Codigo == codigo) &&
                    (descripcion == null || descripcion == "" || a.Descripcion.ToLower().Contains(descripcion.ToLower()) &&
                    (direccion == null || direccion == "" || a.Direccion.ToLower().Contains(direccion.ToLower()))));

            return await Paginator<AgeSucursales, AgeSucursalesDAO>.Paginar(query, pageable, FromEntityToDAO);
        }

        public async Task<AgeSucursalesDAO> ConsultarPorId(AgeSucursalesPKDAO ageSucursalesPKDAO)
        {
            if (_dbContext.AgeSucursales == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeSucursales? ageSucursale = await _dbContext.AgeSucursales
                            .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeLicencCodigo == ageSucursalesPKDAO.AgeLicencCodigo &&
                            p.Codigo == ageSucursalesPKDAO.Codigo).FirstOrDefaultAsync();

            return FromEntityToDAO(ageSucursale);
        }

        public async Task<Page<AgeSucursalesDAO>> ConsultarListaFiltro(int codigoLicenciatario, string filtro, Pageable pageable)
        {
            if (_dbContext.AgeSucursales == null)
            {
                throw new RegisterNotFoundException();
            }

            var query = _dbContext.AgeSucursales
                .Where(a => a.Estado != Globales.ESTADO_ANULADO &&
                            a.AgeLicencCodigo == codigoLicenciatario &&
                            (a.Descripcion.ToUpper().Contains(filtro.ToUpper()) ||
                            a.Direccion.ToUpper().Contains(filtro.ToUpper()) ||
                            a.Codigo.ToString().ToUpper().Contains(filtro.ToUpper()) ||
                            (a.CodigoEstablecimiento != null && a.CodigoEstablecimiento.Value.ToString().ToUpper().Contains(filtro.ToUpper()))));

            return await Paginator<AgeSucursales, AgeSucursalesDAO>.Paginar(query, pageable, FromEntityToDAO);
        }

        public async Task<AgeSucursalesDAO> Insertar(AgeSucursales ageSucursale)
        {
            _dbContext.AgeSucursales.Add(ageSucursale);
            await _dbContext.SaveChangesAsync();

            AgeSucursalesDAO ageAgeSucursalesDAO = await ConsultarPorId(new AgeSucursalesPKDAO
            {
                AgeLicencCodigo = ageSucursale.AgeLicencCodigo,
                Codigo = ageSucursale.Codigo
            });

            return ageAgeSucursalesDAO;
        }

        public async Task<List<AgeSucursalesDAO>> InsertarVarios(List<AgeSucursales> ageSucursalesList)
        {
            _dbContext.AgeSucursales.AddRange(ageSucursalesList);
            _dbContext.SaveChanges();

            return ConversorEntityToDAOList<AgeSucursales, AgeSucursalesDAO>
                .FromEntityToDAOList(ageSucursalesList, FromEntityToDAO);
        }

        public async Task<AgeSucursales> ConsultarCompletePorId(AgeSucursalesPKDAO ageSucursalesPKDAO)
        {
            if (_dbContext.AgeSucursales == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeSucursales? ageSucursale = await _dbContext.AgeSucursales
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeLicencCodigo == ageSucursalesPKDAO.AgeLicencCodigo &&
                            p.Codigo == ageSucursalesPKDAO.Codigo)
                .FirstOrDefaultAsync();

            return ageSucursale;
        }

        public async Task<AgeSucursalesDAO> Actualizar(AgeSucursales AgeSucursale)
        {
            _dbContext.Entry(AgeSucursale).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();

            AgeSucursalesDAO ageAgeSucursalesDAO = await ConsultarPorId(new AgeSucursalesPKDAO
            {
                AgeLicencCodigo = AgeSucursale.AgeLicencCodigo,
                Codigo = AgeSucursale.Codigo
            });

            return ageAgeSucursalesDAO;
        }

        public async Task<List<AgeSucursalesDAO>> ActualizarVarios(List<AgeSucursales> AgeSucursalesList)
        {
            _dbContext.AgeSucursales.UpdateRange(AgeSucursalesList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeSucursales, AgeSucursalesDAO>
                .FromEntityToDAOList(AgeSucursalesList, FromEntityToDAO);
        }

        private static AgeSucursalesDAO? FromEntityToDAO(AgeSucursales ageSucursales)
        {
            if (ageSucursales == null)
            {
                return null;
            }

            return new AgeSucursalesDAO
            {

                Id = new AgeSucursalesPKDAO
                {
                    AgeLicencCodigo = ageSucursales.AgeLicencCodigo,
                    Codigo = ageSucursales.Codigo
                },
                Estado = ageSucursales.Estado,
                Descripcion = ageSucursales.Descripcion,
                AgeAgeTipLoAgePaisCodigo = ageSucursales.AgeAgeTipLoAgePaisCodigo,
                AgeLocaliAgeTipLoCodigo = ageSucursales.AgeLocaliAgeTipLoCodigo,
                AgeLocaliCodigo = ageSucursales.AgeLocaliCodigo,
                AgeTipSuAgeLicencCodigo = ageSucursales.AgeTipSuAgeLicencCodigo,
                AgeTipSuCodigo = ageSucursales.AgeTipSuCodigo,
                Direccion = ageSucursales.Direccion,
                Telefono1 = ageSucursales.Telefono1,
                EMail1 = ageSucursales.EMail1,
                EMail2 = ageSucursales.EMail2,
                AgeSucursAgeLicencCodigo = ageSucursales.AgeSucursAgeLicencCodigo,
                AgeSucursCodigo = ageSucursales.AgeSucursCodigo,
                CodigoEstablecimiento = ageSucursales.CodigoEstablecimiento,
                Telefono2 = ageSucursales.Telefono2,
                Observacion = ageSucursales.Observacion,
                Latitud = ageSucursales.Latitud,
                Longitud = ageSucursales.Longitud,
                CentroAcopio = ageSucursales.CentroAcopio
            };
        }
    }
}
