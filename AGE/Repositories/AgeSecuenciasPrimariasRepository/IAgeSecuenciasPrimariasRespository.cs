﻿using AGE.Entities;
using AGE.Entities.DAO.AgeSecuenciasPrimarias;
using AGE.Utils;
using AGE.Utils.Paginacion;

namespace AGE.Repositories.AgeSecuenciasPrimariasRepository
{
    public interface IAgeSecuenciasPrimariasRespository
    {
        Task<AgeSecuenciasPrimariaDAO> ConsultarPorId(
            int codigoSecuencia);

        Task<AgeSecuenciasPrimaria> ConsultarCompletePorId(
            int codigoSecuencia);

        Task<Page<AgeSecuenciasPrimariaDAO>> ConsultarTodos(
            Pageable pageable);

        Task<AgeSecuenciasPrimariaDAO> Insertar(
            AgeSecuenciasPrimaria ageSecuenciasPrimaria);

        Task<AgeSecuenciasPrimariaDAO> Actualizar(
            AgeSecuenciasPrimaria ageSecuenciasPrimaria);

        Task<List<AgeSecuenciasPrimariaDAO>> InsertarVarios(
            List<AgeSecuenciasPrimaria> ageSecuenciasPrimariaList);

        Task<List<AgeSecuenciasPrimariaDAO>> ActualizarVarios(
            List<AgeSecuenciasPrimaria> ageSecuenciasPrimariaList);

        void ActualizarSecuenciaPrimaria(
            AgeSecuenciasPrimaria ageSecuenciasPrimaria);

    }
}
