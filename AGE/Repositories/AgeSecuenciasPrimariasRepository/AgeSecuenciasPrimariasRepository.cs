﻿using Microsoft.EntityFrameworkCore;
using AGE.Entities;
using AGE.Entities.DAO.AgeSecuenciasPrimarias;
using AGE.Utils;
using AGE.Utils.Paginacion;
using AGE.Middleware.Exceptions.NotFound;

namespace AGE.Repositories.AgeSecuenciasPrimariasRepository
{
    public class AgeSecuenciasPrimariasRepository : IAgeSecuenciasPrimariasRespository
    {

        private readonly DbContextAge _dbContext;
        public AgeSecuenciasPrimariasRepository(DbContextAge dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Page<AgeSecuenciasPrimariaDAO>> ConsultarTodos(Pageable pageable)
        {
            if (_dbContext.AgeSecuenciasPrimarias == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgeSecuenciasPrimarias
                .Where(p => p.Estado != Globales.ESTADO_ANULADO);

            return await Paginator<AgeSecuenciasPrimaria, AgeSecuenciasPrimariaDAO>.Paginar(Query, pageable, FromEntityToDAO);
        }

        public async Task<AgeSecuenciasPrimariaDAO> ConsultarPorId(int codigoSecuencia)
        {
            if (_dbContext.AgeSecuenciasPrimarias == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeSecuenciasPrimaria? ageSecuenciasPrimaria = await _dbContext.AgeSecuenciasPrimarias
                .Where(p => p.Estado != Globales.ESTADO_ANULADO && 
                p.Codigo == codigoSecuencia).FirstOrDefaultAsync();
           
            return FromEntityToDAO(ageSecuenciasPrimaria);
        }

        public async Task<AgeSecuenciasPrimaria> ConsultarCompletePorId(int codigoSecuencia)
        {
            if (_dbContext.AgeSecuenciasPrimarias == null)
            {
                throw new RegisterNotFoundException($"No se encontro una Secuencia Primaria con el id {codigoSecuencia}");
            }

            AgeSecuenciasPrimaria? ageSecuenciasPrimaria = await _dbContext.AgeSecuenciasPrimarias
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                p.Codigo == codigoSecuencia).FirstOrDefaultAsync();

            return ageSecuenciasPrimaria;
        }

        public async Task<AgeSecuenciasPrimariaDAO> Insertar(
            AgeSecuenciasPrimaria ageSecuenciasPrimaria)
        {
            
            _dbContext.AgeSecuenciasPrimarias.Add(ageSecuenciasPrimaria);
            await _dbContext.SaveChangesAsync();
            
            AgeSecuenciasPrimariaDAO ageSecuenciasPrimariasDAO = await ConsultarPorId(ageSecuenciasPrimaria.Codigo);

            return ageSecuenciasPrimariasDAO;
        }


        public async Task<AgeSecuenciasPrimariaDAO> Actualizar(
            AgeSecuenciasPrimaria ageSecuenciasPrimaria)
        {

            _dbContext.Entry(ageSecuenciasPrimaria).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();

            AgeSecuenciasPrimariaDAO ageSecuenciasPrimariasDAO = await ConsultarPorId(ageSecuenciasPrimaria.Codigo);

            return ageSecuenciasPrimariasDAO;
        }


        public async Task<List<AgeSecuenciasPrimariaDAO>> InsertarVarios(
            List<AgeSecuenciasPrimaria> ageSecuenciasPrimariaList)
        {
            await _dbContext.AgeSecuenciasPrimarias.AddRangeAsync(ageSecuenciasPrimariaList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeSecuenciasPrimaria, AgeSecuenciasPrimariaDAO>
                .FromEntityToDAOList(ageSecuenciasPrimariaList, FromEntityToDAO);
        }


        public async Task<List<AgeSecuenciasPrimariaDAO>> ActualizarVarios(
            List<AgeSecuenciasPrimaria> ageSecuenciasPrimariaList)
        {
            _dbContext.AgeSecuenciasPrimarias.UpdateRange(ageSecuenciasPrimariaList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeSecuenciasPrimaria, AgeSecuenciasPrimariaDAO>
                .FromEntityToDAOList(ageSecuenciasPrimariaList, FromEntityToDAO);
        }


        public void ActualizarSecuenciaPrimaria(AgeSecuenciasPrimaria ageSecuenciasPrimaria)
        {
            using (var dbContext = new DbContextAge())
            {
                dbContext.Entry(ageSecuenciasPrimaria).State = EntityState.Modified;
                dbContext.SaveChanges();
            }
        }


        private  AgeSecuenciasPrimariaDAO  FromEntityToDAO(AgeSecuenciasPrimaria entityObject)
        {
            AgeSecuenciasPrimariaDAO DAOobject = null;

            if (entityObject != null)
            {
                DAOobject = new AgeSecuenciasPrimariaDAO
                {
                    Estado = entityObject.Estado,
                    Codigo = entityObject.Codigo,
                    Ciclica = entityObject.Ciclica,
                    Descripcion = entityObject.Descripcion,
                    IncrementaEn = entityObject.IncrementaEn,
                    ValorActual = entityObject.ValorActual,
                    ValorInicial = entityObject.ValorInicial
                };
            }
            
            return DAOobject;

        }

    }
}
