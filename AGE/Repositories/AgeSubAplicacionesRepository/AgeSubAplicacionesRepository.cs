﻿
using AGE.Entities.DAO.AgeSubAplicaciones;
using AGE.Entities;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Utils.Paginacion;
using AGE.Utils;
using Microsoft.EntityFrameworkCore;

namespace AGE.Repositories.AgeSubAplicacionesRepository
{
    public class AgeSubAplicacionesRepository : IAgeSubAplicacionesRepository
    {
        private readonly DbContextAge _dbContext;

        public AgeSubAplicacionesRepository(DbContextAge dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Page<AgeSubAplicacionesDAO>> ConsultarTodos(int codigoLicenciatario,
                                                                    int codigo,
                                                                    int ageLicApAgeAplicaCodigo,
                                                                    string contabiliza,
                                                                    string descripcion,
                                                                    Pageable pageable)
        {
            if (_dbContext.AgeSubAplicaciones == null)
            {
                throw new RegisterNotFoundException();
            }

            var query = _dbContext.AgeSubAplicaciones
            .Where(a => a.Estado != Globales.ESTADO_ANULADO &&
                    (codigo == 0 || a.Codigo == codigo) &&
                    (codigoLicenciatario == 0 || a.AgeLicApAgeLicencCodigo == codigoLicenciatario) &&
                    (ageLicApAgeAplicaCodigo == 0 || a.AgeLicApAgeAplicaCodigo == ageLicApAgeAplicaCodigo) &&
                    (contabiliza == null || contabiliza == "" || a.Contabiliza.ToLower().Contains(contabiliza.ToLower())) &&
                    (descripcion == null || descripcion == "" || a.Descripcion.ToLower().Contains(descripcion.ToLower())));

            return await Paginator<AgeSubAplicaciones, AgeSubAplicacionesDAO>.Paginar(query, pageable, FromEntityToDAO);
        }

        public async Task<AgeSubAplicacionesDAO> ConsultarPorId(AgeSubAplicacionesPKDAO ageSubAplicacionesPKDAO)
        {
            if (_dbContext.AgeSubAplicaciones == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeSubAplicaciones? ageSubAplicacione = await _dbContext.AgeSubAplicaciones
                            .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeLicApAgeAplicaCodigo == ageSubAplicacionesPKDAO.AgeLicApAgeAplicaCodigo &&
                            p.AgeLicApAgeLicencCodigo == ageSubAplicacionesPKDAO.AgeLicApAgeLicencCodigo &&
                            p.Codigo == ageSubAplicacionesPKDAO.Codigo).FirstOrDefaultAsync();

            return FromEntityToDAO(ageSubAplicacione);
        }

        public async Task<Page<AgeSubAplicacionesDAO>> ConsultarListaFiltro(int codigoLicenciatario, string filtro, Pageable pageable)
        {
            if (_dbContext.AgeSubAplicaciones == null)
            {
                throw new RegisterNotFoundException();
            }

            var query = _dbContext.AgeSubAplicaciones
                .Where(a => a.Estado != Globales.ESTADO_ANULADO &&
                            a.AgeLicApAgeLicencCodigo == codigoLicenciatario &&
                            a.Descripcion.ToUpper().Contains(filtro.ToUpper()) ||
                            a.Codigo.ToString().ToUpper().Contains(filtro.ToUpper()));

            return await Paginator<AgeSubAplicaciones, AgeSubAplicacionesDAO>.Paginar(query, pageable, FromEntityToDAO);
        }

        public async Task<AgeSubAplicacionesDAO> Insertar(AgeSubAplicaciones ageSubAplicacione)
        {
            _dbContext.AgeSubAplicaciones.Add(ageSubAplicacione);
            await _dbContext.SaveChangesAsync();

            AgeSubAplicacionesDAO ageAgeSubAplicacionesDAO = await ConsultarPorId(new AgeSubAplicacionesPKDAO
            {
                AgeLicApAgeLicencCodigo = ageSubAplicacione.AgeLicApAgeLicencCodigo,
                Codigo = ageSubAplicacione.Codigo,
                AgeLicApAgeAplicaCodigo = ageSubAplicacione.AgeLicApAgeAplicaCodigo
            });

            return ageAgeSubAplicacionesDAO;
        }

        public async Task<List<AgeSubAplicacionesDAO>> InsertarVarios(List<AgeSubAplicaciones> ageSubAplicacionesList)
        {
            _dbContext.AgeSubAplicaciones.AddRange(ageSubAplicacionesList);
            _dbContext.SaveChanges();

            return ConversorEntityToDAOList<AgeSubAplicaciones, AgeSubAplicacionesDAO>
                .FromEntityToDAOList(ageSubAplicacionesList, FromEntityToDAO);
        }

        public async Task<AgeSubAplicaciones> ConsultarCompletePorId(AgeSubAplicacionesPKDAO ageSubAplicacionesPKDAO)
        {
            if (_dbContext.AgeSubAplicaciones == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeSubAplicaciones? ageSubAplicacione = await _dbContext.AgeSubAplicaciones
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeLicApAgeLicencCodigo == ageSubAplicacionesPKDAO.AgeLicApAgeLicencCodigo &&
                            p.Codigo == ageSubAplicacionesPKDAO.Codigo &&
                            p.AgeLicApAgeAplicaCodigo == ageSubAplicacionesPKDAO.AgeLicApAgeAplicaCodigo)
                .FirstOrDefaultAsync();

            return ageSubAplicacione;
        }

        public async Task<AgeSubAplicacionesDAO> Actualizar(AgeSubAplicaciones AgeSubAplicacione)
        {
            _dbContext.Entry(AgeSubAplicacione).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();

            AgeSubAplicacionesDAO ageAgeSubAplicacionesDAO = await ConsultarPorId(new AgeSubAplicacionesPKDAO
            {
                AgeLicApAgeLicencCodigo = AgeSubAplicacione.AgeLicApAgeLicencCodigo,
                Codigo = AgeSubAplicacione.Codigo,
                AgeLicApAgeAplicaCodigo = AgeSubAplicacione.AgeLicApAgeAplicaCodigo
            });

            return ageAgeSubAplicacionesDAO;
        }

        public async Task<List<AgeSubAplicacionesDAO>> ActualizarVarios(List<AgeSubAplicaciones> AgeSubAplicacionesList)
        {
            _dbContext.AgeSubAplicaciones.UpdateRange(AgeSubAplicacionesList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeSubAplicaciones, AgeSubAplicacionesDAO>
                .FromEntityToDAOList(AgeSubAplicacionesList, FromEntityToDAO);
        }

        private static AgeSubAplicacionesDAO? FromEntityToDAO(AgeSubAplicaciones ageSubAplicaciones)
        {
            if (ageSubAplicaciones == null)
            {
                return null;
            }

            return new AgeSubAplicacionesDAO
            {

                Id = new AgeSubAplicacionesPKDAO
                {
                    AgeLicApAgeLicencCodigo = ageSubAplicaciones.AgeLicApAgeLicencCodigo,
                    AgeLicApAgeAplicaCodigo = ageSubAplicaciones.AgeLicApAgeAplicaCodigo,
                    Codigo = ageSubAplicaciones.Codigo
                },
                Estado = ageSubAplicaciones.Estado,
                AgeSubApCodigo = ageSubAplicaciones.AgeSubApCodigo,
                AgageLicApAgeAplicaCodigo = ageSubAplicaciones.AgageLicApAgeAplicaCodigo,
                AgageLicApAgeLicencCodigo = ageSubAplicaciones.AgageLicApAgeLicencCodigo,
                Descripcion = ageSubAplicaciones.Descripcion,
                Contabiliza = ageSubAplicaciones.Contabiliza
            };
        }
    }
}
