﻿using AGE.Entities.DAO.AgeSubAplicaciones;
using AGE.Entities;
using AGE.Utils.Paginacion;

namespace AGE.Repositories.AgeSubAplicacionesRepository
{
    public interface IAgeSubAplicacionesRepository
    {
        Task<Page<AgeSubAplicacionesDAO>> ConsultarTodos(
            int codigoLicenciatario,
            int codigo,
            int ageLicApAgeAplicaCodigo,
            string contabiliza,
            string descripcion,
            Pageable pageable);

        Task<AgeSubAplicacionesDAO> ConsultarPorId(AgeSubAplicacionesPKDAO ageSubAplicacionesPKDAO);

        Task<Page<AgeSubAplicacionesDAO>> ConsultarListaFiltro(
            int codigoLicenciatario,
            string filtro,
            Pageable pageable);

        Task<AgeSubAplicaciones> ConsultarCompletePorId(
            AgeSubAplicacionesPKDAO ageSubAplicacionesPKDAO);

        Task<AgeSubAplicacionesDAO> Insertar(AgeSubAplicaciones AgeSubAplicaciones);

        Task<List<AgeSubAplicacionesDAO>> InsertarVarios(
            List<AgeSubAplicaciones> AgeSubAplicacionesList);

        Task<AgeSubAplicacionesDAO> Actualizar(AgeSubAplicaciones AgeSubAplicaciones);

        Task<List<AgeSubAplicacionesDAO>> ActualizarVarios(
            List<AgeSubAplicaciones> AgeSubAplicacionesList);
    }
}
