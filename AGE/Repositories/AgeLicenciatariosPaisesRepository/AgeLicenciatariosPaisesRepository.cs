﻿using AGE.Entities;
using AGE.Entities.DAO.AgeLicenciatariosPaises;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Utils;
using AGE.Utils.Paginacion;
using Microsoft.EntityFrameworkCore;

namespace AGE.Repositories.AgeLicenciatariosPaisesRepository
{
    public class AgeLicenciatariosPaisesRepository : IAgeLicenciatariosPaisesRepository
    {

        private readonly DbContextAge _dbContext;

        public AgeLicenciatariosPaisesRepository(DbContextAge dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Page<AgeLicenciatariosPaisesDAO>> ConsultarTodos(
            int codigoLicenciatario,
            int? codigoPais,
            string? principal,
            int? AgeLicPaAgeLicencCodigo,
            int? AgeLicPaAgePaisCodigo,
            Pageable pageable)
        {
            if (_dbContext.AgeLicenciatariosPaises == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgeLicenciatariosPaises
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeLicencCodigo == codigoLicenciatario &&
                            (codigoPais == 0 || p.AgePaisCodigo == codigoPais) &&
                            (AgeLicPaAgeLicencCodigo == 0 || p.AgeLicPaAgeLicencCodigo == AgeLicPaAgeLicencCodigo) &&
                            (AgeLicPaAgePaisCodigo == 0 || p.AgeLicPaAgePaisCodigo == AgeLicPaAgePaisCodigo) &&
                            (principal.Equals("") || p.Principal.ToLower().Contains(principal.ToLower())));

            return await Paginator<AgeLicenciatariosPaise, AgeLicenciatariosPaisesDAO>.Paginar(Query, pageable, FromEntityToDAO);
        }


        public async Task<Page<AgeLicenciatariosPaisesDAO>> ConsultarListaFiltro(
            string filtro,
            Pageable pageable)
        {
            if (_dbContext.AgeLicenciatariosPaises == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgeLicenciatariosPaises
                .Where(p => p.Estado != Globales.ESTADO_ANULADO && (
                            p.AgeLicencCodigo.ToString().ToLower().Contains(filtro) ||
                            p.AgePaisCodigo.ToString().ToLower().Contains(filtro) ||
                            p.Principal.ToLower().Contains(filtro)));

            return await Paginator<AgeLicenciatariosPaise, AgeLicenciatariosPaisesDAO>.Paginar(Query, pageable, FromEntityToDAO);
        }


        public async Task<AgeLicenciatariosPaisesDAO> ConsultarPorId(AgeLicenciatariosPaisesPKDAO ageLicenciatariosPaisesPKDAO)
        {
            if (_dbContext.AgeLicenciatariosPaises == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeLicenciatariosPaise? ageLicenciatariosPaises = await _dbContext.AgeLicenciatariosPaises
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeLicencCodigo == ageLicenciatariosPaisesPKDAO.ageLicencCodigo &&
                            p.AgePaisCodigo == ageLicenciatariosPaisesPKDAO.agePaisCodigo).FirstOrDefaultAsync();

            return FromEntityToDAO(ageLicenciatariosPaises);
        }


        public async Task<AgeLicenciatariosPaise> ConsultarCompletePorId(AgeLicenciatariosPaisesPKDAO ageLicenciatariosPaisesPKDAO)
        {
            if (_dbContext.AgeLicenciatariosPaises == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeLicenciatariosPaise? ageLicenciatariosPaises = await _dbContext.AgeLicenciatariosPaises
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeLicencCodigo == ageLicenciatariosPaisesPKDAO.ageLicencCodigo &&
                            p.AgePaisCodigo == ageLicenciatariosPaisesPKDAO.agePaisCodigo).FirstOrDefaultAsync();

            return ageLicenciatariosPaises;
        }


        public async Task<AgeLicenciatariosPaisesDAO> Insertar(AgeLicenciatariosPaise ageLicenciatariosPaises)
        {
            _dbContext.AgeLicenciatariosPaises.Add(ageLicenciatariosPaises);
            await _dbContext.SaveChangesAsync();

            AgeLicenciatariosPaisesDAO ageLicenciatariosPaisesDAO = await ConsultarPorId(new AgeLicenciatariosPaisesPKDAO
            {
                ageLicencCodigo = ageLicenciatariosPaises.AgeLicencCodigo,
                agePaisCodigo = ageLicenciatariosPaises.AgePaisCodigo
            });

            return ageLicenciatariosPaisesDAO;
        }

        public async Task<AgeLicenciatariosPaisesDAO> Actualizar(AgeLicenciatariosPaise ageLicenciatariosPaises)
        {
            _dbContext.Entry(ageLicenciatariosPaises).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();

            AgeLicenciatariosPaisesDAO ageLicenciatariosPaisesDAO = await ConsultarPorId(new AgeLicenciatariosPaisesPKDAO
            {
                ageLicencCodigo = ageLicenciatariosPaises.AgeLicencCodigo,
                agePaisCodigo = ageLicenciatariosPaises.AgePaisCodigo
            });

            return ageLicenciatariosPaisesDAO;
        }

        public async Task<List<AgeLicenciatariosPaisesDAO>> InsertarVarios(List<AgeLicenciatariosPaise> ageLicenciatariosPaisesList)
        {
            await _dbContext.AgeLicenciatariosPaises.AddRangeAsync(ageLicenciatariosPaisesList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeLicenciatariosPaise, AgeLicenciatariosPaisesDAO>
                .FromEntityToDAOList(ageLicenciatariosPaisesList, FromEntityToDAO);
        }

        public async Task<List<AgeLicenciatariosPaisesDAO>> ActualizarVarios(List<AgeLicenciatariosPaise> ageLicenciatariosPaisesList)
        {
            _dbContext.AgeLicenciatariosPaises.UpdateRange(ageLicenciatariosPaisesList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeLicenciatariosPaise, AgeLicenciatariosPaisesDAO>
                .FromEntityToDAOList(ageLicenciatariosPaisesList, FromEntityToDAO);
        }

        private static AgeLicenciatariosPaisesDAO FromEntityToDAO(AgeLicenciatariosPaise entityObject)
        {
            if (entityObject == null)
            {
                return null;
            }

            return new AgeLicenciatariosPaisesDAO
            {
                Id = new AgeLicenciatariosPaisesPKDAO
                {
                    ageLicencCodigo = entityObject.AgeLicencCodigo,
                    agePaisCodigo = entityObject.AgePaisCodigo
                },
                AgeLicPaAgeLicencCodigo = entityObject.AgeLicPaAgeLicencCodigo,
                AgeLicPaAgePaisCodigo = entityObject.AgeLicPaAgePaisCodigo,
                Principal = entityObject.Principal,
                Estado = entityObject.Estado
            };
        }

    }
}
