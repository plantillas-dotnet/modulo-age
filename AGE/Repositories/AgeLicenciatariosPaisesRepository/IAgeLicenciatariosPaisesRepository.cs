﻿using AGE.Entities.DAO.AgeLicenciatariosPaises;
using AGE.Entities;
using AGE.Utils.Paginacion;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AGE.Repositories.AgeLicenciatariosPaisesRepository
{
    public interface IAgeLicenciatariosPaisesRepository
    {
        Task<Page<AgeLicenciatariosPaisesDAO>> ConsultarTodos(
            int codigoLicenciatario,
            int? codigoPais,
            string? principal,
            int? AgeLicPaAgeLicencCodigo,
            int? AgeLicPaAgePaisCodigo,
            Pageable pageable);

        Task<Page<AgeLicenciatariosPaisesDAO>> ConsultarListaFiltro(
            string filtro,
            Pageable pageable);

        Task<AgeLicenciatariosPaisesDAO> ConsultarPorId(
            AgeLicenciatariosPaisesPKDAO ageLicenciatariosPaisesPKDAO);

        Task<AgeLicenciatariosPaise> ConsultarCompletePorId(
            AgeLicenciatariosPaisesPKDAO ageLicenciatariosPaisesPKDAO);

        Task<AgeLicenciatariosPaisesDAO> Insertar(
            AgeLicenciatariosPaise ageLicenciatariosPaises);

        Task<AgeLicenciatariosPaisesDAO> Actualizar(
            AgeLicenciatariosPaise ageLicenciatariosPaises);

        Task<List<AgeLicenciatariosPaisesDAO>> InsertarVarios(
            List<AgeLicenciatariosPaise> ageLicenciatariosPaisesList);

        Task<List<AgeLicenciatariosPaisesDAO>> ActualizarVarios(
            List<AgeLicenciatariosPaise> ageLicenciatariosPaisesList);
    }
}
