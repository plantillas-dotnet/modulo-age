﻿using AGE.Entities;
using AGE.Entities.DAO.AgePaisesMonedas;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Utils;
using AGE.Utils.Paginacion;
using Microsoft.EntityFrameworkCore;

namespace AGE.Repositories.AgePaisesMonedasRepository
{
    public class AgePaisesMonedasRepository : IAgePaisesMonedasRepository
    {

        private readonly DbContextAge _dbContext;

        public AgePaisesMonedasRepository(DbContextAge dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Page<AgePaisesMonedasDAO>> ConsultarTodos(
            int codigoMoneda,
            int codigoPais,
            string descripcion,
            Pageable pageable)
        {
            if (_dbContext.AgePaisesMonedas == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgePaisesMonedas
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            (codigoMoneda == 0 || p.AgeMonedaCodigo == codigoMoneda) &&
                            (codigoPais == 0 || p.AgePaisCodigo == codigoPais) &&
                            (descripcion.Equals("") || p.Descripcion.ToLower().Contains(descripcion.ToLower())));

            return await Paginator<AgePaisesMonedas, AgePaisesMonedasDAO>.Paginar(Query, pageable, FromEntityToDAO);
        }


        public async Task<Page<AgePaisesMonedasDAO>> ConsultarListaFiltro(
            string filtro,
            Pageable pageable)
        {
            if (_dbContext.AgePaisesMonedas == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgePaisesMonedas
                .Where(p => p.Estado != Globales.ESTADO_ANULADO && (
                            p.AgeMonedaCodigo.ToString().ToLower().Contains(filtro) ||
                            p.Descripcion.ToLower().Contains(filtro)));

            return await Paginator<AgePaisesMonedas, AgePaisesMonedasDAO>.Paginar(Query, pageable, FromEntityToDAO);
        }


        public async Task<AgePaisesMonedasDAO> ConsultarPorId(AgePaisesMonedasPKDAO agePaisesMonedasPKDAO)
        {
            if (_dbContext.AgePaisesMonedas == null)
            {
                throw new RegisterNotFoundException();
            }

            AgePaisesMonedas? agePaisesMoneda = await _dbContext.AgePaisesMonedas
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgePaisCodigo == agePaisesMonedasPKDAO.AgePaisCodigo &&
                            p.AgeMonedaCodigo == agePaisesMonedasPKDAO.AgeMonedaCodigo).FirstOrDefaultAsync();

            return FromEntityToDAO(agePaisesMoneda);
        }


        public async Task<AgePaisesMonedas> ConsultarCompletePorId(AgePaisesMonedasPKDAO agePaisesMonedasPKDAO)
        {
            if (_dbContext.AgePaisesMonedas == null)
            {
                throw new RegisterNotFoundException();
            }

            AgePaisesMonedas? agePaisesMoneda = await _dbContext.AgePaisesMonedas
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgePaisCodigo == agePaisesMonedasPKDAO.AgePaisCodigo &&
                            p.AgeMonedaCodigo == agePaisesMonedasPKDAO.AgeMonedaCodigo).FirstOrDefaultAsync();

            return agePaisesMoneda;
        }


        public async Task<AgePaisesMonedasDAO> Insertar(AgePaisesMonedas agePaisesMoneda)
        {
            _dbContext.AgePaisesMonedas.Add(agePaisesMoneda);
            await _dbContext.SaveChangesAsync();

            AgePaisesMonedasDAO agePaisesMonedasDAO = await ConsultarPorId(new AgePaisesMonedasPKDAO
            {
                AgeMonedaCodigo = agePaisesMoneda.AgeMonedaCodigo,
                AgePaisCodigo = agePaisesMoneda.AgePaisCodigo
            });

            return agePaisesMonedasDAO;
        }

        public async Task<AgePaisesMonedasDAO> Actualizar(AgePaisesMonedas agePaisesMoneda)
        {
            _dbContext.Entry(agePaisesMoneda).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();

            AgePaisesMonedasDAO agePaisesMonedasDAO = await ConsultarPorId(new AgePaisesMonedasPKDAO
            {
                AgeMonedaCodigo = agePaisesMoneda.AgeMonedaCodigo,
                AgePaisCodigo = agePaisesMoneda.AgePaisCodigo
            });

            return agePaisesMonedasDAO;
        }

        public async Task<List<AgePaisesMonedasDAO>> InsertarVarios(List<AgePaisesMonedas> agePaisesMonedaList)
        {
            await _dbContext.AgePaisesMonedas.AddRangeAsync(agePaisesMonedaList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgePaisesMonedas, AgePaisesMonedasDAO>
                .FromEntityToDAOList(agePaisesMonedaList, FromEntityToDAO);
        }

        public async Task<List<AgePaisesMonedasDAO>> ActualizarVarios(List<AgePaisesMonedas> agePaisesMonedaList)
        {
            _dbContext.AgePaisesMonedas.UpdateRange(agePaisesMonedaList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgePaisesMonedas, AgePaisesMonedasDAO>
                .FromEntityToDAOList(agePaisesMonedaList, FromEntityToDAO);
        }

        private static AgePaisesMonedasDAO FromEntityToDAO(AgePaisesMonedas entityObject)
        {
            if (entityObject == null)
            {
                return null;
            }

            return new AgePaisesMonedasDAO
            {
                Id = new AgePaisesMonedasPKDAO
                {
                    AgeMonedaCodigo = entityObject.AgeMonedaCodigo,
                    AgePaisCodigo = entityObject.AgePaisCodigo
                },
                Descripcion = entityObject.Descripcion,
                TipoPrincipalSecundario = entityObject.TipoPrincipalSecundario,
                OrdenPrincipalSecundario = entityObject.OrdenPrincipalSecundario,
                Estado = entityObject.Estado
            };
        }

    }
}
