﻿using AGE.Entities.DAO.AgePaisesMonedas;
using AGE.Entities;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Utils.Paginacion;
using AGE.Utils;
using Microsoft.EntityFrameworkCore;

namespace AGE.Repositories.AgePaisesMonedasRepository
{
    public interface IAgePaisesMonedasRepository
    {
        Task<Page<AgePaisesMonedasDAO>> ConsultarTodos(
            int codigoMoneda,
            int codigoPais,
            string descripcion,
            Pageable pageable);
        Task<Page<AgePaisesMonedasDAO>> ConsultarListaFiltro(
            string filtro,
            Pageable pageable);
        Task<AgePaisesMonedasDAO> ConsultarPorId(AgePaisesMonedasPKDAO agePaisesMonedasPKDAO);
        Task<AgePaisesMonedas> ConsultarCompletePorId(AgePaisesMonedasPKDAO agePaisesMonedasPKDAO);
        Task<AgePaisesMonedasDAO> Insertar(AgePaisesMonedas agePaisesMoneda);
        Task<AgePaisesMonedasDAO> Actualizar(AgePaisesMonedas agePaisesMoneda);
        Task<List<AgePaisesMonedasDAO>> InsertarVarios(List<AgePaisesMonedas> agePaisesMonedaList);
        Task<List<AgePaisesMonedasDAO>> ActualizarVarios(List<AgePaisesMonedas> agePaisesMonedaList);
    }
}
