﻿using AGE.Entities.DAO.AgeLogUsuarios;
using AGE.Entities;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Utils.Paginacion;
using AGE.Utils;
using Microsoft.EntityFrameworkCore;

namespace AGE.Repositories.AgeLogUsuariosRepository
{
    public class AgeLogUsuariosRepository : IAgeLogUsuariosRepository
    {
        private readonly DbContextAge _dbContext;

        public AgeLogUsuariosRepository(DbContextAge dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Page<AgeLogUsuariosDAO>> ConsultarTodos(
             int ageUsuariAgeLicencCodigo,
             int ageUsuariCodigo,
             int codigo,
             string campo,
             string valorAnterior,
             string valorActual,
             Pageable pageable)
        {
            if (_dbContext.AgeLogUsuarios == null)
            {
                throw new RegisterNotFoundException();
            }

            var query = _dbContext.AgeLogUsuarios
                .Where(a => a.Estado != Globales.ESTADO_ANULADO &&
                            a.AgeUsuariAgeLicencCodigo == ageUsuariAgeLicencCodigo &&
                            (ageUsuariCodigo == 0 || a.AgeUsuariCodigo == ageUsuariCodigo) &&
                            (codigo == 0 || a.Codigo == codigo) &&
                            (string.IsNullOrEmpty(campo) || a.Campo.Contains(campo)) &&
                            (string.IsNullOrEmpty(valorAnterior) || a.ValorAnterior.Contains(valorAnterior)) &&
                            (string.IsNullOrEmpty(valorActual) || a.ValorActual.Contains(valorActual)));

            return await Paginator<AgeLogUsuarios, AgeLogUsuariosDAO>.Paginar(query, pageable, FromEntityToDAO);
        }

        public async Task<Page<AgeLogUsuariosDAO>> ConsultarListaFiltro(
            int AgeLicencCodigo,
            string filtro,
            Pageable pageable)
        {
            if (_dbContext.AgeLogUsuarios == null)
            {
                throw new RegisterNotFoundException();
            }

            var query = _dbContext.AgeLogUsuarios
                .Where(a => a.Estado != Globales.ESTADO_ANULADO &&
                            a.AgeUsuariAgeLicencCodigo == AgeLicencCodigo &&
                            a.AgeUsuariAgeLicencCodigo.ToString().ToUpper().Contains(filtro.ToUpper()) ||
                            a.Codigo.ToString().ToUpper().Contains(filtro.ToUpper()) ||
                            a.Campo.ToUpper().Contains(filtro.ToUpper()) ||
                            a.ValorAnterior.ToUpper().Contains(filtro.ToUpper()) ||
                            a.ValorActual.ToUpper().Contains(filtro.ToUpper()));

            return await Paginator<AgeLogUsuarios, AgeLogUsuariosDAO>.Paginar(query, pageable, FromEntityToDAO);
        }

        public async Task<AgeLogUsuariosDAO> ConsultarPorId(AgeLogUsuariosPKDAO ageLogUsuariosPKDAO)
        {
            if (_dbContext.AgeLogUsuarios == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeLogUsuarios? ageLogUsuarios = await _dbContext.AgeLogUsuarios
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeUsuariAgeLicencCodigo == ageLogUsuariosPKDAO.AgeUsuariAgeLicencCodigo &&
                            p.AgeUsuariCodigo == ageLogUsuariosPKDAO.AgeUsuariCodigo &&
                            p.Codigo == ageLogUsuariosPKDAO.Codigo).FirstOrDefaultAsync();

            return FromEntityToDAO(ageLogUsuarios);
        }

        public async Task<AgeLogUsuarios> ConsultarCompletePorId(AgeLogUsuariosPKDAO ageLogUsuariosPK)
        {
            if (_dbContext.AgeLogUsuarios == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeLogUsuarios? ageLogUsuarios = await _dbContext.AgeLogUsuarios
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeUsuariAgeLicencCodigo == ageLogUsuariosPK.AgeUsuariAgeLicencCodigo &&
                            p.AgeUsuariCodigo == ageLogUsuariosPK.AgeUsuariCodigo &&
                            p.Codigo == ageLogUsuariosPK.Codigo).FirstOrDefaultAsync();

            return ageLogUsuarios;
        }

        public async Task<AgeLogUsuariosDAO> Insertar(AgeLogUsuarios ageLogUsuarios)
        {
            _dbContext.AgeLogUsuarios.Add(ageLogUsuarios);
            await _dbContext.SaveChangesAsync();

            AgeLogUsuariosDAO ageLogUsuariosDAO = await ConsultarPorId(new AgeLogUsuariosPKDAO
            {
                AgeUsuariAgeLicencCodigo = ageLogUsuarios.AgeUsuariAgeLicencCodigo,
                AgeUsuariCodigo = ageLogUsuarios.AgeUsuariCodigo,
                Codigo = ageLogUsuarios.Codigo
            });

            return ageLogUsuariosDAO;
        }

        public async Task<List<AgeLogUsuariosDAO>> InsertarVarios(List<AgeLogUsuarios> ageLogUsuariosList)
        {
            await _dbContext.AgeLogUsuarios.AddRangeAsync(ageLogUsuariosList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeLogUsuarios, AgeLogUsuariosDAO>
                .FromEntityToDAOList(ageLogUsuariosList, FromEntityToDAO);
        }

        public async Task<AgeLogUsuariosDAO> Actualizar(AgeLogUsuarios ageLogUsuarios)
        {
            _dbContext.Entry(ageLogUsuarios).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();

            AgeLogUsuariosDAO ageLogUsuariosDAO = await ConsultarPorId(new AgeLogUsuariosPKDAO
            {
                AgeUsuariAgeLicencCodigo = ageLogUsuarios.AgeUsuariAgeLicencCodigo,
                AgeUsuariCodigo = ageLogUsuarios.AgeUsuariCodigo,
                Codigo = ageLogUsuarios.Codigo
            });

            return ageLogUsuariosDAO;
        }

        public async Task<List<AgeLogUsuariosDAO>> ActualizarVarios(List<AgeLogUsuarios> ageLogUsuariosList)
        {
            _dbContext.AgeLogUsuarios.UpdateRange(ageLogUsuariosList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeLogUsuarios, AgeLogUsuariosDAO>
                .FromEntityToDAOList(ageLogUsuariosList, FromEntityToDAO);
        }

        private static AgeLogUsuariosDAO FromEntityToDAO(AgeLogUsuarios entityObject)
        {
            if (entityObject == null)
            {
                return null;
            }

            return new AgeLogUsuariosDAO
            {
                Estado = entityObject.Estado,
                Id = new AgeLogUsuariosPKDAO
                {
                    AgeUsuariAgeLicencCodigo = entityObject.AgeUsuariAgeLicencCodigo,
                    AgeUsuariCodigo = entityObject.AgeUsuariCodigo,
                    Codigo = entityObject.Codigo
                },
                Campo = entityObject.Campo,
                ValorActual = entityObject.ValorActual,
                ValorAnterior = entityObject.ValorAnterior
            };
        }
    }
}
