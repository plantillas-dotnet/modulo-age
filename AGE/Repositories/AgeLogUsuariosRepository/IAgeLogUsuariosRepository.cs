﻿
using AGE.Entities.DAO.AgeLogUsuarios;
using AGE.Entities;
using AGE.Utils.Paginacion;

namespace AGE.Repositories.AgeLogUsuariosRepository
{
    public interface IAgeLogUsuariosRepository
    {
        Task<Page<AgeLogUsuariosDAO>> ConsultarTodos(
            int ageUsuariAgeLicencCodigo,
            int ageUsuariCodigo,
            int codigo,
            string campo,
            string valorAnterior,
            string valorActual,
            Pageable pageable);

        Task<AgeLogUsuariosDAO> ConsultarPorId(
            AgeLogUsuariosPKDAO ageLogUsuariosPKDAO);

        Task<AgeLogUsuarios> ConsultarCompletePorId(
            AgeLogUsuariosPKDAO ageLogUsuariosPKDAO);

        Task<Page<AgeLogUsuariosDAO>> ConsultarListaFiltro(
            int codigoLicenciatario,
            string filtro,
            Pageable pageable);

        Task<AgeLogUsuariosDAO> Insertar(
            AgeLogUsuarios ageLogUsuarios);

        Task<AgeLogUsuariosDAO> Actualizar(
            AgeLogUsuarios ageLogUsuarios);

        Task<List<AgeLogUsuariosDAO>> InsertarVarios(
            List<AgeLogUsuarios> ageLogUsuariosList);

        Task<List<AgeLogUsuariosDAO>> ActualizarVarios(
            List<AgeLogUsuarios> ageLogUsuariosList);
    }
}
