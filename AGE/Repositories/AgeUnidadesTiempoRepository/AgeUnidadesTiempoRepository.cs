﻿using AGE.Entities;
using AGE.Entities.DAO.AgeUnidadesTiempos;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Utils;
using AGE.Utils.Paginacion;
using Microsoft.EntityFrameworkCore;

namespace AGE.Repositories.AgeUnidadesTiempoRepository
{
    public class AgeUnidadesTiempoRepository :IAgeUnidadesTiempoRepository
    {
        private readonly DbContextAge _dbContext;

        public AgeUnidadesTiempoRepository(DbContextAge dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Page<AgeUnidadesTiempoDAO>> ConsultarTodos(
            int ageLicencCodigo,
            long codigo,
            string descripcion,
            string estadoAnulado,
            Pageable pageable)
        {
            if (_dbContext.AgeUnidadesTiempos == null)
            {
                throw new RegisterNotFoundException();
            }

            var query = _dbContext.AgeUnidadesTiempos
                .Where(a => a.Estado != estadoAnulado &&
                            a.AgeLicencCodigo == ageLicencCodigo &&
                            (codigo == 0 || a.Codigo == codigo) &&
                            (string.IsNullOrEmpty(descripcion) || a.Descripcion.Contains(descripcion)));

            return await Paginator<AgeUnidadesTiempo, AgeUnidadesTiempoDAO>.Paginar(query, pageable, FromEntityToDAO);
        }

        public async Task<Page<AgeUnidadesTiempoDAO>> ConsultarListaFiltro(
            int ageLicencCodigo,
            string filtro,
            Pageable pageable)
        {
            if (_dbContext.AgeUnidadesTiempos == null)
            {
                throw new RegisterNotFoundException();
            }

            var query = _dbContext.AgeUnidadesTiempos
                .Where(a => a.Estado != Globales.ESTADO_ANULADO &&
                            a.AgeLicencCodigo == ageLicencCodigo &&
                            a.Descripcion.ToUpper().Contains(filtro.ToUpper()));

            return await Paginator<AgeUnidadesTiempo, AgeUnidadesTiempoDAO>.Paginar(query, pageable, FromEntityToDAO);
        }

        public async Task<AgeUnidadesTiempoDAO> ConsultarPorId(AgeUnidadesTiempoPKDAO ageUnidadesTiempoPKDAO)
        {
            if (_dbContext.AgeUnidadesTiempos == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeUnidadesTiempo? ageUnidadesTiempo = await _dbContext.AgeUnidadesTiempos
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeLicencCodigo == ageUnidadesTiempoPKDAO.ageLicencCodigo &&
                            p.Codigo == ageUnidadesTiempoPKDAO.codigo).FirstOrDefaultAsync();

            return FromEntityToDAO(ageUnidadesTiempo);
        }

        public async Task<AgeUnidadesTiempo> ConsultarCompletePorId(AgeUnidadesTiempoPKDAO ageUnidadesTiempoPK)
        {
            if (_dbContext.AgeUnidadesTiempos == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeUnidadesTiempo? ageUnidadesTiempo = await _dbContext.AgeUnidadesTiempos
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeLicencCodigo == ageUnidadesTiempoPK.ageLicencCodigo &&
                            p.Codigo == ageUnidadesTiempoPK.codigo).FirstOrDefaultAsync();

            return ageUnidadesTiempo;
        }

        public async Task<AgeUnidadesTiempoDAO> Insertar(AgeUnidadesTiempo ageUnidadesTiempo)
        {
            _dbContext.AgeUnidadesTiempos.Add(ageUnidadesTiempo);
            await _dbContext.SaveChangesAsync();

            AgeUnidadesTiempoDAO ageUnidadesTiempoDAO = await ConsultarPorId(new AgeUnidadesTiempoPKDAO
            {
                ageLicencCodigo = ageUnidadesTiempo.AgeLicencCodigo,
                codigo = ageUnidadesTiempo.Codigo
            });

            return ageUnidadesTiempoDAO;
        }

        public async Task<List<AgeUnidadesTiempoDAO>> InsertarVarios(List<AgeUnidadesTiempo> ageUnidadesTiempoList)
        {
            await _dbContext.AgeUnidadesTiempos.AddRangeAsync(ageUnidadesTiempoList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeUnidadesTiempo, AgeUnidadesTiempoDAO>
                .FromEntityToDAOList(ageUnidadesTiempoList, FromEntityToDAO);
        }

        public async Task<AgeUnidadesTiempoDAO> Actualizar(AgeUnidadesTiempo ageUnidadesTiempo)
        {
            _dbContext.Entry(ageUnidadesTiempo).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();

            AgeUnidadesTiempoDAO ageUnidadesTiempoDAO = await ConsultarPorId(new AgeUnidadesTiempoPKDAO
            {
                ageLicencCodigo = ageUnidadesTiempo.AgeLicencCodigo,
                codigo = ageUnidadesTiempo.Codigo
            });

            return ageUnidadesTiempoDAO;
        }

        public async Task<List<AgeUnidadesTiempoDAO>> ActualizarVarios(List<AgeUnidadesTiempo> ageUnidadesTiempoList)
        {
            _dbContext.AgeUnidadesTiempos.UpdateRange(ageUnidadesTiempoList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeUnidadesTiempo, AgeUnidadesTiempoDAO>
                .FromEntityToDAOList(ageUnidadesTiempoList, FromEntityToDAO);
        }

        private static AgeUnidadesTiempoDAO FromEntityToDAO(AgeUnidadesTiempo entityObject)
        {
            if (entityObject == null)
            {
                return null;
            }

            return new AgeUnidadesTiempoDAO
            {

                Id = new AgeUnidadesTiempoPKDAO
                {
                    ageLicencCodigo = entityObject.AgeLicencCodigo,
                    codigo = entityObject.Codigo
                },
                Descripcion = entityObject.Descripcion,
                Estado = entityObject.Estado
            };
        }
    }
}