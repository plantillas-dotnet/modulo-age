﻿using AGE.Entities;
using AGE.Entities.DAO.AgeUnidadesTiempos;
using AGE.Utils.Paginacion;

namespace AGE.Repositories.AgeUnidadesTiempoRepository
{
    public interface IAgeUnidadesTiempoRepository
    {
        Task<Page<AgeUnidadesTiempoDAO>> ConsultarTodos(
            int ageLicencCodigo,
            long codigo,
            string descripcion,
            string estadoAnulado,
            Pageable pageable);

        Task<Page<AgeUnidadesTiempoDAO>> ConsultarListaFiltro(
            int ageLicencCodigo,
            string filtro,
            Pageable pageable);

        Task<AgeUnidadesTiempoDAO> ConsultarPorId(AgeUnidadesTiempoPKDAO ageUnidadesTiempoPKDAO);

        Task<AgeUnidadesTiempo> ConsultarCompletePorId(
            AgeUnidadesTiempoPKDAO ageUnidadesTiempoPK);

        Task<AgeUnidadesTiempoDAO> Insertar(
            AgeUnidadesTiempo ageUnidadesTiempo);

        Task<List<AgeUnidadesTiempoDAO>> InsertarVarios(
            List<AgeUnidadesTiempo> ageUnidadesTiempoList);

        Task<AgeUnidadesTiempoDAO> Actualizar(AgeUnidadesTiempo ageUnidadesTiempo);

        Task<List<AgeUnidadesTiempoDAO>> ActualizarVarios(
            List<AgeUnidadesTiempo> ageUnidadesTiempoList);
    }
}
