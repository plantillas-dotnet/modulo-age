﻿using AGE.Entities;
using AGE.Entities.DAO.AgeNotificacionesPush;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Utils;
using AGE.Utils.Paginacion;
using Microsoft.EntityFrameworkCore;

namespace AGE.Repositories.AgeNotificacionesPushRepository
{
    public class AgeNotificacionesPushRepository : IAgeNotificacionesPushRepository
    {

        private readonly DbContextAge _dbContext;

        public AgeNotificacionesPushRepository(DbContextAge dbContext)
        {
            _dbContext = dbContext;
        }


        public async Task<Page<AgeNotificacionesPushDAO>> ConsultarTodos(
            int codigoLicenciatario, 
            int codigo, 
            string mensaje, 
            string topico, 
            Pageable pageable)
        {
            if (_dbContext.AgePerfiles == null)
                throw new RegisterNotFoundException();

            var Query = _dbContext.AgeNotificacionesPushes
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeLicencCodigo == codigoLicenciatario &&
                            (codigo == 0 || p.Codigo == codigo) &&
                            (mensaje.Equals("") || p.Mensaje.ToLower().Contains(mensaje.ToLower())) &&
                            (topico.Equals("") || p.Topico.ToLower().Contains(topico.ToLower())));

            return await Paginator<AgeNotificacionesPush, AgeNotificacionesPushDAO>.Paginar(Query, pageable, FromEntityToDAO);
        }


        public async Task<Page<AgeNotificacionesPushDAO>> ConsultarListaFiltro(
            int codigoLicenciatario, 
            string filtro, 
            Pageable pageable)
        {
            if (_dbContext.AgePerfiles == null)
                throw new RegisterNotFoundException();

            var Query = _dbContext.AgeNotificacionesPushes
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeLicencCodigo == codigoLicenciatario &&
                            p.Codigo.ToString().ToLower().Contains(filtro) ||
                            p.Mensaje.ToLower().Contains(filtro) ||
                            p.Topico.ToLower().Contains(filtro));

            return await Paginator<AgeNotificacionesPush, AgeNotificacionesPushDAO>.Paginar(Query, pageable, FromEntityToDAO);
        }


        public async Task<AgeNotificacionesPushDAO> ConsultarPorId(
            AgeNotificacionesPushPKDAO ageNotificacionesPushPKDAO)
        {
            if (_dbContext.AgeNotificacionesPushes == null)
                throw new RegisterNotFoundException();

            AgeNotificacionesPush? ageNotificacionesPush = await _dbContext.AgeNotificacionesPushes
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeLicencCodigo == ageNotificacionesPushPKDAO.AgeLicencCodigo &&
                            p.Codigo == ageNotificacionesPushPKDAO.Codigo).FirstOrDefaultAsync();

            return FromEntityToDAO(ageNotificacionesPush);
        }



        public async Task<AgeNotificacionesPushDAO> Insertar(
            AgeNotificacionesPush ageNotificacionesPush)
        {
            _dbContext.AgeNotificacionesPushes.Add(ageNotificacionesPush);
            await _dbContext.SaveChangesAsync();

            AgeNotificacionesPushDAO ageNotificacionesPushDAO = await ConsultarPorId(new AgeNotificacionesPushPKDAO
            {
                AgeLicencCodigo = ageNotificacionesPush.AgeLicencCodigo,
                Codigo = ageNotificacionesPush.Codigo
            });

            return ageNotificacionesPushDAO;
        }


        public async Task<List<AgeNotificacionesPushDAO>> InsertarVarios(
            List<AgeNotificacionesPush> ageNotificacionesPushList)
        {
            await _dbContext.AgeNotificacionesPushes.AddRangeAsync(ageNotificacionesPushList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeNotificacionesPush, AgeNotificacionesPushDAO>
                .FromEntityToDAOList(ageNotificacionesPushList, FromEntityToDAO);
        }


        private static AgeNotificacionesPushDAO FromEntityToDAO(AgeNotificacionesPush entityObject)
        {

            if (entityObject == null)
                return null;

            return new AgeNotificacionesPushDAO
            {
                Id = new AgeNotificacionesPushPKDAO
                {
                    AgeLicencCodigo = entityObject.AgeLicencCodigo,
                    Codigo = entityObject.Codigo
                },
                Titulo = entityObject.Titulo,
                Mensaje = entityObject.Mensaje,
                Topico = entityObject.Topico,
                Data = entityObject.Data,
                IdFirebaseUser = entityObject.IdFirebaseUser,
                Imagen = entityObject.Imagen,
                AgeUsuariAgeLicencCodigo = entityObject.AgeUsuariAgeLicencCodigo,
                AgeUsuariCodigo = entityObject.AgeUsuariCodigo,
                Estado = entityObject.Estado
            };
        }

    }
}
