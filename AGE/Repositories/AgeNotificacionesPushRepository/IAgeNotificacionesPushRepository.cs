﻿using AGE.Entities;
using AGE.Entities.DAO.AgeNotificacionesPush;
using AGE.Entities.DAO.AgePerfiles;
using AGE.Utils.Paginacion;

namespace AGE.Repositories.AgeNotificacionesPushRepository
{
    public interface IAgeNotificacionesPushRepository
    {

        Task<Page<AgeNotificacionesPushDAO>> ConsultarTodos(
            int codigoLicenciatario,
            int codigo,
            string mensaje,
            string topico,
            Pageable pageable);


        Task<Page<AgeNotificacionesPushDAO>> ConsultarListaFiltro(
            int codigoLicenciatario,
            string filtro,
            Pageable pageable);


        Task<AgeNotificacionesPushDAO> ConsultarPorId(
            AgeNotificacionesPushPKDAO ageNotificacionesPushPKDAO);


        Task<AgeNotificacionesPushDAO> Insertar(
            AgeNotificacionesPush AgePerfile);


        Task<List<AgeNotificacionesPushDAO>> InsertarVarios(
            List<AgeNotificacionesPush> AgePerfilesList);

    }
}
