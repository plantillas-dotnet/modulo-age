﻿using AGE.Entities.DAO.AgeFirmasDigitales;
using AGE.Entities;
using AGE.Utils.Paginacion;

namespace AGE.Repositories.AgeFirmasDigitalesRepository
{
    public interface IAgeFirmasDigitalesRepository
    {
        Task<Page<AgeFirmasDigitalesDAO>> ConsultarTodos(
            int codigoLicenciatario,
            string? nombres,
            string? apellidos,
            string? tipoFirmaDigital,
            Pageable pageable);

        Task<AgeFirmasDigitalesDAO> ConsultarPorId(AgeFirmasDigitalesPKDAO ageFirmasDigitalesPKDAO);

        Task<Page<AgeFirmasDigitalesDAO>> ConsultarListaFiltro(
            int codigoLicenciatario,
            string filtro,
            Pageable pageable);

        Task<AgeFirmasDigitales> ConsultarCompletePorId(
            AgeFirmasDigitalesPKDAO ageFirmasDigitalesPKDAO);

        Task<AgeFirmasDigitalesDAO> Insertar(AgeFirmasDigitales AgeFirmasDigitales);

        Task<List<AgeFirmasDigitalesDAO>> InsertarVarios(
            List<AgeFirmasDigitales> AgeFirmasDigitalesList);

        Task<AgeFirmasDigitalesDAO> Actualizar(AgeFirmasDigitales AgeFirmasDigitales);

        Task<List<AgeFirmasDigitalesDAO>> ActualizarVarios(
            List<AgeFirmasDigitales> AgeFirmasDigitalesList);
    }
}
