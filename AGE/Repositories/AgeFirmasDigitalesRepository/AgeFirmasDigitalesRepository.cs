﻿using AGE.Entities.DAO.AgeFirmasDigitales;
using AGE.Entities;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Utils.Paginacion;
using AGE.Utils;
using Microsoft.EntityFrameworkCore;

namespace AGE.Repositories.AgeFirmasDigitalesRepository
{
    public class AgeFirmasDigitalesRepository : IAgeFirmasDigitalesRepository
    {
        private readonly DbContextAge _dbContext;

        public AgeFirmasDigitalesRepository(DbContextAge dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Page<AgeFirmasDigitalesDAO>> ConsultarTodos(int codigoLicenciatario,
                                                                string? nombres,
                                                                string? apellidos,
                                                                string? tipoFirmaDigital,
                                                                Pageable pageable)
        {
            if (_dbContext.AgeFirmasDigitales == null)
            {
                throw new RegisterNotFoundException();
            }

            var query = _dbContext.AgeFirmasDigitales
            .Where(a => a.Estado != Globales.ESTADO_ANULADO &&
                (a.AgeSucursAgeLicencCodigo == codigoLicenciatario) &&
                (nombres == null || nombres == "" || a.Nombres.ToLower().Contains(nombres.ToLower())) &&
                (apellidos == null || apellidos == "" || a.Apellidos.ToLower().Contains(apellidos.ToLower())) &&
                (tipoFirmaDigital == null || tipoFirmaDigital == "" || a.TipoFirmaDigital.ToLower().Contains(tipoFirmaDigital.ToLower())));

            return await Paginator<AgeFirmasDigitales, AgeFirmasDigitalesDAO>.Paginar(query, pageable, FromEntityToDAO);

        }

        public async Task<AgeFirmasDigitalesDAO> ConsultarPorId(AgeFirmasDigitalesPKDAO ageFirmasDigitalesPKDAO)
        {
            if (_dbContext.AgeFirmasDigitales == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeFirmasDigitales? ageFirmasDigitale = await _dbContext.AgeFirmasDigitales
                            .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeSucursAgeLicencCodigo == ageFirmasDigitalesPKDAO.AgeSucursAgeLicencCodigo &&
                            p.AgeSucursCodigo == ageFirmasDigitalesPKDAO.AgeSucursCodigo &&
                            p.Codigo == ageFirmasDigitalesPKDAO.Codigo).FirstOrDefaultAsync();

            return FromEntityToDAO(ageFirmasDigitale);
        }

        public async Task<Page<AgeFirmasDigitalesDAO>> ConsultarListaFiltro(int codigoLicenciatario, string filtro, Pageable pageable)
        {
            if (_dbContext.AgeFirmasDigitales == null)
            {
                throw new RegisterNotFoundException();
            }

            var query = _dbContext.AgeFirmasDigitales
                .Where(a => a.Estado != Globales.ESTADO_ANULADO &&
                            a.AgeSucursAgeLicencCodigo == codigoLicenciatario &&
                            (a.Nombres.ToUpper().Contains(filtro.ToUpper()) ||
                            a.Apellidos.ToUpper().Contains(filtro.ToUpper()) ||
                            a.Codigo.ToString().ToUpper().Contains(filtro.ToUpper()) ||
                            a.TipoFirmaDigital.ToUpper().Contains(filtro.ToUpper())));

            return await Paginator<AgeFirmasDigitales, AgeFirmasDigitalesDAO>.Paginar(query, pageable, FromEntityToDAO);
        }

        public async Task<AgeFirmasDigitalesDAO> Insertar(AgeFirmasDigitales ageFirmasDigitale)
        {
            _dbContext.AgeFirmasDigitales.Add(ageFirmasDigitale);
            await _dbContext.SaveChangesAsync();

            AgeFirmasDigitalesDAO ageAgeFirmasDigitalesDAO = await ConsultarPorId(new AgeFirmasDigitalesPKDAO
            {
                AgeSucursAgeLicencCodigo = ageFirmasDigitale.AgeSucursAgeLicencCodigo,
                AgeSucursCodigo = ageFirmasDigitale.AgeSucursCodigo,
                Codigo = ageFirmasDigitale.Codigo
            });

            return ageAgeFirmasDigitalesDAO;
        }

        public async Task<List<AgeFirmasDigitalesDAO>> InsertarVarios(List<AgeFirmasDigitales> ageFirmasDigitalesList)
        {
            _dbContext.AgeFirmasDigitales.AddRange(ageFirmasDigitalesList);
            _dbContext.SaveChanges();

            return ConversorEntityToDAOList<AgeFirmasDigitales, AgeFirmasDigitalesDAO>
                .FromEntityToDAOList(ageFirmasDigitalesList, FromEntityToDAO);
        }

        public async Task<AgeFirmasDigitales> ConsultarCompletePorId(AgeFirmasDigitalesPKDAO ageFirmasDigitalesPKDAO)
        {
            if (_dbContext.AgeFirmasDigitales == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeFirmasDigitales? ageFirmasDigitale = await _dbContext.AgeFirmasDigitales
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeSucursAgeLicencCodigo == ageFirmasDigitalesPKDAO.AgeSucursAgeLicencCodigo &&
                            p.AgeSucursCodigo == ageFirmasDigitalesPKDAO.AgeSucursCodigo &&
                            p.Codigo == ageFirmasDigitalesPKDAO.Codigo)
                .FirstOrDefaultAsync();

            return ageFirmasDigitale;
        }

        public async Task<AgeFirmasDigitalesDAO> Actualizar(AgeFirmasDigitales AgeFirmasDigitale)
        {
            _dbContext.Entry(AgeFirmasDigitale).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();

            AgeFirmasDigitalesDAO ageAgeFirmasDigitalesDAO = await ConsultarPorId(new AgeFirmasDigitalesPKDAO
            {
                AgeSucursAgeLicencCodigo = AgeFirmasDigitale.AgeSucursAgeLicencCodigo,
                AgeSucursCodigo = AgeFirmasDigitale.AgeSucursCodigo,
                Codigo = AgeFirmasDigitale.Codigo
            });

            return ageAgeFirmasDigitalesDAO;
        }

        public async Task<List<AgeFirmasDigitalesDAO>> ActualizarVarios(List<AgeFirmasDigitales> AgeFirmasDigitalesList)
        {
            _dbContext.AgeFirmasDigitales.UpdateRange(AgeFirmasDigitalesList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeFirmasDigitales, AgeFirmasDigitalesDAO>
                .FromEntityToDAOList(AgeFirmasDigitalesList, FromEntityToDAO);
        }

        private static AgeFirmasDigitalesDAO? FromEntityToDAO(AgeFirmasDigitales ageFirmasDigitales)
        {
            if (ageFirmasDigitales == null)
            {
                return null;
            }

            return new AgeFirmasDigitalesDAO
            {

                Id = new AgeFirmasDigitalesPKDAO
                {
                    AgeSucursAgeLicencCodigo = ageFirmasDigitales.AgeSucursAgeLicencCodigo,
                    AgeSucursCodigo = ageFirmasDigitales.AgeSucursCodigo,
                    Codigo = ageFirmasDigitales.Codigo
                },
                Estado = ageFirmasDigitales.Estado,
                Nombres = ageFirmasDigitales.Nombres,
                Apellidos = ageFirmasDigitales.Apellidos,
                FechaDesde = ageFirmasDigitales.FechaDesde,
                FechaHasta = ageFirmasDigitales.FechaHasta,
                TipoFirmaDigital = ageFirmasDigitales.TipoFirmaDigital,
                Clave = ageFirmasDigitales.Clave,
                NombreArchivo = ageFirmasDigitales.NombreArchivo,
                Url = ageFirmasDigitales.Url,
                FechaUltimaNotificacion = ageFirmasDigitales.FechaUltimaNotificacion
            };
        }
    }
}
