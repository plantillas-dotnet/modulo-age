﻿
using AGE.Entities;
using AGE.Utils.Paginacion;
using AGE.Entities.DAO.AgeLicenciatarioAplicacion;

namespace AGE.Repositories.AgeLicenciatariosAplicacionRepository
{
    public interface IAgeLicenciatarioAplicacionRepository
    {

        Task<Page<AgeLicenciatariosAplicacionesDAO>> ConsultarTodos(
            int codigoLicenciatario,
            int ageAplicaCodigo,
            int ageRutaCodigo,
            Pageable pageable);

        Task<Page<AgeLicenciatariosAplicacionesDAO>> ConsultarListaFiltro(
            int codigoLicenciatario,
            string filtro,
            Pageable pageable);

        Task<AgeLicenciatariosAplicacionesDAO> ConsultarPorId(
            AgeLicenciatariosAplicacionesPKDAO ageLicenciatariosAplicacionPKDAO);

        Task<AgeLicenciatariosAplicaciones> ConsultarCompletePorId(
            AgeLicenciatariosAplicacionesPKDAO ageLicenciatariosAplicacionPKDAO);

        Task<AgeLicenciatariosAplicacionesDAO> Insertar(
            AgeLicenciatariosAplicaciones ageLicenciatariosAplicacion);

        Task<AgeLicenciatariosAplicacionesDAO> Actualizar(
            AgeLicenciatariosAplicaciones ageLicenciatariosAplicacion);

        Task<List<AgeLicenciatariosAplicacionesDAO>> InsertarVarios(
            List<AgeLicenciatariosAplicaciones> ageLicenciatariosAplicacionList);

        Task<List<AgeLicenciatariosAplicacionesDAO>> ActualizarVarios(
            List<AgeLicenciatariosAplicaciones> ageLicenciatariosAplicacionList);

    }
}
