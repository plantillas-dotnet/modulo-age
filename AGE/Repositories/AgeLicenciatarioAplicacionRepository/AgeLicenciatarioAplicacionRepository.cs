﻿using AGE.Entities;
using AGE.Entities.DAO.AgeLicenciatarioAplicacion;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Utils;
using AGE.Utils.Paginacion;
using Microsoft.EntityFrameworkCore;

namespace AGE.Repositories.AgeLicenciatariosAplicacionRepository
{
    public class AgeLicenciatarioAplicacionRepository : IAgeLicenciatarioAplicacionRepository
    {

        private readonly DbContextAge _dbContext;

        public AgeLicenciatarioAplicacionRepository(DbContextAge dbContext)
        {
            _dbContext = dbContext;
        }


        public async Task<Page<AgeLicenciatariosAplicacionesDAO>> ConsultarTodos(
            int codigoLicenciatario, 
            int ageAplicaCodigo, 
            int ageRutaCodigo, 
            Pageable pageable)
        {
            if (_dbContext.AgeLicenciatariosAplicacions == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgeLicenciatariosAplicacions
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            (codigoLicenciatario == 0 || p.AgeLicencCodigo == codigoLicenciatario) &&
                            (ageAplicaCodigo == 0 || p.AgeAplicaCodigo == ageAplicaCodigo) &&
                            (ageRutaCodigo == 0 || p.AgeRutaCodigo == ageRutaCodigo));

            return await Paginator<AgeLicenciatariosAplicaciones, AgeLicenciatariosAplicacionesDAO>.Paginar(Query, pageable, FromEntityToDAO);
        }


        public async Task<Page<AgeLicenciatariosAplicacionesDAO>> ConsultarListaFiltro(
            int codigoLicenciatario, 
            string filtro, 
            Pageable pageable)
        {
            if (_dbContext.AgeLicenciatariosAplicacions == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgeLicenciatariosAplicacions
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeLicencCodigo == codigoLicenciatario && (
                            p.AgeAplicaCodigo.ToString().ToLower().Contains(filtro) ||
                            (p.AgeRutaCodigo == null ? "" : p.AgeRutaCodigo.ToString()).ToLower().Contains(filtro)));

            return await Paginator<AgeLicenciatariosAplicaciones, AgeLicenciatariosAplicacionesDAO>.Paginar(Query, pageable, FromEntityToDAO);
        }


        public async Task<AgeLicenciatariosAplicacionesDAO> ConsultarPorId(
            AgeLicenciatariosAplicacionesPKDAO ageLicenciatariosAplicacionPKDAO)
        {
            if (_dbContext.AgeLicenciatariosAplicacions == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeLicenciatariosAplicaciones? ageLicenciatariosAplicacion = await _dbContext.AgeLicenciatariosAplicacions
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeLicencCodigo == ageLicenciatariosAplicacionPKDAO.AgeLicencCodigo &&
                            p.AgeAplicaCodigo == ageLicenciatariosAplicacionPKDAO.AgeAplicaCodigo).FirstOrDefaultAsync();

            return FromEntityToDAO(ageLicenciatariosAplicacion);
        }


        public async Task<AgeLicenciatariosAplicaciones> ConsultarCompletePorId(
            AgeLicenciatariosAplicacionesPKDAO ageLicenciatariosAplicacionPKDAO)
        {
            if (_dbContext.AgeLicenciatariosAplicacions == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeLicenciatariosAplicaciones? ageLicenciatariosAplicacion = await _dbContext.AgeLicenciatariosAplicacions
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeLicencCodigo == ageLicenciatariosAplicacionPKDAO.AgeLicencCodigo &&
                            p.AgeAplicaCodigo == ageLicenciatariosAplicacionPKDAO.AgeAplicaCodigo).FirstOrDefaultAsync();

            return ageLicenciatariosAplicacion;
        }


        
        public async Task<AgeLicenciatariosAplicacionesDAO> Insertar(
            AgeLicenciatariosAplicaciones ageLicenciatariosAplicacion)
        {
            _dbContext.AgeLicenciatariosAplicacions.Add(ageLicenciatariosAplicacion);
            await _dbContext.SaveChangesAsync();

            AgeLicenciatariosAplicacionesDAO ageLicenciatariosAplicacionDAO = await ConsultarPorId(new AgeLicenciatariosAplicacionesPKDAO
            {
                AgeLicencCodigo = ageLicenciatariosAplicacion.AgeLicencCodigo,
                AgeAplicaCodigo = ageLicenciatariosAplicacion.AgeAplicaCodigo
            });

            return ageLicenciatariosAplicacionDAO;
        }


        public async Task<List<AgeLicenciatariosAplicacionesDAO>> InsertarVarios(
            List<AgeLicenciatariosAplicaciones> ageLicenciatariosAplicacionList)
        {
            await _dbContext.AgeLicenciatariosAplicacions.AddRangeAsync(ageLicenciatariosAplicacionList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeLicenciatariosAplicaciones, AgeLicenciatariosAplicacionesDAO>
                .FromEntityToDAOList(ageLicenciatariosAplicacionList, FromEntityToDAO);
        }


        public async Task<AgeLicenciatariosAplicacionesDAO> Actualizar(
            AgeLicenciatariosAplicaciones ageLicenciatariosAplicacion)
        {
            _dbContext.Entry(ageLicenciatariosAplicacion).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();

            AgeLicenciatariosAplicacionesDAO ageLicenciatariosAplicacionDAO = await ConsultarPorId(new AgeLicenciatariosAplicacionesPKDAO
            {
                AgeLicencCodigo = ageLicenciatariosAplicacion.AgeLicencCodigo,
                AgeAplicaCodigo = ageLicenciatariosAplicacion.AgeAplicaCodigo
            });

            return ageLicenciatariosAplicacionDAO;
        }


        public async Task<List<AgeLicenciatariosAplicacionesDAO>> ActualizarVarios(
            List<AgeLicenciatariosAplicaciones> ageLicenciatariosAplicacionList)
        {
            _dbContext.AgeLicenciatariosAplicacions.UpdateRange(ageLicenciatariosAplicacionList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeLicenciatariosAplicaciones, AgeLicenciatariosAplicacionesDAO>
                .FromEntityToDAOList(ageLicenciatariosAplicacionList, FromEntityToDAO);
        }


        private static AgeLicenciatariosAplicacionesDAO FromEntityToDAO(AgeLicenciatariosAplicaciones entityObject)
        {

            if (entityObject == null)
                return null;

            return new AgeLicenciatariosAplicacionesDAO
            {
                Id = new AgeLicenciatariosAplicacionesPKDAO
                {
                    AgeLicencCodigo = entityObject.AgeLicencCodigo,
                    AgeAplicaCodigo = entityObject.AgeAplicaCodigo
                },
                AgeRutaAgeLicencCodigo = entityObject.AgeRutaAgeLicencCodigo,
                AgeRutaCodigo = entityObject.AgeRutaCodigo,
                Estado = entityObject.Estado
            };
        }

    }
}
