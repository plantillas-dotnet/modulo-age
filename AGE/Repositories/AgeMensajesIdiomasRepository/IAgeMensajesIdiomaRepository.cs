﻿
using AGE.Entities;
using AGE.Utils.Paginacion;
using AGE.Entities.DAO.AgeMensajesIdiomas;

namespace AGE.Repositories.AgeMensajesIdiomasRepository
{
    public interface IAgeMensajesIdiomaRepository
    {

        Task<Page<AgeMensajesIdiomaDAO>> ConsultarTodos(
            long ageMensajCodigo,
            int ageIdiomaCodigo,
            string descripcionMsg,
            string solucionMsg,
            Pageable pageable);

        Task<Page<AgeMensajesIdiomaDAO>> ConsultarListaFiltro(
            string filtro,
            Pageable pageable);

        Task<AgeMensajesIdiomaDAO> ConsultarPorId(
            AgeMensajesIdiomaPKDAO ageMensajesIdiomaPKDAO);

        Task<AgeMensajesIdiomas> ConsultarCompletePorId(
            AgeMensajesIdiomaPKDAO ageMensajesIdiomaPKDAO);

        Task<AgeMensajesIdiomaDAO> Insertar(
            AgeMensajesIdiomas ageMensajesIdioma);

        Task<AgeMensajesIdiomaDAO> Actualizar(
            AgeMensajesIdiomas ageMensajesIdioma);

        Task<List<AgeMensajesIdiomaDAO>> InsertarVarios(
            List<AgeMensajesIdiomas> ageMensajesIdiomaList);

        Task<List<AgeMensajesIdiomaDAO>> ActualizarVarios(
            List<AgeMensajesIdiomas> ageMensajesIdiomaList);

    }
}
