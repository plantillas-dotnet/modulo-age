﻿using AGE.Entities;
using AGE.Entities.DAO.AgeMensajesIdiomas;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Utils;
using AGE.Utils.Paginacion;
using Microsoft.EntityFrameworkCore;

namespace AGE.Repositories.AgeMensajesIdiomasRepository
{
    public class AgeMensajesIdiomaRepository : IAgeMensajesIdiomaRepository
    {

        private readonly DbContextAge _dbContext;

        public AgeMensajesIdiomaRepository(DbContextAge dbContext)
        {
            _dbContext = dbContext;
        }


        public async Task<Page<AgeMensajesIdiomaDAO>> ConsultarTodos(
            long ageMensajCodigo, 
            int ageIdiomaCodigo,
            string descripcionMsg, 
            string solucionMsg, 
            Pageable pageable)
        {
            if (_dbContext.AgeMensajesIdiomas == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgeMensajesIdiomas
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            (ageIdiomaCodigo == 0 || p.AgeIdiomaCodigo == ageIdiomaCodigo) &&
                            (ageMensajCodigo == 0 || p.AgeMensajCodigo == ageMensajCodigo) &&
                            (descripcionMsg.Equals("") || p.DescripcionMsg.ToLower().Contains(descripcionMsg.ToLower())) &&
                            (solucionMsg.Equals("") || p.SolucionMsg.ToLower().Contains(solucionMsg.ToLower())));

            return await Paginator<AgeMensajesIdiomas, AgeMensajesIdiomaDAO>.Paginar(Query, pageable, FromEntityToDAO);
        }


        public async Task<Page<AgeMensajesIdiomaDAO>> ConsultarListaFiltro(
            string filtro, 
            Pageable pageable)
        {
            if (_dbContext.AgeMensajesIdiomas == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgeMensajesIdiomas
                .Where(p => p.Estado != Globales.ESTADO_ANULADO && (
                            p.AgeIdiomaCodigo.ToString().ToLower().Contains(filtro) ||
                            p.DescripcionMsg.ToLower().Contains(filtro) ||
                            p.SolucionMsg.ToLower().Contains(filtro)));

            return await Paginator<AgeMensajesIdiomas, AgeMensajesIdiomaDAO>.Paginar(Query, pageable, FromEntityToDAO);
        }



        public async Task<AgeMensajesIdiomaDAO> ConsultarPorId(
            AgeMensajesIdiomaPKDAO ageMensajesIdiomaPKDAO)
        {
            if (_dbContext.AgeMensajesIdiomas == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeMensajesIdiomas? ageMensajesIdioma = await _dbContext.AgeMensajesIdiomas
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeMensajCodigo == ageMensajesIdiomaPKDAO.AgeMensajCodigo &&
                            p.AgeIdiomaCodigo == ageMensajesIdiomaPKDAO.AgeIdiomaCodigo).FirstOrDefaultAsync();

            return FromEntityToDAO(ageMensajesIdioma);
        }



        public async Task<AgeMensajesIdiomas> ConsultarCompletePorId(
            AgeMensajesIdiomaPKDAO ageMensajesIdiomaPKDAO)
        {
            if (_dbContext.AgeMensajesIdiomas == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeMensajesIdiomas? ageMensajesIdioma = await _dbContext.AgeMensajesIdiomas
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeMensajCodigo == ageMensajesIdiomaPKDAO.AgeMensajCodigo &&
                            p.AgeIdiomaCodigo == ageMensajesIdiomaPKDAO.AgeIdiomaCodigo).FirstOrDefaultAsync();

            return ageMensajesIdioma;
        }



        public async Task<AgeMensajesIdiomaDAO> Insertar(
            AgeMensajesIdiomas ageMensajesIdioma)
        {
            _dbContext.AgeMensajesIdiomas.Add(ageMensajesIdioma);
            await _dbContext.SaveChangesAsync();

            AgeMensajesIdiomaDAO ageMensajesIdiomaDAO = await ConsultarPorId(new AgeMensajesIdiomaPKDAO
            {
                AgeIdiomaCodigo = ageMensajesIdioma.AgeIdiomaCodigo,
                AgeMensajCodigo = ageMensajesIdioma.AgeMensajCodigo
            });

            return ageMensajesIdiomaDAO;
        }


        public async Task<List<AgeMensajesIdiomaDAO>> InsertarVarios(
            List<AgeMensajesIdiomas> ageMensajesIdiomaList)
        {
            await _dbContext.AgeMensajesIdiomas.AddRangeAsync(ageMensajesIdiomaList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeMensajesIdiomas, AgeMensajesIdiomaDAO>
                .FromEntityToDAOList(ageMensajesIdiomaList, FromEntityToDAO);
        }


        public async Task<AgeMensajesIdiomaDAO> Actualizar(
            AgeMensajesIdiomas ageMensajesIdioma)
        {
            _dbContext.Entry(ageMensajesIdioma).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();

            AgeMensajesIdiomaDAO ageMensajesIdiomaDAO = await ConsultarPorId(new AgeMensajesIdiomaPKDAO
            {
                AgeIdiomaCodigo = ageMensajesIdioma.AgeIdiomaCodigo,
                AgeMensajCodigo = ageMensajesIdioma.AgeMensajCodigo
            });

            return ageMensajesIdiomaDAO;
        }
    


        public async Task<List<AgeMensajesIdiomaDAO>> ActualizarVarios(
            List<AgeMensajesIdiomas> ageMensajesIdiomaList)
        {
            _dbContext.AgeMensajesIdiomas.UpdateRange(ageMensajesIdiomaList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeMensajesIdiomas, AgeMensajesIdiomaDAO>
                .FromEntityToDAOList(ageMensajesIdiomaList, FromEntityToDAO);
        }


        private static AgeMensajesIdiomaDAO FromEntityToDAO(AgeMensajesIdiomas entityObject)
        {
            if (entityObject == null)
            {
                return null;
            }

            return new AgeMensajesIdiomaDAO
            {
                Id = new AgeMensajesIdiomaPKDAO
                {
                    AgeIdiomaCodigo = entityObject.AgeIdiomaCodigo,
                    AgeMensajCodigo = entityObject.AgeMensajCodigo
                },
                DescripcionMsg = entityObject.DescripcionMsg,
                SolucionMsg = entityObject.SolucionMsg,
                Estado = entityObject.Estado
            };
        }


    }
}
