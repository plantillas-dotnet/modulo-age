﻿using AGE.Entities;
using AGE.Utils.Paginacion;
using AGE.Entities.DAO.AgeAplicacionesDependencias;

namespace AGE.Repositories.AgeAplicacionesDependenciasRepository
{
    public interface IAgeAplicacionesDependenciasRepository
    {
        Task<Page<AgeAplicacionesDependenciasDAO>> ConsultarTodos(
            Pageable pageable);

        Task<Page<AgeAplicacionesDependenciasDAO>> ConsultarListaFiltro(
            string filtro,
            Pageable pageable);

        Task<AgeAplicacionesDependenciasDAO> ConsultarPorId(
            AgeAplicacionesDependenciasPKDAO AgeAplicacionesDependenciasPKDAO);

        Task<AgeAplicacionesDependencia> ConsultarCompletePorId(
            AgeAplicacionesDependenciasPKDAO AgeAplicacionesDependenciasPKDAO);

        Task<AgeAplicacionesDependenciasDAO> Insertar(
            AgeAplicacionesDependencia AgeAplicacionesDependencias);

        Task<List<AgeAplicacionesDependenciasDAO>> InsertarVarios(
            List<AgeAplicacionesDependencia> AgeAplicacionesDependenciasList);

        Task<AgeAplicacionesDependenciasDAO> Actualizar(AgeAplicacionesDependencia AgeAplicacionesDependencias);

        Task<List<AgeAplicacionesDependenciasDAO>> ActualizarVarios(
            List<AgeAplicacionesDependencia> AgeAplicacionesDependenciasList);
    }
}

