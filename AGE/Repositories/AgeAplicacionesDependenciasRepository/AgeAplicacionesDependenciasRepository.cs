﻿using AGE.Entities.DAO.AgeAplicacionesDependencias;
using AGE.Entities;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Utils.Paginacion;
using AGE.Utils;
using Microsoft.EntityFrameworkCore;

namespace AGE.Repositories.AgeAplicacionesDependenciasRepository
{
    public class AgeAplicacionesDependenciasRepository : IAgeAplicacionesDependenciasRepository
    {
        private readonly DbContextAge _dbContext;

        public AgeAplicacionesDependenciasRepository(DbContextAge dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Page<AgeAplicacionesDependenciasDAO>> ConsultarTodos(
            Pageable pageable)
        {
            if (_dbContext.AgeAplicacionesDependencias == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgeAplicacionesDependencias
                .Where(p => p.Estado != Globales.ESTADO_ANULADO);

            return await Paginator<AgeAplicacionesDependencia, AgeAplicacionesDependenciasDAO>.Paginar(Query, pageable, FromEntityToDAO);
        }

        public async Task<Page<AgeAplicacionesDependenciasDAO>> ConsultarListaFiltro(
            string filtro,
            Pageable pageable)
        {
            if (_dbContext.AgeAplicacionesDependencias == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgeAplicacionesDependencias
                .Where(p => p.Estado != Globales.ESTADO_ANULADO && (
                            p.AgeAplicaCodigo.ToString().ToLower().Contains(filtro) ||
                            p.AgeAplicaCodigoSer.ToString().ToLower().Contains(filtro) ||
                            p.OrdenDependencia.ToString().ToLower().Contains(filtro)));

            return await Paginator<AgeAplicacionesDependencia, AgeAplicacionesDependenciasDAO>.Paginar(Query, pageable, FromEntityToDAO);
        }


        public async Task<AgeAplicacionesDependenciasDAO> ConsultarPorId(AgeAplicacionesDependenciasPKDAO ageAplicacionesDependenciasPKDAO)
        {
            if (_dbContext.AgeAplicacionesDependencias == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeAplicacionesDependencia? ageAplicacionesDependencia = await _dbContext.AgeAplicacionesDependencias
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeAplicaCodigo == ageAplicacionesDependenciasPKDAO.AgeAplicaCodigo &&
                            p.AgeAplicaCodigoSer == ageAplicacionesDependenciasPKDAO.AgeAplicaCodigoSer &&
                            p.OrdenDependencia == ageAplicacionesDependenciasPKDAO.OrdenDependencia).FirstOrDefaultAsync();

            return FromEntityToDAO(ageAplicacionesDependencia);
        }


        public async Task<AgeAplicacionesDependencia> ConsultarCompletePorId(AgeAplicacionesDependenciasPKDAO ageAplicacionesDependenciasPKDAO)
        {
            if (_dbContext.AgeAplicacionesDependencias == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeAplicacionesDependencia? ageAplicacionesDependencia = await _dbContext.AgeAplicacionesDependencias
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeAplicaCodigo == ageAplicacionesDependenciasPKDAO.AgeAplicaCodigo &&
                            p.AgeAplicaCodigoSer == ageAplicacionesDependenciasPKDAO.AgeAplicaCodigoSer &&
                            p.OrdenDependencia == ageAplicacionesDependenciasPKDAO.OrdenDependencia).FirstOrDefaultAsync();

            return ageAplicacionesDependencia;
        }


        public async Task<AgeAplicacionesDependenciasDAO> Insertar(
            AgeAplicacionesDependencia ageAplicacionesDependencias)
        {
            _dbContext.AgeAplicacionesDependencias.Add(ageAplicacionesDependencias);
            await _dbContext.SaveChangesAsync();

            AgeAplicacionesDependenciasDAO ageAplicacionesDependenciasDAO = await ConsultarPorId(new AgeAplicacionesDependenciasPKDAO
            {
                AgeAplicaCodigo = ageAplicacionesDependencias.AgeAplicaCodigo,
                AgeAplicaCodigoSer = ageAplicacionesDependencias.AgeAplicaCodigoSer,
                OrdenDependencia = ageAplicacionesDependencias.OrdenDependencia
            });

            return ageAplicacionesDependenciasDAO;
        }

        public async Task<List<AgeAplicacionesDependenciasDAO>> InsertarVarios(
            List<AgeAplicacionesDependencia> ageAplicacionesDependenciasList)
        {
            await _dbContext.AgeAplicacionesDependencias.AddRangeAsync(ageAplicacionesDependenciasList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeAplicacionesDependencia, AgeAplicacionesDependenciasDAO>
                .FromEntityToDAOList(ageAplicacionesDependenciasList, FromEntityToDAO);
        }


        public async Task<AgeAplicacionesDependenciasDAO> Actualizar(AgeAplicacionesDependencia ageAplicacionesDependencias)
        {
            _dbContext.Entry(ageAplicacionesDependencias).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();

            AgeAplicacionesDependenciasDAO ageAplicacionesDependenciasDAO = await ConsultarPorId(new AgeAplicacionesDependenciasPKDAO
            {
                AgeAplicaCodigo = ageAplicacionesDependencias.AgeAplicaCodigo,
                AgeAplicaCodigoSer = ageAplicacionesDependencias.AgeAplicaCodigoSer,
                OrdenDependencia = ageAplicacionesDependencias.OrdenDependencia
            });

            return ageAplicacionesDependenciasDAO;
        }


        public async Task<List<AgeAplicacionesDependenciasDAO>> ActualizarVarios(List<AgeAplicacionesDependencia> ageAplicacionesDependenciasList)
        {
            _dbContext.AgeAplicacionesDependencias.UpdateRange(ageAplicacionesDependenciasList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeAplicacionesDependencia, AgeAplicacionesDependenciasDAO>
                .FromEntityToDAOList(ageAplicacionesDependenciasList, FromEntityToDAO);
        }


        private static AgeAplicacionesDependenciasDAO FromEntityToDAO(AgeAplicacionesDependencia entityObject)
        {

            if (entityObject == null)
            {
                return null;
            }

            return new AgeAplicacionesDependenciasDAO
            {
                Id = new AgeAplicacionesDependenciasPKDAO
                {
                    AgeAplicaCodigo = entityObject.AgeAplicaCodigo,
                    AgeAplicaCodigoSer = entityObject.AgeAplicaCodigoSer,
                    OrdenDependencia = entityObject.OrdenDependencia
                },
                Estado = entityObject.Estado
            };
        }
    }
}
