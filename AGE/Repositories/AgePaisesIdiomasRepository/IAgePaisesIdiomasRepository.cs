﻿using AGE.Entities.DAO.AgePaisesIdioma;
using AGE.Utils.Paginacion;
using AGE.Utils;
using AGE.Middleware.Exceptions.NotFound;
using System.Collections.Generic;
using System.Threading.Tasks;
using AGE.Entities;
using AGE.Entities.DAO.AgeTiposPersonas;

namespace AGE.Repositories.AgePaisesIdiomaRepository
{
    public interface IAgePaisesIdiomasRepository
    {
        Task<Page<AgePaisesIdiomasDAO>> ConsultarTodos(
            int codigoIdioma,
            int codigoPais,
            string descripcion,
            Pageable pageable);

        Task<Page<AgePaisesIdiomasDAO>> ConsultarListaFiltro(
            string filtro,
            Pageable pageable);

        Task<AgePaisesIdiomasDAO> ConsultarPorId(
            AgePaisesIdiomasPKDAO agePaisesIdiomaPKDAO);

        Task<AgePaisesIdiomas> ConsultarCompletePorId(
            AgePaisesIdiomasPKDAO agePaisesIdiomaPKDAO);

        Task<AgePaisesIdiomasDAO> Insertar(
            AgePaisesIdiomas agePaisesIdioma);

        Task<AgePaisesIdiomasDAO> Actualizar(
            AgePaisesIdiomas agePaisesIdioma);

        Task<List<AgePaisesIdiomasDAO>> InsertarVarios(
            List<AgePaisesIdiomas> agePaisesIdiomaList);

        Task<List<AgePaisesIdiomasDAO>> ActualizarVarios(
            List<AgePaisesIdiomas> agePaisesIdiomaList);
    }
}
