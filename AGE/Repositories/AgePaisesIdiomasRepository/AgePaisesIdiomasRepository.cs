﻿using AGE.Entities;
using AGE.Entities.DAO.AgePaisesIdioma;
using AGE.Entities.DAO.AgeTiposLocalidades;
using AGE.Entities.DAO.AgeTiposPersonas;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Utils;
using AGE.Utils.Paginacion;
using Microsoft.EntityFrameworkCore;

namespace AGE.Repositories.AgePaisesIdiomaRepository
{
    public class AgePaisesIdiomasRepository : IAgePaisesIdiomasRepository
    {

        private readonly DbContextAge _dbContext;

        public AgePaisesIdiomasRepository(DbContextAge dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Page<AgePaisesIdiomasDAO>> ConsultarTodos(
            int codigoIdioma,
            int codigoPais,
            string descripcion,
            Pageable pageable)
        {
            if (_dbContext.AgePaisesIdiomas == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgePaisesIdiomas
                .Where(p => p.Estado != Globales.ESTADO_ANULADO && 
                            (codigoIdioma == 0 || p.AgeIdiomaCodigo == codigoIdioma) &&
                            (codigoPais == 0 || p.AgePaisCodigo == codigoPais) &&
                            (descripcion.Equals("") || p.Descripcion.ToLower().Contains(descripcion.ToLower())));

            return await Paginator<AgePaisesIdiomas, AgePaisesIdiomasDAO>.Paginar(Query, pageable, FromEntityToDAO);
        }


        public async Task<Page<AgePaisesIdiomasDAO>> ConsultarListaFiltro(
            string filtro,
            Pageable pageable)
        {
            if (_dbContext.AgePaisesIdiomas == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgePaisesIdiomas
                .Where(p => p.Estado != Globales.ESTADO_ANULADO && (
                            p.AgeIdiomaCodigo.ToString().ToLower().Contains(filtro) ||
                            p.Descripcion.ToLower().Contains(filtro)));

            return await Paginator<AgePaisesIdiomas, AgePaisesIdiomasDAO>.Paginar(Query, pageable, FromEntityToDAO);
        }


        public async Task<AgePaisesIdiomasDAO> ConsultarPorId(AgePaisesIdiomasPKDAO agePaisesIdiomaPKDAO)
        {
            if (_dbContext.AgePaisesIdiomas == null)
            {
                throw new RegisterNotFoundException();
            }

            AgePaisesIdiomas? agePaisesIdioma = await _dbContext.AgePaisesIdiomas
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgePaisCodigo == agePaisesIdiomaPKDAO.agePaisCodigo &&
                            p.AgeIdiomaCodigo == agePaisesIdiomaPKDAO.ageIdiomaCodigo).FirstOrDefaultAsync();

            return FromEntityToDAO(agePaisesIdioma);
        }


        public async Task<AgePaisesIdiomas> ConsultarCompletePorId(AgePaisesIdiomasPKDAO agePaisesIdiomaPKDAO)
        {
            if (_dbContext.AgePaisesIdiomas == null)
            {
                throw new RegisterNotFoundException();
            }

            AgePaisesIdiomas? agePaisesIdioma = await _dbContext.AgePaisesIdiomas
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgePaisCodigo == agePaisesIdiomaPKDAO.agePaisCodigo &&
                            p.AgeIdiomaCodigo == agePaisesIdiomaPKDAO.ageIdiomaCodigo).FirstOrDefaultAsync();

            return agePaisesIdioma;
        }


        public async Task<AgePaisesIdiomasDAO> Insertar(AgePaisesIdiomas agePaisesIdioma)
        {
            _dbContext.AgePaisesIdiomas.Add(agePaisesIdioma);
            await _dbContext.SaveChangesAsync();

            AgePaisesIdiomasDAO agePaisesIdiomaDAO = await ConsultarPorId(new AgePaisesIdiomasPKDAO
            {
                ageIdiomaCodigo = agePaisesIdioma.AgeIdiomaCodigo,
                agePaisCodigo = agePaisesIdioma.AgePaisCodigo
            });

            return agePaisesIdiomaDAO;
        }

        public async Task<AgePaisesIdiomasDAO> Actualizar(AgePaisesIdiomas agePaisesIdioma)
        {
            _dbContext.Entry(agePaisesIdioma).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();

            AgePaisesIdiomasDAO agePaisesIdiomaDAO = await ConsultarPorId(new AgePaisesIdiomasPKDAO
            {
                ageIdiomaCodigo = agePaisesIdioma.AgeIdiomaCodigo,
                agePaisCodigo = agePaisesIdioma.AgePaisCodigo
            });

            return agePaisesIdiomaDAO;
        }

        public async Task<List<AgePaisesIdiomasDAO>> InsertarVarios(List<AgePaisesIdiomas> agePaisesIdiomaList)
        {
            await _dbContext.AgePaisesIdiomas.AddRangeAsync(agePaisesIdiomaList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgePaisesIdiomas, AgePaisesIdiomasDAO>
                .FromEntityToDAOList(agePaisesIdiomaList, FromEntityToDAO);
        }

        public async Task<List<AgePaisesIdiomasDAO>> ActualizarVarios(List<AgePaisesIdiomas> agePaisesIdiomaList)
        {
            _dbContext.AgePaisesIdiomas.UpdateRange(agePaisesIdiomaList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgePaisesIdiomas, AgePaisesIdiomasDAO>
                .FromEntityToDAOList(agePaisesIdiomaList, FromEntityToDAO);
        }

        private static AgePaisesIdiomasDAO FromEntityToDAO(AgePaisesIdiomas entityObject)
        {
            if (entityObject == null)
            {
                return null;
            }

            return new AgePaisesIdiomasDAO
            {
                Id = new AgePaisesIdiomasPKDAO
                {
                    ageIdiomaCodigo = entityObject.AgeIdiomaCodigo,
                    agePaisCodigo = entityObject.AgePaisCodigo
                },
                Descripcion = entityObject.Descripcion,
                TipoPrincipalSecundario = entityObject.TipoPrincipalSecundario,
                OrdenPrincipalSecundario = entityObject.OrdenPrincipalSecundario,
                Estado = entityObject.Estado
            };
        }

    }
}
