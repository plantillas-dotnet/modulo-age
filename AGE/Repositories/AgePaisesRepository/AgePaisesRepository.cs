﻿using AGE.Entities;
using AGE.Entities.DAO.AgePaises;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Utils;
using AGE.Utils.Paginacion;
using Microsoft.EntityFrameworkCore;

namespace AGE.Repositories.AgePaisesRepository
{
    public class AgePaisesRepository : IAgePaisesRepository
    {

        private readonly DbContextAge _dbContext;

        public AgePaisesRepository(DbContextAge dbContext)
        {
            _dbContext = dbContext;
        }


        public async Task<Page<AgePaisesDAO>> ConsultarTodos(
            int codigo,
            string descripcion,
            Pageable pageable)
        {
            if (_dbContext.AgePaises == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgePaises
                .Where(p => p.Estado != Globales.ESTADO_ANULADO && (codigo == 0 || p.Codigo == codigo) &&
                            (descripcion.Equals("") || p.Descripcion.ToLower().Contains(descripcion.ToLower())));

            return await Paginator<AgePaises, AgePaisesDAO>.Paginar(Query, pageable, FromEntityToDAO);
        }


        public async Task<Page<AgePaisesDAO>> ConsultarListaFiltro(
            string filtro,
            Pageable pageable)
        {
            if (_dbContext.AgePaises == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgePaises
                .Where(p => p.Estado != Globales.ESTADO_ANULADO && (
                            p.Codigo.ToString().ToLower().Contains(filtro) ||
                            p.Descripcion.ToLower().Contains(filtro)));

            return await Paginator<AgePaises, AgePaisesDAO>.Paginar(Query, pageable, FromEntityToDAO);
        }


        public async Task<AgePaisesDAO> ConsultarPorId(int codigo)
        {
            if (_dbContext.AgePaises == null)
            {
                throw new RegisterNotFoundException();
            }

            AgePaises? agePaises = await _dbContext.AgePaises
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                p.Codigo == codigo).FirstOrDefaultAsync();

            return FromEntityToDAO(agePaises);
        }


        public async Task<AgePaises> ConsultarCompletePorId(int codigo)
        {
            if (_dbContext.AgePaises == null)
            {
                throw new RegisterNotFoundException();
            }

            AgePaises? agePaises = await _dbContext.AgePaises
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                p.Codigo == codigo).FirstOrDefaultAsync();

            return agePaises;
        }


        public async Task<AgePaisesDAO> Insertar(AgePaises agePaises)
        {
            _dbContext.AgePaises.Add(agePaises);
            await _dbContext.SaveChangesAsync();

            AgePaisesDAO agePaisesDAO = await ConsultarPorId(agePaises.Codigo);

            return agePaisesDAO;
        }

        public async Task<AgePaisesDAO> Actualizar(AgePaises agePaises)
        {
            _dbContext.Entry(agePaises).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();

            AgePaisesDAO agePaisesDAO = await ConsultarPorId(agePaises.Codigo);

            return agePaisesDAO;
        }

        public async Task<List<AgePaisesDAO>> InsertarVarios(List<AgePaises> agePaisesList)
        {
            await _dbContext.AgePaises.AddRangeAsync(agePaisesList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgePaises, AgePaisesDAO>
                .FromEntityToDAOList(agePaisesList, FromEntityToDAO);
        }

        public async Task<List<AgePaisesDAO>> ActualizarVarios(List<AgePaises> agePaisesList)
        {
            _dbContext.AgePaises.UpdateRange(agePaisesList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgePaises, AgePaisesDAO>
                .FromEntityToDAOList(agePaisesList, FromEntityToDAO);
        }



        private AgePaisesDAO FromEntityToDAO(AgePaises entityObject)
        {
            AgePaisesDAO DAOobject = null;

            if (entityObject != null)
            {
                DAOobject = new AgePaisesDAO
                {
                    Estado = entityObject.Estado,
                    Codigo = entityObject.Codigo,
                    Descripcion = entityObject.Descripcion,
                };
            }

            return DAOobject;

        }

    }
}
