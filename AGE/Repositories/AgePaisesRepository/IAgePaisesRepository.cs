﻿using AGE.Entities;
using AGE.Entities.DAO.AgePaises;
using AGE.Utils.Paginacion;

namespace AGE.Repositories.AgePaisesRepository
{
    public interface IAgePaisesRepository
    {

        Task<Page<AgePaisesDAO>> ConsultarTodos(
            int codigo,
            string descripcion,
            Pageable pageable); 
        
        Task<AgePaisesDAO> ConsultarPorId(
            int codigo);

        Task<AgePaises> ConsultarCompletePorId(
            int codigo);

        Task<Page<AgePaisesDAO>> ConsultarListaFiltro(
            string filtro,
            Pageable pageable);

        Task<AgePaisesDAO> Insertar(
            AgePaises agePaises);

        Task<AgePaisesDAO> Actualizar(
            AgePaises agePaises);

        Task<List<AgePaisesDAO>> InsertarVarios(
            List<AgePaises> agePaisesList);

        Task<List<AgePaisesDAO>> ActualizarVarios(
            List<AgePaises> agePaisesList);

    }
}
