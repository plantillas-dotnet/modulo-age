﻿using AGE.Entities.DAO.AgeAplicaciones;
using AGE.Entities;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Utils.Paginacion;
using AGE.Utils;
using Microsoft.EntityFrameworkCore;

namespace AGE.Repositories.AgeAplicacionesRepository
{
    public class AgeAplicacionesRepository : IAgeAplicacionesRepository
    {
        private readonly DbContextAge _dbContext;

        public AgeAplicacionesRepository(DbContextAge dbContext)
        {
            _dbContext = dbContext;
        }


        public async Task<Page<AgeAplicacionesDAO>> ConsultarTodos(
            string codigoExterno,
            string descripcion,
            Pageable pageable)
        {
            if (_dbContext.AgeAplicaciones == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgeAplicaciones
                .Where(p => p.Estado != Globales.ESTADO_ANULADO && (codigoExterno == "" || p.CodigoExterno == codigoExterno) &&
                            (descripcion.Equals("") || p.Descripcion.ToLower().Contains(descripcion.ToLower())));

            return await Paginator<AgeAplicaciones, AgeAplicacionesDAO>.Paginar(Query, pageable, FromEntityToDAO);
        }


        public async Task<Page<AgeAplicacionesDAO>> ConsultarListaFiltro(
            string filtro,
            Pageable pageable)
        {
            if (_dbContext.AgeAplicaciones == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgeAplicaciones
                .Where(p => p.Estado != Globales.ESTADO_ANULADO && (
                            p.Codigo.ToString().ToLower().Contains(filtro.ToLower()) ||
                            p.CodigoExterno.ToLower().Contains(filtro.ToLower()) ||
                            p.Descripcion.ToLower().Contains(filtro.ToLower())));

            return await Paginator<AgeAplicaciones, AgeAplicacionesDAO>.Paginar(Query, pageable, FromEntityToDAO);
        }


        public async Task<AgeAplicacionesDAO> ConsultarPorId(int codigo)
        {
            if (_dbContext.AgeAplicaciones == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeAplicaciones? ageAplicaciones = await _dbContext.AgeAplicaciones
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                p.Codigo == codigo).FirstOrDefaultAsync();

            return FromEntityToDAO(ageAplicaciones);
        }


        public async Task<AgeAplicaciones> ConsultarCompletePorId(int codigo)
        {
            if (_dbContext.AgeAplicaciones == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeAplicaciones? ageAplicaciones = await _dbContext.AgeAplicaciones
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                p.Codigo == codigo).FirstOrDefaultAsync();

            return ageAplicaciones;
        }


        public async Task<AgeAplicacionesDAO> Insertar(AgeAplicaciones ageAplicaciones)
        {
            _dbContext.AgeAplicaciones.Add(ageAplicaciones);
            await _dbContext.SaveChangesAsync();

            AgeAplicacionesDAO ageAplicacionesDAO = await ConsultarPorId(ageAplicaciones.Codigo);

            return ageAplicacionesDAO;
        }

        public async Task<AgeAplicacionesDAO> Actualizar(AgeAplicaciones ageAplicaciones)
        {
            _dbContext.Entry(ageAplicaciones).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();

            AgeAplicacionesDAO ageAplicacionesDAO = await ConsultarPorId(ageAplicaciones.Codigo);

            return ageAplicacionesDAO;
        }

        public async Task<List<AgeAplicacionesDAO>> InsertarVarios(List<AgeAplicaciones> ageAplicacionesList)
        {
            await _dbContext.AgeAplicaciones.AddRangeAsync(ageAplicacionesList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeAplicaciones, AgeAplicacionesDAO>
                .FromEntityToDAOList(ageAplicacionesList, FromEntityToDAO);
        }

        public async Task<List<AgeAplicacionesDAO>> ActualizarVarios(List<AgeAplicaciones> ageAplicacionesList)
        {
            _dbContext.AgeAplicaciones.UpdateRange(ageAplicacionesList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeAplicaciones, AgeAplicacionesDAO>
                .FromEntityToDAOList(ageAplicacionesList, FromEntityToDAO);
        }

        private AgeAplicacionesDAO FromEntityToDAO(AgeAplicaciones entityObject)
        {
            AgeAplicacionesDAO DAOobject = null;

            if (entityObject != null)
            {
                DAOobject = new AgeAplicacionesDAO
                {
                    Estado = entityObject.Estado,
                    Codigo = entityObject.Codigo,
                    Descripcion = entityObject.Descripcion,
                    CodigoExterno = entityObject.CodigoExterno,
                    InicioSecuTrxCe = entityObject.InicioSecuTrxCe,
                    IncrementoSecuTrxCe = entityObject.IncrementoSecuTrxCe,
                    ValorActualSecuTrxCe = entityObject.ValorActualSecuTrxCe,
                    CiclicaSecuTrxCe = entityObject.CiclicaSecuTrxCe,
                    InicioSecuTrxCi = entityObject.InicioSecuTrxCi,
                    ValorActualSecuTrxCi = entityObject.ValorActualSecuTrxCi,
                    CiclicaSecuTrxCi = entityObject.CiclicaSecuTrxCi,
                    OrdenInstalacion = entityObject.OrdenInstalacion
                };
            }

            return DAOobject;

        }
    }
}
