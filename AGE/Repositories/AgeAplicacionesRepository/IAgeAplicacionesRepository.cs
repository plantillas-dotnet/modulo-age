﻿using AGE.Entities.DAO.AgeAplicaciones;
using AGE.Entities;
using AGE.Utils.Paginacion;

namespace AGE.Repositories.AgeAplicacionesRepository
{
    public interface IAgeAplicacionesRepository
    {
        Task<Page<AgeAplicacionesDAO>> ConsultarTodos(
            string codigoExterno,
            string descripcion,
            Pageable pageable);

        Task<AgeAplicacionesDAO> ConsultarPorId(
            int codigo);

        Task<AgeAplicaciones> ConsultarCompletePorId(
            int codigo);

        Task<Page<AgeAplicacionesDAO>> ConsultarListaFiltro(
            string filtro,
            Pageable pageable);

        Task<AgeAplicacionesDAO> Insertar(
            AgeAplicaciones ageAplicaciones);

        Task<AgeAplicacionesDAO> Actualizar(
            AgeAplicaciones ageAplicaciones);

        Task<List<AgeAplicacionesDAO>> InsertarVarios(
            List<AgeAplicaciones> ageAplicacionesList);

        Task<List<AgeAplicacionesDAO>> ActualizarVarios(
            List<AgeAplicaciones> ageAplicacionesList);
    }
}
