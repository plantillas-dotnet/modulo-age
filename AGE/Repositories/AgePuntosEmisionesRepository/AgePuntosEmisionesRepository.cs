﻿using AGE.Entities;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Utils.Paginacion;
using AGE.Utils;
using Microsoft.EntityFrameworkCore;
using AGE.Entities.DAO.AgePuntosEmision;
using AGE.Repositories.AgePuntosEmisionRepository;

namespace AGE.Repositories.AgePuntosEmisionRepository
{
    public class AgePuntosEmisionesRepository : IAgePuntosEmisionesRepository
    {
        private readonly DbContextAge _dbContext;

        public AgePuntosEmisionesRepository(DbContextAge dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Page<AgePuntosEmisionesDAO>> ConsultarTodos(
            int codigoLicenciatario,
            int? codigo,
            int? ageSucursCodigo,
            Pageable pageable)
        {
            if (_dbContext.AgePuntosEmisions == null)
            {
                throw new RegisterNotFoundException();
            }

            var query = _dbContext.AgePuntosEmisions
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeSucursAgeLicencCodigo == codigoLicenciatario &&
                            (codigo == 0 || p.Codigo == codigo) &&
                            (ageSucursCodigo == 0 || p.AgeSucursCodigo == ageSucursCodigo));

            return await Paginator<AgePuntosEmisiones, AgePuntosEmisionesDAO>.Paginar(query, pageable, FromEntityToDAO);
        }

        public async Task<AgePuntosEmisionesDAO> ConsultarPorId(AgePuntosEmisionesPKDAO agePuntosEmisionPKDAO)
        {
            if (_dbContext.AgePuntosEmisions == null)
            {
                throw new RegisterNotFoundException();
            }

            AgePuntosEmisiones? agePuntosEmision = await _dbContext.AgePuntosEmisions
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeSucursAgeLicencCodigo == agePuntosEmisionPKDAO.ageSucursAgeLicencCodigo &&
                            p.Codigo == agePuntosEmisionPKDAO.codigo &&
                            p.AgeSucursCodigo == agePuntosEmisionPKDAO.ageSucursCodigo).FirstOrDefaultAsync();

            return FromEntityToDAO(agePuntosEmision);
        }

        public async Task<AgePuntosEmisiones> ConsultarCompletePorId(AgePuntosEmisionesPKDAO agePuntosEmisionPKDAO)
        {
            if (_dbContext.AgeTiposLocalidades == null)
            {
                throw new RegisterNotFoundException();
            }

            AgePuntosEmisiones? agePuntosEmision = await _dbContext.AgePuntosEmisions
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeSucursAgeLicencCodigo == agePuntosEmisionPKDAO.ageSucursAgeLicencCodigo &&
                            p.Codigo == agePuntosEmisionPKDAO.codigo &&
                            p.AgeSucursCodigo == agePuntosEmisionPKDAO.ageSucursCodigo).FirstOrDefaultAsync();

            return agePuntosEmision;
        }

        public async Task<Page<AgePuntosEmisionesDAO>> ConsultarListaFiltro(
            int codigoLicenciatario,
            string filtro,
            Pageable pageable)
        {
            if (_dbContext.AgePuntosEmisions == null)
            {
                throw new RegisterNotFoundException();
            }

            var query = _dbContext.AgePuntosEmisions
                 .Where(p => p.Estado != Globales.ESTADO_ANULADO && (
                            p.AgeSucursAgeLicencCodigo == codigoLicenciatario &&
                            p.Codigo.ToString().ToLower().Contains(filtro) ||
                            p.AgeSucursCodigo.ToString().ToLower().Contains(filtro) ||
                            p.Descripcion.ToLower().Contains(filtro)));

            return await Paginator<AgePuntosEmisiones, AgePuntosEmisionesDAO>.Paginar(query, pageable, FromEntityToDAO);
        }

        public async Task<AgePuntosEmisionesDAO> Insertar(AgePuntosEmisiones agePuntosEmision)
        {
            _dbContext.AgePuntosEmisions.Add(agePuntosEmision);
            await _dbContext.SaveChangesAsync();

            AgePuntosEmisionesDAO agePuntosEmisionDAO = await ConsultarPorId(new AgePuntosEmisionesPKDAO
            {
                ageSucursAgeLicencCodigo = agePuntosEmision.AgeSucursAgeLicencCodigo,
                codigo = agePuntosEmision.Codigo,
                ageSucursCodigo = agePuntosEmision.AgeSucursCodigo
            });

            return agePuntosEmisionDAO;
        }

        public async Task<List<AgePuntosEmisionesDAO>> InsertarVarios(List<AgePuntosEmisiones> agePuntosEmisionList)
        {
            await _dbContext.AgePuntosEmisions.AddRangeAsync(agePuntosEmisionList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgePuntosEmisiones, AgePuntosEmisionesDAO>
                .FromEntityToDAOList(agePuntosEmisionList, FromEntityToDAO);
        }

        public async Task<AgePuntosEmisionesDAO> Actualizar(AgePuntosEmisiones agePuntosEmision)
        {
            _dbContext.Entry(agePuntosEmision).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();

            AgePuntosEmisionesDAO agePuntosEmisionDAO = await ConsultarPorId(new AgePuntosEmisionesPKDAO
            {
                ageSucursAgeLicencCodigo = agePuntosEmision.AgeSucursAgeLicencCodigo,
                codigo = agePuntosEmision.Codigo,
                ageSucursCodigo = agePuntosEmision.AgeSucursCodigo
            });

            return agePuntosEmisionDAO;
        }

        public async Task<List<AgePuntosEmisionesDAO>> ActualizarVarios(List<AgePuntosEmisiones> agePuntosEmisionList)
        {
            _dbContext.AgePuntosEmisions.UpdateRange(agePuntosEmisionList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgePuntosEmisiones, AgePuntosEmisionesDAO>
                .FromEntityToDAOList(agePuntosEmisionList, FromEntityToDAO);
        }

        private static AgePuntosEmisionesDAO FromEntityToDAO(AgePuntosEmisiones entityObject)
        {
            if (entityObject == null)
            {
                return null;
            }

            return new AgePuntosEmisionesDAO
            {
                Id = new AgePuntosEmisionesPKDAO
                {
                    ageSucursAgeLicencCodigo = entityObject.AgeSucursAgeLicencCodigo,
                    ageSucursCodigo = entityObject.AgeSucursCodigo,
                    codigo = entityObject.Codigo
                },
                Descripcion = entityObject.Descripcion,
                Estado = entityObject.Estado
            };
        }
    }
}
