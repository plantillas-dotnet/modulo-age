﻿using AGE.Entities.DAO.AgePuntosEmision;
using AGE.Entities;
using AGE.Utils.Paginacion;

namespace AGE.Repositories.AgePuntosEmisionRepository
{
    public interface IAgePuntosEmisionesRepository
    {
        Task<Page<AgePuntosEmisionesDAO>> ConsultarTodos(
            int codigoLicenciatario,
            int? codigo,
            int? ageSucursCodigo,
            Pageable pageable);

        Task<Page<AgePuntosEmisionesDAO>> ConsultarListaFiltro(
            int codigoLicenciatario,
            string filtro,
            Pageable pageable);

        Task<AgePuntosEmisionesDAO> ConsultarPorId(
            AgePuntosEmisionesPKDAO AgePuntosEmisionPKDAO);

        Task<AgePuntosEmisiones> ConsultarCompletePorId(
            AgePuntosEmisionesPKDAO AgePuntosEmisionPKDAO);

        Task<AgePuntosEmisionesDAO> Insertar(
            AgePuntosEmisiones AgePuntosEmision);

        Task<List<AgePuntosEmisionesDAO>> InsertarVarios(
            List<AgePuntosEmisiones> AgePuntosEmisionList);

        Task<AgePuntosEmisionesDAO> Actualizar(AgePuntosEmisiones AgePuntosEmision);

        Task<List<AgePuntosEmisionesDAO>> ActualizarVarios(
            List<AgePuntosEmisiones> AgePuntosEmisionList);
    }
}
