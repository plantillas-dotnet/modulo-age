﻿using AGE.Entities;
using AGE.Entities.DAO.AgeMonedas;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Utils;
using AGE.Utils.Paginacion;
using Microsoft.EntityFrameworkCore;

namespace AGE.Repositories.AgeMonedasRepository
{
    public class AgeMonedasRepository : IAgeMonedasRepository
    {

        private readonly DbContextAge _dbContext;

        public AgeMonedasRepository(DbContextAge dbContext)
        {
            _dbContext = dbContext;
        }


        public async Task<Page<AgeMonedasDAO>> ConsultarTodos(
            int codigo,
            string descripcion,
            string codigoExterno,
            string monedaSeparadorDecimal,
            string monedaSimbolo,
            Pageable pageable)
        {
            if (_dbContext.AgeMonedas == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgeMonedas
                .Where(p => p.Estado != Globales.ESTADO_ANULADO && (codigo == 0 || p.Codigo == codigo) &&
                            (
                                descripcion.Equals("") || 
                                p.Descripcion.ToLower().Contains(descripcion.ToLower()) ||
                                p.CodigoExterno.ToLower().Contains(codigoExterno.ToLower()) ||
                                p.MonedaSeparadorDecimal.ToLower().Contains(monedaSeparadorDecimal.ToLower()) ||
                                p.MonedaSimbolo.ToLower().Contains(monedaSimbolo.ToLower())
                       )
                       );

            return await Paginator<AgeMonedas, AgeMonedasDAO>.Paginar(Query, pageable, FromEntityToDAO);
        }


        public async Task<Page<AgeMonedasDAO>> ConsultarListaFiltro(
            string filtro,
            Pageable pageable)
        {
            if (_dbContext.AgeMonedas == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgeMonedas
                .Where(p => p.Estado != Globales.ESTADO_ANULADO && (
                            p.Codigo.ToString().ToLower().Contains(filtro) ||
                            p.Descripcion.ToLower().Contains(filtro)));

            return await Paginator<AgeMonedas, AgeMonedasDAO>.Paginar(Query, pageable, FromEntityToDAO);
        }


        public async Task<AgeMonedasDAO> ConsultarPorId(int codigo)
        {
            if (_dbContext.AgeMonedas == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeMonedas? ageMonedas = await _dbContext.AgeMonedas
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                p.Codigo == codigo).FirstOrDefaultAsync();

            return FromEntityToDAO(ageMonedas);
        }


        public async Task<AgeMonedas> ConsultarCompletePorId(int codigo)
        {
            if (_dbContext.AgeMonedas == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeMonedas? ageMonedas = await _dbContext.AgeMonedas
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                p.Codigo == codigo).FirstOrDefaultAsync();

            return ageMonedas;
        }


        public async Task<AgeMonedasDAO> Insertar(AgeMonedas ageMonedas)
        {
            _dbContext.AgeMonedas.Add(ageMonedas);
            await _dbContext.SaveChangesAsync();

            AgeMonedasDAO ageMonedasDAO = await ConsultarPorId(ageMonedas.Codigo);

            return ageMonedasDAO;
        }

        public async Task<AgeMonedasDAO> Actualizar(AgeMonedas ageMonedas)
        {
            _dbContext.Entry(ageMonedas).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();

            AgeMonedasDAO ageMonedasDAO = await ConsultarPorId(ageMonedas.Codigo);

            return ageMonedasDAO;
        }

        public async Task<List<AgeMonedasDAO>> InsertarVarios(List<AgeMonedas> ageMonedasList)
        {
            await _dbContext.AgeMonedas.AddRangeAsync(ageMonedasList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeMonedas, AgeMonedasDAO>
                .FromEntityToDAOList(ageMonedasList, FromEntityToDAO);
        }

        public async Task<List<AgeMonedasDAO>> ActualizarVarios(List<AgeMonedas> ageMonedasList)
        {
            _dbContext.AgeMonedas.UpdateRange(ageMonedasList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeMonedas, AgeMonedasDAO>
                .FromEntityToDAOList(ageMonedasList, FromEntityToDAO);
        }



        private AgeMonedasDAO FromEntityToDAO(AgeMonedas entityObject)
        {
            AgeMonedasDAO DAOobject = null;

            if (entityObject != null)
            {
                DAOobject = new AgeMonedasDAO
                {
                    Estado = entityObject.Estado,
                    Codigo = entityObject.Codigo,
                    Descripcion = entityObject.Descripcion,
                    CodigoExterno = entityObject.CodigoExterno,
                    MonedaSimbolo = entityObject.MonedaSimbolo,
                    MonedaSeparadorDecimal = entityObject.MonedaSeparadorDecimal
                };
            }

            return DAOobject;

        }
    }
}
