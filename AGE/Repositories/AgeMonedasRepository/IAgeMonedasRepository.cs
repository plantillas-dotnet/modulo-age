﻿using AGE.Entities;
using AGE.Entities.DAO.AgeMonedas;
using AGE.Utils.Paginacion;

namespace AGE.Repositories.AgeMonedasRepository
{
    public interface IAgeMonedasRepository
    {
        Task<Page<AgeMonedasDAO>> ConsultarTodos(
            int codigo,
            string descripcion,
            string codigoExterno,
            string monedaSeparadorDecimal,
            string monedaSimbolo,
            Pageable pageable);
        Task<Page<AgeMonedasDAO>> ConsultarListaFiltro(
            string filtro,
            Pageable pageable);
        Task<AgeMonedasDAO> ConsultarPorId(int codigo);
        Task<AgeMonedas> ConsultarCompletePorId(int codigo);
        Task<AgeMonedasDAO> Insertar(AgeMonedas ageMonedas);
        Task<AgeMonedasDAO> Actualizar(AgeMonedas ageMonedas);
        Task<List<AgeMonedasDAO>> InsertarVarios(List<AgeMonedas> ageMonedasList);
        Task<List<AgeMonedasDAO>> ActualizarVarios(List<AgeMonedas> ageMonedasList);
    }
}
