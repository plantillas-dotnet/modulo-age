﻿using AGE.Entities.DAO.AgeDiasFeriados;
using AGE.Entities;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Utils.Paginacion;
using AGE.Utils;
using Microsoft.EntityFrameworkCore;

namespace AGE.Repositories.AgeDiasFeriadosRepository
{
    public class AgeDiasFeriadosRepository : IAgeDiasFeriadosRepository
    {
        private readonly DbContextAge _dbContext;

        public AgeDiasFeriadosRepository(DbContextAge dbContext)
        {
            _dbContext = dbContext;
        }


        public async Task<Page<AgeDiasFeriadosDAO>> ConsultarTodos(string descripcion,
                                                                   Pageable pageable)
        {
            if (_dbContext.AgeDiasFeriados == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgeDiasFeriados
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                      (descripcion.Equals("") || p.Descripcion.ToLower().Contains(descripcion.ToLower())));

            return await Paginator<AgeDiasFeriados, AgeDiasFeriadosDAO>.Paginar(Query, pageable, FromEntityToDAO);
        }


        public async Task<Page<AgeDiasFeriadosDAO>> ConsultarListaFiltro(
            string filtro,
            Pageable pageable)
        {
            if (_dbContext.AgeDiasFeriados == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgeDiasFeriados
                .Where(p => p.Estado != Globales.ESTADO_ANULADO && (
                            p.Codigo.ToString().ToLower().Contains(filtro.ToLower()) ||
                            p.Descripcion.ToLower().Contains(filtro.ToLower())));

            return await Paginator<AgeDiasFeriados, AgeDiasFeriadosDAO>.Paginar(Query, pageable, FromEntityToDAO);
        }


        public async Task<AgeDiasFeriadosDAO> ConsultarPorId(int codigo)
        {
            if (_dbContext.AgeDiasFeriados == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeDiasFeriados? ageDiasFeriados = await _dbContext.AgeDiasFeriados
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                p.Codigo == codigo).FirstOrDefaultAsync();

            return FromEntityToDAO(ageDiasFeriados);
        }


        public async Task<AgeDiasFeriados> ConsultarCompletePorId(int codigo)
        {
            if (_dbContext.AgeDiasFeriados == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeDiasFeriados? ageDiasFeriados = await _dbContext.AgeDiasFeriados
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                p.Codigo == codigo).FirstOrDefaultAsync();

            return ageDiasFeriados;
        }

        public async Task<AgeDiasFeriadosDAO> Insertar(AgeDiasFeriados ageDiasFeriados)
        {
            _dbContext.AgeDiasFeriados.Add(ageDiasFeriados);
            await _dbContext.SaveChangesAsync();

            AgeDiasFeriadosDAO ageDiasFeriadosDAO = await ConsultarPorId(ageDiasFeriados.Codigo);

            return ageDiasFeriadosDAO;
        }

        public async Task<AgeDiasFeriadosDAO> Actualizar(AgeDiasFeriados ageDiasFeriados)
        {
            _dbContext.Entry(ageDiasFeriados).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();

            AgeDiasFeriadosDAO ageDiasFeriadosDAO = await ConsultarPorId(ageDiasFeriados.Codigo);

            return ageDiasFeriadosDAO;
        }

        public async Task<List<AgeDiasFeriadosDAO>> InsertarVarios(List<AgeDiasFeriados> ageDiasFeriadosList)
        {
            await _dbContext.AgeDiasFeriados.AddRangeAsync(ageDiasFeriadosList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeDiasFeriados, AgeDiasFeriadosDAO>
                .FromEntityToDAOList(ageDiasFeriadosList, FromEntityToDAO);
        }

        public async Task<List<AgeDiasFeriadosDAO>> ActualizarVarios(List<AgeDiasFeriados> ageDiasFeriadosList)
        {
            _dbContext.AgeDiasFeriados.UpdateRange(ageDiasFeriadosList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeDiasFeriados, AgeDiasFeriadosDAO>
                .FromEntityToDAOList(ageDiasFeriadosList, FromEntityToDAO);
        }

        private AgeDiasFeriadosDAO FromEntityToDAO(AgeDiasFeriados entityObject)
        {
            AgeDiasFeriadosDAO DAOobject = null;

            if (entityObject != null)
            {
                DAOobject = new AgeDiasFeriadosDAO
                {
                    Estado = entityObject.Estado,
                    Codigo = entityObject.Codigo,
                    Descripcion = entityObject.Descripcion,
                    FechaDesde = entityObject.FechaDesde,
                    FechaHasta = entityObject.FechaHasta,
                    HoraDesde = entityObject.HoraDesde,
                    HoraHasta = entityObject.HoraHasta
                };
            }

            return DAOobject;

        }
    }
}
