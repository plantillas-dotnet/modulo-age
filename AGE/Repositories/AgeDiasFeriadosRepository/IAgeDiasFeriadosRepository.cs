﻿using AGE.Entities.DAO.AgeDiasFeriados;
using AGE.Entities;
using AGE.Utils.Paginacion;

namespace AGE.Repositories.AgeDiasFeriadosRepository
{
    public interface IAgeDiasFeriadosRepository
    {
        Task<Page<AgeDiasFeriadosDAO>> ConsultarTodos(
            string descripcion,
            Pageable pageable);

        Task<AgeDiasFeriadosDAO> ConsultarPorId(
            int codigo);

        Task<AgeDiasFeriados> ConsultarCompletePorId(
            int codigo);

        Task<Page<AgeDiasFeriadosDAO>> ConsultarListaFiltro(
            string filtro,
            Pageable pageable);

        Task<AgeDiasFeriadosDAO> Insertar(
            AgeDiasFeriados ageDiasFeriados);

        Task<AgeDiasFeriadosDAO> Actualizar(
            AgeDiasFeriados ageDiasFeriados);

        Task<List<AgeDiasFeriadosDAO>> InsertarVarios(
            List<AgeDiasFeriados> ageDiasFeriadosList);

        Task<List<AgeDiasFeriadosDAO>> ActualizarVarios(
            List<AgeDiasFeriados> ageDiasFeriadosList);
    }
}
