﻿using AGE.Entities;
using AGE.Entities.DAO.AgeTransaccionesIdioma;
using AGE.Utils.Paginacion;

namespace AGE.Repositories.AgeTransaccionesIdiomaRepository
{
    public interface IAgeTransaccionesIdiomasRepository
    {
        Task<Page<AgeTransaccionesIdiomasDAO>> ConsultarTodos(
            Pageable pageable);

        Task<Page<AgeTransaccionesIdiomasDAO>> ConsultarListaFiltro(
            string filtro, 
            Pageable pageable);

        Task<AgeTransaccionesIdiomasDAO> ConsultarPorId(
            AgeTransaccionesIdiomasPKDAO ageTransaccionesIdiomaPKDAO);

        Task<AgeTransaccionesIdiomas> ConsultarCompletePorId(
            AgeTransaccionesIdiomasPKDAO ageTransaccionesIdiomaPKDAO);

        Task<AgeTransaccionesIdiomasDAO> Insertar(
            AgeTransaccionesIdiomas ageTransaccionesIdioma);

        Task<AgeTransaccionesIdiomasDAO> Actualizar(
            AgeTransaccionesIdiomas ageTransaccionesIdioma);

        Task<List<AgeTransaccionesIdiomasDAO>> InsertarVarios(
            List<AgeTransaccionesIdiomas> ageTransaccionesIdiomaList);

        Task<List<AgeTransaccionesIdiomasDAO>> ActualizarVarios(
            List<AgeTransaccionesIdiomas> ageTransaccionesIdiomaList);
    }
}
