﻿using AGE.Entities;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Utils.Paginacion;
using AGE.Utils;
using Microsoft.EntityFrameworkCore;
using AGE.Entities.DAO.AgeTransaccionesIdioma;

namespace AGE.Repositories.AgeTransaccionesIdiomaRepository
{
    public class AgeTransaccionesIdiomasRepository : IAgeTransaccionesIdiomasRepository
    {
        private readonly DbContextAge _dbContext;

        public AgeTransaccionesIdiomasRepository(DbContextAge dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Page<AgeTransaccionesIdiomasDAO>> ConsultarTodos(
            Pageable pageable)
        {
            if (_dbContext.AgeTransaccionesIdiomas == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgeTransaccionesIdiomas
                .Where(p => p.Estado != Globales.ESTADO_ANULADO);

            return await Paginator<AgeTransaccionesIdiomas, AgeTransaccionesIdiomasDAO>.Paginar(Query, pageable, FromEntityToDAO);
        }


        public async Task<Page<AgeTransaccionesIdiomasDAO>> ConsultarListaFiltro(
            string filtro,
            Pageable pageable)
        {
            if (_dbContext.AgeTransaccionesIdiomas == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgeTransaccionesIdiomas
                .Where(p => p.Estado != Globales.ESTADO_ANULADO && (
                            p.AgeIdiomaCodigo.ToString().ToLower().Contains(filtro) ||
                            p.AgeTransaAgeAplicaCodigo.ToString().ToLower().Contains(filtro) ||
                            p.AgeTransaCodigo.ToString().ToLower().Contains(filtro) ||
                            p.Descripcion.ToLower().Contains(filtro)));

            return await Paginator<AgeTransaccionesIdiomas, AgeTransaccionesIdiomasDAO>.Paginar(Query, pageable, FromEntityToDAO);
        }


        public async Task<AgeTransaccionesIdiomasDAO> ConsultarPorId(AgeTransaccionesIdiomasPKDAO ageTransaccionesIdiomaPKDAO)
        {
            if (_dbContext.AgeTransaccionesIdiomas == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeTransaccionesIdiomas? ageTransaccionesIdioma = await _dbContext.AgeTransaccionesIdiomas
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeTransaCodigo == ageTransaccionesIdiomaPKDAO.ageTransaCodigo &&
                            p.AgeTransaAgeAplicaCodigo == ageTransaccionesIdiomaPKDAO.ageTransaAgeAplicaCodigo &&
                            p.AgeIdiomaCodigo == ageTransaccionesIdiomaPKDAO.ageIdiomaCodigo).FirstOrDefaultAsync();

            return FromEntityToDAO(ageTransaccionesIdioma);
        }


        public async Task<AgeTransaccionesIdiomas> ConsultarCompletePorId(AgeTransaccionesIdiomasPKDAO ageTransaccionesIdiomaPKDAO)
        {
            if (_dbContext.AgeTransaccionesIdiomas == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeTransaccionesIdiomas? ageTransaccionesIdioma = await _dbContext.AgeTransaccionesIdiomas
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeTransaCodigo == ageTransaccionesIdiomaPKDAO.ageTransaCodigo &&
                            p.AgeTransaAgeAplicaCodigo == ageTransaccionesIdiomaPKDAO.ageTransaAgeAplicaCodigo &&
                            p.AgeIdiomaCodigo == ageTransaccionesIdiomaPKDAO.ageIdiomaCodigo).FirstOrDefaultAsync();

            return ageTransaccionesIdioma;
        }


        public async Task<AgeTransaccionesIdiomasDAO> Insertar(AgeTransaccionesIdiomas ageTransaccionesIdioma)
        {
            _dbContext.AgeTransaccionesIdiomas.Add(ageTransaccionesIdioma);
            await _dbContext.SaveChangesAsync();

            AgeTransaccionesIdiomasDAO ageTransaccionesIdiomaDAO = await ConsultarPorId(
                new AgeTransaccionesIdiomasPKDAO
                {
                    ageTransaAgeAplicaCodigo = ageTransaccionesIdioma.AgeTransaAgeAplicaCodigo,
                    ageTransaCodigo = ageTransaccionesIdioma.AgeTransaCodigo,
                    ageIdiomaCodigo = ageTransaccionesIdioma.AgeIdiomaCodigo
                });

            return ageTransaccionesIdiomaDAO;
        }

        public async Task<AgeTransaccionesIdiomasDAO> Actualizar(AgeTransaccionesIdiomas ageTransaccionesIdioma)
        {
            _dbContext.Entry(ageTransaccionesIdioma).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();

            AgeTransaccionesIdiomasDAO ageTransaccionesIdiomaDAO = await ConsultarPorId(
                new AgeTransaccionesIdiomasPKDAO
            {
                ageTransaAgeAplicaCodigo = ageTransaccionesIdioma.AgeTransaAgeAplicaCodigo,
                ageTransaCodigo = ageTransaccionesIdioma.AgeTransaCodigo,
                ageIdiomaCodigo = ageTransaccionesIdioma.AgeIdiomaCodigo
            });

            return ageTransaccionesIdiomaDAO;
        }

        public async Task<List<AgeTransaccionesIdiomasDAO>> InsertarVarios(
            List<AgeTransaccionesIdiomas> ageTransaccionesIdiomaList)
        {
            await _dbContext.AgeTransaccionesIdiomas.AddRangeAsync(ageTransaccionesIdiomaList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeTransaccionesIdiomas, AgeTransaccionesIdiomasDAO>
                .FromEntityToDAOList(ageTransaccionesIdiomaList, FromEntityToDAO);
        }


        public async Task<List<AgeTransaccionesIdiomasDAO>> ActualizarVarios(List<AgeTransaccionesIdiomas> ageTransaccionesIdiomaList)
        {
            _dbContext.AgeTransaccionesIdiomas.UpdateRange(ageTransaccionesIdiomaList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeTransaccionesIdiomas, AgeTransaccionesIdiomasDAO>
                .FromEntityToDAOList(ageTransaccionesIdiomaList, FromEntityToDAO);
        }

        private static AgeTransaccionesIdiomasDAO FromEntityToDAO(AgeTransaccionesIdiomas entityObject)
        {
            if (entityObject == null)
            {
                return null;
            }

            return new AgeTransaccionesIdiomasDAO
            {
                Id = new AgeTransaccionesIdiomasPKDAO
                {
                    ageTransaAgeAplicaCodigo = entityObject.AgeTransaAgeAplicaCodigo,
                    ageTransaCodigo = entityObject.AgeTransaCodigo,
                    ageIdiomaCodigo = entityObject.AgeIdiomaCodigo
                },
                Descripcion = entityObject.Descripcion,
                Estado = entityObject.Estado
            };
        }

    }
}

