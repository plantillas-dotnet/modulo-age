﻿using AGE.Entities.DAO.AgeFeriadosExcepciones;
using AGE.Entities;
using AGE.Utils.Paginacion;

namespace AGE.Repositories.AgeFeriadosExcepcionesRepository
{
    public interface IAgeFeriadosExcepcionesRepository
    {
        Task<Page<AgeFeriadosExcepcionesDAO>> ConsultarTodos(
            int codigoLicenciatario,
            string? tipoExcepcion,
            Pageable pageable);

        Task<AgeFeriadosExcepcionesDAO> ConsultarPorId(AgeFeriadosExcepcionesPKDAO ageFeriadosExcepcionesPKDAO);

        Task<Page<AgeFeriadosExcepcionesDAO>> ConsultarListaFiltro(
            int codigoLicenciatario,
            string filtro,
            Pageable pageable);

        Task<AgeFeriadosExcepciones> ConsultarCompletePorId(
            AgeFeriadosExcepcionesPKDAO ageFeriadosExcepcionesPKDAO);

        Task<AgeFeriadosExcepcionesDAO> Insertar(AgeFeriadosExcepciones AgeFeriadosExcepciones);

        Task<List<AgeFeriadosExcepcionesDAO>> InsertarVarios(
            List<AgeFeriadosExcepciones> AgeFeriadosExcepcionesList);

        Task<AgeFeriadosExcepcionesDAO> Actualizar(AgeFeriadosExcepciones AgeFeriadosExcepciones);

        Task<List<AgeFeriadosExcepcionesDAO>> ActualizarVarios(
            List<AgeFeriadosExcepciones> AgeFeriadosExcepcionesList);
    }
}
