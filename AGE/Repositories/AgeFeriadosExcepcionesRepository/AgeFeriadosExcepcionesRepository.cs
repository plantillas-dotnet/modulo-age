﻿using AGE.Entities.DAO.AgeFeriadosExcepciones;
using AGE.Entities;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Utils.Paginacion;
using AGE.Utils;
using Microsoft.EntityFrameworkCore;

namespace AGE.Repositories.AgeFeriadosExcepcionesRepository
{
    public class AgeFeriadosExcepcionesRepository : IAgeFeriadosExcepcionesRepository
    {
        private readonly DbContextAge _dbContext;

        public AgeFeriadosExcepcionesRepository(DbContextAge dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Page<AgeFeriadosExcepcionesDAO>> ConsultarTodos(int codigoLicenciatario,
                                                                        string? tipoExcepcion,
                                                                        Pageable pageable)
        {
            if (_dbContext.AgeFeriadosExcepciones == null)
            {
                throw new RegisterNotFoundException();
            }

            var query = _dbContext.AgeFeriadosExcepciones
            .Where(a => a.Estado != Globales.ESTADO_ANULADO &&
                (a.AgeLicencCodigo == codigoLicenciatario) &&
                (tipoExcepcion == null || tipoExcepcion == "" || a.TipoExcepcion.ToLower().Contains(tipoExcepcion.ToLower())));

            return await Paginator<AgeFeriadosExcepciones, AgeFeriadosExcepcionesDAO>.Paginar(query, pageable, FromEntityToDAO);

        }

        public async Task<AgeFeriadosExcepcionesDAO> ConsultarPorId(AgeFeriadosExcepcionesPKDAO ageFeriadosExcepcionesPKDAO)
        {
            if (_dbContext.AgeFeriadosExcepciones == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeFeriadosExcepciones? ageFeriadosExcepcione = await _dbContext.AgeFeriadosExcepciones
                            .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeLicencCodigo == ageFeriadosExcepcionesPKDAO.AgeLicencCodigo &&
                            p.Codigo == ageFeriadosExcepcionesPKDAO.Codigo).FirstOrDefaultAsync();

            return FromEntityToDAO(ageFeriadosExcepcione);
        }

        public async Task<Page<AgeFeriadosExcepcionesDAO>> ConsultarListaFiltro(int codigoLicenciatario, string filtro, Pageable pageable)
        {
            if (_dbContext.AgeFeriadosExcepciones == null)
            {
                throw new RegisterNotFoundException();
            }

            var query = _dbContext.AgeFeriadosExcepciones
                .Where(a => a.Estado != Globales.ESTADO_ANULADO &&
                            a.AgeLicencCodigo == codigoLicenciatario &&
                            (a.TipoExcepcion.ToUpper().Contains(filtro.ToUpper()) ||
                            (a.Observacion != null && a.Observacion.ToUpper().Contains(filtro.ToUpper())) ||
                            a.Codigo.ToString().ToUpper().Contains(filtro.ToUpper())));

            return await Paginator<AgeFeriadosExcepciones, AgeFeriadosExcepcionesDAO>.Paginar(query, pageable, FromEntityToDAO);
        }

        public async Task<AgeFeriadosExcepcionesDAO> Insertar(AgeFeriadosExcepciones ageFeriadosExcepcione)
        {
            _dbContext.AgeFeriadosExcepciones.Add(ageFeriadosExcepcione);
            await _dbContext.SaveChangesAsync();

            AgeFeriadosExcepcionesDAO ageAgeFeriadosExcepcionesDAO = await ConsultarPorId(new AgeFeriadosExcepcionesPKDAO
            {
                AgeLicencCodigo = ageFeriadosExcepcione.AgeLicencCodigo,
                Codigo = ageFeriadosExcepcione.Codigo
            });

            return ageAgeFeriadosExcepcionesDAO;
        }

        public async Task<List<AgeFeriadosExcepcionesDAO>> InsertarVarios(List<AgeFeriadosExcepciones> ageFeriadosExcepcionesList)
        {
            _dbContext.AgeFeriadosExcepciones.AddRange(ageFeriadosExcepcionesList);
            _dbContext.SaveChanges();

            return ConversorEntityToDAOList<AgeFeriadosExcepciones, AgeFeriadosExcepcionesDAO>
                .FromEntityToDAOList(ageFeriadosExcepcionesList, FromEntityToDAO);
        }

        public async Task<AgeFeriadosExcepciones> ConsultarCompletePorId(AgeFeriadosExcepcionesPKDAO ageFeriadosExcepcionesPKDAO)
        {
            if (_dbContext.AgeFeriadosExcepciones == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeFeriadosExcepciones? ageFeriadosExcepcione = await _dbContext.AgeFeriadosExcepciones
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeLicencCodigo == ageFeriadosExcepcionesPKDAO.AgeLicencCodigo &&
                            p.Codigo == ageFeriadosExcepcionesPKDAO.Codigo)
                .FirstOrDefaultAsync();

            return ageFeriadosExcepcione;
        }

        public async Task<AgeFeriadosExcepcionesDAO> Actualizar(AgeFeriadosExcepciones AgeFeriadosExcepcione)
        {
            _dbContext.Entry(AgeFeriadosExcepcione).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();

            AgeFeriadosExcepcionesDAO ageAgeFeriadosExcepcionesDAO = await ConsultarPorId(new AgeFeriadosExcepcionesPKDAO
            {
                AgeLicencCodigo = AgeFeriadosExcepcione.AgeLicencCodigo,
                Codigo = AgeFeriadosExcepcione.Codigo
            });

            return ageAgeFeriadosExcepcionesDAO;
        }

        public async Task<List<AgeFeriadosExcepcionesDAO>> ActualizarVarios(List<AgeFeriadosExcepciones> AgeFeriadosExcepcionesList)
        {
            _dbContext.AgeFeriadosExcepciones.UpdateRange(AgeFeriadosExcepcionesList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeFeriadosExcepciones, AgeFeriadosExcepcionesDAO>
                .FromEntityToDAOList(AgeFeriadosExcepcionesList, FromEntityToDAO);
        }

        private static AgeFeriadosExcepcionesDAO? FromEntityToDAO(AgeFeriadosExcepciones ageFeriadosExcepciones)
        {
            if (ageFeriadosExcepciones == null)
            {
                return null;
            }

            return new AgeFeriadosExcepcionesDAO
            {

                Id = new AgeFeriadosExcepcionesPKDAO
                {
                    AgeLicencCodigo = ageFeriadosExcepciones.AgeLicencCodigo,
                    Codigo = ageFeriadosExcepciones.Codigo
                },
                Estado = ageFeriadosExcepciones.Estado,
                TipoExcepcion = ageFeriadosExcepciones.TipoExcepcion,
                Observacion = ageFeriadosExcepciones.Observacion,
                FechaDesde = ageFeriadosExcepciones.FechaDesde,
                FechaHasta = ageFeriadosExcepciones.FechaHasta,
                HoraDesde = ageFeriadosExcepciones.HoraDesde,
                HoraHasta = ageFeriadosExcepciones.HoraHasta
            };
        }
    }
}
