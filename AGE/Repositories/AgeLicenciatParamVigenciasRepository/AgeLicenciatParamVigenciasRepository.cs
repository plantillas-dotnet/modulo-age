﻿using AGE.Entities.DAO.AgeLicenciatParamVigencia;
using AGE.Entities;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Utils.Paginacion;
using AGE.Utils;
using Microsoft.EntityFrameworkCore;
namespace AGE.Repositories.AgeLicenciatParamVigenciaRepository
{
    public class AgeLicenciatParamVigenciasRepository : IAgeLicenciatParamVigenciasRepository
    {
        private readonly DbContextAge _dbContext;

        public AgeLicenciatParamVigenciasRepository(DbContextAge dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Page<AgeLicenciatParamVigenciasDAO>> ConsultarTodos(
            int codigoLicenciatario,
            int? codigoParametro,
            int? id,
            string? observacion,
            string? valorParametro,
            Pageable pageable)
        {
            if (_dbContext.AgeLicenciatParamVigencias == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgeLicenciatParamVigencias
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeLicencCodigo == codigoLicenciatario && 
                            (codigoParametro == 0 || p.AgeParGeCodigo == codigoParametro) &&
                            (id == 0 || p.Codigo == id) &&
                            (observacion == "" || p.Observacion.ToLower().Equals(observacion.ToLower())) &&
                            (valorParametro == "" || p.ValorParametro.ToLower().Equals(valorParametro.ToLower())));

            return await Paginator<AgeLicenciatParamVigencias, AgeLicenciatParamVigenciasDAO>.Paginar(Query, pageable, FromEntityToDAO);
        }

        public async Task<Page<AgeLicenciatParamVigenciasDAO>> ConsultarListaFiltro(
            int codigoLicenciatario,
            string filtro,
            Pageable pageable)
        {
            if (_dbContext.AgeLicenciatParamVigencias == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgeLicenciatParamVigencias
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                                p.AgeLicencCodigo == codigoLicenciatario &&
                               (p.AgeParGeCodigo.ToString().ToLower().Contains(filtro) ||
                                p.Codigo.ToString().ToLower().Contains(filtro) ||
                                p.Observacion.ToLower().Contains(filtro) ||
                                p.FechaDesde.ToString().Contains(filtro) ||
                                p.FechaHasta.ToString().Contains(filtro) ||
                                p.ValorParametro.ToLower().Contains(filtro)
                            ));

            return await Paginator<AgeLicenciatParamVigencias, AgeLicenciatParamVigenciasDAO>.Paginar(Query, pageable, FromEntityToDAO);
        }


        public async Task<AgeLicenciatParamVigenciasDAO> ConsultarPorId(
            AgeLicenciatParamVigenciasPKDAO ageLicenciatParamVigenciaPKDAO)
        {
            if (_dbContext.AgeLicenciatParamVigencias == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeLicenciatParamVigencias? ageLicenciatParamVigencia = await _dbContext.AgeLicenciatParamVigencias
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeLicencCodigo == ageLicenciatParamVigenciaPKDAO.AgeLicencCodigo &&
                            p.AgeParGeCodigo == ageLicenciatParamVigenciaPKDAO.AgeParGeCodigo &&
                            p.Codigo == ageLicenciatParamVigenciaPKDAO.Codigo).FirstOrDefaultAsync();

            return FromEntityToDAO(ageLicenciatParamVigencia);
        }


        public async Task<AgeLicenciatParamVigencias> ConsultarCompletePorId(AgeLicenciatParamVigenciasPKDAO ageLicenciatParamVigenciaPKDAO)
        {
            if (_dbContext.AgeLicenciatParamVigencias == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeLicenciatParamVigencias? ageLicenciatParamVigencia = await _dbContext.AgeLicenciatParamVigencias
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeLicencCodigo == ageLicenciatParamVigenciaPKDAO.AgeLicencCodigo &&
                            p.AgeParGeCodigo == ageLicenciatParamVigenciaPKDAO.AgeParGeCodigo &&
                            p.Codigo == ageLicenciatParamVigenciaPKDAO.Codigo)
                .FirstOrDefaultAsync();

            return ageLicenciatParamVigencia;
        }


        public async Task<AgeLicenciatParamVigenciasDAO> Insertar(AgeLicenciatParamVigencias ageLicenciatParamVigencia)
        {
            _dbContext.AgeLicenciatParamVigencias.Add(ageLicenciatParamVigencia);
            await _dbContext.SaveChangesAsync();

            AgeLicenciatParamVigenciasDAO ageLicenciatParamVigenciasDAO = await ConsultarPorId(new AgeLicenciatParamVigenciasPKDAO
            {
                AgeLicencCodigo = ageLicenciatParamVigencia.AgeLicencCodigo,
                AgeParGeCodigo = ageLicenciatParamVigencia.AgeParGeCodigo,
                Codigo = ageLicenciatParamVigencia.Codigo
            });

            return ageLicenciatParamVigenciasDAO;
        }

        public async Task<List<AgeLicenciatParamVigenciasDAO>> InsertarVarios(
            List<AgeLicenciatParamVigencias> ageLicenciatParamVigenciasList)
        {
            await _dbContext.AgeLicenciatParamVigencias.AddRangeAsync(ageLicenciatParamVigenciasList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeLicenciatParamVigencias, AgeLicenciatParamVigenciasDAO>
                .FromEntityToDAOList(ageLicenciatParamVigenciasList, FromEntityToDAO);
        }


        public async Task<AgeLicenciatParamVigenciasDAO> Actualizar(AgeLicenciatParamVigencias ageLicenciatParamVigencia)
        {
            _dbContext.Entry(ageLicenciatParamVigencia).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();

            AgeLicenciatParamVigenciasDAO ageLicenciatParamVigenciasDAO = await ConsultarPorId(new AgeLicenciatParamVigenciasPKDAO
            {
                AgeLicencCodigo = ageLicenciatParamVigencia.AgeLicencCodigo,
                AgeParGeCodigo = ageLicenciatParamVigencia.AgeParGeCodigo,
                Codigo = ageLicenciatParamVigencia.Codigo
            });

            return ageLicenciatParamVigenciasDAO;
        }


        public async Task<List<AgeLicenciatParamVigenciasDAO>> ActualizarVarios(List<AgeLicenciatParamVigencias> ageLicenciatParamVigenciasList)
        {
            _dbContext.AgeLicenciatParamVigencias.UpdateRange(ageLicenciatParamVigenciasList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeLicenciatParamVigencias, AgeLicenciatParamVigenciasDAO>
                .FromEntityToDAOList(ageLicenciatParamVigenciasList, FromEntityToDAO);
        }


        private static AgeLicenciatParamVigenciasDAO FromEntityToDAO(AgeLicenciatParamVigencias entityObject)
        {

            if (entityObject == null)
            {
                return null;
            }

            return new AgeLicenciatParamVigenciasDAO
            {
                Id = new AgeLicenciatParamVigenciasPKDAO
                {
                    AgeParGeCodigo = entityObject.AgeParGeCodigo,
                    AgeLicencCodigo = entityObject.AgeLicencCodigo,
                    Codigo = entityObject.Codigo
                },
                Observacion = entityObject.Observacion,
                FechaDesde = entityObject.FechaDesde,
                FechaHasta = entityObject.FechaHasta,
                ValorParametro = entityObject.ValorParametro,
                Estado = entityObject.Estado
            };
        }
    }

}
