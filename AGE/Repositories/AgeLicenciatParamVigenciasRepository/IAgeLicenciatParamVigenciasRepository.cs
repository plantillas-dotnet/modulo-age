﻿using AGE.Entities;
using AGE.Entities.DAO.AgeLicenciatParamVigencia;
using AGE.Utils.Paginacion;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AGE.Repositories.AgeLicenciatParamVigenciaRepository
{
    public interface IAgeLicenciatParamVigenciasRepository
    {
        Task<Page<AgeLicenciatParamVigenciasDAO>> ConsultarTodos(
            int codigoLicenciatario,
            int? codigoParametro,
            int? id,
            string? observacion,
            string? valorParametro,
            Pageable pageable);

        Task<Page<AgeLicenciatParamVigenciasDAO>> ConsultarListaFiltro(
            int codigoLicenciatario,
            string filtro,
            Pageable pageable);

        Task<AgeLicenciatParamVigenciasDAO> ConsultarPorId(AgeLicenciatParamVigenciasPKDAO ageLicenciatParamVigenciaPKDAO);

        Task<AgeLicenciatParamVigencias> ConsultarCompletePorId(AgeLicenciatParamVigenciasPKDAO ageLicenciatParamVigenciaPKDAO);

        Task<AgeLicenciatParamVigenciasDAO> Insertar(AgeLicenciatParamVigencias ageLicenciatParamVigencia);

        Task<List<AgeLicenciatParamVigenciasDAO>> InsertarVarios(List<AgeLicenciatParamVigencias> ageLicenciatParamVigenciasList);

        Task<AgeLicenciatParamVigenciasDAO> Actualizar(AgeLicenciatParamVigencias ageLicenciatParamVigencia);

        Task<List<AgeLicenciatParamVigenciasDAO>> ActualizarVarios(List<AgeLicenciatParamVigencias> ageLicenciatParamVigenciasList);
    }
}
