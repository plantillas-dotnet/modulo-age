﻿using AGE.Entities.DAO.AgeFranquicias;
using AGE.Entities;
using AGE.Utils.Paginacion;

namespace AGE.Repositories.AgeFranquiciasRepository
{
    public interface IAgeFranquiciasRepository
    {
        Task<Page<AgeFranquiciasDAO>> ConsultarTodos(
            int codigo,
            string descripcion,
            Pageable pageable);

        Task<AgeFranquiciasDAO> ConsultarPorId(
            int codigo);

        Task<AgeFranquicias> ConsultarCompletePorId(
            int codigo);

        Task<Page<AgeFranquiciasDAO>> ConsultarListaFiltro(
            string filtro,
            Pageable pageable);

        Task<AgeFranquiciasDAO> Insertar(
            AgeFranquicias ageFranquicias);

        Task<AgeFranquiciasDAO> Actualizar(
            AgeFranquicias ageFranquicias);

        Task<List<AgeFranquiciasDAO>> InsertarVarios(
            List<AgeFranquicias> ageFranquiciasList);

        Task<List<AgeFranquiciasDAO>> ActualizarVarios(
            List<AgeFranquicias> ageFranquiciasList);
    }
}
