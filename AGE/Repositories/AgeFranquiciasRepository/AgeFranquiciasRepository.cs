﻿using AGE.Entities.DAO.AgeFranquicias;
using AGE.Entities;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Utils.Paginacion;
using AGE.Utils;
using Microsoft.EntityFrameworkCore;

namespace AGE.Repositories.AgeFranquiciasRepository
{
    public class AgeFranquiciasRepository : IAgeFranquiciasRepository
    {
        private readonly DbContextAge _dbContext;

        public AgeFranquiciasRepository(DbContextAge dbContext)
        {
            _dbContext = dbContext;
        }


        public async Task<Page<AgeFranquiciasDAO>> ConsultarTodos(
            int codigo,
            string descripcion,
            Pageable pageable)
        {
            if (_dbContext.AgeFranquicias == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgeFranquicias
                .Where(p => p.Estado != Globales.ESTADO_ANULADO && (codigo == 0 || p.Codigo == codigo) &&
                            (descripcion.Equals("") || p.Descripcion.ToLower().Contains(descripcion.ToLower())));

            return await Paginator<AgeFranquicias, AgeFranquiciasDAO>.Paginar(Query, pageable, FromEntityToDAO);
        }


        public async Task<Page<AgeFranquiciasDAO>> ConsultarListaFiltro(
            string filtro,
            Pageable pageable)
        {
            if (_dbContext.AgeFranquicias == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgeFranquicias
                .Where(p => p.Estado != Globales.ESTADO_ANULADO && (
                            p.Codigo.ToString().ToLower().Contains(filtro.ToLower()) ||
                            p.Descripcion.ToLower().Contains(filtro.ToLower())));

            return await Paginator<AgeFranquicias, AgeFranquiciasDAO>.Paginar(Query, pageable, FromEntityToDAO);
        }


        public async Task<AgeFranquiciasDAO> ConsultarPorId(int codigo)
        {
            if (_dbContext.AgeFranquicias == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeFranquicias? ageFranquicias = await _dbContext.AgeFranquicias
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                p.Codigo == codigo).FirstOrDefaultAsync();

            return FromEntityToDAO(ageFranquicias);
        }


        public async Task<AgeFranquicias> ConsultarCompletePorId(int codigo)
        {
            if (_dbContext.AgeFranquicias == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeFranquicias? ageFranquicias = await _dbContext.AgeFranquicias
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                p.Codigo == codigo).FirstOrDefaultAsync();

            return ageFranquicias;
        }

        public async Task<AgeFranquiciasDAO> Insertar(AgeFranquicias ageFranquicias)
        {
            _dbContext.AgeFranquicias.Add(ageFranquicias);
            await _dbContext.SaveChangesAsync();

            AgeFranquiciasDAO ageFranquiciasDAO = await ConsultarPorId(ageFranquicias.Codigo);

            return ageFranquiciasDAO;
        }

        public async Task<AgeFranquiciasDAO> Actualizar(AgeFranquicias ageFranquicias)
        {
            _dbContext.Entry(ageFranquicias).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();

            AgeFranquiciasDAO ageFranquiciasDAO = await ConsultarPorId(ageFranquicias.Codigo);

            return ageFranquiciasDAO;
        }

        public async Task<List<AgeFranquiciasDAO>> InsertarVarios(List<AgeFranquicias> ageFranquiciasList)
        {
            await _dbContext.AgeFranquicias.AddRangeAsync(ageFranquiciasList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeFranquicias, AgeFranquiciasDAO>
                .FromEntityToDAOList(ageFranquiciasList, FromEntityToDAO);
        }

        public async Task<List<AgeFranquiciasDAO>> ActualizarVarios(List<AgeFranquicias> ageFranquiciasList)
        {
            _dbContext.AgeFranquicias.UpdateRange(ageFranquiciasList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeFranquicias, AgeFranquiciasDAO>
                .FromEntityToDAOList(ageFranquiciasList, FromEntityToDAO);
        }

        private AgeFranquiciasDAO FromEntityToDAO(AgeFranquicias entityObject)
        {
            AgeFranquiciasDAO DAOobject = null;

            if (entityObject != null)
            {
                DAOobject = new AgeFranquiciasDAO
                {
                    Estado = entityObject.Estado,
                    Codigo = entityObject.Codigo,
                    Descripcion = entityObject.Descripcion
                };
            }

            return DAOobject;

        }
    }
}
