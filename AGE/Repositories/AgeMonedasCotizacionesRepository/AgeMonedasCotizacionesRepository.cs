﻿using AGE.Entities;
using AGE.Entities.DAO.AgeMonedasCotizaciones;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Utils;
using AGE.Utils.Paginacion;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;

namespace AGE.Repositories.AgeMonedasCotizacionesRepository
{
    public class AgeMonedasCotizacionesRepository : IAgeMonedasCotizacionesRepository
    {
        private readonly DbContextAge _dbContext;

        public AgeMonedasCotizacionesRepository(DbContextAge dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Page<AgeMonedasCotizacionesDAO>> ConsultarTodos(
            int codigoLicenciatario,
            int Codigo,
            int ageMonedaCodigo,
            int ageMonedaCodigoSerA,
            Pageable pageable)
        {
            if (_dbContext.AgeMonedasCotizaciones == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgeMonedasCotizaciones
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeLicencCodigo == codigoLicenciatario &&
                            (Codigo == 0 || p.Codigo == Codigo) &&
                            (ageMonedaCodigo == 0 || p.AgeMonedaCodigo == ageMonedaCodigo) &&
                            (ageMonedaCodigoSerA == 0 || p.AgeMonedaCodigoSerA == ageMonedaCodigoSerA)
                      );

            return await Paginator<AgeMonedasCotizaciones, AgeMonedasCotizacionesDAO>.Paginar(Query, pageable, FromEntityToDAO);
        }



        public async Task<Page<AgeMonedasCotizacionesDAO>> ConsultarListaFiltro(
            int codigoLicenciatario,
            string filtro,
            Pageable pageable)
        {
            if (_dbContext.AgeMonedasCotizaciones == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgeMonedasCotizaciones
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeLicencCodigo == codigoLicenciatario && (
                                p.Codigo.ToString().ToLower().Contains(filtro) ||
                                p.AgeMonedaCodigo.ToString().ToLower().Contains(filtro) ||
                                p.AgeMonedaCodigoSerA.ToString().ToLower().Contains(filtro)
                            )
                       );

            return await Paginator<AgeMonedasCotizaciones, AgeMonedasCotizacionesDAO>.Paginar(Query, pageable, FromEntityToDAO);
        }

        public async Task<AgeMonedasCotizacionesDAO> ConsultarPorId(AgeMonedasCotizacionesPKDAO ageMonedasCotizacionesPKDAO)
        {
            if (_dbContext.AgeMonedasCotizaciones == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeMonedasCotizaciones? ageMonedasCotizacione = await _dbContext.AgeMonedasCotizaciones
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeLicencCodigo == ageMonedasCotizacionesPKDAO.AgeLicencCodigo &&
                            p.Codigo == ageMonedasCotizacionesPKDAO.Codigo).FirstOrDefaultAsync();

            return FromEntityToDAO(ageMonedasCotizacione);
        }


        public async Task<AgeMonedasCotizaciones> ConsultarCompletePorId(AgeMonedasCotizacionesPKDAO ageMonedasCotizacionesPKDAO)
        {
            if (_dbContext.AgeMonedasCotizaciones == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeMonedasCotizaciones? ageMonedasCotizacione = await _dbContext.AgeMonedasCotizaciones
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeLicencCodigo == ageMonedasCotizacionesPKDAO.AgeLicencCodigo &&
                            p.Codigo == ageMonedasCotizacionesPKDAO.Codigo).FirstOrDefaultAsync();

            return ageMonedasCotizacione;
        }


        public async Task<AgeMonedasCotizacionesDAO> Insertar(
            AgeMonedasCotizaciones ageMonedasCotizaciones)
        {
            _dbContext.AgeMonedasCotizaciones.Add(ageMonedasCotizaciones);
            await _dbContext.SaveChangesAsync();

            AgeMonedasCotizacionesDAO ageMonedasCotizacionesDAO = await ConsultarPorId(new AgeMonedasCotizacionesPKDAO
            {
                AgeLicencCodigo = ageMonedasCotizaciones.AgeLicencCodigo,
                Codigo = ageMonedasCotizaciones.Codigo
            });

            return ageMonedasCotizacionesDAO;
        }

        public async Task<List<AgeMonedasCotizacionesDAO>> InsertarVarios(
            List<AgeMonedasCotizaciones> ageMonedasCotizacionesList)
        {
            await _dbContext.AgeMonedasCotizaciones.AddRangeAsync(ageMonedasCotizacionesList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeMonedasCotizaciones, AgeMonedasCotizacionesDAO>
                .FromEntityToDAOList(ageMonedasCotizacionesList, FromEntityToDAO);
        }


        public async Task<AgeMonedasCotizacionesDAO> Actualizar(AgeMonedasCotizaciones ageMonedasCotizaciones)
        {
            _dbContext.Entry(ageMonedasCotizaciones).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();

            AgeMonedasCotizacionesDAO ageMonedasCotizacionesDAO = await ConsultarPorId(new AgeMonedasCotizacionesPKDAO
            {
                AgeLicencCodigo = ageMonedasCotizaciones.AgeLicencCodigo,
                Codigo = ageMonedasCotizaciones.Codigo
            });

            return ageMonedasCotizacionesDAO;
        }


        public async Task<List<AgeMonedasCotizacionesDAO>> ActualizarVarios(List<AgeMonedasCotizaciones> ageMonedasCotizacionesList)
        {
            _dbContext.AgeMonedasCotizaciones.UpdateRange(ageMonedasCotizacionesList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeMonedasCotizaciones, AgeMonedasCotizacionesDAO>
                .FromEntityToDAOList(ageMonedasCotizacionesList, FromEntityToDAO);
        }


        private static AgeMonedasCotizacionesDAO FromEntityToDAO(AgeMonedasCotizaciones entityObject)
        {

            if (entityObject == null)
            {
                return null;
            }

            return new AgeMonedasCotizacionesDAO
            {
                Id = new AgeMonedasCotizacionesPKDAO
                {
                    AgeLicencCodigo = entityObject.AgeLicencCodigo,
                    Codigo = entityObject.Codigo
                },
                AgeMonedaCodigo = entityObject.AgeMonedaCodigo,
                AgeMonedaCodigoSerA = entityObject.AgeMonedaCodigoSerA,
                FechaDesde = entityObject.FechaDesde,
                FechaHasta = entityObject.FechaHasta,
                CotizacionMercado = entityObject.CotizacionMercado,
                CotizacionCompra = entityObject.CotizacionCompra,
                CotizacionVenta = entityObject.CotizacionVenta,
                Estado = entityObject.Estado
            };
        }

    }
}
