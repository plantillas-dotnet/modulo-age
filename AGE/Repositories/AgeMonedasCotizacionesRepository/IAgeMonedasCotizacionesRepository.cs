﻿using AGE.Entities;
using AGE.Entities.DAO.AgeMonedasCotizaciones;
using AGE.Utils.Paginacion;

namespace AGE.Repositories.AgeMonedasCotizacionesRepository
{
    public interface IAgeMonedasCotizacionesRepository
    {
        Task<Page<AgeMonedasCotizacionesDAO>> ConsultarTodos(
            int codigoLicenciatario,
            int codigo,
            int ageMonedaCodigo,
            int ageMonedaCodigoSerA,
            Pageable pageable);
        Task<Page<AgeMonedasCotizacionesDAO>> ConsultarListaFiltro(
            int codigoLicenciatario,
            string filtro,
            Pageable pageable);
        Task<AgeMonedasCotizacionesDAO> ConsultarPorId(
            AgeMonedasCotizacionesPKDAO ageMonedasCotizacionesPKDAO);
        Task<AgeMonedasCotizaciones> ConsultarCompletePorId(
            AgeMonedasCotizacionesPKDAO ageMonedasCotizacionesPKDAO);
        Task<AgeMonedasCotizacionesDAO> Insertar(
            AgeMonedasCotizaciones ageMonedasCotizaciones);
        Task<List<AgeMonedasCotizacionesDAO>> InsertarVarios(
            List<AgeMonedasCotizaciones> ageMonedasCotizacionesList);
        Task<AgeMonedasCotizacionesDAO> Actualizar(
            AgeMonedasCotizaciones ageMonedasCotizaciones);
        Task<List<AgeMonedasCotizacionesDAO>> ActualizarVarios(
            List<AgeMonedasCotizaciones> ageMonedasCotizacionesList);
    }
}
