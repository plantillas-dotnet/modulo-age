﻿using AGE.Entities.DAO.AgeLogErrores;
using AGE.Entities;
using AGE.Utils.Paginacion;

namespace AGE.Repositories.AgeLogErroresRepository
{
    public interface IAgeLogErroresRepository
    {
        Task<Page<AgeLogErroresDAO>> ConsultarTodos(
            int? ageLicApAgeLicencCodigo,
            int? ageLicApAgeAplicaCodigo,
            long? codigo,
            DateTime? fechaDesde,
            DateTime? fechaHasta,
            string? mensaje,
            Pageable pageable);

        Task<AgeLogErroresDAO> ConsultarPorId(
            AgeLogErroresPKDAO ageLogErroresPKDAO);

        Task<AgeLogErrores> ConsultarCompletePorId(
            AgeLogErroresPKDAO ageLogErroresPKDAO);

        Task<Page<AgeLogErroresDAO>> ConsultarListaFiltro(
            int ageLicApAgeLicencCodigo,
            string filtro,
            Pageable pageable);

        Task<AgeLogErroresDAO> Insertar(
            AgeLogErrores ageLogErrores);

        Task<AgeLogErroresDAO> Actualizar(
            AgeLogErrores ageLogErrores);

        Task<List<AgeLogErroresDAO>> InsertarVarios(
            List<AgeLogErrores> ageLogErroresList);

        Task<List<AgeLogErroresDAO>> ActualizarVarios(
            List<AgeLogErrores> ageLogErroresList);
    }
}
