﻿using AGE.Entities.DAO.AgeLogErrores;
using AGE.Entities;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Utils.Paginacion;
using AGE.Utils;
using Microsoft.EntityFrameworkCore;

namespace AGE.Repositories.AgeLogErroresRepository
{
    public class AgeLogErroresRepository : IAgeLogErroresRepository
    {
        private readonly DbContextAge _dbContext;

        public AgeLogErroresRepository(DbContextAge dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Page<AgeLogErroresDAO>> ConsultarTodos(
             int? ageLicApAgeLicencCodigo,
             int? ageLicApAgeAplicaCodigo,
             long? codigo,
             DateTime? fechaDesde,
             DateTime? fechaHasta,
             string? mensaje,
             Pageable pageable)
        {
            if (_dbContext.AgeLogErrores == null)
            {
                throw new RegisterNotFoundException();
            }

            var query = _dbContext.AgeLogErrores
                .Where(a => a.Estado != Globales.ESTADO_ANULADO &&
                            a.AgeLicApAgeLicencCodigo == ageLicApAgeLicencCodigo &&
                            (ageLicApAgeAplicaCodigo == 0 || a.AgeLicApAgeAplicaCodigo == ageLicApAgeAplicaCodigo) &&
                            (codigo == 0 || a.Codigo == codigo) &&
                            (fechaDesde == null || fechaDesde == default || a.Fecha >= fechaDesde) && 
                            (fechaHasta == null || fechaHasta == default || a.Fecha <= fechaHasta) &&
                            (string.IsNullOrEmpty(mensaje) || a.Mensaje.Contains(mensaje)));

            return await Paginator<AgeLogErrores, AgeLogErroresDAO>.Paginar(query, pageable, FromEntityToDAO);
        }

        public async Task<Page<AgeLogErroresDAO>> ConsultarListaFiltro(
            int ageLicApAgeLicencCodigo,
            string filtro,
            Pageable pageable)
        {
            if (_dbContext.AgeLogErrores == null)
            {
                throw new RegisterNotFoundException();
            }

            var query = _dbContext.AgeLogErrores
                .Where(a => a.Estado != Globales.ESTADO_ANULADO &&
                            a.AgeLicApAgeLicencCodigo == ageLicApAgeLicencCodigo &&
                            a.AgeLicApAgeAplicaCodigo.ToString().ToUpper().Contains(filtro.ToUpper()) ||
                            a.Codigo.ToString().ToUpper().Contains(filtro.ToUpper()) ||
                            a.Mensaje.ToUpper().Contains(filtro.ToUpper()));

            return await Paginator<AgeLogErrores, AgeLogErroresDAO>.Paginar(query, pageable, FromEntityToDAO);
        }

        public async Task<AgeLogErroresDAO> ConsultarPorId(AgeLogErroresPKDAO ageLogErroresPKDAO)
        {
            if (_dbContext.AgeLogErrores == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeLogErrores? ageLogErrores = await _dbContext.AgeLogErrores
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeLicApAgeLicencCodigo == ageLogErroresPKDAO.AgeLicApAgeLicencCodigo &&
                            p.AgeLicApAgeAplicaCodigo == ageLogErroresPKDAO.AgeLicApAgeAplicaCodigo &&
                            p.Codigo == ageLogErroresPKDAO.Codigo).FirstOrDefaultAsync();

            return FromEntityToDAO(ageLogErrores);
        }

        public async Task<AgeLogErrores> ConsultarCompletePorId(AgeLogErroresPKDAO ageLogErroresPK)
        {
            if (_dbContext.AgeLogErrores == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeLogErrores? ageLogErrores = await _dbContext.AgeLogErrores
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeLicApAgeLicencCodigo == ageLogErroresPK.AgeLicApAgeLicencCodigo &&
                            p.AgeLicApAgeAplicaCodigo == ageLogErroresPK.AgeLicApAgeAplicaCodigo &&
                            p.Codigo == ageLogErroresPK.Codigo).FirstOrDefaultAsync();

            return ageLogErrores;
        }

        public async Task<AgeLogErroresDAO> Insertar(AgeLogErrores ageLogErrores)
        {
            _dbContext.AgeLogErrores.Add(ageLogErrores);
            await _dbContext.SaveChangesAsync();

            AgeLogErroresDAO ageLogErroresDAO = await ConsultarPorId(new AgeLogErroresPKDAO
            {
                AgeLicApAgeLicencCodigo = ageLogErrores.AgeLicApAgeLicencCodigo,
                AgeLicApAgeAplicaCodigo = ageLogErrores.AgeLicApAgeAplicaCodigo,
                Codigo = ageLogErrores.Codigo
            });

            return ageLogErroresDAO;
        }

        public async Task<List<AgeLogErroresDAO>> InsertarVarios(List<AgeLogErrores> ageLogErroresList)
        {
            await _dbContext.AgeLogErrores.AddRangeAsync(ageLogErroresList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeLogErrores, AgeLogErroresDAO>
                .FromEntityToDAOList(ageLogErroresList, FromEntityToDAO);
        }

        public async Task<AgeLogErroresDAO> Actualizar(AgeLogErrores ageLogErrores)
        {
            _dbContext.Entry(ageLogErrores).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();

            AgeLogErroresDAO ageLogErroresDAO = await ConsultarPorId(new AgeLogErroresPKDAO
            {
                AgeLicApAgeLicencCodigo = ageLogErrores.AgeLicApAgeLicencCodigo,
                AgeLicApAgeAplicaCodigo = ageLogErrores.AgeLicApAgeAplicaCodigo,
                Codigo = ageLogErrores.Codigo
            });

            return ageLogErroresDAO;
        }

        public async Task<List<AgeLogErroresDAO>> ActualizarVarios(List<AgeLogErrores> ageLogErroresList)
        {
            _dbContext.AgeLogErrores.UpdateRange(ageLogErroresList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeLogErrores, AgeLogErroresDAO>
                .FromEntityToDAOList(ageLogErroresList, FromEntityToDAO);
        }

        private static AgeLogErroresDAO FromEntityToDAO(AgeLogErrores entityObject)
        {
            if (entityObject == null)
            {
                return null;
            }

            return new AgeLogErroresDAO
            {
                Estado = entityObject.Estado,
                Id = new AgeLogErroresPKDAO
                {
                    AgeLicApAgeLicencCodigo = entityObject.AgeLicApAgeLicencCodigo,
                    AgeLicApAgeAplicaCodigo = entityObject.AgeLicApAgeAplicaCodigo,
                    Codigo = entityObject.Codigo
                },
                Fecha = entityObject.Fecha,
                Mensaje = entityObject.Mensaje
            };
        }
    }
}
