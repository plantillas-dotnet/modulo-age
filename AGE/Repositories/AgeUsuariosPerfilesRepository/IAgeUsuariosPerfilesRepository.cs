﻿using AGE.Entities;
using AGE.Entities.DAO.AgeUsuariosPerfiles;
using AGE.Utils.Paginacion;

namespace AGE.Repositories.AgeUsuariosPerfileRepository
{
    public interface IAgeUsuariosPerfilesRepository
    {
        Task<Page<AgeUsuariosPerfileDAO>> ConsultarTodos(
            int ageLicencCodigo,
            Pageable pageable);

        Task<AgeUsuariosPerfileDAO> ConsultarPorId(AgeUsuariosPerfilePKDAO ageUsuariosPerfilePKDAO);

        Task<Page<AgeUsuariosPerfileDAO>> ConsultarListaFiltro(
            int ageLicencCodigo,
            string filtro,
            Pageable pageable);

        Task<AgeUsuariosPerfiles> ConsultarCompletePorId(
            AgeUsuariosPerfilePKDAO ageUsuariosPerfilePKDAO);

        Task<AgeUsuariosPerfileDAO> Insertar(AgeUsuariosPerfiles AgeUsuariosPerfile);

        Task<List<AgeUsuariosPerfileDAO>> InsertarVarios(
            List<AgeUsuariosPerfiles> AgeUsuariosPerfileList);

        Task<AgeUsuariosPerfileDAO> Actualizar(AgeUsuariosPerfiles AgeUsuariosPerfile);

        Task<List<AgeUsuariosPerfileDAO>> ActualizarVarios(
            List<AgeUsuariosPerfiles> AgeUsuariosPerfileList);
    }
}
