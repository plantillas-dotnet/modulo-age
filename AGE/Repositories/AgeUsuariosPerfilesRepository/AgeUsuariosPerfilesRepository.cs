﻿using AGE.Entities;
using AGE.Entities.DAO.AgePerfiles;
using AGE.Entities.DAO.AgeUsuarios;
using AGE.Entities.DAO.AgeUsuariosPerfiles;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Utils;
using AGE.Utils.Paginacion;
using Microsoft.EntityFrameworkCore;

namespace AGE.Repositories.AgeUsuariosPerfileRepository
{
    public class AgeUsuariosPerfilesRepository : IAgeUsuariosPerfilesRepository
    {
        private readonly DbContextAge _dbContext;

        public AgeUsuariosPerfilesRepository(DbContextAge dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Page<AgeUsuariosPerfileDAO>> ConsultarTodos(int ageLicencCodigo, Pageable pageable)
        {
            if (_dbContext.AgeUsuariosPerfiles == null)
            {
                throw new RegisterNotFoundException();
            }

            var query = _dbContext.AgeUsuariosPerfiles
                .Where(a => a.Estado != Globales.ESTADO_ANULADO &&
                            a.AgeUsuariAgeLicencCodigo == ageLicencCodigo)
                .Include(y => y.AgeUsuari)
                    .ThenInclude(x => x.AgePerson)
                            .Include(x => x.AgePerfil);

            return await Paginator<AgeUsuariosPerfiles, AgeUsuariosPerfileDAO>.Paginar(query, pageable, FromEntityToDAO);
        }

        public async Task<AgeUsuariosPerfileDAO> ConsultarPorId(AgeUsuariosPerfilePKDAO ageUsuariosPerfilePKDAO)
        {
            if (_dbContext.AgeUsuariosPerfiles == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeUsuariosPerfiles? ageUsuariosPerfile = await _dbContext.AgeUsuariosPerfiles
                            .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeUsuariAgeLicencCodigo == ageUsuariosPerfilePKDAO.AgeUsuariAgeLicencCodigo &&
                            p.Codigo == ageUsuariosPerfilePKDAO.Codigo &&
                            p.AgeUsuariCodigo == ageUsuariosPerfilePKDAO.AgeUsuariCodigo)
                            .Include(y => y.AgeUsuari)
                                .ThenInclude(x => x.AgePerson)
                                        .Include(x => x.AgePerfil).FirstOrDefaultAsync();

            return FromEntityToDAO(ageUsuariosPerfile);
        }

        public async Task<Page<AgeUsuariosPerfileDAO>> ConsultarListaFiltro(int AgeUsuariAgeLicencCodigo, string filtro, Pageable pageable)
        {
            if (_dbContext.AgeUsuariosPerfiles == null)
            {
                throw new RegisterNotFoundException();
            }

            var query = _dbContext.AgeUsuariosPerfiles
                .Where(a => a.Estado != Globales.ESTADO_ANULADO &&
                            a.AgeUsuariAgeLicencCodigo == AgeUsuariAgeLicencCodigo &&
                            a.AgePerfil.Descripcion.ToUpper().Contains(filtro.ToUpper()) ||
                            a.AgeUsuari.Nombres.ToUpper().Contains(filtro.ToUpper()) ||
                            a.AgeUsuari.Apellidos.ToUpper().Contains(filtro.ToUpper()) ||
                            a.AgeUsuari.NumeroIdentificacion.ToUpper().Contains(filtro.ToUpper()))
                .Include(y => y.AgeUsuari)
                    .ThenInclude(x => x.AgePerson)
                            .Include(x => x.AgePerfil);

            return await Paginator<AgeUsuariosPerfiles, AgeUsuariosPerfileDAO>.Paginar(query, pageable, FromEntityToDAO);
        }

        public async Task<AgeUsuariosPerfileDAO> Insertar(AgeUsuariosPerfiles ageUsuariosPerfile)
        {
            _dbContext.AgeUsuariosPerfiles.Add(ageUsuariosPerfile);
            await _dbContext.SaveChangesAsync();

            AgeUsuariosPerfileDAO ageUsuarioPerfileDAO = await ConsultarPorId(new AgeUsuariosPerfilePKDAO
            {
                AgeUsuariAgeLicencCodigo = ageUsuariosPerfile.AgeUsuariAgeLicencCodigo,
                Codigo = ageUsuariosPerfile.Codigo,
                AgeUsuariCodigo = ageUsuariosPerfile.AgeUsuariCodigo
            });

            return ageUsuarioPerfileDAO;
        }

        public async Task<List<AgeUsuariosPerfileDAO>> InsertarVarios(List<AgeUsuariosPerfiles> ageUsuariosPerfileList)
        {
            await _dbContext.AgeUsuariosPerfiles.AddRangeAsync(ageUsuariosPerfileList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeUsuariosPerfiles, AgeUsuariosPerfileDAO>
                .FromEntityToDAOList(ageUsuariosPerfileList, FromEntityToDAO);
        }

        public async Task<AgeUsuariosPerfiles> ConsultarCompletePorId(AgeUsuariosPerfilePKDAO ageUsuariosPerfilePKDAO)
        {
            if (_dbContext.AgeUsuariosPerfiles == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeUsuariosPerfiles? ageUsuariosPerfile = await _dbContext.AgeUsuariosPerfiles
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeUsuariAgeLicencCodigo == ageUsuariosPerfilePKDAO.AgeUsuariAgeLicencCodigo &&
                            p.Codigo == ageUsuariosPerfilePKDAO.Codigo &&
                            p.AgeUsuariCodigo == ageUsuariosPerfilePKDAO.AgeUsuariCodigo)
                .Include(y => y.AgeUsuari)
                    .ThenInclude(x => x.AgePerson)
                            .Include(x => x.AgePerfil)
                .FirstOrDefaultAsync();

            return ageUsuariosPerfile;
        }

        public async Task<AgeUsuariosPerfileDAO> Actualizar(AgeUsuariosPerfiles AgeUsuariosPerfile)
        {
            _dbContext.Entry(AgeUsuariosPerfile).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();

            AgeUsuariosPerfileDAO ageUsuarioPerfileDAO = await ConsultarPorId(new AgeUsuariosPerfilePKDAO
            {
                AgeUsuariAgeLicencCodigo = AgeUsuariosPerfile.AgeUsuariAgeLicencCodigo,
                Codigo = AgeUsuariosPerfile.Codigo,
                AgeUsuariCodigo = AgeUsuariosPerfile.AgeUsuariCodigo
            });

            return ageUsuarioPerfileDAO;
        }

        public async Task<List<AgeUsuariosPerfileDAO>> ActualizarVarios(List<AgeUsuariosPerfiles> AgeUsuariosPerfileList)
        {
            _dbContext.AgeUsuariosPerfiles.UpdateRange(AgeUsuariosPerfileList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeUsuariosPerfiles, AgeUsuariosPerfileDAO>
                .FromEntityToDAOList(AgeUsuariosPerfileList, FromEntityToDAO);
        }

        private static AgeUsuariosPerfileDAO? FromEntityToDAO(AgeUsuariosPerfiles perfile)
        {
            if (perfile == null)
            {
                return null;
            }

            List<AgePerfiles> agePerfilList = new(); ;
            AgePerfilesDAO agePerfilDAO = new();
            List<AgePerfilesDAO> agePerfilDAOList = new();
            AgeUsuarios ageUsuario = null;
            AgeUsuarioDAO ageUsuarioDAO = new();

            if (perfile.AgeUsuari != null)
                agePerfilList = perfile.AgeUsuari.AgeUsuariosPerfiles
                .Where(up => up.AgePerfil != null)
                .Select(up => up.AgePerfil)
                .ToList();

            if (agePerfilList.Count > 0)
            {
                foreach (AgePerfiles agePerfil in agePerfilList)
                {
                    agePerfilDAO = new AgePerfilesDAO
                    {
                        Id = new AgePerfilesPKDAO
                        {
                            ageLicencCodigo = perfile.AgeUsuariAgeLicencCodigo,
                            codigo = agePerfil.Codigo
                        },
                        Descripcion = agePerfil.Descripcion,
                        Estado = agePerfil.Estado
                    };

                    agePerfilDAOList.Add(agePerfilDAO);
                }
            }

            if (perfile.AgeUsuari != null)
            {
                ageUsuario = perfile.AgeUsuari;

                ageUsuarioDAO = new AgeUsuarioDAO
                {
                    Id = new AgeUsuarioPKDAO
                    {
                        ageLicencCodigo = ageUsuario.AgeLicencCodigo,
                        codigo = ageUsuario.Codigo
                    },
                    AgePersonAgeLicencCodigo = ageUsuario.AgePersonAgeLicencCodigo ?? 0,
                    AgePersonCodigo = ageUsuario.AgePersonCodigo ?? 0,
                    AgeSucursAgeLicencCodigo = ageUsuario.AgeSucursAgeLicencCodigo,
                    AgeSucursCodigo = ageUsuario.AgeSucursCodigo,
                    AgeTipIdCodigo = ageUsuario.AgeTipIdCodigo,
                    ArchivoFoto = ageUsuario.ArchivoFoto,
                    CodigoExterno = ageUsuario.CodigoExterno,
                    Clave = ageUsuario.Clave,
                    NumeroIdentificacion = ageUsuario.NumeroIdentificacion,
                    Nombres = ageUsuario.Nombres,
                    Apellidos = ageUsuario.Apellidos,
                    MailPrincipal = ageUsuario.MailPrincipal,
                    TelefonoCelular = ageUsuario.TelefonoCelular,
                    TipoUsuario = ageUsuario.TipoUsuario,
                    PrimerIngreso = ageUsuario.PrimerIngreso,
                    TipoRegistro = ageUsuario.TipoRegistro,
                    Estado = ageUsuario.Estado,
                    Direccion = ageUsuario.AgePerson?.DireccionPrincipal,
                    EstadoCivil = ageUsuario.AgePerson?.EstadoCivil,
                    FechaNacimiento = ageUsuario.AgePerson?.FechaNacimiento,
                    Genero = ageUsuario.AgePerson?.Sexo,
                    AgePerfilList = agePerfilDAOList
                };
            }

            return new AgeUsuariosPerfileDAO
            {

                Id = new AgeUsuariosPerfilePKDAO
                {
                    AgeUsuariAgeLicencCodigo = perfile.AgeUsuariAgeLicencCodigo,
                    AgeUsuariCodigo = perfile.AgeUsuariCodigo,
                    Codigo = perfile.Codigo
                },
                Estado = perfile.Estado,
                AgePerfilAgeLicencCodigo = perfile.AgePerfilAgeLicencCodigo,
                AgePerfilCodigo = perfile.AgePerfilCodigo,
                FechaDesde = perfile.FechaDesde,
                FechaHasta = perfile.FechaHasta,
                AgeUsuarios = ageUsuarioDAO,
                
            };
        }

    }
}
