﻿using AGE.Entities;
using AGE.Utils.Paginacion;
using AGE.Entities.DAO.AgeFormasPago;

namespace AGE.Repositories.AgeFormasPagoRepository
{
    public interface IAgeFormasPagosRepository
    {

        Task<Page<AgeFormasPagosDAO>> ConsultarTodos(
            int codigoLicenciatario,
            int codigoInstitucionControl,
            string descripcion,
            Pageable pageable);

        Task<Page<AgeFormasPagosDAO>> ConsultarListaFiltro(
            int codigoLicenciatario,
            string filtro,
            Pageable pageable);

        Task<AgeFormasPagosDAO> ConsultarPorId(
            AgeFormasPagosPKDAO ageFormasPagoPKDAO);

        Task<AgeFormasPagos> ConsultarCompletePorId(
           AgeFormasPagosPKDAO ageFormasPagoPKDAO);

        Task<AgeFormasPagosDAO> Insertar(AgeFormasPagos ageFormasPago);

        Task<List<AgeFormasPagosDAO>> InsertarVarios(
            List<AgeFormasPagos> ageFormasPagoList);

        Task<AgeFormasPagosDAO> Actualizar(AgeFormasPagos ageFormasPago);

        Task<List<AgeFormasPagosDAO>> ActualizarVarios(
            List<AgeFormasPagos> ageFormasPagoList);

    }
}
