﻿using AGE.Entities;
using AGE.Entities.DAO.AgeFormasPago;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Utils;
using AGE.Utils.Paginacion;
using Microsoft.EntityFrameworkCore;

namespace AGE.Repositories.AgeFormasPagoRepository
{
    public class AgeFormasPagosRepository : IAgeFormasPagosRepository
    {

        private readonly DbContextAge _dbContext;

        public AgeFormasPagosRepository(DbContextAge dbContext)
        {
            _dbContext = dbContext;
        }


        public async Task<Page<AgeFormasPagosDAO>> ConsultarTodos(
            int codigoLicenciatario,
            int codigoInstitucionControl,
            string descripcion,
            Pageable pageable)
        {
            if (_dbContext.AgeFormasPagos == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgeFormasPagos
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            (codigoLicenciatario == 0 || p.AgeLicencCodigo == codigoLicenciatario) &&
                            (codigoInstitucionControl == 0 || p.AgeLicencCodigo == codigoLicenciatario) &&
                            (descripcion.Equals("") || p.Descripcion.ToLower().Contains(descripcion.ToLower())));

            return await Paginator<AgeFormasPagos, AgeFormasPagosDAO>.Paginar(Query, pageable, FromEntityToDAO);
        }




        public async Task<AgeFormasPagosDAO> ConsultarPorId(
        AgeFormasPagosPKDAO ageFormasPagoPKDAO)
        {
            if (_dbContext.AgeFormasPagos == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeFormasPagos? ageFormasPago = await _dbContext.AgeFormasPagos
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeLicencCodigo == ageFormasPagoPKDAO.AgeLicencCodigo &&
                            p.Codigo == ageFormasPagoPKDAO.Codigo).FirstOrDefaultAsync();

            return FromEntityToDAO(ageFormasPago);
        }


        public async Task<AgeFormasPagos> ConsultarCompletePorId(
            AgeFormasPagosPKDAO ageFormasPagoPKDAO)
        {
            if (_dbContext.AgeFormasPagos == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeFormasPagos? ageFormasPago = await _dbContext.AgeFormasPagos
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeLicencCodigo == ageFormasPagoPKDAO.AgeLicencCodigo &&
                            p.Codigo == ageFormasPagoPKDAO.Codigo).FirstOrDefaultAsync();

            return ageFormasPago;
        }


        public async Task<Page<AgeFormasPagosDAO>> ConsultarListaFiltro(
            int codigoLicenciatario,
            string filtro,
            Pageable pageable)
        {
            if (_dbContext.AgeFormasPagos == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgeFormasPagos
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeLicencCodigo == codigoLicenciatario && (
                            p.Codigo.ToString().ToLower().Contains(filtro) ||
                            p.CodigoInstitucionControl.ToString().ToLower().Contains(filtro) ||
                            p.Descripcion.ToLower().Contains(filtro)));

            return await Paginator<AgeFormasPagos, AgeFormasPagosDAO>.Paginar(Query, pageable, FromEntityToDAO);
        }



        public async Task<AgeFormasPagosDAO> Insertar(
            AgeFormasPagos ageFormasPago)
        {
            _dbContext.AgeFormasPagos.Add(ageFormasPago);
            await _dbContext.SaveChangesAsync();

            AgeFormasPagosDAO ageFormasPagoDAO = await ConsultarPorId(new AgeFormasPagosPKDAO
            {
                AgeLicencCodigo = ageFormasPago.AgeLicencCodigo,
                Codigo = ageFormasPago.Codigo
            });

            return ageFormasPagoDAO;
        }


        public async Task<List<AgeFormasPagosDAO>> InsertarVarios(
            List<AgeFormasPagos> ageFormasPagoList)
        {
            await _dbContext.AgeFormasPagos.AddRangeAsync(ageFormasPagoList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeFormasPagos, AgeFormasPagosDAO>
                .FromEntityToDAOList(ageFormasPagoList, FromEntityToDAO);
        }


        public async Task<AgeFormasPagosDAO> Actualizar(
            AgeFormasPagos ageFormasPago)
        {
            _dbContext.Entry(ageFormasPago).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();

            AgeFormasPagosDAO ageFormasPagoDAO = await ConsultarPorId(new AgeFormasPagosPKDAO
            {
                AgeLicencCodigo = ageFormasPago.AgeLicencCodigo,
                Codigo = ageFormasPago.Codigo
            });

            return ageFormasPagoDAO;
        }


        public async Task<List<AgeFormasPagosDAO>> ActualizarVarios(
            List<AgeFormasPagos> ageFormasPagoList)
        {
            _dbContext.AgeFormasPagos.UpdateRange(ageFormasPagoList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeFormasPagos, AgeFormasPagosDAO>
                .FromEntityToDAOList(ageFormasPagoList, FromEntityToDAO);
        }



        private static AgeFormasPagosDAO FromEntityToDAO(AgeFormasPagos entityObject)
        {

            if (entityObject == null)
                return null;

            return new AgeFormasPagosDAO
            {
                Id = new AgeFormasPagosPKDAO
                {
                    AgeLicencCodigo = entityObject.AgeLicencCodigo,
                    Codigo = entityObject.Codigo
                },
                Descripcion = entityObject.Descripcion,
                CodigoInstitucionControl = entityObject.CodigoInstitucionControl,
                Retiene = entityObject.Retiene,
                PresentaCajaBancos = entityObject.PresentaCajaBancos,
                Estado = entityObject.Estado
            };
        }

    }
}
