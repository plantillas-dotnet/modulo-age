﻿using AGE.Entities;
using AGE.Entities.DAO.AgeInstitucionesFinancieras;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Utils;
using AGE.Utils.Paginacion;
using Microsoft.EntityFrameworkCore;

namespace AGE.Repositories.AgeInstitucionesFinancierasRepository
{
    public class AgeInstitucionesFinancieraRepository : IAgeInstitucionesFinancieraRepository
    {

        private readonly DbContextAge _dbContext;

        public AgeInstitucionesFinancieraRepository(DbContextAge dbContext)
        {
            _dbContext = dbContext;
        }


        public async Task<Page<AgeInstitucionesFinancieraDAO>> ConsultarTodos(
            int codigoLicenciatario, 
            string codigoSegunBanco, 
            string descripcion, 
            string siglasBanco, 
            Pageable pageable)
        {
            if (_dbContext.AgeInstitucionesFinancieras == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgeInstitucionesFinancieras
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            (codigoLicenciatario == 0 || p.AgeLicencCodigo == codigoLicenciatario) &&
                            (codigoSegunBanco.Equals("") || p.CodigoSegunBanco.ToLower().Equals(codigoSegunBanco.ToLower())) &&
                            (descripcion.Equals("") || p.Descripcion.ToLower().Contains(descripcion.ToLower())) &&
                            (siglasBanco.Equals("") || p.SiglasBanco.ToLower().Contains(siglasBanco.ToLower())));

            return await Paginator<AgeInstitucionesFinancieras, AgeInstitucionesFinancieraDAO>.Paginar(Query, pageable, FromEntityToDAO);
        }


        public async Task<Page<AgeInstitucionesFinancieraDAO>> ConsultarListaFiltro(
            int codigoLicenciatario, 
            string filtro, 
            Pageable pageable)
        {
            if (_dbContext.AgeInstitucionesFinancieras == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgeInstitucionesFinancieras
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeLicencCodigo == codigoLicenciatario && (
                            p.Codigo.ToString().ToLower().Contains(filtro) ||
                            p.CodigoSegunBanco.ToLower().Contains(filtro) ||
                            p.Descripcion.ToLower().Contains(filtro) ||
                            p.SiglasBanco.ToLower().Contains(filtro)));

            return await Paginator<AgeInstitucionesFinancieras, AgeInstitucionesFinancieraDAO>.Paginar(Query, pageable, FromEntityToDAO);
        }

        public async Task<AgeInstitucionesFinancieraDAO> ConsultarPorId(
            AgeInstitucionesFinancieraPKDAO ageInstitucionesFinancieraPKDAO)
        {
            if (_dbContext.AgeInstitucionesFinancieras == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeInstitucionesFinancieras? ageInstitucionesFinanciera = await _dbContext.AgeInstitucionesFinancieras
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeLicencCodigo == ageInstitucionesFinancieraPKDAO.AgeLicencCodigo &&
                            p.Codigo == ageInstitucionesFinancieraPKDAO.Codigo).FirstOrDefaultAsync();

            return FromEntityToDAO(ageInstitucionesFinanciera);
        }


        public async Task<AgeInstitucionesFinancieras> ConsultarCompletePorId(
            AgeInstitucionesFinancieraPKDAO ageInstitucionesFinancieraPKDAO)
        {
            if (_dbContext.AgeInstitucionesFinancieras == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeInstitucionesFinancieras? ageInstitucionesFinanciera = await _dbContext.AgeInstitucionesFinancieras
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeLicencCodigo == ageInstitucionesFinancieraPKDAO.AgeLicencCodigo &&
                            p.Codigo == ageInstitucionesFinancieraPKDAO.Codigo).FirstOrDefaultAsync();

            return ageInstitucionesFinanciera;
        }

        

        

        public async Task<AgeInstitucionesFinancieraDAO> Insertar(
            AgeInstitucionesFinancieras ageInstitucionesFinanciera)
        {
            _dbContext.AgeInstitucionesFinancieras.Add(ageInstitucionesFinanciera);
            await _dbContext.SaveChangesAsync();

            AgeInstitucionesFinancieraDAO ageInstitucionesFinancieraDAO = await ConsultarPorId(new AgeInstitucionesFinancieraPKDAO
            {
                AgeLicencCodigo = ageInstitucionesFinanciera.AgeLicencCodigo,
                Codigo = ageInstitucionesFinanciera.Codigo
            });

            return ageInstitucionesFinancieraDAO;
        }


        public async Task<List<AgeInstitucionesFinancieraDAO>> InsertarVarios(
            List<AgeInstitucionesFinancieras> ageInstitucionesFinancieraList)
        {
            await _dbContext.AgeInstitucionesFinancieras.AddRangeAsync(ageInstitucionesFinancieraList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeInstitucionesFinancieras, AgeInstitucionesFinancieraDAO>
                .FromEntityToDAOList(ageInstitucionesFinancieraList, FromEntityToDAO);
        }


        public async Task<AgeInstitucionesFinancieraDAO> Actualizar(
            AgeInstitucionesFinancieras ageInstitucionesFinanciera)
        {
            _dbContext.Entry(ageInstitucionesFinanciera).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();

            AgeInstitucionesFinancieraDAO ageInstitucionesFinancieraDAO = await ConsultarPorId(new AgeInstitucionesFinancieraPKDAO
            {
                AgeLicencCodigo = ageInstitucionesFinanciera.AgeLicencCodigo,
                Codigo = ageInstitucionesFinanciera.Codigo
            });

            return ageInstitucionesFinancieraDAO;
        }


        public async Task<List<AgeInstitucionesFinancieraDAO>> ActualizarVarios(
            List<AgeInstitucionesFinancieras> ageInstitucionesFinancieraList)
        {
            _dbContext.AgeInstitucionesFinancieras.UpdateRange(ageInstitucionesFinancieraList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeInstitucionesFinancieras, AgeInstitucionesFinancieraDAO>
                .FromEntityToDAOList(ageInstitucionesFinancieraList, FromEntityToDAO);
        }


        private static AgeInstitucionesFinancieraDAO FromEntityToDAO(AgeInstitucionesFinancieras entityObject)
        {

            if (entityObject == null)
                return null;

            return new AgeInstitucionesFinancieraDAO
            {
                Id = new AgeInstitucionesFinancieraPKDAO
                {
                    AgeLicencCodigo = entityObject.AgeLicencCodigo,
                    Codigo = entityObject.Codigo
                },
                Descripcion = entityObject.Descripcion,
                CodigoSegunBanco = entityObject.CodigoSegunBanco,
                SiglasBanco = entityObject.SiglasBanco,
                Estado = entityObject.Estado
            };
        }
    }
}
