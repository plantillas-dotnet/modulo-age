﻿using AGE.Entities.DAO.AgeInstitucionesFinancieras;
using AGE.Entities;
using AGE.Utils.Paginacion;

namespace AGE.Repositories.AgeInstitucionesFinancierasRepository
{
    public interface IAgeInstitucionesFinancieraRepository
    {

        Task<Page<AgeInstitucionesFinancieraDAO>> ConsultarTodos(
            int codigoLicenciatario,
            string codigoSegunBanco,
            string descripcion,
            string siglasBanco,
            Pageable pageable);

        Task<Page<AgeInstitucionesFinancieraDAO>> ConsultarListaFiltro(
            int codigoLicenciatario,
            string filtro,
            Pageable pageable);

        Task<AgeInstitucionesFinancieraDAO> ConsultarPorId(
            AgeInstitucionesFinancieraPKDAO ageInstitucionesFinancieraPKDAO);

        Task<AgeInstitucionesFinancieras> ConsultarCompletePorId(
           AgeInstitucionesFinancieraPKDAO ageInstitucionesFinancieraPKDAO);

        Task<AgeInstitucionesFinancieraDAO> Insertar(AgeInstitucionesFinancieras ageInstitucionesFinanciera);

        Task<List<AgeInstitucionesFinancieraDAO>> InsertarVarios(
            List<AgeInstitucionesFinancieras> ageInstitucionesFinancieraList);

        Task<AgeInstitucionesFinancieraDAO> Actualizar(AgeInstitucionesFinancieras ageInstitucionesFinanciera);

        Task<List<AgeInstitucionesFinancieraDAO>> ActualizarVarios(
            List<AgeInstitucionesFinancieras> ageInstitucionesFinancieraList);

    }
}
