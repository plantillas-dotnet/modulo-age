﻿using AGE.Entities.DAO.AgeParamGeneralVigencias;
using AGE.Entities;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Utils.Paginacion;
using AGE.Utils;
using Microsoft.EntityFrameworkCore;

namespace AGE.Repositories.AgeParamGeneralVigenciasRepository
{
    public class AgeParamGeneralVigenciasRepository : IAgeParamGeneralVigenciasRepository
    {
        private readonly DbContextAge _dbContext;

        public AgeParamGeneralVigenciasRepository(DbContextAge dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Page<AgeParamGeneralVigenciasDAO>> ConsultarTodos(
            int codigoPG,
            int codigo,
            string observacion,
            string valorParametro,
            Pageable pageable)
        {
            if (_dbContext.AgeParamGeneralVigencias == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgeParamGeneralVigencias
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeParGeCodigo == codigoPG &&
                            (codigo == 0 || p.Codigo == codigo) &&
                            (observacion.Equals("") || p.Observacion.ToLower().Contains(observacion.ToLower())) &&
                            (valorParametro.Equals("") || p.ValorParametro.ToLower().Contains(valorParametro.ToLower())));

            return await Paginator<AgeParamGeneralVigencias, AgeParamGeneralVigenciasDAO>.Paginar(Query, pageable, FromEntityToDAO);
        }

        public async Task<Page<AgeParamGeneralVigenciasDAO>> ConsultarListaFiltro(
            string filtro,
            Pageable pageable)
        {
            if (_dbContext.AgeParamGeneralVigencias == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgeParamGeneralVigencias
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                               (p.AgeParGeCodigo.ToString().ToLower().Contains(filtro) ||
                                p.Codigo.ToString().ToLower().Contains(filtro) ||
                                p.Observacion.ToLower().Contains(filtro) ||
                                p.Codigo.ToString().Contains(filtro) ||
                                p.ValorParametro.ToLower().Contains(filtro)
                            ));

            return await Paginator<AgeParamGeneralVigencias, AgeParamGeneralVigenciasDAO>.Paginar(Query, pageable, FromEntityToDAO);
        }


        public async Task<AgeParamGeneralVigenciasDAO> ConsultarPorId(
            int codigoPG, 
            long id)
        {
            if (_dbContext.AgeParamGeneralVigencias == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeParamGeneralVigencias? ageParamGeneralVigencia = await _dbContext.AgeParamGeneralVigencias
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeParGeCodigo == codigoPG &&
                            p.Codigo == id).FirstOrDefaultAsync();

            return FromEntityToDAO(ageParamGeneralVigencia);
        }


        public async Task<AgeParamGeneralVigencias> ConsultarCompletePorId(AgeParamGeneralVigenciasPKDAO ageParamGeneralVigenciasPKDAO)
        {
            if (_dbContext.AgeParamGeneralVigencias == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeParamGeneralVigencias? ageParamGeneralVigencia = await _dbContext.AgeParamGeneralVigencias
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeParGeCodigo == ageParamGeneralVigenciasPKDAO.AgeParGeCodigo &&
                            p.Codigo == ageParamGeneralVigenciasPKDAO.Codigo).FirstOrDefaultAsync();

            return ageParamGeneralVigencia;
        }


        public async Task<AgeParamGeneralVigenciasDAO> Insertar(
            AgeParamGeneralVigencias ageParamGeneralVigencias)
        {
            _dbContext.AgeParamGeneralVigencias.Add(ageParamGeneralVigencias);
            await _dbContext.SaveChangesAsync();

            AgeParamGeneralVigenciasDAO ageParamGeneralVigenciasDAO = await ConsultarPorId(
                ageParamGeneralVigencias.AgeParGeCodigo, ageParamGeneralVigencias.Codigo);

            return ageParamGeneralVigenciasDAO;
        }

        public async Task<List<AgeParamGeneralVigenciasDAO>> InsertarVarios(
            List<AgeParamGeneralVigencias> ageParamGeneralVigenciasList)
        {
            await _dbContext.AgeParamGeneralVigencias.AddRangeAsync(ageParamGeneralVigenciasList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeParamGeneralVigencias, AgeParamGeneralVigenciasDAO>
                .FromEntityToDAOList(ageParamGeneralVigenciasList, FromEntityToDAO);
        }


        public async Task<AgeParamGeneralVigenciasDAO> Actualizar(AgeParamGeneralVigencias ageParamGeneralVigencias)
        {
            _dbContext.Entry(ageParamGeneralVigencias).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();

            AgeParamGeneralVigenciasDAO ageParamGeneralVigenciasDAO = await ConsultarPorId(
                ageParamGeneralVigencias.AgeParGeCodigo, ageParamGeneralVigencias.Codigo);

            return ageParamGeneralVigenciasDAO;
        }


        public async Task<List<AgeParamGeneralVigenciasDAO>> ActualizarVarios(List<AgeParamGeneralVigencias> ageParamGeneralVigenciasList)
        {
            _dbContext.AgeParamGeneralVigencias.UpdateRange(ageParamGeneralVigenciasList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeParamGeneralVigencias, AgeParamGeneralVigenciasDAO>
                .FromEntityToDAOList(ageParamGeneralVigenciasList, FromEntityToDAO);
        }


        private static AgeParamGeneralVigenciasDAO FromEntityToDAO(AgeParamGeneralVigencias entityObject)
        {

            if (entityObject == null)
            {
                return null;
            }

            return new AgeParamGeneralVigenciasDAO
            {
                Id = new AgeParamGeneralVigenciasPKDAO
                {
                    AgeParGeCodigo = entityObject.AgeParGeCodigo,
                    Codigo = entityObject.Codigo
                },
                Observacion = entityObject.Observacion,
                FechaDesde = entityObject.FechaDesde,
                FechaHasta = entityObject.FechaHasta,
                ValorParametro = entityObject.ValorParametro,
                Estado = entityObject.Estado
            };
        }
    }
}
