﻿using AGE.Entities;
using AGE.Entities.DAO.AgeParamGeneralVigencias;
using AGE.Utils.Paginacion;

namespace AGE.Repositories.AgeParamGeneralVigenciasRepository
{
    public interface IAgeParamGeneralVigenciasRepository
    {
        Task<Page<AgeParamGeneralVigenciasDAO>> ConsultarTodos(
           int codigoPG,
           int codigo,
           string observacion,
           string valorParametro,
           Pageable pageable);
        Task<Page<AgeParamGeneralVigenciasDAO>> ConsultarListaFiltro(
            string filtro,
            Pageable pageable);
        Task<AgeParamGeneralVigenciasDAO> ConsultarPorId(
            int codigoPG,
            long id);
        Task<AgeParamGeneralVigencias> ConsultarCompletePorId(
            AgeParamGeneralVigenciasPKDAO ageParamGeneralVigenciasPKDAO);
        Task<AgeParamGeneralVigenciasDAO> Insertar(
            AgeParamGeneralVigencias ageParamGeneralVigencias);
        Task<List<AgeParamGeneralVigenciasDAO>> InsertarVarios(
            List<AgeParamGeneralVigencias> ageParamGeneralVigenciasList);
        Task<AgeParamGeneralVigenciasDAO> Actualizar(
            AgeParamGeneralVigencias ageParamGeneralVigencias);
        Task<List<AgeParamGeneralVigenciasDAO>> ActualizarVarios(
            List<AgeParamGeneralVigencias> ageParamGeneralVigenciasList);

    }
}
