﻿using AGE.Entities;
using AGE.Entities.DAO.AgeLicenciatariosSecuencias;
using AGE.Utils.Paginacion;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AGE.Repositories.AgeLicenciatariosSecuenciasRepository
{
    public interface IAgeLicenciatariosSecuenciasRepository
    {
        Task<Page<AgeLicenciatariosSecuenciasDAO>> ConsultarTodos(
            int codigoLicenciatario, 
            int codigo,
            string ciclica,
            string descripcion, 
            Pageable pageable);
        Task<Page<AgeLicenciatariosSecuenciasDAO>> ConsultarListaFiltro(
            int codigoLicenciatario,
            string filtro, 
            Pageable pageable);
        Task<AgeLicenciatariosSecuencia> ConsultarPorSecuencia(
            AgeLicenciatariosSecuenciasPKDAO ageLicenciatariosSecuenciasPKDAO);
        Task<AgeLicenciatariosSecuenciasDAO> ConsultarPorId(
            AgeLicenciatariosSecuenciasPKDAO ageLicenciatariosSecuenciasPKDAO);
        Task<AgeLicenciatariosSecuenciasDAO> Insertar(
            AgeLicenciatariosSecuencia ageLicenciatariosSecuencias);
        Task<List<AgeLicenciatariosSecuenciasDAO>> InsertarVarios(
            List<AgeLicenciatariosSecuencia> ageLicenciatariosSecuenciasList);
        Task<AgeLicenciatariosSecuenciasDAO> Actualizar(
            AgeLicenciatariosSecuencia ageLicenciatariosSecuencias);
        Task<List<AgeLicenciatariosSecuenciasDAO>> ActualizarVarios(
            List<AgeLicenciatariosSecuencia> ageLicenciatariosSecuenciasList);
        void ActualizarSecuencia(
            AgeLicenciatariosSecuencia ageLicenciatariosSecuencias);
    }
}
