﻿using Microsoft.EntityFrameworkCore;
using AGE.Entities;
using AGE.Entities.DAO.AgeLicenciatariosSecuencias;
using AGE.Utils;
using AGE.Utils.Paginacion;
using AGE.Middleware.Exceptions.NotFound;
using Microsoft.EntityFrameworkCore.Storage;

namespace AGE.Repositories.AgeLicenciatariosSecuenciasRepository
{
    public class AgeLicenciatariosSecuenciasRepository : IAgeLicenciatariosSecuenciasRepository
    {

        private readonly DbContextAge _dbContext;
        public AgeLicenciatariosSecuenciasRepository(DbContextAge dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Page<AgeLicenciatariosSecuenciasDAO>> ConsultarTodos(
            int codigoLicenciatario,
            int codigo,
            string ciclica,
            string descripcion,
            Pageable pageable)
        {
            if (_dbContext.AgeLicenciatariosSecuencias == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgeLicenciatariosSecuencias
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeLicencCodigo == codigoLicenciatario &&
                            (codigo == 0 || p.Codigo == codigo) &&
                            (ciclica.Equals("") || p.Ciclica.ToLower().Equals(ciclica.ToLower())) &&
                            (descripcion.Equals("") || p.Descripcion.ToLower().Contains(descripcion.ToLower())));

            return await Paginator<AgeLicenciatariosSecuencia, AgeLicenciatariosSecuenciasDAO>
                .Paginar(Query, pageable, FromEntityToDAO);
        }


        public async Task<Page<AgeLicenciatariosSecuenciasDAO>> ConsultarListaFiltro(
            int codigoLicenciatario,
            string filtro,
            Pageable pageable)
        {
            if (_dbContext.AgeLicenciatariosSecuencias == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgeLicenciatariosSecuencias
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeLicencCodigo == codigoLicenciatario && (
                            p.Codigo.ToString().ToLower().Contains(filtro) ||
                            p.Ciclica.ToLower().Contains(filtro) ||
                            p.Descripcion.ToLower().Contains(filtro)));

            return await Paginator<AgeLicenciatariosSecuencia, AgeLicenciatariosSecuenciasDAO>
                .Paginar(Query, pageable, FromEntityToDAO);
        }


        public async Task<AgeLicenciatariosSecuencia> ConsultarPorSecuencia(
            AgeLicenciatariosSecuenciasPKDAO ageLicenciatariosSecuenciasPKDAO)
        {
            if (_dbContext.AgeLicenciatariosSecuencias == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeLicenciatariosSecuencia? ageLicenciatariosSecuencias = await _dbContext.AgeLicenciatariosSecuencias
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeLicencCodigo == ageLicenciatariosSecuenciasPKDAO.ageLicencCodigo &&
                            p.Codigo == ageLicenciatariosSecuenciasPKDAO.codigo).FirstOrDefaultAsync();

            return ageLicenciatariosSecuencias;
        }


        public async Task<AgeLicenciatariosSecuenciasDAO> ConsultarPorId(
            AgeLicenciatariosSecuenciasPKDAO ageLicenciatariosSecuenciasPKDAO)
        {
            if (_dbContext.AgeLicenciatariosSecuencias == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeLicenciatariosSecuencia? ageLicenciatariosSecuencias = await _dbContext.AgeLicenciatariosSecuencias
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeLicencCodigo == ageLicenciatariosSecuenciasPKDAO.ageLicencCodigo &&
                            p.Codigo == ageLicenciatariosSecuenciasPKDAO.codigo).FirstOrDefaultAsync();

            return FromEntityToDAO(ageLicenciatariosSecuencias);
        }


        public async Task<AgeLicenciatariosSecuenciasDAO> Insertar(AgeLicenciatariosSecuencia ageLicenciatariosSecuencias)
        {

            _dbContext.AgeLicenciatariosSecuencias.Add(ageLicenciatariosSecuencias);
            await _dbContext.SaveChangesAsync();

            AgeLicenciatariosSecuenciasDAO ageLicenciatariosSecuenciasDAO = await ConsultarPorId(new AgeLicenciatariosSecuenciasPKDAO
            {
                ageLicencCodigo = ageLicenciatariosSecuencias.AgeLicencCodigo,
                codigo = ageLicenciatariosSecuencias.Codigo
            });

            return ageLicenciatariosSecuenciasDAO;
        }


        public async Task<List<AgeLicenciatariosSecuenciasDAO>> InsertarVarios(List<AgeLicenciatariosSecuencia> ageLicenciatariosSecuenciasList)
        {
            await _dbContext.AgeLicenciatariosSecuencias.AddRangeAsync(ageLicenciatariosSecuenciasList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeLicenciatariosSecuencia, AgeLicenciatariosSecuenciasDAO>
                .FromEntityToDAOList(ageLicenciatariosSecuenciasList, FromEntityToDAO);
        }


        public async Task<AgeLicenciatariosSecuenciasDAO> Actualizar(AgeLicenciatariosSecuencia ageLicenciatariosSecuencias)
        {

            _dbContext.Entry(ageLicenciatariosSecuencias).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();

            AgeLicenciatariosSecuenciasDAO ageLicenciatariosSecuenciasDAO = await ConsultarPorId(new AgeLicenciatariosSecuenciasPKDAO
            {
                ageLicencCodigo = ageLicenciatariosSecuencias.AgeLicencCodigo,
                codigo = ageLicenciatariosSecuencias.Codigo
            });

            return ageLicenciatariosSecuenciasDAO;
        }


        public async Task<List<AgeLicenciatariosSecuenciasDAO>> ActualizarVarios(List<AgeLicenciatariosSecuencia> ageLicenciatariosSecuenciasList)
        {
            _dbContext.AgeLicenciatariosSecuencias.UpdateRange(ageLicenciatariosSecuenciasList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeLicenciatariosSecuencia, AgeLicenciatariosSecuenciasDAO>
                .FromEntityToDAOList(ageLicenciatariosSecuenciasList, FromEntityToDAO);
        }


        public void ActualizarSecuencia(AgeLicenciatariosSecuencia ageLicenciatariosSecuencias)
        {
            using (var dbContext = new DbContextAge())
            {
                dbContext.Entry(ageLicenciatariosSecuencias).State = EntityState.Modified;
                dbContext.SaveChanges();
            }
        }

        private AgeLicenciatariosSecuenciasDAO FromEntityToDAO(AgeLicenciatariosSecuencia entityObject)
        {

            if (entityObject == null)
            {
                return null;
            }

            return new AgeLicenciatariosSecuenciasDAO
            {
                Id = new AgeLicenciatariosSecuenciasPKDAO
                {
                    ageLicencCodigo = entityObject.AgeLicencCodigo,
                    codigo = entityObject.Codigo
                },
                Descripcion = entityObject.Descripcion,
                ValorInicial = entityObject.ValorInicial,
                IncrementaEn = entityObject.IncrementaEn,
                ValorActual = entityObject.ValorActual,
                Ciclica = entityObject.Ciclica,
                Estado = entityObject.Estado
            };

        }

    }
}
