﻿using AGE.Entities.DAO.AgeClasesContribuyentes;
using AGE.Entities;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Utils.Paginacion;
using AGE.Utils;
using Microsoft.EntityFrameworkCore;

namespace AGE.Repositories.AgeClasesContribuyentesRepository
{
    public class AgeClasesContribuyentesRepository : IAgeClasesContribuyentesRepository
    {
        private readonly DbContextAge _dbContext;

        public AgeClasesContribuyentesRepository(DbContextAge dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Page<AgeClasesContribuyentesDAO>> ConsultarTodos(
            string descripcion,
            Pageable pageable)
        {
            if (_dbContext.AgeClasesContribuyentes == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgeClasesContribuyentes
                .Where(p => p.Estado != Globales.ESTADO_ANULADO && 
                            (descripcion.Equals("") || p.Descripcion.ToLower().Contains(descripcion.ToLower())));

            return await Paginator<AgeClasesContribuyentes, AgeClasesContribuyentesDAO>.Paginar(Query, pageable, FromEntityToDAO);
        }

        public async Task<Page<AgeClasesContribuyentesDAO>> ConsultarListaFiltro(
            string filtro,
            Pageable pageable)
        {
            if (_dbContext.AgeClasesContribuyentes == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgeClasesContribuyentes
                .Where(p => p.Estado != Globales.ESTADO_ANULADO && (
                            p.Codigo.ToString().ToLower().Contains(filtro.ToLower()) ||
                            p.Descripcion.ToLower().Contains(filtro.ToLower())));

            return await Paginator<AgeClasesContribuyentes, AgeClasesContribuyentesDAO>.Paginar(Query, pageable, FromEntityToDAO);
        }

        public async Task<AgeClasesContribuyentesDAO> ConsultarPorId(long codigo)
        {
            if (_dbContext.AgeClasesContribuyentes == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeClasesContribuyentes? ageClasesContribuyentes = await _dbContext.AgeClasesContribuyentes
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                p.Codigo == codigo).FirstOrDefaultAsync();

            return FromEntityToDAO(ageClasesContribuyentes);
        }

        public async Task<AgeClasesContribuyentes> ConsultarCompletePorId(long codigo)
        {
            if (_dbContext.AgeClasesContribuyentes == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeClasesContribuyentes? ageClasesContribuyentes = await _dbContext.AgeClasesContribuyentes
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                p.Codigo == codigo).FirstOrDefaultAsync();

            return ageClasesContribuyentes;
        }

        public async Task<AgeClasesContribuyentesDAO> Insertar(AgeClasesContribuyentes ageClasesContribuyentes)
        {
            _dbContext.AgeClasesContribuyentes.Add(ageClasesContribuyentes);
            await _dbContext.SaveChangesAsync();

            AgeClasesContribuyentesDAO ageClasesContribuyentesDAO = await ConsultarPorId(ageClasesContribuyentes.Codigo);

            return ageClasesContribuyentesDAO;
        }

        public async Task<AgeClasesContribuyentesDAO> Actualizar(AgeClasesContribuyentes ageClasesContribuyentes)
        {
            _dbContext.Entry(ageClasesContribuyentes).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();

            AgeClasesContribuyentesDAO ageClasesContribuyentesDAO = await ConsultarPorId(ageClasesContribuyentes.Codigo);

            return ageClasesContribuyentesDAO;
        }

        public async Task<List<AgeClasesContribuyentesDAO>> InsertarVarios(List<AgeClasesContribuyentes> ageClasesContribuyentesList)
        {
            await _dbContext.AgeClasesContribuyentes.AddRangeAsync(ageClasesContribuyentesList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeClasesContribuyentes, AgeClasesContribuyentesDAO>
                .FromEntityToDAOList(ageClasesContribuyentesList, FromEntityToDAO);
        }

        public async Task<List<AgeClasesContribuyentesDAO>> ActualizarVarios(List<AgeClasesContribuyentes> ageClasesContribuyentesList)
        {
            _dbContext.AgeClasesContribuyentes.UpdateRange(ageClasesContribuyentesList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeClasesContribuyentes, AgeClasesContribuyentesDAO>
                .FromEntityToDAOList(ageClasesContribuyentesList, FromEntityToDAO);
        }

        private AgeClasesContribuyentesDAO FromEntityToDAO(AgeClasesContribuyentes entityObject)
        {
            AgeClasesContribuyentesDAO DAOobject = null;

            if (entityObject != null)
            {
                DAOobject = new AgeClasesContribuyentesDAO
                {
                    Estado = entityObject.Estado,
                    Codigo = entityObject.Codigo,
                    Descripcion = entityObject.Descripcion
                };
            }

            return DAOobject;

        }
    }
}
