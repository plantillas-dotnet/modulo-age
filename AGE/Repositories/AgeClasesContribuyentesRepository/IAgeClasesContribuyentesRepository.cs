﻿using AGE.Entities.DAO.AgeClasesContribuyentes;
using AGE.Entities;
using AGE.Utils.Paginacion;

namespace AGE.Repositories.AgeClasesContribuyentesRepository
{
    public interface IAgeClasesContribuyentesRepository
    {
        Task<Page<AgeClasesContribuyentesDAO>> ConsultarTodos(
            string descripcion,
            Pageable pageable);

        Task<AgeClasesContribuyentesDAO> ConsultarPorId(
            long codigo);

        Task<AgeClasesContribuyentes> ConsultarCompletePorId(
            long codigo);

        Task<Page<AgeClasesContribuyentesDAO>> ConsultarListaFiltro(
            string filtro,
            Pageable pageable);

        Task<AgeClasesContribuyentesDAO> Insertar(
            AgeClasesContribuyentes ageClasesContribuyentes);

        Task<AgeClasesContribuyentesDAO> Actualizar(
            AgeClasesContribuyentes ageClasesContribuyentes);

        Task<List<AgeClasesContribuyentesDAO>> InsertarVarios(
            List<AgeClasesContribuyentes> ageClasesContribuyentesList);

        Task<List<AgeClasesContribuyentesDAO>> ActualizarVarios(
            List<AgeClasesContribuyentes> ageClasesContribuyentesList);
    }
}
