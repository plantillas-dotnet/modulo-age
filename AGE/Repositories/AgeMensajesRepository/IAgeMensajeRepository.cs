﻿
using AGE.Entities;
using AGE.Utils.Paginacion;
using AGE.Entities.DAO.AgeMensajes;

namespace AGE.Repositories.AgeMensajesRepository
{
    public interface IAgeMensajeRepository
    {
        Task<Page<AgeMensajeDAO>> ConsultarTodos(
            long codigo,
            string accionMsg,
            Pageable pageable);

        Task<AgeMensajeDAO> ConsultarPorId(
            long codigo);

        Task<AgeMensajes> ConsultarCompletePorId(
            long codigo);

        Task<Page<AgeMensajeDAO>> ConsultarListaFiltro(
            string filtro,
            Pageable pageable);

        Task<AgeMensajeDAO> Insertar(
            AgeMensajes ageMensaje);

        Task<AgeMensajeDAO> Actualizar(
            AgeMensajes ageMensaje);

        Task<List<AgeMensajeDAO>> InsertarVarios(
            List<AgeMensajes> ageMensajeList);

        Task<List<AgeMensajeDAO>> ActualizarVarios(
            List<AgeMensajes> ageMensajeList);
    }
}
