﻿using AGE.Entities;
using AGE.Entities.DAO.AgeMensajes;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Utils;
using AGE.Utils.Paginacion;
using Microsoft.EntityFrameworkCore;

namespace AGE.Repositories.AgeMensajesRepository
{
    public class AgeMensajeRepository : IAgeMensajeRepository
    {

        private readonly DbContextAge _dbContext;

        public AgeMensajeRepository(DbContextAge dbContext)
        {
            _dbContext = dbContext;
        }


        public async Task<Page<AgeMensajeDAO>> ConsultarTodos(
            long codigo, 
            string accionMsg, 
            Pageable pageable)
        {
            if (_dbContext.AgeMensajes == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgeMensajes
                .Where(p => p.Estado != Globales.ESTADO_ANULADO && (codigo == 0 || p.Codigo == codigo) &&
                            (accionMsg.Equals("") || p.AccionMsg.ToLower().Contains(accionMsg.ToLower())));

            return await Paginator<AgeMensajes, AgeMensajeDAO>.Paginar(Query, pageable, FromEntityToDAO);
        }


        public async Task<Page<AgeMensajeDAO>> ConsultarListaFiltro(
            string filtro, 
            Pageable pageable)
        {
            if (_dbContext.AgeMensajes == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgeMensajes
                .Where(p => p.Estado != Globales.ESTADO_ANULADO && (
                            p.Codigo.ToString().ToLower().Contains(filtro) ||
                            p.AccionMsg.ToLower().Contains(filtro)));

            return await Paginator<AgeMensajes, AgeMensajeDAO>.Paginar(Query, pageable, FromEntityToDAO);
        }


        public async Task<AgeMensajeDAO> ConsultarPorId(long codigo)
        {
            if (_dbContext.AgeMensajes == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeMensajes? ageMensaje = await _dbContext.AgeMensajes
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                p.Codigo == codigo).FirstOrDefaultAsync();

            return FromEntityToDAO(ageMensaje);
        }


        public async Task<AgeMensajes> ConsultarCompletePorId(long codigo)
        {
            if (_dbContext.AgeMensajes == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeMensajes? ageMensaje = await _dbContext.AgeMensajes
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                p.Codigo == codigo).FirstOrDefaultAsync();

            return ageMensaje;
        }

        

        public async Task<AgeMensajeDAO> Insertar(AgeMensajes ageMensaje)
        {
            _dbContext.AgeMensajes.Add(ageMensaje);
            await _dbContext.SaveChangesAsync();

            AgeMensajeDAO ageMensajeDAO = await ConsultarPorId(ageMensaje.Codigo);

            return ageMensajeDAO;
        }


        public async Task<List<AgeMensajeDAO>> InsertarVarios(List<AgeMensajes> ageMensajeList)
        {
            await _dbContext.AgeMensajes.AddRangeAsync(ageMensajeList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeMensajes, AgeMensajeDAO>
                .FromEntityToDAOList(ageMensajeList, FromEntityToDAO);
        }


        public async Task<AgeMensajeDAO> Actualizar(AgeMensajes ageMensaje)
        {
            _dbContext.Entry(ageMensaje).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();

            AgeMensajeDAO ageMensajeDAO = await ConsultarPorId(ageMensaje.Codigo);

            return ageMensajeDAO;
        }


        public async Task<List<AgeMensajeDAO>> ActualizarVarios(List<AgeMensajes> ageMensajeList)
        {
            _dbContext.AgeMensajes.UpdateRange(ageMensajeList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeMensajes, AgeMensajeDAO>
                .FromEntityToDAOList(ageMensajeList, FromEntityToDAO);
        }



        private AgeMensajeDAO FromEntityToDAO(AgeMensajes entityObject)
        {
            AgeMensajeDAO DAOobject = null;

            if (entityObject != null)
            {
                DAOobject = new AgeMensajeDAO
                {
                    Estado = entityObject.Estado,
                    Codigo = entityObject.Codigo,
                    AccionMsg = entityObject.AccionMsg,
                };
            }

            return DAOobject;

        }


    }
}
