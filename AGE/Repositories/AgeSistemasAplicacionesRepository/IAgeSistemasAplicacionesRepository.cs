﻿using AGE.Entities.DAO.AgeSistemasAplicaciones;
using AGE.Entities;
using AGE.Utils.Paginacion;

namespace AGE.Repositories.AgeSistemasAplicacionesRepository
{
    public interface IAgeSistemasAplicacionesRepository
    {
        Task<Page<AgeSistemasAplicacionesDAO>> ConsultarTodos(
            int ageAplicaCodigo,
            int ageSistemCodigo,
            DateTime fechaDesde,
            DateTime fechaHasta,
            Pageable pageable);

        Task<AgeSistemasAplicacionesDAO> ConsultarPorId(AgeSistemasAplicacionesPKDAO ageSistemasAplicacionesPKDAO);

        Task<Page<AgeSistemasAplicacionesDAO>> ConsultarListaFiltro(
            string filtro,
            Pageable pageable);

        Task<AgeSistemasAplicaciones> ConsultarCompletePorId(
            AgeSistemasAplicacionesPKDAO ageSistemasAplicacionesPKDAO);

        Task<AgeSistemasAplicacionesDAO> Insertar(AgeSistemasAplicaciones AgeSistemasAplicaciones);

        Task<List<AgeSistemasAplicacionesDAO>> InsertarVarios(
            List<AgeSistemasAplicaciones> AgeSistemasAplicacionesList);

        Task<AgeSistemasAplicacionesDAO> Actualizar(AgeSistemasAplicaciones AgeSistemasAplicaciones);

        Task<List<AgeSistemasAplicacionesDAO>> ActualizarVarios(
            List<AgeSistemasAplicaciones> AgeSistemasAplicacionesList);
    }
}
