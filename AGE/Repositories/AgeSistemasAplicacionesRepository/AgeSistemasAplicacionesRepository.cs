﻿
using AGE.Entities;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Utils.Paginacion;
using AGE.Utils;
using Microsoft.EntityFrameworkCore;
using AGE.Entities.DAO.AgeSistemasAplicaciones;

namespace AGE.Repositories.AgeSistemasAplicacionesRepository
{
    public class AgeSistemasAplicacionesRepository : IAgeSistemasAplicacionesRepository
    {
        private readonly DbContextAge _dbContext;

        public AgeSistemasAplicacionesRepository(DbContextAge dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Page<AgeSistemasAplicacionesDAO>> ConsultarTodos(int ageAplicaCodigo,
                                                                            int ageSistemCodigo,
                                                                            DateTime fechaDesde,
                                                                            DateTime fechaHasta,
                                                                            Pageable pageable)
        {
            if (_dbContext.AgeSistemasAplicaciones == null)
            {
                throw new RegisterNotFoundException();
            }
            var defaultDateTime = new DateTime(1, 1, 1); // Valor predeterminado

            var Query = _dbContext.AgeSistemasAplicaciones
                .Where(p => p.Estado != Globales.ESTADO_ANULADO && 
                (p.AgeSistemCodigo == ageSistemCodigo) &&
                    (ageAplicaCodigo == 0 || p.AgeAplicaCodigo == ageAplicaCodigo) &&
                    (fechaDesde == null || fechaDesde == defaultDateTime || p.FechaDesde == fechaDesde) &&
                    (fechaHasta == null || fechaHasta == defaultDateTime || p.FechaHasta == fechaHasta)); 

            return await Paginator<AgeSistemasAplicaciones, AgeSistemasAplicacionesDAO>.Paginar(Query, pageable, FromEntityToDAO);
        }

        public async Task<Page<AgeSistemasAplicacionesDAO>> ConsultarListaFiltro(string filtro,
                                                                                 Pageable pageable)
        {
            if (_dbContext.AgeSistemasAplicaciones == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgeSistemasAplicaciones
                .Where(p => p.Estado != Globales.ESTADO_ANULADO && (
                            p.AgeAplicaCodigo.ToString().ToLower().Contains(filtro) ||
                            p.AgeSistemCodigo.ToString().ToLower().Contains(filtro)));

            return await Paginator<AgeSistemasAplicaciones, AgeSistemasAplicacionesDAO>.Paginar(Query, pageable, FromEntityToDAO);
        }


        public async Task<AgeSistemasAplicacionesDAO> ConsultarPorId(AgeSistemasAplicacionesPKDAO ageSistemasAplicacionesPKDAO)
        {
            if (_dbContext.AgeSistemasAplicaciones == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeSistemasAplicaciones? ageSistemasAplicacione = await _dbContext.AgeSistemasAplicaciones
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeAplicaCodigo == ageSistemasAplicacionesPKDAO.AgeAplicaCodigo &&
                            p.AgeSistemCodigo == ageSistemasAplicacionesPKDAO.AgeSistemCodigo).FirstOrDefaultAsync();

            return FromEntityToDAO(ageSistemasAplicacione);
        }

        public async Task<AgeSistemasAplicaciones> ConsultarCompletePorId(AgeSistemasAplicacionesPKDAO ageSistemasAplicacionesPKDAO)
        {
            if (_dbContext.AgeSistemasAplicaciones == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeSistemasAplicaciones? ageSistemasAplicacione = await _dbContext.AgeSistemasAplicaciones
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeAplicaCodigo == ageSistemasAplicacionesPKDAO.AgeAplicaCodigo &&
                            p.AgeSistemCodigo == ageSistemasAplicacionesPKDAO.AgeSistemCodigo).FirstOrDefaultAsync();

            return ageSistemasAplicacione;
        }


        public async Task<AgeSistemasAplicacionesDAO> Insertar(
            AgeSistemasAplicaciones ageSistemasAplicaciones)
        {
            _dbContext.AgeSistemasAplicaciones.Add(ageSistemasAplicaciones);
            await _dbContext.SaveChangesAsync();

            AgeSistemasAplicacionesDAO ageSistemasAplicacionesDAO = await ConsultarPorId(new AgeSistemasAplicacionesPKDAO
            {
                AgeAplicaCodigo = ageSistemasAplicaciones.AgeAplicaCodigo,
                AgeSistemCodigo = ageSistemasAplicaciones.AgeSistemCodigo
            });

            return ageSistemasAplicacionesDAO;
        }

        public async Task<List<AgeSistemasAplicacionesDAO>> InsertarVarios(
            List<AgeSistemasAplicaciones> ageSistemasAplicacionesList)
        {
            await _dbContext.AgeSistemasAplicaciones.AddRangeAsync(ageSistemasAplicacionesList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeSistemasAplicaciones, AgeSistemasAplicacionesDAO>
                .FromEntityToDAOList(ageSistemasAplicacionesList, FromEntityToDAO);
        }


        public async Task<AgeSistemasAplicacionesDAO> Actualizar(AgeSistemasAplicaciones ageSistemasAplicaciones)
        {
            _dbContext.Entry(ageSistemasAplicaciones).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();

            AgeSistemasAplicacionesDAO ageSistemasAplicacionesDAO = await ConsultarPorId(new AgeSistemasAplicacionesPKDAO
            {
                AgeAplicaCodigo = ageSistemasAplicaciones.AgeAplicaCodigo,
                AgeSistemCodigo = ageSistemasAplicaciones.AgeSistemCodigo
            });

            return ageSistemasAplicacionesDAO;
        }


        public async Task<List<AgeSistemasAplicacionesDAO>> ActualizarVarios(List<AgeSistemasAplicaciones> ageSistemasAplicacionesList)
        {
            _dbContext.AgeSistemasAplicaciones.UpdateRange(ageSistemasAplicacionesList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeSistemasAplicaciones, AgeSistemasAplicacionesDAO>
                .FromEntityToDAOList(ageSistemasAplicacionesList, FromEntityToDAO);
        }


        private static AgeSistemasAplicacionesDAO FromEntityToDAO(AgeSistemasAplicaciones entityObject)
        {

            if (entityObject == null)
            {
                return null;
            }

            return new AgeSistemasAplicacionesDAO
            {
                Id = new AgeSistemasAplicacionesPKDAO
                {
                    AgeAplicaCodigo = entityObject.AgeAplicaCodigo,
                    AgeSistemCodigo = entityObject.AgeSistemCodigo
                },
                Estado = entityObject.Estado,
                FechaDesde = entityObject.FechaDesde,
                FechaHasta = entityObject.FechaHasta
            };
        }
    }
}
