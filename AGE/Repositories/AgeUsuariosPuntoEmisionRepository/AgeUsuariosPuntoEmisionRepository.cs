﻿//using AGE.Entities.DAO.AgePuntoEmision;
using AGE.Entities.DAO.AgeUsuarios;
using AGE.Entities.DAO.AgeUsuariosPuntoEmision;
using AGE.Entities;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Utils.Paginacion;
using AGE.Utils;
using Microsoft.EntityFrameworkCore;

namespace AGE.Repositories.AgeUsuariosPuntoEmisionRepository
{
    public class AgeUsuariosPuntoEmisionRepository : IAgeUsuariosPuntoEmisionRepository
    {
        private readonly DbContextAge _dbContext;

        public AgeUsuariosPuntoEmisionRepository(DbContextAge dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Page<AgeUsuariosPuntoEmisionDAO>> ConsultarTodos(int AgageSucursAgeLicencCodigo,
                                                                            int agePunEmCodigo,
                                                                            int agePunEmAgeSucursCodigo,
                                                                            long ageUsuariCodigo, 
                                                                            Pageable pageable)
        {
            if (_dbContext.AgeUsuariosPuntoEmisions == null)
            {
                throw new RegisterNotFoundException();
            }

            var query = _dbContext.AgeUsuariosPuntoEmisions
                .Where(a => a.Estado != Globales.ESTADO_ANULADO &&
                            a.AgageSucursAgeLicencCodigo == AgageSucursAgeLicencCodigo &&
                            (agePunEmCodigo == 0 || a.AgePunEmCodigo == agePunEmCodigo) &&
                            (agePunEmAgeSucursCodigo == 0 || a.AgePunEmAgeSucursCodigo == agePunEmAgeSucursCodigo) &&
                            (ageUsuariCodigo == 0 || a.AgeUsuariCodigo == ageUsuariCodigo));

            return await Paginator<AgeUsuariosPuntoEmision, AgeUsuariosPuntoEmisionDAO>.Paginar(query, pageable, FromEntityToDAO);
        }

        public async Task<AgeUsuariosPuntoEmisionDAO> ConsultarPorId(int AgageSucursAgeLicencCodigo,
                                                                    int agePunEmCodigo,
                                                                    int agePunEmAgeSucursCodigo,
                                                                    long ageUsuariCodigo)
        {
            if (_dbContext.AgeUsuariosPuntoEmisions == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeUsuariosPuntoEmision? ageUsuariosPuntoEmision = await _dbContext.AgeUsuariosPuntoEmisions
                            .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgageSucursAgeLicencCodigo == AgageSucursAgeLicencCodigo &&
                            p.AgeUsuariAgeLicencCodigo == AgageSucursAgeLicencCodigo &&
                            p.AgePunEmCodigo == agePunEmCodigo &&
                            p.AgePunEmAgeSucursCodigo == agePunEmAgeSucursCodigo &&
                            p.AgeUsuariCodigo == ageUsuariCodigo).FirstOrDefaultAsync();

            return FromEntityToDAO(ageUsuariosPuntoEmision);
        }

        public async Task<Page<AgeUsuariosPuntoEmisionDAO>> ConsultarListaFiltro(int AgeUsuariAgeLicencCodigo, string filtro, Pageable pageable)
        {
            if (_dbContext.AgeUsuariosPuntoEmisions == null)
            {
                throw new RegisterNotFoundException();
            }

            var query = _dbContext.AgeUsuariosPuntoEmisions
                .Where(a => a.Estado != Globales.ESTADO_ANULADO &&
                            a.AgeUsuariAgeLicencCodigo == AgeUsuariAgeLicencCodigo &&
                            a.Ag.AgeSucurs.Descripcion.ToUpper().Contains(filtro.ToUpper()) ||
                            a.AgeUsuari.Nombres.ToUpper().Contains(filtro.ToUpper()) ||
                            a.AgeUsuari.Apellidos.ToUpper().Contains(filtro.ToUpper()) ||
                            a.AgeUsuari.NumeroIdentificacion.ToUpper().Contains(filtro.ToUpper()));

            return await Paginator<AgeUsuariosPuntoEmision, AgeUsuariosPuntoEmisionDAO>.Paginar(query, pageable, FromEntityToDAO);
        }

        public async Task<AgeUsuariosPuntoEmisionDAO> Insertar(AgeUsuariosPuntoEmision ageUsuariosPuntoEmision)
        {
            _dbContext.AgeUsuariosPuntoEmisions.Add(ageUsuariosPuntoEmision);
            await _dbContext.SaveChangesAsync();

            AgeUsuariosPuntoEmisionDAO ageUsuarioPuntoEmisionDAO = await ConsultarPorId(ageUsuariosPuntoEmision.AgageSucursAgeLicencCodigo,
                                                                                        ageUsuariosPuntoEmision.AgePunEmCodigo,
                                                                                        ageUsuariosPuntoEmision.AgePunEmAgeSucursCodigo,
                                                                                        ageUsuariosPuntoEmision.AgeUsuariCodigo);

            return ageUsuarioPuntoEmisionDAO;
        }

        public async Task<List<AgeUsuariosPuntoEmisionDAO>> InsertarVarios(List<AgeUsuariosPuntoEmision> ageUsuariosPuntoEmisionList)
        {
            await _dbContext.AgeUsuariosPuntoEmisions.AddRangeAsync(ageUsuariosPuntoEmisionList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeUsuariosPuntoEmision, AgeUsuariosPuntoEmisionDAO>
                .FromEntityToDAOList(ageUsuariosPuntoEmisionList, FromEntityToDAO);
        }

        public async Task<AgeUsuariosPuntoEmision> ConsultarCompletePorId(int AgageSucursAgeLicencCodigo,
                                                                        int agePunEmCodigo,
                                                                        int agePunEmAgeSucursCodigo,
                                                                        long ageUsuariCodigo)
        {
            if (_dbContext.AgeUsuariosPuntoEmisions == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeUsuariosPuntoEmision? ageUsuariosPuntoEmision = await _dbContext.AgeUsuariosPuntoEmisions
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgageSucursAgeLicencCodigo == AgageSucursAgeLicencCodigo &&
                            p.AgeUsuariAgeLicencCodigo == AgageSucursAgeLicencCodigo &&
                            p.AgePunEmCodigo == agePunEmCodigo &&
                            p.AgePunEmAgeSucursCodigo == agePunEmAgeSucursCodigo &&
                            p.AgeUsuariCodigo == ageUsuariCodigo).FirstOrDefaultAsync();

            return ageUsuariosPuntoEmision;
        }

        public async Task<AgeUsuariosPuntoEmisionDAO> Actualizar(AgeUsuariosPuntoEmision AgeUsuariosPuntoEmision)
        {
            _dbContext.Entry(AgeUsuariosPuntoEmision).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();

            AgeUsuariosPuntoEmisionDAO ageUsuarioPuntoEmisionDAO = await ConsultarPorId(AgeUsuariosPuntoEmision.AgageSucursAgeLicencCodigo,
                                                                                        AgeUsuariosPuntoEmision.AgePunEmCodigo,
                                                                                        AgeUsuariosPuntoEmision.AgePunEmAgeSucursCodigo,
                                                                                        AgeUsuariosPuntoEmision.AgeUsuariCodigo);

            return ageUsuarioPuntoEmisionDAO;
        }

        public async Task<List<AgeUsuariosPuntoEmisionDAO>> ActualizarVarios(List<AgeUsuariosPuntoEmision> AgeUsuariosPuntoEmisionList)
        {
            _dbContext.AgeUsuariosPuntoEmisions.UpdateRange(AgeUsuariosPuntoEmisionList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeUsuariosPuntoEmision, AgeUsuariosPuntoEmisionDAO>
                .FromEntityToDAOList(AgeUsuariosPuntoEmisionList, FromEntityToDAO);
        }

        private static AgeUsuariosPuntoEmisionDAO? FromEntityToDAO(AgeUsuariosPuntoEmision ageUsuariosPuntoEmision)
        {
            if (ageUsuariosPuntoEmision == null)
            {
                return null;
            }

            return new AgeUsuariosPuntoEmisionDAO
            {
                Id = new AgeUsuariosPuntoEmisionPKDAO
                {
                    AgageSucursAgeLicencCodigo = ageUsuariosPuntoEmision.AgageSucursAgeLicencCodigo,
                    AgeUsuariAgeLicencCodigo = ageUsuariosPuntoEmision.AgeUsuariAgeLicencCodigo,
                    AgePunEmAgeSucursCodigo = ageUsuariosPuntoEmision.AgePunEmAgeSucursCodigo,
                    AgePunEmCodigo = ageUsuariosPuntoEmision.AgePunEmCodigo,
                    AgeUsuariCodigo = ageUsuariosPuntoEmision.AgeUsuariCodigo,
                },
                Estado = ageUsuariosPuntoEmision.Estado,
                FechaDesde = ageUsuariosPuntoEmision.FechaDesde,
                FechaHasta = ageUsuariosPuntoEmision.FechaHasta,
            };
        }
    }
}
