﻿using AGE.Entities.DAO.AgeUsuariosPuntoEmision;
using AGE.Entities;
using AGE.Utils.Paginacion;

namespace AGE.Repositories.AgeUsuariosPuntoEmisionRepository
{
    public interface IAgeUsuariosPuntoEmisionRepository
    {
        Task<Page<AgeUsuariosPuntoEmisionDAO>> ConsultarTodos(
            int AgageSucursAgeLicencCodigo,
            int agePunEmCodigo,
            int agePunEmAgeSucursCodigo,
            long ageUsuariCodigo,
            Pageable pageable);

        Task<AgeUsuariosPuntoEmisionDAO> ConsultarPorId(
            int AgageSucursAgeLicencCodigo,
            int agePunEmCodigo,
            int agePunEmAgeSucursCodigo,
            long ageUsuariCodigo);

        Task<Page<AgeUsuariosPuntoEmisionDAO>> ConsultarListaFiltro(
            int AgeUsuariAgeLicencCodigo,
            string filtro,
            Pageable pageable);

        Task<AgeUsuariosPuntoEmision> ConsultarCompletePorId(
            int AgageSucursAgeLicencCodigo,
            int agePunEmCodigo,
            int agePunEmAgeSucursCodigo,
            long ageUsuariCodigo);

        Task<AgeUsuariosPuntoEmisionDAO> Insertar(AgeUsuariosPuntoEmision AgeUsuariosPuntoEmision);

        Task<List<AgeUsuariosPuntoEmisionDAO>> InsertarVarios(
            List<AgeUsuariosPuntoEmision> AgeUsuariosPuntoEmisionList);

        Task<AgeUsuariosPuntoEmisionDAO> Actualizar(AgeUsuariosPuntoEmision AgeUsuariosPuntoEmision);

        Task<List<AgeUsuariosPuntoEmisionDAO>> ActualizarVarios(
            List<AgeUsuariosPuntoEmision> AgeUsuariosPuntoEmisionList);
    }
}
