﻿using AGE.Entities;
using AGE.Entities.DAO.AgeArchivosMultimedia;
using AGE.Entities.DAO.AgeRutas;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Utils;
using AGE.Utils.Paginacion;
using Microsoft.EntityFrameworkCore;

namespace AGE.Repositories.AgeArchivosMultimediaRepository
{
    public class AgeArchivosMultimediaRepository : IAgeArchivosMultimediaRepository
    {
        private readonly DbContextAge _dbContext;

        public AgeArchivosMultimediaRepository(DbContextAge dbContext)
        {
            _dbContext = dbContext;
        }
       public async Task<Page<AgeArchivosMultimediaDAO>> ConsultarTodos(
           int codigoLicenciatario, 
           int? codigo, 
           string? ruta, 
           string? descripcion, 
           string? tipo, 
           Pageable pageable)
        {
            if(_dbContext.AgeArchivosMultimedia == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgeArchivosMultimedia
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeLicencCodigo == codigoLicenciatario &&
                            (codigo == 0 || p.Codigo == codigoLicenciatario) &&
                            (ruta.Equals("") || p.Ruta.ToLower().Equals(ruta.ToLower())) &&
                            (descripcion.Equals("") || p.Descripcion.ToLower().Contains(descripcion.ToLower())));

            return await Paginator<AgeArchivosMultimedia, AgeArchivosMultimediaDAO>.Paginar(Query, pageable, FromEntityToDAO);
        }

        public async Task<Page<AgeArchivosMultimediaDAO>> ConsultarLicenciatario(
            int codigoLicenciatario, 
            int? codigo, 
            string? ruta, 
            string? descripcion, 
            string? tipo, 
            Pageable pageable)
        {
            if (_dbContext.AgeArchivosMultimedia == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgeArchivosMultimedia
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeLicencCodigo == codigoLicenciatario &&
                            (codigo == 0 || p.Codigo == codigoLicenciatario) &&
                            (ruta.Equals("") || p.Ruta.ToLower().Equals(ruta.ToLower())) &&
                            (descripcion.Equals("") || p.Descripcion.ToLower().Contains(descripcion.ToLower())));

            return await Paginator<AgeArchivosMultimedia, AgeArchivosMultimediaDAO>.Paginar(Query, pageable, FromEntityToDAO);
        }

        public async Task<List<AgeArchivosMultimedia>> ConsultarTodosPorLicenciatario(int codigoLicenciatario)
        {
            if (_dbContext.AgeArchivosMultimedia == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgeArchivosMultimedia
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeLicencCodigo == codigoLicenciatario
                            );

            return await Query.ToListAsync();
        }

        public async Task<Page<AgeArchivosMultimediaDAO>> ConsultarListaFiltro(
            int codigoLicenciatario,
            string filtro,
            Pageable pageable)
        {
            if (_dbContext.AgeArchivosMultimedia == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgeArchivosMultimedia
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeLicencCodigo == codigoLicenciatario && (
                            p.Codigo.ToString().ToLower().Contains(filtro) ||
                            p.Ruta.ToLower().Contains(filtro) ||
                            p.Descripcion.ToLower().Contains(filtro)));

            return await Paginator<AgeArchivosMultimedia, AgeArchivosMultimediaDAO>.Paginar(
                Query, pageable, FromEntityToDAO);
        }


        public async Task<AgeArchivosMultimediaDAO> ConsultarPorId(AgeArchivosMultimediaPKDAO ageArchivosMultimediaPKDAO)
        {
            if (_dbContext.AgeArchivosMultimedia == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeArchivosMultimedia? ageArchivosMultimedia = await _dbContext.AgeArchivosMultimedia
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeLicencCodigo == ageArchivosMultimediaPKDAO.ageLicencCodigo &&
                            p.Codigo == ageArchivosMultimediaPKDAO.codigo).FirstOrDefaultAsync();

            return FromEntityToDAO(ageArchivosMultimedia);
        }

        public async Task<AgeArchivosMultimedia> ConsultarCompletePorId(
            AgeArchivosMultimediaPKDAO ageArchivosMultimediaPKDAO)
        {
            if (_dbContext.AgeArchivosMultimedia == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeArchivosMultimedia? ageArchivosMultimedia = await _dbContext.AgeArchivosMultimedia
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeLicencCodigo == ageArchivosMultimediaPKDAO.ageLicencCodigo &&
                            p.Codigo == ageArchivosMultimediaPKDAO.codigo).FirstOrDefaultAsync();

            return ageArchivosMultimedia;
        }

        public async Task<AgeArchivosMultimediaDAO> Insertar(AgeArchivosMultimedia ageArchivosMultimedium)
        {
            _dbContext.AgeArchivosMultimedia.Add(ageArchivosMultimedium);
            await _dbContext.SaveChangesAsync();

            AgeArchivosMultimediaDAO ageArchivosMultimediaDAO = await ConsultarPorId(new AgeArchivosMultimediaPKDAO
            {
                ageLicencCodigo = ageArchivosMultimedium.AgeLicencCodigo,
                codigo = ageArchivosMultimedium.Codigo
            });

            return ageArchivosMultimediaDAO;
        }

        public async Task<List<AgeArchivosMultimediaDAO>> InsertarVarios(List<AgeArchivosMultimedia> ageArchivosMultimediaList)
        {
            await _dbContext.AgeArchivosMultimedia.AddRangeAsync(ageArchivosMultimediaList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeArchivosMultimedia, AgeArchivosMultimediaDAO>
                .FromEntityToDAOList(ageArchivosMultimediaList, FromEntityToDAO);
        }

        public async Task<AgeArchivosMultimediaDAO> Actualizar(AgeArchivosMultimedia ageArchivosMultimedia)
        {
            _dbContext.Entry(ageArchivosMultimedia).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();

            AgeArchivosMultimediaDAO ageArchivosMultimediaDAO = await ConsultarPorId(new AgeArchivosMultimediaPKDAO
            {
                ageLicencCodigo = ageArchivosMultimedia.AgeLicencCodigo,
                codigo = ageArchivosMultimedia.Codigo
            });

            return ageArchivosMultimediaDAO;
        }

        public async Task<List<AgeArchivosMultimediaDAO>> ActualizarVarios(List<AgeArchivosMultimedia> ageArchivosMultimediaList)
        {
            _dbContext.AgeArchivosMultimedia.UpdateRange(ageArchivosMultimediaList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeArchivosMultimedia, AgeArchivosMultimediaDAO>
                .FromEntityToDAOList(ageArchivosMultimediaList, FromEntityToDAO);
        }

        private static AgeArchivosMultimediaDAO FromEntityToDAO(AgeArchivosMultimedia entityObject)
        {

            if (entityObject == null)
            {
                return null;
            }

            return new AgeArchivosMultimediaDAO
            {
                Id = new AgeArchivosMultimediaPKDAO
                {
                    ageLicencCodigo = entityObject.AgeLicencCodigo,
                    codigo =  entityObject.Codigo
                },
                Descripcion = entityObject.Descripcion,
                Ruta = entityObject.Ruta,
                Tipo = entityObject.Tipo,
                Promocion = entityObject.Promocion,
                Estado = entityObject.Estado
            };
        }
    }
}
