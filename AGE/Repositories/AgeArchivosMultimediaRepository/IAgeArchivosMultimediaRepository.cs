﻿using AGE.Entities;
using AGE.Entities.DAO.AgeArchivosMultimedia;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Utils;
using AGE.Utils.Paginacion;
using Microsoft.EntityFrameworkCore;

namespace AGE.Repositories.AgeArchivosMultimediaRepository
{
    public interface IAgeArchivosMultimediaRepository
    {
        Task<Page<AgeArchivosMultimediaDAO>> ConsultarTodos(
            int codigoLicenciatario, 
            int? codigo,
            string? ruta, 
            string? descripcion, 
            string? tipo, 
            Pageable pageable);

        Task<Page<AgeArchivosMultimediaDAO>> ConsultarLicenciatario(
        int codigoLicenciatario,
        int? codigo,
        string? ruta,
        string? descripcion,
        string? tipo,
        Pageable pageable);

        Task<List<AgeArchivosMultimedia>> ConsultarTodosPorLicenciatario(int codigoLicenciatario);

        Task<Page<AgeArchivosMultimediaDAO>> ConsultarListaFiltro(
        int codigoLicenciatario,
        string filtro,
        Pageable pageable);

        Task<AgeArchivosMultimediaDAO> ConsultarPorId(AgeArchivosMultimediaPKDAO ageArchivosMultimediaPKDAO);

        Task<AgeArchivosMultimedia> ConsultarCompletePorId(
            AgeArchivosMultimediaPKDAO ageArchivosMultimediaPKDAO);

        Task<AgeArchivosMultimediaDAO> Insertar(AgeArchivosMultimedia ageArchivosMultimedium);

        Task<List<AgeArchivosMultimediaDAO>> InsertarVarios(
            List<AgeArchivosMultimedia> ageArchivosMultimediaList);

        Task<AgeArchivosMultimediaDAO> Actualizar(AgeArchivosMultimedia ageArchivosMultimedia);

        Task<List<AgeArchivosMultimediaDAO>> ActualizarVarios(List<AgeArchivosMultimedia> ageArchivosMultimediaList);
    }
}
