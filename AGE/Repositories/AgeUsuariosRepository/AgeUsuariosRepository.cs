﻿using AGE.Entities;
using AGE.Entities.DAO.AgePerfiles;
using AGE.Entities.DAO.AgeUsuarios;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Utils;
using AGE.Utils.Paginacion;
using Microsoft.EntityFrameworkCore;

namespace AGE.Repositories.AgeUsuarioRepository
{
    public class AgeUsuariosRepository : IAgeUsuariosRepository
    {

        private readonly DbContextAge _dbContext;

        public AgeUsuariosRepository(DbContextAge dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Page<AgeUsuarioDAO>> ConsultarTodos(
            int codigoLicenciatario, 
            string codigoExterno, 
            Pageable pageable)
        {
            if (_dbContext.AgeUsuarios == null)
            {
                throw new RegisterNotFoundException();
            }
            
            var Query = _dbContext.AgeUsuarios
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeLicencCodigo == codigoLicenciatario &&
                            (codigoExterno.Equals("") || p.CodigoExterno.Equals(codigoExterno)))
                .Include(x => x.AgePerson)
                .Include(y => y.AgeUsuariosPerfiles)
                .ThenInclude(x => x.AgePerfil);

            return await Paginator<AgeUsuarios, AgeUsuarioDAO>.Paginar(Query, pageable, FromEntityToDAO);
        }

        public async Task<AgeUsuarioDAO> ConsultarPorId(
            AgeUsuarioPKDAO ageUsuarioPKDAO)
        {
            if (_dbContext.AgeUsuarios == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeUsuarios? ageUsuario = await _dbContext.AgeUsuarios
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeLicencCodigo == ageUsuarioPKDAO.ageLicencCodigo &&
                            p.Codigo == ageUsuarioPKDAO.codigo)
                .Include(x => x.AgePerson)
                .Include(y => y.AgeUsuariosPerfiles)
                .ThenInclude(x => x.AgePerfil).FirstOrDefaultAsync();

            return FromEntityToDAO(ageUsuario);
        }

        public async Task<AgeUsuarios> ConsultarCompletePorId(
            AgeUsuarioPKDAO ageUsuarioPKDAO)
        {
            if (_dbContext.AgeUsuarios == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeUsuarios? ageUsuario = await _dbContext.AgeUsuarios
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeLicencCodigo == ageUsuarioPKDAO.ageLicencCodigo &&
                            p.Codigo == ageUsuarioPKDAO.codigo)
                .Include(x => x.AgePerson)
                .Include(y => y.AgeUsuariosPerfiles)
                .ThenInclude(x => x.AgePerfil).FirstOrDefaultAsync();

            return ageUsuario;
        }

        public async Task<Page<AgeUsuarioDAO>> ConsultarListaFiltro(
            int codigoLicenciatario, 
            string filtro, 
            Pageable pageable)
        {
            if (_dbContext.AgeUsuarios == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgeUsuarios
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeLicencCodigo == codigoLicenciatario && (
                            p.CodigoExterno.ToLower().Contains(filtro) ||
                            p.Apellidos.ToLower().Contains(filtro) ||
                            p.Nombres.ToLower().Contains(filtro) ||
                            p.NumeroIdentificacion.ToLower().Contains(filtro)))
                .Include(x => x.AgePerson)
                .Include(y => y.AgeUsuariosPerfiles)
                .ThenInclude(x => x.AgePerfil);

            return await Paginator<AgeUsuarios, AgeUsuarioDAO>.Paginar(Query, pageable, FromEntityToDAO);
        }

        public async Task<List<AgeUsuarioDAO>> ConsultarPorCodigoExterno(
           string codigoExterno)
        {
            if (_dbContext.AgeUsuarios == null)
            {
                throw new RegisterNotFoundException();
            }

            List<AgeUsuarios> usuarios = await _dbContext.AgeUsuarios
               .Where(p => p.Estado == Globales.ESTADO_ACTIVO && p.CodigoExterno.Equals(codigoExterno))
               .Include(x => x.AgePerson)
               .Include(y => y.AgeUsuariosPerfiles)
               .ThenInclude(x => x.AgePerfil)
               .ToListAsync();

            return ConversorEntityToDAOList<AgeUsuarios, AgeUsuarioDAO>
                .FromEntityToDAOList(usuarios, FromEntityToDAO);
        }

        public async Task<AgeUsuarios> ConsultarPorIdentificacion(
            string identificacion)
        {
            if (_dbContext.AgeUsuarios == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeUsuarios? usuario = await _dbContext.AgeUsuarios
               .Where(p => p.Estado == Globales.ESTADO_ACTIVO &&
                           p.NumeroIdentificacion.Equals(identificacion)).FirstOrDefaultAsync();

            return usuario;
        }

        public async Task<AgeUsuarioDAO> Insertar(AgeUsuarios ageUsuario)
        {
            _dbContext.AgeUsuarios.Add(ageUsuario);
            await _dbContext.SaveChangesAsync();

            AgeUsuarioDAO ageUsuarioDAO = await ConsultarPorId(new AgeUsuarioPKDAO
            {
                ageLicencCodigo = ageUsuario.AgeLicencCodigo,
                codigo = ageUsuario.Codigo
            });

            return ageUsuarioDAO;
        }

        public async Task<List<AgeUsuarioDAO>> InsertarVarios(List<AgeUsuarios> ageUsuariosList)
        {
            await _dbContext.AgeUsuarios.AddRangeAsync(ageUsuariosList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeUsuarios, AgeUsuarioDAO>
                .FromEntityToDAOList(ageUsuariosList, FromEntityToDAO);
        }

        public async Task<AgeUsuarioDAO> Actualizar(AgeUsuarios ageUsuario)
        {
            _dbContext.Entry(ageUsuario).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();

            AgeUsuarioDAO ageUsuarioDAO = await ConsultarPorId(new AgeUsuarioPKDAO
            {
                ageLicencCodigo = ageUsuario.AgeLicencCodigo,
                codigo = ageUsuario.Codigo
            });

            return ageUsuarioDAO;
        }

        public async Task<List<AgeUsuarioDAO>> ActualizarVarios(List<AgeUsuarios> ageUsuariosList)
        {
            _dbContext.AgeUsuarios.UpdateRange(ageUsuariosList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeUsuarios, AgeUsuarioDAO>
                .FromEntityToDAOList(ageUsuariosList, FromEntityToDAO);
        }

        private static AgeUsuarioDAO FromEntityToDAO(AgeUsuarios entityObject)
        {

            if (entityObject == null)
            {
                return null;
            }

            List<AgePerfiles> agePerfilList = new();

            if (entityObject.AgeUsuariosPerfiles != null)
                agePerfilList = entityObject.AgeUsuariosPerfiles
                .Where(up => up.AgePerfil != null)
                .Select(up => up.AgePerfil)
                .ToList();

            AgePerfilesDAO agePerfilDAO;
            List<AgePerfilesDAO> agePerfilDAOList = new List<AgePerfilesDAO>();

            if (agePerfilList.Count > 0)
            {
                foreach(AgePerfiles agePerfil in agePerfilList)
                {
                    agePerfilDAO = new AgePerfilesDAO
                    {
                        Id = new AgePerfilesPKDAO
                        {
                            ageLicencCodigo = entityObject.AgeLicencCodigo,
                            codigo = agePerfil.Codigo
                        },
                        Descripcion = agePerfil.Descripcion,
                        Estado = agePerfil.Estado
                    };

                    agePerfilDAOList.Add(agePerfilDAO);
                }
            }

            return new AgeUsuarioDAO
            {
                Id = new AgeUsuarioPKDAO
                {
                    ageLicencCodigo = entityObject.AgeLicencCodigo,
                    codigo = entityObject.Codigo
                },
                AgePersonAgeLicencCodigo = entityObject.AgePersonAgeLicencCodigo ?? 0,
                AgePersonCodigo = entityObject.AgePersonCodigo ?? 0,
                AgeSucursAgeLicencCodigo = entityObject.AgeSucursAgeLicencCodigo,
                AgeSucursCodigo = entityObject.AgeSucursCodigo,
                AgeTipIdCodigo = entityObject.AgeTipIdCodigo,
                ArchivoFoto = entityObject.ArchivoFoto,
                CodigoExterno = entityObject.CodigoExterno,
                Clave = entityObject.Clave,
                NumeroIdentificacion = entityObject.NumeroIdentificacion,
                Nombres = entityObject.Nombres,
                Apellidos = entityObject.Apellidos,
                MailPrincipal = entityObject.MailPrincipal,
                TelefonoCelular = entityObject.TelefonoCelular,
                TipoUsuario = entityObject.TipoUsuario,
                PrimerIngreso = entityObject.PrimerIngreso,
                TipoRegistro = entityObject.TipoRegistro,
                Estado = entityObject.Estado,

                AgeAgeTipLoAgePaisCodigo = entityObject.AgePerson?.AgeAgeTipLoAgePaisCodigo,
                AgeLocaliAgeTipLoCodigo = entityObject.AgePerson?.AgeLocaliAgeTipLoCodigo,
                AgeLocaliCodigo = entityObject.AgePerson?.AgeLocaliCodigo,
                Direccion = entityObject.AgePerson?.DireccionPrincipal,
                EstadoCivil = entityObject.AgePerson?.EstadoCivil,
                FechaNacimiento = entityObject.AgePerson?.FechaNacimiento,
                Genero = entityObject.AgePerson?.Sexo,
                TokenFirebase = entityObject.TokenFirebase,
                AgePerfilList = agePerfilDAOList
            };
        }

        
    }
}
