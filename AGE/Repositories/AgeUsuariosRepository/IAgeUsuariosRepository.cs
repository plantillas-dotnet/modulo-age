﻿
using AGE.Entities;
using AGE.Utils.Paginacion;
using AGE.Entities.DAO.AgeUsuarios;

namespace AGE.Repositories.AgeUsuarioRepository
{
    public interface IAgeUsuariosRepository
    {
        Task<Page<AgeUsuarioDAO>> ConsultarTodos(
            int codigoLicenciatario,
            string codigoExterno,
            Pageable pageable);

        Task<Page<AgeUsuarioDAO>> ConsultarListaFiltro(
            int codigoLicenciatario,
            string filtro,
            Pageable pageable);

        Task<AgeUsuarioDAO> ConsultarPorId(
            AgeUsuarioPKDAO AgeUsuarioPKDAO);

        Task<AgeUsuarios> ConsultarCompletePorId(
            AgeUsuarioPKDAO AgeUsuarioPKDAO);

        Task<List<AgeUsuarioDAO>> ConsultarPorCodigoExterno(
            string codigoExterno);

        Task<AgeUsuarios> ConsultarPorIdentificacion(
            string identificacion);

        Task<AgeUsuarioDAO> Insertar(AgeUsuarios AgeUsuario);

        Task<List<AgeUsuarioDAO>> InsertarVarios(
            List<AgeUsuarios> AgeUsuariosList);

        Task<AgeUsuarioDAO> Actualizar(AgeUsuarios AgeUsuario);

        Task<List<AgeUsuarioDAO>> ActualizarVarios(
            List<AgeUsuarios> AgeUsuariosList);


    }
}
