﻿using AGE.Entities.DAO.AgeTiposIdentificaciones;
using AGE.Entities;
using AGE.Utils.Paginacion;
using AGE.Entities.DAO.AgeTransacciones;
using AGE.Entities.DAO.AgeTiposSucursales;

namespace AGE.Repositories.AgeTransaccioneRepository
{
    public interface IAgeTransaccionesRepository 
    {
        Task<Page<AgeTransaccioneDAO>> ConsultarTodos(
            Pageable pageable);

        Task<Page<AgeTransaccioneDAO>> ConsultarPorNivel(
            short nivel,
            Pageable pageable);

        Task<Page<AgeTransaccioneDAO>> ConsultarPorUsuarioLicenciatario(
            long usuarioCodigo,
            int licenciatarioCodigo,
            Pageable pageable);

        Task<Page<AgeTransaccioneDAO>> ConsultarPorPerfilUsuarioLicenciatario(
            long perfilCodigo,
            long usuarioCodigo,
            int licenciatarioCodigo,
            Pageable pageable);

        Task<Page<AgeTransaccioneDAO>> ConsultarAplicacion(
            int codigoAplicacion,
            int codigo,
            int codigoExterno,
            string descripcion,
            string transaccion,
            short ordenPresentacion,
            string tipoTransaccion,
            string tipoOperacion,
            int ageTransaAgeAplicaCodigo,
            int ageTransaCodigo,
            string? opcion,
            short? nivel,
            string? url,
            string? parametros,
           Pageable pageable);

        Task<Page<AgeTransaccioneDAO>> ConsultarListaFiltro(
            string filtro,
            Pageable pageable);

        Task<AgeTransaccioneDAO> ConsultarPorId(
            AgeTransaccionePKDAO ageTransaccionePKDAO);

        Task<AgeTransacciones> ConsultarCompletePorId(
           AgeTransaccionePKDAO AgeTransaccionePKDAO);

        Task<AgeTransaccioneDAO> Insertar(AgeTransacciones AgeTransaccione);

        Task<List<AgeTransaccioneDAO>> InsertarVarios(
            List<AgeTransacciones> AgTransaccioneList);

        Task<AgeTransaccioneDAO> Actualizar(AgeTransacciones AgeTransaccione);

        Task<List<AgeTransaccioneDAO>> ActualizarVarios(
            List<AgeTransacciones> AgeTransaccioneList);

    }
}
