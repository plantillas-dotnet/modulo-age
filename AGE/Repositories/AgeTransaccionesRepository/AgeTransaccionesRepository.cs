﻿using AGE.Entities;
using AGE.Entities.DAO.AgeTiposIdentificaciones;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Utils.Paginacion;
using AGE.Utils;
using AGE.Entities.DAO.AgeTransacciones;
using Microsoft.EntityFrameworkCore;
using AGE.Entities.DAO.AgeUsuarios;
using AGE.Entities.DAO.AgeUsuariosPerfiles;
using AGE.Entities.DAO.AgePerfiles;

namespace AGE.Repositories.AgeTransaccioneRepository
{
    public class AgeTransaccionesRepository : IAgeTransaccionesRepository
    {
        private readonly DbContextAge _dbContext;

        public AgeTransaccionesRepository(DbContextAge dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Page<AgeTransaccioneDAO>> ConsultarTodos(Pageable pageable)
        {
            var query = _dbContext.AgeTransacciones
                .Where(p => p.Estado != Globales.ESTADO_ANULADO)
                .Include(y => y.InverseAgeTransa)
                    .ThenInclude(y => y.InverseAgeTransa)
                    .ThenInclude(y => y.InverseAgeTransa)
                    .ThenInclude(y => y.InverseAgeTransa)
                    .ThenInclude(y => y.InverseAgeTransa);

            return await Paginator<AgeTransacciones, AgeTransaccioneDAO>.Paginar(query, pageable, FromEntityToDAO);
        }

        public async Task<Page<AgeTransaccioneDAO>> ConsultarPorNivel(short nivel, Pageable pageable)
        {
            var query = _dbContext.AgeTransacciones
                .Where(p => p.Estado != Globales.ESTADO_ANULADO && p.Nivel == nivel)
                .Include(y => y.InverseAgeTransa)
                    .ThenInclude(y => y.InverseAgeTransa)
                    .ThenInclude(y => y.InverseAgeTransa)
                    .ThenInclude(y => y.InverseAgeTransa)
                    .ThenInclude(y => y.InverseAgeTransa);

            return await Paginator<AgeTransacciones, AgeTransaccioneDAO>.Paginar(query, pageable, FromEntityToDAO);
        }


        public async Task<Page<AgeTransaccioneDAO>> ConsultarPorUsuarioLicenciatario(
            long usuarioCodigo, 
            int licenciatarioCodigo, 
            Pageable pageable)
        {

            // Encontrar perfiles por usuario

            List<AgeUsuariosPerfiles>? ageUsuariosPerfileList = await _dbContext.AgeUsuariosPerfiles
                            .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeUsuariAgeLicencCodigo == licenciatarioCodigo &&
                            p.AgeUsuariCodigo == usuarioCodigo)
                            .Include(up => up.AgeUsuari)
                                .ThenInclude(u => u.AgePerson)
                            .Include(up => up.AgePerfil)
                                .ThenInclude(p => p.AgePerfilesTransacciones)
                                    .ThenInclude(pt => pt.AgeTransa)
                                        .ThenInclude(t => t.InverseAgeTransa)
                                        .ThenInclude(t => t.InverseAgeTransa)
                                        .ThenInclude(t => t.InverseAgeTransa)
                                        .ThenInclude(t => t.InverseAgeTransa)
                                .ToListAsync();


            List<AgeTransaccioneDAO> ageTransaccioneDAOList = ObtenerTransacciones(ageUsuariosPerfileList, licenciatarioCodigo);

            return Paginator<AgeTransacciones, AgeTransaccioneDAO>.Paginar(ageTransaccioneDAOList, pageable);
        }


        public async Task<Page<AgeTransaccioneDAO>> ConsultarPorPerfilUsuarioLicenciatario(
            long perfilCodigo, 
            long usuarioCodigo, 
            int licenciatarioCodigo, 
            Pageable pageable)
        {
            // Encontrar perfiles por usuario

            List<AgeUsuariosPerfiles>? ageUsuariosPerfileList = await _dbContext.AgeUsuariosPerfiles
                            .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeUsuariAgeLicencCodigo == licenciatarioCodigo &&
                            p.AgeUsuariCodigo == usuarioCodigo &&
                            p.AgePerfilAgeLicencCodigo == licenciatarioCodigo &&
                            p.AgePerfilCodigo == perfilCodigo)
                            .Include(y => y.AgeUsuari)
                                .ThenInclude(x => x.AgePerson)
                            .Include(x => x.AgePerfil)
                                .ThenInclude(z => z.AgePerfilesTransacciones)
                                    .ThenInclude(t => t.AgeTransa)
                                        .ThenInclude(y => y.InverseAgeTransa)
                                        .ThenInclude(y => y.InverseAgeTransa)
                                        .ThenInclude(y => y.InverseAgeTransa)
                                        .ThenInclude(y => y.InverseAgeTransa)
                                .ToListAsync();

            List<AgeTransaccioneDAO> ageTransaccioneDAOList = ObtenerTransacciones(ageUsuariosPerfileList, licenciatarioCodigo);

            return Paginator<AgeTransacciones, AgeTransaccioneDAO>.Paginar(ageTransaccioneDAOList, pageable);

        }


        public async Task<Page<AgeTransaccioneDAO>> ConsultarAplicacion(
            int codigoAplicacion, 
            int codigo, 
            int codigoExterno, 
            string descripcion, 
            string transaccion, 
            short ordenPresentacion, 
            string tipoTransaccion, 
            string tipoOperacion, 
            int ageTransaAgeAplicaCodigo, 
            int ageTransaCodigo, 
            string? opcion, 
            short? nivel, 
            string? url,
            string? parametros, 
            Pageable pageable)
        {
            var query = _dbContext.AgeTransacciones
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            (codigoAplicacion == 0 || p.AgeAplicaCodigo == codigoAplicacion) &&
                            (codigo == 0 || p.Codigo == codigo) &&
                            (codigoExterno == 0 || p.CodigoExterno == codigoExterno) &&
                            (string.IsNullOrEmpty(descripcion) || p.Descripcion.ToLower().Contains(descripcion.ToLower())) &&
                            (string.IsNullOrEmpty(transaccion) || p.Transaccion.ToLower().Contains(transaccion.ToLower())) &&
                            (ordenPresentacion == 0 || p.OrdenPresentacion == ordenPresentacion) &&
                            (string.IsNullOrEmpty(tipoTransaccion) || p.TipoTransaccion.ToLower().Contains(tipoTransaccion.ToLower())) &&
                            (string.IsNullOrEmpty(tipoOperacion) || p.TipoOperacion.ToLower().Contains(tipoOperacion.ToLower())) &&
                            (ageTransaAgeAplicaCodigo == 0 || p.AgeTransaAgeAplicaCodigo == ageTransaAgeAplicaCodigo) &&
                            (ageTransaCodigo == 0 || p.AgeTransaCodigo == ageTransaCodigo) &&
                            (nivel == 0 || p.Nivel == nivel) &&
                            (string.IsNullOrEmpty(url) || p.Url.ToLower().Contains(url.ToLower())) &&
                            (string.IsNullOrEmpty(parametros) || p.Parametros.ToLower().Contains(parametros.ToLower())))
                .Include(y => y.InverseAgeTransa)
                    .ThenInclude(y => y.InverseAgeTransa)
                    .ThenInclude(y => y.InverseAgeTransa)
                    .ThenInclude(y => y.InverseAgeTransa)
                    .ThenInclude(y => y.InverseAgeTransa);

            return await Paginator<AgeTransacciones, AgeTransaccioneDAO>.Paginar(query, pageable, FromEntityToDAO);
        }

        public async Task<AgeTransaccioneDAO> ConsultarPorId(
            AgeTransaccionePKDAO ageTransaccionePKDAO)
        {
            if (_dbContext.AgeTransacciones == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeTransacciones? ageTransaccione = await _dbContext.AgeTransacciones
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeAplicaCodigo == ageTransaccionePKDAO.ageAplicaCodigo &&
                            p.Codigo == ageTransaccionePKDAO.codigo)
                .Include(y => y.InverseAgeTransa)
                    .ThenInclude(y => y.InverseAgeTransa)
                    .ThenInclude(y => y.InverseAgeTransa)
                    .ThenInclude(y => y.InverseAgeTransa)
                    .ThenInclude(y => y.InverseAgeTransa)
                .FirstOrDefaultAsync();

            return FromEntityToDAO(ageTransaccione);
        }

        public async Task<AgeTransacciones> ConsultarCompletePorId(
            AgeTransaccionePKDAO ageTransaccionePKDAO)
        {
            if (_dbContext.AgeTransacciones == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeTransacciones? ageTransaccione = await _dbContext.AgeTransacciones
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeAplicaCodigo == ageTransaccionePKDAO.ageAplicaCodigo &&
                            p.Codigo == ageTransaccionePKDAO.codigo)
                .Include(y => y.InverseAgeTransa)
                    .ThenInclude(y => y.InverseAgeTransa)
                    .ThenInclude(y => y.InverseAgeTransa)
                    .ThenInclude(y => y.InverseAgeTransa)
                    .ThenInclude(y => y.InverseAgeTransa)
                .FirstOrDefaultAsync();

            return ageTransaccione;
        }

        public async Task<Page<AgeTransaccioneDAO>> ConsultarListaFiltro(string filtro, Pageable pageable)
        {
            var query = _dbContext.AgeTransacciones
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            (p.AgeAplicaCodigo.ToString().ToLower().Contains(filtro) ||
                            p.Codigo.ToString().ToLower().Contains(filtro) ||
                            p.CodigoExterno.ToString().ToLower().Contains(filtro) ||
                            p.Descripcion.ToLower().Contains(filtro) ||
                            p.Transaccion.ToLower().Contains(filtro) ||
                            p.OrdenPresentacion.ToString().ToLower().Contains(filtro) ||
                            p.TipoTransaccion.ToLower().Contains(filtro) ||
                            p.TipoOperacion.ToLower().Contains(filtro) ||
                            p.ObservacionEstado.ToLower().Contains(filtro) ||
                            p.AgeTransaAgeAplicaCodigo.ToString().ToLower().Contains(filtro) ||
                            p.AgeTransaCodigo.ToString().ToLower().Contains(filtro) ||
                            p.Opcion.ToLower().Contains(filtro) ||
                            p.Nivel.ToString().ToLower().Contains(filtro) ||
                            p.Url.ToLower().Contains(filtro) ||
                            p.ObservacionEstado.ToLower().Contains(filtro) ||
                            p.Parametros.ToLower().Contains(filtro)))
                .Include(y => y.InverseAgeTransa)
                    .ThenInclude(y => y.InverseAgeTransa)
                    .ThenInclude(y => y.InverseAgeTransa)
                    .ThenInclude(y => y.InverseAgeTransa)
                    .ThenInclude(y => y.InverseAgeTransa);

            return await Paginator<AgeTransacciones, AgeTransaccioneDAO>.Paginar(query, pageable, FromEntityToDAO);
        }

        public async Task<AgeTransaccioneDAO> Insertar(AgeTransacciones ageTransaccione)
        {
            _dbContext.AgeTransacciones.Add(ageTransaccione);
            await _dbContext.SaveChangesAsync();

            return await ConsultarPorId(new AgeTransaccionePKDAO
            {
                ageAplicaCodigo = ageTransaccione.AgeAplicaCodigo,
                codigo = ageTransaccione.Codigo
            });
        }

        public async Task<List<AgeTransaccioneDAO>> InsertarVarios(List<AgeTransacciones> ageTransaccioneList)
        {
            await _dbContext.AgeTransacciones.AddRangeAsync(ageTransaccioneList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeTransacciones, AgeTransaccioneDAO>
                .FromEntityToDAOList(ageTransaccioneList, FromEntityToDAO);
        }

        public async Task<AgeTransaccioneDAO> Actualizar(AgeTransacciones ageTransaccione)
        {
            _dbContext.Entry(ageTransaccione).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();

            AgeTransaccioneDAO ageTransaccioneDAO = await ConsultarPorId(new AgeTransaccionePKDAO
            {
                ageAplicaCodigo = ageTransaccione.AgeAplicaCodigo,
                codigo = ageTransaccione.Codigo
            });

            return ageTransaccioneDAO;
        }


        public async Task<List<AgeTransaccioneDAO>> ActualizarVarios(List<AgeTransacciones> ageTransaccioneList)
        {
            _dbContext.AgeTransacciones.UpdateRange(ageTransaccioneList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList< AgeTransacciones, AgeTransaccioneDAO>
                .FromEntityToDAOList(ageTransaccioneList, FromEntityToDAO);
        }


        private static AgeTransaccioneDAO FromEntityToDAO(AgeTransacciones entityObject)
        {
            if (entityObject == null)
            {
                return null;
            }

            List<AgeTransaccioneDAO> ageTransaccioneDAOList = new List<AgeTransaccioneDAO>();

            foreach (AgeTransacciones ageTransaccione in entityObject.InverseAgeTransa)
            {
                AgeTransaccioneDAO ageTransaccioneDAO = FromEntityToDAO(ageTransaccione);
                ageTransaccioneDAOList.Add(ageTransaccioneDAO);
            }

            return new AgeTransaccioneDAO
            {
                Id = new AgeTransaccionePKDAO
                {
                    ageAplicaCodigo = entityObject.AgeAplicaCodigo,
                    codigo = entityObject.Codigo
                },
                CodigoExterno = entityObject.CodigoExterno,
                Descripcion = entityObject.Descripcion,
                Transaccion = entityObject.Transaccion,
                OrdenPresentacion = entityObject.OrdenPresentacion,
                TipoTransaccion = entityObject.TipoTransaccion,
                TipoOperacion = entityObject.TipoOperacion,
                AgeTransaAgeAplicaCodigo = entityObject.AgeTransaAgeAplicaCodigo,
                AgeTransaCodigo = entityObject.AgeTransaCodigo,
                Opcion = entityObject.Opcion,
                Nivel = entityObject.Nivel,
                Url = entityObject.Url,
                Parametros = entityObject.Parametros,
                Estado = entityObject.Estado,
                AgeTransaccioneList = ageTransaccioneDAOList
            };
        }


        private List<AgeTransaccioneDAO> ObtenerTransacciones(
            List<AgeUsuariosPerfiles> ageUsuariosPerfileList,
            int licenciatarioCodigo)
        {

            if (ageUsuariosPerfileList == null)
                throw new RegisterNotFoundException("Usuario no tiene perfil asignado.");


            // Obtener transacciones

            List<AgeTransacciones> ageTransaccionesList = new List<AgeTransacciones>();
            List<AgeTransaccioneDAO> ageTransaccioneDAOList = new List<AgeTransaccioneDAO>();

            ageUsuariosPerfileList.ForEach(ageUsuariosPerfil => {

                ageTransaccionesList = ageUsuariosPerfil.AgePerfil.AgePerfilesTransacciones
                    .Where(pt => pt.Estado != Globales.ESTADO_ANULADO &&
                                 pt.AgeTransa.Nivel == Globales.NIVEL_1 &&
                                 pt.AgeTransa.Opcion == Globales.SI &&
                                 (_dbContext.AgeLicenciatariosAplicacions
                                    .Where(la => la.Estado != Globales.ESTADO_ANULADO &&
                                                 la.AgeLicencCodigo == licenciatarioCodigo &&
                                                 la.AgeAplicaCodigo == pt.AgeTransaAgeAplicaCodigo)
                                    .ToListAsync() != null))
                    .OrderBy(pt => pt.AgeTransa.OrdenPresentacion)
                    .Select(pt => pt.AgeTransa).ToList();


                // Convertir a DAO

                ageTransaccioneDAOList = ConversorEntityToDAOList<AgeTransacciones, AgeTransaccioneDAO>
                                            .FromEntityToDAOList(ageTransaccionesList, FromEntityToDAO);


                // settear hijos

                AgePerfilesPKDAO agePerfilesPK = new AgePerfilesPKDAO
                {
                    codigo = ageUsuariosPerfil.AgePerfilAgeLicencCodigo,
                    ageLicencCodigo = ageUsuariosPerfil.AgePerfilAgeLicencCodigo
                };

                ageTransaccioneDAOList.ForEach(t => t.AgeTransaccioneList = VerificarHijos(t, agePerfilesPK));

            });

            if (ageTransaccioneDAOList == null)
                throw new RegisterNotFoundException("Usuario no tiene transacciones asignadas.");


            return ageTransaccioneDAOList;
        }


        private List<AgeTransaccioneDAO> VerificarHijos(AgeTransaccioneDAO ageTransaccionesDAO, AgePerfilesPKDAO agePerfilesPK)
        {

            if(ageTransaccionesDAO.AgeTransaccioneList == null || 
               ageTransaccionesDAO.AgeTransaccioneList.Count == 0)
                return new List<AgeTransaccioneDAO>();

            List<AgeTransaccioneDAO> listaHija = ageTransaccionesDAO.AgeTransaccioneList
                .Where(t => (_dbContext.AgePerfilesTransacciones
                                .Where(pt => pt.Estado != Globales.ESTADO_ANULADO &&
                                             pt.AgePerfilCodigo == agePerfilesPK.codigo &&
                                             pt.AgePerfilAgeLicencCodigo == agePerfilesPK.ageLicencCodigo &&
                                             pt.AgeTransaCodigo == t.Id.codigo &&
                                             pt.AgeTransaAgeAplicaCodigo == t.Id.ageAplicaCodigo)
                                .ToListAsync() != null)).OrderBy(t => t.OrdenPresentacion).ToList();

            return listaHija;

        }

    }
}

