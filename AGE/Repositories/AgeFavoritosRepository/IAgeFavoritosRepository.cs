﻿using AGE.Entities.DAO.AgeFavoritos;
using AGE.Entities;
using AGE.Utils.Paginacion;

namespace AGE.Repositories.AgeFavoritosRepository
{
    public interface IAgeFavoritosRepository
    {
        Task<Page<AgeFavoritosDAO>> ConsultarTodos(
            int codigoLicenciatario,
            int codigoUsuario,
            Pageable pageable);

        Task<AgeFavoritosDAO> ConsultarPorId(AgeFavoritosPKDAO ageFavoritosPKDAO);

        Task<Page<AgeFavoritosDAO>> ConsultarListaFiltro(
            string filtro,
            int codigoLicenciatario,
            int codigoUsuario,
            Pageable pageable);

        Task<AgeFavoritos> ConsultarCompletePorId(
            AgeFavoritosPKDAO ageFavoritosPKDAO);

        Task<AgeFavoritosDAO> Insertar(AgeFavoritos AgeFavoritos);

        Task<List<AgeFavoritosDAO>> InsertarVarios(
            List<AgeFavoritos> AgeFavoritosList);

        Task<AgeFavoritosDAO> Actualizar(AgeFavoritos AgeFavoritos);

        Task<List<AgeFavoritosDAO>> ActualizarVarios(
            List<AgeFavoritos> AgeFavoritosList);
    }
}
