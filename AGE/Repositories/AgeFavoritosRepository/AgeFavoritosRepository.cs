﻿using AGE.Entities.DAO.AgeFavoritos;
using AGE.Entities;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Utils.Paginacion;
using AGE.Utils;
using Microsoft.EntityFrameworkCore;

namespace AGE.Repositories.AgeFavoritosRepository
{
    public class AgeFavoritosRepository : IAgeFavoritosRepository
    {
        private readonly DbContextAge _dbContext;

        public AgeFavoritosRepository(DbContextAge dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Page<AgeFavoritosDAO>> ConsultarTodos(int codigoLicenciatario,
                                                                int codigoUsuario,
                                                                Pageable pageable)
        {
            if (_dbContext.AgeFavoritos == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgeFavoritos
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                    (p.AgeUsuariAgeLicencCodigo == codigoLicenciatario) &&
                    (p.AgeUsuariCodigo == codigoUsuario));

            return await Paginator<AgeFavoritos, AgeFavoritosDAO>.Paginar(Query, pageable, FromEntityToDAO);
        }

        public async Task<Page<AgeFavoritosDAO>> ConsultarListaFiltro(string filtro,
                                                                      int codigoLicenciatario,
                                                                      int codigoUsuario,
                                                                      Pageable pageable)
        {
            if (_dbContext.AgeFavoritos == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgeFavoritos
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeUsuariAgeLicencCodigo == codigoLicenciatario &&
                            p.AgeUsuariCodigo == codigoUsuario && (
                            p.AgeTransaCodigo.ToString().ToLower().Contains(filtro) ||
                            p.AgeTransaAgeAplicaCodigo.ToString().ToLower().Contains(filtro)));

            return await Paginator<AgeFavoritos, AgeFavoritosDAO>.Paginar(Query, pageable, FromEntityToDAO);
        }

        public async Task<AgeFavoritosDAO> ConsultarPorId(AgeFavoritosPKDAO ageFavoritosPKDAO)
        {
            if (_dbContext.AgeFavoritos == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeFavoritos? ageFavorito = await _dbContext.AgeFavoritos
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeUsuariAgeLicencCodigo == ageFavoritosPKDAO.AgeUsuariAgeLicencCodigo &&
                            p.AgeUsuariCodigo == ageFavoritosPKDAO.AgeUsuariCodigo &&
                            p.AgeTransaAgeAplicaCodigo == ageFavoritosPKDAO.AgeTransaAgeAplicaCodigo &&
                            p.AgeTransaCodigo == ageFavoritosPKDAO.AgeTransaCodigo &&
                            p.Codigo == ageFavoritosPKDAO.Codigo).FirstOrDefaultAsync();

            return FromEntityToDAO(ageFavorito);
        }

        public async Task<AgeFavoritos> ConsultarCompletePorId(AgeFavoritosPKDAO ageFavoritosPKDAO)
        {
            if (_dbContext.AgeFavoritos == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeFavoritos? ageFavorito = await _dbContext.AgeFavoritos
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeUsuariAgeLicencCodigo == ageFavoritosPKDAO.AgeUsuariAgeLicencCodigo &&
                            p.AgeUsuariCodigo == ageFavoritosPKDAO.AgeUsuariCodigo &&
                            p.AgeTransaAgeAplicaCodigo == ageFavoritosPKDAO.AgeTransaAgeAplicaCodigo &&
                            p.AgeTransaCodigo == ageFavoritosPKDAO.AgeTransaCodigo &&
                            p.Codigo == ageFavoritosPKDAO.Codigo).FirstOrDefaultAsync();

            return ageFavorito;
        }


        public async Task<AgeFavoritosDAO> Insertar(
            AgeFavoritos ageFavoritos)
        {
            _dbContext.AgeFavoritos.Add(ageFavoritos);
            await _dbContext.SaveChangesAsync();

            AgeFavoritosDAO ageFavoritosDAO = await ConsultarPorId(new AgeFavoritosPKDAO
            {
                AgeUsuariAgeLicencCodigo = ageFavoritos.AgeUsuariAgeLicencCodigo,
                AgeUsuariCodigo = ageFavoritos.AgeUsuariCodigo,
                AgeTransaAgeAplicaCodigo = ageFavoritos.AgeTransaAgeAplicaCodigo,
                AgeTransaCodigo = ageFavoritos.AgeTransaCodigo,
                Codigo = ageFavoritos.Codigo
            });

            return ageFavoritosDAO;
        }

        public async Task<List<AgeFavoritosDAO>> InsertarVarios(
            List<AgeFavoritos> ageFavoritosList)
        {
            await _dbContext.AgeFavoritos.AddRangeAsync(ageFavoritosList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeFavoritos, AgeFavoritosDAO>
                .FromEntityToDAOList(ageFavoritosList, FromEntityToDAO);
        }

        public async Task<AgeFavoritosDAO> Actualizar(AgeFavoritos ageFavoritos)
        {
            _dbContext.Entry(ageFavoritos).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();

            AgeFavoritosDAO ageFavoritosDAO = await ConsultarPorId(new AgeFavoritosPKDAO
            {
                AgeUsuariAgeLicencCodigo = ageFavoritos.AgeUsuariAgeLicencCodigo,
                AgeUsuariCodigo = ageFavoritos.AgeUsuariCodigo,
                AgeTransaAgeAplicaCodigo = ageFavoritos.AgeTransaAgeAplicaCodigo,
                AgeTransaCodigo = ageFavoritos.AgeTransaCodigo,
                Codigo = ageFavoritos.Codigo
            });

            return ageFavoritosDAO;
        }

        public async Task<List<AgeFavoritosDAO>> ActualizarVarios(List<AgeFavoritos> ageFavoritosList)
        {
            _dbContext.AgeFavoritos.UpdateRange(ageFavoritosList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeFavoritos, AgeFavoritosDAO>
                .FromEntityToDAOList(ageFavoritosList, FromEntityToDAO);
        }

        private static AgeFavoritosDAO FromEntityToDAO(AgeFavoritos ageFavoritos)
        {

            if (ageFavoritos == null)
            {
                return null;
            }

            return new AgeFavoritosDAO
            {
                Id = new AgeFavoritosPKDAO
                {
                    AgeUsuariAgeLicencCodigo = ageFavoritos.AgeUsuariAgeLicencCodigo,
                    AgeUsuariCodigo = ageFavoritos.AgeUsuariCodigo,
                    AgeTransaAgeAplicaCodigo = ageFavoritos.AgeTransaAgeAplicaCodigo,
                    AgeTransaCodigo = ageFavoritos.AgeTransaCodigo,
                    Codigo = ageFavoritos.Codigo
                },
                Estado = ageFavoritos.Estado,
                FechaDesde = ageFavoritos.FechaDesde,
                FechaHasta = ageFavoritos.FechaHasta
            };
        }
    }
}
