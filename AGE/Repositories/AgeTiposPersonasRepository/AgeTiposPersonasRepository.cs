﻿using AGE.Entities;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Utils.Paginacion;
using AGE.Utils;
using Microsoft.EntityFrameworkCore;
using AGE.Entities.DAO.AgeTiposPersonas;

namespace AGE.Repositories.AgeTiposPersonaRepository
{
    public class AgeTiposPersonasRepository : IAgeTiposPersonasRepository
    {
        private readonly DbContextAge _dbContext;

        public AgeTiposPersonasRepository(DbContextAge dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Page<AgeTiposPersonaDAO>> ConsultarTodos(
            int codigoLicenciatario,
            int codigo,
            string codigoExterno,
            string descripcion,
            Pageable pageable)
        {
            if (_dbContext.AgeTiposPersonas == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgeTiposPersonas
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeLicencCodigo == codigoLicenciatario &&
                            (codigo == 0 || p.Codigo == codigo) &&
                            (codigoExterno.Equals("") || p.CodigoExterno.ToLower().Equals(codigoExterno.ToLower())) &&
                            (descripcion.Equals("") || p.Descripcion.ToLower().Contains(descripcion.ToLower())));

            return await Paginator<AgeTiposPersonas, AgeTiposPersonaDAO>.Paginar(Query, pageable, FromEntityToDAO);
        }


        public async Task<AgeTiposPersonaDAO> ConsultarPorId(AgeTiposPersonaPKDAO ageTiposPersonaPKDAO)
        {
            if (_dbContext.AgeTiposPersonas == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeTiposPersonas? ageTiposPersona = await _dbContext.AgeTiposPersonas
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeLicencCodigo == ageTiposPersonaPKDAO.ageLicencCodigo &&
                            p.Codigo == ageTiposPersonaPKDAO.codigo).FirstOrDefaultAsync();

            return FromEntityToDAO(ageTiposPersona);
        }


        public async Task<AgeTiposPersonas> ConsultarCompletePorId(AgeTiposPersonaPKDAO ageTiposPersonaPKDAO)
        {
            if (_dbContext.AgeTiposPersonas == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeTiposPersonas? ageTiposPersona = await _dbContext.AgeTiposPersonas
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeLicencCodigo == ageTiposPersonaPKDAO.ageLicencCodigo &&
                            p.Codigo == ageTiposPersonaPKDAO.codigo).FirstOrDefaultAsync();

            return ageTiposPersona;
        }


        public async Task<Page<AgeTiposPersonaDAO>> ConsultarListaFiltro(
            int codigoLicenciatario,
            string filtro,
            Pageable pageable)
        {
            if (_dbContext.AgeTiposPersonas == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgeTiposPersonas
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeLicencCodigo == codigoLicenciatario && (
                            p.Codigo.ToString().ToLower().Contains(filtro) ||
                            p.CodigoExterno.ToLower().Contains(filtro) ||
                            p.Descripcion.ToLower().Contains(filtro)));

            return await Paginator<AgeTiposPersonas, AgeTiposPersonaDAO>.Paginar(Query, pageable, FromEntityToDAO);
        }


        public async Task<AgeTiposPersonaDAO> Insertar(AgeTiposPersonas ageTiposPersona)
        {
            _dbContext.AgeTiposPersonas.Add(ageTiposPersona);
            await _dbContext.SaveChangesAsync();

            AgeTiposPersonaDAO ageTiposPersonaDAO = await ConsultarPorId(new AgeTiposPersonaPKDAO
            {
                ageLicencCodigo = ageTiposPersona.AgeLicencCodigo,
                codigo = ageTiposPersona.Codigo
            });

            return ageTiposPersonaDAO;
        }


        public async Task<List<AgeTiposPersonaDAO>> InsertarVarios(List<AgeTiposPersonas> ageTiposPersonaList)
        {
            await _dbContext.AgeTiposPersonas.AddRangeAsync(ageTiposPersonaList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeTiposPersonas, AgeTiposPersonaDAO>
                .FromEntityToDAOList(ageTiposPersonaList, FromEntityToDAO);
        }

        public async Task<AgeTiposPersonaDAO> Actualizar(AgeTiposPersonas ageTiposPersona)
        {
            _dbContext.Entry(ageTiposPersona).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();

            AgeTiposPersonaDAO ageTiposPersonaDAO = await ConsultarPorId(new AgeTiposPersonaPKDAO
            {
                ageLicencCodigo = ageTiposPersona.AgeLicencCodigo,
                codigo = ageTiposPersona.Codigo
            });

            return ageTiposPersonaDAO;
        }

        public async Task<List<AgeTiposPersonaDAO>> ActualizarVarios(List<AgeTiposPersonas> ageTiposPersonaList)
        {
            _dbContext.AgeTiposPersonas.UpdateRange(ageTiposPersonaList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeTiposPersonas, AgeTiposPersonaDAO>
                .FromEntityToDAOList(ageTiposPersonaList, FromEntityToDAO);
        }


        private static AgeTiposPersonaDAO FromEntityToDAO(AgeTiposPersonas entityObject)
        {

            if (entityObject == null)
            {
                return null;
            }

            return new AgeTiposPersonaDAO
            {
                Id = new AgeTiposPersonaPKDAO
                {
                    ageLicencCodigo = entityObject.AgeLicencCodigo,
                    codigo = entityObject.Codigo
                },
                Descripcion = entityObject.Descripcion,
                CodigoExterno = entityObject.CodigoExterno,
                Estado = entityObject.Estado
            };
        }

    }

}
