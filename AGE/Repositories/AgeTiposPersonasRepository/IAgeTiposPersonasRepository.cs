﻿using AGE.Entities;
using AGE.Utils.Paginacion;
using AGE.Entities.DAO.AgeTiposPersonas;

namespace AGE.Repositories.AgeTiposPersonaRepository
{
    public interface IAgeTiposPersonasRepository
    {
        Task<Page<AgeTiposPersonaDAO>> ConsultarTodos(
            int codigoLicenciatario,
            int codigo,
            string codigoExterno,
            string descripcion,
            Pageable pageable);

        Task<Page<AgeTiposPersonaDAO>> ConsultarListaFiltro(
            int codigoLicenciatario,
            string filtro,
            Pageable pageable);

        Task<AgeTiposPersonaDAO> ConsultarPorId(
            AgeTiposPersonaPKDAO AgeTiposPersonaPKDAO);

        Task<AgeTiposPersonas> ConsultarCompletePorId(
           AgeTiposPersonaPKDAO AgeTiposPersonaPKDAO);

        Task<AgeTiposPersonaDAO> Insertar(AgeTiposPersonas AgeTiposPersona);

        Task<List<AgeTiposPersonaDAO>> InsertarVarios(
            List<AgeTiposPersonas> AgeTiposPersonaList);

        Task<AgeTiposPersonaDAO> Actualizar(AgeTiposPersonas AgeTiposPersona);

        Task<List<AgeTiposPersonaDAO>> ActualizarVarios(
            List<AgeTiposPersonas> AgeTiposPersonaList);
    }
}
