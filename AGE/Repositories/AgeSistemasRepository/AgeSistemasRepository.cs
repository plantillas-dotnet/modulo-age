﻿using AGE.Entities.DAO.AgeSistemas;
using AGE.Entities;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Utils.Paginacion;
using AGE.Utils;
using Microsoft.EntityFrameworkCore;

namespace AGE.Repositories.AgeSistemasRepository
{
    public class AgeSistemasRepository : IAgeSistemasRepository
    {
        private readonly DbContextAge _dbContext;

        public AgeSistemasRepository(DbContextAge dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Page<AgeSistemasDAO>> ConsultarTodos(
            int codigo,
            string descripcion,
            Pageable pageable)
        {
            if (_dbContext.AgeSistemas == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgeSistemas
                .Where(p => p.Estado != Globales.ESTADO_ANULADO && (codigo == 0 || p.Codigo == codigo) &&
                            (descripcion.Equals("") || p.Descripcion.ToLower().Contains(descripcion.ToLower())));

            return await Paginator<AgeSistemas, AgeSistemasDAO>.Paginar(Query, pageable, FromEntityToDAO);
        }

        public async Task<Page<AgeSistemasDAO>> ConsultarListaFiltro(
            string filtro,
            Pageable pageable)
        {
            if (_dbContext.AgeSistemas == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgeSistemas
                .Where(p => p.Estado != Globales.ESTADO_ANULADO && (
                            p.Codigo.ToString().ToLower().Contains(filtro) ||
                            p.Descripcion.ToLower().Contains(filtro)));

            return await Paginator<AgeSistemas, AgeSistemasDAO>.Paginar(Query, pageable, FromEntityToDAO);
        }


        public async Task<AgeSistemasDAO> ConsultarPorId(int codigo)
        {
            if (_dbContext.AgeSistemas == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeSistemas? ageSistemas = await _dbContext.AgeSistemas
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                p.Codigo == codigo).FirstOrDefaultAsync();

            return FromEntityToDAO(ageSistemas);
        }


        public async Task<AgeSistemas> ConsultarCompletePorId(int codigo)
        {
            if (_dbContext.AgeSistemas == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeSistemas? ageSistemas = await _dbContext.AgeSistemas
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                p.Codigo == codigo).FirstOrDefaultAsync();

            return ageSistemas;
        }


        public async Task<AgeSistemasDAO> Insertar(AgeSistemas ageSistemas)
        {
            _dbContext.AgeSistemas.Add(ageSistemas);
            await _dbContext.SaveChangesAsync();

            AgeSistemasDAO ageSistemasDAO = await ConsultarPorId(ageSistemas.Codigo);

            return ageSistemasDAO;
        }

        public async Task<AgeSistemasDAO> Actualizar(AgeSistemas ageSistemas)
        {
            _dbContext.Entry(ageSistemas).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();

            AgeSistemasDAO ageSistemasDAO = await ConsultarPorId(ageSistemas.Codigo);

            return ageSistemasDAO;
        }

        public async Task<List<AgeSistemasDAO>> InsertarVarios(List<AgeSistemas> ageSistemasList)
        {
            await _dbContext.AgeSistemas.AddRangeAsync(ageSistemasList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeSistemas, AgeSistemasDAO>
                .FromEntityToDAOList(ageSistemasList, FromEntityToDAO);
        }

        public async Task<List<AgeSistemasDAO>> ActualizarVarios(List<AgeSistemas> ageSistemasList)
        {
            _dbContext.AgeSistemas.UpdateRange(ageSistemasList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeSistemas, AgeSistemasDAO>
                .FromEntityToDAOList(ageSistemasList, FromEntityToDAO);
        }

        private AgeSistemasDAO FromEntityToDAO(AgeSistemas entityObject)
        {
            AgeSistemasDAO DAOobject = null;

            if (entityObject != null)
            {
                DAOobject = new AgeSistemasDAO
                {
                    Estado = entityObject.Estado,
                    Codigo = entityObject.Codigo,
                    Descripcion = entityObject.Descripcion,
                };
            }

            return DAOobject;

        }
    }
}
