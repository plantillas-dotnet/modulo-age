﻿using AGE.Entities.DAO.AgeSistemas;
using AGE.Entities;
using AGE.Utils.Paginacion;

namespace AGE.Repositories.AgeSistemasRepository
{
    public interface IAgeSistemasRepository
    {
        Task<Page<AgeSistemasDAO>> ConsultarTodos(
            int codigo,
            string descripcion,
            Pageable pageable);

        Task<AgeSistemasDAO> ConsultarPorId(
            int codigo);

        Task<AgeSistemas> ConsultarCompletePorId(
            int codigo);

        Task<Page<AgeSistemasDAO>> ConsultarListaFiltro(
            string filtro,
            Pageable pageable);

        Task<AgeSistemasDAO> Insertar(
            AgeSistemas ageSistemas);

        Task<AgeSistemasDAO> Actualizar(
            AgeSistemas ageSistemas);

        Task<List<AgeSistemasDAO>> InsertarVarios(
            List<AgeSistemas> ageSistemasList);

        Task<List<AgeSistemasDAO>> ActualizarVarios(
            List<AgeSistemas> ageSistemasList);
    }
}
