﻿using AGE.Entities.DAO.AgeFormasPagosInstFina;
using AGE.Entities;
using AGE.Utils.Paginacion;

namespace AGE.Repositories.AgeFormasPagosInstFinasInstFinaRepository
{
    public interface IAgeFormasPagosInstFinaRepository
    {

        Task<Page<AgeFormasPagosInstFinaDAO>> ConsultarTodos(
            int codigoLicenciatario,
            int ageForPaAgeLicencCodigo,
            int ageForPaCodigo,
            string descripcion,
            Pageable pageable);

        Task<Page<AgeFormasPagosInstFinaDAO>> ConsultarListaFiltro(
            int codigoLicenciatario,
            string filtro,
            Pageable pageable);

        Task<AgeFormasPagosInstFinaDAO> ConsultarPorId(
            AgeFormasPagosInstFinaPKDAO ageFormasPagosInstFinaPKDAO);

        Task<AgeFormasPagosInstFina> ConsultarCompletePorId(
           AgeFormasPagosInstFinaPKDAO ageFormasPagosInstFinaPKDAO);

        Task<AgeFormasPagosInstFinaDAO> Insertar(AgeFormasPagosInstFina ageFormasPagosInstFina);

        Task<List<AgeFormasPagosInstFinaDAO>> InsertarVarios(
            List<AgeFormasPagosInstFina> ageFormasPagosInstFinaList);

        Task<AgeFormasPagosInstFinaDAO> Actualizar(AgeFormasPagosInstFina ageFormasPagosInstFina);

        Task<List<AgeFormasPagosInstFinaDAO>> ActualizarVarios(
            List<AgeFormasPagosInstFina> ageFormasPagosInstFinaList);

    }
}
