﻿using AGE.Entities;
using AGE.Entities.DAO.AgeFormasPagosInstFina;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Repositories.AgeFormasPagosInstFinasInstFinaRepository;
using AGE.Utils;
using AGE.Utils.Paginacion;
using Microsoft.EntityFrameworkCore;

namespace AGE.Repositories.AgeFormasPagosInstFinaRepository
{
    public class AgeFormasPagosInstFinaRepository : IAgeFormasPagosInstFinaRepository
    {

        private readonly DbContextAge _dbContext;

        public AgeFormasPagosInstFinaRepository(DbContextAge dbContext)
        {
            _dbContext = dbContext;
        }


        public async Task<Page<AgeFormasPagosInstFinaDAO>> ConsultarTodos(
            int codigoLicenciatario, 
            int ageForPaAgeLicencCodigo, 
            int ageForPaCodigo, 
            string descripcion, 
            Pageable pageable)
        {
            if (_dbContext.AgeFormasPagosInstFinas == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgeFormasPagosInstFinas
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            (codigoLicenciatario == 0 || p.AgeLicencCodigo == codigoLicenciatario) &&
                            (ageForPaAgeLicencCodigo == 0 || p.AgeForPaAgeLicencCodigo == ageForPaAgeLicencCodigo) &&
                            (ageForPaCodigo == 0 || p.AgeForPaCodigo == ageForPaCodigo) &&
                            (descripcion.Equals("") || p.Descripcion.ToLower().Contains(descripcion.ToLower())));

            return await Paginator<AgeFormasPagosInstFina, AgeFormasPagosInstFinaDAO>.Paginar(Query, pageable, FromEntityToDAO);
        }


        public async Task<AgeFormasPagosInstFinaDAO> ConsultarPorId(
            AgeFormasPagosInstFinaPKDAO ageFormasPagosInstFinaPKDAO)
        {
            if (_dbContext.AgeFormasPagosInstFinas == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeFormasPagosInstFina? ageFormasPagosInstFina = await _dbContext.AgeFormasPagosInstFinas
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeLicencCodigo == ageFormasPagosInstFinaPKDAO.AgeLicencCodigo &&
                            p.Codigo == ageFormasPagosInstFinaPKDAO.Codigo).FirstOrDefaultAsync();

            return FromEntityToDAO(ageFormasPagosInstFina);
        }


        public async Task<AgeFormasPagosInstFina> ConsultarCompletePorId(
            AgeFormasPagosInstFinaPKDAO ageFormasPagosInstFinaPKDAO)
        {
            if (_dbContext.AgeFormasPagosInstFinas == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeFormasPagosInstFina? ageFormasPagosInstFina = await _dbContext.AgeFormasPagosInstFinas
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeLicencCodigo == ageFormasPagosInstFinaPKDAO.AgeLicencCodigo &&
                            p.Codigo == ageFormasPagosInstFinaPKDAO.Codigo).FirstOrDefaultAsync();

            return ageFormasPagosInstFina;
        }


        public async Task<Page<AgeFormasPagosInstFinaDAO>> ConsultarListaFiltro(
            int codigoLicenciatario, 
            string filtro, 
            Pageable pageable)
        {
            if (_dbContext.AgeFormasPagosInstFinas == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgeFormasPagosInstFinas
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeLicencCodigo == codigoLicenciatario && (
                            p.Codigo.ToString().ToLower().Contains(filtro) ||
                            p.AgeForPaAgeLicencCodigo.ToString().ToLower().Contains(filtro) ||
                            p.AgeForPaCodigo.ToString().ToLower().Contains(filtro) ||
                            p.Descripcion.ToLower().Contains(filtro)));

            return await Paginator<AgeFormasPagosInstFina, AgeFormasPagosInstFinaDAO>.Paginar(Query, pageable, FromEntityToDAO);
        }

       

        public async Task<AgeFormasPagosInstFinaDAO> Insertar(
            AgeFormasPagosInstFina ageFormasPagosInstFina)
        {
            _dbContext.AgeFormasPagosInstFinas.Add(ageFormasPagosInstFina);
            await _dbContext.SaveChangesAsync();

            AgeFormasPagosInstFinaDAO ageFormasPagosInstFinaDAO = await ConsultarPorId(new AgeFormasPagosInstFinaPKDAO
            {
                AgeLicencCodigo = ageFormasPagosInstFina.AgeLicencCodigo,
                Codigo = ageFormasPagosInstFina.Codigo
            });

            return ageFormasPagosInstFinaDAO;
        }


        public async Task<List<AgeFormasPagosInstFinaDAO>> InsertarVarios(
            List<AgeFormasPagosInstFina> ageFormasPagosInstFinaList)
        {
            await _dbContext.AgeFormasPagosInstFinas.AddRangeAsync(ageFormasPagosInstFinaList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeFormasPagosInstFina, AgeFormasPagosInstFinaDAO>
                .FromEntityToDAOList(ageFormasPagosInstFinaList, FromEntityToDAO);
        }

        public async Task<AgeFormasPagosInstFinaDAO> Actualizar(
            AgeFormasPagosInstFina ageFormasPagosInstFina)
        {
            _dbContext.Entry(ageFormasPagosInstFina).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();

            AgeFormasPagosInstFinaDAO ageFormasPagosInstFinaDAO = await ConsultarPorId(new AgeFormasPagosInstFinaPKDAO
            {
                AgeLicencCodigo = ageFormasPagosInstFina.AgeLicencCodigo,
                Codigo = ageFormasPagosInstFina.Codigo
            });

            return ageFormasPagosInstFinaDAO;
        }


        public async Task<List<AgeFormasPagosInstFinaDAO>> ActualizarVarios(
            List<AgeFormasPagosInstFina> ageFormasPagosInstFinaList)
        {
            _dbContext.AgeFormasPagosInstFinas.UpdateRange(ageFormasPagosInstFinaList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeFormasPagosInstFina, AgeFormasPagosInstFinaDAO>
                .FromEntityToDAOList(ageFormasPagosInstFinaList, FromEntityToDAO);
        }



        private static AgeFormasPagosInstFinaDAO FromEntityToDAO(AgeFormasPagosInstFina entityObject)
        {

            if (entityObject == null)
                return null;

            return new AgeFormasPagosInstFinaDAO
            {
                Id = new AgeFormasPagosInstFinaPKDAO
                {
                    AgeLicencCodigo = entityObject.AgeLicencCodigo,
                    Codigo = entityObject.Codigo
                },
                Descripcion = entityObject.Descripcion,
                AgeForPaAgeLicencCodigo = entityObject.AgeForPaAgeLicencCodigo,
                AgeForPaCodigo = entityObject.AgeForPaCodigo,
                AgeFranquCodigo = entityObject.AgeFranquCodigo,
                AgeInsFiAgeLicencCodigo = entityObject.AgeInsFiAgeLicencCodigo,
                AgeInsFiCodigo = entityObject.AgeInsFiCodigo,
                Estado = entityObject.Estado
            };
        }


    }
}
