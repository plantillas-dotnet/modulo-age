﻿using Microsoft.EntityFrameworkCore;
using AGE.Entities;
using AGE.Entities.DAO.AgeLicenciatariosAplicaSecu;
using AGE.Utils;
using AGE.Utils.Paginacion;
using AGE.Middleware.Exceptions.NotFound;
using Microsoft.EntityFrameworkCore.Storage;

namespace AGE.Repositories.AgeLicenciatariosAplicaSecuRepository
{
    public class AgeLicenciatariosAplicaSecuRepository : IAgeLicenciatariosAplicaSecuRepository
    {
        
        private readonly DbContextAge _dbContext;
        public AgeLicenciatariosAplicaSecuRepository(DbContextAge dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Page<AgeLicenciatariosAplicaSecuDAO>> ConsultarTodos(
            int codigoLicenciatario,
            int codigo,
            int ageLicApAgeAplicaCodigo,
            string ciclica,
            string descripcion,
            Pageable pageable)
        {
            if (_dbContext.AgeLicenciatariosAplicaSecus == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgeLicenciatariosAplicaSecus
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeLicApAgeLicencCodigo == codigoLicenciatario &&
                            (codigo == 0 || p.Codigo == codigo) &&
                            (ageLicApAgeAplicaCodigo == 0 || p.AgeLicApAgeAplicaCodigo == ageLicApAgeAplicaCodigo) &&
                            (ciclica.Equals("") || p.Ciclica.ToLower().Equals(ciclica.ToLower())) &&
                            (descripcion.Equals("") || p.Descripcion.ToLower().Contains(descripcion.ToLower())));

            return await Paginator<AgeLicenciatariosAplicaSecu, AgeLicenciatariosAplicaSecuDAO>
                .Paginar(Query, pageable, FromEntityToDAO);
        }


        public async Task<Page<AgeLicenciatariosAplicaSecuDAO>> ConsultarListaFiltro(
            int codigoLicenciatario,
            string filtro,
            Pageable pageable)
        {
            if (_dbContext.AgeLicenciatariosAplicaSecus == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgeLicenciatariosAplicaSecus
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeLicApAgeLicencCodigo == codigoLicenciatario && (
                            p.Codigo.ToString().ToLower().Contains(filtro) ||
                            p.AgeLicApAgeAplicaCodigo.ToString().ToLower().Contains(filtro) ||
                            p.Ciclica.ToLower().Contains(filtro) ||
                            p.Descripcion.ToLower().Contains(filtro)));

            return await Paginator<AgeLicenciatariosAplicaSecu, AgeLicenciatariosAplicaSecuDAO>
                .Paginar(Query, pageable, FromEntityToDAO);
        }


        public async Task<AgeLicenciatariosAplicaSecu> ConsultarPorSecuencia(
            AgeLicenciatariosAplicaSecuPKDAO ageLicenciatariosAplicaSecuPKDAO)
        {
            if (_dbContext.AgeLicenciatariosAplicaSecus == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeLicenciatariosAplicaSecu? ageLicenciatariosAplicaSecu = await _dbContext.AgeLicenciatariosAplicaSecus
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeLicApAgeLicencCodigo == ageLicenciatariosAplicaSecuPKDAO.ageLicencCodigo &&
                            p.AgeLicApAgeAplicaCodigo == ageLicenciatariosAplicaSecuPKDAO.ageAplicaCodigo &&
                            p.Codigo == ageLicenciatariosAplicaSecuPKDAO.codigo).AsNoTracking()
                            .FirstOrDefaultAsync();

            return ageLicenciatariosAplicaSecu;
        }


        public async Task<AgeLicenciatariosAplicaSecuDAO> ConsultarPorId(
            AgeLicenciatariosAplicaSecuPKDAO ageLicenciatariosAplicaSecuPKDAO)
        {
            if (_dbContext.AgeLicenciatariosAplicaSecus == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeLicenciatariosAplicaSecu? ageLicenciatariosAplicaSecu = await _dbContext.AgeLicenciatariosAplicaSecus
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeLicApAgeLicencCodigo == ageLicenciatariosAplicaSecuPKDAO.ageLicencCodigo &&
                            p.AgeLicApAgeAplicaCodigo == ageLicenciatariosAplicaSecuPKDAO.ageAplicaCodigo &&
                            p.Codigo == ageLicenciatariosAplicaSecuPKDAO.codigo).AsNoTracking()
                            .FirstOrDefaultAsync();

            return FromEntityToDAO(ageLicenciatariosAplicaSecu);
        }


        public async Task<AgeLicenciatariosAplicaSecuDAO> Insertar(AgeLicenciatariosAplicaSecu ageLicenciatariosAplicaSecu)
        {

            _dbContext.AgeLicenciatariosAplicaSecus.Add(ageLicenciatariosAplicaSecu);
            await _dbContext.SaveChangesAsync();

            AgeLicenciatariosAplicaSecuDAO ageLicenciatariosAplicaSecuDAO = await ConsultarPorId(new AgeLicenciatariosAplicaSecuPKDAO {
                ageLicencCodigo = ageLicenciatariosAplicaSecu.AgeLicApAgeLicencCodigo,
                ageAplicaCodigo = ageLicenciatariosAplicaSecu.AgeLicApAgeAplicaCodigo,
                codigo = ageLicenciatariosAplicaSecu.Codigo
            });

            return ageLicenciatariosAplicaSecuDAO;
        }


        public async Task<List<AgeLicenciatariosAplicaSecuDAO>> InsertarVarios(List<AgeLicenciatariosAplicaSecu> ageLicenciatariosAplicaSecuList)
        {
            await _dbContext.AgeLicenciatariosAplicaSecus.AddRangeAsync(ageLicenciatariosAplicaSecuList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeLicenciatariosAplicaSecu, AgeLicenciatariosAplicaSecuDAO>
                .FromEntityToDAOList(ageLicenciatariosAplicaSecuList, FromEntityToDAO);
        }


        public async Task<AgeLicenciatariosAplicaSecuDAO> Actualizar(AgeLicenciatariosAplicaSecu ageLicenciatariosAplicaSecu)
        {

            _dbContext.Entry(ageLicenciatariosAplicaSecu).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();

            AgeLicenciatariosAplicaSecuDAO ageLicenciatariosAplicaSecuDAO = await ConsultarPorId(new AgeLicenciatariosAplicaSecuPKDAO{
                ageLicencCodigo = ageLicenciatariosAplicaSecu.AgeLicApAgeLicencCodigo,
                ageAplicaCodigo = ageLicenciatariosAplicaSecu.AgeLicApAgeAplicaCodigo,
                codigo = ageLicenciatariosAplicaSecu.Codigo
            });

            return ageLicenciatariosAplicaSecuDAO;
        }


        public async Task<List<AgeLicenciatariosAplicaSecuDAO>> ActualizarVarios(List<AgeLicenciatariosAplicaSecu> ageLicenciatariosAplicaSecuList)
        {
            _dbContext.AgeLicenciatariosAplicaSecus.UpdateRange(ageLicenciatariosAplicaSecuList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeLicenciatariosAplicaSecu, AgeLicenciatariosAplicaSecuDAO>
                .FromEntityToDAOList(ageLicenciatariosAplicaSecuList, FromEntityToDAO);
        }


        public void ActualizarSecuencia(AgeLicenciatariosAplicaSecu ageLicenciatariosAplicaSecu)
        {
            using (var dbContext = new DbContextAge())
            {
                dbContext.Entry(ageLicenciatariosAplicaSecu).State = EntityState.Modified;
                dbContext.SaveChanges();
            }
        }
        /*
        public async void ActualizarSecuencia(IDbContextTransaction transaction, AgeLicenciatariosAplicaSecu ageLicenciatariosAplicaSecu)
        {
            using (var dbContext = new DbContextAge())
            {
                await _dbContext.Database.UseTransactionAsync(transaction.GetDbTransaction());
                _dbContext.Entry(ageLicenciatariosAplicaSecu).State = EntityState.Modified;
                await _dbContext.SaveChangesAsync();
            }
        }*/



        private AgeLicenciatariosAplicaSecuDAO FromEntityToDAO(AgeLicenciatariosAplicaSecu entityObject)
        {

            if (entityObject == null)
            {
                return null;
            }

            return new AgeLicenciatariosAplicaSecuDAO
            {
                Id = new AgeLicenciatariosAplicaSecuPKDAO
                {
                    ageLicencCodigo = entityObject.AgeLicApAgeLicencCodigo,
                    ageAplicaCodigo = entityObject.AgeLicApAgeAplicaCodigo,
                    codigo = entityObject.Codigo
                },
                Descripcion = entityObject.Descripcion,
                ValorInicial = entityObject.ValorInicial,
                IncrementaEn = entityObject.IncrementaEn,
                ValorActual = entityObject.ValorActual,
                Ciclica = entityObject.Ciclica,
                Estado = entityObject.Estado
            };

        }

    }
}
