﻿using AGE.Entities;
using AGE.Entities.DAO.AgeLicenciatariosAplicaSecu;
using AGE.Utils.Paginacion;
using Microsoft.EntityFrameworkCore.Storage;

namespace AGE.Repositories.AgeLicenciatariosAplicaSecuRepository
{
    public interface IAgeLicenciatariosAplicaSecuRepository
    {
        Task<Page<AgeLicenciatariosAplicaSecuDAO>> ConsultarTodos(
            int codigoLicenciatario,
            int codigo,
            int ageLicApAgeAplicaCodigo,
            string ciclica,
            string descripcion,
            Pageable pageable);

        Task<Page<AgeLicenciatariosAplicaSecuDAO>> ConsultarListaFiltro(
            int codigoLicenciatario,
            string filtro,
            Pageable pageable);

        Task<AgeLicenciatariosAplicaSecu> ConsultarPorSecuencia(
            AgeLicenciatariosAplicaSecuPKDAO ageLicenciatariosAplicaSecuPKDAO);

        Task<AgeLicenciatariosAplicaSecuDAO> ConsultarPorId(
            AgeLicenciatariosAplicaSecuPKDAO ageLicenciatariosAplicaSecuPKDAO);

        Task<AgeLicenciatariosAplicaSecuDAO> Insertar(AgeLicenciatariosAplicaSecu ageLicenciatariosAplicaSecu);

        Task<List<AgeLicenciatariosAplicaSecuDAO>> InsertarVarios(
            List<AgeLicenciatariosAplicaSecu> ageLicenciatariosAplicaSecuList);

        Task<AgeLicenciatariosAplicaSecuDAO> Actualizar(AgeLicenciatariosAplicaSecu ageLicenciatariosAplicaSecu);

        Task<List<AgeLicenciatariosAplicaSecuDAO>> ActualizarVarios(
            List<AgeLicenciatariosAplicaSecu> ageLicenciatariosAplicaSecuList);

        void ActualizarSecuencia(AgeLicenciatariosAplicaSecu ageLicenciatariosAplicaSecu);
        /*
        void ActualizarSecuencia(
            IDbContextTransaction transaction,
            AgeLicenciatariosAplicaSecu ageLicenciatariosAplicaSecu);*/

    }
}
