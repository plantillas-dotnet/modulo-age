﻿using AGE.Entities.DAO.AgeModalidades;
using AGE.Entities;
using AGE.Utils.Paginacion;

namespace AGE.Repositories.AgeModalidadesRepository
{
    public interface IAgeModalidadRepository
    {
        Task<Page<AgeModalidadesDAO>> ConsultarTodos(
            int codigoLicenciatario,
            int codigo,
            string codigoExterno,
            string descripcion,
            Pageable pageable);

        Task<AgeModalidadesDAO> ConsultarPorId(
            AgeModalidadesPKDAO ageModalidadesPKDAO);

        Task<AgeModalidad> ConsultarCompletePorId(
            AgeModalidadesPKDAO ageModalidadesPKDAO);

        Task<Page<AgeModalidadesDAO>> ConsultarListaFiltro(
            int codigoLicenciatario,
            string filtro,
            Pageable pageable);

        Task<AgeModalidadesDAO> Insertar(
            AgeModalidad ageModalidades);

        Task<AgeModalidadesDAO> Actualizar(
            AgeModalidad ageModalidades);

        Task<List<AgeModalidadesDAO>> InsertarVarios(
            List<AgeModalidad> ageModalidadesList);

        Task<List<AgeModalidadesDAO>> ActualizarVarios(
            List<AgeModalidad> ageModalidadesList);
    }
}
