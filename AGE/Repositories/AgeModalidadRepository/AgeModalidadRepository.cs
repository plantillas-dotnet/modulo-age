﻿using AGE.Entities.DAO.AgeModalidades;
using AGE.Entities;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Utils.Paginacion;
using AGE.Utils;
using Microsoft.EntityFrameworkCore;

namespace AGE.Repositories.AgeModalidadesRepository
{
    public class AgeModalidadRepository : IAgeModalidadRepository
    {
        private readonly DbContextAge _dbContext;

        public AgeModalidadRepository(DbContextAge dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Page<AgeModalidadesDAO>> ConsultarTodos(
            int codigoLicenciatario,
            int Codigo,
            string codigoExterno,
            string descripcion,
            Pageable pageable)
        {
            if (_dbContext.AgeModalidads == null)
            {
                throw new RegisterNotFoundException();
            }

            var query = _dbContext.AgeModalidads
                .Where(a => a.Estado != Globales.ESTADO_ANULADO &&
                            a.AgeLicencCodigo == codigoLicenciatario &&
                            (Codigo == 0 || a.Codigo == Codigo) &&
                            (string.IsNullOrEmpty(codigoExterno) || a.Descripcion.Contains(codigoExterno)) &&
                            (string.IsNullOrEmpty(descripcion) || a.Descripcion.Contains(descripcion)));

            return await Paginator<AgeModalidad, AgeModalidadesDAO>.Paginar(query, pageable, FromEntityToDAO);
        }

        public async Task<Page<AgeModalidadesDAO>> ConsultarListaFiltro(
            int AgeLicencCodigo,
            string filtro,
            Pageable pageable)
        {
            if (_dbContext.AgeModalidads == null)
            {
                throw new RegisterNotFoundException();
            }

            var query = _dbContext.AgeModalidads
                .Where(a => a.Estado != Globales.ESTADO_ANULADO &&
                            a.AgeLicencCodigo == AgeLicencCodigo &&
                            a.Descripcion.ToUpper().Contains(filtro.ToUpper()) ||
                            a.Codigo.ToString().ToUpper().Contains(filtro.ToUpper()) ||
                            a.CodigoExterno.ToUpper().Contains(filtro.ToUpper()));

            return await Paginator<AgeModalidad, AgeModalidadesDAO>.Paginar(query, pageable, FromEntityToDAO);
        }

        public async Task<AgeModalidadesDAO> ConsultarPorId(AgeModalidadesPKDAO ageModalidadesPKDAO)
        {
            if (_dbContext.AgeModalidads == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeModalidad? ageModalidades = await _dbContext.AgeModalidads
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeLicencCodigo == ageModalidadesPKDAO.AgeLicencCodigo &&
                            p.Codigo == ageModalidadesPKDAO.Codigo).FirstOrDefaultAsync();

            return FromEntityToDAO(ageModalidades);
        }

        public async Task<AgeModalidad> ConsultarCompletePorId(AgeModalidadesPKDAO ageModalidadesPK)
        {
            if (_dbContext.AgeModalidads == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeModalidad? ageModalidades = await _dbContext.AgeModalidads
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeLicencCodigo == ageModalidadesPK.AgeLicencCodigo &&
                            p.Codigo == ageModalidadesPK.Codigo).FirstOrDefaultAsync();

            return ageModalidades;
        }

        public async Task<AgeModalidadesDAO> Insertar(AgeModalidad ageModalidades)
        {
            _dbContext.AgeModalidads.Add(ageModalidades);
            await _dbContext.SaveChangesAsync();

            AgeModalidadesDAO ageModalidadesDAO = await ConsultarPorId(new AgeModalidadesPKDAO
            {
                AgeLicencCodigo = ageModalidades.AgeLicencCodigo,
                Codigo = ageModalidades.Codigo
            });

            return ageModalidadesDAO;
        }

        public async Task<List<AgeModalidadesDAO>> InsertarVarios(List<AgeModalidad> ageModalidadesList)
        {
            await _dbContext.AgeModalidads.AddRangeAsync(ageModalidadesList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeModalidad, AgeModalidadesDAO>
                .FromEntityToDAOList(ageModalidadesList, FromEntityToDAO);
        }

        public async Task<AgeModalidadesDAO> Actualizar(AgeModalidad ageModalidades)
        {
            _dbContext.Entry(ageModalidades).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();

            AgeModalidadesDAO ageModalidadesDAO = await ConsultarPorId(new AgeModalidadesPKDAO
            {
                AgeLicencCodigo = ageModalidades.AgeLicencCodigo,
                Codigo = ageModalidades.Codigo
            });

            return ageModalidadesDAO;
        }

        public async Task<List<AgeModalidadesDAO>> ActualizarVarios(List<AgeModalidad> ageModalidadesList)
        {
            _dbContext.AgeModalidads.UpdateRange(ageModalidadesList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeModalidad, AgeModalidadesDAO>
                .FromEntityToDAOList(ageModalidadesList, FromEntityToDAO);
        }

        private static AgeModalidadesDAO FromEntityToDAO(AgeModalidad entityObject)
        {
            if (entityObject == null)
            {
                return null;
            }

            return new AgeModalidadesDAO
            {

                Id = new AgeModalidadesPKDAO
                {
                    AgeLicencCodigo = entityObject.AgeLicencCodigo,
                    Codigo = entityObject.Codigo
                },
                Descripcion = entityObject.Descripcion,
                Estado = entityObject.Estado,
                codigoExterno = entityObject.CodigoExterno
            };
        }
    }
}
