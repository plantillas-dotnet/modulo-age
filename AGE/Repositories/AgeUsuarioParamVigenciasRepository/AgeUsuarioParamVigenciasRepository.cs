﻿using AGE.Entities.DAO.AgeUsuarioParamVigencia;
using AGE.Entities;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Utils.Paginacion;
using AGE.Utils;
using Microsoft.EntityFrameworkCore;
namespace AGE.Repositories.AgeUsuarioParamVigenciaRepository
{
    public class AgeUsuarioParamVigenciasRepository : IAgeUsuarioParamVigenciasRepository
    {
        private readonly DbContextAge _dbContext;

        public AgeUsuarioParamVigenciasRepository(DbContextAge dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Page<AgeUsuarioParamVigenciaDAO>> ConsultarTodos(
            int codigoLicenciatario,
            Pageable pageable)
        {
            if (_dbContext.AgeUsuarioParamVigencias == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgeUsuarioParamVigencias
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeUsuariAgeLicencCodigo == codigoLicenciatario);

            return await Paginator<AgeUsuarioParamVigencias, AgeUsuarioParamVigenciaDAO>.Paginar(Query, pageable, FromEntityToDAO);
        }

        public async Task<Page<AgeUsuarioParamVigenciaDAO>> ConsultarListaFiltro(
            int codigoLicenciatario,
            string filtro,
            Pageable pageable)
        {
            if (_dbContext.AgeUsuarioParamVigencias == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgeUsuarioParamVigencias
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                                p.AgeUsuariAgeLicencCodigo == codigoLicenciatario &&
                               (p.AgeParGeCodigo.ToString().ToLower().Contains(filtro) ||
                                p.AgeUsuariCodigo.ToString().ToLower().Contains(filtro) ||
                                p.Codigo.ToString().ToLower().Contains(filtro) ||
                                p.Observacion.ToLower().Contains(filtro) ||
                                p.FechaDesde.ToString().Contains(filtro) ||
                                p.FechaHasta.ToString().Contains(filtro) ||
                                p.ValorParametro.ToLower().Contains(filtro)
                            ));

            return await Paginator<AgeUsuarioParamVigencias, AgeUsuarioParamVigenciaDAO>.Paginar(Query, pageable, FromEntityToDAO);
        }


        public async Task<AgeUsuarioParamVigenciaDAO> ConsultarPorId(
            AgeUsuarioParamVigenciaPKDAO ageUsuarioParamVigenciaPKDAO)
        {
            if (_dbContext.AgeUsuarioParamVigencias == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeUsuarioParamVigencias? ageUsuarioParamVigencia = await _dbContext.AgeUsuarioParamVigencias
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeUsuariAgeLicencCodigo == ageUsuarioParamVigenciaPKDAO.AgeUsuariAgeLicencCodigo &&
                            p.AgeUsuariCodigo == ageUsuarioParamVigenciaPKDAO.AgeUsuariCodigo &&
                            p.AgeParGeCodigo== ageUsuarioParamVigenciaPKDAO.AgeParGeCodigo &&
                            p.Codigo == ageUsuarioParamVigenciaPKDAO.Codigo).FirstOrDefaultAsync();

            return FromEntityToDAO(ageUsuarioParamVigencia);
        }


        public async Task<AgeUsuarioParamVigencias> ConsultarCompletePorId(AgeUsuarioParamVigenciaPKDAO ageUsuarioParamVigenciaPKDAO)
        {
            if (_dbContext.AgeUsuarioParamVigencias == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeUsuarioParamVigencias? ageUsuarioParamVigencia = await _dbContext.AgeUsuarioParamVigencias
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgeUsuariAgeLicencCodigo == ageUsuarioParamVigenciaPKDAO.AgeUsuariAgeLicencCodigo &&
                            p.AgeUsuariCodigo == ageUsuarioParamVigenciaPKDAO.AgeUsuariCodigo &&
                            p.AgeParGeCodigo == ageUsuarioParamVigenciaPKDAO.AgeParGeCodigo &&
                            p.Codigo == ageUsuarioParamVigenciaPKDAO.Codigo)
                .FirstOrDefaultAsync();

            return ageUsuarioParamVigencia;
        }


        public async Task<AgeUsuarioParamVigenciaDAO> Insertar(AgeUsuarioParamVigencias ageUsuarioParamVigencia)
        {
            _dbContext.AgeUsuarioParamVigencias.Add(ageUsuarioParamVigencia);
            await _dbContext.SaveChangesAsync();

            AgeUsuarioParamVigenciaDAO ageUsuarioParamVigenciasDAO = await ConsultarPorId(new AgeUsuarioParamVigenciaPKDAO
            {
                AgeUsuariAgeLicencCodigo = ageUsuarioParamVigencia.AgeUsuariAgeLicencCodigo,
                AgeParGeCodigo = ageUsuarioParamVigencia.AgeParGeCodigo,
                AgeUsuariCodigo = ageUsuarioParamVigencia.AgeUsuariCodigo,
                Codigo = ageUsuarioParamVigencia.Codigo
            });

            return ageUsuarioParamVigenciasDAO;
        }

        public async Task<List<AgeUsuarioParamVigenciaDAO>> InsertarVarios(
            List<AgeUsuarioParamVigencias> ageUsuarioParamVigenciasList)
        {
            await _dbContext.AgeUsuarioParamVigencias.AddRangeAsync(ageUsuarioParamVigenciasList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeUsuarioParamVigencias, AgeUsuarioParamVigenciaDAO>
                .FromEntityToDAOList(ageUsuarioParamVigenciasList, FromEntityToDAO);
        }


        public async Task<AgeUsuarioParamVigenciaDAO> Actualizar(AgeUsuarioParamVigencias ageUsuarioParamVigencia)
        {
            _dbContext.Entry(ageUsuarioParamVigencia).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();

            AgeUsuarioParamVigenciaDAO ageUsuarioParamVigenciasDAO = await ConsultarPorId(new AgeUsuarioParamVigenciaPKDAO
            {
                AgeUsuariAgeLicencCodigo = ageUsuarioParamVigencia.AgeUsuariAgeLicencCodigo,
                AgeParGeCodigo = ageUsuarioParamVigencia.AgeParGeCodigo,
                AgeUsuariCodigo = ageUsuarioParamVigencia.AgeUsuariCodigo,
                Codigo = ageUsuarioParamVigencia.Codigo
            });

            return ageUsuarioParamVigenciasDAO;
        }


        public async Task<List<AgeUsuarioParamVigenciaDAO>> ActualizarVarios(List<AgeUsuarioParamVigencias> ageUsuarioParamVigenciasList)
        {
            _dbContext.AgeUsuarioParamVigencias.UpdateRange(ageUsuarioParamVigenciasList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeUsuarioParamVigencias, AgeUsuarioParamVigenciaDAO>
                .FromEntityToDAOList(ageUsuarioParamVigenciasList, FromEntityToDAO);
        }


        private static AgeUsuarioParamVigenciaDAO FromEntityToDAO(AgeUsuarioParamVigencias entityObject)
        {

            if (entityObject == null)
            {
                return null;
            }

            return new AgeUsuarioParamVigenciaDAO
            {
                Id = new AgeUsuarioParamVigenciaPKDAO
                {
                    AgeParGeCodigo = entityObject.AgeParGeCodigo,
                    AgeUsuariAgeLicencCodigo = entityObject.AgeUsuariAgeLicencCodigo,
                    AgeUsuariCodigo = entityObject.AgeUsuariCodigo,
                    Codigo = entityObject.Codigo
                },
                Observacion = entityObject.Observacion,
                FechaDesde = entityObject.FechaDesde,
                FechaHasta = entityObject.FechaHasta,
                ValorParametro = entityObject.ValorParametro,
                Estado = entityObject.Estado
            };
        }
    }

}
