﻿using AGE.Entities;
using AGE.Entities.DAO.AgeUsuarioParamVigencia;
using AGE.Utils.Paginacion;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AGE.Repositories.AgeUsuarioParamVigenciaRepository
{
    public interface IAgeUsuarioParamVigenciasRepository
    {
        Task<Page<AgeUsuarioParamVigenciaDAO>> ConsultarTodos(
            int codigoLicenciatario,
            Pageable pageable);

        Task<Page<AgeUsuarioParamVigenciaDAO>> ConsultarListaFiltro(
            int codigoLicenciatario,
            string filtro,
            Pageable pageable);

        Task<AgeUsuarioParamVigenciaDAO> ConsultarPorId(AgeUsuarioParamVigenciaPKDAO ageUsuarioParamVigenciaPKDAO);

        Task<AgeUsuarioParamVigencias> ConsultarCompletePorId(AgeUsuarioParamVigenciaPKDAO ageUsuarioParamVigenciasPKDAO);

        Task<AgeUsuarioParamVigenciaDAO> Insertar(AgeUsuarioParamVigencias ageUsuarioParamVigencia);

        Task<List<AgeUsuarioParamVigenciaDAO>> InsertarVarios(List<AgeUsuarioParamVigencias> ageUsuarioParamVigenciasList);

        Task<AgeUsuarioParamVigenciaDAO> Actualizar(AgeUsuarioParamVigencias ageUsuarioParamVigencia);

        Task<List<AgeUsuarioParamVigenciaDAO>> ActualizarVarios(List<AgeUsuarioParamVigencias> ageUsuarioParamVigenciasList);
    }
}
