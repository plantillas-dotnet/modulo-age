﻿using AGE.Entities;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Utils.Paginacion;
using AGE.Utils;
using Microsoft.EntityFrameworkCore;
using AGE.Entities.DAO.AgeTiposLocalidades;

namespace AGE.Repositories.AgeTiposLocalidadeRepository
{
    public class AgeTiposLocalidadesRepository : IAgeTiposLocalidadesRepository
    {
        private readonly DbContextAge _dbContext;

        public AgeTiposLocalidadesRepository(DbContextAge dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Page<AgeTiposLocalidadeDAO>> ConsultarTodos(
            int codigoPais,
            int codigo,
            int codigoAgeTipLoAgePais,
            int codigoAgeTipLo,
            string descripcion,
            Pageable pageable)
        {
            if (_dbContext.AgeTiposLocalidades == null)
            {
                throw new RegisterNotFoundException();
            }

            var query = _dbContext.AgeTiposLocalidades
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgePaisCodigo == codigoPais);

            return await Paginator<AgeTiposLocalidades, AgeTiposLocalidadeDAO>.Paginar(query, pageable, FromEntityToDAO);
        }

        public async Task<AgeTiposLocalidadeDAO> ConsultarPorId(AgeTiposLocalidadePKDAO ageTiposLocalidadePKDAO)
        {
            if (_dbContext.AgeTiposLocalidades == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeTiposLocalidades? ageTiposLocalidade = await _dbContext.AgeTiposLocalidades
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgePaisCodigo == ageTiposLocalidadePKDAO.agePaisCodigo &&
                            p.Codigo == ageTiposLocalidadePKDAO.codigo).FirstOrDefaultAsync();

            return FromEntityToDAO(ageTiposLocalidade);
        }

        public async Task<AgeTiposLocalidades> ConsultarCompletePorId(AgeTiposLocalidadePKDAO ageTiposLocalidadePKDAO)
        {
            if (_dbContext.AgeTiposLocalidades == null)
            {
                throw new RegisterNotFoundException();
            }

            AgeTiposLocalidades? ageTiposLocalidade = await _dbContext.AgeTiposLocalidades
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgePaisCodigo == ageTiposLocalidadePKDAO.agePaisCodigo &&
                            p.Codigo == ageTiposLocalidadePKDAO.codigo).FirstOrDefaultAsync();

            return ageTiposLocalidade;
        }

        public async Task<Page<AgeTiposLocalidadeDAO>> ConsultarListaFiltro(
            string filtro,
            Pageable pageable)
        {
            if (_dbContext.AgeTiposLocalidades == null)
            {
                throw new RegisterNotFoundException();
            }

            var query = _dbContext.AgeTiposLocalidades
                 .Where(p => p.Estado != Globales.ESTADO_ANULADO &&(
                            p.AgePaisCodigo.ToString().ToLower().Contains(filtro) ||
                            p.Codigo.ToString().ToLower().Contains(filtro) ||
                            p.Descripcion.ToLower().Contains(filtro)));

            return await Paginator<AgeTiposLocalidades, AgeTiposLocalidadeDAO>.Paginar(query, pageable, FromEntityToDAO);
        }

        public async Task<AgeTiposLocalidadeDAO> Insertar(AgeTiposLocalidades ageTiposLocalidade)
        {
            _dbContext.AgeTiposLocalidades.Add(ageTiposLocalidade);
            await _dbContext.SaveChangesAsync();

            AgeTiposLocalidadeDAO ageTiposLocalidadeDAO = await ConsultarPorId(new AgeTiposLocalidadePKDAO
            {
                agePaisCodigo = ageTiposLocalidade.AgePaisCodigo,
                codigo = ageTiposLocalidade.Codigo
            });

            return ageTiposLocalidadeDAO;
        }

        public async Task<List<AgeTiposLocalidadeDAO>> InsertarVarios(List<AgeTiposLocalidades> ageTiposLocalidadeList)
        {
            await _dbContext.AgeTiposLocalidades.AddRangeAsync(ageTiposLocalidadeList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeTiposLocalidades, AgeTiposLocalidadeDAO>
                .FromEntityToDAOList(ageTiposLocalidadeList, FromEntityToDAO);
        }

        public async Task<AgeTiposLocalidadeDAO> Actualizar(AgeTiposLocalidades ageTiposLocalidade)
        {
            _dbContext.Entry(ageTiposLocalidade).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();

            AgeTiposLocalidadeDAO ageTiposLocalidadeDAO = await ConsultarPorId(new AgeTiposLocalidadePKDAO
            {
                agePaisCodigo = ageTiposLocalidade.AgePaisCodigo,
                codigo = ageTiposLocalidade.Codigo
            });

            return ageTiposLocalidadeDAO;
        }

        public async Task<List<AgeTiposLocalidadeDAO>> ActualizarVarios(List<AgeTiposLocalidades> ageTiposLocalidadeList)
        {
            _dbContext.AgeTiposLocalidades.UpdateRange(ageTiposLocalidadeList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgeTiposLocalidades, AgeTiposLocalidadeDAO>
                .FromEntityToDAOList(ageTiposLocalidadeList, FromEntityToDAO);
        }

        private static AgeTiposLocalidadeDAO FromEntityToDAO(AgeTiposLocalidades entityObject)
        {
            if (entityObject == null)
            {
                return null;
            }

            return new AgeTiposLocalidadeDAO
            {
                Id = new AgeTiposLocalidadePKDAO
                {
                    agePaisCodigo = entityObject.AgePaisCodigo,
                    codigo = entityObject.Codigo
                },
                Descripcion = entityObject.Descripcion,
                AgeTipLoAgePaisCodigo = entityObject.AgeTipLoAgePaisCodigo,
                AgeTipLoCodigo = entityObject.AgeTipLoCodigo,
                Estado = entityObject.Estado
            };
        }
    }
}
