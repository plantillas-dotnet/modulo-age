﻿using AGE.Utils.Paginacion;
using AGE.Entities.DAO.AgeTiposLocalidades;
using AGE.Entities;

namespace AGE.Repositories.AgeTiposLocalidadeRepository
{
    public interface IAgeTiposLocalidadesRepository
    {
        Task<Page<AgeTiposLocalidadeDAO>> ConsultarTodos(
            int codigoPais,
            int codigo,
            int codigoAgeTipLoAgePais,
            int codigoAgeTipLo,
            string descripcion,
            Pageable pageable);

        Task<Page<AgeTiposLocalidadeDAO>> ConsultarListaFiltro(
            string filtro,
            Pageable pageable);

        Task<AgeTiposLocalidadeDAO> ConsultarPorId(
            AgeTiposLocalidadePKDAO ageTiposLocalidadePKDAO);

        Task<AgeTiposLocalidades> ConsultarCompletePorId(
           AgeTiposLocalidadePKDAO ageTiposLocalidadePKDAO);

        Task<AgeTiposLocalidadeDAO> Insertar(AgeTiposLocalidades ageTiposLocalidade);

        Task<List<AgeTiposLocalidadeDAO>> InsertarVarios(
            List<AgeTiposLocalidades> ageTiposLocalidadeList);

        Task<AgeTiposLocalidadeDAO> Actualizar(AgeTiposLocalidades ageTiposLocalidade);

        Task<List<AgeTiposLocalidadeDAO>> ActualizarVarios(
            List<AgeTiposLocalidades> ageTiposLocalidadeList);
    }
}
