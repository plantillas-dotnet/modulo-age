﻿using AGE.Entities.DAO.AgePerfilesTransacciones;
using AGE.Entities;
using AGE.Utils.Paginacion;

namespace AGE.Repositories.AgePerfilesTransaccioneRepository
{
    public interface IAgePerfilesTransaccionesRepository
    {
        Task<Page<AgePerfilesTransaccioneDAO>> ConsultarTodos(
            int codigoLicenciatario,
            int codigoPerfil, 
            int codigo, 
            int codigoTransaccion, 
            int codigoAplicacion, 
            Pageable pageable);


        Task<Page<AgePerfilesTransaccioneDAO>> ObtenerTodos(
            int codigoLicenciatario,
            int codigo,
            int agePerfilCodigo,
            int ageTransaAgeAplicaCodigo,
            int ageTransaCodigo,
            Pageable pageable);


        Task<AgePerfilTransaccionDAO> ConsultarPorLicenciatarioPerfil(
            int codigoLicenciatario, 
            int codigoPerfil, 
            int codigoAplicacion);

        Task<Page<AgePerfilesTransaccioneDAO>> ConsultarListaFiltro(
            int codigoLicenciatario,
            string filtro, 
            Pageable pageable);

        Task<AgePerfilesTransaccioneDAO> ConsultarPorId(
            AgePerfilesTransaccionePKDAO agePerfilesTransaccionePKDAO);

        Task<AgePerfilesTransacciones> ConsultarCompletePorId(
            AgePerfilesTransaccionePKDAO agePerfilesTransaccionePKDAO);

        Task<AgePerfilesTransaccioneDAO> Insertar(
            AgePerfilesTransacciones agePerfilesTransaccione);

        Task<List<AgePerfilesTransaccioneDAO>> InsertarVarios
            (List<AgePerfilesTransacciones> agePerfilesTransaccioneList);

        Task<AgePerfilesTransaccioneDAO> Actualizar(
            AgePerfilesTransacciones agePerfilesTransaccione);

        Task<List<AgePerfilesTransaccioneDAO>> ActualizarVarios(
            List<AgePerfilesTransacciones> agePerfilesTransaccioneList);
    }
}
