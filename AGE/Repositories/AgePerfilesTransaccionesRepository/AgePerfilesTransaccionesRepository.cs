﻿using AGE.Entities.DAO.AgePerfiles;
using AGE.Entities;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Utils.Paginacion;
using AGE.Utils;
using Microsoft.EntityFrameworkCore;
using AGE.Entities.DAO.AgePerfilesTransacciones;
using AGE.Entities.DAO.AgeTransacciones;
using AGE.Entities.DAO.AgeUsuarios;

namespace AGE.Repositories.AgePerfilesTransaccioneRepository
{
    public class AgePerfilesTransaccionesRepository : IAgePerfilesTransaccionesRepository
    {
        private readonly DbContextAge _dbContext;
        public AgePerfilesTransaccionesRepository(DbContextAge dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Page<AgePerfilesTransaccioneDAO>> ConsultarTodos(
            int codigoLicenciatario,
            int codigoPerfil,
            int codigo,
            int codigoTransaccion,
            int codigoAplicacion,
            Pageable pageable)
        {
            if (_dbContext.AgePerfilesTransacciones == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgePerfilesTransacciones
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgePerfilAgeLicencCodigo == codigoLicenciatario &&
                            (codigo == 0 || p.Codigo == codigo) &&
                            (codigoPerfil == 0 || p.AgePerfilCodigo == codigoPerfil) &&
                            (codigoTransaccion == 0 || p.AgeTransaCodigo == codigoTransaccion) &&
                            (codigoAplicacion == 0 || p.AgeTransaAgeAplicaCodigo == codigoAplicacion))
                             .Include(y => y.AgeTransa)
                                .ThenInclude(y => y.InverseAgeTransa)
                                .ThenInclude(y => y.InverseAgeTransa)
                                .ThenInclude(y => y.InverseAgeTransa)
                             .Include(x => x.AgePerfil);

            return await Paginator<AgePerfilesTransacciones, AgePerfilesTransaccioneDAO>.Paginar(Query, pageable, FromEntityToDAO);
        }


        public async Task<Page<AgePerfilesTransaccioneDAO>> ObtenerTodos(
            int codigoLicenciatario,
            int codigo,
            int agePerfilCodigo,
            int ageTransaAgeAplicaCodigo,
            int ageTransaCodigo,
            Pageable pageable)
        {
            if (_dbContext.AgePerfilesTransacciones == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgePerfilesTransacciones
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            (codigoLicenciatario == 0 || p.AgePerfilAgeLicencCodigo == codigoLicenciatario) &&
                            (codigo == 0 || p.Codigo == codigo) &&
                            (agePerfilCodigo == 0 || p.AgePerfilCodigo == agePerfilCodigo) &&
                            (ageTransaCodigo == 0 || p.AgeTransaCodigo == ageTransaCodigo) &&
                            (ageTransaAgeAplicaCodigo == 0 || p.AgeTransaAgeAplicaCodigo == ageTransaAgeAplicaCodigo))
                             .Include(y => y.AgeTransa)
                                .ThenInclude(y => y.InverseAgeTransa)
                                .ThenInclude(y => y.InverseAgeTransa)
                                .ThenInclude(y => y.InverseAgeTransa)
                             .Include(x => x.AgePerfil);

            return await Paginator<AgePerfilesTransacciones, AgePerfilesTransaccioneDAO>.Paginar(Query, pageable, FromEntityToDAO);
        }


        public async Task<AgePerfilTransaccionDAO> ConsultarPorLicenciatarioPerfil(
            int codigoLicenciatario,
            int codigoPerfil,
            int codigoAplicacion)
        {
            if (_dbContext.AgePerfilesTransacciones == null)
            {
                throw new RegisterNotFoundException();
            }

            List<AgePerfilesTransacciones> agePerfilesTransaccioneList = await _dbContext.AgePerfilesTransacciones
                                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                                            p.AgePerfilAgeLicencCodigo == codigoLicenciatario &&
                                            p.AgePerfilCodigo == codigoPerfil &&
                                            (codigoAplicacion == 0 || p.AgeTransaAgeAplicaCodigo == codigoAplicacion))
                                .Include(y => y.AgeTransa)
                                    .ThenInclude(y => y.InverseAgeTransa)
                                    .ThenInclude(y => y.InverseAgeTransa)
                                    .ThenInclude(y => y.InverseAgeTransa)
                                .Include(x => x.AgePerfil).ToListAsync();


            AgePerfilTransaccionDAO agePerfilTransaccionDAO = new AgePerfilTransaccionDAO();

            List<AgeTransacciones> ageTransacciones = new List<AgeTransacciones>();

            foreach (AgePerfilesTransacciones agePerfilesTransaccione in agePerfilesTransaccioneList)
            {
                if (agePerfilesTransaccione.AgeTransa != null)
                    ageTransacciones.Add(agePerfilesTransaccione.AgeTransa);
            }

            List<AgeTransaccioneDAO> ageTransaccionesDAO = ConversorEntityToDAOList<AgeTransacciones, AgeTransaccioneDAO>
                                                            .FromEntityToDAOList(ageTransacciones, FromEntityToDAO);

            agePerfilTransaccionDAO.AgePerfiles = FromEntityToDAO(agePerfilesTransaccioneList.FirstOrDefault().AgePerfil);
            agePerfilTransaccionDAO.AgeTransaccionesList = ageTransaccionesDAO;

            return agePerfilTransaccionDAO;

        }

        public async Task<Page<AgePerfilesTransaccioneDAO>> ConsultarListaFiltro(
            int codigoLicenciatario,
            string filtro,
            Pageable pageable)
        {
            if (_dbContext.AgePerfilesTransacciones == null)
            {
                throw new RegisterNotFoundException();
            }

            var Query = _dbContext.AgePerfilesTransacciones
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgePerfilAgeLicencCodigo == codigoLicenciatario &&
                            (p.Codigo.ToString().ToLower().Contains(filtro) ||
                            p.AgeTransaAgeAplicaCodigo.ToString().ToLower().Contains(filtro) ||
                            p.AgeTransaCodigo.ToString().ToLower().Contains(filtro)))
                                .Include(y => y.AgeTransa)
                                    .ThenInclude(y => y.InverseAgeTransa)
                                    .ThenInclude(y => y.InverseAgeTransa)
                                    .ThenInclude(y => y.InverseAgeTransa)
                                .Include(x => x.AgePerfil);

            return await Paginator<AgePerfilesTransacciones, AgePerfilesTransaccioneDAO>.Paginar(Query, pageable, FromEntityToDAO);
        }


        public async Task<AgePerfilesTransaccioneDAO> ConsultarPorId(AgePerfilesTransaccionePKDAO agePerfilesTransaccionePKDAO)
        {
            if (_dbContext.AgePerfilesTransacciones == null)
            {
                throw new RegisterNotFoundException();
            }

            AgePerfilesTransacciones? agePerfilesTransaccione = await _dbContext.AgePerfilesTransacciones
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgePerfilAgeLicencCodigo == agePerfilesTransaccionePKDAO.agePerfilAgeLicencCodigo &&
                            p.AgePerfilCodigo == agePerfilesTransaccionePKDAO.agePerfilCodigo &&
                            p.Codigo == agePerfilesTransaccionePKDAO.codigo)
                                .Include(y => y.AgeTransa)
                                    .ThenInclude(y => y.InverseAgeTransa)
                                    .ThenInclude(y => y.InverseAgeTransa)
                                    .ThenInclude(y => y.InverseAgeTransa)
                                .Include(x => x.AgePerfil)
                                .FirstOrDefaultAsync();

            return FromEntityToDAO(agePerfilesTransaccione);
        }


        public async Task<AgePerfilesTransacciones> ConsultarCompletePorId(AgePerfilesTransaccionePKDAO agePerfilesTransaccionePKDAO)
        {
            if (_dbContext.AgePerfilesTransacciones == null)
            {
                throw new RegisterNotFoundException();
            }

            AgePerfilesTransacciones? agePerfilesTransaccione = await _dbContext.AgePerfilesTransacciones
                .Where(p => p.Estado != Globales.ESTADO_ANULADO &&
                            p.AgePerfilAgeLicencCodigo == agePerfilesTransaccionePKDAO.agePerfilAgeLicencCodigo &&
                            p.AgePerfilCodigo == agePerfilesTransaccionePKDAO.agePerfilAgeLicencCodigo &&
                            p.Codigo == agePerfilesTransaccionePKDAO.codigo)
                                .Include(y => y.AgeTransa)
                                    .ThenInclude(y => y.InverseAgeTransa)
                                    .ThenInclude(y => y.InverseAgeTransa)
                                    .ThenInclude(y => y.InverseAgeTransa)
                                .Include(x => x.AgePerfil)
                                .FirstOrDefaultAsync();

            return agePerfilesTransaccione;
        }

        public async Task<AgePerfilesTransaccioneDAO> Insertar(
            AgePerfilesTransacciones agePerfilesTransaccione)
        {
            _dbContext.AgePerfilesTransacciones.Add(agePerfilesTransaccione);
            await _dbContext.SaveChangesAsync();

            AgePerfilesTransaccioneDAO agePerfilesTransaccioneDAO = await ConsultarPorId(new AgePerfilesTransaccionePKDAO
            {
                agePerfilAgeLicencCodigo = agePerfilesTransaccione.AgePerfilAgeLicencCodigo,
                agePerfilCodigo = agePerfilesTransaccione.AgePerfilCodigo,
                codigo = agePerfilesTransaccione.Codigo
            });

            return agePerfilesTransaccioneDAO;
        }

        public async Task<List<AgePerfilesTransaccioneDAO>> InsertarVarios(
            List<AgePerfilesTransacciones> agePerfilesTransaccioneList)
        {
            await _dbContext.AgePerfilesTransacciones.AddRangeAsync(agePerfilesTransaccioneList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgePerfilesTransacciones, AgePerfilesTransaccioneDAO>
                .FromEntityToDAOList(agePerfilesTransaccioneList, FromEntityToDAO);
        }


        public async Task<AgePerfilesTransaccioneDAO> Actualizar(AgePerfilesTransacciones agePerfilesTransaccione)
        {
            _dbContext.Entry(agePerfilesTransaccione).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();

            AgePerfilesTransaccioneDAO agePerfilesTransaccioneDAO = await ConsultarPorId(new AgePerfilesTransaccionePKDAO
            {
                agePerfilAgeLicencCodigo = agePerfilesTransaccione.AgePerfilAgeLicencCodigo,
                agePerfilCodigo = agePerfilesTransaccione.AgePerfilCodigo,
                codigo = agePerfilesTransaccione.Codigo
            });

            return agePerfilesTransaccioneDAO;
        }


        public async Task<List<AgePerfilesTransaccioneDAO>> ActualizarVarios(List<AgePerfilesTransacciones> agePerfilesTransaccioneList)
        {
            _dbContext.AgePerfilesTransacciones.UpdateRange(agePerfilesTransaccioneList);
            await _dbContext.SaveChangesAsync();

            return ConversorEntityToDAOList<AgePerfilesTransacciones, AgePerfilesTransaccioneDAO>
                .FromEntityToDAOList(agePerfilesTransaccioneList, FromEntityToDAO);
        }

        private static AgePerfilesTransaccioneDAO FromEntityToDAO(AgePerfilesTransacciones entityObject)
        {
            if (entityObject == null)
                return null;

            AgePerfilesDAO agePerfilDAO = FromEntityToDAO(entityObject.AgePerfil);
            AgeTransaccioneDAO ageTransaDAO = FromEntityToDAO(entityObject.AgeTransa);

            return new AgePerfilesTransaccioneDAO
            {
                Id = new AgePerfilesTransaccionePKDAO
                {
                    agePerfilAgeLicencCodigo = entityObject.AgePerfilAgeLicencCodigo,
                    agePerfilCodigo = entityObject.AgePerfilCodigo,
                    codigo = entityObject.Codigo
                },
                AgeTransaAgeAplicaCodigo = entityObject.AgeTransaAgeAplicaCodigo,
                AgeTransaCodigo = entityObject.AgeTransaCodigo,
                Estado = entityObject.Estado,
                AgePerfiles = agePerfilDAO,
                AgeTransacciones = ageTransaDAO
            };
        }

        private static AgePerfilesDAO FromEntityToDAO(AgePerfiles entityObject)
        {
            if (entityObject == null)
                return null;

            return new AgePerfilesDAO
            {
                Id = new AgePerfilesPKDAO
                {
                    ageLicencCodigo = entityObject.AgeLicencCodigo,
                    codigo = entityObject.Codigo
                },
                Descripcion = entityObject.Descripcion,
                Estado = entityObject.Estado
            };
        }

        private static AgeTransaccioneDAO FromEntityToDAO(AgeTransacciones entityObject)
        {
            if (entityObject == null)
                return null;

            List<AgeTransaccioneDAO> ageTransaccioneDAOList = new List<AgeTransaccioneDAO>();

            foreach (AgeTransacciones ageTransaccione in entityObject.InverseAgeTransa)
            {
                AgeTransaccioneDAO ageTransaccioneDAO = FromEntityToDAO(ageTransaccione);
                ageTransaccioneDAOList.Add(ageTransaccioneDAO);
            }

            return new AgeTransaccioneDAO
            {
                Id = new AgeTransaccionePKDAO
                {
                    ageAplicaCodigo = entityObject.AgeAplicaCodigo,
                    codigo = entityObject.Codigo
                },
                CodigoExterno = entityObject.CodigoExterno,
                Descripcion = entityObject.Descripcion,
                Transaccion = entityObject.Transaccion,
                OrdenPresentacion = entityObject.OrdenPresentacion,
                TipoTransaccion = entityObject.TipoTransaccion,
                TipoOperacion = entityObject.TipoOperacion,
                AgeTransaAgeAplicaCodigo = entityObject.AgeTransaAgeAplicaCodigo,
                AgeTransaCodigo = entityObject.AgeTransaCodigo,
                Opcion = entityObject.Opcion,
                Nivel = entityObject.Nivel,
                Url = entityObject.Url,
                Parametros = entityObject.Parametros,
                Estado = entityObject.Estado,
                AgeTransaccioneList = ageTransaccioneDAOList
            };
        }

    }
}

