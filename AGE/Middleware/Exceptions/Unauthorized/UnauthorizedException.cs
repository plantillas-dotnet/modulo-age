﻿using Microsoft.AspNetCore.WebUtilities;
using System.Net;

namespace AGE.Middleware.Exceptions.Unauthorized
{
    public class UnauthorizedException : Exception
    {
        public UnauthorizedException(string mensaje) : base(mensaje)
        {
        }
    }
}
