﻿namespace AGE.Middleware.Exceptions.Unauthorized
{
    public class NotAuthenticatedException : UnauthorizedException
    {
        public NotAuthenticatedException() : base("Es necesario autenticarse para obtener la respuesta solicitada.")
        {
        }
    }
}
