﻿namespace AGE.Firebase.Models
{
    public enum NotificationParameter
    {
        SOUND,
        COLOR
    }

    public static class NotificationParameterExtensions
    {
        public static string GetValue(this NotificationParameter notificationParameter)
        {
            switch (notificationParameter)
            {
                case NotificationParameter.SOUND:
                    return "default";

                case NotificationParameter.COLOR:
                    return "#FFFF00";

                default:
                    return null;
            }
        }
    }
}
