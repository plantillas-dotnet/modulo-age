﻿namespace AGE.Firebase.Models
{
    public class PushNotificationResponse
    {

        public PushNotificationResponse(int status, string message)
        {
            Status = status;
            Message = message;
        }


        public int Status { get; set; }

        public string Message { get; set; }
    }
}
