﻿namespace AGE.Firebase.Models
{
    public class PushNotificationRequest
    {

        public PushNotificationRequest(string title, string message, string topic)
        {
            Title = title;
            Message = message;
            Topic = topic;
        }

        public PushNotificationRequest()
        {
        }


        public string Title { get; set; }

        public string Message { get; set; }

        public string Topic { get; set; }

        public string? Token { get; set; }

        public long? CodigoUsuario { get; set; }

        public long? CodigoPaquete { get; set; }

        public int? CodigoLicenciatario { get; set; }

        public string? Accion { get; set; }

        public string? Imagen { get; set; }

    }
}
