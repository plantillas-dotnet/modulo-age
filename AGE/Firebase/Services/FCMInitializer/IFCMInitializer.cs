﻿namespace AGE.Firebase.Services.FCMInitializer
{
    public interface IFCMInitializer
    {

        void Initialize();

    }
}
