﻿using FirebaseAdmin;
using Google.Apis.Auth.OAuth2;

namespace AGE.Firebase.Services.FCMInitializer
{
    public class FCMInitializer : IFCMInitializer
    {

        public FCMInitializer() 
        {
            Initialize();
        }

        public void Initialize()
        {
            // @Value("${app.firebase-configuration-file}")
            // Ruta al archivo de configuración de Firebase (por ejemplo: "firebase-config.json")
            string configPath = "Path_Config_File";

            try
            {

                FirebaseApp app = FirebaseApp.Create(new AppOptions()
                {
                    Credential = GoogleCredential.FromFile(configPath)
                });

                if (app != null)
                    Console.WriteLine("Firebase application has been initialized");

            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error initializing Firebase: {ex.Message}");
            }
        }

    }
}
