﻿using AGE.Entities;
using AGE.Entities.DAO.AgeUsuarios;
using AGE.Firebase.Models;
using AGE.Firebase.Services.FCMService;
using AGE.Middleware.Exceptions.BadRequest;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Services.AgeUsuarioService;

namespace AGE.Firebase.Services.PushNotificationService
{
    public class PushNotificationService : IPushNotificationService
    {

        //@Value("#{${app.notifications.defaults}}")
        private Dictionary<string, string> defaults;

        private readonly IFCMService _fcmService;
        private readonly IAgeUsuariosService _serviceUsuario;

        public PushNotificationService(
            IFCMService fcmService, 
            IAgeUsuariosService serviceUsuario)
        {
            _fcmService = fcmService;
            _serviceUsuario = serviceUsuario;
        }

        public void SendPushNotification(PushNotificationRequest request)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(request.Topic))
                    throw new InvalidFieldException("Topico no puede ser nulo ");

                if (string.IsNullOrWhiteSpace(request.Accion) 
                        || (request.CodigoPaquete == null || request.CodigoPaquete < 1)
                        || (request.CodigoLicenciatario == null || request.CodigoLicenciatario < 1))
                    throw new InvalidFieldException("Los datos no pueden estar vacíos.");

                Dictionary<string, string> pushData = new Dictionary<string, string>();
                pushData["accion"] = request.Accion;
                pushData["paqueteId"] = request.CodigoPaquete.ToString();
                pushData["licenciatario"] = request.CodigoLicenciatario.ToString();

                _fcmService.SendMessage(pushData, request);
            }
            catch (Exception)
            {
                throw new PushNotificationException();
            }
        }

        public void SendPushNotificationToken(
            PushNotificationRequest request, 
            Dictionary<string, string>? pushData)
        {
            try
            {

                if (string.IsNullOrWhiteSpace(request.Token))
                {
                    if (request.CodigoUsuario == null || request.CodigoUsuario < 1 || 
                        request.CodigoLicenciatario == null || request.CodigoLicenciatario < 1)
                        throw new InvalidFieldException("Id de usuario no encontrado.");
                }

                if (string.IsNullOrWhiteSpace(request.Accion)
                        || (request.CodigoPaquete == null || request.CodigoPaquete < 1))
                    throw new InvalidFieldException("Los datos no pueden estar vacíos.");

                AgeUsuarioDAO ageUsuarioDAO = _serviceUsuario.ConsultarPorId(new AgeUsuarioPKDAO
                {
                    ageLicencCodigo = request.CodigoLicenciatario ?? 0,
                    codigo = request.CodigoUsuario ?? 0
                }).Result;

                if (ageUsuarioDAO == null)
                    throw new RegisterNotFoundException();
                else
                    request.Token = ageUsuarioDAO.TokenFirebase;

                pushData ??= new Dictionary<string, string>();

                pushData["accion"] = request.Accion;
                pushData["paqueteId"] = request.CodigoPaquete.ToString();
                pushData["licenciatario"] = request.CodigoLicenciatario.ToString();

                _fcmService.SendMessageToken(pushData, request);
            }
            catch (Exception)
            {
                throw new PushNotificationException();
            }
        }


        public void SendPushNotificationWithoutData(PushNotificationRequest request)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(request.Topic))
                    throw new InvalidFieldException("Topico no puede ser nulo ");

                _fcmService.SendMessageWithoutData(request);
            }
            catch (Exception)
            {
                throw new PushNotificationException();
            }
        }


        public void SendPushNotificationToToken(PushNotificationRequest request)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(request.Token))
                {
                    if (request.CodigoUsuario == null || request.CodigoUsuario < 1 ||
                        request.CodigoLicenciatario == null || request.CodigoLicenciatario < 1)
                        throw new InvalidFieldException("Id de usuario no encontrado.");
                }

                AgeUsuarioDAO ageUsuarioDAO = _serviceUsuario.ConsultarPorId(new AgeUsuarioPKDAO
                {
                    ageLicencCodigo = request.CodigoLicenciatario ?? 0,
                    codigo = request.CodigoUsuario ?? 0
                }).Result;

                if (ageUsuarioDAO == null)
                    throw new RegisterNotFoundException();
                else
                    request.Token = ageUsuarioDAO.TokenFirebase;

                _fcmService.SendMessageToToken(request);
            }
            catch (Exception)
            {
                throw new PushNotificationException();
            }
        }


        private Dictionary<string, string> GetSamplePayloadData()
        {
            Dictionary<string, string> pushData = new Dictionary<string, string>();
            pushData["messageId"] = defaults["payloadMessageId"];
            pushData["text"] = defaults["payloadData"] + " " + DateTime.Now;
            return pushData;
        }


        private PushNotificationRequest GetSamplePushNotificationRequest()
        {
            PushNotificationRequest request = new PushNotificationRequest(
                defaults["title"],
                defaults["message"],
                defaults["topic"]);
            return request;
        }

    }
}
