﻿using AGE.Firebase.Models;

namespace AGE.Firebase.Services.PushNotificationService
{
    public interface IPushNotificationService
    {

        void SendPushNotification(PushNotificationRequest request);

        void SendPushNotificationToken(PushNotificationRequest request, Dictionary<string, string>? pushData);

        void SendPushNotificationWithoutData(PushNotificationRequest request);

        void SendPushNotificationToToken(PushNotificationRequest request);
    }
}
