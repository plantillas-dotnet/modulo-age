﻿using AGE.Firebase.Models;

namespace AGE.Firebase.Services.FCMService
{
    public interface IFCMService
    {
        void SendMessage(Dictionary<string, string> data, PushNotificationRequest request);

        void SendMessageToken(Dictionary<string, string> data, PushNotificationRequest request);

        void SendMessageWithoutData(PushNotificationRequest request);

        void SendMessageToToken(PushNotificationRequest request);
    }
}
