﻿using AGE.Firebase.Models;
using FirebaseAdmin.Messaging;

namespace AGE.Firebase.Services.FCMService
{
    public class FCMService : IFCMService
    {

        private const string CHANNEL_ID = "sasf-omni-channel";
        private const string CLICK_ACTION = "FCM_PLUGIN_ACTIVITY";


        public void SendMessage(Dictionary<string, string> data, PushNotificationRequest request)
        {
            Message message = GetPreconfiguredMessageWithData(data, request);
            string response = SendAndGetResponse(message);
            Console.WriteLine("Sent message with data. Topic: " + request.Topic + ", " + response);
        }


        public void SendMessageToken(Dictionary<string, string> data, PushNotificationRequest request)
        {
            Message message = GetPreconfiguredMessageWithDataToken(data, request);
            Console.WriteLine("Sent message with data. Token: " + message);
            string response = SendAndGetResponse(message);
            Console.WriteLine("Sent message with data. Token: " + request.Token + ", " + response);
        }


        public void SendMessageWithoutData(PushNotificationRequest request)
        {
            Message message = GetPreconfiguredMessageWithoutData(request);
            string response = SendAndGetResponse(message);
            Console.WriteLine("Sent message without data. Topic: " + request.Topic + ", " + response);
        }


        public void SendMessageToToken(PushNotificationRequest request)
        {
            Message message = GetPreconfiguredMessageToToken(request);
            string response = SendAndGetResponse(message);
            Console.WriteLine("Sent message to token. Device token: " + request.Token + ", " + response);
        }


        private string SendAndGetResponse(Message message)
        {
            return FirebaseMessaging.DefaultInstance.SendAsync(message).Result;
        }

        private AndroidConfig GetAndroidConfig(string topic)
        {
            return new AndroidConfig
            {
                TimeToLive = TimeSpan.FromMinutes(2),
                CollapseKey = topic,
                Priority = Priority.High,
                Notification = new AndroidNotification
                {
                    Sound = NotificationParameter.SOUND.GetValue(),
                    Color = NotificationParameter.COLOR.GetValue(),
                    Tag = topic,
                    ChannelId = CHANNEL_ID,
                    ClickAction = CLICK_ACTION
                }
            };

        }


        private Message GetPreconfiguredMessageToToken(PushNotificationRequest request)
        {
            Message message = GetPreconfiguredMessageBuilder(request);
            message.Token = request.Token;

            return message;
        }


        private Message GetPreconfiguredMessageWithoutData(
            PushNotificationRequest request)
        {
            Message message = GetPreconfiguredMessageBuilder(request);
            message.Topic = request.Topic;

            return message;
        }


        private Message GetPreconfiguredMessageWithData(
            Dictionary<string, string> data,
            PushNotificationRequest request)
        {
            Message message = GetPreconfiguredMessageBuilder(request);
            message.Data = data;
            message.Topic = request.Topic;

            return message;
        }


        private Message GetPreconfiguredMessageWithDataToken(
            Dictionary<string, string> data,
            PushNotificationRequest request)
        {
            Message message = GetPreconfiguredMessageBuilder(request);
            message.Data = data;
            message.Token = request.Token;

            return message;
        }


        private Message GetPreconfiguredMessageBuilder(
            PushNotificationRequest request)
        {
            AndroidConfig androidConfig = GetAndroidConfig(request.Topic);

            return new Message()
            {
                Android = androidConfig,
                Notification = new Notification
                {
                    Title = request.Title,
                    ImageUrl = request.Imagen,
                    Body = request.Message
                }
            };
        }

    }
}
