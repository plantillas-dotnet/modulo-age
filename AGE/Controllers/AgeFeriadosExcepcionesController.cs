using AGE.Entities.DAO.AgeFeriadosExcepciones;
using AGE.Services.AgeFeriadosExcepcionesService;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace AGE.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class AgeFeriadosExcepcionesController : ControllerBase
    {
        private readonly IAgeFeriadosExcepcionesService _service;

        public AgeFeriadosExcepcionesController(IAgeFeriadosExcepcionesService Service)
        {
            _service = Service;
        }

        [HttpGet("{codigoLicenciatario}")]
        [ProducesResponseType(typeof(Page<AgeFeriadosExcepcionesDAO>), 200)]
        public async Task<IActionResult> ConsultarTodos([FromRoute] int codigoLicenciatario,
                                                        [FromQuery] string tipoExcepcion,
                                                        [FromQuery] Pageable pageable)
        {
            Page<AgeFeriadosExcepcionesDAO> ageFeriadosExcepcionesDAOList =
                await _service.ConsultarTodos(codigoLicenciatario, tipoExcepcion, pageable);

            return Ok(ageFeriadosExcepcionesDAOList);
        }

        [HttpGet("{codigoLicenciatario}/{id}")]
        [ProducesResponseType(typeof(AgeFeriadosExcepcionesDAO), 200)]
        public async Task<IActionResult> ConsultarPorId(
            [FromRoute] AgeFeriadosExcepcionesPKDAO ageFeriadosExcepcionesPKDAO)
        {
            var ageFeriadosExcepcionesDAO = await _service.ConsultarPorId(ageFeriadosExcepcionesPKDAO);
            return Ok(ageFeriadosExcepcionesDAO);
        }

        [HttpGet("filtro/{codigoLicenciatario}")]
        [ProducesResponseType(typeof(Page<AgeFeriadosExcepcionesDAO>), 200)]
        public async Task<IActionResult> ConsultarListaFiltro(
           [FromRoute, Required] int codigoLicenciatario,
           [FromQuery, Required] string filtro,
           [FromQuery] Pageable pageable)
        {
            Page<AgeFeriadosExcepcionesDAO> ageFeriadosExcepcionesDAO =
                await _service.ConsultarListaFiltro(codigoLicenciatario, filtro, pageable);

            return Ok(ageFeriadosExcepcionesDAO);
        }

        [HttpPost]
        [ProducesResponseType(typeof(Resource<AgeFeriadosExcepcionesDAO>), 200)]
        public async Task<IActionResult> Insertar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeFeriadosExcepcionesSaveDAO ageFeriadosExcepcionesSaveDAO)
        {
            Resource<AgeFeriadosExcepcionesDAO> ageFeriadosExcepcionesDAO =
                await _service.Insertar(_httpContextAccessor, ageFeriadosExcepcionesSaveDAO);

            return Ok(ageFeriadosExcepcionesDAO);
        }

        [HttpPost("varios")]
        [ProducesResponseType(typeof(List<Resource<AgeFeriadosExcepcionesDAO>>), 200)]
        public async Task<IActionResult> InsertarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeFeriadosExcepcionesSaveDAO> ageFeriadosExcepcionesSaveDAOList)
        {
            List<Resource<AgeFeriadosExcepcionesDAO>> ageFeriadosExcepcionesDAOList =
                await _service.InsertarVarios(_httpContextAccessor, ageFeriadosExcepcionesSaveDAOList);

            return Ok(ageFeriadosExcepcionesDAOList);
        }

        [HttpPut]
        [ProducesResponseType(typeof(AgeFeriadosExcepcionesDAO), 200)]
        public async Task<IActionResult> Actualizar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeFeriadosExcepcionesSaveDAO ageFeriadosExcepcionesSaveDAO)
        {
            AgeFeriadosExcepcionesDAO ageFeriadosExcepcionesDAO = await _service.Actualizar(_httpContextAccessor, ageFeriadosExcepcionesSaveDAO);
            return Ok(ageFeriadosExcepcionesDAO);
        }

        [HttpPut("varios")]
        [ProducesResponseType(typeof(List<AgeFeriadosExcepcionesDAO>), 200)]
        public async Task<IActionResult> ActualizarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeFeriadosExcepcionesSaveDAO> ageFeriadosExcepcionesSaveDAOList)
        {
            List<AgeFeriadosExcepcionesDAO> ageFeriadosExcepcionesDAOList =
                await _service.ActualizarVarios(_httpContextAccessor, ageFeriadosExcepcionesSaveDAOList);

            return Ok(ageFeriadosExcepcionesDAOList);
        }
    }
}
