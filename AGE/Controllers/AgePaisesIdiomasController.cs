using AGE.Entities.DAO.AgePaisesIdioma;
using AGE.Entities.DAO.AgeTiposPersonas;
using AGE.Services.AgePaisesIdiomaService;
using AGE.Services.AgeTiposLocalidadeService;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace AGE.Controllers
{
    [Route("[controller]")]
    [ApiController]
    //[Authorize]
    public class AgePaisesIdiomasController : ControllerBase
    {
        private readonly IAgePaisesIdiomasService _service;

        public AgePaisesIdiomasController(IAgePaisesIdiomasService service)
        {
            _service = service;
        }

        [HttpGet("{codigoIdioma}")]
        [ProducesResponseType(typeof(Page<AgePaisesIdiomasDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarTodos(
            [FromRoute] int codigoIdioma,
            [FromQuery] int? codigoPais,
            [FromQuery] string? descripcion,
            [FromQuery] Pageable pageable)
        {
            Page<AgePaisesIdiomasDAO> agePaisesIdiomaDAOList =
                await _service.ConsultarTodos(codigoIdioma, codigoPais ?? 0,
                                              descripcion ?? "", pageable);

            return Ok(agePaisesIdiomaDAOList);
        }

        [HttpGet("{codigoIdioma}/{codigoPais}")]
        [ProducesResponseType(typeof(AgePaisesIdiomasDAO), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarPorId(
            [FromRoute] AgePaisesIdiomasPKDAO agePaisesIdiomaPKDAO)
        {
            AgePaisesIdiomasDAO agePaisesIdiomaDAO = await _service.ConsultarPorId(agePaisesIdiomaPKDAO);
            return Ok(agePaisesIdiomaDAO);
        }

        [HttpGet("filtro")]
        [ProducesResponseType(typeof(Page<AgePaisesIdiomasDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarListaFiltro(
            [FromQuery, Required] string filtro,
            [FromQuery] Pageable pageable)
        {
            Page<AgePaisesIdiomasDAO> agePaisesIdiomaDAOList =
                await _service.ConsultarListaFiltro(filtro, pageable);

            return Ok(agePaisesIdiomaDAOList);
        }


        [HttpPost]
        [ProducesResponseType(typeof(Resource<AgePaisesIdiomasDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> Insertar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgePaisesIdiomasSaveDAO ageTiposLocalidadeSaveDAO)
        {
            Resource<AgePaisesIdiomasDAO> agePaisesIdiomaDAO = 
                await _service.Insertar(_httpContextAccessor, ageTiposLocalidadeSaveDAO);
            return Ok(agePaisesIdiomaDAO);
        }


        [HttpPost("varios")]
        [ProducesResponseType(typeof(List<Resource<AgePaisesIdiomasDAO>>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> InsertarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgePaisesIdiomasSaveDAO> agePaisesIdiomaSaveDAOList)
        {
            List<Resource<AgePaisesIdiomasDAO>> agePaisesIdiomaDAOList =
                await _service.InsertarVarios(_httpContextAccessor, agePaisesIdiomaSaveDAOList);

            return Ok(agePaisesIdiomaDAOList);
        }


        [HttpPut]
        [ProducesResponseType(typeof(AgePaisesIdiomasDAO), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> Actualizar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgePaisesIdiomasSaveDAO agePaisesIdiomaSaveDAO)
        {
            AgePaisesIdiomasDAO agePaisesIdiomaDAO = await _service.Actualizar(
                _httpContextAccessor, agePaisesIdiomaSaveDAO);
            return Ok(agePaisesIdiomaDAO);
        }


        [HttpPut("varios")]
        [ProducesResponseType(typeof(List<AgePaisesIdiomasDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ActualizarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgePaisesIdiomasSaveDAO> agePaisesIdiomaSaveDAOList)
        {
            List<AgePaisesIdiomasDAO> agePaisesIdiomaDAOList =
                await _service.ActualizarVarios(_httpContextAccessor, agePaisesIdiomaSaveDAOList);

            return Ok(agePaisesIdiomaDAOList);
        }
    }
}
