﻿using AGE.Entities.DAO.AgeIdioma;
using AGE.Services.AgeIdiomaService;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace AGE.Controllers
{
    [Route("[controller]")]
    [ApiController]
    //[Authorize]
    public class AgeIdiomasController : ControllerBase
    {
        private readonly IAgeIdiomasService _service;

        public AgeIdiomasController(IAgeIdiomasService Service)
        {
            _service = Service;
        }

        [HttpGet]
        [ProducesResponseType(typeof(Page<AgeIdiomasDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarTodos(
            [FromQuery] int codigo,
            [FromQuery] string? descripcion,
            [FromQuery] Pageable pageable)
        {
            Page<AgeIdiomasDAO> ageIdiomaDAOList =
                await _service.ConsultarTodos(codigo, descripcion ?? "", pageable);

            return Ok(ageIdiomaDAOList);
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(AgeIdiomasDAO), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarPorId(
            [FromRoute] int id)
        {
            AgeIdiomasDAO ageTiposIdentificacioneDAO = await _service.ConsultarPorId(id);
            return Ok(ageTiposIdentificacioneDAO);
        }

        [HttpGet("filtro")]
        [ProducesResponseType(typeof(Page<AgeIdiomasDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarListaFiltro(
            [FromQuery, Required] string filtro,
            [FromQuery] Pageable pageable)
        {
            Page<AgeIdiomasDAO> ageIdiomaDAOList =
                await _service.ConsultarListaFiltro(filtro, pageable);

            return Ok(ageIdiomaDAOList);
        }

        [HttpPost]
        [ProducesResponseType(typeof(Resource<AgeIdiomasDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> Insertar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeIdiomasSaveDAO ageIdiomaSaveDAO)
        {
            Resource<AgeIdiomasDAO> ageIdiomaDAO = await _service.Insertar(
                _httpContextAccessor, ageIdiomaSaveDAO);
            return Ok(ageIdiomaDAO);
        }


        [HttpPost("varios")]
        [ProducesResponseType(typeof(List<Resource<AgeIdiomasDAO>>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> InsertarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeIdiomasSaveDAO> ageIdiomaSaveDAOList)
        {
            List<Resource<AgeIdiomasDAO>> ageIdiomaDAOList =
                await _service.InsertarVarios(_httpContextAccessor, ageIdiomaSaveDAOList);

            return Ok(ageIdiomaDAOList);
        }


        [HttpPut]
        [ProducesResponseType(typeof(AgeIdiomasDAO), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> Actualizar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeIdiomasSaveDAO ageIdiomaSaveDAO)
        {
            AgeIdiomasDAO ageIdiomaDAO = await _service.Actualizar(_httpContextAccessor, ageIdiomaSaveDAO);
            return Ok(ageIdiomaDAO);
        }


        [HttpPut("varios")]
        [ProducesResponseType(typeof(List<AgeIdiomasDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ActualizarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeIdiomasSaveDAO> ageIdiomaSaveDAOList)
        {
            List<AgeIdiomasDAO> ageIdiomaDAOList =
                await _service.ActualizarVarios(_httpContextAccessor, ageIdiomaSaveDAOList);

            return Ok(ageIdiomaDAOList);
        }
    }

}

