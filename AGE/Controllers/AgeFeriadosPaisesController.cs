﻿using AGE.Entities.DAO.AgeFeriadosPaise;
using AGE.Services.AgeFeriadosPaiseService;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace AGE.Controllers
{

    [Route("[controller]")]
    [ApiController]
    public class AgeFeriadosPaisesController : ControllerBase
    {

        private readonly IAgeFeriadosPaisesService _service;

        public AgeFeriadosPaisesController(IAgeFeriadosPaisesService Service)
        {
            _service = Service;
        }

        [HttpGet]
        [ProducesResponseType(typeof(Page<AgeFeriadosPaisesDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarTodos(
            [FromQuery] Pageable pageable)
        {
            Page<AgeFeriadosPaisesDAO> ageFeriadosPaiseDAOList = await _service.ConsultarTodos(pageable);

            return Ok(ageFeriadosPaiseDAOList);
        }


        [HttpGet("{codigoPais}/{codigoDiaFeriado}")]
        [ProducesResponseType(typeof(AgeFeriadosPaisesDAO), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarPorId(
            [FromRoute] AgeFeriadosPaisesPKDAO ageFeriadosPaisePKDAO)
        {
            AgeFeriadosPaisesDAO ageFeriadosPaiseDAO =
                await _service.ConsultarPorId(ageFeriadosPaisePKDAO);

            return Ok(ageFeriadosPaiseDAO);
        }

    
        [HttpGet("filtro")]
        [ProducesResponseType(typeof(Page<AgeFeriadosPaisesDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarListaFiltro(
            [FromQuery, Required] string filtro,
            [FromQuery] Pageable pageable)
        {
            Page<AgeFeriadosPaisesDAO> ageFeriadosPaiseDAOList =
                await _service.ConsultarListaFiltro(filtro, pageable);

            return Ok(ageFeriadosPaiseDAOList);
        }


        [HttpPost]
        [ProducesResponseType(typeof(Resource<AgeFeriadosPaisesDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> Insertar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeFeriadosPaisesSaveDAO ageFeriadosPaiseSaveDAO)
        {
            Resource<AgeFeriadosPaisesDAO> ageFeriadosPaiseDAO =
                await _service.Insertar(_httpContextAccessor, ageFeriadosPaiseSaveDAO);

            return Ok(ageFeriadosPaiseDAO);
        }


        [HttpPost("varios")]
        [ProducesResponseType(typeof(List<Resource<AgeFeriadosPaisesDAO>>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> InsertarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeFeriadosPaisesSaveDAO> ageFeriadosPaiseSaveDAOList)
        {
            List<Resource<AgeFeriadosPaisesDAO>> ageFeriadosPaiseDAOList =
                await _service.InsertarVarios(_httpContextAccessor, ageFeriadosPaiseSaveDAOList);

            return Ok(ageFeriadosPaiseDAOList);
        }


        [HttpPut]
        [ProducesResponseType(typeof(AgeFeriadosPaisesDAO), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> Actualizar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeFeriadosPaisesSaveDAO ageFeriadosPaiseSaveDAO)
        {
            AgeFeriadosPaisesDAO ageFeriadosPaiseDAO =
                await _service.Actualizar(_httpContextAccessor, ageFeriadosPaiseSaveDAO);

            return Ok(ageFeriadosPaiseDAO);
        }


        [HttpPut("varios")]
        [ProducesResponseType(typeof(List<AgeFeriadosPaisesDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ActualizarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeFeriadosPaisesSaveDAO> ageFeriadosPaiseSaveDAOList)
        {
            List<AgeFeriadosPaisesDAO> ageFeriadosPaiseDAOList =
                await _service.ActualizarVarios(_httpContextAccessor, ageFeriadosPaiseSaveDAOList);

            return Ok(ageFeriadosPaiseDAOList);
        }

    }
}
