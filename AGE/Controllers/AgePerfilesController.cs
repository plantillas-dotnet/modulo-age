using AGE.Entities.DAO.AgePerfiles;
using AGE.Services.AgePerfilesService;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace AGE.Controllers
{

    [Route("[controller]")]
    [ApiController]
    //[Authorize]
    public class AgePerfilesController : ControllerBase
    {
        private readonly IAgePerfilesService _service;

        public AgePerfilesController(IAgePerfilesService Service)
        {
            _service = Service;
        }

        [HttpGet("{codigoLicenciatario}")]
        [ProducesResponseType(typeof(Page<AgePerfilesDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarTodos(
            [FromRoute] int codigoLicenciatario,
            [FromQuery] int? codigo,
            [FromQuery] string? descripcion,
            [FromQuery] Pageable pageable)
        {
            Page<AgePerfilesDAO> agePerfilesDAOList = 
                await _service.ConsultarTodos(codigoLicenciatario, codigo ?? 0, descripcion ?? "", pageable);

            return Ok(agePerfilesDAOList);
        }


        [HttpGet("{codigoLicenciatario}/{id}")]
        [ProducesResponseType(typeof(AgePerfilesDAO), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarPorId(
            [FromRoute] AgePerfilesPKDAO agePerfilesPKDAO)
        {
            var agePerfilesDAO = await _service.ConsultarPorId(agePerfilesPKDAO);
            return Ok(agePerfilesDAO);
        }



        [HttpGet("filtro/{codigoLicenciatario}")]
        [ProducesResponseType(typeof(Page<AgePerfilesDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarListaFiltro(
            [FromRoute] int codigoLicenciatario,
            [FromQuery, Required] string filtro,
            [FromQuery] Pageable pageable)
        {
            Page<AgePerfilesDAO> agePerfilesDAOList = 
                await _service.ConsultarListaFiltro(codigoLicenciatario, filtro, pageable);

            return Ok(agePerfilesDAOList);
        }


        [HttpPost]
        [ProducesResponseType(typeof(Resource<AgePerfilesDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> Insertar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgePerfilesSaveDAO agePerfilesSaveDAO)
        {
            Resource<AgePerfilesDAO> agePerfilesDAO = 
                await _service.Insertar(_httpContextAccessor, agePerfilesSaveDAO);

            return Ok(agePerfilesDAO);
        }


        [HttpPost("varios")]
        [ProducesResponseType(typeof(List<Resource<AgePerfilesDAO>>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> InsertarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgePerfilesSaveDAO> agePerfilesSaveDAOList)
        {
            List<Resource<AgePerfilesDAO>> agePerfilesDAOList = 
                await _service.InsertarVarios(_httpContextAccessor, agePerfilesSaveDAOList);

            return Ok(agePerfilesDAOList);
        }


        [HttpPut]
        [ProducesResponseType(typeof(AgePerfilesDAO), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> Actualizar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgePerfilesSaveDAO agePerfilesSaveDAO)
        {
            AgePerfilesDAO agePerfilesDAO = 
                await _service.Actualizar(_httpContextAccessor, agePerfilesSaveDAO);

            return Ok(agePerfilesDAO);
        }


        [HttpPut("varios")]
        [ProducesResponseType(typeof(List<AgePerfilesDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ActualizarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgePerfilesSaveDAO> agePerfilesSaveDAOList)
        {
            List<AgePerfilesDAO> agePerfilesDAOList = 
                await _service.ActualizarVarios(_httpContextAccessor, agePerfilesSaveDAOList);

            return Ok(agePerfilesDAOList);
        }
    }
}
