﻿using AGE.Entities.DAO.AgeMensajes;
using AGE.Services.AgeMensajesService;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace AGE.Controllers
{

    [Route("[controller]")]
    [ApiController]
    public class AgeMensajesController : ControllerBase
    {

        private readonly IAgeMensajeService _service;

        public AgeMensajesController(IAgeMensajeService Service)
        {
            _service = Service;
        }

        [HttpGet]
        [ProducesResponseType(typeof(Page<AgeMensajeDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarTodos(
            [FromQuery] long? codigo,
            [FromQuery] string? accionMsg,
            [FromQuery] Pageable pageable)
        {
            Page<AgeMensajeDAO> ageMensajeDAOList =
                await _service.ConsultarTodos(codigo ?? 0, accionMsg ?? "", pageable);

            return Ok(ageMensajeDAOList);
        }


        [HttpGet("filtro")]
        [ProducesResponseType(typeof(Page<AgeMensajeDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarListaFiltro(
            [FromQuery, Required] string filtro,
            [FromQuery] Pageable pageable)
        {
            Page<AgeMensajeDAO> ageMensajeDAOList =
                await _service.ConsultarListaFiltro(filtro, pageable);

            return Ok(ageMensajeDAOList);
        }


        [HttpGet("{id}")]
        [ProducesResponseType(typeof(AgeMensajeDAO), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarPorId(
            [FromRoute, Required] long id)
        {
            AgeMensajeDAO ageMensajeDAO = await _service.ConsultarPorId(id);
            return Ok(ageMensajeDAO);
        }



        [HttpPost]
        [ProducesResponseType(typeof(Resource<AgeMensajeDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> Insertar(
            [FromServices] IHttpContextAccessor httpContextAccessor,
            [FromBody] AgeMensajeSaveDAO ageMensajeSaveDAO)
        {
            Resource<AgeMensajeDAO> ageMensajeDAO = await _service.Insertar(httpContextAccessor, ageMensajeSaveDAO);
            return Ok(ageMensajeDAO);
        }


        [HttpPost("varios")]
        [ProducesResponseType(typeof(List<Resource<AgeMensajeDAO>>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> InsertarVarios(
            [FromServices] IHttpContextAccessor httpContextAccessor,
            [FromBody] List<AgeMensajeSaveDAO> ageMensajeSaveDAOList)
        {
            List<Resource<AgeMensajeDAO>> ageMensajeDAOList = await _service.InsertarVarios(httpContextAccessor, ageMensajeSaveDAOList);
            return Ok(ageMensajeDAOList);
        }


        [HttpPut]
        [ProducesResponseType(typeof(AgeMensajeDAO), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> Actualizar(
            [FromServices] IHttpContextAccessor httpContextAccessor,
            [FromBody] AgeMensajeSaveDAO ageMensajeSaveDAO)
        {
            AgeMensajeDAO ageMensajeDAO = await _service.Actualizar(httpContextAccessor, ageMensajeSaveDAO);
            return Ok(ageMensajeDAO);
        }


        [HttpPut("varios")]
        [ProducesResponseType(typeof(List<AgeMensajeDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ActualizarVarios(
            [FromServices] IHttpContextAccessor httpContextAccessor,
            [FromBody] List<AgeMensajeSaveDAO> ageMensajeSaveDAOList)
        {
            List<AgeMensajeDAO> ageMensajeDAOList = await _service.ActualizarVarios(httpContextAccessor, ageMensajeSaveDAOList);
            return Ok(ageMensajeDAOList);
        }
    }
}
