﻿using Microsoft.AspNetCore.Mvc;
using AGE.Entities.DAO.AgeLicenciatariosSecuencias;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using System.ComponentModel.DataAnnotations;
using AGE.Services.AgeLicenciatariosSecuenciaService;

namespace AGE.Controllers
{

    [Route("[controller]")]
    [ApiController]
    //[Authorize]
    public class AgeLicenciatariosSecuenciasController : ControllerBase
    {

        private readonly IAgeLicenciatariosSecuenciasService _service;

        public AgeLicenciatariosSecuenciasController(IAgeLicenciatariosSecuenciasService Service)
        {
            _service = Service;
        }

        [HttpGet("{codigoLicenciatario}")]
        [ProducesResponseType(typeof(Page<AgeLicenciatariosSecuenciasDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarTodos(
            [FromRoute] int codigoLicenciatario,
            [FromQuery] int? codigo,
            [FromQuery] string? ciclica,
            [FromQuery] string? descripcion,
            [FromQuery] Pageable pageable)
        {
            Page<AgeLicenciatariosSecuenciasDAO> ageLicenciatariosSecuenciasDAOList =
                await _service.ConsultarTodos(codigoLicenciatario, codigo ?? 0, ciclica ?? "", descripcion ?? "", pageable);

            return Ok(ageLicenciatariosSecuenciasDAOList);
        }


        [HttpGet("{codigoLicenciatario}/{id}")]
        [ProducesResponseType(typeof(AgeLicenciatariosSecuenciasDAO), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarPorId(
            [FromRoute] AgeLicenciatariosSecuenciasPKDAO ageLicenciatariosSecuenciasPKDAO)
        {
            AgeLicenciatariosSecuenciasDAO ageLicenciatariosSecuenciasDAO =
                await _service.ConsultarPorId(ageLicenciatariosSecuenciasPKDAO);

            return Ok(ageLicenciatariosSecuenciasDAO);
        }



        [HttpGet("filtro/{codigoLicenciatario}")]
        [ProducesResponseType(typeof(Page<AgeLicenciatariosSecuenciasDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarListaFiltro(
            [FromRoute] int codigoLicenciatario,
            [FromQuery, Required] string filtro,
            [FromQuery] Pageable pageable)
        {
            Page<AgeLicenciatariosSecuenciasDAO> ageLicenciatariosSecuenciasDAOList =
                await _service.ConsultarListaFiltro(codigoLicenciatario, filtro, pageable);

            return Ok(ageLicenciatariosSecuenciasDAOList);
        }


        [HttpPost]
        [ProducesResponseType(typeof(Resource<AgeLicenciatariosSecuenciasDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> Insertar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeLicenciatariosSecuenciasSaveDAO ageLicenciatariosSecuenciasSaveDAO)
        {
            Resource<AgeLicenciatariosSecuenciasDAO> ageLicenciatariosSecuenciasDAO =
                await _service.Insertar(_httpContextAccessor, ageLicenciatariosSecuenciasSaveDAO);

            return Ok(ageLicenciatariosSecuenciasDAO);
        }


        [HttpPost("varios")]
        [ProducesResponseType(typeof(List<Resource<AgeLicenciatariosSecuenciasDAO>>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> InsertarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeLicenciatariosSecuenciasSaveDAO> ageLicenciatariosSecuenciasSaveDAOList)
        {
            List<Resource<AgeLicenciatariosSecuenciasDAO>> ageLicenciatariosSecuenciasDAOList =
                await _service.InsertarVarios(_httpContextAccessor, ageLicenciatariosSecuenciasSaveDAOList);

            return Ok(ageLicenciatariosSecuenciasDAOList);
        }


        [HttpPut]
        [ProducesResponseType(typeof(AgeLicenciatariosSecuenciasDAO), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> Actualizar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeLicenciatariosSecuenciasSaveDAO ageLicenciatariosSecuenciasSaveDAO)
        {
            AgeLicenciatariosSecuenciasDAO ageLicenciatariosSecuenciasDAO =
                await _service.Actualizar(_httpContextAccessor, ageLicenciatariosSecuenciasSaveDAO);

            return Ok(ageLicenciatariosSecuenciasDAO);
        }


        [HttpPut("varios")]
        [ProducesResponseType(typeof(List<AgeLicenciatariosSecuenciasDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ActualizarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeLicenciatariosSecuenciasSaveDAO> ageLicenciatariosSecuenciasSaveDAOList)
        {
            List<AgeLicenciatariosSecuenciasDAO> ageLicenciatariosSecuenciasDAOList =
                await _service.ActualizarVarios(_httpContextAccessor, ageLicenciatariosSecuenciasSaveDAOList);

            return Ok(ageLicenciatariosSecuenciasDAOList);
        }
    }
}
