using AGE.Entities.DAO.AgePuntosEmision;
using AGE.Entities.DAO.AgeRutas;
using AGE.Entities.DAO.AgeTiposLocalidades;
using AGE.Services.AgePuntosEmisionService;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace AGE.Controllers
{
    [Route("[controller]")]
    [ApiController]
    //[Authorize]
    public class AgePuntosEmisionesController : ControllerBase
    {
        private readonly IAgePuntosEmisionesService _service;

        public AgePuntosEmisionesController(IAgePuntosEmisionesService service)
        {
            _service = service;
        }

        [HttpGet("{codigoLicenciatario}")]
        [ProducesResponseType(typeof(Page<AgePuntosEmisionesDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarTodos(
            [FromRoute] int codigoLicenciatario,
            [FromQuery] int? codigo,
            [FromQuery] int? ageSucursCodigo,
            [FromQuery] Pageable pageable)
        {
            Page<AgePuntosEmisionesDAO> agePuntosEmisionDAOList =
                await _service.ConsultarTodos(codigoLicenciatario, codigo ?? 0, ageSucursCodigo ?? 0, pageable);

            return Ok(agePuntosEmisionDAOList);
        }

        [HttpGet("{codigoLicenciatario}/{codigoSucursal}/{id}")]
        [ProducesResponseType(typeof(AgePuntosEmisionesDAO), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarPorId(
            [FromRoute] int codigoLicenciatario,
            [FromRoute] int codigoSucursal,
            [FromRoute] int id,
            [FromQuery] Pageable pageable)
        {
            AgePuntosEmisionesPKDAO agePuntosEmisionPKDAO = new AgePuntosEmisionesPKDAO
            {
                ageSucursAgeLicencCodigo = codigoLicenciatario,
                codigo = id,
                ageSucursCodigo = codigoSucursal
            };

            AgePuntosEmisionesDAO agePuntosEmisionDAO = await _service.ConsultarPorId(agePuntosEmisionPKDAO);
            return Ok(agePuntosEmisionDAO);
        }

        [HttpGet("filtro")]
        [ProducesResponseType(typeof(Page<AgePuntosEmisionesDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarListaFiltro(
            [FromQuery, Required] int codigoLicenciatario,
            [FromQuery, Required] string filtro,
            [FromQuery] Pageable pageable)
        {
            Page<AgePuntosEmisionesDAO> agePuntosEmisionDAOList =
                await _service.ConsultarListaFiltro(codigoLicenciatario, filtro, pageable);

            return Ok(agePuntosEmisionDAOList);
        }


        [HttpPost]
        [ProducesResponseType(typeof(Resource<AgePuntosEmisionesDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> Insertar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgePuntosEmisionesSaveDAO agePuntosEmisionSaveDAO)
        {
            Resource<AgePuntosEmisionesDAO> agePuntosEmisionDAO = await _service.Insertar(
                _httpContextAccessor, agePuntosEmisionSaveDAO);
            return Ok(agePuntosEmisionDAO);
        }


        [HttpPost("varios")]
        [ProducesResponseType(typeof(List<Resource<AgePuntosEmisionesDAO>>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> InsertarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgePuntosEmisionesSaveDAO> agePuntosEmisionSaveDAOList)
        {
            List<Resource<AgePuntosEmisionesDAO>> agePuntosEmisionDAOList =
                await _service.InsertarVarios(_httpContextAccessor, agePuntosEmisionSaveDAOList);

            return Ok(agePuntosEmisionDAOList);
        }


        [HttpPut]
        [ProducesResponseType(typeof(AgePuntosEmisionesDAO), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> Actualizar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgePuntosEmisionesSaveDAO agePuntosEmisionSaveDAO)
        {
            AgePuntosEmisionesDAO agePuntosEmisionDAO = await _service.Actualizar(
                _httpContextAccessor, agePuntosEmisionSaveDAO);
            return Ok(agePuntosEmisionDAO);
        }


        [HttpPut("varios")]
        [ProducesResponseType(typeof(List<AgePuntosEmisionesDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ActualizarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgePuntosEmisionesSaveDAO> agePuntosEmisionSaveDAOList)
        {
            List<AgePuntosEmisionesDAO> agePuntosEmisionDAOList =
                await _service.ActualizarVarios(_httpContextAccessor, agePuntosEmisionSaveDAOList);

            return Ok(agePuntosEmisionDAOList);
        }
    }
}
