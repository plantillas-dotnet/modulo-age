using AGE.Entities.DAO.AgeFirmasDigitales;
using AGE.Services.AgeFirmasDigitalesService;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace AGE.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class AgeFirmasDigitalesController : ControllerBase
    {
        private readonly IAgeFirmasDigitalesService _service;

        public AgeFirmasDigitalesController(IAgeFirmasDigitalesService Service)
        {
            _service = Service;
        }

        [HttpGet("{codigoLicenciatario}")]
        [ProducesResponseType(typeof(Page<AgeFirmasDigitalesDAO>), 200)]
        public async Task<IActionResult> ConsultarTodos([FromRoute] int codigoLicenciatario,
                                                        [FromQuery] string nombres,
                                                        [FromQuery] string apellidos,
                                                        [FromQuery] string tipoFirmaDigital,
                                                        [FromQuery] Pageable pageable)
        {
            Page<AgeFirmasDigitalesDAO> ageFirmasDigitalesDAOList =
                await _service.ConsultarTodos(codigoLicenciatario, nombres, apellidos, tipoFirmaDigital, pageable);

            return Ok(ageFirmasDigitalesDAOList);
        }

        [HttpGet("{codigoLicenciatario}/{codigoSucursal}/{id}")]
        [ProducesResponseType(typeof(AgeFirmasDigitalesDAO), 200)]
        public async Task<IActionResult> ConsultarPorId(
            [FromRoute] AgeFirmasDigitalesPKDAO ageFirmasDigitalesPKDAO)
        {
            var ageFirmasDigitalesDAO = await _service.ConsultarPorId(ageFirmasDigitalesPKDAO);
            return Ok(ageFirmasDigitalesDAO);
        }

        [HttpGet("filtro/{codigoLicenciatario}")]
        [ProducesResponseType(typeof(Page<AgeFirmasDigitalesDAO>), 200)]
        public async Task<IActionResult> ConsultarListaFiltro(
           [FromRoute, Required] int codigoLicenciatario,
           [FromQuery, Required] string filtro,
           [FromQuery] Pageable pageable)
        {
            Page<AgeFirmasDigitalesDAO> ageFirmasDigitalesDAO =
                await _service.ConsultarListaFiltro(codigoLicenciatario, filtro, pageable);

            return Ok(ageFirmasDigitalesDAO);
        }

        [HttpPost]
        [ProducesResponseType(typeof(Resource<AgeFirmasDigitalesDAO>), 200)]
        public async Task<IActionResult> Insertar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeFirmasDigitalesSaveDAO ageFirmasDigitalesSaveDAO)
        {
            Resource<AgeFirmasDigitalesDAO> ageFirmasDigitalesDAO =
                await _service.Insertar(_httpContextAccessor, ageFirmasDigitalesSaveDAO);

            return Ok(ageFirmasDigitalesDAO);
        }

        [HttpPost("varios")]
        [ProducesResponseType(typeof(List<Resource<AgeFirmasDigitalesDAO>>), 200)]
        public async Task<IActionResult> InsertarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeFirmasDigitalesSaveDAO> ageFirmasDigitalesSaveDAOList)
        {
            List<Resource<AgeFirmasDigitalesDAO>> ageFirmasDigitalesDAOList =
                await _service.InsertarVarios(_httpContextAccessor, ageFirmasDigitalesSaveDAOList);

            return Ok(ageFirmasDigitalesDAOList);
        }

        [HttpPut]
        [ProducesResponseType(typeof(AgeFirmasDigitalesDAO), 200)]
        public async Task<IActionResult> Actualizar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeFirmasDigitalesSaveDAO ageFirmasDigitalesSaveDAO)
        {
            AgeFirmasDigitalesDAO ageFirmasDigitalesDAO = await _service.Actualizar(_httpContextAccessor, ageFirmasDigitalesSaveDAO);
            return Ok(ageFirmasDigitalesDAO);
        }

        [HttpPut("varios")]
        [ProducesResponseType(typeof(List<AgeFirmasDigitalesDAO>), 200)]
        public async Task<IActionResult> ActualizarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeFirmasDigitalesSaveDAO> ageFirmasDigitalesSaveDAOList)
        {
            List<AgeFirmasDigitalesDAO> ageFirmasDigitalesDAOList =
                await _service.ActualizarVarios(_httpContextAccessor, ageFirmasDigitalesSaveDAOList);

            return Ok(ageFirmasDigitalesDAOList);
        }
    }
}
