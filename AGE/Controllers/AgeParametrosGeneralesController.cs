using AGE.Entities.DAO.AgeParametrosGenerales;
using AGE.Services.AgeParametrosGeneralesService;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace AGE.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class AgeParametroGeneralesController : Controller
    {
        private readonly IAgeParametrosGeneralesService _service;

        public AgeParametroGeneralesController(IAgeParametrosGeneralesService Service)
        {
            _service = Service;
        }

        [HttpGet]
        [ProducesResponseType(typeof(Page<AgeParametrosGeneralesDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarTodos(
            [FromQuery] int? codigo,
            [FromQuery] string? descripcion,
            [FromQuery] Pageable pageable)
        {
            Page<AgeParametrosGeneralesDAO> ageParametrosGeneralesDAOList =
                await _service.ConsultarTodos(codigo ?? 0, descripcion ?? "", pageable);

            return Ok(ageParametrosGeneralesDAOList);
        }

        [HttpPut]
        [ProducesResponseType(typeof(AgeParametrosGeneralesDAO), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> Actualizar(
            [FromServices] IHttpContextAccessor httpContextAccessor,
            [FromBody] AgeParametrosGeneralesSaveDAO ageParametrosGeneralesSaveDAO)
        {
            AgeParametrosGeneralesDAO ageParametrosGeneralesDAO = await _service.Actualizar(httpContextAccessor, ageParametrosGeneralesSaveDAO);
            return Ok(ageParametrosGeneralesDAO);
        }

        [HttpPost]
        [ProducesResponseType(typeof(Resource<AgeParametrosGeneralesDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> Insertar(
            [FromServices] IHttpContextAccessor httpContextAccessor,
            [FromBody] AgeParametrosGeneralesSaveDAO ageParametrosGeneralesSaveDAO)
        {
            Resource<AgeParametrosGeneralesDAO> ageParametrosGeneralesDAO = await _service.Insertar(httpContextAccessor, ageParametrosGeneralesSaveDAO);
            return Ok(ageParametrosGeneralesDAO);
        }


        [HttpPut("varios")]
        [ProducesResponseType(typeof(List<AgeParametrosGeneralesDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ActualizarVarios(
            [FromServices] IHttpContextAccessor httpContextAccessor,
            [FromBody] List<AgeParametrosGeneralesSaveDAO> ageParametrosGeneralesSaveDAOList)
        {
            List<AgeParametrosGeneralesDAO> ageParametrosGeneralesDAOList = await _service.ActualizarVarios(httpContextAccessor, ageParametrosGeneralesSaveDAOList);
            return Ok(ageParametrosGeneralesDAOList);
        }

        [HttpPost("varios")]
        [ProducesResponseType(typeof(List<Resource<AgeParametrosGeneralesDAO>>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> InsertarVarios(
            [FromServices] IHttpContextAccessor httpContextAccessor,
            [FromBody] List<AgeParametrosGeneralesSaveDAO> ageParametrosGeneralesSaveDAOList)
        {
            List<Resource<AgeParametrosGeneralesDAO>> ageParametrosGeneralesDAOList = await _service.InsertarVarios(httpContextAccessor, ageParametrosGeneralesSaveDAOList);
            return Ok(ageParametrosGeneralesDAOList);
        }


        [HttpGet("{id}")]
        [ProducesResponseType(typeof(AgeParametrosGeneralesDAO), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarPorId(
            [FromRoute, Required] int id)
        {
            AgeParametrosGeneralesDAO ageParametrosGeneralesDAO = await _service.ConsultarPorId(id);
            return Ok(ageParametrosGeneralesDAO);
        }

        [HttpGet("filtro")]
        [ProducesResponseType(typeof(Page<AgeParametrosGeneralesDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarListaFiltro(
            [FromQuery, Required] string filtro,
            [FromQuery] Pageable pageable)
        {
            Page<AgeParametrosGeneralesDAO> ageParametrosGeneralesDAOList =
                await _service.ConsultarListaFiltro(filtro, pageable);

            return Ok(ageParametrosGeneralesDAOList);
        }

    }
}
