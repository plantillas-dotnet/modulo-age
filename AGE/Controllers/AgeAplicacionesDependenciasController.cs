﻿using AGE.Entities.DAO.AgeAplicacionesDependencias;
using AGE.Services.AgeAplicacionesDependenciasService;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace AGE.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class AgeAplicacionesDependenciasController : ControllerBase
    {
        private readonly IAgeAplicacionesDependenciasService _service;

        public AgeAplicacionesDependenciasController(IAgeAplicacionesDependenciasService Service)
        {
            _service = Service;
        }

        [HttpGet]
        [ProducesResponseType(typeof(Page<AgeAplicacionesDependenciasDAO>), 200)]
        public async Task<IActionResult> ConsultarTodos(
            [FromQuery] Pageable pageable)
        {
            Page<AgeAplicacionesDependenciasDAO> ageAplicacionesDependenciasDAOList =
                await _service.ConsultarTodos(pageable);

            return Ok(ageAplicacionesDependenciasDAOList);
        }

        [HttpGet("{codigoAplicacionDe}/{codigoAplicacionSer}/{orden}")]
        [ProducesResponseType(typeof(AgeAplicacionesDependenciasDAO), 200)]
        public async Task<IActionResult> ConsultarPorId(
            [FromRoute] AgeAplicacionesDependenciasPKDAO ageAplicacionesDependenciasPKDAO)
        {
            var ageAplicacionesDependenciasDAO = await _service.ConsultarPorId(ageAplicacionesDependenciasPKDAO);
            return Ok(ageAplicacionesDependenciasDAO);
        }

        [HttpGet("filtro")]
        [ProducesResponseType(typeof(Page<AgeAplicacionesDependenciasDAO>), 200)]
        public async Task<IActionResult> ConsultarListaFiltro(
            [FromQuery, Required] string filtro,
            [FromQuery] Pageable pageable)
        {
            Page<AgeAplicacionesDependenciasDAO> ageAplicacionesDependenciasDAOList =
                await _service.ConsultarListaFiltro(filtro, pageable);

            return Ok(ageAplicacionesDependenciasDAOList);
        }

        [HttpPost]
        [ProducesResponseType(typeof(Resource<AgeAplicacionesDependenciasDAO>), 200)]
        public async Task<IActionResult> Insertar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeAplicacionesDependenciasSaveDAO ageAplicacionesDependenciasSaveDAO)
        {
            Resource<AgeAplicacionesDependenciasDAO> ageAplicacionesDependenciasDAO =
                await _service.Insertar(_httpContextAccessor, ageAplicacionesDependenciasSaveDAO);

            return Ok(ageAplicacionesDependenciasDAO);
        }

        [HttpPost("varios")]
        [ProducesResponseType(typeof(List<Resource<AgeAplicacionesDependenciasDAO>>), 200)]
        public async Task<IActionResult> InsertarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeAplicacionesDependenciasSaveDAO> ageAplicacionesDependenciasSaveDAOList)
        {
            List<Resource<AgeAplicacionesDependenciasDAO>> ageAplicacionesDependenciasDAOList =
                await _service.InsertarVarios(_httpContextAccessor, ageAplicacionesDependenciasSaveDAOList);

            return Ok(ageAplicacionesDependenciasDAOList);
        }

        [HttpPut]
        [ProducesResponseType(typeof(AgeAplicacionesDependenciasDAO), 200)]
        public async Task<IActionResult> Actualizar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeAplicacionesDependenciasSaveDAO ageAplicacionesDependenciasSaveDAO)
        {
            AgeAplicacionesDependenciasDAO ageAplicacionesDependenciasDAO =
                await _service.Actualizar(_httpContextAccessor, ageAplicacionesDependenciasSaveDAO);

            return Ok(ageAplicacionesDependenciasDAO);
        }

        [HttpPut("varios")]
        [ProducesResponseType(typeof(List<AgeAplicacionesDependenciasDAO>), 200)]
        public async Task<IActionResult> ActualizarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeAplicacionesDependenciasSaveDAO> ageAplicacionesDependenciasSaveDAOList)
        {
            List<AgeAplicacionesDependenciasDAO> ageAplicacionesDependenciasDAOList =
                await _service.ActualizarVarios(_httpContextAccessor, ageAplicacionesDependenciasSaveDAOList);

            return Ok(ageAplicacionesDependenciasDAOList);
        }

    }
}
