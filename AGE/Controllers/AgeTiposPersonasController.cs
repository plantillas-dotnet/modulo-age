using AGE.Entities.DAO.AgeTiposPersonas;
using AGE.Services.AgeTiposPersonaService;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace AGE.Controllers
{
    [Route("[controller]")]
    [ApiController]
    //[Authorize]
    public class AgeTiposPersonasController : ControllerBase
    {
        private readonly IAgeTiposPersonasService _service;

        public AgeTiposPersonasController(IAgeTiposPersonasService Service)
        {
            _service = Service;
        }

        [HttpGet("{codigoLicenciatario}")]
        [ProducesResponseType(typeof(Page<AgeTiposPersonaDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarTodos(
            [FromRoute] int codigoLicenciatario,
            [FromQuery] int? codigo,
            [FromQuery] string? codigoExterno,
            [FromQuery] string? descripcion,
            [FromQuery] Pageable pageable)
        {
            Page<AgeTiposPersonaDAO> ageTiposPersonaDAOList =
                await _service.ConsultarTodos(codigoLicenciatario, codigo ?? 0, codigoExterno ?? "",
                                              descripcion ?? "", pageable);

            return Ok(ageTiposPersonaDAOList);
        }

        [HttpGet("{codigoLicenciatario}/{id}")]
        [ProducesResponseType(typeof(AgeTiposPersonaDAO), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarPorId(
        [FromRoute] AgeTiposPersonaPKDAO ageTiposPersonaPKDAO)
        {
            AgeTiposPersonaDAO ageTiposPersonaDAO = await _service.ConsultarPorId(ageTiposPersonaPKDAO);
            return Ok(ageTiposPersonaDAO);
        }

        [HttpGet("filtro/{codigoLicenciatario}")]
        [ProducesResponseType(typeof(Page<AgeTiposPersonaDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarListaFiltro(
            [FromRoute] int codigoLicenciatario,
            [FromQuery, Required] string filtro,
            [FromQuery] Pageable pageable)
        {
            Page<AgeTiposPersonaDAO> ageTiposPersonaDAOList =
                await _service.ConsultarListaFiltro(codigoLicenciatario, filtro, pageable);

            return Ok(ageTiposPersonaDAOList);
        }


        [HttpPost]
        [ProducesResponseType(typeof(Resource<AgeTiposPersonaDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> Insertar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeTiposPersonaSaveDAO ageTiposPersonaSaveDAO)
        {
            Resource<AgeTiposPersonaDAO> ageTiposPersonaDAO = await _service.Insertar(_httpContextAccessor, ageTiposPersonaSaveDAO);
            return Ok(ageTiposPersonaDAO);
        }


        [HttpPost("varios")]
        [ProducesResponseType(typeof(List<Resource<AgeTiposPersonaDAO>>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> InsertarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeTiposPersonaSaveDAO> ageTiposPersonaSaveDAOList)
        {
            List<Resource<AgeTiposPersonaDAO>> ageTiposPersonaDAOList =
                await _service.InsertarVarios(_httpContextAccessor, ageTiposPersonaSaveDAOList);

            return Ok(ageTiposPersonaDAOList);
        }


        [HttpPut]
        [ProducesResponseType(typeof(AgeTiposPersonaDAO), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> Actualizar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeTiposPersonaSaveDAO ageTiposPersonaSaveDAO)
        {
            AgeTiposPersonaDAO ageTiposPersonaDAO = await _service.Actualizar(_httpContextAccessor, ageTiposPersonaSaveDAO);
            return Ok(ageTiposPersonaDAO);
        }


        [HttpPut("varios")]
        [ProducesResponseType(typeof(List<AgeTiposPersonaDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ActualizarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeTiposPersonaSaveDAO> ageTiposPersonaSaveDAOList)
        {
            List<AgeTiposPersonaDAO> ageTiposPersonaDAOList =
                await _service.ActualizarVarios(_httpContextAccessor, ageTiposPersonaSaveDAOList);

            return Ok(ageTiposPersonaDAOList);
        }

    }

}
