﻿using AGE.Entities.DAO.AgeFranquicias;
using AGE.Services.AgeFranquiciasService;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace AGE.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class AgeFranquiciasController : ControllerBase
    {
        private readonly IAgeFranquiciasService _service;

        public AgeFranquiciasController(IAgeFranquiciasService Service)
        {
            _service = Service;
        }

        [HttpGet]
        [ProducesResponseType(typeof(Page<AgeFranquiciasDAO>), 200)]
        public async Task<IActionResult> ConsultarTodos(
            [FromQuery] int? codigo,
            [FromQuery] string? descripcion,
            [FromQuery] Pageable pageable)
        {
            Page<AgeFranquiciasDAO> ageFranquiciasDAOList =
                await _service.ConsultarTodos(codigo ?? 0, descripcion ?? "", pageable);

            return Ok(ageFranquiciasDAOList);
        }


        [HttpGet("filtro")]
        [ProducesResponseType(typeof(Page<AgeFranquiciasDAO>), 200)]
        public async Task<IActionResult> ConsultarListaFiltro(
            [FromQuery, Required] string filtro,
            [FromQuery] Pageable pageable)
        {
            Page<AgeFranquiciasDAO> ageFranquiciasDAOList =
                await _service.ConsultarListaFiltro(filtro, pageable);

            return Ok(ageFranquiciasDAOList);
        }


        [HttpGet("{id}")]
        [ProducesResponseType(typeof(AgeFranquiciasDAO), 200)]
        public async Task<IActionResult> ConsultarPorId(
            [FromRoute, Required] int id)
        {
            AgeFranquiciasDAO ageFranquiciasDAO = await _service.ConsultarPorId(id);
            return Ok(ageFranquiciasDAO);
        }

        [HttpPost]
        [ProducesResponseType(typeof(Resource<AgeFranquiciasDAO>), 200)]
        public async Task<IActionResult> Insertar(
            [FromServices] IHttpContextAccessor httpContextAccessor,
            [FromBody] AgeFranquiciasSaveDAO ageFranquiciasSaveDAO)
        {
            Resource<AgeFranquiciasDAO> ageFranquiciasDAO = await _service.Insertar(httpContextAccessor, ageFranquiciasSaveDAO);
            return Ok(ageFranquiciasDAO);
        }

        [HttpPost("varios")]
        [ProducesResponseType(typeof(List<Resource<AgeFranquiciasDAO>>), 200)]
        public async Task<IActionResult> InsertarVarios(
            [FromServices] IHttpContextAccessor httpContextAccessor,
            [FromBody] List<AgeFranquiciasSaveDAO> ageFranquiciasSaveDAOList)
        {
            List<Resource<AgeFranquiciasDAO>> ageFranquiciasDAOList = await _service.InsertarVarios(httpContextAccessor, ageFranquiciasSaveDAOList);
            return Ok(ageFranquiciasDAOList);
        }

        [HttpPut]
        [ProducesResponseType(typeof(AgeFranquiciasDAO), 200)]
        public async Task<IActionResult> Actualizar(
            [FromServices] IHttpContextAccessor httpContextAccessor,
            [FromBody] AgeFranquiciasSaveDAO ageFranquiciasSaveDAO)
        {
            AgeFranquiciasDAO ageFranquiciasDAO = await _service.Actualizar(httpContextAccessor, ageFranquiciasSaveDAO);
            return Ok(ageFranquiciasDAO);
        }


        [HttpPut("varios")]
        [ProducesResponseType(typeof(List<AgeFranquiciasDAO>), 200)]
        public async Task<IActionResult> ActualizarVarios(
            [FromServices] IHttpContextAccessor httpContextAccessor,
            [FromBody] List<AgeFranquiciasSaveDAO> ageFranquiciasSaveDAOList)
        {
            List<AgeFranquiciasDAO> ageFranquiciasDAOList = await _service.ActualizarVarios(httpContextAccessor, ageFranquiciasSaveDAOList);
            return Ok(ageFranquiciasDAOList);
        }
    }
}
