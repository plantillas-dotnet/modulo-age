﻿using AGE.Entities.DAO.AgeMonedasCotizaciones;
using AGE.Services.AgeMonedasCotizacionesService;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace AGE.Controllers
{

    [Route("[controller]")]
    [ApiController]
    //[Authorize]
    public class AgeMonedasCotizacionesController : ControllerBase
    {
        private readonly IAgeMonedasCotizacionesService _service;

        public AgeMonedasCotizacionesController(IAgeMonedasCotizacionesService Service)
        {
            _service = Service;
        }

        [HttpPut]
        [ProducesResponseType(typeof(AgeMonedasCotizacionesDAO), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> Actualizar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeMonedasCotizacionesSaveDAO ageMonedasCotizacionesSaveDAO)
        {
            AgeMonedasCotizacionesDAO ageMonedasCotizacionesDAO =
                await _service.Actualizar(_httpContextAccessor, ageMonedasCotizacionesSaveDAO);

            return Ok(ageMonedasCotizacionesDAO);
        }

        [HttpPost]
        [ProducesResponseType(typeof(Resource<AgeMonedasCotizacionesDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> Insertar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeMonedasCotizacionesSaveDAO ageMonedasCotizacionesSaveDAO)
        {
            Resource<AgeMonedasCotizacionesDAO> ageMonedasCotizacionesDAO =
                await _service.Insertar(_httpContextAccessor, ageMonedasCotizacionesSaveDAO);

            return Ok(ageMonedasCotizacionesDAO);
        }

        [HttpPut("varios")]
        [ProducesResponseType(typeof(List<AgeMonedasCotizacionesDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ActualizarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeMonedasCotizacionesSaveDAO> ageMonedasCotizacionesSaveDAOList)
        {
            List<AgeMonedasCotizacionesDAO> ageMonedasCotizacionesDAOList =
                await _service.ActualizarVarios(_httpContextAccessor, ageMonedasCotizacionesSaveDAOList);

            return Ok(ageMonedasCotizacionesDAOList);
        }

        [HttpPost("varios")]
        [ProducesResponseType(typeof(List<Resource<AgeMonedasCotizacionesDAO>>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> InsertarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeMonedasCotizacionesSaveDAO> ageMonedasCotizacionesSaveDAOList)
        {
            List<Resource<AgeMonedasCotizacionesDAO>> ageMonedasCotizacionesDAOList =
                await _service.InsertarVarios(_httpContextAccessor, ageMonedasCotizacionesSaveDAOList);

            return Ok(ageMonedasCotizacionesDAOList);
        }

        [HttpGet("{codigoLicenciatario}")]
        [ProducesResponseType(typeof(Page<AgeMonedasCotizacionesDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarTodos(
            [FromRoute] int codigoLicenciatario,
            [FromQuery] int? codigo,
            [FromQuery] int? ageMonedaCodigo,
            [FromQuery] int? ageMonedaCodigoSerA,
            [FromQuery] Pageable pageable)
        {
            Page<AgeMonedasCotizacionesDAO> ageMonedasCotizacionesDAOList =
                await _service.ConsultarTodos(codigoLicenciatario, codigo ?? 0, ageMonedaCodigo ?? 0, 
                ageMonedaCodigoSerA ?? 0, pageable);

            return Ok(ageMonedasCotizacionesDAOList);
        }

        [HttpGet("{codigoLicenciatario}/{id}")]
        [ProducesResponseType(typeof(AgeMonedasCotizacionesDAO), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarPorId(
            [FromRoute] AgeMonedasCotizacionesPKDAO ageMonedasCotizacionesPKDAO)
        {
            var ageMonedasCotizacionesDAO = await _service.ConsultarPorId(ageMonedasCotizacionesPKDAO);
            return Ok(ageMonedasCotizacionesDAO);
        }

        [HttpGet("filtro/{codigoLicenciatario}")]
        [ProducesResponseType(typeof(Page<AgeMonedasCotizacionesDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarListaFiltro(
            [FromRoute] int codigoLicenciatario,
            [FromQuery, Required] string filtro,
            [FromQuery] Pageable pageable)
        {
            Page<AgeMonedasCotizacionesDAO> ageMonedasCotizacionesDAOList =
                await _service.ConsultarListaFiltro(codigoLicenciatario, filtro, pageable);

            return Ok(ageMonedasCotizacionesDAOList);
        }
    }
}
