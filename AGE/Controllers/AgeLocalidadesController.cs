﻿using AGE.Entities.DAO.AgeLocalidades;
using AGE.Services.AgeLocalidadesService;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace AGE.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class AgeLocalidadesController : ControllerBase
    {
        private readonly IAgeLocalidadesService _service;

        public AgeLocalidadesController(IAgeLocalidadesService Service)
        {
            _service = Service;
        }

        [HttpGet]
        [ProducesResponseType(typeof(Page<AgeLocalidadesDAO>), 200)]
        public async Task<IActionResult> ConsultarTodos([FromQuery] string descripcion,
                                                        [FromQuery] int ageTipLoCodigo,
                                                        [FromQuery] int ageTipLoAgePaisCodigo,
                                                        [FromQuery] int ageLocaliCodigo,
                                                        [FromQuery] int ageAgeTipLoAgePaisCodigo,
                                                        [FromQuery] int ageLocaliAgeTipLoCodigo,
                                                        [FromQuery] Pageable pageable)
        {
            Page<AgeLocalidadesDAO> ageLocalidadesDAOList =
                await _service.ConsultarTodos(descripcion, ageTipLoCodigo, ageTipLoAgePaisCodigo,ageLocaliCodigo,ageAgeTipLoAgePaisCodigo,ageLocaliAgeTipLoCodigo, pageable);

            return Ok(ageLocalidadesDAOList);
        }

        [HttpGet("{codigoPais}/{codigoTipoLocalidad}/{id}")]
        [ProducesResponseType(typeof(AgeLocalidadesDAO), 200)]
        public async Task<IActionResult> ConsultarPorId(
            [FromRoute] AgeLocalidadesPKDAO ageLocalidadesPKDAO)
        {
            var ageLocalidadesDAO = await _service.ConsultarPorId(ageLocalidadesPKDAO);
            return Ok(ageLocalidadesDAO);
        }

        [HttpGet("filtro")]
        [ProducesResponseType(typeof(Page<AgeLocalidadesDAO>), 200)]
        public async Task<IActionResult> ConsultarListaFiltro(
           [FromQuery, Required] string filtro,
           [FromQuery] Pageable pageable)
        {
            Page<AgeLocalidadesDAO> ageLocalidadesDAO =
                await _service.ConsultarListaFiltro(filtro, pageable);

            return Ok(ageLocalidadesDAO);
        }

        [HttpPost]
        [ProducesResponseType(typeof(Resource<AgeLocalidadesDAO>), 200)]
        public async Task<IActionResult> Insertar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeLocalidadesSaveDAO ageLocalidadesSaveDAO)
        {
            Resource<AgeLocalidadesDAO> ageLocalidadesDAO =
                await _service.Insertar(_httpContextAccessor, ageLocalidadesSaveDAO);

            return Ok(ageLocalidadesDAO);
        }

        [HttpPost("varios")]
        [ProducesResponseType(typeof(List<Resource<AgeLocalidadesDAO>>), 200)]
        public async Task<IActionResult> InsertarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeLocalidadesSaveDAO> ageLocalidadesSaveDAOList)
        {
            List<Resource<AgeLocalidadesDAO>> ageLocalidadesDAOList =
                await _service.InsertarVarios(_httpContextAccessor, ageLocalidadesSaveDAOList);

            return Ok(ageLocalidadesDAOList);
        }

        [HttpPut]
        [ProducesResponseType(typeof(AgeLocalidadesDAO), 200)]
        public async Task<IActionResult> Actualizar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeLocalidadesSaveDAO ageLocalidadesSaveDAO)
        {
            AgeLocalidadesDAO ageLocalidadesDAO = await _service.Actualizar(_httpContextAccessor, ageLocalidadesSaveDAO);
            return Ok(ageLocalidadesDAO);
        }

        [HttpPut("varios")]
        [ProducesResponseType(typeof(List<AgeLocalidadesDAO>), 200)]
        public async Task<IActionResult> ActualizarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeLocalidadesSaveDAO> ageLocalidadesSaveDAOList)
        {
            List<AgeLocalidadesDAO> ageLocalidadesDAOList =
                await _service.ActualizarVarios(_httpContextAccessor, ageLocalidadesSaveDAOList);

            return Ok(ageLocalidadesDAOList);
        }
    }
}
