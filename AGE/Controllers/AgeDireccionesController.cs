using AGE.Entities.DAO.AgeDirecciones;
using AGE.Services.AgeDireccionesService;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace AGE.Controllers
{
    [Route("[controller]")]
    [ApiController]
    //[Authorize]
    public class AgeDireccionesController : ControllerBase
    {
        private readonly IAgeDireccionesService _service;

        public AgeDireccionesController(IAgeDireccionesService Service)
        {
            _service = Service;
        }


        [HttpPut]
        [ProducesResponseType(typeof(AgeDireccionesDAO), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> Actualizar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeDireccionesSaveDAO ageDireccionesSaveDAO)
        {
            AgeDireccionesDAO ageDireccionesDAO = await _service.Actualizar(_httpContextAccessor, ageDireccionesSaveDAO);
            return Ok(ageDireccionesDAO);
        }

        [HttpPost]
        [ProducesResponseType(typeof(Resource<AgeDireccionesDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> Insertar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeDireccionesSaveDAO ageDireccionesSaveDAO)
        {
            Resource<AgeDireccionesDAO> ageDireccionesDAO = await _service.Insertar(_httpContextAccessor, ageDireccionesSaveDAO);
            return Ok(ageDireccionesDAO);
        }

        [HttpPut("varios")]
        [ProducesResponseType(typeof(List<AgeDireccionesDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ActualizarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeDireccionesSaveDAO> ageDireccionesSaveDAOList)
        {
            List<AgeDireccionesDAO> ageDireccionesDAOList =
                await _service.ActualizarVarios(_httpContextAccessor, ageDireccionesSaveDAOList);

            return Ok(ageDireccionesDAOList);
        }

        [HttpPost("varios")]
        [ProducesResponseType(typeof(List<Resource<AgeDireccionesDAO>>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> InsertarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeDireccionesSaveDAO> ageDireccionesSaveDAOList)
        {
            List<Resource<AgeDireccionesDAO>> ageDireccionesDAOList =
                await _service.InsertarVarios(_httpContextAccessor, ageDireccionesSaveDAOList);

            return Ok(ageDireccionesDAOList);
        }

        [HttpGet("{codigoLicenciatario}/{codigoPersona}/{id}")]
        [ProducesResponseType(typeof(AgeDireccionesDAO), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarPorId(
            [FromRoute] AgeDireccionesPKDAO ageDireccionesPKDAO)
        {
            AgeDireccionesDAO ageDireccionesDAO = await _service.ConsultarPorId(ageDireccionesPKDAO);
            return Ok(ageDireccionesDAO);
        }

        [HttpGet("{codigoLicenciatario}")]
        [ProducesResponseType(typeof(Page<AgeDireccionesDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarTodos(
            [FromRoute] int codigoLicenciatario,
            [FromQuery] int? codigo,
            [FromQuery] int? codigoPersona,
            [FromQuery] int? ageTipDiCodigo,
            [FromQuery] string? descripcion,
            [FromQuery] Pageable pageable)
        {

            Page<AgeDireccionesDAO> ageDireccionesDAOList =
                await _service.ConsultarTodos(codigoLicenciatario, codigoPersona ?? 0,
                codigo ?? 0, ageTipDiCodigo ?? 0, descripcion ?? "", pageable);

            return Ok(ageDireccionesDAOList);
        }

        [HttpGet("filtro/{codigoLicenciatario}")]
        [ProducesResponseType(typeof(Page<AgeDireccionesDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarListaFiltro(
            [FromRoute] int codigoLicenciatario,
            [FromQuery, Required] string filtro,
            [FromQuery] Pageable pageable)
        {
            Page<AgeDireccionesDAO> ageDireccionesDAOList =
                await _service.ConsultarListaFiltro(codigoLicenciatario, filtro, pageable);

            return Ok(ageDireccionesDAOList);
        }

    }
}
