﻿using Microsoft.AspNetCore.Mvc;
using AGE.Entities.DAO.AgeLicenciatariosAplicaSecu;
using AGE.Services.AgeLicenciatariosAplicaSecuService;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using System.ComponentModel.DataAnnotations;

namespace AGE.Controllers
{

    [Route("[controller]")]
    [ApiController]
    //[Authorize]
    public class AgeLicenciatarioAplicaSecuController : ControllerBase
    {

        private readonly IAgeLicenciatariosAplicaSecuService _service;

        public AgeLicenciatarioAplicaSecuController(IAgeLicenciatariosAplicaSecuService Service)
        {
            _service = Service;
        }
        
        [HttpGet("{codigoLicenciatario}")]
        [ProducesResponseType(typeof(Page<AgeLicenciatariosAplicaSecuDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarTodos(
            [FromRoute] int codigoLicenciatario,
            [FromQuery] int? codigo,
            [FromQuery] int? ageLicApAgeAplicaCodigo,
            [FromQuery] string? ciclica,
            [FromQuery] string? descripcion,
            [FromQuery] Pageable pageable)
        {
            Page<AgeLicenciatariosAplicaSecuDAO> ageLicenciatariosAplicaSecuDAOList = 
                await _service.ConsultarTodos(codigoLicenciatario, codigo??0, ageLicApAgeAplicaCodigo??0, ciclica??"", descripcion??"", pageable);
            
            return Ok(ageLicenciatariosAplicaSecuDAOList);
        }


        [HttpGet("{codigoLicenciatario}/{codigoAplicacion}/{id}")]
        [ProducesResponseType(typeof(AgeLicenciatariosAplicaSecuDAO), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarPorId(
            [FromRoute] AgeLicenciatariosAplicaSecuPKDAO ageLicenciatariosAplicaSecuPKDAO)
        {
            AgeLicenciatariosAplicaSecuDAO ageLicenciatariosAplicaSecuDAO = 
                await _service.ConsultarPorId(ageLicenciatariosAplicaSecuPKDAO);

            return Ok(ageLicenciatariosAplicaSecuDAO);
        }
        


        [HttpGet("filtro/{codigoLicenciatario}")]
        [ProducesResponseType(typeof(Page<AgeLicenciatariosAplicaSecuDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarListaFiltro(
            [FromRoute] int codigoLicenciatario,
            [FromQuery, Required] string filtro,
            [FromQuery] Pageable pageable)
        {
            Page<AgeLicenciatariosAplicaSecuDAO> ageLicenciatariosAplicaSecuDAOList = 
                await _service.ConsultarListaFiltro(codigoLicenciatario, filtro, pageable);

            return Ok(ageLicenciatariosAplicaSecuDAOList);
        }


        [HttpPost]
        [ProducesResponseType(typeof(Resource<AgeLicenciatariosAplicaSecuDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> Insertar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeLicenciatariosAplicaSecuSaveDAO ageLicenciatariosAplicaSecuSaveDAO)
        {
            Resource<AgeLicenciatariosAplicaSecuDAO> ageLicenciatariosAplicaSecuDAO = 
                await _service.Insertar(_httpContextAccessor, ageLicenciatariosAplicaSecuSaveDAO);

            return Ok(ageLicenciatariosAplicaSecuDAO);
        }


        [HttpPost("varios")]
        [ProducesResponseType(typeof(List<Resource<AgeLicenciatariosAplicaSecuDAO>>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> InsertarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeLicenciatariosAplicaSecuSaveDAO> ageLicenciatariosAplicaSecuSaveDAOList)
        {
            List<Resource<AgeLicenciatariosAplicaSecuDAO>> ageLicenciatariosAplicaSecuDAOList = 
                await _service.InsertarVarios(_httpContextAccessor, ageLicenciatariosAplicaSecuSaveDAOList);

            return Ok(ageLicenciatariosAplicaSecuDAOList);
        }


        [HttpPut]
        [ProducesResponseType(typeof(AgeLicenciatariosAplicaSecuDAO), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> Actualizar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeLicenciatariosAplicaSecuSaveDAO ageLicenciatariosAplicaSecuSaveDAO)
        {
            AgeLicenciatariosAplicaSecuDAO ageLicenciatariosAplicaSecuDAO = 
                await _service.Actualizar(_httpContextAccessor, ageLicenciatariosAplicaSecuSaveDAO);

            return Ok(ageLicenciatariosAplicaSecuDAO);
        }


        [HttpPut("varios")]
        [ProducesResponseType(typeof(List<AgeLicenciatariosAplicaSecuDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ActualizarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeLicenciatariosAplicaSecuSaveDAO> ageLicenciatariosAplicaSecuSaveDAOList)
        {
            List<AgeLicenciatariosAplicaSecuDAO> ageLicenciatariosAplicaSecuDAOList = 
                await _service.ActualizarVarios(_httpContextAccessor, ageLicenciatariosAplicaSecuSaveDAOList);

            return Ok(ageLicenciatariosAplicaSecuDAOList);
        }
    }
}
