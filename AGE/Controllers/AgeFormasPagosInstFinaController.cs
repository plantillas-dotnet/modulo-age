﻿using AGE.Entities.DAO.AgeFormasPagosInstFina;
using AGE.Services.AgeFormasPagoInstFinaService;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace AGE.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class AgeFormasPagosInstFinaController : ControllerBase
    {

        private readonly IAgeFormasPagosInstFinaService _service;

        public AgeFormasPagosInstFinaController(IAgeFormasPagosInstFinaService Service)
        {
            _service = Service;
        }

        [HttpGet("{codigoLicenciatario}")]
        [ProducesResponseType(typeof(Page<AgeFormasPagosInstFinaDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarTodos(
            [FromRoute] int codigoLicenciatario,
            [FromQuery] int? ageForPaAgeLicencCodigo,
            [FromQuery] int? ageForPaCodigo,
            [FromQuery] string? descripcion,
            [FromQuery] Pageable pageable)
        {
            Page<AgeFormasPagosInstFinaDAO> ageFormasPagosInstFinaDAOList =
                await _service.ConsultarTodos(codigoLicenciatario, ageForPaAgeLicencCodigo ?? 0, ageForPaCodigo ?? 0, descripcion ?? "", pageable);

            return Ok(ageFormasPagosInstFinaDAOList);
        }


        [HttpGet("{codigoLicenciatario}/{id}")]
        [ProducesResponseType(typeof(AgeFormasPagosInstFinaDAO), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarPorId(
            [FromRoute] AgeFormasPagosInstFinaPKDAO ageFormasPagosInstFinaPKDAO)
        {
            AgeFormasPagosInstFinaDAO ageFormasPagosInstFinaDAO =
                await _service.ConsultarPorId(ageFormasPagosInstFinaPKDAO);

            return Ok(ageFormasPagosInstFinaDAO);
        }



        [HttpGet("filtro/{codigoLicenciatario}")]
        [ProducesResponseType(typeof(Page<AgeFormasPagosInstFinaDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarListaFiltro(
            [FromRoute] int codigoLicenciatario,
            [FromQuery, Required] string filtro,
            [FromQuery] Pageable pageable)
        {
            Page<AgeFormasPagosInstFinaDAO> ageFormasPagosInstFinaDAOList =
                await _service.ConsultarListaFiltro(codigoLicenciatario, filtro, pageable);

            return Ok(ageFormasPagosInstFinaDAOList);
        }


        [HttpPost]
        [ProducesResponseType(typeof(Resource<AgeFormasPagosInstFinaDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> Insertar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeFormasPagosInstFinaSaveDAO ageFormasPagosInstFinaSaveDAO)
        {
            Resource<AgeFormasPagosInstFinaDAO> ageFormasPagosInstFinaDAO =
                await _service.Insertar(_httpContextAccessor, ageFormasPagosInstFinaSaveDAO);

            return Ok(ageFormasPagosInstFinaDAO);
        }


        [HttpPost("varios")]
        [ProducesResponseType(typeof(List<Resource<AgeFormasPagosInstFinaDAO>>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> InsertarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeFormasPagosInstFinaSaveDAO> ageFormasPagosInstFinaSaveDAOList)
        {
            List<Resource<AgeFormasPagosInstFinaDAO>> ageFormasPagosInstFinaDAOList =
                await _service.InsertarVarios(_httpContextAccessor, ageFormasPagosInstFinaSaveDAOList);

            return Ok(ageFormasPagosInstFinaDAOList);
        }


        [HttpPut]
        [ProducesResponseType(typeof(AgeFormasPagosInstFinaDAO), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> Actualizar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeFormasPagosInstFinaSaveDAO ageFormasPagosInstFinaSaveDAO)
        {
            AgeFormasPagosInstFinaDAO ageFormasPagosInstFinaDAO =
                await _service.Actualizar(_httpContextAccessor, ageFormasPagosInstFinaSaveDAO);

            return Ok(ageFormasPagosInstFinaDAO);
        }


        [HttpPut("varios")]
        [ProducesResponseType(typeof(List<AgeFormasPagosInstFinaDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ActualizarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeFormasPagosInstFinaSaveDAO> ageFormasPagosInstFinaSaveDAOList)
        {
            List<AgeFormasPagosInstFinaDAO> ageFormasPagosInstFinaDAOList =
                await _service.ActualizarVarios(_httpContextAccessor, ageFormasPagosInstFinaSaveDAOList);

            return Ok(ageFormasPagosInstFinaDAOList);
        }


    }
}
