using AGE.Entities.DAO.AgeTransacciones;
using AGE.Services.AgeTransaccioneService;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace AGE.Controllers
{
    [Route("[controller]")]
    [ApiController]
    //[Authorize]
    public class AgeTransaccionesController : ControllerBase
    {
        private readonly IAgeTransaccionesService _service;

        public AgeTransaccionesController(IAgeTransaccionesService Service)
        {
            _service = Service;
        }

        [HttpGet]
        [ProducesResponseType(typeof(Page<AgeTransaccioneDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarTodos(
            [FromQuery] Pageable pageable)
        {
            Page<AgeTransaccioneDAO> ageTransaccioneDAOList =
                await _service.ConsultarTodos(pageable);

            return Ok(ageTransaccioneDAOList);
        }

        [HttpGet("{codigoAplicacion}/{id}")]
        [ProducesResponseType(typeof(AgeTransaccioneDAO), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarPorId(
        [FromRoute] AgeTransaccionePKDAO ageTransaccionePKDAO)
        {
            AgeTransaccioneDAO ageTransaccioneDAO = await _service.ConsultarPorId(ageTransaccionePKDAO);
            return Ok(ageTransaccioneDAO);
        }


        [HttpGet("transacciones/{usuarioCodigo}/{licenciatarioCodigo}")]
        [ProducesResponseType(typeof(Page<AgeTransaccioneDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarPorUsuarioLicenciatario(
        [FromRoute] long usuarioCodigo,
        [FromRoute] int licenciatarioCodigo,
        [FromQuery] Pageable pageable)
        {
            Page<AgeTransaccioneDAO> ageTransaccioneDAO = await _service
                .ConsultarPorUsuarioLicenciatario(usuarioCodigo, licenciatarioCodigo, pageable);
            
            return Ok(ageTransaccioneDAO);
        }


        [HttpGet("transacciones/{perfilCodigo}/{usuarioCodigo}/{licenciatarioCodigo}")]
        [ProducesResponseType(typeof(Page<AgeTransaccioneDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarPorPerfilUsuarioLicenciatario(
        [FromRoute] long perfilCodigo,
        [FromRoute] long usuarioCodigo,
        [FromRoute] int licenciatarioCodigo,
        [FromQuery] Pageable pageable)
        {
            Page<AgeTransaccioneDAO> ageTransaccioneDAO = await _service
                .ConsultarPorPerfilUsuarioLicenciatario(perfilCodigo, usuarioCodigo, licenciatarioCodigo, pageable);

            return Ok(ageTransaccioneDAO);
        }


        [HttpGet("consulta/{codigoAplicacion}")]
        [ProducesResponseType(typeof(Page<AgeTransaccioneDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarAplicacion(
            [FromRoute, Required] int codigoAplicacion,
            [FromQuery] int codigo,
            [FromQuery] int codigoExterno,
            [FromQuery] string descripcion,
            [FromQuery] string transaccion,
            [FromQuery] short ordenPresentacion,
            [FromQuery] string tipoTransaccion,
            [FromQuery] string tipoOperacion,
            [FromQuery] int ageTransaAgeAplicaCodigo,
            [FromQuery] int ageTransaCodigo,
            [FromQuery] string? opcion,
            [FromQuery] short nivel,
            [FromQuery] string? url,
            [FromQuery] string? parametros,
            [FromQuery] Pageable pageable)
        {
            Page<AgeTransaccioneDAO> ageTransaccioneDAOList =
                await _service.ConsultarAplicacion(
                    codigoAplicacion,
                    codigo,
                    codigoExterno,
                    descripcion ?? "",
                    transaccion ?? "",
                    ordenPresentacion,
                    tipoTransaccion ?? "",
                    tipoOperacion ?? "",
                    ageTransaAgeAplicaCodigo,
                    ageTransaCodigo,
                    opcion,
                    nivel,
                    url,
                    parametros,
                    pageable
                );

            return Ok(ageTransaccioneDAOList);
        }

        [HttpGet("{nivel}")]
        [ProducesResponseType(typeof(Page<AgeTransaccioneDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarPorNivel(
        [FromRoute, Required] short nivel,
        [FromQuery] Pageable pageable)
        {
            Page<AgeTransaccioneDAO> ageTransaccioneDAOList =
                await _service.ConsultarPorNivel(nivel, pageable);

            return Ok(ageTransaccioneDAOList);
        }

        [HttpGet("filtro")]
        [ProducesResponseType(typeof(Page<AgeTransaccioneDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarListaFiltro(
            [FromQuery, Required] string filtro,
            [FromQuery] Pageable pageable)
        {
            Page<AgeTransaccioneDAO> ageTransaccioneDAOList =
                await _service.ConsultarListaFiltro(filtro, pageable);

            return Ok(ageTransaccioneDAOList);
        }


        [HttpPost]
        [ProducesResponseType(typeof(Resource<AgeTransaccioneDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> Insertar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeTransaccioneSaveDAO ageTransaccioneSaveDAO)
        {
            Resource<AgeTransaccioneDAO> ageTransaccioneDAO = 
                await _service.Insertar(_httpContextAccessor, ageTransaccioneSaveDAO);
            return Ok(ageTransaccioneDAO);
        }


        [HttpPost("varios")]
        [ProducesResponseType(typeof(List<Resource<AgeTransaccioneDAO>>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> InsertarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeTransaccioneSaveDAO> ageTransaccioneSaveDAOList)
        {
            List<Resource<AgeTransaccioneDAO>> ageTransaccioneDAOList =
                await _service.InsertarVarios(_httpContextAccessor, ageTransaccioneSaveDAOList);

            return Ok(ageTransaccioneDAOList);
        }


        [HttpPut]
        [ProducesResponseType(typeof(AgeTransaccioneDAO), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> Actualizar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeTransaccioneSaveDAO ageTransaccioneSaveDAO)
        {
            AgeTransaccioneDAO ageTransaccioneDAO = await _service.Actualizar(_httpContextAccessor, ageTransaccioneSaveDAO);
            return Ok(ageTransaccioneDAO);
        }


        [HttpPut("varios")]
        [ProducesResponseType(typeof(List<AgeTransaccioneDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ActualizarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeTransaccioneSaveDAO> ageTransaccioneSaveDAOList)
        {
            List<AgeTransaccioneDAO> ageTransaccioneDAOList =
                await _service.ActualizarVarios(_httpContextAccessor, ageTransaccioneSaveDAOList);

            return Ok(ageTransaccioneDAOList);
        }

    }

}

