﻿using AGE.Entities.DAO.AgeSistemas;
using AGE.Services.AgeSistemasService;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace AGE.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class AgeSistemasController : Controller
    {
        private readonly IAgeSistemasService _service;

        public AgeSistemasController(IAgeSistemasService Service)
        {
            _service = Service;
        }

        [HttpGet]
        [ProducesResponseType(typeof(Page<AgeSistemasDAO>), 200)]
        public async Task<IActionResult> ConsultarTodos(
            [FromQuery] int? codigo,
            [FromQuery] string? descripcion,
            [FromQuery] Pageable pageable)
        {
            Page<AgeSistemasDAO> ageSistemasDAOList =
                await _service.ConsultarTodos(codigo ?? 0, descripcion ?? "", pageable);

            return Ok(ageSistemasDAOList);
        }


        [HttpGet("filtro")]
        [ProducesResponseType(typeof(Page<AgeSistemasDAO>), 200)]
        public async Task<IActionResult> ConsultarListaFiltro(
            [FromQuery, Required] string filtro,
            [FromQuery] Pageable pageable)
        {
            Page<AgeSistemasDAO> ageSistemasDAOList =
                await _service.ConsultarListaFiltro(filtro, pageable);

            return Ok(ageSistemasDAOList);
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(AgeSistemasDAO), 200)]
        public async Task<IActionResult> ConsultarPorId(
            [FromRoute, Required] int id)
        {
            AgeSistemasDAO ageSistemasDAO = await _service.ConsultarPorId(id);
            return Ok(ageSistemasDAO);
        }

        [HttpPost]
        [ProducesResponseType(typeof(Resource<AgeSistemasDAO>), 200)]
        public async Task<IActionResult> Insertar(
            [FromServices] IHttpContextAccessor httpContextAccessor,
            [FromBody] AgeSistemasSaveDAO ageSistemasSaveDAO)
        {
            Resource<AgeSistemasDAO> ageSistemasDAO = await _service.Insertar(httpContextAccessor, ageSistemasSaveDAO);
            return Ok(ageSistemasDAO);
        }

        [HttpPost("varios")]
        [ProducesResponseType(typeof(List<Resource<AgeSistemasDAO>>), 200)]
        public async Task<IActionResult> InsertarVarios(
            [FromServices] IHttpContextAccessor httpContextAccessor,
            [FromBody] List<AgeSistemasSaveDAO> ageSistemasSaveDAOList)
        {
            List<Resource<AgeSistemasDAO>> ageSistemasDAOList = await _service.InsertarVarios(httpContextAccessor, ageSistemasSaveDAOList);
            return Ok(ageSistemasDAOList);
        }

        [HttpPut]
        [ProducesResponseType(typeof(AgeSistemasDAO), 200)]
        public async Task<IActionResult> Actualizar(
            [FromServices] IHttpContextAccessor httpContextAccessor,
            [FromBody] AgeSistemasSaveDAO ageSistemasSaveDAO)
        {
            AgeSistemasDAO ageSistemasDAO = await _service.Actualizar(httpContextAccessor, ageSistemasSaveDAO);
            return Ok(ageSistemasDAO);
        }

        [HttpPut("varios")]
        [ProducesResponseType(typeof(List<AgeSistemasDAO>), 200)]
        public async Task<IActionResult> ActualizarVarios(
            [FromServices] IHttpContextAccessor httpContextAccessor,
            [FromBody] List<AgeSistemasSaveDAO> ageSistemasSaveDAOList)
        {
            List<AgeSistemasDAO> ageSistemasDAOList = await _service.ActualizarVarios(httpContextAccessor, ageSistemasSaveDAOList);
            return Ok(ageSistemasDAOList);
        }
    }
}
