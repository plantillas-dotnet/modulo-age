﻿using AGE.Entities.DAO.AgeLicenciatariosPaises;
using AGE.Services.AgeLicenciatariosPaiseService;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace AGE.Controllers
{
    [Route("[controller]")]
    [ApiController]
    //[Authorize]
    public class AgeLicenciatariosPaisesController : ControllerBase
    {
        private readonly IAgeLicenciatariosPaisesService _service;

        public AgeLicenciatariosPaisesController(IAgeLicenciatariosPaisesService service)
        {
            _service = service;
        }

        [HttpGet("{codigoLicenciatario}")]
        [ProducesResponseType(typeof(Page<AgeLicenciatariosPaisesDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarTodos(
            [FromRoute] int codigoLicenciatario,
            [FromQuery] int? codigoPais,
            [FromQuery] string? principal,
            [FromQuery] int? AgeLicPaAgeLicencCodigo,
            [FromQuery] int? AgeLicPaAgePaisCodigo,
            [FromQuery] Pageable pageable)
        {
            Page<AgeLicenciatariosPaisesDAO> ageLicenciatariosPaiseDAOList =
                await _service.ConsultarTodos(codigoLicenciatario, codigoPais ?? 0, principal ?? "",
                                              AgeLicPaAgeLicencCodigo ?? 0, AgeLicPaAgePaisCodigo ?? 0, pageable);

            return Ok(ageLicenciatariosPaiseDAOList);
        }

        [HttpGet("{codigoLicenciatario}/{codigoPais}")]
        [ProducesResponseType(typeof(AgeLicenciatariosPaisesDAO), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarPorId(
            [FromRoute] AgeLicenciatariosPaisesPKDAO ageLicenciatariosPaisePKDAO)
        {
            AgeLicenciatariosPaisesDAO ageLicenciatariosPaiseDAO = await _service.ConsultarPorId(ageLicenciatariosPaisePKDAO);
            return Ok(ageLicenciatariosPaiseDAO);
        }

        [HttpGet("filtro")]
        [ProducesResponseType(typeof(Page<AgeLicenciatariosPaisesDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarListaFiltro(
            [FromQuery, Required] string filtro,
            [FromQuery] Pageable pageable)
        {
            Page<AgeLicenciatariosPaisesDAO> ageLicenciatariosPaiseDAOList =
                await _service.ConsultarListaFiltro(filtro, pageable);

            return Ok(ageLicenciatariosPaiseDAOList);
        }

        [HttpPost]
        [ProducesResponseType(typeof(Resource<AgeLicenciatariosPaisesDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> Insertar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeLicenciatariosPaisesSaveDAO ageTiposLocalidadeSaveDAO)
        {
            Resource<AgeLicenciatariosPaisesDAO> ageLicenciatariosPaiseDAO =
                await _service.Insertar(_httpContextAccessor, ageTiposLocalidadeSaveDAO);
            return Ok(ageLicenciatariosPaiseDAO);
        }


        [HttpPost("varios")]
        [ProducesResponseType(typeof(List<Resource<AgeLicenciatariosPaisesDAO>>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> InsertarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeLicenciatariosPaisesSaveDAO> ageLicenciatariosPaiseSaveDAOList)
        {
            List<Resource<AgeLicenciatariosPaisesDAO>> ageLicenciatariosPaiseDAOList =
                await _service.InsertarVarios(_httpContextAccessor, ageLicenciatariosPaiseSaveDAOList);

            return Ok(ageLicenciatariosPaiseDAOList);
        }


        [HttpPut]
        [ProducesResponseType(typeof(AgeLicenciatariosPaisesDAO), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> Actualizar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeLicenciatariosPaisesSaveDAO ageLicenciatariosPaiseSaveDAO)
        {
            AgeLicenciatariosPaisesDAO ageLicenciatariosPaiseDAO = await _service.Actualizar(
                _httpContextAccessor, ageLicenciatariosPaiseSaveDAO);
            return Ok(ageLicenciatariosPaiseDAO);
        }


        [HttpPut("varios")]
        [ProducesResponseType(typeof(List<AgeLicenciatariosPaisesDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ActualizarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeLicenciatariosPaisesSaveDAO> ageLicenciatariosPaiseSaveDAOList)
        {
            List<AgeLicenciatariosPaisesDAO> ageLicenciatariosPaiseDAOList =
                await _service.ActualizarVarios(_httpContextAccessor, ageLicenciatariosPaiseSaveDAOList);

            return Ok(ageLicenciatariosPaiseDAOList);
        }
    }
}
