﻿using AGE.Entities.DAO.AgeLogUsuarios;
using AGE.Services.AgeLogUsuariosService;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace AGE.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class AgeLogUsuariosController : ControllerBase
    {
        private readonly IAgeLogUsuariosService _service;

        public AgeLogUsuariosController(IAgeLogUsuariosService service)
        {
            _service = service;
        }

        [HttpGet("{ageUsuariAgeLicencCodigo}")]
        [ProducesResponseType(typeof(Page<AgeLogUsuariosDAO>), 200)]
        public async Task<IActionResult> ConsultarTodos(
            [FromRoute] int ageUsuariAgeLicencCodigo,
            [FromQuery] int? ageUsuariCodigo,
            [FromQuery] int? codigo,
            [FromQuery] string? campo,
            [FromQuery] string? valorAnterior,
            [FromQuery] string? valorActual,
            [FromQuery] Pageable pageable)
        {
            Page<AgeLogUsuariosDAO> ageLogUsuariosDAOList =
                await _service.ConsultarTodos(ageUsuariAgeLicencCodigo, ageUsuariCodigo ?? 0, codigo ?? 0, campo ?? "", valorAnterior ?? "", valorActual ?? "", pageable);

            return Ok(ageLogUsuariosDAOList);
        }

        [HttpGet("{ageUsuariAgeLicencCodigo}/{ageUsuariCodigo}/{codigo}")]
        [ProducesResponseType(typeof(AgeLogUsuariosDAO), 200)]
        public async Task<IActionResult> ConsultarPorId(
            [FromRoute] AgeLogUsuariosPKDAO ageLogUsuariosPKDAP)
        {
            var ageLogUsuariosDAO = await _service.ConsultarPorId(ageLogUsuariosPKDAP);
            return Ok(ageLogUsuariosDAO);
        }

        [HttpGet("filtro/{ageUsuariAgeLicencCodigo}")]
        [ProducesResponseType(typeof(Page<AgeLogUsuariosDAO>), 200)]
        public async Task<IActionResult> ConsultarListaFiltro(
           [FromRoute, Required] int ageUsuariAgeLicencCodigo,
           [FromQuery, Required] string filtro,
           [FromQuery] Pageable pageable)
        {
            Page<AgeLogUsuariosDAO> ageLogUsuariosDAO =
                await _service.ConsultarListaFiltro(ageUsuariAgeLicencCodigo, filtro, pageable);

            return Ok(ageLogUsuariosDAO);
        }

        [HttpPost]
        [ProducesResponseType(typeof(Resource<AgeLogUsuariosDAO>), 200)]
        public async Task<IActionResult> Insertar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeLogUsuariosSaveDAO ageLogUsuariosSave)
        {
            Resource<AgeLogUsuariosDAO> ageLogUsuariosDAO =
                await _service.Insertar(_httpContextAccessor, ageLogUsuariosSave);

            return Ok(ageLogUsuariosDAO);
        }


        [HttpPost("varios")]
        [ProducesResponseType(typeof(List<Resource<AgeLogUsuariosDAO>>), 200)]
        public async Task<IActionResult> InsertarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeLogUsuariosSaveDAO> ageLogUsuariosSaveDAOList)
        {
            List<Resource<AgeLogUsuariosDAO>> ageLogUsuariosDAOList =
                await _service.InsertarVarios(_httpContextAccessor, ageLogUsuariosSaveDAOList);

            return Ok(ageLogUsuariosDAOList);
        }

        [HttpPut]
        [ProducesResponseType(typeof(AgeLogUsuariosDAO), 200)]
        public async Task<IActionResult> Actualizar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeLogUsuariosSaveDAO ageLogUsuariosSaveDAO)
        {
            AgeLogUsuariosDAO ageLogUsuariosDAO =
                await _service.Actualizar(_httpContextAccessor, ageLogUsuariosSaveDAO);

            return Ok(ageLogUsuariosDAO);
        }


        [HttpPut("varios")]
        [ProducesResponseType(typeof(List<AgeLogUsuariosDAO>), 200)]
        public async Task<IActionResult> ActualizarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeLogUsuariosSaveDAO> ageLogUsuariosSaveDAOList)
        {
            List<AgeLogUsuariosDAO> ageLogUsuariosDAOList =
                await _service.ActualizarVarios(_httpContextAccessor, ageLogUsuariosSaveDAOList);

            return Ok(ageLogUsuariosDAOList);
        }
    }
}
