﻿
using AGE.Entities.DAO.AgeMensajesIdiomas;
using AGE.Services.AgeMensajesIdiomasService;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace AGE.Controllers
{

    [Route("[controller]")]
    [ApiController]
    //[Authorize]
    public class AgeMensajesIdiomasController : ControllerBase
    {

        private readonly IAgeMensajesIdiomaService _service;

        public AgeMensajesIdiomasController(IAgeMensajesIdiomaService Service)
        {
            _service = Service;
        }

        [HttpGet]
        [ProducesResponseType(typeof(Page<AgeMensajesIdiomaDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarTodos(
            [FromQuery] long? codigoMensaje,
            [FromQuery] int? codigoIdioma,
            [FromQuery] string? descripcionMsg,
            [FromQuery] string? solucionMsg,
            [FromQuery] Pageable pageable)
        {
            Page<AgeMensajesIdiomaDAO> ageMensajesIdiomaDAOList =
                await _service.ConsultarTodos(codigoMensaje ?? 0, codigoIdioma ?? 0, descripcionMsg ?? "", solucionMsg ?? "", pageable);

            return Ok(ageMensajesIdiomaDAOList);
        }


        [HttpGet("{codigoMensaje}/{codigoIdioma}")]
        [ProducesResponseType(typeof(AgeMensajesIdiomaDAO), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarPorId(
            [FromRoute] AgeMensajesIdiomaPKDAO ageMensajesIdiomaPKDAO)
        {
            AgeMensajesIdiomaDAO ageMensajesIdiomaDAO =
                await _service.ConsultarPorId(ageMensajesIdiomaPKDAO);

            return Ok(ageMensajesIdiomaDAO);
        }



        [HttpGet("filtro")]
        [ProducesResponseType(typeof(Page<AgeMensajesIdiomaDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarListaFiltro(
            [FromQuery, Required] string filtro,
            [FromQuery] Pageable pageable)
        {
            Page<AgeMensajesIdiomaDAO> ageMensajesIdiomaDAOList =
                await _service.ConsultarListaFiltro(filtro, pageable);

            return Ok(ageMensajesIdiomaDAOList);
        }


        [HttpPost]
        [ProducesResponseType(typeof(Resource<AgeMensajesIdiomaDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> Insertar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeMensajesIdiomaSaveDAO ageMensajesIdiomaSaveDAO)
        {
            Resource<AgeMensajesIdiomaDAO> ageMensajesIdiomaDAO =
                await _service.Insertar(_httpContextAccessor, ageMensajesIdiomaSaveDAO);

            return Ok(ageMensajesIdiomaDAO);
        }


        [HttpPost("varios")]
        [ProducesResponseType(typeof(List<Resource<AgeMensajesIdiomaDAO>>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> InsertarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeMensajesIdiomaSaveDAO> ageMensajesIdiomaSaveDAOList)
        {
            List<Resource<AgeMensajesIdiomaDAO>> ageMensajesIdiomaDAOList =
                await _service.InsertarVarios(_httpContextAccessor, ageMensajesIdiomaSaveDAOList);

            return Ok(ageMensajesIdiomaDAOList);
        }


        [HttpPut]
        [ProducesResponseType(typeof(AgeMensajesIdiomaDAO), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> Actualizar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeMensajesIdiomaSaveDAO ageMensajesIdiomaSaveDAO)
        {
            AgeMensajesIdiomaDAO ageMensajesIdiomaDAO =
                await _service.Actualizar(_httpContextAccessor, ageMensajesIdiomaSaveDAO);

            return Ok(ageMensajesIdiomaDAO);
        }


        [HttpPut("varios")]
        [ProducesResponseType(typeof(List<AgeMensajesIdiomaDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ActualizarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeMensajesIdiomaSaveDAO> ageMensajesIdiomaSaveDAOList)
        {
            List<AgeMensajesIdiomaDAO> ageMensajesIdiomaDAOList =
                await _service.ActualizarVarios(_httpContextAccessor, ageMensajesIdiomaSaveDAOList);

            return Ok(ageMensajesIdiomaDAOList);
        }

    }
}
