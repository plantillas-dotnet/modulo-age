using AGE.Entities.DAO.AgeParamGeneralVigencias;
using AGE.Services.AgeParamGeneralVigenciasService;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace AGE.Controllers
{
    [Route("[controller]")]
    [ApiController]
    //[Authorize]
    public class AgePGVigenciasController : ControllerBase
    {
        private readonly IAgeParamGeneralVigenciasService _service;

        public AgePGVigenciasController(IAgeParamGeneralVigenciasService Service)
        {
            _service = Service;
        }

        [HttpGet("{codigoPG}")]
        [ProducesResponseType(typeof(Page<AgeParamGeneralVigenciasDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarTodos(
            [FromQuery] int? codigoPG,
            [FromQuery] int? codigo,
            [FromQuery] string? descripcion,
            [FromQuery] string? valorParametro,
            [FromQuery] Pageable pageable)
        {
            Page<AgeParamGeneralVigenciasDAO> ageParamGeneralVigenciasDAOList =
                await _service.ConsultarTodos(codigoPG ?? 0, codigo ?? 0,
                                              descripcion ?? "", valorParametro ?? "", pageable);

            return Ok(ageParamGeneralVigenciasDAOList);
        }

        [HttpGet("{codigoPG}/{id}")]
        [ProducesResponseType(typeof(AgeParamGeneralVigenciasDAO), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarPorId(
            [FromRoute] int codigoPG,
            [FromRoute] int id)
        {
            AgeParamGeneralVigenciasDAO ageParamGeneralVigenciasDAO = await _service.ConsultarPorId(codigoPG, id);
            return Ok(ageParamGeneralVigenciasDAO);
        }

        [HttpGet("filtro")]
        [ProducesResponseType(typeof(Page<AgeParamGeneralVigenciasDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarListaFiltro(
            [FromQuery, Required] string filtro,
            [FromQuery] Pageable pageable)
        {
            Page<AgeParamGeneralVigenciasDAO> ageParamGeneralVigenciasDAOList =
                await _service.ConsultarListaFiltro(filtro, pageable);

            return Ok(ageParamGeneralVigenciasDAOList);
        }

        [HttpPut]
        [ProducesResponseType(typeof(AgeParamGeneralVigenciasDAO), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> Actualizar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeParamGeneralVigenciasSaveDAO ageParamGeneralVigenciasSaveDAO)
        {
            AgeParamGeneralVigenciasDAO ageParamGeneralVigenciasDAO = await _service.Actualizar(_httpContextAccessor, ageParamGeneralVigenciasSaveDAO);
            return Ok(ageParamGeneralVigenciasDAO);
        }

        [HttpPost]
        [ProducesResponseType(typeof(Resource<AgeParamGeneralVigenciasDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> Insertar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeParamGeneralVigenciasSaveDAO ageParamGeneralVigenciasSaveDAO)
        {
            Resource<AgeParamGeneralVigenciasDAO> ageParamGeneralVigenciasDAO = await _service.Insertar(_httpContextAccessor, ageParamGeneralVigenciasSaveDAO);
            return Ok(ageParamGeneralVigenciasDAO);
        }

        [HttpPut("varios")]
        [ProducesResponseType(typeof(List<AgeParamGeneralVigenciasDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ActualizarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeParamGeneralVigenciasSaveDAO> ageParamGeneralVigenciasSaveDAOList)
        {
            List<AgeParamGeneralVigenciasDAO> ageParamGeneralVigenciasDAOList =
                await _service.ActualizarVarios(_httpContextAccessor, ageParamGeneralVigenciasSaveDAOList);

            return Ok(ageParamGeneralVigenciasDAOList);
        }

        [HttpPost("varios")]
        [ProducesResponseType(typeof(List<Resource<AgeParamGeneralVigenciasDAO>>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> InsertarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeParamGeneralVigenciasSaveDAO> ageParamGeneralVigenciasSaveDAOList)
        {
            List<Resource<AgeParamGeneralVigenciasDAO>> ageParamGeneralVigenciasDAOList =
                await _service.InsertarVarios(_httpContextAccessor, ageParamGeneralVigenciasSaveDAOList);

            return Ok(ageParamGeneralVigenciasDAOList);
        }


        
    }
}
