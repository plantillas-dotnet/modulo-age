using AGE.Entities.DAO.AgeTransaccionesIdioma;
using AGE.Services.AgeTransaccionesIdiomaService;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace AGE.Controllers
{
    [Route("[controller]")]
    [ApiController]
    //[Authorize]
    public class AgeTransaccionesIdiomasController : ControllerBase
    {
        private readonly IAgeTransaccionesIdiomasService _service;

        public AgeTransaccionesIdiomasController(IAgeTransaccionesIdiomasService service)
        {
            _service = service;
        }

        [HttpGet]
        [ProducesResponseType(typeof(Page<AgeTransaccionesIdiomasDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarTodos(
            [FromQuery] Pageable pageable)
        {
            Page<AgeTransaccionesIdiomasDAO> ageTransaccionesIdiomaDAOList =
                await _service.ConsultarTodos(pageable);

            return Ok(ageTransaccionesIdiomaDAOList);
        }

        [HttpGet("{codigoTransaccion}/{codigoAplicacion}/{codigoIdioma}")]
        [ProducesResponseType(typeof(AgeTransaccionesIdiomasDAO), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarPorId(
            [FromRoute] AgeTransaccionesIdiomasPKDAO ageTransaccionesIdiomaPKDAO)
        {
            AgeTransaccionesIdiomasDAO ageTransaccionesIdiomaDAO = await _service.ConsultarPorId(ageTransaccionesIdiomaPKDAO);
            return Ok(ageTransaccionesIdiomaDAO);
        }

        [HttpGet("filtro")]
        [ProducesResponseType(typeof(Page<AgeTransaccionesIdiomasDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarListaFiltro(
            [FromQuery, Required] string filtro,
            [FromQuery] Pageable pageable)
        {
            Page<AgeTransaccionesIdiomasDAO> ageTransaccionesIdiomaDAOList =
                await _service.ConsultarListaFiltro(filtro, pageable);

            return Ok(ageTransaccionesIdiomaDAOList);
        }


        [HttpPost]
        [ProducesResponseType(typeof(Resource<AgeTransaccionesIdiomasDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> Insertar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeTransaccionesIdiomasSaveDAO ageTiposLocalidadeSaveDAO)
        {
            Resource<AgeTransaccionesIdiomasDAO> ageTransaccionesIdiomaDAO =
                await _service.Insertar(_httpContextAccessor, ageTiposLocalidadeSaveDAO);
            return Ok(ageTransaccionesIdiomaDAO);
        }


        [HttpPost("varios")]
        [ProducesResponseType(typeof(List<Resource<AgeTransaccionesIdiomasDAO>>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> InsertarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeTransaccionesIdiomasSaveDAO> ageTransaccionesIdiomaSaveDAOList)
        {
            List<Resource<AgeTransaccionesIdiomasDAO>> ageTransaccionesIdiomaDAOList =
                await _service.InsertarVarios(_httpContextAccessor, ageTransaccionesIdiomaSaveDAOList);

            return Ok(ageTransaccionesIdiomaDAOList);
        }


        [HttpPut]
        [ProducesResponseType(typeof(AgeTransaccionesIdiomasDAO), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> Actualizar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeTransaccionesIdiomasSaveDAO ageTransaccionesIdiomaSaveDAO)
        {
            AgeTransaccionesIdiomasDAO ageTransaccionesIdiomaDAO = await _service.Actualizar(
                _httpContextAccessor, ageTransaccionesIdiomaSaveDAO);
            return Ok(ageTransaccionesIdiomaDAO);
        }


        [HttpPut("varios")]
        [ProducesResponseType(typeof(List<AgeTransaccionesIdiomasDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ActualizarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeTransaccionesIdiomasSaveDAO> ageTransaccionesIdiomaSaveDAOList)
        {
            List<AgeTransaccionesIdiomasDAO> ageTransaccionesIdiomaDAOList =
                await _service.ActualizarVarios(_httpContextAccessor, ageTransaccionesIdiomaSaveDAOList);

            return Ok(ageTransaccionesIdiomaDAOList);
        }
    }
}
