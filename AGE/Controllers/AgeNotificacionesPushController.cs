﻿using AGE.Entities.DAO.AgeNotificacionesPush;
using AGE.Entities.DAO.AgePerfiles;
using AGE.Services.AgeNotificacionesPushService;
using AGE.Services.AgePerfilesService;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace AGE.Controllers
{

    [Route("[controller]")]
    [ApiController]
    public class AgeNotificacionesPushController : ControllerBase
    {

        private readonly IAgeNotificacionesPushService _service;

        public AgeNotificacionesPushController(IAgeNotificacionesPushService Service)
        {
            _service = Service;
        }

        [HttpGet("{codigoLicenciatario}")]
        [ProducesResponseType(typeof(Page<AgeNotificacionesPushDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarTodos(
            [FromRoute] int codigoLicenciatario,
            [FromQuery] int? codigo,
            [FromQuery] string? mensaje,
            [FromQuery] string? topico,
            [FromQuery] Pageable pageable)
        {
            Page<AgeNotificacionesPushDAO> agePerfilesDAOList =
                await _service.ConsultarTodos(codigoLicenciatario, codigo ?? 0, mensaje ?? "", topico ?? "", pageable);

            return Ok(agePerfilesDAOList);
        }



        [HttpGet("filtro/{codigoLicenciatario}")]
        [ProducesResponseType(typeof(Page<AgeNotificacionesPushDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarListaFiltro(
            [FromRoute] int codigoLicenciatario,
            [FromQuery, Required] string filtro,
            [FromQuery] Pageable pageable)
        {
            Page<AgeNotificacionesPushDAO> agePerfilesDAOList =
                await _service.ConsultarListaFiltro(codigoLicenciatario, filtro, pageable);

            return Ok(agePerfilesDAOList);
        }


        [HttpPost]
        [ProducesResponseType(typeof(Resource<AgeNotificacionesPushDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> Insertar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeNotificacionesPushSaveDAO ageNotificacionesPushSaveDAO)
        {
            Resource<AgeNotificacionesPushDAO> ageNotificacionesPushDAO =
                await _service.Insertar(_httpContextAccessor, ageNotificacionesPushSaveDAO);

            return Ok(ageNotificacionesPushDAO);
        }


        [HttpPost("varios")]
        [ProducesResponseType(typeof(List<Resource<AgeNotificacionesPushDAO>>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> InsertarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeNotificacionesPushSaveDAO> agePerfilesSaveDAOList)
        {
            List<Resource<AgeNotificacionesPushDAO>> agePerfilesDAOList =
                await _service.InsertarVarios(_httpContextAccessor, agePerfilesSaveDAOList);

            return Ok(agePerfilesDAOList);
        }

    }
}
