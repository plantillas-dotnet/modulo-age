﻿using AGE.Entities.DAO.AgeDepartamentos;
using AGE.Services.AgeDepartamentosService;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace AGE.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class AgeDepartamentosController : ControllerBase
    {
        private readonly IAgeDepartamentosService _service;

        public AgeDepartamentosController(IAgeDepartamentosService Service)
        {
            _service = Service;
        }

        [HttpGet("{codigoLicenciatario}")]
        [ProducesResponseType(typeof(Page<AgeDepartamentosDAO>), 200)]
        public async Task<IActionResult> ConsultarTodos([FromRoute] int codigoLicenciatario,
                                                        [FromQuery] string descripcion,
                                                        [FromQuery] Pageable pageable)
        {
            Page<AgeDepartamentosDAO> ageDepartamentosDAOList =
                await _service.ConsultarTodos(codigoLicenciatario,descripcion, pageable);

            return Ok(ageDepartamentosDAOList);
        }

        [HttpGet("{codigoLicenciatario}/{id}")]
        [ProducesResponseType(typeof(AgeDepartamentosDAO), 200)]
        public async Task<IActionResult> ConsultarPorId(
            [FromRoute] AgeDepartamentosPKDAO ageDepartamentosPKDAO)
        {
            var ageDepartamentosDAO = await _service.ConsultarPorId(ageDepartamentosPKDAO);
            return Ok(ageDepartamentosDAO);
        }

        [HttpGet("filtro/{codigoLicenciatario}")]
        [ProducesResponseType(typeof(Page<AgeDepartamentosDAO>), 200)]
        public async Task<IActionResult> ConsultarListaFiltro(
           [FromRoute, Required] int codigoLicenciatario,
           [FromQuery, Required] string filtro,
           [FromQuery] Pageable pageable)
        {
            Page<AgeDepartamentosDAO> ageDepartamentosDAO =
                await _service.ConsultarListaFiltro(codigoLicenciatario, filtro, pageable);

            return Ok(ageDepartamentosDAO);
        }

        [HttpPost]
        [ProducesResponseType(typeof(Resource<AgeDepartamentosDAO>), 200)]
        public async Task<IActionResult> Insertar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeDepartamentosSaveDAO ageDepartamentosSaveDAO)
        {
            Resource<AgeDepartamentosDAO> ageDepartamentosDAO =
                await _service.Insertar(_httpContextAccessor, ageDepartamentosSaveDAO);

            return Ok(ageDepartamentosDAO);
        }

        [HttpPost("varios")]
        [ProducesResponseType(typeof(List<Resource<AgeDepartamentosDAO>>), 200)]
        public async Task<IActionResult> InsertarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeDepartamentosSaveDAO> ageDepartamentosSaveDAOList)
        {
            List<Resource<AgeDepartamentosDAO>> ageDepartamentosDAOList =
                await _service.InsertarVarios(_httpContextAccessor, ageDepartamentosSaveDAOList);

            return Ok(ageDepartamentosDAOList);
        }

        [HttpPut]
        [ProducesResponseType(typeof(AgeDepartamentosDAO), 200)]
        public async Task<IActionResult> Actualizar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeDepartamentosSaveDAO ageDepartamentosSaveDAO)
        {
            AgeDepartamentosDAO ageDepartamentosDAO = await _service.Actualizar(_httpContextAccessor, ageDepartamentosSaveDAO);
            return Ok(ageDepartamentosDAO);
        }

        [HttpPut("varios")]
        [ProducesResponseType(typeof(List<AgeDepartamentosDAO>), 200)]
        public async Task<IActionResult> ActualizarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeDepartamentosSaveDAO> ageDepartamentosSaveDAOList)
        {
            List<AgeDepartamentosDAO> ageDepartamentosDAOList =
                await _service.ActualizarVarios(_httpContextAccessor, ageDepartamentosSaveDAOList);

            return Ok(ageDepartamentosDAOList);
        }
    }
}
