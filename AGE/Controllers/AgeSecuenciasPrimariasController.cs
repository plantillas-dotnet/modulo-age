﻿
using Microsoft.AspNetCore.Mvc;
using AGE.Entities.DAO.AgeSecuenciasPrimarias;
using AGE.Services.AgeSecuenciasPrimariasService;
using System.ComponentModel.DataAnnotations;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;

namespace AGE.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class AgeSecuenciasPrimariasController : ControllerBase
    {
        private readonly IAgeSecuenciasPrimariasService _service;

        public AgeSecuenciasPrimariasController(IAgeSecuenciasPrimariasService Service)
        {
            _service = Service;
        }

        [HttpGet]
        [ProducesResponseType(typeof(Page<AgeSecuenciasPrimariaDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarTodos(
            [FromQuery] Pageable pageable)
        {
            Page<AgeSecuenciasPrimariaDAO> ageSecuenciasPrimariaDAOList = 
                await _service.ConsultarTodos(pageable);

            return Ok(ageSecuenciasPrimariaDAOList);
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(AgeSecuenciasPrimariaDAO), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarPorId(
            [FromRoute, Required] int id)
        {
            AgeSecuenciasPrimariaDAO ageSecuenciasPrimariaDAO = await _service.ConsultarPorId(id);
            return Ok(ageSecuenciasPrimariaDAO);
        }

        [HttpPost]
        [ProducesResponseType(typeof(Resource<AgeSecuenciasPrimariaDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> Insertar(
            [FromServices] IHttpContextAccessor httpContextAccessor,
            [FromBody] AgeSecuenciasPrimariasSaveDAO ageSecuenciasPrimariasSaveDAO)
        {
            Resource<AgeSecuenciasPrimariaDAO> ageSecuenciasPrimariaDAO = 
                await _service.Insertar(httpContextAccessor,ageSecuenciasPrimariasSaveDAO);

            return Ok(ageSecuenciasPrimariaDAO);
        }


        [HttpPost]
        [Route("varios")]
        [ProducesResponseType(typeof(List<Resource<AgeSecuenciasPrimariaDAO>>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> InsertarVarios(
            [FromServices] IHttpContextAccessor httpContextAccessor, 
            [FromBody] List<AgeSecuenciasPrimariasSaveDAO> ageSecuenciasPrimariasSaveDAO)
        {
            List<Resource<AgeSecuenciasPrimariaDAO>> ageSecuenciasPrimariaDAOList = 
                await _service.InsertarVarios(httpContextAccessor, ageSecuenciasPrimariasSaveDAO);

            return Ok(ageSecuenciasPrimariaDAOList);
        }


        [HttpPut]
        [ProducesResponseType(typeof(AgeSecuenciasPrimariaDAO), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> Actualizar(
            [FromServices] IHttpContextAccessor httpContextAccessor,
            [FromBody] AgeSecuenciasPrimariasSaveDAO ageSecuenciasPrimariasSaveDAO)
        {
            AgeSecuenciasPrimariaDAO ageSecuenciasPrimariaDAO = 
                await _service.Actualizar(httpContextAccessor,ageSecuenciasPrimariasSaveDAO);

            return Ok(ageSecuenciasPrimariaDAO);
        }


        [HttpPut]
        [Route("varios")]
        [ProducesResponseType(typeof(List<AgeSecuenciasPrimariaDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ActualizarVarios(
            [FromServices] IHttpContextAccessor httpContextAccessor,
            [FromBody] List<AgeSecuenciasPrimariasSaveDAO> ageSecuenciasPrimariasSaveDAO)
        {
            List<AgeSecuenciasPrimariaDAO> ageSecuenciasPrimariaDAOList = 
                await _service.ActualizarVarios(httpContextAccessor, ageSecuenciasPrimariasSaveDAO);

            return Ok(ageSecuenciasPrimariaDAOList);
        }
    }
}
