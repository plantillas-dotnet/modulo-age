﻿
using AGE.Entities.DAO.AgeUnidadesTiempos;
using AGE.Services.AgeUnidadesTiempoService;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace AGE.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class AgeUnidadesTiempoController : ControllerBase
    {
        private readonly IAgeUnidadesTiempoService _service;

        public AgeUnidadesTiempoController(IAgeUnidadesTiempoService service)
        {
            _service = service;
        }

        [HttpGet("{codigoLicenciatario}")]
        [ProducesResponseType(typeof(Page<AgeUnidadesTiempoDAO>),200)]
        public async Task<IActionResult> ConsultarTodos(
            [FromRoute] int codigoLicenciatario,
            [FromQuery] int? codigo,
            [FromQuery] string? descripcion,
            [FromQuery] Pageable pageable)
        {
            Page<AgeUnidadesTiempoDAO> ageUnidadesTiempoDAOList =
                await _service.ConsultarTodos(codigoLicenciatario, codigo ?? 0, descripcion ?? "", pageable);

            return Ok(ageUnidadesTiempoDAOList);
        }

        [HttpGet("{codigoLicenciatario}/{id}")]
        [ProducesResponseType(typeof(AgeUnidadesTiempoDAO), 200)]
        public async Task<IActionResult> ConsultarPorId(
            [FromRoute] AgeUnidadesTiempoPKDAO ageUnidades)
        {
            var ageUnidadesDAO = await _service.ConsultarPorId(ageUnidades);
            return Ok(ageUnidadesDAO);
        }

        [HttpGet("filtro/{codigoLicenciatario}")]
        [ProducesResponseType(typeof(Page<AgeUnidadesTiempoDAO>), 200)]
        public async Task<IActionResult> ConsultarListaFiltro(
           [FromRoute, Required] int codigoLicenciatario,
           [FromQuery, Required] string filtro,
           [FromQuery] Pageable pageable)
        {
            Page<AgeUnidadesTiempoDAO> ageUnidadesTiempoDAO =
                await _service.ConsultarListaFiltro(codigoLicenciatario, filtro, pageable);

            return Ok(ageUnidadesTiempoDAO);
        }

        [HttpPost]
        [ProducesResponseType(typeof(Resource<AgeUnidadesTiempoDAO>), 200)]
        public async Task<IActionResult> Insertar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeUnidadesTiempoSaveDAO ageUnidadesTiempoSave)
        {
            Resource<AgeUnidadesTiempoDAO> ageUnidadesTiempoDAO =
                await _service.Ingresar(ageUnidadesTiempoSave, _httpContextAccessor);

            return Ok(ageUnidadesTiempoDAO);
        }


        [HttpPost("varios")]
        [ProducesResponseType(typeof(List<Resource<AgeUnidadesTiempoDAO>>), 200)]
        public async Task<IActionResult> InsertarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeUnidadesTiempoSaveDAO> ageUnidadesTiempoSave)
        {
            List<Resource<AgeUnidadesTiempoDAO>> ageUnidadesTiempoDAOList =
                await _service.IngresarVarios(ageUnidadesTiempoSave, _httpContextAccessor);

            return Ok(ageUnidadesTiempoDAOList);
        }

        [HttpPut]
        [ProducesResponseType(typeof(AgeUnidadesTiempoDAO), 200)]
        public async Task<IActionResult> Actualizar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeUnidadesTiempoSaveDAO ageUnidadesTiempoSaveDAO)
        {
            AgeUnidadesTiempoDAO ageUnidadesTiempoDAO =
                await _service.Actualizar(ageUnidadesTiempoSaveDAO,_httpContextAccessor);

            return Ok(ageUnidadesTiempoDAO);
        }


        [HttpPut("varios")]
        [ProducesResponseType(typeof(List<AgeUnidadesTiempoDAO>), 200)]
        public async Task<IActionResult> ActualizarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeUnidadesTiempoSaveDAO> ageUnidadesTiempoSaveDAOList)
        {
            List<AgeUnidadesTiempoDAO> ageUnidadesTiempoDAOList =
                await _service.ActualizarVarios(ageUnidadesTiempoSaveDAOList, _httpContextAccessor);

            return Ok(ageUnidadesTiempoDAOList);
        }
    }
}
