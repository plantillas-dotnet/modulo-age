using AGE.Entities.DAO.AgeTiposIdentificaciones;
using AGE.Services.AgeTiposIdentificacioneService;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace AGE.Controllers
{
    [Route("[controller]")]
    [ApiController]
    //[Authorize]
    public class AgeTiposIdentificacionesController : ControllerBase
    {
        private readonly IAgeTiposIdentificacionesService _service;

        public AgeTiposIdentificacionesController(IAgeTiposIdentificacionesService Service)
        {
            _service = Service;
        }

        [HttpGet]
        [ProducesResponseType(typeof(Page<AgeTiposIdentificacioneDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarTodos(
            [FromQuery] int? codigo,
            [FromQuery] int? codigoInstitucionControl,
            [FromQuery] string? siglas,
            [FromQuery] string? descripcion,
            [FromQuery] Pageable pageable)
        {
            Page<AgeTiposIdentificacioneDAO> ageTiposIdentificacioneDAOList =
                await _service.ConsultarTodos(codigoInstitucionControl ?? 0, codigo ?? 0, siglas ?? "",
                                              descripcion ?? "", pageable);

            return Ok(ageTiposIdentificacioneDAOList);
        }


        [HttpGet("{id}")]
        [ProducesResponseType(typeof(AgeTiposIdentificacioneDAO), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarPorId(
            [FromRoute] int id)
        {
            AgeTiposIdentificacioneDAO ageTiposIdentificacioneDAO = await _service.ConsultarPorId(id);
            return Ok(ageTiposIdentificacioneDAO);
        }




        [HttpGet("filtro")]
        [ProducesResponseType(typeof(Page<AgeTiposIdentificacioneDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarListaFiltro(
            [FromQuery, Required] string filtro,
            [FromQuery] Pageable pageable)
        {
            Page<AgeTiposIdentificacioneDAO> ageTiposIdentificacioneDAOList =
                await _service.ConsultarListaFiltro(filtro, pageable);

            return Ok(ageTiposIdentificacioneDAOList);
        }


        [HttpPost]
        [ProducesResponseType(typeof(Resource<AgeTiposIdentificacioneDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> Insertar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeTiposIdentificacioneSaveDAO ageTiposIdentificacioneSaveDAO)
        {
            Resource<AgeTiposIdentificacioneDAO> ageTiposIdentificacioneDAO = await _service.Insertar(_httpContextAccessor, ageTiposIdentificacioneSaveDAO);
            return Ok(ageTiposIdentificacioneDAO);
        }


        [HttpPost("varios")]
        [ProducesResponseType(typeof(List<Resource<AgeTiposIdentificacioneDAO>>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> InsertarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeTiposIdentificacioneSaveDAO> ageTiposIdentificacioneSaveDAOList)
        {
            List<Resource<AgeTiposIdentificacioneDAO>> ageTiposIdentificacioneDAOList =
                await _service.InsertarVarios(_httpContextAccessor, ageTiposIdentificacioneSaveDAOList);

            return Ok(ageTiposIdentificacioneDAOList);
        }


        [HttpPut]
        [ProducesResponseType(typeof(AgeTiposIdentificacioneDAO), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> Actualizar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeTiposIdentificacioneSaveDAO ageTiposIdentificacioneSaveDAO)
        {
            AgeTiposIdentificacioneDAO ageTiposIdentificacioneDAO = await _service.Actualizar(_httpContextAccessor, ageTiposIdentificacioneSaveDAO);
            return Ok(ageTiposIdentificacioneDAO);
        }


        [HttpPut("varios")]
        [ProducesResponseType(typeof(List<AgeTiposIdentificacioneDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ActualizarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeTiposIdentificacioneSaveDAO> ageTiposIdentificacioneSaveDAOList)
        {
            List<AgeTiposIdentificacioneDAO> ageTiposIdentificacioneDAOList =
                await _service.ActualizarVarios(_httpContextAccessor, ageTiposIdentificacioneSaveDAOList);

            return Ok(ageTiposIdentificacioneDAOList);
        }
    }
}
