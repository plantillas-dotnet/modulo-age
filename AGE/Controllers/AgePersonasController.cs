using AGE.Entities;
using AGE.Entities.DAO.AgePersonas;
using AGE.Entities.DAO.AgeRutas;
using AGE.Services.AgePersonasService;
using AGE.Services.AgeRutasService;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace AGE.Controllers
{
    [Route("[controller]")]
    [ApiController]
    //[Authorize]
    public class AgePersonasController : ControllerBase
    {
        private readonly IAgePersonasService _service;

        public AgePersonasController(IAgePersonasService Service)
        {
            _service = Service;
        }

        [HttpPut]
        [ProducesResponseType(typeof(AgePersonasDAO), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> Actualizar(
        [FromServices] IHttpContextAccessor _httpContextAccessor,
        [FromBody] AgePersonasSaveDAO agePersonasSaveDAO)
        {
            AgePersonasDAO agePersonasDAO = await _service.Actualizar(_httpContextAccessor, agePersonasSaveDAO);
            return Ok(agePersonasDAO);
        }

        [HttpPost]
        [ProducesResponseType(typeof(Resource<AgePersonasDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> Insertar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgePersonasSaveDAO agePersonasSaveDAO)
        {
            Resource<AgePersonasDAO> agePersonasDAO = await _service.Insertar(_httpContextAccessor, agePersonasSaveDAO);
            return Ok(agePersonasDAO);
        }

        [HttpPut("varios")]
        [ProducesResponseType(typeof(List<AgePersonasDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ActualizarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgePersonasSaveDAO> agePersonasSaveDAOList)
        {
            List<AgePersonasDAO> agePersonasDAOList =
                await _service.ActualizarVarios(_httpContextAccessor, agePersonasSaveDAOList);

            return Ok(agePersonasDAOList);
        }

        [HttpPost("varios")]
        [ProducesResponseType(typeof(List<Resource<AgePersonasDAO>>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> InsertarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgePersonasSaveDAO> agePersonasSaveDAOList)
        {
            List<Resource<AgePersonasDAO>> agePersonasDAOList =
                await _service.InsertarVarios(_httpContextAccessor, agePersonasSaveDAOList);

            return Ok(agePersonasDAOList);
        }

        [HttpGet("{codigoLicenciatario}")]
        [ProducesResponseType(typeof(Page<AgePersonasDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarTodos(
            [FromRoute] int codigoLicenciatario,
            [FromQuery] int? codigo,
            [FromQuery] string? apellidos,
            [FromQuery] string? nombres,
            [FromQuery] string? numeroIdentificacion,
            [FromQuery] Pageable pageable)
        {
            Page<AgePersonasDAO> agePersonasDAOList =
                await _service.ConsultarTodos(codigoLicenciatario,codigo ?? 0,
             apellidos ?? "",nombres ?? "", numeroIdentificacion ?? "",  pageable);

            return Ok(agePersonasDAOList);
        }

        [HttpGet("{codigoLicenciatario}/{id}")]
        [ProducesResponseType(typeof(AgePersonasDAO), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarPorId(
            [FromRoute] AgePersonasPKDAO agePersonasPKDAO)
        {
            AgePersonasDAO agePersonasDAO = await _service.ConsultarPorId(agePersonasPKDAO);
            return Ok(agePersonasDAO);
        }


        [HttpGet("filtro/{codigoLicenciatario}")]
        [ProducesResponseType(typeof(Page<AgePersonasDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarListaFiltro(
            [FromRoute] int codigoLicenciatario,
            [FromQuery, Required] string filtro,
            [FromQuery] Pageable pageable)
        {
            Page<AgePersonasDAO> agePersonasDAOList =
                await _service.ConsultarListaFiltro(codigoLicenciatario, filtro, pageable);

            return Ok(agePersonasDAOList);
        }

        [HttpGet("filtrar/{codigoLicenciatario}/{identificacion}")]
        [ProducesResponseType(typeof(AgePersonasDAO), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarLicencYNumeroIdent(
            [FromRoute] int codigoLicenciatario,
            [FromRoute] string identificacion)
        {
            AgePersonasDAO agePersonasDAO =
                await _service.ConsultarLicencYNumeroIdent(codigoLicenciatario, identificacion);

            return Ok(agePersonasDAO);
        }

    }
}
