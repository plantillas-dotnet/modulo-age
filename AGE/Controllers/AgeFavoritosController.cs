﻿using AGE.Entities.DAO.AgeFavoritos;
using AGE.Services.AgeFavoritosService;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace AGE.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class AgeFavoritosController : ControllerBase
    {
        private readonly IAgeFavoritosService _service;

        public AgeFavoritosController(IAgeFavoritosService Service)
        {
            _service = Service;
        }

        [HttpGet("{codigoLicenciatario}/{codigoUsuario}")]
        [ProducesResponseType(typeof(Page<AgeFavoritosDAO>), 200)]
        public async Task<IActionResult> ConsultarTodos([FromRoute] int codigoLicenciatario,
                                                        [FromRoute] int codigoUsuario,
                                                        [FromQuery] Pageable pageable)
        {
            Page<AgeFavoritosDAO> ageFavoritosDAOList =
                await _service.ConsultarTodos(codigoLicenciatario, codigoUsuario,pageable);

            return Ok(ageFavoritosDAOList);
        }

        [HttpGet("{codigoLicenciatario}/{codigoUsuario}/{codigoAplicacion}/{codigoTransaccion}/{id}")]
        [ProducesResponseType(typeof(AgeFavoritosDAO), 200)]
        public async Task<IActionResult> ConsultarPorId(
            [FromRoute] AgeFavoritosPKDAO ageFavoritosPKDAO)
        {
            var ageFavoritosDAO = await _service.ConsultarPorId(ageFavoritosPKDAO);
            return Ok(ageFavoritosDAO);
        }

        [HttpGet("filtro/{codigoLicenciatario}/{codigoUsuario}")]
        [ProducesResponseType(typeof(Page<AgeFavoritosDAO>), 200)]
        public async Task<IActionResult> ConsultarListaFiltro(
           [FromRoute] int codigoLicenciatario,
           [FromRoute] int codigoUsuario,
           [FromQuery, Required] string filtro,
           [FromQuery] Pageable pageable)
        {
            Page<AgeFavoritosDAO> ageFavoritosDAO =
                await _service.ConsultarListaFiltro(filtro,codigoLicenciatario, codigoUsuario, pageable);

            return Ok(ageFavoritosDAO);
        }

        [HttpPost]
        [ProducesResponseType(typeof(Resource<AgeFavoritosDAO>), 200)]
        public async Task<IActionResult> Insertar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeFavoritosSaveDAO ageFavoritosSaveDAO)
        {
            Resource<AgeFavoritosDAO> ageFavoritosDAO =
                await _service.Insertar(_httpContextAccessor, ageFavoritosSaveDAO);

            return Ok(ageFavoritosDAO);
        }

        [HttpPost("varios")]
        [ProducesResponseType(typeof(List<Resource<AgeFavoritosDAO>>), 200)]
        public async Task<IActionResult> InsertarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeFavoritosSaveDAO> ageFavoritosSaveDAOList)
        {
            List<Resource<AgeFavoritosDAO>> ageFavoritosDAOList =
                await _service.InsertarVarios(_httpContextAccessor, ageFavoritosSaveDAOList);

            return Ok(ageFavoritosDAOList);
        }

        [HttpPut]
        [ProducesResponseType(typeof(AgeFavoritosDAO), 200)]
        public async Task<IActionResult> Actualizar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeFavoritosSaveDAO ageFavoritosSaveDAO)
        {
            AgeFavoritosDAO ageFavoritosDAO = await _service.Actualizar(_httpContextAccessor, ageFavoritosSaveDAO);
            return Ok(ageFavoritosDAO);
        }

        [HttpPut("varios")]
        [ProducesResponseType(typeof(List<AgeFavoritosDAO>), 200)]
        public async Task<IActionResult> ActualizarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeFavoritosSaveDAO> ageFavoritosSaveDAOList)
        {
            List<AgeFavoritosDAO> ageFavoritosDAOList =
                await _service.ActualizarVarios(_httpContextAccessor, ageFavoritosSaveDAOList);

            return Ok(ageFavoritosDAOList);
        }
    }
}
