﻿using AGE.Entities.DAO.AgeUsuarioParamVigencia;
using AGE.Services.AgeUsuarioParamVigenciaService;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace AGE.Controllers
{
    [Route("[controller]")]
    [ApiController]
    //[Authorize]
    public class AgeUsuarioParamVigenciasController : ControllerBase
    {
        private readonly IAgeUsuarioParamVigenciasService _service;

        public AgeUsuarioParamVigenciasController(IAgeUsuarioParamVigenciasService Service)
        {
            _service = Service;
        }

        [HttpPut]
        [ProducesResponseType(typeof(AgeUsuarioParamVigenciaDAO), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> Actualizar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeUsuarioParamVigenciaSaveDAO ageUsuarioParamVigenciaSaveDAO)
        {
            AgeUsuarioParamVigenciaDAO ageUsuarioParamVigenciaDAO = await _service.Actualizar(_httpContextAccessor, ageUsuarioParamVigenciaSaveDAO);
            return Ok(ageUsuarioParamVigenciaDAO);
        }

        [HttpPost]
        [ProducesResponseType(typeof(Resource<AgeUsuarioParamVigenciaDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> Insertar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeUsuarioParamVigenciaSaveDAO ageUsuarioParamVigenciaSaveDAO)
        {
            Resource<AgeUsuarioParamVigenciaDAO> ageUsuarioParamVigenciaDAO = await _service.Insertar(_httpContextAccessor, ageUsuarioParamVigenciaSaveDAO);
            return Ok(ageUsuarioParamVigenciaDAO);
        }

        [HttpPut("varios")]
        [ProducesResponseType(typeof(List<AgeUsuarioParamVigenciaDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ActualizarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeUsuarioParamVigenciaSaveDAO> ageUsuarioParamVigenciaSaveDAOList)
        {
            List<AgeUsuarioParamVigenciaDAO> ageUsuarioParamVigenciaDAOList =
                await _service.ActualizarVarios(_httpContextAccessor, ageUsuarioParamVigenciaSaveDAOList);

            return Ok(ageUsuarioParamVigenciaDAOList);
        }

        [HttpPost("varios")]
        [ProducesResponseType(typeof(List<Resource<AgeUsuarioParamVigenciaDAO>>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> InsertarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeUsuarioParamVigenciaSaveDAO> ageUsuarioParamVigenciaSaveDAOList)
        {
            List<Resource<AgeUsuarioParamVigenciaDAO>> ageUsuarioParamVigenciaDAOList =
                await _service.InsertarVarios(_httpContextAccessor, ageUsuarioParamVigenciaSaveDAOList);

            return Ok(ageUsuarioParamVigenciaDAOList);
        }

        [HttpGet("{codigoLicenciatario}")]
        [ProducesResponseType(typeof(Page<AgeUsuarioParamVigenciaDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarTodos(
            [FromRoute] int codigoLicenciatario,
            [FromQuery] Pageable pageable)
        {
            Page<AgeUsuarioParamVigenciaDAO> ageUsuarioParamVigenciaDAOList =
                await _service.ConsultarTodos(codigoLicenciatario, pageable);

            return Ok(ageUsuarioParamVigenciaDAOList);
        }


        [HttpGet("{codigoLicenciatario}/{codigoUsuario}/{codigoPG}/{id}")]
        [ProducesResponseType(typeof(AgeUsuarioParamVigenciaDAO), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarPorId(
            [FromRoute] int codigoLicenciatario,
            [FromRoute] long id,
            [FromRoute] long codigoUsuario,
            [FromRoute] int codigoPG,
            [FromQuery] Pageable pageable)
        {
            AgeUsuarioParamVigenciaPKDAO ageUsuarioParamVigenciaPKDAO = new AgeUsuarioParamVigenciaPKDAO
            {
                AgeUsuariAgeLicencCodigo = codigoLicenciatario,
                AgeUsuariCodigo = codigoUsuario,
                AgeParGeCodigo = codigoPG,
                Codigo = id
            };

            AgeUsuarioParamVigenciaDAO ageUsuarioParamVigenciaDAO = await _service.ConsultarPorId(ageUsuarioParamVigenciaPKDAO);
            return Ok(ageUsuarioParamVigenciaDAO);
        }



        [HttpGet("filtro/{codigoLicenciatario}")]
        [ProducesResponseType(typeof(Page<AgeUsuarioParamVigenciaDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarListaFiltro(
            [FromRoute] int codigoLicenciatario,
            [FromQuery, Required] string filtro,
            [FromQuery] Pageable pageable)
        {
            Page<AgeUsuarioParamVigenciaDAO> ageUsuarioParamVigenciaDAOList =
                await _service.ConsultarListaFiltro(codigoLicenciatario, filtro, pageable);

            return Ok(ageUsuarioParamVigenciaDAOList);
        }



    }
}
