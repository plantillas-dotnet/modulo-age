using AGE.Entities.DAO.AgeTiposSucursales;
using AGE.Services.AgeTiposSucursaleService;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace AGE.Controllers
{
    [Route("[controller]")]
    [ApiController]
    //[Authorize]
    public class AgeTiposSucursalesController : ControllerBase
    {
        private readonly IAgeTiposSucursalesService _service;

        public AgeTiposSucursalesController(IAgeTiposSucursalesService Service)
        {
            _service = Service;
        }

        [HttpGet("{codigoLicenciatario}")]
        [ProducesResponseType(typeof(Page<AgeTiposSucursaleDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarTodos(
            [FromRoute] int codigoLicenciatario,
            [FromQuery] int? codigo,
            [FromQuery] string? descripcion,
            [FromQuery] Pageable pageable)
        {
            Page<AgeTiposSucursaleDAO> ageTiposSucursaleDAOList =
                await _service.ConsultarTodos(codigoLicenciatario, codigo ?? 0,
                                              descripcion ?? "", pageable);

            return Ok(ageTiposSucursaleDAOList);
        }

        [HttpGet("{codigoLicenciatario}/{id}")]
        [ProducesResponseType(typeof(AgeTiposSucursaleDAO), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarPorId(
        [FromRoute] AgeTiposSucursalePKDAO ageTiposSucursalePKDAO)
        {
            AgeTiposSucursaleDAO ageTiposSucursaleDAO = await _service.ConsultarPorId(ageTiposSucursalePKDAO);
            return Ok(ageTiposSucursaleDAO);
        }

        [HttpGet("filtro/{codigoLicenciatario}")]
        [ProducesResponseType(typeof(Page<AgeTiposSucursaleDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarListaFiltro(
            [FromRoute] int codigoLicenciatario,
            [FromQuery, Required] string filtro,
            [FromQuery] Pageable pageable)
        {
            Page<AgeTiposSucursaleDAO> ageTiposSucursaleDAOList =
                await _service.ConsultarListaFiltro(codigoLicenciatario, filtro, pageable);

            return Ok(ageTiposSucursaleDAOList);
        }


        [HttpPost]
        [ProducesResponseType(typeof(Resource<AgeTiposSucursaleDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> Insertar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeTiposSucursaleSaveDAO ageTiposSucursaleSaveDAO)
        {
            Resource<AgeTiposSucursaleDAO> ageTiposSucursaleDAO = await _service.Insertar(_httpContextAccessor, ageTiposSucursaleSaveDAO);
            return Ok(ageTiposSucursaleDAO);
        }


        [HttpPost("varios")]
        [ProducesResponseType(typeof(List<Resource<AgeTiposSucursaleDAO>>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> InsertarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeTiposSucursaleSaveDAO>   ageTiposSucursaleSaveDAOList)
        {
            List<Resource<AgeTiposSucursaleDAO>> ageTiposSucursaleDAOList =
                await _service.InsertarVarios(_httpContextAccessor, ageTiposSucursaleSaveDAOList);

            return Ok(ageTiposSucursaleDAOList);
        }


        [HttpPut]
        [ProducesResponseType(typeof(AgeTiposSucursaleDAO), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> Actualizar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeTiposSucursaleSaveDAO ageTiposSucursaleSaveDAO)
        {
            AgeTiposSucursaleDAO ageTiposSucursaleDAO = await _service.Actualizar(_httpContextAccessor, ageTiposSucursaleSaveDAO);
            return Ok(ageTiposSucursaleDAO);
        }


        [HttpPut("varios")]
        [ProducesResponseType(typeof(List<AgeTiposSucursaleDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ActualizarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeTiposSucursaleSaveDAO> ageTiposSucursaleSaveDAOList)
        {
            List<AgeTiposSucursaleDAO> ageTiposSucursaleDAOList =
                await _service.ActualizarVarios(_httpContextAccessor, ageTiposSucursaleSaveDAOList);

            return Ok(ageTiposSucursaleDAOList);
        }

    }

}
