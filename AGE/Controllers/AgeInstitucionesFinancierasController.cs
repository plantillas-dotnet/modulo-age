using AGE.Entities.DAO.AgeInstitucionesFinancieras;
using AGE.Entities.DAO.AgeLicenciatarioAplicacion;
using AGE.Services.AgeInstitucionesFinancierasService;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace AGE.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class AgeInstitucionesFinancierasController : ControllerBase
    {

        private readonly IAgeInstitucionesFinancieraService _service;

        public AgeInstitucionesFinancierasController(IAgeInstitucionesFinancieraService Service)
        {
            _service = Service;
        }

        [HttpGet("{codigoLicenciatario}")]
        [ProducesResponseType(typeof(Page<AgeInstitucionesFinancieraDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarTodos(
            [FromRoute] int codigoLicenciatario,
            [FromQuery] string codigoSegunBanco,
            [FromQuery] string descripcion,
            [FromQuery] string siglasBanco,
            [FromQuery] Pageable pageable)
        {
            Page<AgeInstitucionesFinancieraDAO> ageInstitucionesFinancieraDAOList =
                await _service.ConsultarTodos(codigoLicenciatario, codigoSegunBanco ?? "", descripcion ?? "", siglasBanco ?? "", pageable);

            return Ok(ageInstitucionesFinancieraDAOList);
        }


        [HttpGet("{codigoLicenciatario}/{id}")]
        [ProducesResponseType(typeof(AgeInstitucionesFinancieraDAO), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarPorId(
            [FromRoute] AgeInstitucionesFinancieraPKDAO ageInstitucionesFinancieraPKDAO)
        {
            AgeInstitucionesFinancieraDAO ageInstitucionesFinancieraDAO =
                await _service.ConsultarPorId(ageInstitucionesFinancieraPKDAO);

            return Ok(ageInstitucionesFinancieraDAO);
        }



        [HttpGet("filtro/{codigoLicenciatario}")]
        [ProducesResponseType(typeof(Page<AgeInstitucionesFinancieraDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarListaFiltro(
            [FromRoute] int codigoLicenciatario,
            [FromQuery, Required] string filtro,
            [FromQuery] Pageable pageable)
        {
            Page<AgeInstitucionesFinancieraDAO> ageInstitucionesFinancieraDAOList =
                await _service.ConsultarListaFiltro(codigoLicenciatario, filtro, pageable);

            return Ok(ageInstitucionesFinancieraDAOList);
        }


        [HttpPost]
        [ProducesResponseType(typeof(Resource<AgeInstitucionesFinancieraDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> Insertar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeInstitucionesFinancieraSaveDAO ageInstitucionesFinancieraSaveDAO)
        {
            Resource<AgeInstitucionesFinancieraDAO> ageInstitucionesFinancieraDAO =
                await _service.Insertar(_httpContextAccessor, ageInstitucionesFinancieraSaveDAO);

            return Ok(ageInstitucionesFinancieraDAO);
        }


        [HttpPost("varios")]
        [ProducesResponseType(typeof(List<Resource<AgeInstitucionesFinancieraDAO>>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> InsertarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeInstitucionesFinancieraSaveDAO> ageInstitucionesFinancieraSaveDAOList)
        {
            List<Resource<AgeInstitucionesFinancieraDAO>> ageInstitucionesFinancieraDAOList =
                await _service.InsertarVarios(_httpContextAccessor, ageInstitucionesFinancieraSaveDAOList);

            return Ok(ageInstitucionesFinancieraDAOList);
        }


        [HttpPut]
        [ProducesResponseType(typeof(AgeInstitucionesFinancieraDAO), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> Actualizar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeInstitucionesFinancieraSaveDAO ageInstitucionesFinancieraSaveDAO)
        {
            AgeInstitucionesFinancieraDAO ageInstitucionesFinancieraDAO =
                await _service.Actualizar(_httpContextAccessor, ageInstitucionesFinancieraSaveDAO);

            return Ok(ageInstitucionesFinancieraDAO);
        }


        [HttpPut("varios")]
        [ProducesResponseType(typeof(List<AgeInstitucionesFinancieraDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ActualizarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeInstitucionesFinancieraSaveDAO> ageInstitucionesFinancieraSaveDAOList)
        {
            List<AgeInstitucionesFinancieraDAO> ageInstitucionesFinancieraDAOList =
                await _service.ActualizarVarios(_httpContextAccessor, ageInstitucionesFinancieraSaveDAOList);

            return Ok(ageInstitucionesFinancieraDAOList);
        }


    }
}
