﻿using AGE.Entities.DAO.AgeSucursales;
using AGE.Services.AgeSucursalesService;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace AGE.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class AgeSucursalesController : ControllerBase
    {
        private readonly IAgeSucursalesService _service;

        public AgeSucursalesController(IAgeSucursalesService Service)
        {
            _service = Service;
        }

        [HttpGet("{codigoLicenciatario}")]
        [ProducesResponseType(typeof(Page<AgeSucursalesDAO>), 200)]
        public async Task<IActionResult> ConsultarTodos([FromRoute] int codigoLicenciatario,
                                                        [FromQuery] int codigo,
                                                        [FromQuery] string descripcion,
                                                        [FromQuery] string direccion,
                                                        [FromQuery] Pageable pageable)
        {
            Page<AgeSucursalesDAO> ageSucursalesDAOList =
                await _service.ConsultarTodos(codigoLicenciatario,codigo, descripcion, direccion, pageable);

            return Ok(ageSucursalesDAOList);
        }

        [HttpGet("{codigoLicenciatario}/{id}")]
        [ProducesResponseType(typeof(AgeSucursalesDAO), 200)]
        public async Task<IActionResult> ConsultarPorId(
            [FromRoute] AgeSucursalesPKDAO ageSucursalesPKDAO)
        {
            var ageSucursalesDAO = await _service.ConsultarPorId(ageSucursalesPKDAO);
            return Ok(ageSucursalesDAO);
        }

        [HttpGet("filtro/{codigoLicenciatario}")]
        [ProducesResponseType(typeof(Page<AgeSucursalesDAO>), 200)]
        public async Task<IActionResult> ConsultarListaFiltro(
           [FromRoute, Required] int codigoLicenciatario,
           [FromQuery, Required] string filtro,
           [FromQuery] Pageable pageable)
        {
            Page<AgeSucursalesDAO> ageSucursalesDAO =
                await _service.ConsultarListaFiltro(codigoLicenciatario, filtro, pageable);

            return Ok(ageSucursalesDAO);
        }

        [HttpPost]
        [ProducesResponseType(typeof(Resource<AgeSucursalesDAO>), 200)]
        public async Task<IActionResult> Insertar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeSucursalesSaveDAO ageSucursalesSaveDAO)
        {
            Resource<AgeSucursalesDAO> ageSucursalesDAO =
                await _service.Insertar(_httpContextAccessor, ageSucursalesSaveDAO);

            return Ok(ageSucursalesDAO);
        }

        [HttpPost("varios")]
        [ProducesResponseType(typeof(List<Resource<AgeSucursalesDAO>>), 200)]
        public async Task<IActionResult> InsertarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeSucursalesSaveDAO> ageSucursalesSaveDAOList)
        {
            List<Resource<AgeSucursalesDAO>> ageSucursalesDAOList =
                await _service.InsertarVarios(_httpContextAccessor, ageSucursalesSaveDAOList);

            return Ok(ageSucursalesDAOList);
        }

        [HttpPut]
        [ProducesResponseType(typeof(AgeSucursalesDAO), 200)]
        public async Task<IActionResult> Actualizar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeSucursalesSaveDAO ageSucursalesSaveDAO)
        {
            AgeSucursalesDAO ageSucursalesDAO = await _service.Actualizar(_httpContextAccessor, ageSucursalesSaveDAO);
            return Ok(ageSucursalesDAO);
        }

        [HttpPut("varios")]
        [ProducesResponseType(typeof(List<AgeSucursalesDAO>), 200)]
        public async Task<IActionResult> ActualizarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeSucursalesSaveDAO> ageSucursalesSaveDAOList)
        {
            List<AgeSucursalesDAO> ageSucursalesDAOList =
                await _service.ActualizarVarios(_httpContextAccessor, ageSucursalesSaveDAOList);

            return Ok(ageSucursalesDAOList);
        }
    }
}
