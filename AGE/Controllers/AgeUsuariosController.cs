
using AGE.Entities;
using AGE.Entities.DAO.AgeUsuarios;
using AGE.Services.AgeUsuarioService;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace AGE.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class AgeUsuariosController : ControllerBase
    {
        private readonly IAgeUsuariosService _service;

        public AgeUsuariosController(IAgeUsuariosService Service)
        {
            _service = Service;
        }

        [HttpGet("{codigoLicenciatario}")]
        [ProducesResponseType(typeof(Page<AgeUsuarioDAO>), 200)]
        public async Task<IActionResult> ConsultarTodos(
            [FromRoute] int codigoLicenciatario,
            [FromQuery] string? codigoExterno,
            [FromQuery] Pageable pageable)
        {
            Page<AgeUsuarioDAO> ageUsuarioDAOList = 
                await _service.ConsultarTodos(codigoLicenciatario, codigoExterno ?? "", pageable);

            return Ok(ageUsuarioDAOList);
        }

        [HttpGet("{codigoLicenciatario}/{id}")]
        [ProducesResponseType(typeof(AgeUsuarioDAO), 200)]
        public async Task<IActionResult> ConsultarPorId(
            [FromRoute] AgeUsuarioPKDAO ageUsuarioPKDAO)
        {
            AgeUsuarioDAO ageUsuarioDAO = await _service.ConsultarPorId(ageUsuarioPKDAO);
            return Ok(ageUsuarioDAO);
        }

        [HttpGet("filtro/{codigoLicenciatario}")]
        [ProducesResponseType(typeof(Page<AgeUsuarioDAO>), 200)]
        public async Task<IActionResult> ConsultarListaFiltro(
            [FromRoute] int codigoLicenciatario,
            [FromQuery, Required] string filtro,
            [FromQuery] Pageable pageable)
        {
            Page<AgeUsuarioDAO> ageUsuarioDAOList = 
                await _service.ConsultarListaFiltro(codigoLicenciatario, filtro, pageable);

            return Ok(ageUsuarioDAOList);
        }

        [HttpPost("registrarse")]
        [ProducesResponseType(typeof(Resource<AgeUsuarioDAO>), 200)]
        public async Task<IActionResult> Insertar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeUsuarioSaveDAO ageUsuarioSaveDAO)
        {
            Resource<AgeUsuarioDAO> ageUsuarioDAO = 
                await _service.Insertar(_httpContextAccessor, ageUsuarioSaveDAO);

            return Ok(ageUsuarioDAO);
        }

        [HttpPost("varios")]
        [ProducesResponseType(typeof(List<Resource<AgeUsuarioDAO>>), 200)]
        public async Task<IActionResult> InsertarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeUsuarioSaveDAO> ageUsuarioSaveDAOList)
        {
            List<Resource<AgeUsuarioDAO>> ageUsuarioDAOList = 
                await _service.InsertarVarios(_httpContextAccessor, ageUsuarioSaveDAOList);

            return Ok(ageUsuarioDAOList);
        }

        [HttpPut]
        [ProducesResponseType(typeof(AgeUsuarioDAO), 200)]
        public async Task<IActionResult> Actualizar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeUsuarioSaveDAO ageUsuarioSaveDAO)
        {
            AgeUsuarioDAO ageUsuarioDAO = await _service.Actualizar(_httpContextAccessor, ageUsuarioSaveDAO);
            return Ok(ageUsuarioDAO);
        }

        [HttpPut("varios")]
        [ProducesResponseType(typeof(List<AgeUsuarioDAO>), 200)]
        public async Task<IActionResult> ActualizarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeUsuarioSaveDAO> ageUsuarioSaveDAOList)
        {
            List<AgeUsuarioDAO> ageUsuarioDAOList = 
                await _service.ActualizarVarios(_httpContextAccessor, ageUsuarioSaveDAOList);

            return Ok(ageUsuarioDAOList);
        }

        [HttpPost("user")]
        [ProducesResponseType(typeof(Resource<AgeUsuariosLoginDAO>), 200)]
        public async Task<IActionResult> ActualizarDatosUsuario(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeUsuariosLoginDAO ageUsuariosLoginDAO)
        {
            AgeUsuariosLoginDAO ageUsuarios = await _service.ActualizarDatosUsuario(_httpContextAccessor, ageUsuariosLoginDAO);

            return Ok(ageUsuarios);
        }

    }
}
