using AGE.Entities.DAO.AgePerfiles;
using AGE.Entities.DAO.AgePerfilesTransacciones;
using AGE.Services.AgePerfilesTransaccioneService;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Threading.Tasks;
using AGE.Entities.Composite.AgePerfilesTransacciones;

namespace AGE.Controllers
{
    [Route("[controller]")]
    [ApiController]
    //[Authorize]
    public class AgePerfilesTransaccionesController : ControllerBase
    {
        private readonly IAgePerfilesTransaccionesService _service;

        public AgePerfilesTransaccionesController(IAgePerfilesTransaccionesService service)
        {
            _service = service;
        }

        [HttpGet("{codigoLicenciatario}")]
        [ProducesResponseType(typeof(Page<AgePerfilesTransaccioneDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarTodos(
            [FromRoute, Required] int codigoLicenciatario,
            [FromQuery] int? codigoPerfil,
            [FromQuery] int? codigo,
            [FromQuery] int? codigoTransaccion,
            [FromQuery] int? codigoAplicacion,
            [FromQuery] Pageable pageable)
        {
            Page<AgePerfilesTransaccioneDAO> agePerfilesTransaccioneDAOList =
                await _service.ConsultarTodos(codigoLicenciatario, codigo ?? 0, codigoPerfil ?? 0, codigoTransaccion ?? 0, codigoAplicacion ?? 0, pageable);

            return Ok(agePerfilesTransaccioneDAOList);
        }


        [HttpGet("todos/{codigoLicenciatario}")]
        [ProducesResponseType(typeof(Page<AgePerfilesTransaccioneDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ObtenerTodos(
            [FromRoute, Required] int codigoLicenciatario,
            [FromQuery] int? codigo,
            [FromQuery] int? agePerfilCodigo,
            [FromQuery] int? ageTransaAgeAplicaCodigo,
            [FromQuery] int? ageTransaCodigo,
            [FromQuery] Pageable pageable)
        {
            Page<AgePerfilesTransaccioneDAO> agePerfilesTransaccioneDAOList =
                await _service.ObtenerTodos(codigoLicenciatario, codigo ?? 0, agePerfilCodigo ?? 0, ageTransaAgeAplicaCodigo ?? 0, ageTransaCodigo ?? 0, pageable);

            return Ok(agePerfilesTransaccioneDAOList);
        }


        [HttpGet("consulta/{codigoLicenciatario}/{codigoPerfil}")]
        [ProducesResponseType(typeof(AgePerfilTransaccionDAO), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarPorLicenciatarioPerfil(
             [FromRoute, Required] int codigoLicenciatario,
             [FromRoute, Required] int codigoPerfil,
             [FromQuery] int? codigoAplicacion)
        {
            AgePerfilTransaccionDAO agePerfilesTransaccioneDAOList =
                await _service.ConsultarPorLicenciatarioPerfil(codigoLicenciatario, codigoPerfil, codigoAplicacion ?? 0);

            return Ok(agePerfilesTransaccioneDAOList);
        }

        [HttpGet("{codigoLicenciatario}/{codigoPerfil}/{id}")]
        [ProducesResponseType(typeof(AgePerfilesDAO), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarPorId(
            [FromRoute] AgePerfilesTransaccionePKDAO agePerfilesTransaccionePKDAO)
        {
            AgePerfilesTransaccioneDAO agePerfilesTransaccioneDAO = await _service.ConsultarPorId(agePerfilesTransaccionePKDAO);
            return Ok(agePerfilesTransaccioneDAO);
        }

        [HttpGet("filtro/{codigoLicenciatario}")]
        [ProducesResponseType(typeof(Page<AgePerfilesTransaccioneDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarListaFiltro(
            [FromRoute] int codigoLicenciatario,
            [FromQuery, Required] string filtro,
            [FromQuery] Pageable pageable)
        {
            Page<AgePerfilesTransaccioneDAO> agePerfilesTransaccioneDAOList =
                await _service.ConsultarListaFiltro(codigoLicenciatario, filtro, pageable);

            return Ok(agePerfilesTransaccioneDAOList);
        }

        [HttpPost]
        [ProducesResponseType(typeof(Resource<AgePerfilesTransaccioneDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> Insertar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgePerfilesTransaccioneSaveDAO agePerfilesTransaccioneSaveDAO)
        {
            Resource<AgePerfilesTransaccioneDAO> agePerfilesTransaccioneDAO =
                await _service.Insertar(_httpContextAccessor, agePerfilesTransaccioneSaveDAO);

            return Ok(agePerfilesTransaccioneDAO);
        }


        [HttpPost("composite")]
        [ProducesResponseType(typeof(AgePerfilesTransaccionesComposite), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> InsertarComposite(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgePerfilesTransaccionesCompositeSave agePerfilesTransaccioneCompositeSave)
        {
            AgePerfilesTransaccionesComposite agePerfilesTransaccionesComposite =
                await _service.InsertarComposite(_httpContextAccessor, agePerfilesTransaccioneCompositeSave);

            return Ok(agePerfilesTransaccionesComposite);
        }


        [HttpPost("varios")]
        [ProducesResponseType(typeof(List<Resource<AgePerfilesTransaccioneDAO>>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> InsertarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgePerfilesTransaccioneSaveDAO> agePerfilesTransaccioneSaveDAOList)
        {
            List<Resource<AgePerfilesTransaccioneDAO>> agePerfilesTransaccioneDAOList =
                await _service.InsertarVarios(_httpContextAccessor, agePerfilesTransaccioneSaveDAOList);

            return Ok(agePerfilesTransaccioneDAOList);
        }

        [HttpPut]
        [ProducesResponseType(typeof(AgePerfilesTransaccioneDAO), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> Actualizar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgePerfilesTransaccioneSaveDAO agePerfilesTransaccioneSaveDAO)
        {
            AgePerfilesTransaccioneDAO agePerfilesTransaccioneDAO =
                await _service.Actualizar(_httpContextAccessor, agePerfilesTransaccioneSaveDAO);

            return Ok(agePerfilesTransaccioneDAO);
        }

        [HttpPut("varios")]
        [ProducesResponseType(typeof(List<AgePerfilesTransaccioneDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ActualizarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgePerfilesTransaccioneSaveDAO> agePerfilesTransaccioneSaveDAOList)
        {
            List<AgePerfilesTransaccioneDAO> agePerfilesTransaccioneDAOList =
                await _service.ActualizarVarios(_httpContextAccessor, agePerfilesTransaccioneSaveDAOList);

            return Ok(agePerfilesTransaccioneDAOList);
        }
    }
}
