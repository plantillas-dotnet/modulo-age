﻿using AGE.Entities.DAO.AgeLicenciatarios;
using AGE.Services.AgeLicenciatariosService;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace AGE.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class AgeLicenciatariosController : Controller
    {
        private readonly IAgeLicenciatariosService _service;

        public AgeLicenciatariosController(IAgeLicenciatariosService Service)
        {
            _service = Service;
        }

        [HttpGet]
        [ProducesResponseType(typeof(Page<AgeLicenciatarioDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarTodos(
            [FromQuery] int? codigo,
            [FromQuery] string? descripcion,
            [FromQuery] Pageable pageable)
        {
            Page<AgeLicenciatarioDAO> ageLicenciatariosDAOList =
                await _service.ConsultarTodos(codigo ?? 0, descripcion ?? "", pageable);

            return Ok(ageLicenciatariosDAOList);
        }

        [HttpPut]
        [ProducesResponseType(typeof(AgeLicenciatarioDAO), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> Actualizar(
            [FromServices] IHttpContextAccessor httpContextAccessor,
            [FromBody] AgeLicenciatarioSaveDAO ageLicenciatariosSaveDAO)
        {
            AgeLicenciatarioDAO ageLicenciatariosDAO = await _service.Actualizar(httpContextAccessor, ageLicenciatariosSaveDAO);
            return Ok(ageLicenciatariosDAO);
        }

        [HttpPost]
        [ProducesResponseType(typeof(Resource<AgeLicenciatarioDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> Insertar(
            [FromServices] IHttpContextAccessor httpContextAccessor,
            [FromBody] AgeLicenciatarioSaveDAO ageLicenciatariosSaveDAO)
        {
            Resource<AgeLicenciatarioDAO> AgeLicenciatariosDAO = await _service.Insertar(httpContextAccessor, ageLicenciatariosSaveDAO);
            return Ok(AgeLicenciatariosDAO);
        }

        [HttpPut("varios")]
        [ProducesResponseType(typeof(List<AgeLicenciatarioDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ActualizarVarios(
            [FromServices] IHttpContextAccessor httpContextAccessor,
            [FromBody] List<AgeLicenciatarioSaveDAO> ageLicenciatariosSaveDAOList)
        {
            List<AgeLicenciatarioDAO> agePaisesDAOList = await _service.ActualizarVarios(httpContextAccessor, ageLicenciatariosSaveDAOList);
            return Ok(agePaisesDAOList);
        }

        [HttpPost("varios")]
        [ProducesResponseType(typeof(List<Resource<AgeLicenciatarioDAO>>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> InsertarVarios(
            [FromServices] IHttpContextAccessor httpContextAccessor,
            [FromBody] List<AgeLicenciatarioSaveDAO> ageLicenciatariosSaveDAOList)
        {
            List<Resource<AgeLicenciatarioDAO>> agePaisesDAOList = await _service.InsertarVarios(httpContextAccessor, ageLicenciatariosSaveDAOList);
            return Ok(agePaisesDAOList);
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(AgeLicenciatarioDAO), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarPorId(
        [FromRoute, Required] int id)
        {
            AgeLicenciatarioDAO ageLicenciatariosDAO = await _service.ConsultarPorId(id);
            return Ok(ageLicenciatariosDAO);
        }

        [HttpGet("filtro")]
        [ProducesResponseType(typeof(Page<AgeLicenciatarioDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarListaFiltro(
            [FromQuery, Required] string filtro,
            [FromQuery] Pageable pageable)
        {
            Page<AgeLicenciatarioDAO> ageLicenciatariosDAOList =
                await _service.ConsultarListaFiltro(filtro, pageable);

            return Ok(ageLicenciatariosDAOList);
        }

    }
}
