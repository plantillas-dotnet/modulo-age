using AGE.Entities.DAO.AgeFeriadosLocalidade;
using AGE.Services.AgeFeriadosLocalidadeService;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace AGE.Controllers
{

    [Route("[controller]")]
    [ApiController]
    public class AgeFeriadosLocalidadesController : ControllerBase
    {

        private readonly IAgeFeriadosLocalidadesService _service;

        public AgeFeriadosLocalidadesController(IAgeFeriadosLocalidadesService Service)
        {
            _service = Service;
        }

        [HttpGet]
        [ProducesResponseType(typeof(Page<AgeFeriadosLocalidadesDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarTodos(
            [FromQuery] Pageable pageable)
        {
            Page<AgeFeriadosLocalidadesDAO> ageFeriadosLocalidadeDAOList = await _service.ConsultarTodos(pageable);

            return Ok(ageFeriadosLocalidadeDAOList);
        }


        [HttpGet("{codigoPais}/{codigoTipoLocalidad}/{codigoLocalidad}/{codigoDiaFeriado}")]
        [ProducesResponseType(typeof(AgeFeriadosLocalidadesDAO), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarPorId(
            [FromRoute] AgeFeriadosLocalidadesPKDAO ageFeriadosLocalidadePKDAO)
        {
            AgeFeriadosLocalidadesDAO ageFeriadosLocalidadeDAO =
                await _service.ConsultarPorId(ageFeriadosLocalidadePKDAO);

            return Ok(ageFeriadosLocalidadeDAO);
        }



        [HttpGet("filtro")]
        [ProducesResponseType(typeof(Page<AgeFeriadosLocalidadesDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarListaFiltro(
            [FromQuery, Required] string filtro,
            [FromQuery] Pageable pageable)
        {
            Page<AgeFeriadosLocalidadesDAO> ageFeriadosLocalidadeDAOList =
                await _service.ConsultarListaFiltro(filtro, pageable);

            return Ok(ageFeriadosLocalidadeDAOList);
        }


        [HttpPost]
        [ProducesResponseType(typeof(Resource<AgeFeriadosLocalidadesDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> Insertar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeFeriadosLocalidadesSaveDAO ageFeriadosLocalidadeSaveDAO)
        {
            Resource<AgeFeriadosLocalidadesDAO> ageFeriadosLocalidadeDAO =
                await _service.Insertar(_httpContextAccessor, ageFeriadosLocalidadeSaveDAO);

            return Ok(ageFeriadosLocalidadeDAO);
        }


        [HttpPost("varios")]
        [ProducesResponseType(typeof(List<Resource<AgeFeriadosLocalidadesDAO>>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> InsertarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeFeriadosLocalidadesSaveDAO> ageFeriadosLocalidadeSaveDAOList)
        {
            List<Resource<AgeFeriadosLocalidadesDAO>> ageFeriadosLocalidadeDAOList =
                await _service.InsertarVarios(_httpContextAccessor, ageFeriadosLocalidadeSaveDAOList);

            return Ok(ageFeriadosLocalidadeDAOList);
        }


        [HttpPut]
        [ProducesResponseType(typeof(AgeFeriadosLocalidadesDAO), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> Actualizar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeFeriadosLocalidadesSaveDAO ageFeriadosLocalidadeSaveDAO)
        {
            AgeFeriadosLocalidadesDAO ageFeriadosLocalidadeDAO =
                await _service.Actualizar(_httpContextAccessor, ageFeriadosLocalidadeSaveDAO);

            return Ok(ageFeriadosLocalidadeDAO);
        }


        [HttpPut("varios")]
        [ProducesResponseType(typeof(List<AgeFeriadosLocalidadesDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ActualizarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeFeriadosLocalidadesSaveDAO> ageFeriadosLocalidadeSaveDAOList)
        {
            List<AgeFeriadosLocalidadesDAO> ageFeriadosLocalidadeDAOList =
                await _service.ActualizarVarios(_httpContextAccessor, ageFeriadosLocalidadeSaveDAOList);

            return Ok(ageFeriadosLocalidadeDAOList);
        }

    }
}
