﻿using AGE.Entities.DAO.AgeRutas;
using AGE.Services.AgeRutasService;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace AGE.Controllers
{
    [Route("[controller]")]
    [ApiController]
    //[Authorize]
    public class AgeRutasController : ControllerBase
    {
        private readonly IAgeRutasService _service;

        public AgeRutasController(IAgeRutasService Service)
        {
            _service = Service;
        }

        [HttpGet("{codigoLicenciatario}")]
        [ProducesResponseType(typeof(Page<AgeRutasDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarTodos(
            [FromRoute] int codigoLicenciatario,
            [FromQuery] int? codigo,
            [FromQuery] string? ruta,
            [FromQuery] string? descripcion,
           [FromQuery] Pageable pageable)
        {
            Page<AgeRutasDAO> ageRutasDAOList = 
                await _service.ConsultarTodos(codigoLicenciatario, codigo ?? 0, ruta ?? "", 
                                              descripcion ?? "", pageable);
            
            return Ok(ageRutasDAOList);
        }


        [HttpGet("{codigoLicenciatario}/{id}")]
        [ProducesResponseType(typeof(AgeRutasDAO), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarPorId(
            [FromRoute] AgeRutasPKDAO ageRutasPKDAO)
        {
            AgeRutasDAO ageRutasDAO = await _service.ConsultarPorId(ageRutasPKDAO);
            return Ok(ageRutasDAO);
        }



        [HttpGet("filtro/{codigoLicenciatario}")]
        [ProducesResponseType(typeof(Page<AgeRutasDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarListaFiltro(
            [FromRoute] int codigoLicenciatario,
            [FromQuery, Required] string filtro,
           [FromQuery] Pageable pageable)
        {
            Page<AgeRutasDAO> ageRutasDAOList = 
                await _service.ConsultarListaFiltro(codigoLicenciatario, filtro, pageable);

            return Ok(ageRutasDAOList);
        }


        [HttpPost]
        [ProducesResponseType(typeof(Resource<AgeRutasDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> Insertar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeRutasSaveDAO ageRutasSaveDAO)
        {
            Resource<AgeRutasDAO> ageRutasDAO = await _service.Insertar(_httpContextAccessor, ageRutasSaveDAO);
            return Ok(ageRutasDAO);
        }

        
        [HttpPost("varios")]
        [ProducesResponseType(typeof(List<Resource<AgeRutasDAO>>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> InsertarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeRutasSaveDAO> ageRutasSaveDAOList)
        {
            List<Resource<AgeRutasDAO>> ageRutasDAOList = 
                await _service.InsertarVarios(_httpContextAccessor, ageRutasSaveDAOList);

            return Ok(ageRutasDAOList);
        }


        [HttpPut]
        [ProducesResponseType(typeof(AgeRutasDAO), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> Actualizar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeRutasSaveDAO ageRutasSaveDAO)
        {
            AgeRutasDAO ageRutasDAO = await _service.Actualizar(_httpContextAccessor, ageRutasSaveDAO);
            return Ok(ageRutasDAO);
        }


        [HttpPut("varios")]
        [ProducesResponseType(typeof(List<AgeRutasDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ActualizarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeRutasSaveDAO> ageRutasSaveDAOList)
        {
            List<AgeRutasDAO> ageRutasDAOList = 
                await _service.ActualizarVarios(_httpContextAccessor, ageRutasSaveDAOList);

            return Ok(ageRutasDAOList);
        }
    }
}
