﻿using AGE.Firebase.Models;
using AGE.Firebase.Services.PushNotificationService;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace AGE.Controllers
{

    [Route("[controller]")]
    [ApiController]
    public class AgeNotificacionController : ControllerBase
    {

        private readonly IPushNotificationService _service;

        public AgeNotificacionController(IPushNotificationService Service)
        {
            _service = Service;
        }


        [HttpPost("topic")]
        [ProducesResponseType(typeof(PushNotificationResponse), 200)]
        //[Authorize(Roles = "Empleado")]
        public IActionResult SendNotification(
            [FromBody] PushNotificationRequest pushNotificationRequest)
        {
            _service.SendPushNotificationWithoutData(pushNotificationRequest);

            PushNotificationResponse pushNotificationResponse =
                new PushNotificationResponse((int)HttpStatusCode.OK, "Notification has been sent.");

            return Ok(pushNotificationResponse);
        }


        [HttpPost("topic/data")]
        [ProducesResponseType(typeof(PushNotificationResponse), 200)]
        //[Authorize(Roles = "Empleado")]
        public IActionResult SendDataNotification(
            [FromBody] PushNotificationRequest pushNotificationRequest)
        {
            _service.SendPushNotification(pushNotificationRequest);

            PushNotificationResponse pushNotificationResponse =
                new PushNotificationResponse((int)HttpStatusCode.OK, "Notification has been sent.");

            return Ok(pushNotificationResponse);
        }


        [HttpPost("token")]
        [ProducesResponseType(typeof(PushNotificationResponse), 200)]
        //[Authorize(Roles = "Empleado")]
        public IActionResult SendTokenNotification(
            [FromBody] PushNotificationRequest pushNotificationRequest)
        {
            _service.SendPushNotificationToToken(pushNotificationRequest);

            PushNotificationResponse pushNotificationResponse =
                new PushNotificationResponse((int)HttpStatusCode.OK, "Notification has been sent.");

            return Ok(pushNotificationResponse);
        }


        [HttpPost("token/data")]
        [ProducesResponseType(typeof(PushNotificationResponse), 200)]
        //[Authorize(Roles = "Empleado")]
        public IActionResult SendDataNotificationToken(
            [FromBody] PushNotificationRequest pushNotificationRequest)
        {
            _service.SendPushNotificationToken(pushNotificationRequest, null);

            PushNotificationResponse pushNotificationResponse =
                new PushNotificationResponse((int)HttpStatusCode.OK, "Notification has been sent.");

            return Ok(pushNotificationResponse);
        }

    }
}
