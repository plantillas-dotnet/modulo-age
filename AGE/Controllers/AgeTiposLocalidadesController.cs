using AGE.Entities.DAO.AgeTiposLocalidades;
using AGE.Services.AgeTiposLocalidadeService;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace AGE.Controllers
{
    [Route("[controller]")]
    [ApiController]
    //[Authorize]
    public class AgeTiposLocalidadesController : ControllerBase
    {
        private readonly IAgeTiposLocalidadesService _service;

        public AgeTiposLocalidadesController(IAgeTiposLocalidadesService service)
        {
            _service = service;
        }

        [HttpGet("{codigoPais}")]
        [ProducesResponseType(typeof(Page<AgeTiposLocalidadeDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarTodos(
            [FromRoute] int codigoPais,
            [FromQuery] int? codigo,
            [FromQuery] int? codigoAgeTipLoAgePais,
            [FromQuery] int? codigoAgeTipLo,
            [FromQuery] string? descripcion,
            [FromQuery] Pageable pageable)
        {
            Page<AgeTiposLocalidadeDAO> ageTiposLocalidadeDAOList =
                await _service.ConsultarTodos(codigoPais, codigo ?? 0, codigoAgeTipLoAgePais ?? 0, codigoAgeTipLo ?? 0,
                                              descripcion ?? "", pageable);

            return Ok(ageTiposLocalidadeDAOList);
        }

        [HttpGet("{codigoPais}/{id}")]
        [ProducesResponseType(typeof(AgeTiposLocalidadeDAO), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarPorId(
            [FromRoute] int codigoPais,
            [FromRoute] int id)
        {
            AgeTiposLocalidadePKDAO ageTiposLocalidadePKDAO = new AgeTiposLocalidadePKDAO
            {
                agePaisCodigo = codigoPais,
                codigo = id
            };

            AgeTiposLocalidadeDAO ageTiposLocalidadeDAO = await _service.ConsultarPorId(ageTiposLocalidadePKDAO);
            return Ok(ageTiposLocalidadeDAO);
        }

        [HttpGet("filtro")]
        [ProducesResponseType(typeof(Page<AgeTiposLocalidadeDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarListaFiltro(
            [FromQuery, Required] string filtro,
            [FromQuery] Pageable pageable)
        {
            Page<AgeTiposLocalidadeDAO> ageTiposLocalidadeDAOList =
                await _service.ConsultarListaFiltro(filtro, pageable);

            return Ok(ageTiposLocalidadeDAOList);
        }


        [HttpPost]
        [ProducesResponseType(typeof(Resource<AgeTiposLocalidadeDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> Insertar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeTiposLocalidadeSaveDAO ageTiposLocalidadeSaveDAO)
        {
            Resource<AgeTiposLocalidadeDAO> ageTiposLocalidadeDAO = await _service.Insertar(_httpContextAccessor, ageTiposLocalidadeSaveDAO);
            return Ok(ageTiposLocalidadeDAO);
        }


        [HttpPost("varios")]
        [ProducesResponseType(typeof(List<Resource<AgeTiposLocalidadeDAO>>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> InsertarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeTiposLocalidadeSaveDAO> ageTiposLocalidadeSaveDAOList)
        {
            List<Resource<AgeTiposLocalidadeDAO>> ageTiposLocalidadeDAOList =
                await _service.InsertarVarios(_httpContextAccessor, ageTiposLocalidadeSaveDAOList);

            return Ok(ageTiposLocalidadeDAOList);
        }


        [HttpPut]
        [ProducesResponseType(typeof(AgeTiposLocalidadeDAO), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> Actualizar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeTiposLocalidadeSaveDAO ageTiposLocalidadeSaveDAO)
        {
            AgeTiposLocalidadeDAO ageTiposLocalidadeDAO = await _service.Actualizar(_httpContextAccessor, ageTiposLocalidadeSaveDAO);
            return Ok(ageTiposLocalidadeDAO);
        }


        [HttpPut("varios")]
        [ProducesResponseType(typeof(List<AgeTiposLocalidadeDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ActualizarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeTiposLocalidadeSaveDAO> ageTiposLocalidadeSaveDAOList)
        {
            List<AgeTiposLocalidadeDAO> ageTiposLocalidadeDAOList =
                await _service.ActualizarVarios(_httpContextAccessor, ageTiposLocalidadeSaveDAOList);

            return Ok(ageTiposLocalidadeDAOList);
        }
    }
}
