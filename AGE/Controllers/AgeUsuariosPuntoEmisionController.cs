﻿using AGE.Entities.DAO.AgeUsuariosPuntoEmision;
using AGE.Services.AgeUsuariosPuntoEmisionService;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.ComponentModel.DataAnnotations;

namespace AGE.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class AgeUsuariosPuntoEmisionController : ControllerBase
    {
        private readonly IAgeUsuariosPuntoEmisionService _service;

        public AgeUsuariosPuntoEmisionController(IAgeUsuariosPuntoEmisionService Service)
        {
            _service = Service;
        }

        [HttpGet("{codigoLicenciatario}")]
        [ProducesResponseType(typeof(Page<AgeUsuariosPuntoEmisionDAO>), 200)]
        public async Task<IActionResult> ConsultarTodos([FromRoute] int codigoLicenciatario,
                                                        [FromQuery] int agePunEmCodigo,
                                                        [FromQuery] int agePunEmAgeSucursCodigo,
                                                        [FromQuery] long ageUsuariCodigo,
                                                        [FromQuery] Pageable pageable)
        {
            Page<AgeUsuariosPuntoEmisionDAO> ageUsuariosPuntoEmisionDAOList =
                await _service.ConsultarTodos(codigoLicenciatario, agePunEmCodigo,agePunEmAgeSucursCodigo,ageUsuariCodigo, pageable);

            return Ok(ageUsuariosPuntoEmisionDAOList);
        }

        [HttpGet("{codigoLicenciatario}/{codigoPuntoEmision}/{codigoSucursal}/{codigoUsuario}")]
        [ProducesResponseType(typeof(AgeUsuariosPuntoEmisionDAO), 200)]
        public async Task<IActionResult> ConsultarPorId([FromRoute] int codigoLicenciatario,
                                                        [FromRoute] int codigoPuntoEmision,
                                                        [FromRoute] int codigoSucursal,
                                                        [FromRoute] long codigoUsuario)
        {
            var ageUsuariosPuntoEmisionDAO = await _service.ConsultarPorId(codigoLicenciatario, codigoPuntoEmision, codigoSucursal, codigoUsuario);
            return Ok(ageUsuariosPuntoEmisionDAO);
        }

        [HttpGet("filtro/{codigoLicenciatario}")]
        [ProducesResponseType(typeof(Page<AgeUsuariosPuntoEmisionDAO>), 200)]
        public async Task<IActionResult> ConsultarListaFiltro(
           [FromRoute, Required] int codigoLicenciatario,
           [FromQuery, Required] string filtro,
           [FromQuery] Pageable pageable)
        {
            Page<AgeUsuariosPuntoEmisionDAO> ageUsuariosPuntoEmisionDAO =
                await _service.ConsultarListaFiltro(codigoLicenciatario, filtro, pageable);

            return Ok(ageUsuariosPuntoEmisionDAO);
        }

        [HttpPost]
        [ProducesResponseType(typeof(Resource<AgeUsuariosPuntoEmisionDAO>), 200)]
        public async Task<IActionResult> Insertar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeUsuariosPuntoEmisionSaveDAO ageUsuarioPuntoEmisionSaveDAO)
        {
            Resource<AgeUsuariosPuntoEmisionDAO> ageUsuarioPuntoEmisionDAO =
                await _service.Ingresar(_httpContextAccessor, ageUsuarioPuntoEmisionSaveDAO);

            return Ok(ageUsuarioPuntoEmisionDAO);
        }

        [HttpPost("varios")]
        [ProducesResponseType(typeof(List<Resource<AgeUsuariosPuntoEmisionDAO>>), 200)]
        public async Task<IActionResult> InsertarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeUsuariosPuntoEmisionSaveDAO> ageUsuariosPuntoEmisionSaveDAOList)
        {
            List<Resource<AgeUsuariosPuntoEmisionDAO>> ageUsuariosPuntoEmisionDAOList =
                await _service.IngresarVarios(_httpContextAccessor, ageUsuariosPuntoEmisionSaveDAOList);

            return Ok(ageUsuariosPuntoEmisionDAOList);
        }

        [HttpPut]
        [ProducesResponseType(typeof(AgeUsuariosPuntoEmisionDAO), 200)]
        public async Task<IActionResult> Actualizar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeUsuariosPuntoEmisionSaveDAO ageUsuariosPuntoEmisionSaveDAO)
        {
            AgeUsuariosPuntoEmisionDAO ageUsuariosPuntoEmisionDAO = await _service.Actualizar(_httpContextAccessor, ageUsuariosPuntoEmisionSaveDAO);
            return Ok(ageUsuariosPuntoEmisionDAO);
        }


        [HttpPut("varios")]
        [ProducesResponseType(typeof(List<AgeUsuariosPuntoEmisionDAO>), 200)]
        public async Task<IActionResult> ActualizarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeUsuariosPuntoEmisionSaveDAO> ageUsuariosPuntoEmisionSaveDAOList)
        {
            List<AgeUsuariosPuntoEmisionDAO> ageUsuariosPuntoEmisionDAOList =
                await _service.ActualizarVarios(_httpContextAccessor, ageUsuariosPuntoEmisionSaveDAOList);

            return Ok(ageUsuariosPuntoEmisionDAOList);
        }
    }
}
