using AGE.Entities.DAO.AgeClasesContribuyentes;
using AGE.Services.AgeClasesContribuyentesService;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
namespace AGE.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class AgeClasesContribuyentesController : ControllerBase
    {
        private readonly IAgeClasesContribuyentesService _service;

        public AgeClasesContribuyentesController(IAgeClasesContribuyentesService Service)
        {
            _service = Service;
        }

        [HttpGet]
        [ProducesResponseType(typeof(Page<AgeClasesContribuyentesDAO>), 200)]
        public async Task<IActionResult> ConsultarTodos(
            [FromQuery] string? descripcion,
            [FromQuery] Pageable pageable)
        {
            Page<AgeClasesContribuyentesDAO> ageClasesContribuyentesDAOList =
                await _service.ConsultarTodos(descripcion ?? "", pageable);

            return Ok(ageClasesContribuyentesDAOList);
        }


        [HttpGet("filtro")]
        [ProducesResponseType(typeof(Page<AgeClasesContribuyentesDAO>), 200)]
        public async Task<IActionResult> ConsultarListaFiltro(
            [FromQuery, Required] string filtro,
            [FromQuery] Pageable pageable)
        {
            Page<AgeClasesContribuyentesDAO> ageClasesContribuyentesDAOList =
                await _service.ConsultarListaFiltro(filtro, pageable);

            return Ok(ageClasesContribuyentesDAOList);
        }


        [HttpGet("{id}")]
        [ProducesResponseType(typeof(AgeClasesContribuyentesDAO), 200)]
        public async Task<IActionResult> ConsultarPorId(
            [FromRoute, Required] long id)
        {
            AgeClasesContribuyentesDAO ageClasesContribuyentesDAO = await _service.ConsultarPorId(id);
            return Ok(ageClasesContribuyentesDAO);
        }

        [HttpPost]
        [ProducesResponseType(typeof(Resource<AgeClasesContribuyentesDAO>), 200)]
        public async Task<IActionResult> Insertar(
            [FromServices] IHttpContextAccessor httpContextAccessor,
            [FromBody] AgeClasesContribuyentesSaveDAO ageClasesContribuyentesSaveDAO)
        {
            Resource<AgeClasesContribuyentesDAO> ageClasesContribuyentesDAO = await _service.Insertar(httpContextAccessor, ageClasesContribuyentesSaveDAO);
            return Ok(ageClasesContribuyentesDAO);
        }

        [HttpPost("varios")]
        [ProducesResponseType(typeof(List<Resource<AgeClasesContribuyentesDAO>>), 200)]
        public async Task<IActionResult> InsertarVarios(
            [FromServices] IHttpContextAccessor httpContextAccessor,
            [FromBody] List<AgeClasesContribuyentesSaveDAO> ageClasesContribuyentesSaveDAOList)
        {
            List<Resource<AgeClasesContribuyentesDAO>> ageClasesContribuyentesDAOList = await _service.InsertarVarios(httpContextAccessor, ageClasesContribuyentesSaveDAOList);
            return Ok(ageClasesContribuyentesDAOList);
        }

        [HttpPut]
        [ProducesResponseType(typeof(AgeClasesContribuyentesDAO), 200)]
        public async Task<IActionResult> Actualizar(
            [FromServices] IHttpContextAccessor httpContextAccessor,
            [FromBody] AgeClasesContribuyentesSaveDAO ageClasesContribuyentesSaveDAO)
        {
            AgeClasesContribuyentesDAO ageClasesContribuyentesDAO = await _service.Actualizar(httpContextAccessor, ageClasesContribuyentesSaveDAO);
            return Ok(ageClasesContribuyentesDAO);
        }


        [HttpPut("varios")]
        [ProducesResponseType(typeof(List<AgeClasesContribuyentesDAO>), 200)]
        public async Task<IActionResult> ActualizarVarios(
            [FromServices] IHttpContextAccessor httpContextAccessor,
            [FromBody] List<AgeClasesContribuyentesSaveDAO> ageClasesContribuyentesSaveDAOList)
        {
            List<AgeClasesContribuyentesDAO> ageClasesContribuyentesDAOList = await _service.ActualizarVarios(httpContextAccessor, ageClasesContribuyentesSaveDAOList);
            return Ok(ageClasesContribuyentesDAOList);
        }
    }
}
