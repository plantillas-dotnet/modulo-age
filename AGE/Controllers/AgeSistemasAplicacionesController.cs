﻿using AGE.Entities.DAO.AgeSistemasAplicaciones;
using AGE.Services.AgeSistemasAplicacionesService;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace AGE.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class AgeSistemasAplicacionesController : ControllerBase
    {
        private readonly IAgeSistemasAplicacionesService _service;

        public AgeSistemasAplicacionesController(IAgeSistemasAplicacionesService Service)
        {
            _service = Service;
        }

        [HttpGet("{codigoSistema}")]
        [ProducesResponseType(typeof(Page<AgeSistemasAplicacionesDAO>), 200)]
        public async Task<IActionResult> ConsultarTodos( [FromRoute] int codigoSistema, 
                                                        [FromQuery] int ageAplicaCodigo,
                                                        [FromQuery] DateTime fechaDesde,
                                                        [FromQuery] DateTime fechaHasta,
                                                        [FromQuery] Pageable pageable)
        {
            Page<AgeSistemasAplicacionesDAO> ageSistemasAplicacionesDAOList =
                await _service.ConsultarTodos(ageAplicaCodigo, codigoSistema, fechaDesde, fechaHasta, pageable);

            return Ok(ageSistemasAplicacionesDAOList);
        }

        [HttpGet("{codigoSistema}/{codigoAplicacion}")]
        [ProducesResponseType(typeof(AgeSistemasAplicacionesDAO), 200)]
        public async Task<IActionResult> ConsultarPorId(
            [FromRoute] AgeSistemasAplicacionesPKDAO ageSistemasAplicacionesPKDAO)
        {
            var ageSistemasAplicacionesDAO = await _service.ConsultarPorId(ageSistemasAplicacionesPKDAO);
            return Ok(ageSistemasAplicacionesDAO);
        }

        [HttpGet("filtro")]
        [ProducesResponseType(typeof(Page<AgeSistemasAplicacionesDAO>), 200)]
        public async Task<IActionResult> ConsultarListaFiltro(
           [FromQuery, Required] string filtro,
           [FromQuery] Pageable pageable)
        {
            Page<AgeSistemasAplicacionesDAO> ageSistemasAplicacionesDAO =
                await _service.ConsultarListaFiltro(filtro, pageable);

            return Ok(ageSistemasAplicacionesDAO);
        }

        [HttpPost]
        [ProducesResponseType(typeof(Resource<AgeSistemasAplicacionesDAO>), 200)]
        public async Task<IActionResult> Insertar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeSistemasAplicacionesSaveDAO ageSistemasAplicacionesSaveDAO)
        {
            Resource<AgeSistemasAplicacionesDAO> ageSistemasAplicacionesDAO =
                await _service.Insertar(_httpContextAccessor, ageSistemasAplicacionesSaveDAO);

            return Ok(ageSistemasAplicacionesDAO);
        }

        [HttpPost("varios")]
        [ProducesResponseType(typeof(List<Resource<AgeSistemasAplicacionesDAO>>), 200)]
        public async Task<IActionResult> InsertarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeSistemasAplicacionesSaveDAO> ageSistemasAplicacionesSaveDAOList)
        {
            List<Resource<AgeSistemasAplicacionesDAO>> ageSistemasAplicacionesDAOList =
                await _service.InsertarVarios(_httpContextAccessor, ageSistemasAplicacionesSaveDAOList);

            return Ok(ageSistemasAplicacionesDAOList);
        }

        [HttpPut]
        [ProducesResponseType(typeof(AgeSistemasAplicacionesDAO), 200)]
        public async Task<IActionResult> Actualizar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeSistemasAplicacionesSaveDAO ageSistemasAplicacionesSaveDAO)
        {
            AgeSistemasAplicacionesDAO ageSistemasAplicacionesDAO = await _service.Actualizar(_httpContextAccessor, ageSistemasAplicacionesSaveDAO);
            return Ok(ageSistemasAplicacionesDAO);
        }

        [HttpPut("varios")]
        [ProducesResponseType(typeof(List<AgeSistemasAplicacionesDAO>), 200)]
        public async Task<IActionResult> ActualizarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeSistemasAplicacionesSaveDAO> ageSistemasAplicacionesSaveDAOList)
        {
            List<AgeSistemasAplicacionesDAO> ageSistemasAplicacionesDAOList =
                await _service.ActualizarVarios(_httpContextAccessor, ageSistemasAplicacionesSaveDAOList);

            return Ok(ageSistemasAplicacionesDAOList);
        }
    }
}
