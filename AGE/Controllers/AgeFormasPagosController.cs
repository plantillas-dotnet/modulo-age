﻿using AGE.Entities.DAO.AgeFormasPago;
using AGE.Services.AgeFormasPagoService;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace AGE.Controllers
{

    [Route("[controller]")]
    [ApiController]
    public class AgeFormasPagosController : ControllerBase
    {

        private readonly IAgeFormasPagosService _service;

        public AgeFormasPagosController(IAgeFormasPagosService Service)
        {
            _service = Service;
        }

        [HttpGet("{codigoLicenciatario}")]
        [ProducesResponseType(typeof(Page<AgeFormasPagosDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarTodos(
            [FromRoute] int codigoLicenciatario,
            [FromQuery] int? codigoInstitucionControl,
            [FromQuery] string? descripcion,
            [FromQuery] Pageable pageable)
        {
            Page<AgeFormasPagosDAO> ageFormasPagoDAOList =
                await _service.ConsultarTodos(codigoLicenciatario, codigoInstitucionControl ?? 0, descripcion ?? "", pageable);

            return Ok(ageFormasPagoDAOList);
        }


        [HttpGet("{codigoLicenciatario}/{id}")]
        [ProducesResponseType(typeof(AgeFormasPagosDAO), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarPorId(
            [FromRoute] AgeFormasPagosPKDAO ageFormasPagoPKDAO)
        {
            AgeFormasPagosDAO ageFormasPagoDAO =
                await _service.ConsultarPorId(ageFormasPagoPKDAO);

            return Ok(ageFormasPagoDAO);
        }



        [HttpGet("filtro/{codigoLicenciatario}")]
        [ProducesResponseType(typeof(Page<AgeFormasPagosDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarListaFiltro(
            [FromRoute] int codigoLicenciatario,
            [FromQuery, Required] string filtro,
            [FromQuery] Pageable pageable)
        {
            Page<AgeFormasPagosDAO> ageFormasPagoDAOList =
                await _service.ConsultarListaFiltro(codigoLicenciatario, filtro, pageable);

            return Ok(ageFormasPagoDAOList);
        }


        [HttpPost]
        [ProducesResponseType(typeof(Resource<AgeFormasPagosDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> Insertar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeFormasPagosSaveDAO ageFormasPagoSaveDAO)
        {
            Resource<AgeFormasPagosDAO> ageFormasPagoDAO =
                await _service.Insertar(_httpContextAccessor, ageFormasPagoSaveDAO);

            return Ok(ageFormasPagoDAO);
        }


        [HttpPost("varios")]
        [ProducesResponseType(typeof(List<Resource<AgeFormasPagosDAO>>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> InsertarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeFormasPagosSaveDAO> ageFormasPagoSaveDAOList)
        {
            List<Resource<AgeFormasPagosDAO>> ageFormasPagoDAOList =
                await _service.InsertarVarios(_httpContextAccessor, ageFormasPagoSaveDAOList);

            return Ok(ageFormasPagoDAOList);
        }


        [HttpPut]
        [ProducesResponseType(typeof(AgeFormasPagosDAO), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> Actualizar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeFormasPagosSaveDAO ageFormasPagoSaveDAO)
        {
            AgeFormasPagosDAO ageFormasPagoDAO =
                await _service.Actualizar(_httpContextAccessor, ageFormasPagoSaveDAO);

            return Ok(ageFormasPagoDAO);
        }


        [HttpPut("varios")]
        [ProducesResponseType(typeof(List<AgeFormasPagosDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ActualizarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeFormasPagosSaveDAO> ageFormasPagoSaveDAOList)
        {
            List<AgeFormasPagosDAO> ageFormasPagoDAOList =
                await _service.ActualizarVarios(_httpContextAccessor, ageFormasPagoSaveDAOList);

            return Ok(ageFormasPagoDAOList);
        }

    }
}
