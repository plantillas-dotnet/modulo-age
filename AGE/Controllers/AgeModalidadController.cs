﻿using AGE.Entities.DAO.AgeModalidades;
using AGE.Services.AgeModalidadesService;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace AGE.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class AgeModalidadController : ControllerBase
    {
        private readonly IAgeModalidadService _service;

        public AgeModalidadController(IAgeModalidadService service)
        {
            _service = service;
        }

        [HttpGet("{codigoLicenciatario}")]
        [ProducesResponseType(typeof(Page<AgeModalidadesDAO>), 200)]
        public async Task<IActionResult> ConsultarTodos(
            [FromRoute] int codigoLicenciatario,
            [FromQuery] int? codigo,
            [FromQuery] string? codigoExterno,
            [FromQuery] string? descripcion,
            [FromQuery] Pageable pageable)
        {
            Page<AgeModalidadesDAO> ageModalidadesDAOList =
                await _service.ConsultarTodos(codigoLicenciatario, codigo ?? 0,codigoExterno ?? "", descripcion ?? "", pageable);

            return Ok(ageModalidadesDAOList);
        }

        [HttpGet("{codigoLicenciatario}/{id}")]
        [ProducesResponseType(typeof(AgeModalidadesDAO), 200)]
        public async Task<IActionResult> ConsultarPorId(
            [FromRoute] AgeModalidadesPKDAO ageModalidadesPKDAP)
        {
            var ageModalidadesDAO = await _service.ConsultarPorId(ageModalidadesPKDAP);
            return Ok(ageModalidadesDAO);
        }

        [HttpGet("filtro/{codigoLicenciatario}")]
        [ProducesResponseType(typeof(Page<AgeModalidadesDAO>), 200)]
        public async Task<IActionResult> ConsultarListaFiltro(
           [FromRoute, Required] int codigoLicenciatario,
           [FromQuery, Required] string filtro,
           [FromQuery] Pageable pageable)
        {
            Page<AgeModalidadesDAO> ageModalidadesDAO =
                await _service.ConsultarListaFiltro(codigoLicenciatario, filtro, pageable);

            return Ok(ageModalidadesDAO);
        }

        [HttpPost]
        [ProducesResponseType(typeof(Resource<AgeModalidadesDAO>), 200)]
        public async Task<IActionResult> Insertar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeModalidadesSaveDAO ageModalidadesSave)
        {
            Resource<AgeModalidadesDAO> ageModalidadesDAO =
                await _service.Insertar(_httpContextAccessor,ageModalidadesSave);

            return Ok(ageModalidadesDAO);
        }


        [HttpPost("varios")]
        [ProducesResponseType(typeof(List<Resource<AgeModalidadesDAO>>), 200)]
        public async Task<IActionResult> InsertarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeModalidadesSaveDAO> ageModalidadesSaveDAOList)
        {
            List<Resource<AgeModalidadesDAO>> ageModalidadesDAOList =
                await _service.InsertarVarios(_httpContextAccessor, ageModalidadesSaveDAOList);

            return Ok(ageModalidadesDAOList);
        }

        [HttpPut]
        [ProducesResponseType(typeof(AgeModalidadesDAO), 200)]
        public async Task<IActionResult> Actualizar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeModalidadesSaveDAO ageModalidadesSaveDAO)
        {
            AgeModalidadesDAO ageModalidadesDAO =
                await _service.Actualizar(_httpContextAccessor, ageModalidadesSaveDAO);

            return Ok(ageModalidadesDAO);
        }


        [HttpPut("varios")]
        [ProducesResponseType(typeof(List<AgeModalidadesDAO>), 200)]
        public async Task<IActionResult> ActualizarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeModalidadesSaveDAO> ageModalidadesSaveDAOList)
        {
            List<AgeModalidadesDAO> ageModalidadesDAOList =
                await _service.ActualizarVarios(_httpContextAccessor, ageModalidadesSaveDAOList);

            return Ok(ageModalidadesDAOList);
        }
    }
}
