using AGE.Entities.DAO.AgeAplicaciones;
using AGE.Services.AgeAplicacionesService;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace AGE.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class AgeAplicacionesController : Controller
    {
        private readonly IAgeAplicacionesService _service;

        public AgeAplicacionesController(IAgeAplicacionesService Service)
        {
            _service = Service;
        }

        [HttpGet]
        [ProducesResponseType(typeof(Page<AgeAplicacionesDAO>), 200)]
        public async Task<IActionResult> ConsultarTodos(
            [FromQuery] string? codigoExterno,
            [FromQuery] string? descripcion,
            [FromQuery] Pageable pageable)
        {
            Page<AgeAplicacionesDAO> ageAplicacionesDAOList =
                await _service.ConsultarTodos(codigoExterno ?? "", descripcion ?? "", pageable);

            return Ok(ageAplicacionesDAOList);
        }


        [HttpGet("filtro")]
        [ProducesResponseType(typeof(Page<AgeAplicacionesDAO>), 200)]
        public async Task<IActionResult> ConsultarListaFiltro(
            [FromQuery, Required] string filtro,
            [FromQuery] Pageable pageable)
        {
            Page<AgeAplicacionesDAO> ageAplicacionesDAOList =
                await _service.ConsultarListaFiltro(filtro, pageable);

            return Ok(ageAplicacionesDAOList);
        }


        [HttpGet("{id}")]
        [ProducesResponseType(typeof(AgeAplicacionesDAO), 200)]
        public async Task<IActionResult> ConsultarPorId(
            [FromRoute, Required] int id)
        {
            AgeAplicacionesDAO ageAplicacionesDAO = await _service.ConsultarPorId(id);
            return Ok(ageAplicacionesDAO);
        }

        [HttpPost]
        [ProducesResponseType(typeof(Resource<AgeAplicacionesDAO>), 200)]
        public async Task<IActionResult> Insertar(
            [FromServices] IHttpContextAccessor httpContextAccessor,
            [FromBody] AgeAplicacionesSaveDAO ageAplicacionesSaveDAO)
        {
            Resource<AgeAplicacionesDAO> ageAplicacionesDAO = await _service.Insertar(httpContextAccessor, ageAplicacionesSaveDAO);
            return Ok(ageAplicacionesDAO);
        }

        [HttpPost("varios")]
        [ProducesResponseType(typeof(List<Resource<AgeAplicacionesDAO>>), 200)]
        public async Task<IActionResult> InsertarVarios(
            [FromServices] IHttpContextAccessor httpContextAccessor,
            [FromBody] List<AgeAplicacionesSaveDAO> ageAplicacionesSaveDAOList)
        {
            List<Resource<AgeAplicacionesDAO>> ageAplicacionesDAOList = await _service.InsertarVarios(httpContextAccessor, ageAplicacionesSaveDAOList);
            return Ok(ageAplicacionesDAOList);
        }

        [HttpPut]
        [ProducesResponseType(typeof(AgeAplicacionesDAO), 200)]
        public async Task<IActionResult> Actualizar(
            [FromServices] IHttpContextAccessor httpContextAccessor,
            [FromBody] AgeAplicacionesSaveDAO ageAplicacionesSaveDAO)
        {
            AgeAplicacionesDAO ageAplicacionesDAO = await _service.Actualizar(httpContextAccessor, ageAplicacionesSaveDAO);
            return Ok(ageAplicacionesDAO);
        }


        [HttpPut("varios")]
        [ProducesResponseType(typeof(List<AgeAplicacionesDAO>), 200)]
        public async Task<IActionResult> ActualizarVarios(
            [FromServices] IHttpContextAccessor httpContextAccessor,
            [FromBody] List<AgeAplicacionesSaveDAO> ageAplicacionesSaveDAOList)
        {
            List<AgeAplicacionesDAO> ageAplicacionesDAOList = await _service.ActualizarVarios(httpContextAccessor, ageAplicacionesSaveDAOList);
            return Ok(ageAplicacionesDAOList);
        }
    }
}
