﻿using AGE.Entities.DAO.AgeLicenciatParamVigencia;
using AGE.Services.AgeLicenciatParamVigenciaService;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace AGE.Controllers
{
    [Route("[controller]")]
    [ApiController]
    //[Authorize]
    public class AgeLicenciatParamVigenciasController : ControllerBase
    {
        private readonly IAgeLicenciatParamVigenciasService _service;

        public AgeLicenciatParamVigenciasController(IAgeLicenciatParamVigenciasService Service)
        {
            _service = Service;
        }

        [HttpPut]
        [ProducesResponseType(typeof(AgeLicenciatParamVigenciasDAO), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> Actualizar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeLicenciatParamVigenciasSaveDAO ageLicenciatParamVigenciaSaveDAO)
        {
            AgeLicenciatParamVigenciasDAO ageLicenciatParamVigenciaDAO = await _service.Actualizar(_httpContextAccessor, ageLicenciatParamVigenciaSaveDAO);
            return Ok(ageLicenciatParamVigenciaDAO);
        }

        [HttpPost]
        [ProducesResponseType(typeof(Resource<AgeLicenciatParamVigenciasDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> Insertar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeLicenciatParamVigenciasSaveDAO ageLicenciatParamVigenciaSaveDAO)
        {
            Resource<AgeLicenciatParamVigenciasDAO> ageLicenciatParamVigenciaDAO = await _service.Insertar(_httpContextAccessor, ageLicenciatParamVigenciaSaveDAO);
            return Ok(ageLicenciatParamVigenciaDAO);
        }

        [HttpPut("varios")]
        [ProducesResponseType(typeof(List<AgeLicenciatParamVigenciasDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ActualizarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeLicenciatParamVigenciasSaveDAO> ageLicenciatParamVigenciaSaveDAOList)
        {
            List<AgeLicenciatParamVigenciasDAO> ageLicenciatParamVigenciaDAOList =
                await _service.ActualizarVarios(_httpContextAccessor, ageLicenciatParamVigenciaSaveDAOList);

            return Ok(ageLicenciatParamVigenciaDAOList);
        }

        [HttpPost("varios")]
        [ProducesResponseType(typeof(List<Resource<AgeLicenciatParamVigenciasDAO>>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> InsertarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeLicenciatParamVigenciasSaveDAO> ageLicenciatParamVigenciaSaveDAOList)
        {
            List<Resource<AgeLicenciatParamVigenciasDAO>> ageLicenciatParamVigenciaDAOList =
                await _service.InsertarVarios(_httpContextAccessor, ageLicenciatParamVigenciaSaveDAOList);

            return Ok(ageLicenciatParamVigenciaDAOList);
        }

        [HttpGet("{codigoLicenciatario}")]
        [ProducesResponseType(typeof(Page<AgeLicenciatParamVigenciasDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarTodos(
            [FromRoute] int codigoLicenciatario,
            [FromQuery] int? codigoParametro,
            [FromQuery] int? id,
            [FromQuery] string? observacion,
            [FromQuery] string? valorParametro,
            [FromQuery] Pageable pageable)
        {
            Page<AgeLicenciatParamVigenciasDAO> ageLicenciatParamVigenciaDAOList =
                await _service.ConsultarTodos(codigoLicenciatario, codigoParametro ?? 0, id ?? 0, observacion ?? "", valorParametro ?? "", pageable);

            return Ok(ageLicenciatParamVigenciaDAOList);
        }


        [HttpGet("{codigoLicenciatario}/{codigoParametro}/{id}")]
        [ProducesResponseType(typeof(AgeLicenciatParamVigenciasDAO), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarPorId(
            [FromRoute] int codigoLicenciatario,
            [FromRoute] int codigoParametro,
            [FromRoute] int id,
            [FromQuery] Pageable pageable)
        {
            AgeLicenciatParamVigenciasPKDAO ageLicenciatParamVigenciaPKDAO = new AgeLicenciatParamVigenciasPKDAO
            {
                AgeLicencCodigo = codigoLicenciatario,
                AgeParGeCodigo = codigoParametro,
                Codigo = id
            };

            AgeLicenciatParamVigenciasDAO ageLicenciatParamVigenciaDAO = await _service.ConsultarPorId(ageLicenciatParamVigenciaPKDAO);
            return Ok(ageLicenciatParamVigenciaDAO);
        }

        [HttpGet("filtro/{codigoLicenciatario}")]
        [ProducesResponseType(typeof(Page<AgeLicenciatParamVigenciasDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarListaFiltro(
            [FromRoute] int codigoLicenciatario,
            [FromQuery, Required] string filtro,
            [FromQuery] Pageable pageable)
        {
            Page<AgeLicenciatParamVigenciasDAO> ageLicenciatParamVigenciaDAOList =
                await _service.ConsultarListaFiltro(codigoLicenciatario, filtro, pageable);

            return Ok(ageLicenciatParamVigenciaDAOList);
        }
    }
}
