﻿
using AGE.Entities.DAO.AgeUsuariosPerfiles;
using AGE.Services.AgeUsuariosPerfileService;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace AGE.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class AgeUsuariosPerfilesController : ControllerBase
    {
        private readonly IAgeUsuariosPerfilesService _service;

        public AgeUsuariosPerfilesController(IAgeUsuariosPerfilesService Service)
        {
            _service = Service;
        }

        [HttpGet("{codigoLicenciatario}")]
        [ProducesResponseType(typeof(Page<AgeUsuariosPerfileDAO>), 200)]
        public async Task<IActionResult> ConsultarTodos([FromRoute] int codigoLicenciatario,
                                                        [FromQuery] Pageable pageable)
        {
            Page<AgeUsuariosPerfileDAO> ageUsuariosPerfileDAOList =
                await _service.ConsultarTodos(codigoLicenciatario, pageable);

            return Ok(ageUsuariosPerfileDAOList);
        }

        [HttpGet("{codigoLicenciatario}/{codigoUsuario}/{id}")]
        [ProducesResponseType(typeof(AgeUsuariosPerfileDAO), 200)]
        public async Task<IActionResult> ConsultarPorId(
            [FromRoute] AgeUsuariosPerfilePKDAO ageUsuariosPerfilePKDAO)
        {
            var ageUsuariosPerfileDAO = await _service.ConsultarPorId(ageUsuariosPerfilePKDAO);
            return Ok(ageUsuariosPerfileDAO);
        }

        [HttpGet("filtro/{codigoLicenciatario}")]
        [ProducesResponseType(typeof(Page<AgeUsuariosPerfileDAO>), 200)]
        public async Task<IActionResult> ConsultarListaFiltro(
           [FromRoute, Required] int codigoLicenciatario,
           [FromQuery, Required] string filtro,
           [FromQuery] Pageable pageable)
        {
            Page<AgeUsuariosPerfileDAO> ageUsuariosPerfileDAO =
                await _service.ConsultarListaFiltro(codigoLicenciatario, filtro, pageable);

            return Ok(ageUsuariosPerfileDAO);
        }

        [HttpPost]
        [ProducesResponseType(typeof(Resource<AgeUsuariosPerfileDAO>), 200)]
        public async Task<IActionResult> Insertar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeUsuariosPerfileSaveDAO ageUsuarioPerfileSaveDAO)
        {
            Resource<AgeUsuariosPerfileDAO> ageUsuarioPerfileDAO =
                await _service.Insertar(_httpContextAccessor, ageUsuarioPerfileSaveDAO);

            return Ok(ageUsuarioPerfileDAO);
        }

        [HttpPost("varios")]
        [ProducesResponseType(typeof(List<Resource<AgeUsuariosPerfileDAO>>), 200)]
        public async Task<IActionResult> InsertarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeUsuariosPerfileSaveDAO> ageUsuariosPerfileSaveDAOList)
        {
            List<Resource<AgeUsuariosPerfileDAO>> ageUsuariosPerfileDAOList =
                await _service.InsertarVarios(_httpContextAccessor, ageUsuariosPerfileSaveDAOList);

            return Ok(ageUsuariosPerfileDAOList);
        }

        [HttpPut]
        [ProducesResponseType(typeof(AgeUsuariosPerfileDAO), 200)]
        public async Task<IActionResult> Actualizar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeUsuariosPerfileSaveDAO ageUsuariosPerfileSaveDAO)
        {
            AgeUsuariosPerfileDAO ageUsuariosPerfileDAO = await _service.Actualizar(_httpContextAccessor, ageUsuariosPerfileSaveDAO);
            return Ok(ageUsuariosPerfileDAO);
        }


        [HttpPut("varios")]
        [ProducesResponseType(typeof(List<AgeUsuariosPerfileDAO>), 200)]
        public async Task<IActionResult> ActualizarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeUsuariosPerfileSaveDAO> ageUsuariosPerfileSaveDAOList)
        {
            List<AgeUsuariosPerfileDAO> ageUsuariosPerfileDAOList =
                await _service.ActualizarVarios(_httpContextAccessor, ageUsuariosPerfileSaveDAOList);

            return Ok(ageUsuariosPerfileDAOList);
        }
    }
}
