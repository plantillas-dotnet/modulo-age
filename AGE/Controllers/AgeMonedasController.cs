﻿
using AGE.Entities.DAO.AgeMonedas;
using AGE.Services.AgeMonedasService;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace AGE.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class AgeMonedasController : Controller
    {
        private readonly IAgeMonedasService _service;

        public AgeMonedasController(IAgeMonedasService Service)
        {
            _service = Service;
        }

        [HttpGet]
        [ProducesResponseType(typeof(Page<AgeMonedasDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarTodos(
            [FromQuery] int? codigo,
            [FromQuery] string? descripcion,
            [FromQuery] string? codigoExterno,
            [FromQuery] string? monedaSeparadorDecimal,
            [FromQuery] string? monedaSimbolo,
            [FromQuery] Pageable pageable)
        {
            Page<AgeMonedasDAO> ageMonedasDAOList =
                await _service.ConsultarTodos(codigo ?? 0, descripcion ?? "", codigoExterno ?? "", 
                monedaSeparadorDecimal ?? "", monedaSimbolo ?? "", pageable);

            return Ok(ageMonedasDAOList);
        }

        [HttpPut]
        [ProducesResponseType(typeof(AgeMonedasDAO), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> Actualizar(
            [FromServices] IHttpContextAccessor httpContextAccessor,
            [FromBody] AgeMonedasSaveDAO ageMonedasSaveDAO)
        {
            AgeMonedasDAO ageMonedasDAO = await _service.Actualizar(httpContextAccessor, ageMonedasSaveDAO);
            return Ok(ageMonedasDAO);
        }

        [HttpPost]
        [ProducesResponseType(typeof(Resource<AgeMonedasDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> Insertar(
            [FromServices] IHttpContextAccessor httpContextAccessor,
            [FromBody] AgeMonedasSaveDAO ageMonedasSaveDAO)
        {
            Resource<AgeMonedasDAO> ageMonedasDAO = await _service.Insertar(httpContextAccessor, ageMonedasSaveDAO);
            return Ok(ageMonedasDAO);
        }

        [HttpPut("varios")]
        [ProducesResponseType(typeof(List<AgeMonedasDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ActualizarVarios(
            [FromServices] IHttpContextAccessor httpContextAccessor,
            [FromBody] List<AgeMonedasSaveDAO> ageMonedasSaveDAOList)
        {
            List<AgeMonedasDAO> ageMonedasDAOList = await _service.ActualizarVarios(httpContextAccessor, ageMonedasSaveDAOList);
            return Ok(ageMonedasDAOList);
        }

        [HttpPost("varios")]
        [ProducesResponseType(typeof(List<Resource<AgeMonedasDAO>>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> InsertarVarios(
            [FromServices] IHttpContextAccessor httpContextAccessor,
            [FromBody] List<AgeMonedasSaveDAO> ageMonedasSaveDAOList)
        {
            List<Resource<AgeMonedasDAO>> ageMonedasDAOList = await _service.InsertarVarios(httpContextAccessor, ageMonedasSaveDAOList);
            return Ok(ageMonedasDAOList);
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(AgeMonedasDAO), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarPorId(
            [FromRoute, Required] int id)
        {
            AgeMonedasDAO ageMonedasDAO = await _service.ConsultarPorId(id);
            return Ok(ageMonedasDAO);
        }

        [HttpGet("filtro")]
        [ProducesResponseType(typeof(Page<AgeMonedasDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarListaFiltro(
            [FromQuery, Required] string filtro,
            [FromQuery] Pageable pageable)
        {
            Page<AgeMonedasDAO> ageMonedasDAOList =
                await _service.ConsultarListaFiltro(filtro, pageable);

            return Ok(ageMonedasDAOList);
        }

    }
}
