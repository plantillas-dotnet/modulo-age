﻿using AGE.Entities.DAO.AgeLogErrores;
using AGE.Services.AgeLogErroresService;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace AGE.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class AgeLogErroresController : ControllerBase
    {
        private readonly IAgeLogErroresService _service;

        public AgeLogErroresController(IAgeLogErroresService service)
        {
            _service = service;
        }

        [HttpGet("{ageLicApAgeLicencCodigo}")]
        [ProducesResponseType(typeof(Page<AgeLogErroresDAO>), 200)]
        public async Task<IActionResult> ConsultarTodos(
            [FromRoute] int ageLicApAgeLicencCodigo,
            [FromQuery] int? ageLicApAgeAplicaCodigo,
            [FromQuery] int? codigo,
            [FromQuery] string? mensaje,
            [FromQuery] DateTime? fechaDesde,
            [FromQuery] DateTime? fechaHasta,
            [FromQuery] Pageable pageable)
        {
            Page<AgeLogErroresDAO> ageLogErroresDAOList =
                await _service.ConsultarTodos(ageLicApAgeLicencCodigo, ageLicApAgeAplicaCodigo ?? 0, codigo ?? 0, fechaDesde ?? null, fechaHasta ?? null, mensaje ?? "",  pageable);

            return Ok(ageLogErroresDAOList);
        }

        [HttpGet("{ageLicApAgeLicencCodigo}/{ageLicApAgeAplicaCodigo}/{codigo}")]
        [ProducesResponseType(typeof(AgeLogErroresDAO), 200)]
        public async Task<IActionResult> ConsultarPorId(
            [FromRoute] AgeLogErroresPKDAO ageLogErroresPKDAP)
        {
            var ageLogErroresDAO = await _service.ConsultarPorId(ageLogErroresPKDAP);
            return Ok(ageLogErroresDAO);
        }

        [HttpGet("filtro/{ageUsuariAgeLicencCodigo}")]
        [ProducesResponseType(typeof(Page<AgeLogErroresDAO>), 200)]
        public async Task<IActionResult> ConsultarListaFiltro(
           [FromRoute, Required] int ageUsuariAgeLicencCodigo,
           [FromQuery, Required] string filtro,
           [FromQuery] Pageable pageable)
        {
            Page<AgeLogErroresDAO> ageLogErroresDAO =
                await _service.ConsultarListaFiltro(ageUsuariAgeLicencCodigo, filtro, pageable);

            return Ok(ageLogErroresDAO);
        }

        [HttpPost]
        [ProducesResponseType(typeof(Resource<AgeLogErroresDAO>), 200)]
        public async Task<IActionResult> Insertar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeLogErroresSaveDAO ageLogErroresSave)
        {
            Resource<AgeLogErroresDAO> ageLogErroresDAO =
                await _service.Insertar(_httpContextAccessor, ageLogErroresSave);

            return Ok(ageLogErroresDAO);
        }


        [HttpPost("varios")]
        [ProducesResponseType(typeof(List<Resource<AgeLogErroresDAO>>), 200)]
        public async Task<IActionResult> InsertarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeLogErroresSaveDAO> ageLogErroresSaveDAOList)
        {
            List<Resource<AgeLogErroresDAO>> ageLogErroresDAOList =
                await _service.InsertarVarios(_httpContextAccessor, ageLogErroresSaveDAOList);

            return Ok(ageLogErroresDAOList);
        }

        [HttpPut]
        [ProducesResponseType(typeof(AgeLogErroresDAO), 200)]
        public async Task<IActionResult> Actualizar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeLogErroresSaveDAO ageLogErroresSaveDAO)
        {
            AgeLogErroresDAO ageLogErroresDAO =
                await _service.Actualizar(_httpContextAccessor, ageLogErroresSaveDAO);

            return Ok(ageLogErroresDAO);
        }


        [HttpPut("varios")]
        [ProducesResponseType(typeof(List<AgeLogErroresDAO>), 200)]
        public async Task<IActionResult> ActualizarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeLogErroresSaveDAO> ageLogErroresSaveDAOList)
        {
            List<AgeLogErroresDAO> ageLogErroresDAOList =
                await _service.ActualizarVarios(_httpContextAccessor, ageLogErroresSaveDAOList);

            return Ok(ageLogErroresDAOList);
        }
    }
}
