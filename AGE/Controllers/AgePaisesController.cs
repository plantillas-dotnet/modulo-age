﻿
using AGE.Entities.DAO.AgePaises;
using AGE.Services.AgePaisesService;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace AGE.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class AgePaisesController : Controller
    {
        private readonly IAgePaisesService _service;

        public AgePaisesController(IAgePaisesService Service)
        {
            _service = Service;
        }

        [HttpGet]
        [ProducesResponseType(typeof(Page<AgePaisesDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarTodos(
            [FromQuery] int? codigo,
            [FromQuery] string? descripcion,
            [FromQuery] Pageable pageable)
        {
            Page<AgePaisesDAO> agePaisesDAOList = 
                await _service.ConsultarTodos(codigo ?? 0, descripcion ?? "", pageable);

            return Ok(agePaisesDAOList);
        }


        [HttpGet("filtro")]
        [ProducesResponseType(typeof(Page<AgePaisesDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarListaFiltro(
            [FromQuery, Required] string filtro,
            [FromQuery] Pageable pageable)
        {
            Page<AgePaisesDAO> agePaisesDAOList =
                await _service.ConsultarListaFiltro(filtro, pageable);

            return Ok(agePaisesDAOList);
        }


        [HttpGet("{id}")]
        [ProducesResponseType(typeof(AgePaisesDAO), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarPorId(
            [FromRoute, Required] int id)
        {
            AgePaisesDAO agePaisesDAO = await _service.ConsultarPorId(id);
            return Ok(agePaisesDAO);
        }

        

        [HttpPost]
        [ProducesResponseType(typeof(Resource<AgePaisesDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> Insertar(
            [FromServices] IHttpContextAccessor httpContextAccessor, 
            [FromBody] AgePaisesSaveDAO agePaisesSaveDAO)
        {
            Resource<AgePaisesDAO> agePaisesDAO = await _service.Insertar(httpContextAccessor, agePaisesSaveDAO);
            return Ok(agePaisesDAO);
        }


        [HttpPost("varios")]
        [ProducesResponseType(typeof(List<Resource<AgePaisesDAO>>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> InsertarVarios(
            [FromServices] IHttpContextAccessor httpContextAccessor,
            [FromBody] List<AgePaisesSaveDAO> agePaisesSaveDAOList)
        {
            List<Resource<AgePaisesDAO>> agePaisesDAOList = await _service.InsertarVarios(httpContextAccessor, agePaisesSaveDAOList);
            return Ok(agePaisesDAOList);
        }


        [HttpPut]
        [ProducesResponseType(typeof(AgePaisesDAO), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> Actualizar(
            [FromServices] IHttpContextAccessor httpContextAccessor,
            [FromBody] AgePaisesSaveDAO agePaisesSaveDAO)
        {
            AgePaisesDAO agePaisesDAO = await _service.Actualizar(httpContextAccessor, agePaisesSaveDAO);
            return Ok(agePaisesDAO);
        }


        [HttpPut("varios")]
        [ProducesResponseType(typeof(List<AgePaisesDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ActualizarVarios(
            [FromServices] IHttpContextAccessor httpContextAccessor,
            [FromBody] List<AgePaisesSaveDAO> agePaisesSaveDAOList)
        {
            List<AgePaisesDAO> agePaisesDAOList = await _service.ActualizarVarios(httpContextAccessor, agePaisesSaveDAOList);
            return Ok(agePaisesDAOList);
        }

    }
}
