using AGE.Entities.DAO.AgeTiposDireccione;
using AGE.Services.AgeTiposDireccioneService;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace AGE.Controllers
{
    [Route("[controller]")]
    [ApiController]
    //[Authorize]
    public class AgeTiposDireccionesController : ControllerBase
    {
        private readonly IAgeTiposDireccionesService _service;

        public AgeTiposDireccionesController(IAgeTiposDireccionesService Service)
        {
            _service = Service;
        }

        [HttpGet]
        [ProducesResponseType(typeof(Page<AgeTiposDireccionesDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarTodos(
            [FromQuery] int? codigo,
            [FromQuery] string? descripcion,
            [FromQuery] Pageable pageable)
        {
            Page<AgeTiposDireccionesDAO> ageTiposDireccioneDAOList =
                await _service.ConsultarTodos(codigo ?? 0, descripcion ?? "", pageable);

            return Ok(ageTiposDireccioneDAOList);
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(AgeTiposDireccionesDAO), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarPorId(
            [FromRoute] int id)
        {
            AgeTiposDireccionesDAO ageTiposIdentificacioneDAO = await _service.ConsultarPorId(id);
            return Ok(ageTiposIdentificacioneDAO);
        }

        [HttpGet("filtro")]
        [ProducesResponseType(typeof(Page<AgeTiposDireccionesDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarListaFiltro(
            [FromQuery, Required] string filtro,
            [FromQuery] Pageable pageable)
        {
            Page<AgeTiposDireccionesDAO> ageTiposDireccioneDAOList =
                await _service.ConsultarListaFiltro(filtro, pageable);

            return Ok(ageTiposDireccioneDAOList);
        }

        [HttpPost]
        [ProducesResponseType(typeof(Resource<AgeTiposDireccionesDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> Insertar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeTiposDireccionesSaveDAO ageTiposDireccioneSaveDAO)
        {
            Resource<AgeTiposDireccionesDAO> ageTiposDireccioneDAO = await _service.Insertar(
                _httpContextAccessor, ageTiposDireccioneSaveDAO);
            return Ok(ageTiposDireccioneDAO);
        }


        [HttpPost("varios")]
        [ProducesResponseType(typeof(List<Resource<AgeTiposDireccionesDAO>>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> InsertarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeTiposDireccionesSaveDAO> ageTiposDireccioneSaveDAOList)
        {
            List<Resource<AgeTiposDireccionesDAO>> ageTiposDireccioneDAOList =
                await _service.InsertarVarios(_httpContextAccessor, ageTiposDireccioneSaveDAOList);

            return Ok(ageTiposDireccioneDAOList);
        }


        [HttpPut]
        [ProducesResponseType(typeof(AgeTiposDireccionesDAO), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> Actualizar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeTiposDireccionesSaveDAO ageTiposDireccioneSaveDAO)
        {
            AgeTiposDireccionesDAO ageTiposDireccioneDAO = await _service.Actualizar(_httpContextAccessor, ageTiposDireccioneSaveDAO);
            return Ok(ageTiposDireccioneDAO);
        }


        [HttpPut("varios")]
        [ProducesResponseType(typeof(List<AgeTiposDireccionesDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ActualizarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeTiposDireccionesSaveDAO> ageTiposDireccioneSaveDAOList)
        {
            List<AgeTiposDireccionesDAO> ageTiposDireccioneDAOList =
                await _service.ActualizarVarios(_httpContextAccessor, ageTiposDireccioneSaveDAOList);

            return Ok(ageTiposDireccioneDAOList);
        }
    }

}

