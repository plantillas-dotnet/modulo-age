﻿using AGE.Entities.DAO.AgeSubAplicaciones;
using AGE.Services.AgeSubAplicacionesService;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace AGE.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class AgeSubAplicacionesController : ControllerBase
    {
        private readonly IAgeSubAplicacionesService _service;

        public AgeSubAplicacionesController(IAgeSubAplicacionesService Service)
        {
            _service = Service;
        }

        [HttpGet("{codigoLicenciatario}")]
        [ProducesResponseType(typeof(Page<AgeSubAplicacionesDAO>), 200)]
        public async Task<IActionResult> ConsultarTodos([FromRoute] int codigoLicenciatario,
                                                        [FromQuery] int codigo,
                                                        [FromQuery] int ageLicApAgeAplicaCodigo,
                                                        [FromQuery] string contabiliza,
                                                        [FromQuery] string descripcion,
                                                        [FromQuery] Pageable pageable)
        {
            Page<AgeSubAplicacionesDAO> ageSubAplicacionesDAOList =
                await _service.ConsultarTodos(codigoLicenciatario,codigo, ageLicApAgeAplicaCodigo,contabiliza,descripcion, pageable);

            return Ok(ageSubAplicacionesDAOList);
        }

        [HttpGet("{codigoLicenciatario}/{codigoAplicacion}/{id}")]
        [ProducesResponseType(typeof(AgeSubAplicacionesDAO), 200)]
        public async Task<IActionResult> ConsultarPorId(
            [FromRoute] AgeSubAplicacionesPKDAO ageSubAplicacionesPKDAO)
        {
            var ageSubAplicacionesDAO = await _service.ConsultarPorId(ageSubAplicacionesPKDAO);
            return Ok(ageSubAplicacionesDAO);
        }

        [HttpGet("filtro/{codigoLicenciatario}")]
        [ProducesResponseType(typeof(Page<AgeSubAplicacionesDAO>), 200)]
        public async Task<IActionResult> ConsultarListaFiltro(
           [FromRoute, Required] int codigoLicenciatario,
           [FromQuery, Required] string filtro,
           [FromQuery] Pageable pageable)
        {
            Page<AgeSubAplicacionesDAO> ageSubAplicacionesDAO =
                await _service.ConsultarListaFiltro(codigoLicenciatario, filtro, pageable);

            return Ok(ageSubAplicacionesDAO);
        }

        [HttpPost]
        [ProducesResponseType(typeof(Resource<AgeSubAplicacionesDAO>), 200)]
        public async Task<IActionResult> Insertar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeSubAplicacionesSaveDAO ageSubAplicacionesSaveDAO)
        {
            Resource<AgeSubAplicacionesDAO> ageSubAplicacionesDAO =
                await _service.Insertar(_httpContextAccessor, ageSubAplicacionesSaveDAO);

            return Ok(ageSubAplicacionesDAO);
        }

        [HttpPost("varios")]
        [ProducesResponseType(typeof(List<Resource<AgeSubAplicacionesDAO>>), 200)]
        public async Task<IActionResult> InsertarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeSubAplicacionesSaveDAO> ageSubAplicacionesSaveDAOList)
        {
            List<Resource<AgeSubAplicacionesDAO>> ageSubAplicacionesDAOList =
                await _service.InsertarVarios(_httpContextAccessor, ageSubAplicacionesSaveDAOList);

            return Ok(ageSubAplicacionesDAOList);
        }

        [HttpPut]
        [ProducesResponseType(typeof(AgeSubAplicacionesDAO), 200)]
        public async Task<IActionResult> Actualizar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeSubAplicacionesSaveDAO ageSubAplicacionesSaveDAO)
        {
            AgeSubAplicacionesDAO ageSubAplicacionesDAO = await _service.Actualizar(_httpContextAccessor, ageSubAplicacionesSaveDAO);
            return Ok(ageSubAplicacionesDAO);
        }


        [HttpPut("varios")]
        [ProducesResponseType(typeof(List<AgeSubAplicacionesDAO>), 200)]
        public async Task<IActionResult> ActualizarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeSubAplicacionesSaveDAO> ageSubAplicacionesSaveDAOList)
        {
            List<AgeSubAplicacionesDAO> ageSubAplicacionesDAOList =
                await _service.ActualizarVarios(_httpContextAccessor, ageSubAplicacionesSaveDAOList);

            return Ok(ageSubAplicacionesDAOList);
        }

    }
}
