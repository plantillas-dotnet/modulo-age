using AGE.Entities.DAO.AgePaisesMonedas;
using AGE.Services.AgePaisesMonedasService;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace AGE.Controllers
{
    [Route("[controller]")]
    [ApiController]
    //[Authorize]
    public class AgePaisesMonedasController : ControllerBase
    {
        private readonly IAgePaisesMonedasService _service;

        public AgePaisesMonedasController(IAgePaisesMonedasService service)
        {
            _service = service;
        }

        [HttpGet]
        [ProducesResponseType(typeof(Page<AgePaisesMonedasDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarTodos(
            [FromQuery] int? codigoMoneda,
            [FromQuery] int? codigoPais,
            [FromQuery] string? descripcion,
            [FromQuery] Pageable pageable)
        {
            Page<AgePaisesMonedasDAO> agePaisesMonedasDAOList =
                await _service.ConsultarTodos(codigoMoneda ?? 0, codigoPais ?? 0,
                                              descripcion ?? "", pageable);

            return Ok(agePaisesMonedasDAOList);
        }

        [HttpPut]
        [ProducesResponseType(typeof(AgePaisesMonedasDAO), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> Actualizar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgePaisesMonedasSaveDAO agePaisesMonedasSaveDAO)
        {
            AgePaisesMonedasDAO agePaisesMonedasDAO = await _service.Actualizar(
                _httpContextAccessor, agePaisesMonedasSaveDAO);
            return Ok(agePaisesMonedasDAO);
        }

        [HttpPost]
        [ProducesResponseType(typeof(Resource<AgePaisesMonedasDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> Insertar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgePaisesMonedasSaveDAO ageTiposLocalidadeSaveDAO)
        {
            Resource<AgePaisesMonedasDAO> agePaisesMonedasDAO =
                await _service.Insertar(_httpContextAccessor, ageTiposLocalidadeSaveDAO);
            return Ok(agePaisesMonedasDAO);
        }


        [HttpPut("varios")]
        [ProducesResponseType(typeof(List<AgePaisesMonedasDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ActualizarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgePaisesMonedasSaveDAO> agePaisesMonedasSaveDAOList)
        {
            List<AgePaisesMonedasDAO> agePaisesMonedasDAOList =
                await _service.ActualizarVarios(_httpContextAccessor, agePaisesMonedasSaveDAOList);

            return Ok(agePaisesMonedasDAOList);
        }

        [HttpPost("varios")]
        [ProducesResponseType(typeof(List<Resource<AgePaisesMonedasDAO>>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> InsertarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgePaisesMonedasSaveDAO> agePaisesMonedasSaveDAOList)
        {
            List<Resource<AgePaisesMonedasDAO>> agePaisesMonedasDAOList =
                await _service.InsertarVarios(_httpContextAccessor, agePaisesMonedasSaveDAOList);

            return Ok(agePaisesMonedasDAOList);
        }

        [HttpGet("{codigoMoneda}/{codigoPais}")]
        [ProducesResponseType(typeof(AgePaisesMonedasDAO), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarPorId(
            [FromRoute] AgePaisesMonedasPKDAO agePaisesMonedasPKDAO)
        {
            AgePaisesMonedasDAO agePaisesMonedasDAO = await _service.ConsultarPorId(agePaisesMonedasPKDAO);
            return Ok(agePaisesMonedasDAO);
        }

        [HttpGet("filtro")]
        [ProducesResponseType(typeof(Page<AgePaisesMonedasDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarListaFiltro(
            [FromQuery, Required] string filtro,
            [FromQuery] Pageable pageable)
        {
            Page<AgePaisesMonedasDAO> agePaisesMonedasDAOList =
                await _service.ConsultarListaFiltro(filtro, pageable);

            return Ok(agePaisesMonedasDAOList);
        }
    }
}
