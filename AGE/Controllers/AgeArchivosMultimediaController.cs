﻿
using AGE.Entities.DAO.AgeArchivosMultimedia;
using AGE.Entities.DAO.AgeRutas;
using AGE.Services.AgeArchivosMultimediaService;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace AGE.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class AgeArchivosMultimediaController : ControllerBase
    {
        private readonly IAgeArchivosMultimediaService _service;

        public AgeArchivosMultimediaController(IAgeArchivosMultimediaService Service)
        {
            _service = Service;
        }

        [HttpPut]
        [ProducesResponseType(typeof(AgeRutasDAO), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> Actualizar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeArchivosMultimediaSaveDAO ageArchivosMultimediaSaveDAO)
        {
            AgeArchivosMultimediaDAO ageArchivosMultimediaDAO = await _service.Actualizar(_httpContextAccessor, ageArchivosMultimediaSaveDAO);
            return Ok(ageArchivosMultimediaDAO);
        }

        [HttpPost]
        [ProducesResponseType(typeof(Resource<AgeArchivosMultimediaDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> Insertar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeArchivosMultimediaSaveDAO ageArchivosMultimediaSaveDAO)
        {
            
            Resource<AgeArchivosMultimediaDAO> ageArchivosMultimediaDAO = await _service.Insertar(_httpContextAccessor, ageArchivosMultimediaSaveDAO);
            return Ok(ageArchivosMultimediaDAO);
            
        }

        [HttpPut("varios")]
        [ProducesResponseType(typeof(List<AgeArchivosMultimediaDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ActualizarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeArchivosMultimediaSaveDAO> ageArchivosMultimediaSaveDAOList)
        {
            List<AgeArchivosMultimediaDAO> ageArchivosMultimediaDAOList =
                await _service.ActualizarVarios(_httpContextAccessor, ageArchivosMultimediaSaveDAOList);

            return Ok(ageArchivosMultimediaDAOList);
        }

        [HttpPost("varios")]
        [ProducesResponseType(typeof(List<Resource<AgeArchivosMultimediaDAO>>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> InsertarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeArchivosMultimediaSaveDAO> ageArchivosMulrimediaSaveDAOList)
        {
            List<Resource<AgeArchivosMultimediaDAO>> ageArchivosMultimediaDAOList =
                await _service.InsertarVarios(_httpContextAccessor, ageArchivosMulrimediaSaveDAOList);

            return Ok(ageArchivosMultimediaDAOList);
        }

        [HttpGet("{codigoLicenciatario}")]
        [ProducesResponseType(typeof(Page<AgeArchivosMultimediaDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarTodos(
            [FromRoute] int codigoLicenciatario,
            [FromQuery] int? codigo,
            [FromQuery] string? ruta,
            [FromQuery] string? descripcion,
            [FromQuery] string? tipo,
            [FromQuery] Pageable pageable)
        {
            Page<AgeArchivosMultimediaDAO> ageArchivosMultimediaList =
                await _service.ConsultarTodos(codigoLicenciatario, codigo ?? 0, ruta ?? "", descripcion ?? "", tipo ?? "", pageable);

            return Ok(ageArchivosMultimediaList);
        }

        [HttpGet("{codigoLicenciatario}/{id}")]
        [ProducesResponseType(typeof(AgeArchivosMultimediaDAO), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarPorId(
            [FromRoute] AgeArchivosMultimediaPKDAO ageArchivosMultimediaPKDAO)
        {
            AgeArchivosMultimediaDAO ageArchivosMultimediaDAO = await _service.ConsultarPorId(ageArchivosMultimediaPKDAO);
            return Ok(ageArchivosMultimediaDAO);
        }

        [HttpGet("filtro/{codigoLicenciatario}")]
        [ProducesResponseType(typeof(Page<AgeArchivosMultimediaDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarListaFiltro(
            [FromRoute] int codigoLicenciatario,
            [FromQuery, Required] string filtro,
            [FromQuery] Pageable pageable)
        {
            Page<AgeArchivosMultimediaDAO> ageArchivosMultimediaDAO =
                await _service.ConsultarListaFiltro(codigoLicenciatario, filtro, pageable);

            return Ok(ageArchivosMultimediaDAO);
        }

        [HttpGet("consulta/{codigoLicenciatario}")]
        [ProducesResponseType(typeof(Page<AgeArchivosMultimediaDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarLicenciatario(
        [FromRoute] int codigoLicenciatario,
        [FromQuery] int? codigo,
        [FromQuery] string? ruta,
        [FromQuery] string? descripcion,
        [FromQuery] string? tipo,
        [FromQuery] Pageable pageable)
        {
            Page<AgeArchivosMultimediaDAO> ageArchivosMultimediaList =
                await _service.ConsultarTodos(codigoLicenciatario, codigo ?? 0, ruta ?? "", descripcion ?? "", tipo ?? "", pageable);

            return Ok(ageArchivosMultimediaList);
        }
    }
}
