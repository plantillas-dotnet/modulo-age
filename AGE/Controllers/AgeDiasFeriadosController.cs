﻿using AGE.Entities.DAO.AgeDiasFeriados;
using AGE.Services.AgeDiasFeriadosService;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace AGE.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class AgeDiasFeriadosController : ControllerBase
    {
        private readonly IAgeDiasFeriadosService _service;

        public AgeDiasFeriadosController(IAgeDiasFeriadosService Service)
        {
            _service = Service;
        }

        [HttpGet]
        [ProducesResponseType(typeof(Page<AgeDiasFeriadosDAO>), 200)]
        public async Task<IActionResult> ConsultarTodos(
            [FromQuery] string? descripcion,
            [FromQuery] Pageable pageable)
        {
            Page<AgeDiasFeriadosDAO> ageDiasFeriadosDAOList =
                await _service.ConsultarTodos(descripcion ?? "", pageable);

            return Ok(ageDiasFeriadosDAOList);
        }

        [HttpGet("filtro")]
        [ProducesResponseType(typeof(Page<AgeDiasFeriadosDAO>), 200)]
        public async Task<IActionResult> ConsultarListaFiltro(
            [FromQuery, Required] string filtro,
            [FromQuery] Pageable pageable)
        {
            Page<AgeDiasFeriadosDAO> ageDiasFeriadosDAOList =
                await _service.ConsultarListaFiltro(filtro, pageable);

            return Ok(ageDiasFeriadosDAOList);
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(AgeDiasFeriadosDAO), 200)]
        public async Task<IActionResult> ConsultarPorId(
            [FromRoute, Required] int id)
        {
            AgeDiasFeriadosDAO ageDiasFeriadosDAO = await _service.ConsultarPorId(id);
            return Ok(ageDiasFeriadosDAO);
        }

        [HttpPost]
        [ProducesResponseType(typeof(Resource<AgeDiasFeriadosDAO>), 200)]
        public async Task<IActionResult> Insertar(
            [FromServices] IHttpContextAccessor httpContextAccessor,
            [FromBody] AgeDiasFeriadosSaveDAO ageDiasFeriadosSaveDAO)
        {
            Resource<AgeDiasFeriadosDAO> ageDiasFeriadosDAO = await _service.Insertar(httpContextAccessor, ageDiasFeriadosSaveDAO);
            return Ok(ageDiasFeriadosDAO);
        }

        [HttpPost("varios")]
        [ProducesResponseType(typeof(List<Resource<AgeDiasFeriadosDAO>>), 200)]
        public async Task<IActionResult> InsertarVarios(
            [FromServices] IHttpContextAccessor httpContextAccessor,
            [FromBody] List<AgeDiasFeriadosSaveDAO> ageDiasFeriadosSaveDAOList)
        {
            List<Resource<AgeDiasFeriadosDAO>> ageDiasFeriadosDAOList = await _service.InsertarVarios(httpContextAccessor, ageDiasFeriadosSaveDAOList);
            return Ok(ageDiasFeriadosDAOList);
        }

        [HttpPut]
        [ProducesResponseType(typeof(AgeDiasFeriadosDAO), 200)]
        public async Task<IActionResult> Actualizar(
            [FromServices] IHttpContextAccessor httpContextAccessor,
            [FromBody] AgeDiasFeriadosSaveDAO ageDiasFeriadosSaveDAO)
        {
            AgeDiasFeriadosDAO ageDiasFeriadosDAO = await _service.Actualizar(httpContextAccessor, ageDiasFeriadosSaveDAO);
            return Ok(ageDiasFeriadosDAO);
        }

        [HttpPut("varios")]
        [ProducesResponseType(typeof(List<AgeDiasFeriadosDAO>), 200)]
        public async Task<IActionResult> ActualizarVarios(
            [FromServices] IHttpContextAccessor httpContextAccessor,
            [FromBody] List<AgeDiasFeriadosSaveDAO> ageDiasFeriadosSaveDAOList)
        {
            List<AgeDiasFeriadosDAO> ageDiasFeriadosDAOList = await _service.ActualizarVarios(httpContextAccessor, ageDiasFeriadosSaveDAOList);
            return Ok(ageDiasFeriadosDAOList);
        }
    }
}
