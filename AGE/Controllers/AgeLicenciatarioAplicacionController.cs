using AGE.Entities.DAO.AgeLicenciatarioAplicacion;
using AGE.Services.AgeLicenciatariosAplicacionService;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace AGE.Controllers
{

    [Route("[controller]")]
    [ApiController]
    //[Authorize]
    public class AgeLicenciatarioAplicacionController : ControllerBase
    {

        private readonly IAgeLicenciatarioAplicacionService _service;

        public AgeLicenciatarioAplicacionController(IAgeLicenciatarioAplicacionService Service)
        {
            _service = Service;
        }

        [HttpGet("{codigoLicenciatario}")]
        [ProducesResponseType(typeof(Page<AgeLicenciatariosAplicacionesDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarTodos(
            [FromRoute] int codigoLicenciatario,
            [FromQuery] int? ageAplicaCodigo,
            [FromQuery] int? ageRutaCodigo,
            [FromQuery] Pageable pageable)
        {
            Page<AgeLicenciatariosAplicacionesDAO> ageLicenciatariosAplicacionDAOList =
                await _service.ConsultarTodos(codigoLicenciatario, ageAplicaCodigo ?? 0, ageRutaCodigo ?? 0, pageable);

            return Ok(ageLicenciatariosAplicacionDAOList);
        }


        [HttpGet("{codigoLicenciatario}/{codigoAplicacion}")]
        [ProducesResponseType(typeof(AgeLicenciatariosAplicacionesDAO), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarPorId(
            [FromRoute] AgeLicenciatariosAplicacionesPKDAO ageLicenciatariosAplicacionPKDAO)
        {
            AgeLicenciatariosAplicacionesDAO ageLicenciatariosAplicacionDAO =
                await _service.ConsultarPorId(ageLicenciatariosAplicacionPKDAO);

            return Ok(ageLicenciatariosAplicacionDAO);
        }



        [HttpGet("filtro/{codigoLicenciatario}")]
        [ProducesResponseType(typeof(Page<AgeLicenciatariosAplicacionesDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ConsultarListaFiltro(
            [FromRoute] int codigoLicenciatario,
            [FromQuery, Required] string filtro,
            [FromQuery] Pageable pageable)
        {
            Page<AgeLicenciatariosAplicacionesDAO> ageLicenciatariosAplicacionDAOList =
                await _service.ConsultarListaFiltro(codigoLicenciatario, filtro, pageable);

            return Ok(ageLicenciatariosAplicacionDAOList);
        }


        [HttpPost]
        [ProducesResponseType(typeof(Resource<AgeLicenciatariosAplicacionesDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> Insertar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeLicenciatariosAplicacionesSaveDAO ageLicenciatariosAplicacionSaveDAO)
        {
            Resource<AgeLicenciatariosAplicacionesDAO> ageLicenciatariosAplicacionDAO =
                await _service.Insertar(_httpContextAccessor, ageLicenciatariosAplicacionSaveDAO);

            return Ok(ageLicenciatariosAplicacionDAO);
        }


        [HttpPost("varios")]
        [ProducesResponseType(typeof(List<Resource<AgeLicenciatariosAplicacionesDAO>>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> InsertarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeLicenciatariosAplicacionesSaveDAO> ageLicenciatariosAplicacionSaveDAOList)
        {
            List<Resource<AgeLicenciatariosAplicacionesDAO>> ageLicenciatariosAplicacionDAOList =
                await _service.InsertarVarios(_httpContextAccessor, ageLicenciatariosAplicacionSaveDAOList);

            return Ok(ageLicenciatariosAplicacionDAOList);
        }


        [HttpPut]
        [ProducesResponseType(typeof(AgeLicenciatariosAplicacionesDAO), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> Actualizar(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] AgeLicenciatariosAplicacionesSaveDAO ageLicenciatariosAplicacionSaveDAO)
        {
            AgeLicenciatariosAplicacionesDAO ageLicenciatariosAplicacionDAO =
                await _service.Actualizar(_httpContextAccessor, ageLicenciatariosAplicacionSaveDAO);

            return Ok(ageLicenciatariosAplicacionDAO);
        }


        [HttpPut("varios")]
        [ProducesResponseType(typeof(List<AgeLicenciatariosAplicacionesDAO>), 200)]
        //[Authorize(Roles = "Empleado")]
        public async Task<IActionResult> ActualizarVarios(
            [FromServices] IHttpContextAccessor _httpContextAccessor,
            [FromBody] List<AgeLicenciatariosAplicacionesSaveDAO> ageLicenciatariosAplicacionSaveDAOList)
        {
            List<AgeLicenciatariosAplicacionesDAO> ageLicenciatariosAplicacionDAOList =
                await _service.ActualizarVarios(_httpContextAccessor, ageLicenciatariosAplicacionSaveDAOList);

            return Ok(ageLicenciatariosAplicacionDAOList);
        }


    }
}
