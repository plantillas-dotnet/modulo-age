using AGE.Utils;
using Microsoft.EntityFrameworkCore;
using AGE.Middleware;
using Microsoft.AspNetCore.Mvc;
using AGE.Repositories.AgeLicenciatariosAplicaSecuRepository;
using AGE.Services.AgeLicenciatariosAplicaSecuService;
using AGE.Repositories.AgeSecuenciasPrimariasRepository;
using AGE.Services.AgeSecuenciasPrimariasService;
using AGE.Entities;
using Microsoft.AspNetCore.HttpOverrides;
using AGE.Services.AgePaisesService;
using AGE.Repositories.AgePaisesRepository;
using AGE.Services.AgeRutasService;
using AGE.Repositories.AgeRutasRepository;
using AGE.Repositories.AgeUsuarioRepository;
using AGE.Services.AgeUsuarioService;
using AGE.Repositories.AgePerfilesRepository;
using AGE.Services.AgePerfilesService;
using AGE.Repositories.AgeTiposIdentificacioneRepository;
using AGE.Services.AgeTiposIdentificacioneService;
using AGE.Repositories.AgeTiposPersonaRepository;
using AGE.Services.AgeTiposPersonaService;
using AGE.Services.AgeTiposSucursaleService;
using AGE.Repositories.AgeTiposSucursaleRepository;
using AGE.Services.AgeTiposLocalidadeService;
using AGE.Repositories.AgeTiposLocalidadeRepository;
using AGE.Services.AgeTransaccioneService;
using AGE.Repositories.AgeTransaccioneRepository;
using AGE.Services.AgePerfilesTransaccioneService;
using AGE.Repositories.AgePerfilesTransaccioneRepository;
using AGE.Services.AgePuntosEmisionService;
using AGE.Repositories.AgePuntosEmisionRepository;
using AGE.Services.AgePaisesIdiomaService;
using AGE.Repositories.AgePaisesIdiomaRepository;
using AGE.Services.AgeIdiomaService;
using AGE.Repositories.AgeIdiomaRepository;
using AGE.Repositories.AgeTransaccionesIdiomaRepository;
using AGE.Services.AgeTransaccionesIdiomaService;
using AGE.Services.AgeTiposDireccioneService;
using AGE.Repositories.AgeTiposDireccioneRepository;
using AGE.Services.AgeArchivosMultimediaService;
using AGE.Repositories.AgeArchivosMultimediaRepository;
using AGE.Services.AgeUsuariosPerfileService;
using AGE.Repositories.AgeUsuariosPerfileRepository;
using AGE.Services.AgeUnidadesTiempoService;
using AGE.Repositories.AgeUnidadesTiempoRepository;
using AGE.Services.AgeAplicacionesService;
using AGE.Repositories.AgeAplicacionesRepository;
using AGE.Services.AgeAplicacionesDependenciasService;
using AGE.Repositories.AgeAplicacionesDependenciasRepository;
using AGE.Services.AgeSistemasService;
using AGE.Repositories.AgeSistemasRepository;
using AGE.Services.AgeUsuariosPuntoEmisionService;
using AGE.Repositories.AgeUsuariosPuntoEmisionRepository;
using AGE.Services.AgeSubAplicacionesService;
using AGE.Repositories.AgeSubAplicacionesRepository;
using AGE.Services.AgeLicenciatariosService;
using AGE.Repositories.AgeLicenciatariosRepository;
using AGE.Services.AgePersonasService;
using AGE.Repositories.AgePersonasRepository;
using AGE.Repositories.AgeParametrosGeneralesRepository;
using AGE.Services.AgeParametrosGeneralesService;
using AGE.Services.AgeParamGeneralVigenciasService;
using AGE.Repositories.AgeParamGeneralVigenciasRepository;
using AGE.Repositories.AgeMonedasRepository;
using AGE.Services.AgeMonedasService;
using AGE.Repositories.AgePaisesMonedasRepository;
using AGE.Services.AgePaisesMonedasService;
using AGE.Services.AgeDireccionesService;
using AGE.Repositories.AgeDireccionesRepository;
using AGE.Services.AgeMonedasCotizacionesService;
using AGE.Repositories.AgeMonedasCotizacionesRepository;
using AGE.Services.AgeSistemasAplicacionesService;
using AGE.Repositories.AgeSistemasAplicacionesRepository;
using AGE.Services.AgeLocalidadesService;
using AGE.Repositories.AgeLocalidadesRepository;
using AGE.Services.AgeModalidadesService;
using AGE.Repositories.AgeModalidadesRepository;
using AGE.Services.AgeLogUsuariosService;
using AGE.Repositories.AgeLogUsuariosRepository;
using AGE.Services.AgeLogErroresService;
using AGE.Repositories.AgeLogErroresRepository;
using AGE.Services.AgeFavoritosService;
using AGE.Repositories.AgeFavoritosRepository;
using AGE.Services.AgeFranquiciasService;
using AGE.Repositories.AgeFranquiciasRepository;
using AGE.Services.AgeDepartamentosService;
using AGE.Repositories.AgeDepartamentosRepository;
using AGE.Services.AgeDiasFeriadosService;
using AGE.Repositories.AgeDiasFeriadosRepository;
using AGE.Repositories.AgeMensajesRepository;
using AGE.Services.AgeMensajesService;
using AGE.Repositories.AgeMensajesIdiomasRepository;
using AGE.Services.AgeMensajesIdiomasService;
using AGE.Repositories.AgeSucursalesRepository;
using AGE.Services.AgeSucursalesService;
using AGE.Services.AgeLicenciatariosAplicacionService;
using AGE.Repositories.AgeLicenciatariosAplicacionRepository;
using AGE.Repositories.AgeInstitucionesFinancierasRepository;
using AGE.Services.AgeInstitucionesFinancierasService;
using AGE.Repositories.AgeFormasPagoRepository;
using AGE.Services.AgeFormasPagoService;
using AGE.Repositories.AgeFirmasDigitalesRepository;
using AGE.Services.AgeFirmasDigitalesService;
using AGE.Repositories.AgeFeriadosExcepcionesRepository;
using AGE.Services.AgeFeriadosExcepcionesService;
using AGE.Repositories.AgeFormasPagosInstFinaRepository;
using AGE.Services.AgeFormasPagosInstFinaService;
using AGE.Repositories.AgeFormasPagosInstFinasInstFinaRepository;
using AGE.Services.AgeFormasPagoInstFinaService;
using AGE.Repositories.AgeFeriadosPaiseRepository;
using AGE.Services.AgeFeriadosPaiseService;
using AGE.Repositories.AgeFeriadosLocalidadeRepository;
using AGE.Services.AgeFeriadosLocalidadeService;
using AGE.Repositories.AgeClasesContribuyentesRepository;
using AGE.Services.AgeClasesContribuyentesService;
using AGE.Repositories.AgeUsuarioParamVigenciaRepository;
using AGE.Services.AgeUsuarioParamVigenciaService;
using AGE.Repositories.AgeLicenciatParamVigenciaRepository;
using AGE.Services.AgeLicenciatParamVigenciaService;
using AGE.Repositories.AgeLicenciatariosSecuenciasRepository;
using AGE.Services.AgeLicenciatariosSecuenciaService;
using AGE.Repositories.AgeLicenciatariosPaisesRepository;
using AGE.Services.AgeLicenciatariosPaiseService;
using AGE.Firebase.Services.FCMInitializer;
using AGE.Firebase.Services.FCMService;
using AGE.Firebase.Services.PushNotificationService;
using AGE.Repositories.AgeNotificacionesPushRepository;
using AGE.Services.AgeNotificacionesPushService;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddHttpContextAccessor();

var connectionString = Environment.GetEnvironmentVariable(StringHandler.Database);
builder.Services.AddDbContext<DbContextAge>(options =>
    options.UseNpgsql(connectionString)
);


builder.Services.Configure<ApiBehaviorOptions>(options =>
{
    options.SuppressModelStateInvalidFilter = true;
    AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);

});


builder.Services.AddScoped<IAgeLicenciatariosAplicaSecuRepository, AgeLicenciatariosAplicaSecuRepository>();
builder.Services.AddScoped<IAgeLicenciatariosAplicaSecuService, AgeLicenciatariosAplicaSecuService>();

builder.Services.AddScoped<IAgeSecuenciasPrimariasRespository, AgeSecuenciasPrimariasRepository>();
builder.Services.AddScoped<IAgeSecuenciasPrimariasService, AgeSecuenciasPrimariasService>();

builder.Services.AddScoped<IAgePaisesRepository, AgePaisesRepository>();
builder.Services.AddScoped<IAgePaisesService, AgePaisesService>();

builder.Services.AddScoped<IAgeRutasService, AgeRutasService>();
builder.Services.AddScoped<IAgeRutasRepository, AgeRutasRepository>();

builder.Services.AddScoped<IAgeUsuariosService, AgeUsuariosService>();
builder.Services.AddScoped<IAgeUsuariosRepository, AgeUsuariosRepository>();

builder.Services.AddScoped<IAgePerfilesService, AgePerfilesService>();
builder.Services.AddScoped<IAgePerfilesRepository, AgePerfilesRepository>();

builder.Services.AddScoped<IAgeTiposIdentificacionesService, AgeTiposIdentificacionesService>();
builder.Services.AddScoped<IAgeTiposIdentificacionesRepository, AgeTiposIdentificacionesRepository>();

builder.Services.AddScoped<IAgeTiposPersonasService, AgeTiposPersonasService>();
builder.Services.AddScoped<IAgeTiposPersonasRepository,AgeTiposPersonasRepository>();

builder.Services.AddScoped<IAgeTiposSucursalesService, AgeTiposSucursalesService>();
builder.Services.AddScoped<IAgeTiposSucursalesRepository, AgeTiposSucursalesRepository>();

builder.Services.AddScoped<IAgeTiposLocalidadesService, AgeTiposLocalidadesService>();
builder.Services.AddScoped<IAgeTiposLocalidadesRepository, AgeTiposLocalidadesRepository>();

builder.Services.AddScoped<IAgeTransaccionesService, AgeTransaccionesService>();
builder.Services.AddScoped<IAgeTransaccionesRepository, AgeTransaccionesRepository>();
builder.Services.AddScoped<IAgeArchivosMultimediaService, AgeArchivosMultimediaService>();
builder.Services.AddScoped<IAgeArchivosMultimediaRepository,  AgeArchivosMultimediaRepository>();

builder.Services.AddScoped<IAgePerfilesTransaccionesService, AgePerfilesTransaccionesService>();
builder.Services.AddScoped<IAgePerfilesTransaccionesRepository, AgePerfilesTransaccionesRepository>();

builder.Services.AddScoped<IAgePuntosEmisionesService, AgePuntosEmisionesService>();
builder.Services.AddScoped<IAgePuntosEmisionesRepository, AgePuntosEmisionesRepository>();

builder.Services.AddScoped<IAgePaisesIdiomasService, AgePaisesIdiomasService>();
builder.Services.AddScoped<IAgePaisesIdiomasRepository, AgePaisesIdiomasRepository>();

builder.Services.AddScoped<IAgeIdiomasService, AgeIdiomasService>();
builder.Services.AddScoped<IAgeIdiomasRepository, AgeIdiomasRepository>();

builder.Services.AddScoped<IAgeTransaccionesIdiomasService, AgeTransaccionesIdiomasService>();
builder.Services.AddScoped<IAgeTransaccionesIdiomasRepository, AgeTransaccionesIdiomasRepository>();

builder.Services.AddScoped<IAgeTiposDireccionesService, AgeTiposDireccionesService>();
builder.Services.AddScoped<IAgeTiposDireccionesRepository, AgeTiposDireccionesRepository>();


builder.Services.AddScoped<IAgeUsuariosPerfilesService, AgeUsuariosPerfilesService>();
builder.Services.AddScoped<IAgeUsuariosPerfilesRepository, AgeUsuariosPerfilesRepository>();

builder.Services.AddScoped<IAgeUnidadesTiempoService, AgeUnidadesTiempoService>();
builder.Services.AddScoped<IAgeUnidadesTiempoRepository, AgeUnidadesTiempoRepository>();

builder.Services.AddScoped<IAgeAplicacionesService, AgeAplicacionesService>();
builder.Services.AddScoped<IAgeAplicacionesRepository, AgeAplicacionesRepository>();

builder.Services.AddScoped<IAgeAplicacionesDependenciasService, AgeAplicacionesDependenciasService>();
builder.Services.AddScoped<IAgeAplicacionesDependenciasRepository, AgeAplicacionesDependenciasRepository>();

builder.Services.AddScoped<IAgeSistemasService, AgeSistemasService>();
builder.Services.AddScoped<IAgeSistemasRepository, AgeSistemasRepository>();

builder.Services.AddScoped<IAgeUsuariosPuntoEmisionService, AgeUsuariosPuntoEmisionService>();
builder.Services.AddScoped<IAgeUsuariosPuntoEmisionRepository, AgeUsuariosPuntoEmisionRepository>();

builder.Services.AddScoped<IAgeSubAplicacionesService, AgeSubAplicacionesService>();
builder.Services.AddScoped<IAgeSubAplicacionesRepository, AgeSubAplicacionesRepository>();

builder.Services.AddScoped<IAgeLicenciatariosService, AgeLicenciatariosService>();
builder.Services.AddScoped<IAgeLicenciatariosRepository, AgeLicenciatariosRepository>();

builder.Services.AddScoped<IAgePersonasService, AgePersonasService>();
builder.Services.AddScoped<IAgePersonasRepository, AgePersonasRepository>();

builder.Services.AddScoped<IAgeParametrosGeneralesService, AgeParametrosGeneralesService>();
builder.Services.AddScoped<IAgeParametrosGeneralesRepository, AgeParametrosGeneralesRepository>();

builder.Services.AddScoped<IAgeParamGeneralVigenciasService, AgeParamGeneralVigenciasService>();
builder.Services.AddScoped<IAgeParamGeneralVigenciasRepository, AgeParamGeneralVigenciasRepository>();

builder.Services.AddScoped<IAgeMonedasService, AgeMonedasService>();
builder.Services.AddScoped<IAgeMonedasRepository, AgeMonedasRepository>();

builder.Services.AddScoped<IAgePaisesMonedasService, AgePaisesMonedasService>();
builder.Services.AddScoped<IAgePaisesMonedasRepository, AgePaisesMonedasRepository>();

builder.Services.AddScoped<IAgeDireccionesService, AgeDireccionesService>();
builder.Services.AddScoped<IAgeDireccionesRepository, AgeDireccionesRepository>();

builder.Services.AddScoped<IAgeMonedasCotizacionesService, AgeMonedasCotizacionesService>();
builder.Services.AddScoped<IAgeMonedasCotizacionesRepository, AgeMonedasCotizacionesRepository>();

builder.Services.AddScoped<IAgeSistemasAplicacionesService, AgeSistemasAplicacionesService>();
builder.Services.AddScoped<IAgeSistemasAplicacionesRepository, AgeSistemasAplicacionesRepository>();

builder.Services.AddScoped<IAgeLocalidadesService, AgeLocalidadesService>();
builder.Services.AddScoped<IAgeLocalidadesRepository, AgeLocalidadesRepository>();

builder.Services.AddScoped<IAgeModalidadService, AgeModalidadService>();
builder.Services.AddScoped<IAgeModalidadRepository, AgeModalidadRepository>();

builder.Services.AddScoped<IAgeLogUsuariosService, AgeLogUsuariosService>();
builder.Services.AddScoped<IAgeLogUsuariosRepository, AgeLogUsuariosRepository>();

builder.Services.AddScoped<IAgeLogErroresService, AgeLogErroresService>();
builder.Services.AddScoped<IAgeLogErroresRepository, AgeLogErroresRepository>();

builder.Services.AddScoped<IAgeFavoritosService, AgeFavoritosService>();
builder.Services.AddScoped<IAgeFavoritosRepository, AgeFavoritosRepository>();

builder.Services.AddScoped<IAgeFranquiciasService, AgeFranquiciasService>();
builder.Services.AddScoped<IAgeFranquiciasRepository, AgeFranquiciasRepository>();

builder.Services.AddScoped<IAgeDepartamentosService, AgeDepartamentosService>();
builder.Services.AddScoped<IAgeDepartamentosRepository, AgeDepartamentosRepository>();

builder.Services.AddScoped<IAgeDiasFeriadosService, AgeDiasFeriadosService>();
builder.Services.AddScoped<IAgeDiasFeriadosRepository, AgeDiasFeriadosRepository>();

builder.Services.AddScoped<IAgeMensajeRepository, AgeMensajeRepository>();
builder.Services.AddScoped<IAgeMensajeService, AgeMensajeService>();

builder.Services.AddScoped<IAgeMensajesIdiomaRepository, AgeMensajesIdiomaRepository>();
builder.Services.AddScoped<IAgeMensajesIdiomaService, AgeMensajesIdiomaService>();

builder.Services.AddScoped<IAgeSucursalesRepository, AgeSucursalesRepository>();
builder.Services.AddScoped<IAgeSucursalesService, AgeSucursalesService>();

builder.Services.AddScoped<IAgeLicenciatarioAplicacionService, AgeLicenciatarioAplicacionService>();
builder.Services.AddScoped<IAgeLicenciatarioAplicacionRepository, AgeLicenciatarioAplicacionRepository>();

builder.Services.AddScoped<IAgeInstitucionesFinancieraRepository, AgeInstitucionesFinancieraRepository>();
builder.Services.AddScoped<IAgeInstitucionesFinancieraService, AgeInstitucionesFinancieraService>();

builder.Services.AddScoped<IAgeFormasPagosRepository, AgeFormasPagosRepository>();
builder.Services.AddScoped<IAgeFormasPagosService, AgeFormasPagosService>();

builder.Services.AddScoped<IAgeFirmasDigitalesRepository, AgeFirmasDigitalesRepository>();
builder.Services.AddScoped<IAgeFirmasDigitalesService, AgeFirmasDigitalesService>();

builder.Services.AddScoped<IAgeFeriadosExcepcionesRepository, AgeFeriadosExcepcionesRepository>();
builder.Services.AddScoped<IAgeFeriadosExcepcionesService, AgeFeriadosExcepcionesService>();

builder.Services.AddScoped<IAgeFormasPagosInstFinaRepository, AgeFormasPagosInstFinaRepository>();
builder.Services.AddScoped<IAgeFormasPagosInstFinaService, AgeFormasPagosInstFinaService>();

builder.Services.AddScoped<IAgeFeriadosPaisesRepository, AgeFeriadosPaisesRepository>();
builder.Services.AddScoped<IAgeFeriadosPaisesService, AgeFeriadosPaisesService>();

builder.Services.AddScoped<IAgeFeriadosLocalidadesRepository, AgeFeriadosLocalidadesRepository>();
builder.Services.AddScoped<IAgeFeriadosLocalidadesService, AgeFeriadosLocalidadesService>();

builder.Services.AddScoped<IAgeClasesContribuyentesRepository, AgeClasesContribuyentesRepository>();
builder.Services.AddScoped<IAgeClasesContribuyentesService, AgeClasesContribuyentesService>();

builder.Services.AddScoped<IAgeUsuarioParamVigenciasRepository, AgeUsuarioParamVigenciasRepository>();
builder.Services.AddScoped<IAgeUsuarioParamVigenciasService, AgeUsuarioParamVigenciasService>();

builder.Services.AddScoped<IAgeLicenciatParamVigenciasRepository, AgeLicenciatParamVigenciasRepository>();
builder.Services.AddScoped<IAgeLicenciatParamVigenciasService, AgeLicenciatParamVigenciasService>();

builder.Services.AddScoped<IAgeLicenciatariosSecuenciasRepository, AgeLicenciatariosSecuenciasRepository>();
builder.Services.AddScoped<IAgeLicenciatariosSecuenciasService, AgeLicenciatariosSecuenciasService>();

builder.Services.AddScoped<IAgeLicenciatariosPaisesRepository, AgeLicenciatariosPaisesRepository>();
builder.Services.AddScoped<IAgeLicenciatariosPaisesService, AgeLicenciatariosPaisesService>();

builder.Services.AddScoped<IAgeNotificacionesPushRepository, AgeNotificacionesPushRepository>();
builder.Services.AddScoped<IAgeNotificacionesPushService, AgeNotificacionesPushService>();

builder.Services.AddScoped<IFCMInitializer, FCMInitializer>();
builder.Services.AddScoped<IFCMService, FCMService>();
builder.Services.AddScoped<IPushNotificationService, PushNotificationService>();


builder.Services.AddCors(options =>
{
    options.AddDefaultPolicy(
                      builder =>
                      {
                          builder.AllowAnyMethod().AllowAnyOrigin().AllowAnyHeader();
                      });

});
builder.Services.AddHttpContextAccessor();
builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseGlobalExceptionHandler();

app.UseCors();

app.UseHttpsRedirection();

app.UseAuthentication();

app.UseForwardedHeaders(new ForwardedHeadersOptions
{
    ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
});

app.UseAuthorization();

app.MapControllers();

app.Run();

