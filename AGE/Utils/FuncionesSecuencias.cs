﻿using AGE.Entities;
using AGE.Entities.DAO.AgeLicenciatariosAplicaSecu;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Services.AgeLicenciatariosAplicaSecuService;
using AGE.Services.AgeSecuenciasPrimariasService;

namespace AGE.Utils
{
    public class FuncionesSecuencias
    {
        private readonly IAgeLicenciatariosAplicaSecuService _ageLicenciatariosAplicaSecuService;
        private readonly IAgeSecuenciasPrimariasService _ageSecuenciasPrimariasService;

        public FuncionesSecuencias(IAgeSecuenciasPrimariasService ageSecuenciasPrimariasService)
        {
            _ageSecuenciasPrimariasService = ageSecuenciasPrimariasService;
        }

        public FuncionesSecuencias(IAgeLicenciatariosAplicaSecuService ageLicenciatariosAplicaSecuService)
        {
            _ageLicenciatariosAplicaSecuService = ageLicenciatariosAplicaSecuService;
        }

        
        public int ObtenerSecuencia(
            IHttpContextAccessor _httpContextAccessor,
            int ageLicencCodigo,
            int codigoSecuencia,
            long Usuario)
        {

            AgeLicenciatariosAplicaSecuPKDAO ageLicenciatariosAplicaSecuPKDAO = new AgeLicenciatariosAplicaSecuPKDAO
            {
                ageLicencCodigo = ageLicencCodigo,
                ageAplicaCodigo = Globales.CODIGO_APLICACION_AGE,
                codigo = codigoSecuencia
            };

            AgeLicenciatariosAplicaSecu? ageLicenciatariosAplicaSecu = _ageLicenciatariosAplicaSecuService
                    .ConsultarPorSecuencia(ageLicenciatariosAplicaSecuPKDAO).Result;

            if (ageLicenciatariosAplicaSecu == null)
            {
                throw new RegisterNotFoundException($"Secuencia {codigoSecuencia} " +
                    $"con licenciatario {ageLicencCodigo} " +
                    $"para la aplicación {Globales.CODIGO_APLICACION_AGE} no existe.");
            }

            long Id;

            if (ageLicenciatariosAplicaSecu.ValorInicial > ageLicenciatariosAplicaSecu.ValorActual)
                Id = ageLicenciatariosAplicaSecu.ValorInicial;
            else
                Id = ageLicenciatariosAplicaSecu.ValorActual + ageLicenciatariosAplicaSecu.IncrementaEn;


            ageLicenciatariosAplicaSecu.ValorActual = Id;
            ageLicenciatariosAplicaSecu.UsuarioModificacion = Usuario;

            _ageLicenciatariosAplicaSecuService.ActualizarSecuencia(_httpContextAccessor, ageLicenciatariosAplicaSecu);

            return (int)Id;
        }



        public int ObtenerSecuenciaPrimaria(
            IHttpContextAccessor _httpContextAccessor,
            int codigoSecuencia,
            long Usuario)
        {

            AgeSecuenciasPrimaria? ageSecuenciasPrimaria = _ageSecuenciasPrimariasService
                .ConsultarCompletePorId(codigoSecuencia).Result;

            if (ageSecuenciasPrimaria == null)
                throw new RegisterNotFoundException($"Secuencia primaria con código {codigoSecuencia} no existe.");

            int id;

            if (ageSecuenciasPrimaria.ValorInicial > ageSecuenciasPrimaria.ValorActual)
                id = ageSecuenciasPrimaria.ValorInicial;
            else
                id = ageSecuenciasPrimaria.ValorActual+ ageSecuenciasPrimaria.IncrementaEn;

            ageSecuenciasPrimaria.ValorActual= id;
            ageSecuenciasPrimaria.UsuarioModificacion = Usuario;

            _ageSecuenciasPrimariasService.ActualizarSecuenciaPrimaria(_httpContextAccessor, ageSecuenciasPrimaria);

            return id;
        }


    }
}
