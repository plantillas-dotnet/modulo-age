﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AGE.Utils;
using Microsoft.EntityFrameworkCore;

namespace AGE.Entities
{
    [Table("age_personas", Schema = "sasf_user_desa")]
    public partial class AgePersonas
    {
        public AgePersonas()
        {
            AgeDirecciones = new HashSet<AgeDirecciones>();
            AgeUsuarios = new HashSet<AgeUsuarios>();
        }

        [Key]
        [Column("age_licenc_codigo")]
        [Required(ErrorMessage = "El código de licenciatario no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de licenciatario debe ser un número entero mayor a cero.")]
        public int AgeLicencCodigo { get; set; }

        [Key]
        [Column("codigo")]
        [Required(ErrorMessage = "El código de la persona no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de la persona debe ser un número entero mayor a cero.")]
        public long Codigo { get; set; }

        [Column("age_tip_id_codigo")]
        [Required(ErrorMessage = "El código del tipo de identificación no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código del tipo de identificación debe ser un número entero mayor a cero.")]
        public int AgeTipIdCodigo { get; set; }

        [Column("numero_identificacion")]
        [Required(ErrorMessage = "El número identificación no puede ser nulo.")]
        [RegularExpression("^\\d{10}$", ErrorMessage = "El número identificación debe contener 10 dígitos numéricos.")]
        public string NumeroIdentificacion { get; set; } = null!;

        [Column("nombres")]
        [Required(ErrorMessage = "Los nombres no pueden ser nulos.")]
        [StringLength(200, ErrorMessage = "Los nombres deben contener máximo 200 caracteres.")]
        public string Nombres { get; set; } = null!;

        [Column("apellidos")]
        [Required(ErrorMessage = "Los apellidos no pueden ser nulos.")]
        [StringLength(200, ErrorMessage = "Los apellidos deben contener máximo 200 caracteres.")]
        public string Apellidos { get; set; } = null!;

        [Column("age_locali_age_tip_lo_codigo")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código del tipo de localidad debe ser un número entero mayor a cero.")]
        public int? AgeLocaliAgeTipLoCodigo { get; set; }

        [Column("razon_social")]
        [StringLength(2000, ErrorMessage = "La razón social debe contener máximo 2000 caracteres.")]
        public string? RazonSocial { get; set; }

        [Column("nombre_comercial")]
        [StringLength(2000, ErrorMessage = "El nombre comercial debe contener máximo 2000 caracteres.")]
        public string? NombreComercial { get; set; }

        [Column("telefono_celular_1")]
        [RegularExpression("^\\d{10}$", ErrorMessage = "El teléfono celular 1 debe contener 10 caracteres numéricos.")]
        public string? TelefonoCelular1 { get; set; }

        [Column("telefono_celular_2")]
        [RegularExpression("^\\d{10}$", ErrorMessage = "El teléfono celular 2 debe contener 10 caracteres numéricos.")]
        public string? TelefonoCelular2 { get; set; }

        [Column("age_locali_codigo")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de localidad debe ser un número entero mayor a cero.")]
        public int? AgeLocaliCodigo { get; set; }

        [Column("fecha_nacimiento", TypeName = "timestamp(0) without time zone")]
        public DateTime? FechaNacimiento { get; set; }

        [Column("direccion_principal")]
        [StringLength(2000, ErrorMessage = "La dirección principal debe contener máximo 2000 caracteres.")]
        public string? DireccionPrincipal { get; set; }

        [Column("correo_electronico")]
        [RegularExpression("^[a-zA-Z0-9_!#$%&’*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$", ErrorMessage = "El formato del correo electrónico no es válido.")]
        public string? CorreoElectronico { get; set; }

        [Column("age_age_tip_lo_age_pais_codigo")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de país debe ser un número entero mayor a cero.")]
        public int? AgeAgeTipLoAgePaisCodigo { get; set; }

        [Column("sexo")]
        [RegularExpression("^[MF]$", ErrorMessage = $"El género debe ser {Globales.GENERO_MASCULINO} o {Globales.GENERO_FEMENINO}.")]
        public string? Sexo { get; set; }

        [Column("estado_civil")]
        [RegularExpression("^[SC]$", ErrorMessage = $"El estado cívil debe ser {Globales.SOLTERO} o {Globales.CASADO}.")]
        public string? EstadoCivil { get; set; }

        [Column("telefono_principal")]
        [RegularExpression("^\\d{10}$", ErrorMessage = "El teléfono principal debe contener 10 caracteres numéricos.")]
        public string? TelefonoPrincipal { get; set; }

        [Column("estado")]
        [Required(ErrorMessage = "El estado no puede ser nulo.")]
        [RegularExpression("^[AI]$", ErrorMessage = $"El campo estado debe ser {Globales.ESTADO_ACTIVO} o {Globales.ESTADO_INACTIVO}.")]
        public string Estado { get; set; } = null!;

        [Required(ErrorMessage = "La fecha de estado no puede ser nula.")]
        [Column("fecha_estado", TypeName = "timestamp(0) without time zone")]
        public DateTime FechaEstado { get; set; }

        [Column("observacion_estado")]
        [StringLength(2000,ErrorMessage = "La observación debe contener máximo 2000 caracteres.")]
        public string? ObservacionEstado { get; set; }

        [Required(ErrorMessage = "El campo usuario ingreso no puede ser nulo.")]
        [Column("usuario_ingreso")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El campo usuario ingreso debe ser un número entero mayor a cero.")]
        public long UsuarioIngreso { get; set; }

        [Required(ErrorMessage = "La fecha de ingreso no puede ser nula.")]
        [Column("fecha_ingreso", TypeName = "timestamp(0) without time zone")]
        public DateTime FechaIngreso { get; set; }

        [Required(ErrorMessage = "La ubicación de ingreso no puede ser nula.")]
        [Column("ubicacion_ingreso")]
        [RegularExpression("^[0-9.]+$", ErrorMessage = "La ubicación de ingreso debe contener máximo 200 caracteres.")]
        public string? UbicacionIngreso { get; set; }

        [Column("usuario_modificacion")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El usuario modificación debe ser un número entero mayor a cero.")]
        public long? UsuarioModificacion { get; set; }

        [Column("fecha_modificacion", TypeName = "timestamp(0) without time zone")]
        public DateTime? FechaModificacion { get; set; }

        [RegularExpression("^[0-9.]+$", ErrorMessage = "La ubicación de modificación debe contener máximo 200 caracteres. SSS")]
        [Column("ubicacion_modificacion")]
        public string? UbicacionModificacion { get; set; }


        [ForeignKey("AgeLocaliCodigo,AgeLocaliAgeTipLoCodigo,AgeAgeTipLoAgePaisCodigo")]
        [InverseProperty("AgePersonas")]
        public virtual AgeLocalidades? Age { get; set; }
        [ForeignKey("AgeLicencCodigo")]
        [InverseProperty("AgePersonas")]
        public virtual AgeLicenciatario AgeLicencCodigoNavigation { get; set; } = null!;
        [ForeignKey("AgeTipIdCodigo")]
        [InverseProperty("AgePersonas")]
        public virtual AgeTiposIdentificaciones AgeTipIdCodigoNavigation { get; set; } = null!;

        [InverseProperty("AgePerson")]
        public virtual ICollection<AgeDirecciones> AgeDirecciones { get; set; }
        [InverseProperty("AgePerson")]
        public virtual ICollection<AgeUsuarios> AgeUsuarios { get; set; }
    }
}
