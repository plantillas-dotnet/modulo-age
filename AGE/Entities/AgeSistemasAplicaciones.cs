﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AGE.Utils;
using Microsoft.EntityFrameworkCore;

namespace AGE.Entities
{
    [Table("age_sistemas_aplicaciones", Schema = "sasf_user_desa")]
    public partial class AgeSistemasAplicaciones
    {
        [Key]
        [Required(ErrorMessage = "El código de la aplicación no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de la aplicación debe ser un número entero mayor a cero.")]
        [Column("age_aplica_codigo")]
        public int AgeAplicaCodigo { get; set; }

        [Key]
        [Required(ErrorMessage = "El código del sistema no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código del sistema debe ser un número entero mayor a cero.")]
        [Column("age_sistem_codigo")]
        public int AgeSistemCodigo { get; set; }

        [Required(ErrorMessage = "La fecha desde no puede ser nula.")]
        [Column("fecha_desde", TypeName = "timestamp without time zone")]
        public DateTime FechaDesde { get; set; }

        [Column("fecha_hasta", TypeName = "timestamp without time zone")]
        public DateTime? FechaHasta { get; set; }

        [Required(ErrorMessage = "El estado no puede ser nulo.")]
        [Column("estado")]
        [RegularExpression("^[AI]$", ErrorMessage = $"El campo estado debe ser {Globales.ESTADO_ACTIVO} o {Globales.ESTADO_INACTIVO}.")]
        public string Estado { get; set; } = null!;

        [Column("fecha_estado", TypeName = "timestamp(0) without time zone")]
        [Required(ErrorMessage = "La fecha de estado no puede ser nula.")]
        public DateTime FechaEstado { get; set; }

        [Column("observacion_estado")]
        [StringLength(2000,ErrorMessage = "La observación debe contener máximo 2000 caracteres.")]
        public string? ObservacionEstado { get; set; }

        [Column("usuario_ingreso")]
        [Required(ErrorMessage = "El campo usuario ingreso no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El campo usuario ingreso debe ser un número entero mayor a cero.")]
        public long UsuarioIngreso { get; set; }

        [Column("fecha_ingreso", TypeName = "timestamp(0) without time zone")]
        [Required(ErrorMessage = "La fecha de ingreso no puede ser nula.")]
        public DateTime FechaIngreso { get; set; }

        [Column("ubicacion_ingreso")]
        [Required(ErrorMessage = "La ubicación de ingreso no puede ser nula.")]
        [RegularExpression("^[0-9.]+$", ErrorMessage = "La ubicación de ingreso debe contener máximo 200 caracteres.")]
        public string UbicacionIngreso { get; set; } = null!;

        [Column("usuario_modificacion")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El usuario modificación debe ser un número entero mayor a cero.")]
        public long? UsuarioModificacion { get; set; }

        [Column("fecha_modificacion", TypeName = "timestamp(0) without time zone")]
        public DateTime? FechaModificacion { get; set; }

        [Column("ubicacion_modificacion")]
        [RegularExpression("^[0-9.]+$", ErrorMessage = "La ubicación de modificación debe contener máximo 200 caracteres.")]
        public string? UbicacionModificacion { get; set; }

        [ForeignKey("AgeAplicaCodigo")]
        [InverseProperty("AgeSistemasAplicaciones")]
        public virtual AgeAplicaciones AgeAplicaCodigoNavigation { get; set; } = null!;
        [ForeignKey("AgeSistemCodigo")]
        [InverseProperty("AgeSistemasAplicaciones")]
        public virtual AgeSistemas AgeSistemCodigoNavigation { get; set; } = null!;
    }
}
