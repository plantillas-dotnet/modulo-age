﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AGE.Utils;

namespace AGE.Entities
{
    [Table("age_tipos_localidades", Schema = "sasf_user_desa")]
    public partial class AgeTiposLocalidades
    {
        public AgeTiposLocalidades()
        {
            AgeLocalidades = new HashSet<AgeLocalidades>();
            InverseAgeTipLo = new HashSet<AgeTiposLocalidades>();
        }

        [Key]
        [Column("age_pais_codigo")]
        [Required(ErrorMessage = "El código de país no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de país debe ser un número entero mayor a cero.")]
        public int AgePaisCodigo { get; set; }
        
        
        [Key]
        [Column("codigo")]
        [Required(ErrorMessage = "El código no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código debe ser un número entero mayor a cero.")]
        public int Codigo { get; set; }
        
        
        [Required(ErrorMessage = "La descripción no puede ser nula.")]
        [Column("descripcion")]
        [StringLength(200, ErrorMessage = "La descripción debe contener máximo 200 caracteres.")]
        public string Descripcion { get; set; } = null!;


        [Required(ErrorMessage = "El estado no puede ser nulo.")]
        [Column("estado")]
        [RegularExpression("^[AI]$", ErrorMessage = $"El campo estado debe ser {Globales.ESTADO_ACTIVO} o {Globales.ESTADO_INACTIVO}.")]
        public string Estado { get; set; } = null!;


        [Required(ErrorMessage = "La fecha de estado no puede ser nula.")]
        [Column("fecha_estado", TypeName = "timestamp(0) without time zone")]
        public DateTime FechaEstado { get; set; }


        [Column("age_tip_lo_age_pais_codigo")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código AgeTipLoAgePaisCodigo debe ser un número entero mayor a cero.")]
        public int? AgeTipLoAgePaisCodigo { get; set; }


        [Column("age_tip_lo_codigo")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código AgeTipLoCodigo debe ser un número entero mayor a cero.")]
        public int? AgeTipLoCodigo { get; set; }


        [Column("observacion_estado")]
        [StringLength(2000,ErrorMessage = "La observación debe contener máximo 2000 caracteres.")]
        public string? ObservacionEstado { get; set; }


        [Required(ErrorMessage = "El campo usuario ingreso no puede ser nulo.")]
        [Column("usuario_ingreso")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El campo usuario ingreso debe ser un número entero mayor a cero.")]
        public long UsuarioIngreso { get; set; }


        [Required(ErrorMessage = "La fecha de ingreso no puede ser nula.")]
        [Column("fecha_ingreso", TypeName = "timestamp(0) without time zone")]
        public DateTime FechaIngreso { get; set; }


        [Required(ErrorMessage = "La ubicación de ingreso no puede ser nula.")]
        [Column("ubicacion_ingreso")]
        [RegularExpression("^[0-9.]+$", ErrorMessage = "La ubicación de ingreso debe contener máximo 200 caracteres.")]
        public string? UbicacionIngreso { get; set; }


        [Column("usuario_modificacion")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El usuario modificación debe ser un número entero mayor a cero.")]
        public long? UsuarioModificacion { get; set; }


        [Column("fecha_modificacion", TypeName = "timestamp(0) without time zone")]
        public DateTime? FechaModificacion { get; set; }


        [RegularExpression("^[0-9.]+$", ErrorMessage = "La ubicación de modificación debe contener máximo 200 caracteres.")]
        [Column("ubicacion_modificacion")]
        public string? UbicacionModificacion { get; set; }


        [ForeignKey("AgePaisCodigo")]
        [InverseProperty("AgeTiposLocalidades")]
        public virtual AgePaises AgePaisCodigoNavigation { get; set; } = null!;

        [ForeignKey("AgeTipLoCodigo,AgeTipLoAgePaisCodigo")]
        [InverseProperty("InverseAgeTipLo")]
        public virtual AgeTiposLocalidades? AgeTipLo { get; set; }
        [InverseProperty("AgeTipLo")]
        public virtual ICollection<AgeLocalidades> AgeLocalidades { get; set; }
        [InverseProperty("AgeTipLo")]
        public virtual ICollection<AgeTiposLocalidades> InverseAgeTipLo { get; set; }
    }
}
