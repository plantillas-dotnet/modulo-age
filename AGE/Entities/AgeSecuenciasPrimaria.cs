﻿
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AGE.Utils;

namespace AGE.Entities
{
    [Table("age_secuencias_primarias", Schema = "sasf_user_desa")]
    public partial class AgeSecuenciasPrimaria
    {
        [Key]
        [Column("codigo")]
        [Required(ErrorMessage = "El código no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código debe ser un número entero mayor a cero.")]
        public int Codigo { get; set; }

        [Column("descripcion")]
        [Required(ErrorMessage = "La descripción no puede ser nula.")]
        [StringLength(200, ErrorMessage = "La descripción debe contener máximo 200 caracteres.")]
        public string Descripcion { get; set; } = null!;

        [Column("valor_inicial")]
        [RegularExpression("^[0-9]\\d*$", ErrorMessage = "El valor inicial debe ser un número entero mayor o igual a cero.")]
        [Required(ErrorMessage = "El valor inicial no puede ser nulo.")]
        public int ValorInicial { get; set; }

        [Column("incrementa_en")]
        [Required(ErrorMessage = "El campo incrementa_en no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El campo incrementa_en debe ser un número entero mayor a cero.")]
        public int IncrementaEn { get; set; }

        [Column("valor_actual")]
        [RegularExpression("^[0-9]\\d*$", ErrorMessage = "El valor actual debe ser un número entero mayor o igual a cero.")]
        [Required(ErrorMessage = "El valor actual no puede ser nulo.")]
        public int ValorActual { get; set; }

        [Required(ErrorMessage = "El campo ciclica no puede ser nulo.")]
        [Column("ciclica")]
        [RegularExpression("^[NS]$", ErrorMessage = $"El campo ciclica debe ser {Globales.SI} o {Globales.NO}.")]
        public string Ciclica { get; set; } = null!;

        [Required(ErrorMessage = "El estado no puede ser nulo.")]
        [Column("estado")]
        [RegularExpression("^[AI]$", ErrorMessage = $"El campo estado debe ser {Globales.ESTADO_ACTIVO} o {Globales.ESTADO_INACTIVO}.")]
        public string Estado { get; set; } = null!;

        [Column("fecha_estado", TypeName = "timestamp(0) without time zone")]
        [Required(ErrorMessage = "La fecha de estado no puede ser nula.")]
        public DateTime FechaEstado { get; set; }

        [Column("observacion_estado")]
        [StringLength(2000,ErrorMessage = "La observación debe contener máximo 2000 caracteres.")]
        public string? ObservacionEstado { get; set; }

        [Column("usuario_ingreso")]
        [Required(ErrorMessage = "El campo usuario ingreso no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El campo usuario ingreso debe ser un número entero mayor a cero.")]
        public long UsuarioIngreso { get; set; }

        [Column("fecha_ingreso", TypeName = "timestamp(0) without time zone")]
        [Required(ErrorMessage = "La fecha de ingreso no puede ser nula.")]
        public DateTime FechaIngreso { get; set; }

        [Column("ubicacion_ingreso")]
        [Required(ErrorMessage = "La ubicación de ingreso no puede ser nula.")]
        [RegularExpression("^[0-9.]+$", ErrorMessage = "La ubicación de ingreso debe contener máximo 200 caracteres.")]
        public string UbicacionIngreso { get; set; } = null!;

        [Column("usuario_modificacion")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El usuario modificación debe ser un número entero mayor a cero.")]
        public long? UsuarioModificacion { get; set; }

        [Column("fecha_modificacion", TypeName = "timestamp(0) without time zone")]
        public DateTime? FechaModificacion { get; set; }

        [Column("ubicacion_modificacion")]
        [RegularExpression("^[0-9.]+$", ErrorMessage = "La ubicación de modificación debe contener máximo 200 caracteres.")]
        public string? UbicacionModificacion { get; set; }

        /*[Column("version")]
        public int? Version { get; set; }*/

    }


}
