﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AGE.Utils;
using Microsoft.EntityFrameworkCore;

namespace AGE.Entities
{
    [Table("age_mensajes_idiomas", Schema = "sasf_user_desa")]
    public partial class AgeMensajesIdiomas
    {

        [Key]
        [Column("age_idioma_codigo")]
        [Required(ErrorMessage = "El código del idioma no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código del idioma debe ser un número entero mayor a cero.")]
        public int AgeIdiomaCodigo { get; set; }


        [Key]
        [Column("age_mensaj_codigo")]
        [Required(ErrorMessage = "El código del mensaje no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código del mensaje debe ser un número entero mayor a cero.")]
        public long AgeMensajCodigo { get; set; }


        [Column("descripcion_msg")]
        [Required(ErrorMessage = "La descripción del mensaje no puede ser nula.")]
        [StringLength(2000, ErrorMessage = "La descripción del mensaje no puede estar vacía.")]
        public string DescripcionMsg { get; set; } = null!;


        [Column("solucion_msg")]
        [Required(ErrorMessage = "La solución del mensaje no puede ser nula.")]
        [StringLength(2000, ErrorMessage = "La solución del mensaje no puede estar vacía.")]
        public string? SolucionMsg { get; set; }


        [Required(ErrorMessage = "El estado no puede ser nulo.")]
        [Column("estado")]
        [RegularExpression("^[AI]$", ErrorMessage = $"El campo estado debe ser {Globales.ESTADO_ACTIVO} o {Globales.ESTADO_INACTIVO}.")]
        public string Estado { get; set; } = null!;


        [Required(ErrorMessage = "La fecha de estado no puede ser nula.")]
        [Column("fecha_estado", TypeName = "timestamp(0) without time zone")]
        public DateTime FechaEstado { get; set; }


        [Column("observacion_estado")]
        [StringLength(2000, ErrorMessage = "La observación del estado debe contener máximo 2000 caracteres.")]
        public string? ObservacionEstado { get; set; }


        [Required(ErrorMessage = "El campo usuario ingreso no puede ser nulo.")]
        [Column("usuario_ingreso")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El campo usuario ingreso debe ser un número entero mayor a cero.")]
        public long UsuarioIngreso { get; set; }


        [Required(ErrorMessage = "La fecha de ingreso no puede ser nula.")]
        [Column("fecha_ingreso", TypeName = "timestamp(0) without time zone")]
        public DateTime FechaIngreso { get; set; }


        [Required(ErrorMessage = "La ubicación de ingreso no puede ser nula.")]
        [Column("ubicacion_ingreso")]
        [RegularExpression("^[0-9.]+$", ErrorMessage = "La ubicación de ingreso debe contener máximo 200 caracteres.")]
        public string? UbicacionIngreso { get; set; }


        [Column("usuario_modificacion")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El usuario modificación debe ser un número entero mayor a cero.")]
        public long? UsuarioModificacion { get; set; }


        [Column("fecha_modificacion", TypeName = "timestamp(0) without time zone")]
        public DateTime? FechaModificacion { get; set; }


        [RegularExpression("^[0-9.]+$", ErrorMessage = "La ubicación de modificación debe contener máximo 200 caracteres.")]
        [Column("ubicacion_modificacion")]
        public string? UbicacionModificacion { get; set; }



        [ForeignKey("AgeIdiomaCodigo")]
        [InverseProperty("AgeMensajesIdiomas")]
        public virtual AgeIdiomas AgeIdiomaCodigoNavigation { get; set; } = null!;


        [ForeignKey("AgeMensajCodigo")]
        [InverseProperty("AgeMensajesIdiomas")]
        public virtual AgeMensajes AgeMensajCodigoNavigation { get; set; } = null!;
    }
}
