﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AGE.Utils;

namespace AGE.Entities
{
    [Table("age_tipos_identificaciones", Schema = "sasf_user_desa")]
    public partial class AgeTiposIdentificaciones
    {
        public AgeTiposIdentificaciones()
        {
            AgeLicenciatarios = new HashSet<AgeLicenciatario>();
            AgePersonas = new HashSet<AgePersonas>();
            AgeUsuarios = new HashSet<AgeUsuarios>();
        }

        [Key]
        [Column("codigo")]
        [Required(ErrorMessage = "El código de tipo de identificación no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de tipo de identificación debe ser un número entero mayor a cero.")]
        public int Codigo { get; set; }


        [Column("codigo_institucion_control")]
        [Required(ErrorMessage = "El código de institución de control no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de institución de control debe ser un número entero mayor a cero.")]
        public int CodigoInstitucionControl { get; set; }


        [Column("siglas")]
        [Required(ErrorMessage = "Las siglas no pueden ser nulas.")]
        [StringLength(2, ErrorMessage = "Las siglas deben contener máximo 2 caracteres.")]
        public string Siglas { get; set; } = null!;


        [Required(ErrorMessage = "La descripción no puede ser nula.")]
        [Column("descripcion")]
        [StringLength(200, ErrorMessage = "La descripción debe contener máximo 200 caracteres.")]
        public string Descripcion { get; set; } = null!;


        [Required(ErrorMessage = "El estado no puede ser nulo.")]
        [Column("estado")]
        [RegularExpression("^[AI]$", ErrorMessage = $"El campo estado debe ser {Globales.ESTADO_ACTIVO} o {Globales.ESTADO_INACTIVO}.")]
        public string Estado { get; set; } = null!;


        [Required(ErrorMessage = "La fecha de estado no puede ser nula.")]
        [Column("fecha_estado", TypeName = "timestamp(0) without time zone")]
        public DateTime FechaEstado { get; set; }


        [Column("observacion_estado")]
        [StringLength(2000,ErrorMessage = "La observación debe contener máximo 2000 caracteres.")]
        public string? ObservacionEstado { get; set; }


        [Required(ErrorMessage = "El campo usuario ingreso no puede ser nulo.")]
        [Column("usuario_ingreso")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El campo usuario ingreso debe ser un número entero mayor a cero.")]
        public long UsuarioIngreso { get; set; }


        [Required(ErrorMessage = "La fecha de ingreso no puede ser nula.")]
        [Column("fecha_ingreso", TypeName = "timestamp(0) without time zone")]
        public DateTime FechaIngreso { get; set; }


        [Required(ErrorMessage = "La ubicación de ingreso no puede ser nula.")]
        [Column("ubicacion_ingreso")]
        [RegularExpression("^[0-9.]+$", ErrorMessage = "La ubicación de ingreso debe contener máximo 200 caracteres.")]
        public string? UbicacionIngreso { get; set; }


        [Column("usuario_modificacion")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El usuario modificación debe ser un número entero mayor a cero.")]
        public long? UsuarioModificacion { get; set; }


        [Column("fecha_modificacion", TypeName = "timestamp(0) without time zone")]
        public DateTime? FechaModificacion { get; set; }


        [RegularExpression("^[0-9.]+$", ErrorMessage = "La ubicación de modificación debe contener máximo 200 caracteres.")]
        [Column("ubicacion_modificacion")]
        public string? UbicacionModificacion { get; set; }


        [InverseProperty("AgeTipIdCodigoNavigation")]
        public virtual ICollection<AgeLicenciatario> AgeLicenciatarios { get; set; }
        [InverseProperty("AgeTipIdCodigoNavigation")]
        public virtual ICollection<AgePersonas> AgePersonas { get; set; }
        [InverseProperty("AgeTipIdCodigoNavigation")]
        public virtual ICollection<AgeUsuarios> AgeUsuarios { get; set; }
    }
}
