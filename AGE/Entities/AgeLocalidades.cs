﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AGE.Utils;
using Microsoft.EntityFrameworkCore;

namespace AGE.Entities
{
    [Table("age_localidades", Schema = "sasf_user_desa")]
    public partial class AgeLocalidades
    {
        public AgeLocalidades()
        {
            AgeDirecciones = new HashSet<AgeDirecciones>();
            AgeFeriadosLocalidades = new HashSet<AgeFeriadosLocalidades>();
            AgePersonas = new HashSet<AgePersonas>();
            AgeSucursales = new HashSet<AgeSucursales>();
            InverseAge = new HashSet<AgeLocalidades>();
        }

        [Key]
        [Required(ErrorMessage = "El código del país no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código del país debe ser un número entero mayor a cero.")]
        [Column("age_tip_lo_age_pais_codigo")]
        public int AgeTipLoAgePaisCodigo { get; set; }

        [Key]
        [Required(ErrorMessage = "El código del tipo localidad no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código del tipo localidad debe ser un número entero mayor a cero.")]
        [Column("age_tip_lo_codigo")]
        public int AgeTipLoCodigo { get; set; }

        [Key]
        [Required(ErrorMessage = "El código de la localidad no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de la localidad debe ser un número entero mayor a cero.")]
        [Column("codigo")]
        public int Codigo { get; set; }

        [Column("descripcion")]
        [StringLength(200, ErrorMessage = "La descripción debe contener máximo 200 caracteres.")]
        [Required(ErrorMessage = "La descripción no puede ser nula.")]
        public string Descripcion { get; set; } = null!;

        [Required(ErrorMessage = "El estado no puede ser nulo.")]
        [Column("estado")]
        [RegularExpression("^[AI]$", ErrorMessage = $"El campo estado debe ser {Globales.ESTADO_ACTIVO} o {Globales.ESTADO_INACTIVO}.")]
        public string Estado { get; set; } = null!;

        [Column("fecha_estado", TypeName = "timestamp(0) without time zone")]
        [Required(ErrorMessage = "La fecha de estado no puede ser nula.")]
        public DateTime FechaEstado { get; set; }

        [Column("age_age_tip_lo_age_pais_codigo")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código del país debe ser un número entero mayor a cero.")]
        public int? AgeAgeTipLoAgePaisCodigo { get; set; }

        [Column("age_locali_age_tip_lo_codigo")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código del tipo localidad debe ser un número entero mayor a cero.")]
        public int? AgeLocaliAgeTipLoCodigo { get; set; }

        [Column("observacion_estado")]
        [StringLength(2000,ErrorMessage = "La observación debe contener máximo 2000 caracteres.")]
        public string? ObservacionEstado { get; set; }

        [Column("usuario_ingreso")]
        [Required(ErrorMessage = "El campo usuario ingreso no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El campo usuario ingreso debe ser un número entero mayor a cero.")]
        public long UsuarioIngreso { get; set; }

        [Column("fecha_ingreso", TypeName = "timestamp(0) without time zone")]
        [Required(ErrorMessage = "La fecha de ingreso no puede ser nula.")]
        public DateTime FechaIngreso { get; set; }

        [Column("ubicacion_ingreso")]
        [Required(ErrorMessage = "La ubicación de ingreso no puede ser nula.")]
        [RegularExpression("^[0-9.]+$", ErrorMessage = "La ubicación de ingreso debe contener máximo 200 caracteres.")]
        public string UbicacionIngreso { get; set; } = null!;

        [Column("age_idioma_codigo")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código del idioma debe ser un número entero mayor a cero.")]
        public int? AgeIdiomaCodigo { get; set; }

        [Column("age_locali_codigo")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de la localidad debe ser un número entero mayor a cero.")]
        public int? AgeLocaliCodigo { get; set; }

        [Column("age_moneda_codigo")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de la moneda debe ser un número entero mayor a cero.")]
        public int? AgeMonedaCodigo { get; set; }

        [Column("usuario_modificacion")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El usuario modificación debe ser un número entero mayor a cero.")]
        public long? UsuarioModificacion { get; set; }

        [Column("fecha_modificacion", TypeName = "timestamp(0) without time zone")]
        public DateTime? FechaModificacion { get; set; }

        [Column("ubicacion_modificacion")]
        [RegularExpression("^[0-9.]+$", ErrorMessage = "La ubicación de modificación debe contener máximo 200 caracteres.")]
        public string? UbicacionModificacion { get; set; }

        [Column("latitud_centro", TypeName = "decimal(18, 7)")]
        [Range(-999999999999999999.9999999, 999999999999999999.9999999, ErrorMessage = "El valor de LatitudCentro debe estar dentro del rango permitido.")]
        public decimal? LatitudCentro { get; set; }

        [Column("longitud_centro", TypeName = "decimal(18, 7)")]
        [Range(-999999999999999999.9999999, 999999999999999999.9999999, ErrorMessage = "El valor de LongitudCentro debe estar dentro del rango permitido.")]
        public decimal? LongitudCentro { get; set; }

        [Column("poligono")]
        public byte[]? Poligono { get; set; }

        [ForeignKey("AgeLocaliCodigo,AgeLocaliAgeTipLoCodigo,AgeAgeTipLoAgePaisCodigo")]
        [InverseProperty("InverseAge")]
        public virtual AgeLocalidades? Age { get; set; }

        [ForeignKey("AgeIdiomaCodigo")]
        [InverseProperty("AgeLocalidades")]
        public virtual AgeIdiomas? AgeIdiomaCodigoNavigation { get; set; }

        [ForeignKey("AgeMonedaCodigo")]
        [InverseProperty("AgeLocalidades")]
        public virtual AgeMonedas? AgeMonedaCodigoNavigation { get; set; }

        [ForeignKey("AgeTipLoCodigo,AgeTipLoAgePaisCodigo")]
        [InverseProperty("AgeLocalidades")]
        public virtual AgeTiposLocalidades AgeTipLo { get; set; } = null!;

        [InverseProperty("Age")]
        public virtual ICollection<AgeDirecciones> AgeDirecciones { get; set; }
        [InverseProperty("Age")]
        public virtual ICollection<AgeFeriadosLocalidades> AgeFeriadosLocalidades { get; set; }
        [InverseProperty("Age")]
        public virtual ICollection<AgePersonas> AgePersonas { get; set; }
        [InverseProperty("Age")]
        public virtual ICollection<AgeSucursales> AgeSucursales { get; set; }
        [InverseProperty("Age")]
        public virtual ICollection<AgeLocalidades> InverseAge { get; set; }
    }
}
