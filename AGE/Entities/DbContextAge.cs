﻿using System;
using System.Collections.Generic;
using System.Xml;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace AGE.Entities
{
    public partial class DbContextAge : DbContext
    {
        public DbContextAge()
        {
        }

        public DbContextAge(DbContextOptions<DbContextAge> options)
            : base(options)
        {
        }

        public virtual DbSet<AgeAplicaciones> AgeAplicaciones { get; set; } = null!;
        public virtual DbSet<AgeAplicacionesDependencia> AgeAplicacionesDependencias { get; set; } = null!;
        public virtual DbSet<AgeArchivosMultimedia> AgeArchivosMultimedia { get; set; } = null!;
        public virtual DbSet<AgeClasesContribuyentes> AgeClasesContribuyentes { get; set; } = null!;
        public virtual DbSet<AgeDepartamentos> AgeDepartamentos { get; set; } = null!;
        public virtual DbSet<AgeDiasFeriados> AgeDiasFeriados { get; set; } = null!;
        public virtual DbSet<AgeDirecciones> AgeDirecciones { get; set; } = null!;
        public virtual DbSet<AgeFavoritos> AgeFavoritos { get; set; } = null!;
        public virtual DbSet<AgeFeriadosExcepciones> AgeFeriadosExcepciones { get; set; } = null!;
        public virtual DbSet<AgeFeriadosLocalidades> AgeFeriadosLocalidades { get; set; } = null!;
        public virtual DbSet<AgeFeriadosPaises> AgeFeriadosPaises { get; set; } = null!;
        public virtual DbSet<AgeFirmasDigitales> AgeFirmasDigitales { get; set; } = null!;
        public virtual DbSet<AgeFormasPagos> AgeFormasPagos { get; set; } = null!;
        public virtual DbSet<AgeFormasPagosInstFina> AgeFormasPagosInstFinas { get; set; } = null!;
        public virtual DbSet<AgeFranquicias> AgeFranquicias { get; set; } = null!;
        public virtual DbSet<AgeIdiomas> AgeIdiomas { get; set; } = null!;
        public virtual DbSet<AgeInstitucionesFinancieras> AgeInstitucionesFinancieras { get; set; } = null!;
        public virtual DbSet<AgeLicenciatParamVigencias> AgeLicenciatParamVigencias { get; set; } = null!;
        public virtual DbSet<AgeLicenciatariTransaccione> AgeLicenciatariTransacciones { get; set; } = null!;
        public virtual DbSet<AgeLicenciatario> AgeLicenciatarios { get; set; } = null!;
        public virtual DbSet<AgeLicenciatariosAplicaSecu> AgeLicenciatariosAplicaSecus { get; set; } = null!;
        public virtual DbSet<AgeLicenciatariosAplicaciones> AgeLicenciatariosAplicacions { get; set; } = null!;
        public virtual DbSet<AgeLicenciatariosPaise> AgeLicenciatariosPaises { get; set; } = null!;
        public virtual DbSet<AgeLicenciatariosSecuencia> AgeLicenciatariosSecuencias { get; set; } = null!;
        public virtual DbSet<AgeLocalidades> AgeLocalidades { get; set; } = null!;
        public virtual DbSet<AgeLogErrores> AgeLogErrores { get; set; } = null!;
        public virtual DbSet<AgeLogUsuarios> AgeLogUsuarios { get; set; } = null!;
        public virtual DbSet<AgeMensajes> AgeMensajes { get; set; } = null!;
        public virtual DbSet<AgeMensajesIdiomas> AgeMensajesIdiomas { get; set; } = null!;
        public virtual DbSet<AgeModalidad> AgeModalidads { get; set; } = null!;
        public virtual DbSet<AgeMonedas> AgeMonedas { get; set; } = null!;
        public virtual DbSet<AgeMonedasCotizaciones> AgeMonedasCotizaciones { get; set; } = null!;
        public virtual DbSet<AgeNotificacionesPush> AgeNotificacionesPushes { get; set; } = null!;
        public virtual DbSet<AgePaises> AgePaises { get; set; } = null!;
        public virtual DbSet<AgePaisesIdiomas> AgePaisesIdiomas { get; set; } = null!;
        public virtual DbSet<AgePaisesMonedas> AgePaisesMonedas { get; set; } = null!;
        public virtual DbSet<AgeParamGeneralVigencias> AgeParamGeneralVigencias { get; set; } = null!;
        public virtual DbSet<AgeParametrosGenerales> AgeParametrosGenerales { get; set; } = null!;
        public virtual DbSet<AgePerfiles> AgePerfiles { get; set; } = null!;
        public virtual DbSet<AgePerfilesTransacciones> AgePerfilesTransacciones { get; set; } = null!;
        public virtual DbSet<AgePersonas> AgePersonas { get; set; } = null!;
        public virtual DbSet<AgePuntosEmisiones> AgePuntosEmisions { get; set; } = null!;
        public virtual DbSet<AgeRutas> AgeRutas { get; set; } = null!;
        public virtual DbSet<AgeSecuenciasPrimaria> AgeSecuenciasPrimarias { get; set; } = null!;
        public virtual DbSet<AgeSistemas> AgeSistemas { get; set; } = null!;
        public virtual DbSet<AgeSistemasAplicaciones> AgeSistemasAplicaciones { get; set; } = null!;
        public virtual DbSet<AgeSubAplicaciones> AgeSubAplicaciones { get; set; } = null!;
        public virtual DbSet<AgeSucursales> AgeSucursales { get; set; } = null!;
        public virtual DbSet<AgeTiposDirecciones> AgeTiposDirecciones { get; set; } = null!;
        public virtual DbSet<AgeTiposIdentificaciones> AgeTiposIdentificaciones { get; set; } = null!;
        public virtual DbSet<AgeTiposLocalidades> AgeTiposLocalidades { get; set; } = null!;
        public virtual DbSet<AgeTiposPersonas> AgeTiposPersonas { get; set; } = null!;
        public virtual DbSet<AgeTiposSucursales> AgeTiposSucursales { get; set; } = null!;
        public virtual DbSet<AgeTransacciones> AgeTransacciones { get; set; } = null!;
        public virtual DbSet<AgeTransaccionesIdiomas> AgeTransaccionesIdiomas { get; set; } = null!;
        public virtual DbSet<AgeUnidadesTiempo> AgeUnidadesTiempos { get; set; } = null!;
        public virtual DbSet<AgeUsuarios> AgeUsuarios { get; set; } = null!;
        public virtual DbSet<AgeUsuarioParamVigencias> AgeUsuarioParamVigencias { get; set; } = null!;
        public virtual DbSet<AgeUsuariosPerfiles> AgeUsuariosPerfiles { get; set; } = null!;
        public virtual DbSet<AgeUsuariosPuntoEmision> AgeUsuariosPuntoEmisions { get; set; } = null!;
        public virtual DbSet<OauthClientDetail> OauthClientDetails { get; set; } = null!;
        public virtual DbSet<UmUser> UmUsers { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseNpgsql("Server=192.168.0.115;Port=5432;UID=postgres;Password=Salvarado1;Database=age;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AgeAplicaciones>(entity =>
            {
                entity.HasKey(e => e.Codigo)
                    .HasName("age_aplica_pk");

                entity.Property(e => e.Codigo).ValueGeneratedNever();
            });

            modelBuilder.Entity<AgeAplicacionesDependencia>(entity =>
            {
                entity.HasKey(e => new { e.AgeAplicaCodigo, e.AgeAplicaCodigoSer, e.OrdenDependencia })
                    .HasName("age_apl_de_pk");

                entity.HasOne(d => d.AgeAplicaCodigoNavigation)
                    .WithMany(p => p.AgeAplicacionesDependenciaAgeAplicaCodigoNavigations)
                    .HasForeignKey(d => d.AgeAplicaCodigo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("age_apl_de_age_aplica_fk");

                entity.HasOne(d => d.AgeAplicaCodigoSerNavigation)
                    .WithMany(p => p.AgeAplicacionesDependenciaAgeAplicaCodigoSerNavigations)
                    .HasForeignKey(d => d.AgeAplicaCodigoSer)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("age_apl_de_age_aplica_ser_fk");
            });

            modelBuilder.Entity<AgeArchivosMultimedia>(entity =>
            {
                entity.HasKey(e => new { e.AgeLicencCodigo, e.Codigo })
                    .HasName("age_archivos_multimedia_pkey");
            });

            modelBuilder.Entity<AgeClasesContribuyentes>(entity =>
            {
                entity.HasKey(e => e.Codigo)
                    .HasName("age_cla_co_pk");

                entity.Property(e => e.Codigo).ValueGeneratedNever();
            });

            modelBuilder.Entity<AgeDepartamentos>(entity =>
            {
                entity.HasKey(e => new { e.Codigo, e.AgeLicencCodigo })
                    .HasName("age_depart_pk");

                entity.HasOne(d => d.AgeLicencCodigoNavigation)
                    .WithMany(p => p.AgeDepartamentos)
                    .HasForeignKey(d => d.AgeLicencCodigo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("age_depart_age_licenc_fk");

                entity.HasOne(d => d.AgeDepart)
                    .WithMany(p => p.InverseAgeDepart)
                    .HasForeignKey(d => new { d.AgeDepartCodigo, d.AgeDepartAgeLicencCodigo })
                    .HasConstraintName("age_depart_age_depart_fk");
            });

            modelBuilder.Entity<AgeDiasFeriados>(entity =>
            {
                entity.HasKey(e => e.Codigo)
                    .HasName("age_dia_fe_pk");

                entity.Property(e => e.Codigo).ValueGeneratedNever();
            });

            modelBuilder.Entity<AgeDirecciones>(entity =>
            {
                entity.HasKey(e => new { e.Codigo, e.AgePersonCodigo, e.AgePersonAgeLicencCodigo })
                    .HasName("age_direcc_pk");

                entity.HasOne(d => d.AgeTipDiCodigoNavigation)
                    .WithMany(p => p.AgeDirecciones)
                    .HasForeignKey(d => d.AgeTipDiCodigo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("age_direcc_age_tip_di_fk");

                entity.HasOne(d => d.AgePerson)
                    .WithMany(p => p.AgeDirecciones)
                    .HasForeignKey(d => new { d.AgePersonCodigo, d.AgePersonAgeLicencCodigo })
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("age_direcc_age_person_fk");

                entity.HasOne(d => d.Age)
                    .WithMany(p => p.AgeDirecciones)
                    .HasForeignKey(d => new { d.AgeLocaliCodigo, d.AgeLocaliAgeTipLoCodigo, d.AgeAgeTipLoAgePaisCodigo })
                    .HasConstraintName("age_direcc_age_locali_fk");
            });

            modelBuilder.Entity<AgeFavoritos>(entity =>
            {
                entity.HasKey(e => new { e.AgeUsuariCodigo, e.AgeUsuariAgeLicencCodigo, e.AgeTransaCodigo, e.AgeTransaAgeAplicaCodigo, e.Codigo })
                    .HasName("age_favori_pk");

                entity.HasOne(d => d.AgeTransa)
                    .WithMany(p => p.AgeFavoritos)
                    .HasForeignKey(d => new { d.AgeTransaCodigo, d.AgeTransaAgeAplicaCodigo })
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("age_favori_age_transa_fk");

                entity.HasOne(d => d.AgeUsuari)
                    .WithMany(p => p.AgeFavoritos)
                    .HasForeignKey(d => new { d.AgeUsuariCodigo, d.AgeUsuariAgeLicencCodigo })
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("age_favori_age_usuari_fk");
            });

            modelBuilder.Entity<AgeFeriadosExcepciones>(entity =>
            {
                entity.HasKey(e => new { e.Codigo, e.AgeLicencCodigo })
                    .HasName("age_fer_ex_pk");

                entity.HasOne(d => d.AgeLicencCodigoNavigation)
                    .WithMany(p => p.AgeFeriadosExcepciones)
                    .HasForeignKey(d => d.AgeLicencCodigo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("age_fer_ex_age_licenc_fk");
            });

            modelBuilder.Entity<AgeFeriadosLocalidades>(entity =>
            {
                entity.HasKey(e => new { e.AgeLocaliCodigo, e.AgeLocaliAgeTipLoCodigo, e.AgeAgeTipLoAgePaisCodigo, e.AgeDiaFeCodigo })
                    .HasName("age_fer_lo_pk");

                entity.HasOne(d => d.AgeDiaFeCodigoNavigation)
                    .WithMany(p => p.AgeFeriadosLocalidades)
                    .HasForeignKey(d => d.AgeDiaFeCodigo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("age_fer_lo_age_dia_fe_fk");

                entity.HasOne(d => d.Age)
                    .WithMany(p => p.AgeFeriadosLocalidades)
                    .HasForeignKey(d => new { d.AgeLocaliCodigo, d.AgeLocaliAgeTipLoCodigo, d.AgeAgeTipLoAgePaisCodigo })
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("age_fer_lo_age_locali_fk");
            });

            modelBuilder.Entity<AgeFeriadosPaises>(entity =>
            {
                entity.HasKey(e => new { e.AgePaisCodigo, e.AgeDiaFeCodigo })
                    .HasName("age_fer_pa_pk");

                entity.HasOne(d => d.AgeDiaFeCodigoNavigation)
                    .WithMany(p => p.AgeFeriadosPaises)
                    .HasForeignKey(d => d.AgeDiaFeCodigo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("age_fer_pa_age_dia_fe_fk");

                entity.HasOne(d => d.AgePaisCodigoNavigation)
                    .WithMany(p => p.AgeFeriadosPaises)
                    .HasForeignKey(d => d.AgePaisCodigo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("age_fer_pa_age_pais_fk");
            });

            modelBuilder.Entity<AgeFirmasDigitales>(entity =>
            {
                entity.HasKey(e => new { e.AgeSucursCodigo, e.AgeSucursAgeLicencCodigo, e.Codigo })
                    .HasName("age_fir_di_pk");

                entity.HasOne(d => d.AgeSucurs)
                    .WithMany(p => p.AgeFirmasDigitales)
                    .HasForeignKey(d => new { d.AgeSucursCodigo, d.AgeSucursAgeLicencCodigo })
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("age_fir_di_age_sucurs_fk");
            });

            modelBuilder.Entity<AgeFormasPagos>(entity =>
            {
                entity.HasKey(e => new { e.Codigo, e.AgeLicencCodigo })
                    .HasName("age_for_pa_pk");

                entity.HasOne(d => d.AgeLicencCodigoNavigation)
                    .WithMany(p => p.AgeFormasPagos)
                    .HasForeignKey(d => d.AgeLicencCodigo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("age_for_pa_age_licenc_fk");
            });

            modelBuilder.Entity<AgeFormasPagosInstFina>(entity =>
            {
                entity.HasKey(e => new { e.Codigo, e.AgeLicencCodigo })
                    .HasName("age_fp_if_pk");

                entity.HasOne(d => d.AgeFranquCodigoNavigation)
                    .WithMany(p => p.AgeFormasPagosInstFinas)
                    .HasForeignKey(d => d.AgeFranquCodigo)
                    .HasConstraintName("age_fp_if_age_franqu_fk");

                entity.HasOne(d => d.AgeLicencCodigoNavigation)
                    .WithMany(p => p.AgeFormasPagosInstFinas)
                    .HasForeignKey(d => d.AgeLicencCodigo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("age_fp_if_age_licenc_fk");

                entity.HasOne(d => d.AgeForPa)
                    .WithMany(p => p.AgeFormasPagosInstFinas)
                    .HasForeignKey(d => new { d.AgeForPaCodigo, d.AgeForPaAgeLicencCodigo })
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("age_fp_if_age_for_pa_fk");

                entity.HasOne(d => d.AgeInsFi)
                    .WithMany(p => p.AgeFormasPagosInstFinas)
                    .HasForeignKey(d => new { d.AgeInsFiCodigo, d.AgeInsFiAgeLicencCodigo })
                    .HasConstraintName("age_fp_if_age_ins_fi_fk");
            });

            modelBuilder.Entity<AgeFranquicias>(entity =>
            {
                entity.HasKey(e => e.Codigo)
                    .HasName("age_franqu_pk");

                entity.Property(e => e.Codigo).ValueGeneratedNever();
            });

            modelBuilder.Entity<AgeIdiomas>(entity =>
            {
                entity.HasKey(e => e.Codigo)
                    .HasName("age_idioma_pk");

                entity.Property(e => e.Codigo).ValueGeneratedNever();
            });

            modelBuilder.Entity<AgeInstitucionesFinancieras>(entity =>
            {
                entity.HasKey(e => new { e.Codigo, e.AgeLicencCodigo })
                    .HasName("age_ins_fi_pk");

                entity.HasOne(d => d.AgeLicencCodigoNavigation)
                    .WithMany(p => p.AgeInstitucionesFinancieras)
                    .HasForeignKey(d => d.AgeLicencCodigo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("age_ins_fi_age_licenc_fk");
            });

            modelBuilder.Entity<AgeLicenciatParamVigencias>(entity =>
            {
                entity.HasKey(e => new { e.Codigo, e.AgeLicencCodigo, e.AgeParGeCodigo })
                    .HasName("age_lic_pv_pk");

                entity.HasOne(d => d.AgeLicencCodigoNavigation)
                    .WithMany(p => p.AgeLicenciatParamVigencia)
                    .HasForeignKey(d => d.AgeLicencCodigo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("age_lic_pv_age_licenc_fk");

                entity.HasOne(d => d.AgeParGeCodigoNavigation)
                    .WithMany(p => p.AgeLicenciatParamVigencia)
                    .HasForeignKey(d => d.AgeParGeCodigo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("age_lic_pv_age_par_ge_fk");
            });

            modelBuilder.Entity<AgeLicenciatariTransaccione>(entity =>
            {
                entity.HasKey(e => new { e.AgeLicencCodigo, e.AgeTransaCodigo, e.AgeTransaAgeAplicaCodigo })
                    .HasName("age_lic_tr_pk");

                entity.HasOne(d => d.AgeLicencCodigoNavigation)
                    .WithMany(p => p.AgeLicenciatariTransacciones)
                    .HasForeignKey(d => d.AgeLicencCodigo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("age_lic_tr_age_licenc_fk");

                entity.HasOne(d => d.AgeTransa)
                    .WithMany(p => p.AgeLicenciatariTransacciones)
                    .HasForeignKey(d => new { d.AgeTransaCodigo, d.AgeTransaAgeAplicaCodigo })
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("age_lic_tr_age_transa_fk");
            });

            modelBuilder.Entity<AgeLicenciatario>(entity =>
            {
                entity.HasKey(e => e.Codigo)
                    .HasName("age_licenc_pk");

                entity.Property(e => e.Codigo).ValueGeneratedNever();

                entity.HasOne(d => d.AgeClaCoCodigoNavigation)
                    .WithMany(p => p.AgeLicenciatarios)
                    .HasForeignKey(d => d.AgeClaCoCodigo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("age_licenc_age_cla_co_fk");

                entity.HasOne(d => d.AgeLicencCodigoNavigation)
                    .WithMany(p => p.InverseAgeLicencCodigoNavigation)
                    .HasForeignKey(d => d.AgeLicencCodigo)
                    .HasConstraintName("age_licenc_age_licenc_fk");

                entity.HasOne(d => d.AgeTipIdCodigoNavigation)
                    .WithMany(p => p.AgeLicenciatarios)
                    .HasForeignKey(d => d.AgeTipIdCodigo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("age_licenc_age_tip_id_fk");
            });

            modelBuilder.Entity<AgeLicenciatariosAplicaSecu>(entity =>
            {
                entity.HasKey(e => new { e.Codigo, e.AgeLicApAgeLicencCodigo, e.AgeLicApAgeAplicaCodigo })
                    .HasName("age_lic_as_pk");

                entity.HasOne(d => d.AgeLicApAge)
                    .WithMany(p => p.AgeLicenciatariosAplicaSecus)
                    .HasForeignKey(d => new { d.AgeLicApAgeLicencCodigo, d.AgeLicApAgeAplicaCodigo })
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("age_lic_as_age_lic_ap_fk");
            });

            modelBuilder.Entity<AgeLicenciatariosAplicaciones>(entity =>
            {
                entity.HasKey(e => new { e.AgeLicencCodigo, e.AgeAplicaCodigo })
                    .HasName("age_lic_ap_pk");

                entity.HasOne(d => d.AgeAplicaCodigoNavigation)
                    .WithMany(p => p.AgeLicenciatariosAplicacions)
                    .HasForeignKey(d => d.AgeAplicaCodigo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("age_lic_ap_age_aplica_fk");

                entity.HasOne(d => d.AgeLicencCodigoNavigation)
                    .WithMany(p => p.AgeLicenciatariosAplicacions)
                    .HasForeignKey(d => d.AgeLicencCodigo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("age_lic_ap_age_licenc_fk");

                entity.HasOne(d => d.AgeRuta)
                    .WithMany(p => p.AgeLicenciatariosAplicacions)
                    .HasForeignKey(d => new { d.AgeRutaAgeLicencCodigo, d.AgeRutaCodigo })
                    .HasConstraintName("age_lic_ap_age_ruta_fk");
            });

            modelBuilder.Entity<AgeLicenciatariosPaise>(entity =>
            {
                entity.HasKey(e => new { e.AgePaisCodigo, e.AgeLicencCodigo })
                    .HasName("age_lic_pa_pk");

                entity.HasOne(d => d.AgeLicencCodigoNavigation)
                    .WithMany(p => p.AgeLicenciatariosPaises)
                    .HasForeignKey(d => d.AgeLicencCodigo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("age_lic_pa_age_licenc_fk");

                entity.HasOne(d => d.AgePaisCodigoNavigation)
                    .WithMany(p => p.AgeLicenciatariosPaises)
                    .HasForeignKey(d => d.AgePaisCodigo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("age_lic_pa_age_pais_fk");

                entity.HasOne(d => d.AgeLicPaAge)
                    .WithMany(p => p.InverseAgeLicPaAge)
                    .HasForeignKey(d => new { d.AgeLicPaAgePaisCodigo, d.AgeLicPaAgeLicencCodigo })
                    .HasConstraintName("age_lic_pa_age_lic_pa_fk");
            });

            modelBuilder.Entity<AgeLicenciatariosSecuencia>(entity =>
            {
                entity.HasKey(e => new { e.AgeLicencCodigo, e.Codigo })
                    .HasName("age_lic_se_pk");

                entity.HasOne(d => d.AgeLicencCodigoNavigation)
                    .WithMany(p => p.AgeLicenciatariosSecuencia)
                    .HasForeignKey(d => d.AgeLicencCodigo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("age_lic_se_age_licenc_fk");
            });

            modelBuilder.Entity<AgeLocalidades>(entity =>
            {
                entity.HasKey(e => new { e.Codigo, e.AgeTipLoCodigo, e.AgeTipLoAgePaisCodigo })
                    .HasName("age_locali_pk");

                entity.HasOne(d => d.AgeIdiomaCodigoNavigation)
                    .WithMany(p => p.AgeLocalidades)
                    .HasForeignKey(d => d.AgeIdiomaCodigo)
                    .HasConstraintName("age_locali_age_idioma_fk");

                entity.HasOne(d => d.AgeMonedaCodigoNavigation)
                    .WithMany(p => p.AgeLocalidades)
                    .HasForeignKey(d => d.AgeMonedaCodigo)
                    .HasConstraintName("age_locali_age_moneda_fk");

                entity.HasOne(d => d.AgeTipLo)
                    .WithMany(p => p.AgeLocalidades)
                    .HasForeignKey(d => new { d.AgeTipLoCodigo, d.AgeTipLoAgePaisCodigo })
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("age_locali_age_tip_lo_fk");

                entity.HasOne(d => d.Age)
                    .WithMany(p => p.InverseAge)
                    .HasForeignKey(d => new { d.AgeLocaliCodigo, d.AgeLocaliAgeTipLoCodigo, d.AgeAgeTipLoAgePaisCodigo })
                    .HasConstraintName("age_locali_age_locali_fk");
            });

            modelBuilder.Entity<AgeLogErrores>(entity =>
            {
                entity.HasKey(e => new { e.Codigo, e.AgeLicApAgeLicencCodigo, e.AgeLicApAgeAplicaCodigo })
                    .HasName("age_log_er_pk");

                entity.HasOne(d => d.AgeLicApAge)
                    .WithMany(p => p.AgeLogErrores)
                    .HasForeignKey(d => new { d.AgeLicApAgeLicencCodigo, d.AgeLicApAgeAplicaCodigo })
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("age_log_er_age_lic_ap_fk");
            });

            modelBuilder.Entity<AgeLogUsuarios>(entity =>
            {
                entity.HasKey(e => new { e.AgeUsuariAgeLicencCodigo, e.AgeUsuariCodigo, e.Codigo })
                    .HasName("age_log_usuarios_pkey");
            });

            modelBuilder.Entity<AgeMensajes>(entity =>
            {
                entity.HasKey(e => e.Codigo)
                    .HasName("age_mensaj_pk");

                entity.Property(e => e.Codigo).ValueGeneratedNever();
            });

            modelBuilder.Entity<AgeMensajesIdiomas>(entity =>
            {
                entity.HasKey(e => new { e.AgeIdiomaCodigo, e.AgeMensajCodigo })
                    .HasName("age_men_id_pk");

                entity.HasOne(d => d.AgeIdiomaCodigoNavigation)
                    .WithMany(p => p.AgeMensajesIdiomas)
                    .HasForeignKey(d => d.AgeIdiomaCodigo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("age_men_id_age_idioma_fk");

                entity.HasOne(d => d.AgeMensajCodigoNavigation)
                    .WithMany(p => p.AgeMensajesIdiomas)
                    .HasForeignKey(d => d.AgeMensajCodigo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("age_men_id_age_mensaj_fk");
            });

            modelBuilder.Entity<AgeModalidad>(entity =>
            {
                entity.HasKey(e => new { e.AgeLicencCodigo, e.Codigo })
                    .HasName("age_modalidad_pkey");
            });

            modelBuilder.Entity<AgeMonedas>(entity =>
            {
                entity.HasKey(e => e.Codigo)
                    .HasName("age_moneda_pk");

                entity.Property(e => e.Codigo).ValueGeneratedNever();
            });

            modelBuilder.Entity<AgeMonedasCotizaciones>(entity =>
            {
                entity.HasKey(e => new { e.Codigo, e.AgeLicencCodigo })
                    .HasName("age_mon_co_pk");

                entity.HasOne(d => d.AgeLicencCodigoNavigation)
                    .WithMany(p => p.AgeMonedasCotizaciones)
                    .HasForeignKey(d => d.AgeLicencCodigo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("age_mon_co_age_licenc_fk");

                entity.HasOne(d => d.AgeMonedaCodigoNavigation)
                    .WithMany(p => p.AgeMonedasCotizacioneAgeMonedaCodigoNavigations)
                    .HasForeignKey(d => d.AgeMonedaCodigo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("age_mon_co_age_moneda_ser_d_fk");

                entity.HasOne(d => d.AgeMonedaCodigoSerANavigation)
                    .WithMany(p => p.AgeMonedasCotizacioneAgeMonedaCodigoSerANavigations)
                    .HasForeignKey(d => d.AgeMonedaCodigoSerA)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("age_mon_co_age_moneda_fk");
            });

            modelBuilder.Entity<AgeNotificacionesPush>(entity =>
            {
                entity.HasKey(e => new { e.AgeLicencCodigo, e.Codigo })
                    .HasName("age_notificaciones_push_pkey");
            });

            modelBuilder.Entity<AgePaises>(entity =>
            {
                entity.HasKey(e => e.Codigo)
                    .HasName("age_pais_pk");

                entity.Property(e => e.Codigo).ValueGeneratedNever();
            });

            modelBuilder.Entity<AgePaisesIdiomas>(entity =>
            {
                entity.HasKey(e => new { e.AgePaisCodigo, e.AgeIdiomaCodigo })
                    .HasName("age_pai_id_pk");

                entity.HasOne(d => d.AgeIdiomaCodigoNavigation)
                    .WithMany(p => p.AgePaisesIdiomas)
                    .HasForeignKey(d => d.AgeIdiomaCodigo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("age_pai_id_age_idioma_fk");

                entity.HasOne(d => d.AgePaisCodigoNavigation)
                    .WithMany(p => p.AgePaisesIdiomas)
                    .HasForeignKey(d => d.AgePaisCodigo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("age_pai_id_age_pais_fk");
            });

            modelBuilder.Entity<AgePaisesMonedas>(entity =>
            {
                entity.HasKey(e => new { e.AgeMonedaCodigo, e.AgePaisCodigo })
                    .HasName("age_pai_mo_pk");

                entity.HasOne(d => d.AgeMonedaCodigoNavigation)
                    .WithMany(p => p.AgePaisesMoneda)
                    .HasForeignKey(d => d.AgeMonedaCodigo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("age_pai_mo_age_moneda_fk");

                entity.HasOne(d => d.AgePaisCodigoNavigation)
                    .WithMany(p => p.AgePaisesMoneda)
                    .HasForeignKey(d => d.AgePaisCodigo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("age_pai_mo_age_pais_fk");
            });

            modelBuilder.Entity<AgeParamGeneralVigencias>(entity =>
            {
                entity.HasKey(e => new { e.Codigo, e.AgeParGeCodigo })
                    .HasName("age_par_gv_pk");

                entity.HasOne(d => d.AgeParGeCodigoNavigation)
                    .WithMany(p => p.AgeParamGeneralVigencia)
                    .HasForeignKey(d => d.AgeParGeCodigo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("age_par_gv_age_par_ge_fk");
            });

            modelBuilder.Entity<AgeParametrosGenerales>(entity =>
            {
                entity.HasKey(e => e.Codigo)
                    .HasName("age_par_ge_pk");

                entity.Property(e => e.Codigo).ValueGeneratedNever();
            });

            modelBuilder.Entity<AgePerfiles>(entity =>
            {
                entity.HasKey(e => new { e.Codigo, e.AgeLicencCodigo })
                    .HasName("age_perfil_pk");

                entity.HasOne(d => d.AgeLicencCodigoNavigation)
                    .WithMany(p => p.AgePerfiles)
                    .HasForeignKey(d => d.AgeLicencCodigo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("age_perfil_age_licenc_fk");
            });

            modelBuilder.Entity<AgePerfilesTransacciones>(entity =>
            {
                entity.HasKey(e => new { e.Codigo, e.AgePerfilCodigo, e.AgePerfilAgeLicencCodigo })
                    .HasName("age_per_tr_pk");

                entity.HasOne(d => d.AgePerfil)
                    .WithMany(p => p.AgePerfilesTransacciones)
                    .HasForeignKey(d => new { d.AgePerfilCodigo, d.AgePerfilAgeLicencCodigo })
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("age_per_tr_age_perfil_fk");

                entity.HasOne(d => d.AgeTransa)
                    .WithMany(p => p.AgePerfilesTransacciones)
                    .HasForeignKey(d => new { d.AgeTransaCodigo, d.AgeTransaAgeAplicaCodigo })
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("age_per_tr_age_transa_fk");
            });

            modelBuilder.Entity<AgePersonas>(entity =>
            {
                entity.HasKey(e => new { e.Codigo, e.AgeLicencCodigo })
                    .HasName("age_person_pk");

                entity.HasOne(d => d.AgeLicencCodigoNavigation)
                    .WithMany(p => p.AgePersonas)
                    .HasForeignKey(d => d.AgeLicencCodigo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("age_person_age_licenc_fk");

                entity.HasOne(d => d.AgeTipIdCodigoNavigation)
                    .WithMany(p => p.AgePersonas)
                    .HasForeignKey(d => d.AgeTipIdCodigo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("age_person_age_tip_id_fk");

                entity.HasOne(d => d.Age)
                    .WithMany(p => p.AgePersonas)
                    .HasForeignKey(d => new { d.AgeLocaliCodigo, d.AgeLocaliAgeTipLoCodigo, d.AgeAgeTipLoAgePaisCodigo })
                    .HasConstraintName("age_person_age_locali_fk");
            });

            modelBuilder.Entity<AgePuntosEmisiones>(entity =>
            {
                entity.HasKey(e => new { e.Codigo, e.AgeSucursCodigo, e.AgeSucursAgeLicencCodigo })
                    .HasName("age_pun_em_pk");

                entity.HasOne(d => d.AgeSucurs)
                    .WithMany(p => p.AgePuntosEmisions)
                    .HasForeignKey(d => new { d.AgeSucursCodigo, d.AgeSucursAgeLicencCodigo })
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("age_pun_em_age_sucurs_fk");
            });

            modelBuilder.Entity<AgeRutas>(entity =>
            {
                entity.HasKey(e => new { e.AgeLicencCodigo, e.Codigo })
                    .HasName("age_ruta_pk");

                entity.HasOne(d => d.AgeLicencCodigoNavigation)
                    .WithMany(p => p.AgeRuta)
                    .HasForeignKey(d => d.AgeLicencCodigo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("age_ruta_age_licenc_fk");
            });

            modelBuilder.Entity<AgeSecuenciasPrimaria>(entity =>
            {
                entity.HasKey(e => e.Codigo)
                    .HasName("age_sec_pr_pk");

                entity.Property(e => e.Codigo).ValueGeneratedNever();
            });

            modelBuilder.Entity<AgeSistemas>(entity =>
            {
                entity.HasKey(e => e.Codigo)
                    .HasName("age_sistem_pk");

                entity.Property(e => e.Codigo).ValueGeneratedNever();
            });

            modelBuilder.Entity<AgeSistemasAplicaciones>(entity =>
            {
                entity.HasKey(e => new { e.AgeAplicaCodigo, e.AgeSistemCodigo })
                    .HasName("age_sis_ap_pk");

                entity.HasOne(d => d.AgeAplicaCodigoNavigation)
                    .WithMany(p => p.AgeSistemasAplicaciones)
                    .HasForeignKey(d => d.AgeAplicaCodigo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("age_sis_ap_age_aplica_fk");

                entity.HasOne(d => d.AgeSistemCodigoNavigation)
                    .WithMany(p => p.AgeSistemasAplicaciones)
                    .HasForeignKey(d => d.AgeSistemCodigo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("age_sis_ap_age_sistem_fk");
            });

            modelBuilder.Entity<AgeSubAplicaciones>(entity =>
            {
                entity.HasKey(e => new { e.Codigo, e.AgeLicApAgeLicencCodigo, e.AgeLicApAgeAplicaCodigo })
                    .HasName("age_sub_ap_pk");

                entity.HasOne(d => d.AgeLicApAge)
                    .WithMany(p => p.AgeSubAplicaciones)
                    .HasForeignKey(d => new { d.AgeLicApAgeLicencCodigo, d.AgeLicApAgeAplicaCodigo })
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("age_sub_ap_age_lic_ap_fk");

                entity.HasOne(d => d.Ag)
                    .WithMany(p => p.InverseAg)
                    .HasForeignKey(d => new { d.AgeSubApCodigo, d.AgageLicApAgeLicencCodigo, d.AgageLicApAgeAplicaCodigo })
                    .HasConstraintName("age_sub_ap_age_sub_ap_fk");
            });

            modelBuilder.Entity<AgeSucursales>(entity =>
            {
                entity.HasKey(e => new { e.Codigo, e.AgeLicencCodigo })
                    .HasName("age_sucurs_pk");

                entity.HasOne(d => d.AgeLicencCodigoNavigation)
                    .WithMany(p => p.AgeSucursales)
                    .HasForeignKey(d => d.AgeLicencCodigo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("age_sucurs_age_licenc_fk");

                entity.HasOne(d => d.AgeSucurs)
                    .WithMany(p => p.InverseAgeSucurs)
                    .HasForeignKey(d => new { d.AgeSucursCodigo, d.AgeSucursAgeLicencCodigo })
                    .HasConstraintName("age_sucurs_age_sucurs_fk");

                entity.HasOne(d => d.AgeTipSu)
                    .WithMany(p => p.AgeSucursales)
                    .HasForeignKey(d => new { d.AgeTipSuCodigo, d.AgeTipSuAgeLicencCodigo })
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("age_sucurs_age_tip_su_fk");

                entity.HasOne(d => d.Age)
                    .WithMany(p => p.AgeSucursales)
                    .HasForeignKey(d => new { d.AgeLocaliCodigo, d.AgeLocaliAgeTipLoCodigo, d.AgeAgeTipLoAgePaisCodigo })
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("age_sucurs_age_locali_fk");
            });

            modelBuilder.Entity<AgeTiposDirecciones>(entity =>
            {
                entity.HasKey(e => e.Codigo)
                    .HasName("age_tip_di_pk");

                entity.Property(e => e.Codigo).ValueGeneratedNever();
            });

            modelBuilder.Entity<AgeTiposIdentificaciones>(entity =>
            {
                entity.HasKey(e => e.Codigo)
                    .HasName("age_tip_id_pk");

                entity.Property(e => e.Codigo).ValueGeneratedNever();
            });

            modelBuilder.Entity<AgeTiposLocalidades>(entity =>
            {
                entity.HasKey(e => new { e.Codigo, e.AgePaisCodigo })
                    .HasName("age_tip_lo_pk");

                entity.HasOne(d => d.AgePaisCodigoNavigation)
                    .WithMany(p => p.AgeTiposLocalidades)
                    .HasForeignKey(d => d.AgePaisCodigo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("age_tip_lo_age_pais_fk");

                entity.HasOne(d => d.AgeTipLo)
                    .WithMany(p => p.InverseAgeTipLo)
                    .HasForeignKey(d => new { d.AgeTipLoCodigo, d.AgeTipLoAgePaisCodigo })
                    .HasConstraintName("age_tip_lo_age_tip_lo_fk");
            });

            modelBuilder.Entity<AgeTiposPersonas>(entity =>
            {
                entity.HasKey(e => new { e.AgeLicencCodigo, e.Codigo })
                    .HasName("age_tipos_personas_pkey");
            });

            modelBuilder.Entity<AgeTiposSucursales>(entity =>
            {
                entity.HasKey(e => new { e.Codigo, e.AgeLicencCodigo })
                    .HasName("age_tip_su_pk");

                entity.HasOne(d => d.AgeLicencCodigoNavigation)
                    .WithMany(p => p.AgeTiposSucursales)
                    .HasForeignKey(d => d.AgeLicencCodigo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("age_tip_su_age_licenc_fk");
            });

            modelBuilder.Entity<AgeTransacciones>(entity =>
            {
                entity.HasKey(e => new { e.Codigo, e.AgeAplicaCodigo })
                    .HasName("age_transa_pk");

                entity.HasOne(d => d.AgeAplicaCodigoNavigation)
                    .WithMany(p => p.AgeTransacciones)
                    .HasForeignKey(d => d.AgeAplicaCodigo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("age_transa_age_aplica_fk");

                entity.HasOne(d => d.AgeTransa)
                    .WithMany(p => p.InverseAgeTransa)
                    .HasForeignKey(d => new { d.AgeTransaCodigo, d.AgeTransaAgeAplicaCodigo })
                    .HasConstraintName("age_transa_age_transa_fk");
            });

            modelBuilder.Entity<AgeTransaccionesIdiomas>(entity =>
            {
                entity.HasKey(e => new { e.AgeTransaCodigo, e.AgeTransaAgeAplicaCodigo, e.AgeIdiomaCodigo })
                    .HasName("age_trx_id_pk");

                entity.HasOne(d => d.AgeIdiomaCodigoNavigation)
                    .WithMany(p => p.AgeTransaccionesIdiomas)
                    .HasForeignKey(d => d.AgeIdiomaCodigo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("age_trx_id_age_idioma_fk");

                entity.HasOne(d => d.AgeTransa)
                    .WithMany(p => p.AgeTransaccionesIdiomas)
                    .HasForeignKey(d => new { d.AgeTransaCodigo, d.AgeTransaAgeAplicaCodigo })
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("age_trx_id_age_transa_fk");
            });

            modelBuilder.Entity<AgeUnidadesTiempo>(entity =>
            {
                entity.HasKey(e => new { e.AgeLicencCodigo, e.Codigo })
                    .HasName("age_unidades_tiempo_pkey");
            });

            modelBuilder.Entity<AgeUsuarios>(entity =>
            {
                entity.HasKey(e => new { e.Codigo, e.AgeLicencCodigo })
                    .HasName("age_usuari_pk");

                entity.HasOne(d => d.AgeLicencCodigoNavigation)
                    .WithMany(p => p.AgeUsuarios)
                    .HasForeignKey(d => d.AgeLicencCodigo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("age_usuari_age_licenc_fk");

                entity.HasOne(d => d.AgeTipIdCodigoNavigation)
                    .WithMany(p => p.AgeUsuarios)
                    .HasForeignKey(d => d.AgeTipIdCodigo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("age_usuari_age_tip_id_fk");

                entity.HasOne(d => d.AgePerson)
                    .WithMany(p => p.AgeUsuarios)
                    .HasForeignKey(d => new { d.AgePersonCodigo, d.AgePersonAgeLicencCodigo })
                    .HasConstraintName("age_usuari_age_person_fk");

                entity.HasOne(d => d.AgeSucurs)
                    .WithMany(p => p.AgeUsuarios)
                    .HasForeignKey(d => new { d.AgeSucursCodigo, d.AgeSucursAgeLicencCodigo })
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("age_usuari_age_sucurs_fk");
            });

            modelBuilder.Entity<AgeUsuarioParamVigencias>(entity =>
            {
                entity.HasKey(e => new { e.AgeParGeCodigo, e.AgeUsuariCodigo, e.AgeUsuariAgeLicencCodigo, e.Codigo })
                    .HasName("age_usu_pv_pk");

                entity.HasOne(d => d.AgeParGeCodigoNavigation)
                    .WithMany(p => p.AgeUsuarioParamVigencia)
                    .HasForeignKey(d => d.AgeParGeCodigo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("age_usu_pv_age_par_ge_fk");

                entity.HasOne(d => d.AgeUsuari)
                    .WithMany(p => p.AgeUsuarioParamVigencia)
                    .HasForeignKey(d => new { d.AgeUsuariCodigo, d.AgeUsuariAgeLicencCodigo })
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("age_usu_pv_age_usuari_fk");
            });

            modelBuilder.Entity<AgeUsuariosPerfiles>(entity =>
            {
                entity.HasKey(e => new { e.Codigo, e.AgeUsuariCodigo, e.AgeUsuariAgeLicencCodigo })
                    .HasName("age_usu_pe_pk");

                entity.HasOne(d => d.AgePerfil)
                    .WithMany(p => p.AgeUsuariosPerfiles)
                    .HasForeignKey(d => new { d.AgePerfilCodigo, d.AgePerfilAgeLicencCodigo })
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("age_usu_pe_age_perfil_fk");

                entity.HasOne(d => d.AgeUsuari)
                    .WithMany(p => p.AgeUsuariosPerfiles)
                    .HasForeignKey(d => new { d.AgeUsuariCodigo, d.AgeUsuariAgeLicencCodigo })
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("age_usu_pe_age_usuari_fk");
            });

            modelBuilder.Entity<AgeUsuariosPuntoEmision>(entity =>
            {
                entity.HasKey(e => new { e.AgePunEmCodigo, e.AgePunEmAgeSucursCodigo, e.AgageSucursAgeLicencCodigo, e.AgeUsuariCodigo, e.AgeUsuariAgeLicencCodigo })
                    .HasName("age_usu_pm_pk");

                entity.HasOne(d => d.AgeUsuari)
                    .WithMany(p => p.AgeUsuariosPuntoEmisions)
                    .HasForeignKey(d => new { d.AgeUsuariCodigo, d.AgeUsuariAgeLicencCodigo })
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("age_usu_pm_age_usuari_fk");

                entity.HasOne(d => d.Ag)
                    .WithMany(p => p.AgeUsuariosPuntoEmisions)
                    .HasForeignKey(d => new { d.AgePunEmCodigo, d.AgePunEmAgeSucursCodigo, d.AgageSucursAgeLicencCodigo })
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("age_usu_pm_age_pun_em_fk");
            });

            modelBuilder.Entity<OauthClientDetail>(entity =>
            {
                entity.HasKey(e => e.ClientId)
                    .HasName("oauth_client_details_pkey");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
