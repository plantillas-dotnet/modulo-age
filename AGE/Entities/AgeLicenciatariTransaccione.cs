﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace AGE.Entities
{
    [Table("age_licenciatari_transacciones", Schema = "sasf_user_desa")]
    public partial class AgeLicenciatariTransaccione
    {
        [Key]
        [Column("age_licenc_codigo")]
        public int AgeLicencCodigo { get; set; }
        [Key]
        [Column("age_transa_age_aplica_codigo")]
        public int AgeTransaAgeAplicaCodigo { get; set; }
        [Key]
        [Column("age_transa_codigo")]
        public int AgeTransaCodigo { get; set; }
        [Column("estado")]
        [StringLength(1)]
        public string Estado { get; set; } = null!;
        [Column("fecha_estado", TypeName = "timestamp(0) without time zone")]
        public DateTime FechaEstado { get; set; }
        [Column("observacion_estado")]
        [StringLength(2000)]
        public string? ObservacionEstado { get; set; }
        [Column("usuario_ingreso")]
        public long UsuarioIngreso { get; set; }
        [Column("fecha_ingreso", TypeName = "timestamp(0) without time zone")]
        public DateTime FechaIngreso { get; set; }
        [Column("ubicacion_ingreso")]
        [StringLength(200)]
        public string UbicacionIngreso { get; set; } = null!;
        [Column("usuario_modificacion")]
        public long? UsuarioModificacion { get; set; }
        [Column("fecha_modificacion", TypeName = "timestamp(0) without time zone")]
        public DateTime? FechaModificacion { get; set; }
        [Column("ubicacion_modificacion")]
        [StringLength(200)]
        public string? UbicacionModificacion { get; set; }

        [ForeignKey("AgeLicencCodigo")]
        [InverseProperty("AgeLicenciatariTransacciones")]
        public virtual AgeLicenciatario AgeLicencCodigoNavigation { get; set; } = null!;
        [ForeignKey("AgeTransaCodigo,AgeTransaAgeAplicaCodigo")]
        [InverseProperty("AgeLicenciatariTransacciones")]
        public virtual AgeTransacciones AgeTransa { get; set; } = null!;
    }
}
