﻿
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AGE.Utils;
using Newtonsoft.Json;

namespace AGE.Entities
{
    [Table("age_usuarios_perfiles", Schema = "sasf_user_desa")]
    public partial class AgeUsuariosPerfiles
    {
        [Key]
        [Column("age_usuari_age_licenc_codigo")]
        [Required(ErrorMessage = "El código del licenciatario no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de licenciatario debe ser un número entero mayor a cero.")]
        public int AgeUsuariAgeLicencCodigo { get; set; }

        [Key]
        [Column("age_usuari_codigo")]
        [Required(ErrorMessage = "El código del usuario no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código del usuario debe ser un número entero mayor a cero.")]
        public long AgeUsuariCodigo { get; set; }

        [Key]
        [Column("codigo")]
        [Required(ErrorMessage = "El código del usuario perfil no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código del ususario perfil debe ser un número entero mayor a cero.")]
        public int Codigo { get; set; }

        [Required(ErrorMessage = "El código de licenciatario del perfil no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de licenciatario del perfil debe ser un número entero mayor a cero.")]
        [Column("age_perfil_age_licenc_codigo")]
        public int AgePerfilAgeLicencCodigo { get; set; }

        [Required(ErrorMessage = "El código de perfil no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de perfil debe ser un número entero mayor a cero.")]
        [Column("age_perfil_codigo")]
        public int AgePerfilCodigo { get; set; }

        [Required(ErrorMessage = "La fecha desde no puede ser nula.")]
        [Column("fecha_desde", TypeName = "timestamp without time zone")]
        public DateTime FechaDesde { get; set; }

        [Column("fecha_hasta", TypeName = "timestamp without time zone")]
        public DateTime? FechaHasta { get; set; }

        [Column("estado")]
        [Required(ErrorMessage = "El estado no puede ser nulo.")]
        [RegularExpression("^[AI]$", ErrorMessage = $"El campo estado debe ser {Globales.ESTADO_ACTIVO} o {Globales.ESTADO_INACTIVO}.")]
        public string? Estado { get; set; }

        [Required(ErrorMessage = "Fecha estado no puede ser nulo.")]
        [Column("fecha_estado", TypeName = "timestamp without time zone")]
        public DateTime FechaEstado { get; set; }

        [Column("observacion_estado")]
        [StringLength(2000,ErrorMessage = "La observación debe contener máximo 2000 caracteres.")]
        public string? ObservacionEstado { get; set; }

        [Column("usuario_ingreso")]
        [Required(ErrorMessage = "El campo usuario ingreso no puede ser nulo.")]
        [Range(1, long.MaxValue, ErrorMessage = "El código del usuario de ingreso debe ser mayor a 0.")]
        public long? UsuarioIngreso { get; set; }

        [Required(ErrorMessage = "La fecha de ingreso no puede ser nula.")]
        [Column("fecha_ingreso", TypeName = "timestamp without time zone")]
        public DateTime? FechaIngreso { get; set; }

        [Column("ubicacion_ingreso")]
        [Required(ErrorMessage = "La ubicación de ingreso no puede ser nula.")]
        [RegularExpression("^[0-9.]+$", ErrorMessage = "La ubicación de ingreso debe contener máximo 200 caracteres.")]
        public string? UbicacionIngreso { get; set; }

        [Column("usuario_modificacion")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El usuario modificación debe ser un número entero mayor a cero.")]
        public long? UsuarioModificacion { get; set; }

        [Column("fecha_modificacion", TypeName = "timestamp without time zone")]
        public DateTime? FechaModificacion { get; set; }

        [Column("ubicacion_modificacion")]
        [RegularExpression("^[0-9.]+$", ErrorMessage = "La ubicación de modificación debe contener máximo 200 caracteres.")]
        public string? UbicacionModificacion { get; set; }

        [ForeignKey("AgePerfilCodigo,AgePerfilAgeLicencCodigo")]
        [InverseProperty("AgeUsuariosPerfiles")]
        public virtual AgePerfiles AgePerfil { get; set; } = null!;

        [ForeignKey("AgeUsuariCodigo,AgeUsuariAgeLicencCodigo")]
        [InverseProperty("AgeUsuariosPerfiles")]
        public virtual AgeUsuarios AgeUsuari { get; set; } = null!;
    }
}
