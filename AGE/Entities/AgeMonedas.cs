
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AGE.Utils;

namespace AGE.Entities
{
    [Table("age_monedas", Schema = "sasf_user_desa")]
    public partial class AgeMonedas
    {
        public AgeMonedas()
        {
            AgeLocalidades = new HashSet<AgeLocalidades>();
            AgeMonedasCotizacioneAgeMonedaCodigoNavigations = new HashSet<AgeMonedasCotizaciones>();
            AgeMonedasCotizacioneAgeMonedaCodigoSerANavigations = new HashSet<AgeMonedasCotizaciones>();
            AgePaisesMoneda = new HashSet<AgePaisesMonedas>();
        }

        [Key]
        [Column("codigo")]
        [Required(ErrorMessage = "El código no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código debe ser un número entero mayor a cero.")]
        public int Codigo { get; set; }

        [Column("codigo_externo")]
        [Required(ErrorMessage = "El código externo no puede ser nulo.")]
        [StringLength(3, ErrorMessage = "El código externo debe contener máximo 3 caracteres.")]
        public string CodigoExterno { get; set; } = null!;

        [Column("descripcion")]
        [Required(ErrorMessage = "La descripción no puede ser nula.")]
        [StringLength(200, ErrorMessage = "La descripción debe contener máximo 200 caracteres.")]
        public string Descripcion { get; set; } = null!;

        [Column("moneda_simbolo")]
        [Required(ErrorMessage = "El símbolo de la moneda no puede ser nulo.")]
        [StringLength(1, ErrorMessage = "El símbolo de la moneda debe contener máximo 1 caracteres.")]
        public string MonedaSimbolo { get; set; } = null!;

        [Column("moneda_separador_decimal")]
        [Required(ErrorMessage = "El separador decimal no puede ser nulo.")]
        [RegularExpression("^[.,]$", ErrorMessage = "El separador decimal debe ser un punto (.) o una coma (,).")]
        public string MonedaSeparadorDecimal { get; set; } = null!;

        [Column("estado")]
        [Required(ErrorMessage = "El estado no puede ser nulo.")]
        [RegularExpression("^[AI]$", ErrorMessage = $"El campo estado debe ser {Globales.ESTADO_ACTIVO} o {Globales.ESTADO_INACTIVO}.")]
        public string Estado { get; set; } = null!;

        [Column("fecha_estado", TypeName = "timestamp(0) without time zone")]
        [Required(ErrorMessage = "La fecha de estado no puede ser nula.")]
        public DateTime FechaEstado { get; set; }

        [Column("fecha_ingreso", TypeName = "timestamp(0) without time zone")]
        [Required(ErrorMessage = "La fecha de ingreso no puede ser nula.")]
        public DateTime FechaIngreso { get; set; }

        [Column("ubicacion_ingreso")]
        public string UbicacionIngreso { get; set; } = null!;

        [Column("observacion_estado")]
        [StringLength(2000,ErrorMessage = "La observación debe contener máximo 2000 caracteres.")]
        public string? ObservacionEstado { get; set; }

        [Column("usuario_ingreso")]
        [Required(ErrorMessage = "El campo usuario ingreso no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El campo usuario ingreso debe ser un número entero mayor a cero.")]
        public long UsuarioIngreso { get; set; }


        [Column("usuario_modificacion")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El usuario modificación debe ser un número entero mayor a cero.")]
        public long? UsuarioModificacion { get; set; }

        [Column("fecha_modificacion", TypeName = "timestamp(0) without time zone")]
        public DateTime? FechaModificacion { get; set; }

        [Column("ubicacion_modificacion")]
        [RegularExpression("^[0-9.]+$", ErrorMessage = "La ubicación de modificación debe contener máximo 200 caracteres.")]
        public string? UbicacionModificacion { get; set; }

        [InverseProperty("AgeMonedaCodigoNavigation")]
        public virtual ICollection<AgeLocalidades> AgeLocalidades { get; set; }
        [InverseProperty("AgeMonedaCodigoNavigation")]
        public virtual ICollection<AgeMonedasCotizaciones> AgeMonedasCotizacioneAgeMonedaCodigoNavigations { get; set; }
        [InverseProperty("AgeMonedaCodigoSerANavigation")]
        public virtual ICollection<AgeMonedasCotizaciones> AgeMonedasCotizacioneAgeMonedaCodigoSerANavigations { get; set; }
        [InverseProperty("AgeMonedaCodigoNavigation")]
        public virtual ICollection<AgePaisesMonedas> AgePaisesMoneda { get; set; }
    }
}
