﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AGE.Utils;
using Microsoft.EntityFrameworkCore;

namespace AGE.Entities
{
    [Table("age_mensajes", Schema = "sasf_user_desa")]
    public partial class AgeMensajes
    {
        public AgeMensajes()
        {
            AgeMensajesIdiomas = new HashSet<AgeMensajesIdiomas>();
        }

        [Key]
        [Column("codigo")]
        [Required(ErrorMessage = "El código de idioma no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código idioma debe ser un número entero mayor a cero.")]
        public long Codigo { get; set; }


        [Column("accion_msg")]
        [Required(ErrorMessage = "La acción del mensaje no puede ser nula.")]
        [RegularExpression("^[DC]$", ErrorMessage = $"La acción del mensaje debe ser {Globales.ACCION_MENSAJE_D} o {Globales.ACCION_MENSAJE_C}.")]
        public string AccionMsg { get; set; } = null!;


        [Required(ErrorMessage = "El estado no puede ser nulo.")]
        [Column("estado")]
        [RegularExpression("^[AI]$", ErrorMessage = $"El campo estado debe ser {Globales.ESTADO_ACTIVO} o {Globales.ESTADO_INACTIVO}.")]
        public string Estado { get; set; } = null!;


        [Required(ErrorMessage = "La fecha de estado no puede ser nula.")]
        [Column("fecha_estado", TypeName = "timestamp(0) without time zone")]
        public DateTime FechaEstado { get; set; }


        [Column("observacion_estado")]
        [StringLength(2000, ErrorMessage = "La observación del estado debe contener máximo 2000 caracteres.")]
        public string? ObservacionEstado { get; set; }


        [Required(ErrorMessage = "El campo usuario ingreso no puede ser nulo.")]
        [Column("usuario_ingreso")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El campo usuario ingreso debe ser un número entero mayor a cero.")]
        public long UsuarioIngreso { get; set; }


        [Required(ErrorMessage = "La fecha de ingreso no puede ser nula.")]
        [Column("fecha_ingreso", TypeName = "timestamp(0) without time zone")]
        public DateTime FechaIngreso { get; set; }


        [Required(ErrorMessage = "La ubicación de ingreso no puede ser nula.")]
        [Column("ubicacion_ingreso")]
        [RegularExpression("^[0-9.]+$", ErrorMessage = "La ubicación de ingreso debe contener máximo 200 caracteres.")]
        public string UbicacionIngreso { get; set; }


        [Column("usuario_modificacion")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El usuario modificación debe ser un número entero mayor a cero.")]
        public long? UsuarioModificacion { get; set; }


        [Column("fecha_modificacion", TypeName = "timestamp(0) without time zone")]
        public DateTime? FechaModificacion { get; set; }


        [RegularExpression("^[0-9.]+$", ErrorMessage = "La ubicación de modificación debe contener máximo 200 caracteres.")]
        [Column("ubicacion_modificacion")]
        public string? UbicacionModificacion { get; set; }


        [InverseProperty("AgeMensajCodigoNavigation")]
        public virtual ICollection<AgeMensajesIdiomas> AgeMensajesIdiomas { get; set; }
    }
}
