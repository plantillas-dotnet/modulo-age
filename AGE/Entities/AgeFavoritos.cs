﻿using AGE.Utils;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AGE.Entities
{
    [Table("age_favoritos", Schema = "sasf_user_desa")]
    public partial class AgeFavoritos
    {
        [Key]
        [Column("age_transa_age_aplica_codigo")]
        [Required(ErrorMessage = "El código de aplicación no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de aplicación debe ser un número entero mayor a cero.")]
        public int AgeTransaAgeAplicaCodigo { get; set; }

        [Key]
        [Column("age_transa_codigo")]
        [Required(ErrorMessage = "El código de la transacción no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de la transacción debe ser un número entero mayor a cero.")]
        public int AgeTransaCodigo { get; set; }

        [Key]
        [Column("age_usuari_age_licenc_codigo")]
        [Required(ErrorMessage = "El código de licenciatario no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de licenciatario debe ser un número entero mayor a cero.")]
        public int AgeUsuariAgeLicencCodigo { get; set; }

        [Key]
        [Required(ErrorMessage = "El código del usuario no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código del usuario debe ser un número entero mayor a cero.")]
        [Column("age_usuari_codigo")]
        public long AgeUsuariCodigo { get; set; }

        [Key]
        [Column("codigo")]
        [Required(ErrorMessage = "El código del favorito no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código del favorito debe ser un número entero mayor a cero.")]
        public int Codigo { get; set; }

        [Required(ErrorMessage = "La fecha desde no puede ser nula.")]
        [Column("fecha_desde", TypeName = "timestamp without time zone")]
        public DateTime FechaDesde { get; set; }

        [Column("fecha_hasta", TypeName = "timestamp without time zone")]
        public DateTime? FechaHasta { get; set; }

        [Column("estado")]
        [Required(ErrorMessage = "El estado no puede ser nulo.")]
        [RegularExpression("^[AI]$", ErrorMessage = $"El campo estado debe ser {Globales.ESTADO_ACTIVO} o {Globales.ESTADO_INACTIVO}.")]
        public string Estado { get; set; } = null!;

        [Column("fecha_estado", TypeName = "timestamp(0) without time zone")]
        [Required(ErrorMessage = "La fecha de estado no puede ser nula.")]
        public DateTime FechaEstado { get; set; }

        [Column("observacion_estado")]
        [StringLength(2000,ErrorMessage = "La observación debe contener máximo 2000 caracteres.")]
        public string? ObservacionEstado { get; set; }

        [Column("usuario_ingreso")]
        [Required(ErrorMessage = "El campo usuario ingreso no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El campo usuario ingreso debe ser un número entero mayor a cero.")]
        public long UsuarioIngreso { get; set; }

        [Column("fecha_ingreso", TypeName = "timestamp(0) without time zone")]
        [Required(ErrorMessage = "La fecha de ingreso no puede ser nula.")]
        public DateTime FechaIngreso { get; set; }

        [Column("ubicacion_ingreso")]
        [Required(ErrorMessage = "La ubicación de ingreso no puede ser nula.")]
        [RegularExpression("^[0-9.]+$", ErrorMessage = "La ubicación de ingreso debe contener máximo 200 caracteres.")]
        public string UbicacionIngreso { get; set; } = null!;

        [Column("usuario_modificacion")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El usuario modificación debe ser un número entero mayor a cero.")]
        public long? UsuarioModificacion { get; set; }

        [Column("fecha_modificacion", TypeName = "timestamp(0) without time zone")]
        public DateTime? FechaModificacion { get; set; }

        [Column("ubicacion_modificacion")]
        [RegularExpression("^[0-9.]+$", ErrorMessage = "La ubicación de modificación debe contener máximo 200 caracteres.")]
        public string? UbicacionModificacion { get; set; }

        [ForeignKey("AgeTransaCodigo,AgeTransaAgeAplicaCodigo")]
        [InverseProperty("AgeFavoritos")]
        public virtual AgeTransacciones AgeTransa { get; set; } = null!;
        [ForeignKey("AgeUsuariCodigo,AgeUsuariAgeLicencCodigo")]
        [InverseProperty("AgeFavoritos")]
        public virtual AgeUsuarios AgeUsuari { get; set; } = null!;
    }
}
