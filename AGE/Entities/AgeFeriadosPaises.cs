﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AGE.Utils;
using Microsoft.EntityFrameworkCore;

namespace AGE.Entities
{
    [Table("age_feriados_paises", Schema = "sasf_user_desa")]
    public partial class AgeFeriadosPaises
    {
        [Key]
        [Column("age_dia_fe_codigo")]
        [Required(ErrorMessage = "El código del día feriado no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código del día feriado debe ser un número entero mayor a cero.")]
        public int AgeDiaFeCodigo { get; set; }


        [Key]
        [Column("age_pais_codigo")]
        [Required(ErrorMessage = "El código del país no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código del país debe ser un número entero mayor a cero.")]
        public int AgePaisCodigo { get; set; }


        [Column("fecha_ejecucion_desde", TypeName = "timestamp(0) without time zone")]
        [Required(ErrorMessage = "La fecha de ejecución desde no puede ser nula.")]
        public DateTime FechaEjecucionDesde { get; set; }


        [Column("fecha_ejecucion_hasta", TypeName = "timestamp(0) without time zone")]
        [Required(ErrorMessage = "La fecha de ejecución hasta no puede ser nula.")]
        public DateTime FechaEjecucionHasta { get; set; }


        [Column("hora_ejecucion_desde", TypeName = "timestamp(0) without time zone")]
        public DateTime? HoraEjecucionDesde { get; set; }


        [Column("hora_ejecucion_hasta", TypeName = "timestamp(0) without time zone")]
        public DateTime? HoraEjecucionHasta { get; set; }


        [Required(ErrorMessage = "El estado no puede ser nulo.")]
        [Column("estado")]
        [RegularExpression("^[AI]$", ErrorMessage = $"El campo estado debe ser {Globales.ESTADO_ACTIVO} o {Globales.ESTADO_INACTIVO}.")]
        public string Estado { get; set; } = null!;


        [Required(ErrorMessage = "La fecha de estado no puede ser nula.")]
        [Column("fecha_estado", TypeName = "timestamp(0) without time zone")]
        public DateTime FechaEstado { get; set; }


        [Column("observacion_estado")]
        [StringLength(2000,ErrorMessage = "La observación debe contener máximo 2000 caracteres.")]
        public string? ObservacionEstado { get; set; }


        [Required(ErrorMessage = "El campo usuario ingreso no puede ser nulo.")]
        [Column("usuario_ingreso")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El campo usuario ingreso debe ser un número entero mayor a cero.")]
        public long UsuarioIngreso { get; set; }


        [Required(ErrorMessage = "La fecha de ingreso no puede ser nula.")]
        [Column("fecha_ingreso", TypeName = "timestamp(0) without time zone")]
        public DateTime FechaIngreso { get; set; }


        [Required(ErrorMessage = "La ubicación de ingreso no puede ser nula.")]
        [Column("ubicacion_ingreso")]
        [RegularExpression("^[0-9.]+$", ErrorMessage = "La ubicación de ingreso debe contener máximo 200 caracteres.")]
        public string? UbicacionIngreso { get; set; }


        [Column("usuario_modificacion")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El usuario modificación debe ser un número entero mayor a cero.")]
        public long? UsuarioModificacion { get; set; }


        [Column("fecha_modificacion", TypeName = "timestamp(0) without time zone")]
        public DateTime? FechaModificacion { get; set; }


        [RegularExpression("^[0-9.]+$", ErrorMessage = "La ubicación de modificación debe contener máximo 200 caracteres.")]
        [Column("ubicacion_modificacion")]
        public string? UbicacionModificacion { get; set; }



        [ForeignKey("AgeDiaFeCodigo")]
        [InverseProperty("AgeFeriadosPaises")]
        public virtual AgeDiasFeriados AgeDiaFeCodigoNavigation { get; set; } = null!;
        [ForeignKey("AgePaisCodigo")]
        [InverseProperty("AgeFeriadosPaises")]
        public virtual AgePaises AgePaisCodigoNavigation { get; set; } = null!;
    }
}
