﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace AGE.Entities
{
    [Keyless]
    [Table("UM_USER", Schema = "sasf_user_desa")]
    public partial class UmUser
    {
        [Column("UM_ID")]
        [Precision(5, 0)]
        public decimal UmId { get; set; }
        [Column(" UM_USER_NAME")]
        public string UmUserName { get; set; } = null!;
    }
}
