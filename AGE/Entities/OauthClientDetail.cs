﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace AGE.Entities
{
    [Table("oauth_client_details", Schema = "sasf_user_desa")]
    public partial class OauthClientDetail
    {
        [Key]
        [Column("client_id")]
        [StringLength(255)]
        public string ClientId { get; set; } = null!;
        [Column("access_token_validity")]
        public int AccessTokenValidity { get; set; }
        [Column("additional_information")]
        [StringLength(4096)]
        public string? AdditionalInformation { get; set; }
        [Column("authorities")]
        [StringLength(255)]
        public string Authorities { get; set; } = null!;
        [Column("autoapprove")]
        [StringLength(255)]
        public string? Autoapprove { get; set; }
        [Column("client_secret")]
        [StringLength(255)]
        public string ClientSecret { get; set; } = null!;
        [Column("authorized_grant_types")]
        [StringLength(255)]
        public string AuthorizedGrantTypes { get; set; } = null!;
        [Column("refresh_token_validity")]
        public long? RefreshTokenValidity { get; set; }
        [Column("resource_ids")]
        [StringLength(255)]
        public string? ResourceIds { get; set; }
        [Column("scope")]
        [StringLength(255)]
        public string Scope { get; set; } = null!;
        [Column("web_server_redirect_uri")]
        [StringLength(255)]
        public string? WebServerRedirectUri { get; set; }
    }
}
