﻿
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AGE.Utils;

namespace AGE.Entities
{
    [Table("age_usuarios_punto_emision", Schema = "sasf_user_desa")]
    public partial class AgeUsuariosPuntoEmision
    {
        [Key]
        [Required(ErrorMessage = "El código de licenciatario no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de licenciatario debe ser un número entero mayor a cero.")]
        [Column("agage_sucurs_age_licenc_codigo")]
        public int AgageSucursAgeLicencCodigo { get; set; }

        [Key]
        [Required(ErrorMessage = "El código de la sucursal no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de la sucursal debe ser un número entero mayor a cero.")]
        [Column("age_pun_em_age_sucurs_codigo")]
        public int AgePunEmAgeSucursCodigo { get; set; }

        [Key]
        [Column("age_pun_em_codigo")]
        [Required(ErrorMessage = "El código del punto de emisión no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código del punto de emisión debe ser un número entero mayor a cero.")]
        public int AgePunEmCodigo { get; set; }

        [Key]
        [Column("age_usuari_age_licenc_codigo")]
        [Required(ErrorMessage = "El código de licenciatario no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de licenciatario debe ser un número entero mayor a cero.")]
        public int AgeUsuariAgeLicencCodigo { get; set; }

        [Key]
        [Required(ErrorMessage = "El código de licenciatario no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de licenciatario debe ser un número entero mayor a cero.")]
        [Column("age_usuari_codigo")]
        public long AgeUsuariCodigo { get; set; }

        [Required(ErrorMessage = "La fecha desde no puede ser nula.")]
        [Column("fecha_desde", TypeName = "timestamp(0) without time zone")]
        public DateTime FechaDesde { get; set; }

        [Column("fecha_hasta", TypeName = "timestamp(0) without time zone")]
        public DateTime? FechaHasta { get; set; }

        [Column("estado")]
        [Required(ErrorMessage = "El estado no puede ser nulo.")]
        [RegularExpression("^[AI]$", ErrorMessage = $"El campo estado debe ser {Globales.ESTADO_ACTIVO} o {Globales.ESTADO_INACTIVO}.")]
        public string? Estado { get; set; }

        [Required(ErrorMessage = "Fecha estado no puede ser nulo.")]
        [Column("fecha_estado", TypeName = "timestamp without time zone")]
        public DateTime FechaEstado { get; set; }

        [Column("observacion_estado")]
        [StringLength(2000,ErrorMessage = "La observación debe contener máximo 2000 caracteres.")]
        public string? ObservacionEstado { get; set; }

        [Column("usuario_ingreso")]
        [Required(ErrorMessage = "El campo usuario ingreso no puede ser nulo.")]
        [Range(1, long.MaxValue, ErrorMessage = "El código del usuario de ingreso debe ser mayor a 0.")]
        public long? UsuarioIngreso { get; set; }

        [Required(ErrorMessage = "La fecha de ingreso no puede ser nula.")]
        [Column("fecha_ingreso", TypeName = "timestamp without time zone")]
        public DateTime? FechaIngreso { get; set; }

        [Column("ubicacion_ingreso")]
        [Required(ErrorMessage = "La ubicación de ingreso no puede ser nula.")]
        [RegularExpression("^[0-9.]+$", ErrorMessage = "La ubicación de ingreso debe contener máximo 200 caracteres.")]
        public string? UbicacionIngreso { get; set; }

        [Column("usuario_modificacion")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El usuario modificación debe ser un número entero mayor a cero.")]
        public long? UsuarioModificacion { get; set; }

        [Column("fecha_modificacion", TypeName = "timestamp without time zone")]
        public DateTime? FechaModificacion { get; set; }

        [Column("ubicacion_modificacion")]
        [RegularExpression("^[0-9.]+$", ErrorMessage = "La ubicación de modificación debe contener máximo 200 caracteres.")]
        public string? UbicacionModificacion { get; set; }

        [ForeignKey("AgePunEmCodigo,AgePunEmAgeSucursCodigo,AgageSucursAgeLicencCodigo")]
        [InverseProperty("AgeUsuariosPuntoEmisions")]
        public virtual AgePuntosEmisiones Ag { get; set; } = null!;
        [ForeignKey("AgeUsuariCodigo,AgeUsuariAgeLicencCodigo")]
        [InverseProperty("AgeUsuariosPuntoEmisions")]
        public virtual AgeUsuarios AgeUsuari { get; set; } = null!;
    }
}
