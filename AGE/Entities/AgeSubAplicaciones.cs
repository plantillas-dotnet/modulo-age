﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AGE.Utils;
using Microsoft.EntityFrameworkCore;

namespace AGE.Entities
{
    [Table("age_sub_aplicaciones", Schema = "sasf_user_desa")]
    public partial class AgeSubAplicaciones
    {
        public AgeSubAplicaciones()
        {
            InverseAg = new HashSet<AgeSubAplicaciones>();
        }

        [Key]
        [Required(ErrorMessage = "El código de la aplicación no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de la aplicación debe ser un número entero mayor a cero.")]
        [Column("age_lic_ap_age_aplica_codigo")]
        public int AgeLicApAgeAplicaCodigo { get; set; }

        [Key]
        [Column("age_lic_ap_age_licenc_codigo")]
        [Required(ErrorMessage = "El código de licenciatario no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de licenciatario debe ser un número entero mayor a cero.")]
        public int AgeLicApAgeLicencCodigo { get; set; }

        [Key]
        [Column("codigo")]
        [Required(ErrorMessage = "El código de la subaplicación no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de la subaplicación debe ser un número entero mayor a cero.")]
        public int Codigo { get; set; }

        [Column("descripcion")]
        [StringLength(200, ErrorMessage = "La descripción debe contener máximo 200 caracteres.")]
        [Required(ErrorMessage = "La descripción no puede ser nula.")]
        public string Descripcion { get; set; } = null!;

        [Column("contabiliza")]
        [Required(ErrorMessage = "El campo contabiliza no puede ser nula.")]
        [RegularExpression("^[SN]$", ErrorMessage = $"El campo contabiliza debe ser {Globales.SI} o {Globales.NO}.")]
        public string Contabiliza { get; set; } = null!;

        [Column("agage_lic_ap_age_aplica_codigo")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de la aplicación debe ser un número entero mayor a cero.")]
        public int? AgageLicApAgeAplicaCodigo { get; set; }

        [Column("agage_lic_ap_age_licenc_codigo")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de licenciatario debe ser un número entero mayor a cero.")]
        public int? AgageLicApAgeLicencCodigo { get; set; }

        [Column("age_sub_ap_codigo")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de la subaplicación debe ser un número entero mayor a cero.")]
        public int? AgeSubApCodigo { get; set; }

        [Column("estado")]
        [Required(ErrorMessage = "El estado no puede ser nulo.")]
        [RegularExpression("^[AI]$", ErrorMessage = $"El campo estado debe ser {Globales.ESTADO_ACTIVO} o {Globales.ESTADO_INACTIVO}.")]
        public string? Estado { get; set; }

        [Required(ErrorMessage = "Fecha estado no puede ser nulo.")]
        [Column("fecha_estado", TypeName = "timestamp without time zone")]
        public DateTime FechaEstado { get; set; }

        [Column("observacion_estado")]
        [StringLength(2000,ErrorMessage = "La observación debe contener máximo 2000 caracteres.")]
        public string? ObservacionEstado { get; set; }

        [Column("usuario_ingreso")]
        [Required(ErrorMessage = "El campo usuario ingreso no puede ser nulo.")]
        [Range(1, long.MaxValue, ErrorMessage = "El código del usuario de ingreso debe ser mayor a 0.")]
        public long? UsuarioIngreso { get; set; }

        [Required(ErrorMessage = "La fecha de ingreso no puede ser nula.")]
        [Column("fecha_ingreso", TypeName = "timestamp without time zone")]
        public DateTime? FechaIngreso { get; set; }

        [Column("ubicacion_ingreso")]
        [Required(ErrorMessage = "La ubicación de ingreso no puede ser nula.")]
        [RegularExpression("^[0-9.]+$", ErrorMessage = "La ubicación de ingreso debe contener máximo 200 caracteres.")]
        public string? UbicacionIngreso { get; set; }

        [Column("usuario_modificacion")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El usuario modificación debe ser un número entero mayor a cero.")]
        public long? UsuarioModificacion { get; set; }

        [Column("fecha_modificacion", TypeName = "timestamp without time zone")]
        public DateTime? FechaModificacion { get; set; }

        [Column("ubicacion_modificacion")]
        [RegularExpression("^[0-9.]+$", ErrorMessage = "La ubicación de modificación debe contener máximo 200 caracteres.")]
        public string? UbicacionModificacion { get; set; }

        [ForeignKey("AgeSubApCodigo,AgageLicApAgeLicencCodigo,AgageLicApAgeAplicaCodigo")]
        [InverseProperty("InverseAg")]
        public virtual AgeSubAplicaciones? Ag { get; set; }
        [ForeignKey("AgeLicApAgeLicencCodigo,AgeLicApAgeAplicaCodigo")]
        [InverseProperty("AgeSubAplicaciones")]
        public virtual AgeLicenciatariosAplicaciones AgeLicApAge { get; set; } = null!;
        [InverseProperty("Ag")]
        public virtual ICollection<AgeSubAplicaciones> InverseAg { get; set; }
    }
}
