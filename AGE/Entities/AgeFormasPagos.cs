﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AGE.Utils;
using Microsoft.EntityFrameworkCore;

namespace AGE.Entities
{
    [Table("age_formas_pagos", Schema = "sasf_user_desa")]
    public partial class AgeFormasPagos
    {
        public AgeFormasPagos()
        {
            AgeFormasPagosInstFinas = new HashSet<AgeFormasPagosInstFina>();
        }

        [Key]
        [Column("age_licenc_codigo")]
        [Required(ErrorMessage = "El código de licenciatario no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de licenciatario debe ser un número entero mayor a cero.")]
        public int AgeLicencCodigo { get; set; }


        [Key]
        [Column("codigo")]
        [Required(ErrorMessage = "El código de la forma de pago no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de la forma de pago debe ser un número entero mayor a cero.")]
        public int Codigo { get; set; }


        [Column("codigo_institucion_control")]
        [Required(ErrorMessage = "El código de institución de control no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de institución de control debe ser un número entero mayor a cero.")]
        public int CodigoInstitucionControl { get; set; }


        [Column("descripcion")]
        [Required(ErrorMessage = "La descripción no puede ser nula.")]
        [StringLength(200, MinimumLength = 1, ErrorMessage = "La descripción debe contener máximo 200 caracteres.")]
        public string Descripcion { get; set; } = null!;


        [Column("retiene")]
        [RegularExpression("^[SN]$", ErrorMessage = $"El campo retiene debe ser {Globales.SI} o {Globales.NO}.")]
        public string Retiene { get; set; } = null!;


        [Column("presenta_caja_bancos")]
        [RegularExpression("^[SN]$", ErrorMessage = $"El campo presenta banco debe ser {Globales.SI} o {Globales.NO}.")]
        public string? PresentaCajaBancos { get; set; }


        [Required(ErrorMessage = "El estado no puede ser nulo.")]
        [Column("estado")]
        [RegularExpression("^[AI]$", ErrorMessage = $"El campo estado debe ser {Globales.ESTADO_ACTIVO} o {Globales.ESTADO_INACTIVO}.")]
        public string Estado { get; set; } = null!;


        [Required(ErrorMessage = "La fecha de estado no puede ser nula.")]
        [Column("fecha_estado", TypeName = "timestamp(0) without time zone")]
        public DateTime FechaEstado { get; set; }


        [Column("observacion_estado")]
        [StringLength(2000, ErrorMessage = "La observación del estado debe contener máximo 2000 caracteres.")]
        public string? ObservacionEstado { get; set; }


        [Required(ErrorMessage = "El campo usuario ingreso no puede ser nulo.")]
        [Column("usuario_ingreso")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El campo usuario ingreso debe ser un número entero mayor a cero.")]
        public long UsuarioIngreso { get; set; }


        [Required(ErrorMessage = "La fecha de ingreso no puede ser nula.")]
        [Column("fecha_ingreso", TypeName = "timestamp(0) without time zone")]
        public DateTime FechaIngreso { get; set; }


        [Required(ErrorMessage = "La ubicación de ingreso no puede ser nula.")]
        [Column("ubicacion_ingreso")]
        [RegularExpression("^[0-9.]+$", ErrorMessage = "La ubicación de ingreso debe contener máximo 200 caracteres.")]
        public string? UbicacionIngreso { get; set; }


        [Column("usuario_modificacion")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El usuario modificación debe ser un número entero mayor a cero.")]
        public long? UsuarioModificacion { get; set; }


        [Column("fecha_modificacion", TypeName = "timestamp(0) without time zone")]
        public DateTime? FechaModificacion { get; set; }


        [RegularExpression("^[0-9.]+$", ErrorMessage = "La ubicación de modificación debe contener máximo 200 caracteres.")]
        [Column("ubicacion_modificacion")]
        public string? UbicacionModificacion { get; set; }



        [ForeignKey("AgeLicencCodigo")]
        [InverseProperty("AgeFormasPagos")]
        public virtual AgeLicenciatario AgeLicencCodigoNavigation { get; set; } = null!;
        [InverseProperty("AgeForPa")]
        public virtual ICollection<AgeFormasPagosInstFina> AgeFormasPagosInstFinas { get; set; }
    }
}
