﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AGE.Utils;
using Microsoft.EntityFrameworkCore;

namespace AGE.Entities
{
    [Table("age_licenciatarios_paises", Schema = "sasf_user_desa")]
    public partial class AgeLicenciatariosPaise
    {
        public AgeLicenciatariosPaise()
        {
            InverseAgeLicPaAge = new HashSet<AgeLicenciatariosPaise>();
        }

        [Key]
        [Column("age_licenc_codigo")]
        [Required(ErrorMessage = "El código Licenciatario no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código Licenciatario debe ser un número entero mayor a cero.")]
        public int AgeLicencCodigo { get; set; }

        [Key]
        [Column("age_pais_codigo")]
        [Required(ErrorMessage = "El código del país no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código del país debe ser un número entero mayor a cero.")]
        public int AgePaisCodigo { get; set; }

        [Column("principal")]
        [Required(ErrorMessage = "Principal no puede ser nulo.")]
        [RegularExpression("^[SN]$", ErrorMessage = $"El campo Principal debe ser {Globales.SI} o {Globales.NO}.")]
        public string Principal { get; set; } = null!;

        [Column("estado")]
        [Required(ErrorMessage = "El estado no puede ser nulo.")]
        [RegularExpression("^[AI]$", ErrorMessage = $"El campo estado debe ser {Globales.ESTADO_ACTIVO} o {Globales.ESTADO_INACTIVO}.")]
        public string Estado { get; set; } = null!;

        [Required(ErrorMessage = "La fecha de estado no puede ser nula.")]
        [Column("fecha_estado", TypeName = "timestamp(0) without time zone")]
        public DateTime FechaEstado { get; set; }

        [Column("observacion_estado")]
        [StringLength(2000, ErrorMessage = "La observación debe contener máximo 2000 caracteres.")]
        public string ObservacionEstado { get; set; } = null!;

        [Required(ErrorMessage = "El campo usuario ingreso no puede ser nulo.")]
        [Column("usuario_ingreso")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El campo usuario ingreso debe ser un número entero mayor a cero.")]
        public long UsuarioIngreso { get; set; }

        [Column("fecha_ingreso", TypeName = "timestamp(0) without time zone")]
        [Required(ErrorMessage = "La fecha de ingreso no puede ser nula.")]
        public DateTime FechaIngreso { get; set; }

        [Column("ubicacion_ingreso")]
        [StringLength(200, ErrorMessage = "La ubicación de modificación debe contener máximo 200 caracteres.")]
        [RegularExpression("^[0-9.]+$", ErrorMessage = "La ubicación de modificación solo debe contener números.")]
        public string UbicacionIngreso { get; set; } = null!;

        [Column("age_lic_pa_age_licenc_codigo")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código Licenciatario Pais debe ser un número entero mayor a cero.")]
        public int? AgeLicPaAgeLicencCodigo { get; set; }

        [Column("age_lic_pa_age_pais_codigo")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código Licenciatario Pais debe ser un número entero mayor a cero.")]
        public int? AgeLicPaAgePaisCodigo { get; set; }

        [Column("usuario_modificacion")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El usuario modificación debe ser un número entero mayor a cero.")]
        public long? UsuarioModificacion { get; set; }

        [Column("fecha_modificacion", TypeName = "timestamp(0) without time zone")]
        public DateTime? FechaModificacion { get; set; }

        [Column("ubicacion_modificacion")]
        [StringLength(200, ErrorMessage = "La ubicación de modificación debe contener máximo 200 caracteres.")]
        [RegularExpression("^[0-9.]+$", ErrorMessage = "La ubicación de modificación solo debe contener números.")]
        public string? UbicacionModificacion { get; set; }

        [ForeignKey("AgeLicPaAgePaisCodigo,AgeLicPaAgeLicencCodigo")]
        [InverseProperty("InverseAgeLicPaAge")]
        public virtual AgeLicenciatariosPaise? AgeLicPaAge { get; set; }
        [ForeignKey("AgeLicencCodigo")]
        [InverseProperty("AgeLicenciatariosPaises")]
        public virtual AgeLicenciatario AgeLicencCodigoNavigation { get; set; } = null!;
        [ForeignKey("AgePaisCodigo")]
        [InverseProperty("AgeLicenciatariosPaises")]
        public virtual AgePaises AgePaisCodigoNavigation { get; set; } = null!;
        [InverseProperty("AgeLicPaAge")]
        public virtual ICollection<AgeLicenciatariosPaise> InverseAgeLicPaAge { get; set; }
    }
}
