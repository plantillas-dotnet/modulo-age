﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AGE.Utils;
using Microsoft.EntityFrameworkCore;

namespace AGE.Entities
{
    [Table("age_licenciat_param_vigencias", Schema = "sasf_user_desa")]
    public partial class AgeLicenciatParamVigencias
    {
        [Key]
        [Column("age_licenc_codigo")]
        [Required(ErrorMessage = "El código de licenciatario no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de licenciatario debe ser un número entero mayor a cero.")]
        public int AgeLicencCodigo { get; set; }

        [Key]
        [Column("age_par_ge_codigo")]
        [Required(ErrorMessage = "El código de parámetro general no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de parámetro general debe ser un número entero mayor a cero.")]
        public int AgeParGeCodigo { get; set; }

        [Key]
        [Column("codigo")]
        [Required(ErrorMessage = "El código de secuencia no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de secuencia debe ser un número entero mayor a cero.")]
        public long Codigo { get; set; }

        [Column("observacion")]
        [StringLength(2000,ErrorMessage = "La observación debe contener máximo 2000 caracteres.")]
        public string? Observacion { get; set; }

        [Column("fecha_desde", TypeName = "timestamp(0) without time zone")]
        [Required(ErrorMessage = "La fecha desde no puede ser nula.")]
        public DateTime FechaDesde { get; set; }

        [Column("fecha_hasta", TypeName = "timestamp(0) without time zone")]
        public DateTime? FechaHasta { get; set; }

        [Column("valor_parametro")]
        [Required(ErrorMessage = "El valor del parámetro no puede ser nulo.")]
        [StringLength(200, ErrorMessage = "El valor del parámetro debe contener máximo 200 caracteres.")]
        public string ValorParametro { get; set; } = null!;

        [Column("estado")]
        [Required(ErrorMessage = "El estado no puede ser nulo.")]
        [RegularExpression("^[AI]$", ErrorMessage = $"El campo estado debe ser {Globales.ESTADO_ACTIVO} o {Globales.ESTADO_INACTIVO}.")]
        public string Estado { get; set; } = null!;

        [Required(ErrorMessage = "La fecha de estado no puede ser nula.")]
        [Column("fecha_estado", TypeName = "timestamp(0) without time zone")]
        public DateTime FechaEstado { get; set; }

        [Column("observacion_estado")]
        [StringLength(2000, ErrorMessage = "La observación debe contener máximo 2000 caracteres.")]
        public string? ObservacionEstado { get; set; }

        [Required(ErrorMessage = "El campo usuario ingreso no puede ser nulo.")]
        [Column("usuario_ingreso")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El campo usuario ingreso debe ser un número entero mayor a cero.")]
        public long UsuarioIngreso { get; set; }

        [Required(ErrorMessage = "La fecha de ingreso no puede ser nula.")]
        [Column("fecha_ingreso", TypeName = "timestamp(0) without time zone")]
        public DateTime FechaIngreso { get; set; }

        [Required(ErrorMessage = "La ubicación de ingreso no puede ser nula.")]
        [Column("ubicacion_ingreso")]
        [RegularExpression("^[0-9.]+$", ErrorMessage = "La ubicación de ingreso debe contener máximo 200 caracteres.")]
        public string? UbicacionIngreso { get; set; }

        [Column("usuario_modificacion")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El usuario modificación debe ser un número entero mayor a cero.")]
        public long? UsuarioModificacion { get; set; }

        [Column("fecha_modificacion", TypeName = "timestamp(0) without time zone")]
        public DateTime? FechaModificacion { get; set; }

        [RegularExpression("^[0-9.]+$", ErrorMessage = "La ubicación de modificación debe contener máximo 200 caracteres. SSS")]
        [Column("ubicacion_modificacion")]
        public string? UbicacionModificacion { get; set; }

        [ForeignKey("AgeLicencCodigo")]
        [InverseProperty("AgeLicenciatParamVigencia")]
        public virtual AgeLicenciatario AgeLicencCodigoNavigation { get; set; } = null!;
        [ForeignKey("AgeParGeCodigo")]
        [InverseProperty("AgeLicenciatParamVigencia")]
        public virtual AgeParametrosGenerales AgeParGeCodigoNavigation { get; set; } = null!;
    }
}
