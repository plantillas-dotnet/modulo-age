﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AGE.Utils;

namespace AGE.Entities
{
    [Table("age_transacciones_idiomas", Schema = "sasf_user_desa")]
    public partial class AgeTransaccionesIdiomas
    {
        [Key]
        [Column("age_idioma_codigo")]
        [Required(ErrorMessage = "El código de idioma no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de idioma debe ser un número entero mayor a cero.")]
        public int AgeIdiomaCodigo { get; set; }

        [Key]
        [Column("age_transa_age_aplica_codigo")]
        [Required(ErrorMessage = "El código de aplicación no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de aplicación debe ser un número entero mayor a cero.")]
        public int AgeTransaAgeAplicaCodigo { get; set; }

        [Key]
        [Column("age_transa_codigo")]
        [Required(ErrorMessage = "El código de transacción no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código debe ser un número entero mayor a cero.")]
        public int AgeTransaCodigo { get; set; }

        [Column("descripcion")]
        [Required(ErrorMessage = "La descripción no puede ser nula.")]
        [StringLength(200, ErrorMessage = "La descripción debe contener máximo 200 caracteres.")]
        public string Descripcion { get; set; } = null!;

        [Required(ErrorMessage = "El estado no puede ser nulo.")]
        [Column("estado")]
        [RegularExpression("^[AI]$", ErrorMessage = $"El campo estado debe ser {Globales.ESTADO_ACTIVO} o {Globales.ESTADO_INACTIVO}.")]
        public string Estado { get; set; } = null!;

        [Column("fecha_estado", TypeName = "timestamp(0) without time zone")]
        [Required(ErrorMessage = "La fecha de estado no puede ser nula.")]
        public DateTime FechaEstado { get; set; }

        [Column("observacion_estado")]
        [StringLength(2000,ErrorMessage = "La observación debe contener máximo 2000 caracteres.")]
        public string? ObservacionEstado { get; set; }

        [Column("usuario_ingreso")]
        [Required(ErrorMessage = "El campo usuario ingreso no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El campo usuario ingreso debe ser un número entero mayor a cero.")]
        public long UsuarioIngreso { get; set; }

        [Column("fecha_ingreso", TypeName = "timestamp(0) without time zone")]
        [Required(ErrorMessage = "La fecha de ingreso no puede ser nula.")]
        public DateTime FechaIngreso { get; set; }

        [Column("ubicacion_ingreso")]
        [Required(ErrorMessage = "La ubicación de ingreso no puede ser nula.")]
        [RegularExpression("^[0-9.]+$", ErrorMessage = "La ubicación de ingreso debe contener máximo 200 caracteres.")]
        public string UbicacionIngreso { get; set; } = null!;

        [Column("usuario_modificacion")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El usuario modificación debe ser un número entero mayor a cero.")]
        public long? UsuarioModificacion { get; set; }

        [Column("fecha_modificacion", TypeName = "timestamp(0) without time zone")]
        public DateTime? FechaModificacion { get; set; }

        [Column("ubicacion_modificacion")]
        [RegularExpression("^[0-9.]+$", ErrorMessage = "La ubicación de modificación debe contener máximo 200 caracteres.")]
        public string? UbicacionModificacion { get; set; }

        [ForeignKey("AgeIdiomaCodigo")]
        [InverseProperty("AgeTransaccionesIdiomas")]
        public virtual AgeIdiomas AgeIdiomaCodigoNavigation { get; set; } = null!;
        [ForeignKey("AgeTransaCodigo,AgeTransaAgeAplicaCodigo")]
        [InverseProperty("AgeTransaccionesIdiomas")]
        public virtual AgeTransacciones AgeTransa { get; set; } = null!;
    }
}
