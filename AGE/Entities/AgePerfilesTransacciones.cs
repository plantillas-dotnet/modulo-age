﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AGE.Utils;
using Microsoft.EntityFrameworkCore;

namespace AGE.Entities
{
    [Table("age_perfiles_transacciones", Schema = "sasf_user_desa")]
    public partial class AgePerfilesTransacciones
    {
        [Key]
        [Column("age_perfil_age_licenc_codigo")]
        [Required(ErrorMessage = "El código del licenciatario no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código del licenciatario debe ser un número entero mayor a cero.")]
        public int AgePerfilAgeLicencCodigo { get; set; }

        [Key]
        [Column("age_perfil_codigo")]
        [Required(ErrorMessage = "El código de perfil no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de perfil debe ser un número entero mayor a cero.")]
        public int AgePerfilCodigo { get; set; }

        [Key]
        [Column("codigo")]
        [Required(ErrorMessage = "El código no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código debe ser un número entero mayor a cero.")]
        public long Codigo { get; set; }

        [Column("age_transa_age_aplica_codigo")]
        [Required(ErrorMessage = "La aplicación no puede ser nula.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "La aplicación debe ser mayor a 0.")]
        public int AgeTransaAgeAplicaCodigo { get; set; }

        [Column("age_transa_codigo")]
        [Required(ErrorMessage = "La transaccion no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "La transaccion debe ser un número entero mayor a cero.")]
        public int AgeTransaCodigo { get; set; }

        [Column("estado")]
        [Required(ErrorMessage = "El estado no puede ser nulo.")]
        [RegularExpression("^[AI]$", ErrorMessage = $"El campo estado debe ser {Globales.ESTADO_ACTIVO} o {Globales.ESTADO_INACTIVO}.")]
        public string Estado { get; set; } = null!;

        [Column("fecha_estado", TypeName = "timestamp(0) without time zone")]
        [Required(ErrorMessage = "La fecha de estado no puede ser nula.")]
        public DateTime FechaEstado { get; set; }

        [Column("observacion_estado")]
        [StringLength(2000,ErrorMessage = "La observación debe contener máximo 2000 caracteres.")]
        public string? ObservacionEstado { get; set; }

        [Required(ErrorMessage = "El campo usuario ingreso no puede ser nulo.")]
        [Column("usuario_ingreso")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El campo usuario ingreso debe ser un número entero mayor a cero.")]
        public long UsuarioIngreso { get; set; }

        [Required(ErrorMessage = "La fecha de ingreso no puede ser nula.")]
        [Column("fecha_ingreso", TypeName = "timestamp(0) without time zone")]
        public DateTime FechaIngreso { get; set; }


        [Required(ErrorMessage = "La ubicación de ingreso no puede ser nula.")]
        [Column("ubicacion_ingreso")]
        [RegularExpression("^[0-9.]+$", ErrorMessage = "La ubicación de ingreso debe contener máximo 200 caracteres.")]
        public string? UbicacionIngreso { get; set; }


        [Column("usuario_modificacion")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El usuario modificación debe ser un número entero mayor a cero.")]
        public long? UsuarioModificacion { get; set; }


        [Column("fecha_modificacion", TypeName = "timestamp(0) without time zone")]
        public DateTime? FechaModificacion { get; set; }


        [RegularExpression("^[0-9.]+$", ErrorMessage = "La ubicación de modificación debe contener máximo 200 caracteres.")]
        [Column("ubicacion_modificacion")]
        public string? UbicacionModificacion { get; set; }

        [ForeignKey("AgePerfilCodigo,AgePerfilAgeLicencCodigo")]
        [InverseProperty("AgePerfilesTransacciones")]
        public virtual AgePerfiles AgePerfil { get; set; } = null!;
        [ForeignKey("AgeTransaCodigo,AgeTransaAgeAplicaCodigo")]
        [InverseProperty("AgePerfilesTransacciones")]
        public virtual AgeTransacciones AgeTransa { get; set; } = null!;

    }
}
