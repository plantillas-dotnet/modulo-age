﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AGE.Utils;

namespace AGE.Entities
{
    [Table("age_firmas_digitales", Schema = "sasf_user_desa")]
    public partial class AgeFirmasDigitales
    {
        [Key]
        [Column("age_sucurs_age_licenc_codigo")]
        [Required(ErrorMessage = "El código de licenciatario no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de licenciatario debe ser un número entero mayor a cero.")]
        public int AgeSucursAgeLicencCodigo { get; set; }

        [Key]
        [Column("age_sucurs_codigo")]
        [Required(ErrorMessage = "El código de la sucursal no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de la sucursal debe ser un número entero mayor a cero.")]
        public int AgeSucursCodigo { get; set; }

        [Key]
        [Column("codigo")]
        [Required(ErrorMessage = "El código de la firma digital no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de la firma digital debe ser un número entero mayor a cero.")]
        public int Codigo { get; set; }

        [Column("nombres")]
        [StringLength(200,ErrorMessage = "Los nombres deben contener máximo 200 caracteres.")]
        [Required(ErrorMessage = "Los nombres no pueden ser nulos.")]
        public string Nombres { get; set; } = null!;

        [Column("apellidos")]
        [StringLength(200,ErrorMessage = "Los apellidos deben contener máximo 200 caracteres.")]
        [Required(ErrorMessage = "Los apellidos no pueden ser nulos.")]
        public string Apellidos { get; set; } = null!;

        [Required(ErrorMessage = "La fecha desde no puede ser nula.")]
        [Column("fecha_desde", TypeName = "timestamp(0) without time zone")]
        public DateTime FechaDesde { get; set; }

        [Column("fecha_hasta", TypeName = "timestamp(0) without time zone")]
        public DateTime? FechaHasta { get; set; }

        [Column("tipo_firma_digital")]
        [Required(ErrorMessage = "El Tipo de firma digital no puede ser nulo.")]
        [RegularExpression("^[AT]$", ErrorMessage = $"El Tipo de firma digital debe ser {Globales.TIPO_FIRMA_A} o {Globales.TIPO_FIRMA_T}.")]
        public string TipoFirmaDigital { get; set; } = null!;

        [Column("clave")]
        [StringLength(20,ErrorMessage = "La clave debe contener máximo 20 caracteres.")]
        [Required(ErrorMessage = "La clave no puede ser nula.")]
        public string Clave { get; set; } = null!;

        [Column("nombre_archivo")]
        [StringLength(200,ErrorMessage = "El nombre del archivo debe contener máximo 200 caracteres.")]
        public string? NombreArchivo { get; set; }

        [Column("url")]
        [StringLength(2000,ErrorMessage = "La URL deben contener máximo 2000 caracteres.")]
        public string? Url { get; set; }

        [Column("fecha_ultima_notificacion", TypeName = "timestamp(0) without time zone")]
        public DateTime? FechaUltimaNotificacion { get; set; }

        [Column("estado")]
        [Required(ErrorMessage = "El estado no puede ser nulo.")]
        [RegularExpression("^[AI]$", ErrorMessage = $"El campo estado debe ser {Globales.ESTADO_ACTIVO} o {Globales.ESTADO_INACTIVO}.")]
        public string Estado { get; set; } = null!;

        [Column("fecha_estado", TypeName = "timestamp(0) without time zone")]
        [Required(ErrorMessage = "La fecha de estado no puede ser nula.")]
        public DateTime FechaEstado { get; set; }

        [Column("observacion_estado")]
        [StringLength(2000,ErrorMessage = "La observación debe contener máximo 2000 caracteres.")]
        public string? ObservacionEstado { get; set; }

        [Column("usuario_ingreso")]
        [Required(ErrorMessage = "El campo usuario ingreso no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El campo usuario ingreso debe ser un número entero mayor a cero.")]
        public long UsuarioIngreso { get; set; }

        [Column("fecha_ingreso", TypeName = "timestamp(0) without time zone")]
        [Required(ErrorMessage = "La fecha de ingreso no puede ser nula.")]
        public DateTime FechaIngreso { get; set; }

        [Column("ubicacion_ingreso")]
        [Required(ErrorMessage = "La ubicación de ingreso no puede ser nula.")]
        [RegularExpression("^[0-9.]+$", ErrorMessage = "La ubicación de ingreso debe contener máximo 200 caracteres.")]
        public string UbicacionIngreso { get; set; } = null!;

        [Column("usuario_modificacion")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El usuario modificación debe ser un número entero mayor a cero.")]
        public long? UsuarioModificacion { get; set; }

        [Column("fecha_modificacion", TypeName = "timestamp(0) without time zone")]
        public DateTime? FechaModificacion { get; set; }

        [Column("ubicacion_modificacion")]
        [RegularExpression("^[0-9.]+$", ErrorMessage = "La ubicación de modificación debe contener máximo 200 caracteres.")]
        public string? UbicacionModificacion { get; set; }

        [ForeignKey("AgeSucursCodigo,AgeSucursAgeLicencCodigo")]
        [InverseProperty("AgeFirmasDigitales")]
        public virtual AgeSucursales AgeSucurs { get; set; } = null!;
    }
}
