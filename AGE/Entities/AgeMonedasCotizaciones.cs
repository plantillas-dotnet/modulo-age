﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AGE.Utils;
using Microsoft.EntityFrameworkCore;

namespace AGE.Entities
{
    [Table("age_monedas_cotizaciones", Schema = "sasf_user_desa")]
    public partial class AgeMonedasCotizaciones
    {
        [Key]
        [Column("age_licenc_codigo")]
        [Required(ErrorMessage = "El código de licenciatario no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de licenciatario debe ser un número entero mayor a cero.")]
        public int AgeLicencCodigo { get; set; }

        [Key]
        [Column("codigo")]
        [Required(ErrorMessage = "El código de la persona no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de la persona debe ser un número entero mayor a cero.")]
        public long Codigo { get; set; }

        [Column("age_moneda_codigo")]
        [Required(ErrorMessage = "El código de la moneda no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de la moneda debe ser un número entero mayor a cero.")]
        public int AgeMonedaCodigo { get; set; }

        [Column("age_moneda_codigo_ser_a")]
        [Required(ErrorMessage = "El código de la moneda no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de la moneda debe ser un número entero mayor a cero.")]
        public int AgeMonedaCodigoSerA { get; set; }

        [Column("fecha_desde", TypeName = "timestamp(0) without time zone")]
        [Required(ErrorMessage = "La fecha desde no puede ser nula.")]
        public DateTime FechaDesde { get; set; }

        [Column("fecha_hasta", TypeName = "timestamp(0) without time zone")]
        [Required(ErrorMessage = "La fecha hasta no puede ser nula.")]
        public DateTime FechaHasta { get; set; }

        [Column("cotizacion_mercado")]
        [Required(ErrorMessage = "La cotización de mercado no puede ser nula.")]
        [Range(1, 999999999999999999.999999, ErrorMessage = "La cotización de mercado no es válida.")]
        public decimal CotizacionMercado { get; set; }

        [Column("cotizacion_compra")]
        [Required(ErrorMessage = "La cotización de compra no puede ser nula.")]
        [Range(1, 999999999999999999.999999, ErrorMessage = "La cotización de compra no es válida.")]
        public decimal CotizacionCompra { get; set; }

        [Column("cotizacion_venta")]
        [Required(ErrorMessage = "La cotización de venta no puede ser nula.")]
        [Range(1, 999999999999999999.999999, ErrorMessage = "La cotización de venta no es válida.")]
        public decimal CotizacionVenta { get; set; }

        [Column("estado")]
        [Required(ErrorMessage = "El estado no puede ser nulo.")]
        [RegularExpression("^[AI]$", ErrorMessage = $"El campo estado debe ser {Globales.ESTADO_ACTIVO} o {Globales.ESTADO_INACTIVO}.")]
        public string Estado { get; set; } = null!;

        [Column("fecha_estado", TypeName = "timestamp(0) without time zone")]
        [Required(ErrorMessage = "La fecha de estado no puede ser nula.")]
        public DateTime FechaEstado { get; set; }

        [Column("observacion_estado")]
        [StringLength(2000,ErrorMessage = "La observación debe contener máximo 2000 caracteres.")]
        public string? ObservacionEstado { get; set; }

        [Column("usuario_ingreso")]
        [Required(ErrorMessage = "El campo usuario ingreso no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El campo usuario ingreso debe ser un número entero mayor a cero.")]
        public long UsuarioIngreso { get; set; }

        [Column("fecha_ingreso", TypeName = "timestamp(0) without time zone")]
        [Required(ErrorMessage = "La fecha de ingreso no puede ser nula.")]
        public DateTime FechaIngreso { get; set; }

        [Column("ubicacion_ingreso")]
        [Required(ErrorMessage = "La ubicación de ingreso no puede ser nula.")]
        [RegularExpression("^[0-9.]+$", ErrorMessage = "La ubicación de ingreso debe contener máximo 200 caracteres.")]
        public string UbicacionIngreso { get; set; } = null!;

        [Column("usuario_modificacion")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El usuario modificación debe ser un número entero mayor a cero.")]
        public long? UsuarioModificacion { get; set; }

        [Column("fecha_modificacion", TypeName = "timestamp(0) without time zone")]
        public DateTime? FechaModificacion { get; set; }

        [Column("ubicacion_modificacion")]
        [RegularExpression("^[0-9.]+$", ErrorMessage = "La ubicación de modificación debe contener máximo 200 caracteres.")]
        public string? UbicacionModificacion { get; set; }

        [ForeignKey("AgeLicencCodigo")]
        [InverseProperty("AgeMonedasCotizaciones")]
        public virtual AgeLicenciatario AgeLicencCodigoNavigation { get; set; } = null!;
        [ForeignKey("AgeMonedaCodigo")]
        [InverseProperty("AgeMonedasCotizacioneAgeMonedaCodigoNavigations")]
        public virtual AgeMonedas AgeMonedaCodigoNavigation { get; set; } = null!;
        [ForeignKey("AgeMonedaCodigoSerA")]
        [InverseProperty("AgeMonedasCotizacioneAgeMonedaCodigoSerANavigations")]
        public virtual AgeMonedas AgeMonedaCodigoSerANavigation { get; set; } = null!;
    }
}
