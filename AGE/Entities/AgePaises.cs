﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AGE.Utils;
using Microsoft.EntityFrameworkCore;

namespace AGE.Entities
{
    [Table("age_paises", Schema = "sasf_user_desa")]
    public partial class AgePaises
    {
        public AgePaises()
        {
            AgeFeriadosPaises = new HashSet<AgeFeriadosPaises>();
            AgeLicenciatariosPaises = new HashSet<AgeLicenciatariosPaise>();
            AgePaisesIdiomas = new HashSet<AgePaisesIdiomas>();
            AgePaisesMoneda = new HashSet<AgePaisesMonedas>();
            AgeTiposLocalidades = new HashSet<AgeTiposLocalidades>();
        }

        [Key]
        [Required(ErrorMessage = "El código del país no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código del país debe ser un número entero mayor a cero.")]
        [Column("codigo")]
        public int Codigo { get; set; }

        [Column("descripcion")]
        [Required(ErrorMessage = "La descripción del país no puede ser nula.")]
        [StringLength(200, ErrorMessage = "La descripción del país debe contener máximo 200 caracteres.")]
        public string Descripcion { get; set; } = null!;

        [Required(ErrorMessage = "El estado no puede ser nulo.")]
        [Column("estado")]
        [RegularExpression("^[AI]$", ErrorMessage = $"El campo estado debe ser {Globales.ESTADO_ACTIVO} o {Globales.ESTADO_INACTIVO}.")]
        public string Estado { get; set; } = null!;

        [Column("fecha_estado", TypeName = "timestamp(0) without time zone")]
        [Required(ErrorMessage = "La fecha de estado no puede ser nula.")]
        public DateTime FechaEstado { get; set; }

        [Column("observacion_estado")]
        [StringLength(2000,ErrorMessage = "La observación debe contener máximo 2000 caracteres.")]
        public string? ObservacionEstado { get; set; }

        [Column("usuario_ingreso")]
        [Required(ErrorMessage = "El campo usuario ingreso no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El campo usuario ingreso debe ser un número entero mayor a cero.")]
        public long UsuarioIngreso { get; set; }

        [Column("fecha_ingreso", TypeName = "timestamp(0) without time zone")]
        [Required(ErrorMessage = "La fecha de ingreso no puede ser nula.")]
        public DateTime FechaIngreso { get; set; }

        [Column("ubicacion_ingreso")]
        [Required(ErrorMessage = "La ubicación de ingreso no puede ser nula.")]
        [RegularExpression("^[0-9.]+$", ErrorMessage = "La ubicación de ingreso debe contener máximo 200 caracteres.")]
        public string UbicacionIngreso { get; set; } = null!;

        [Column("usuario_modificacion")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El usuario modificación debe ser un número entero mayor a cero.")]
        public long? UsuarioModificacion { get; set; }

        [Column("fecha_modificacion", TypeName = "timestamp(0) without time zone")]
        public DateTime? FechaModificacion { get; set; }

        [Column("ubicacion_modificacion")]
        [RegularExpression("^[0-9.]+$", ErrorMessage = "La ubicación de modificación debe contener máximo 200 caracteres.")]
        public string? UbicacionModificacion { get; set; }


        [InverseProperty("AgePaisCodigoNavigation")]
        public virtual ICollection<AgeFeriadosPaises> AgeFeriadosPaises { get; set; }

        [InverseProperty("AgePaisCodigoNavigation")]
        public virtual ICollection<AgeLicenciatariosPaise> AgeLicenciatariosPaises { get; set; }

        [InverseProperty("AgePaisCodigoNavigation")]
        public virtual ICollection<AgePaisesIdiomas> AgePaisesIdiomas { get; set; }

        [InverseProperty("AgePaisCodigoNavigation")]
        public virtual ICollection<AgePaisesMonedas> AgePaisesMoneda { get; set; }

        [InverseProperty("AgePaisCodigoNavigation")]
        public virtual ICollection<AgeTiposLocalidades> AgeTiposLocalidades { get; set; }

    }
}
