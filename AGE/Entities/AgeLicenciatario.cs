using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AGE.Utils;
using Microsoft.EntityFrameworkCore;

namespace AGE.Entities
{
    [Table("age_licenciatarios", Schema = "sasf_user_desa")]
    public partial class AgeLicenciatario
    {
        public AgeLicenciatario()
        {
            AgeDepartamentos = new HashSet<AgeDepartamentos>();
            AgeFeriadosExcepciones = new HashSet<AgeFeriadosExcepciones>();
            AgeFormasPagos = new HashSet<AgeFormasPagos>();
            AgeFormasPagosInstFinas = new HashSet<AgeFormasPagosInstFina>();
            AgeInstitucionesFinancieras = new HashSet<AgeInstitucionesFinancieras>();
            AgeLicenciatParamVigencia = new HashSet<AgeLicenciatParamVigencias>();
            AgeLicenciatariTransacciones = new HashSet<AgeLicenciatariTransaccione>();
            AgeLicenciatariosAplicacions = new HashSet<AgeLicenciatariosAplicaciones>();
            AgeLicenciatariosPaises = new HashSet<AgeLicenciatariosPaise>();
            AgeLicenciatariosSecuencia = new HashSet<AgeLicenciatariosSecuencia>();
            AgeMonedasCotizaciones = new HashSet<AgeMonedasCotizaciones>();
            AgePerfiles = new HashSet<AgePerfiles>();
            AgePersonas = new HashSet<AgePersonas>();
            AgeRuta = new HashSet<AgeRutas>();
            AgeSucursales = new HashSet<AgeSucursales>();
            AgeTiposSucursales = new HashSet<AgeTiposSucursales>();
            AgeUsuarios = new HashSet<AgeUsuarios>();
            InverseAgeLicencCodigoNavigation = new HashSet<AgeLicenciatario>();
        }

        [Key]
        [Column("codigo")]
        [Required(ErrorMessage = "El código Licenciatario no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código Licenciatario debe ser un número entero mayor a cero.")]
        public int Codigo { get; set; }

        [Column("age_cla_co_codigo")]
        [Required(ErrorMessage = "La clase contribuyente no puede ser nula.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "La clase contribuyente  debe ser un número entero mayor a cero.")]
        public long AgeClaCoCodigo { get; set; }

        [Column("age_tip_id_codigo")]
        [Required(ErrorMessage = "El tipo de identificacion no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El tipo de identificación debe ser un número entero mayor a cero.")]
        public int AgeTipIdCodigo { get; set; }

        [Column("nombre_corto")]
        [Required(ErrorMessage = "El nombre corto es requerido.")]
        [StringLength(50, ErrorMessage = "El nombre corto debe contener máximo 50 caracteres.")]
        public string NombreCorto { get; set; } = null!;

        [Column("representante_legal")]
        [Required(ErrorMessage = "El representante legal no puede ser nulo.")]
        [StringLength(200, ErrorMessage = "El representante legal debe contener máximo 200 caracteres.")]
        public string RepresentanteLegal { get; set; } = null!;

        [Column("direccion_principal")]
        [StringLength(200, ErrorMessage = "La dirección debe contener máximo 200 caracteres.")]
        [Required(ErrorMessage = "La dirección no puede ser nula.")]
        public string DireccionPrincipal { get; set; } = null!;

        [Column("telefono1")]
        [RegularExpression("^\\d{10}$", ErrorMessage = "El teléfono celular 1 debe contener 10 caracteres numéricos.")]
        public string Telefono1 { get; set; } = null!;

        [Column("e_mail2")]
        [RegularExpression("^[a-zA-Z0-9_!#$%&’*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$", ErrorMessage = "El formato del email 2 no es válido.")]
        public string? EMail2 { get; set; }

        [Column("es_corporacion")]
        [RegularExpression("^[SN]$", ErrorMessage = $"El campo Es Corporación debe ser {Globales.SI} o {Globales.NO}.")]
        public string? EsCorporacion { get; set; }

        [Column("observacion")]
        [StringLength(2000, ErrorMessage = "La observación debe contener máximo 2000 caracteres.")]
        public string? Observacion { get; set; }

        [Column("pagina_web")]
        [StringLength(200 , ErrorMessage = "La página web no puede tener más de 200 caracteres.")]
        [Url(ErrorMessage = "La URL de la página web no es válida.")]
        public string? PaginaWeb { get; set; }

        [Column("contribuyente_especial_resoluc")]
        [StringLength(13, ErrorMessage = "La resolución especial del contribuyente no puede tener más de 13 caracteres.")]
        public string? ContribuyenteEspecialResoluc { get; set; }
        
        [Column("age_licenc_codigo")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de licenciatario debe ser un número entero mayor a cero.")]
        public int? AgeLicencCodigo { get; set; }

        [Column("razon_social")]
        [StringLength(2000, ErrorMessage = "La razón social debe contener máximo 2000 caracteres.")]
        public string? RazonSocial { get; set; }

        [Column("nombre_comercial")]
        [StringLength(2000, ErrorMessage = "El nombre comercial debe contener máximo 2000 caracteres.")]
        [Required(ErrorMessage = "El nombre comercial no puede ser nulo.")]
        public string NombreComercial { get; set; } = null!;

        [Column("numero_identificacion")]
        [Required(ErrorMessage = "El número de identificación es requerido.")]
        [RegularExpression("^\\d{10}$", ErrorMessage = "El número identificación debe contener 10 dígitos numéricos.")]
        public string NumeroIdentificacion { get; set; } = null!;

        [Column("telefono2")]
        [RegularExpression("^\\d{10}$", ErrorMessage = "El teléfono celular 2 debe contener 10 caracteres numéricos.")]
        public string? Telefono2 { get; set; }

        [Column("e_mail1")]
        [RegularExpression("^[a-zA-Z0-9_!#$%&’*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$", ErrorMessage = "El formato del email 1 no es válido.")]
        public string? EMail1 { get; set; }

        [Column("obligado_llevar_contabilidad")]
        [Required(ErrorMessage = "El campo obligado a llevar contabilidad no puede ser nulo.")]
        [RegularExpression("^[SN]$", ErrorMessage = $"El campo obligado a llevar contabilidad debe ser {Globales.SI} o {Globales.NO}.")]
        public string ObligadoLlevarContabilidad { get; set; } = null!;

        [Column("ruta_licenciatario")]
        [Required(ErrorMessage = "La ruta del licenciatario no puede ser nula")]
        [StringLength(2000,ErrorMessage = "La ruta del licenciatario debe contener máximo 2000 caracteres.")]
        public string RutaLicenciatario { get; set; } = null!;

        [Column("estado")]
        [Required(ErrorMessage = "El estado no puede ser nulo.")]
        [RegularExpression("^[AI]$", ErrorMessage = $"El campo estado debe ser {Globales.ESTADO_ACTIVO} o {Globales.ESTADO_INACTIVO}.")]
        public string Estado { get; set; } = null!;

        [Required(ErrorMessage = "La fecha de estado no puede ser nula.")]
        [Column("fecha_estado", TypeName = "timestamp(0) without time zone")]
        public DateTime FechaEstado { get; set; }

        [Column("observacion_estado")]
        [StringLength(2000,ErrorMessage = "La observación debe contener máximo 2000 caracteres.")]
        public string? ObservacionEstado { get; set; }

        [Required(ErrorMessage = "El campo usuario ingreso no puede ser nulo.")]
        [Column("usuario_ingreso")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El campo usuario ingreso debe ser un número entero mayor a cero.")]
        public long UsuarioIngreso { get; set; }

        [Required(ErrorMessage = "La fecha de ingreso no puede ser nula.")]
        [Column("fecha_ingreso", TypeName = "timestamp(0) without time zone")]
        public DateTime FechaIngreso { get; set; }

        [Required(ErrorMessage = "La ubicación de ingreso no puede ser nula.")]
        [Column("ubicacion_ingreso")]
        [RegularExpression("^[0-9.]+$", ErrorMessage = "La ubicación de ingreso debe contener máximo 200 caracteres.")]
        public string? UbicacionIngreso { get; set; }

        [Column("usuario_modificacion")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El usuario modificación debe ser un número entero mayor a cero.")]
        public long? UsuarioModificacion { get; set; }

        [Column("fecha_modificacion", TypeName = "timestamp(0) without time zone")]
        public DateTime? FechaModificacion { get; set; }

        [RegularExpression("^[0-9.]+$", ErrorMessage = "La ubicación de modificación debe contener máximo 200 caracteres. SSS")]
        [Column("ubicacion_modificacion")]
        public string? UbicacionModificacion { get; set; }

        [Column("descripcion")]
        [StringLength(2000, ErrorMessage = "La descripción debe contener máximo 2000 caracteres.")]
        public string? Descripcion { get; set; } = null!;


        [ForeignKey("AgeClaCoCodigo")]
        [InverseProperty("AgeLicenciatarios")]
        public virtual AgeClasesContribuyentes AgeClaCoCodigoNavigation { get; set; } = null!;
        [ForeignKey("AgeLicencCodigo")]
        [InverseProperty("InverseAgeLicencCodigoNavigation")]
        public virtual AgeLicenciatario? AgeLicencCodigoNavigation { get; set; }
        [ForeignKey("AgeTipIdCodigo")]
        [InverseProperty("AgeLicenciatarios")]
        public virtual AgeTiposIdentificaciones AgeTipIdCodigoNavigation { get; set; } = null!;

        [InverseProperty("AgeLicencCodigoNavigation")]
        public virtual ICollection<AgeDepartamentos> AgeDepartamentos { get; set; }
        [InverseProperty("AgeLicencCodigoNavigation")]
        public virtual ICollection<AgeFeriadosExcepciones> AgeFeriadosExcepciones { get; set; }
        [InverseProperty("AgeLicencCodigoNavigation")]
        public virtual ICollection<AgeFormasPagos> AgeFormasPagos { get; set; }
        [InverseProperty("AgeLicencCodigoNavigation")]
        public virtual ICollection<AgeFormasPagosInstFina> AgeFormasPagosInstFinas { get; set; }
        [InverseProperty("AgeLicencCodigoNavigation")]
        public virtual ICollection<AgeInstitucionesFinancieras> AgeInstitucionesFinancieras { get; set; }
        [InverseProperty("AgeLicencCodigoNavigation")]
        public virtual ICollection<AgeLicenciatParamVigencias> AgeLicenciatParamVigencia { get; set; }
        [InverseProperty("AgeLicencCodigoNavigation")]
        public virtual ICollection<AgeLicenciatariTransaccione> AgeLicenciatariTransacciones { get; set; }
        [InverseProperty("AgeLicencCodigoNavigation")]
        public virtual ICollection<AgeLicenciatariosAplicaciones> AgeLicenciatariosAplicacions { get; set; }
        [InverseProperty("AgeLicencCodigoNavigation")]
        public virtual ICollection<AgeLicenciatariosPaise> AgeLicenciatariosPaises { get; set; }
        [InverseProperty("AgeLicencCodigoNavigation")]
        public virtual ICollection<AgeLicenciatariosSecuencia> AgeLicenciatariosSecuencia { get; set; }
        [InverseProperty("AgeLicencCodigoNavigation")]
        public virtual ICollection<AgeMonedasCotizaciones> AgeMonedasCotizaciones { get; set; }
        [InverseProperty("AgeLicencCodigoNavigation")]
        public virtual ICollection<AgePerfiles> AgePerfiles { get; set; }
        [InverseProperty("AgeLicencCodigoNavigation")]
        public virtual ICollection<AgePersonas> AgePersonas { get; set; }
        [InverseProperty("AgeLicencCodigoNavigation")]
        public virtual ICollection<AgeRutas> AgeRuta { get; set; }
        [InverseProperty("AgeLicencCodigoNavigation")]
        public virtual ICollection<AgeSucursales> AgeSucursales { get; set; }
        [InverseProperty("AgeLicencCodigoNavigation")]
        public virtual ICollection<AgeTiposSucursales> AgeTiposSucursales { get; set; }
        [InverseProperty("AgeLicencCodigoNavigation")]
        public virtual ICollection<AgeUsuarios> AgeUsuarios { get; set; }
        [InverseProperty("AgeLicencCodigoNavigation")]
        public virtual ICollection<AgeLicenciatario> InverseAgeLicencCodigoNavigation { get; set; }
    }
}
