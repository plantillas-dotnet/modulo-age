﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.InteropServices;
using AGE.Utils;
using Microsoft.EntityFrameworkCore;

namespace AGE.Entities
{
    [Table("age_usuarios", Schema = "sasf_user_desa")]
    public partial class AgeUsuarios
    {
        public AgeUsuarios()
        {
            AgeFavoritos = new HashSet<AgeFavoritos>();
            AgeUsuarioParamVigencia = new HashSet<AgeUsuarioParamVigencias>();
            AgeUsuariosPerfiles = new HashSet<AgeUsuariosPerfiles>();
            AgeUsuariosPuntoEmisions = new HashSet<AgeUsuariosPuntoEmision>();
        }

        [Key]
        [Column("age_licenc_codigo")]
        [Required(ErrorMessage = "El código de licenciatario no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de licenciatario debe ser un número entero mayor a cero.")]
        public int AgeLicencCodigo { get; set; }

        [Key]
        [Column("codigo")]
        [Required(ErrorMessage = "El código del usuario no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código del usuario debe ser un número entero mayor a cero.")]
        public long Codigo { get; set; }

        [Column("age_person_age_licenc_codigo")]        
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de licenciatario de la persona debe ser un número entero mayor a cero.")]
        public int? AgePersonAgeLicencCodigo { get; set; }

        [Column("age_person_codigo")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de la persona debe ser un número entero mayor a cero.")]
        public long? AgePersonCodigo { get; set; }

        [Column("age_sucurs_age_licenc_codigo")]
        [Required(ErrorMessage = "El código de licenciatario de la sucursal no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de licenciatario de la sucursal debe ser un número entero mayor a cero.")]
        public int AgeSucursAgeLicencCodigo { get; set; }

        [Column("age_sucurs_codigo")]
        [Required(ErrorMessage = "El código de la sucursal no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de la sucursal debe ser un número entero mayor a cero.")]
        public int AgeSucursCodigo { get; set; }

        [Column("age_tip_id_codigo")]
        [Required(ErrorMessage = "El código del tipo de identificación no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código del tipo de identificación debe ser un número entero mayor a cero.")]
        public int AgeTipIdCodigo { get; set; }

        [Column("archivo_foto")]
        [StringLength(200, ErrorMessage = "El archivo de la foto debe contener máximo 200 caracteres.")]
        public string? ArchivoFoto { get; set; }

        [Column("codigo_externo")]
        [Required(ErrorMessage = "El usuario no puede ser nulo.")]
        [StringLength(200, ErrorMessage = "El código externo debe contener máximo 200 caracteres.")]
        public string CodigoExterno { get; set; } = null!;

        [Column("clave")]
        [Required(ErrorMessage = "La clave no puede ser nula.")]
        [StringLength(150, ErrorMessage = "La clave debe contener máximo 150 caracteres.")]
        public string Clave { get; set; } = null!;

        [Column("numero_identificacion")]
        [Required(ErrorMessage = "El número identificación no puede ser nulo.")]
        [RegularExpression("^\\d{10}$", ErrorMessage = "El número identificación debe contener 10 dígitos numéricos.")]
        public string NumeroIdentificacion { get; set; } = null!;

        [Column("nombres")]
        [Required(ErrorMessage = "Los nombres no pueden ser nulo.")]
        [StringLength(200, ErrorMessage = "Los nombres deben contener máximo 200 caracteres.")]
        public string Nombres { get; set; } = null!;

        [Column("apellidos")]
        [Required(ErrorMessage = "Los apellidos no pueden ser nulo.")]
        [StringLength(200, ErrorMessage = "Los apellidos deben contener máximo 200 caracteres.")]
        public string Apellidos { get; set; } = null!;

        [Column("mail_principal")]
        [StringLength(200, ErrorMessage = "El email debe contener máximo 200 caracteres.")]
        [RegularExpression("^[a-zA-Z0-9_!#$%&’*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$", ErrorMessage = "El formato del mail principal no es válido.")]
        public string MailPrincipal { get; set; } = null!;

        [Column("telefono_celular")]
        [Required(ErrorMessage = "El telefono celular no puede ser nulo.")]
        [RegularExpression("^\\d{10}$", ErrorMessage = "El telefono celular debe contener 10 dígitos numéricos.")]
        public string TelefonoCelular { get; set; } = null!;

        [Column("tipo_usuario")]
        [Required(ErrorMessage = "El tipo usuario no puede ser nulo.")]
        [RegularExpression("["+Globales.TIPO_I+Globales.TIPO_E+"]", ErrorMessage = "El tipo usuario debe ser I o E.")]
        public string TipoUsuario { get; set; } = null!;

        [Column("primer_ingreso")]
        [Required(ErrorMessage = "El primer ingreso no puede ser nulo.")]
        [RegularExpression("^[SN]$", ErrorMessage = $"El campo estado debe ser {Globales.SI} o {Globales.NO}.")]
        public string PrimerIngreso { get; set; } = null!;

        [Column("tipo_registro")]
        [RegularExpression("["+Globales.USUARIO_TIPO_REGISTRO_FACEBOOK+Globales.USUARIO_TIPO_REGISTRO_GMAIL+Globales.USUARIO_TIPO_REGISTRO_NORMAL+Globales.USUARIO_TIPO_REGISTRO_A+ "]?$", ErrorMessage = "El tipo de registro puede ser "+Globales.USUARIO_TIPO_REGISTRO_FACEBOOK+" (facebook), "+Globales.USUARIO_TIPO_REGISTRO_GMAIL+" (gmail), "+Globales.USUARIO_TIPO_REGISTRO_NORMAL+" (normal) o "+Globales.USUARIO_TIPO_REGISTRO_A)]
        public string? TipoRegistro { get; set; }

        [Required(ErrorMessage = "El estado no puede ser nulo.")]
        [Column("estado")]
        [RegularExpression("^[AI]$", ErrorMessage = $"El campo estado debe ser {Globales.ESTADO_ACTIVO} o {Globales.ESTADO_INACTIVO}.")]
        public string Estado { get; set; } = null!;

        [Required(ErrorMessage = "La fecha de estado no puede ser nula.")]
        [Column("fecha_estado", TypeName = "timestamp(0) without time zone")]
        public DateTime FechaEstado { get; set; }

        [Column("observacion_estado")]
        [StringLength(2000,ErrorMessage = "La observación debe contener máximo 2000 caracteres.")]
        public string? ObservacionEstado { get; set; }

        [Required(ErrorMessage = "El campo usuario ingreso no puede ser nulo.")]
        [Column("usuario_ingreso")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El campo usuario ingreso debe ser un número entero mayor a cero.")]
        public long UsuarioIngreso { get; set; }

        [Required(ErrorMessage = "La fecha de ingreso no puede ser nula.")]
        [Column("fecha_ingreso", TypeName = "timestamp(0) without time zone")]
        public DateTime FechaIngreso { get; set; }

        [Required(ErrorMessage = "La ubicación de ingreso no puede ser nula.")]
        [Column("ubicacion_ingreso")]
        [RegularExpression("^[0-9.]+$", ErrorMessage = "La ubicación de ingreso debe contener máximo 200 caracteres.")]
        public string? UbicacionIngreso { get; set; }

        [Column("usuario_modificacion")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El usuario modificación debe ser un número entero mayor a cero.")]
        public long? UsuarioModificacion { get; set; }

        [Column("fecha_modificacion", TypeName = "timestamp(0) without time zone")]
        public DateTime? FechaModificacion { get; set; }

        [RegularExpression("^[0-9.]+$", ErrorMessage = "La ubicación de modificación debe contener máximo 200 caracteres. SSS")]
        [Column("ubicacion_modificacion")]
        public string? UbicacionModificacion { get; set; }

        [Column("password")]
        public string? Password { get; set; }

        [Column("token_firebase")]
        public string? TokenFirebase { get; set; }

        [ForeignKey("AgeLicencCodigo")]
        [InverseProperty("AgeUsuarios")]
        public virtual AgeLicenciatario AgeLicencCodigoNavigation { get; set; } = null!;

        [ForeignKey("AgePersonCodigo,AgePersonAgeLicencCodigo")]
        [InverseProperty("AgeUsuarios")]
        public virtual AgePersonas? AgePerson { get; set; }

        [ForeignKey("AgeSucursCodigo,AgeSucursAgeLicencCodigo")]
        [InverseProperty("AgeUsuarios")]
        public virtual AgeSucursales AgeSucurs { get; set; } = null!;

        [ForeignKey("AgeTipIdCodigo")]
        [InverseProperty("AgeUsuarios")]
        public virtual AgeTiposIdentificaciones AgeTipIdCodigoNavigation { get; set; } = null!;

        [InverseProperty("AgeUsuari")]
        public virtual ICollection<AgeFavoritos> AgeFavoritos { get; set; }
        [InverseProperty("AgeUsuari")]
        public virtual ICollection<AgeUsuarioParamVigencias> AgeUsuarioParamVigencia { get; set; }

        [InverseProperty("AgeUsuari")]
        public virtual ICollection<AgeUsuariosPerfiles> AgeUsuariosPerfiles { get; set; }

        [InverseProperty("AgeUsuari")]
        public virtual ICollection<AgeUsuariosPuntoEmision> AgeUsuariosPuntoEmisions { get; set; }
    }
}
