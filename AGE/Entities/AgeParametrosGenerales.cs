﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AGE.Utils;
using Microsoft.EntityFrameworkCore;

namespace AGE.Entities
{
    [Table("age_parametros_generales", Schema = "sasf_user_desa")]
    public partial class AgeParametrosGenerales
    {
        public AgeParametrosGenerales()
        {
            AgeLicenciatParamVigencia = new HashSet<AgeLicenciatParamVigencias>();
            AgeParamGeneralVigencia = new HashSet<AgeParamGeneralVigencias>();
            AgeUsuarioParamVigencia = new HashSet<AgeUsuarioParamVigencias>();
        }

        [Key]
        [Column("codigo")]
        [Required(ErrorMessage = "El código no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código debe ser un número entero mayor a cero.")]
        public int Codigo { get; set; }

        [Column("descripcion")]
        [Required(ErrorMessage = "La descripción no puede ser nula.")]
        [StringLength(200, ErrorMessage = "La descripción debe contener máximo 200 caracteres.")]
        public string Descripcion { get; set; } = null!;

        [Column("tipo_dato_parametro")]
        [Required(ErrorMessage = "El tipo de dato del parámetro no puede ser nulo.")]
        [RegularExpression("^[VIDN]$", ErrorMessage = $"El campo tipo de dato del parámetro debe ser {Globales.TIPO_DATO_VARCHAR}, {Globales.TIPO_DATO_INT}, {Globales.TIPO_DATO_DATE} o {Globales.TIPO_DATO_N}.")]
        public string TipoDatoParametro { get; set; } = null!;

        [Column("estado")]
        [Required(ErrorMessage = "El estado no puede ser nulo.")]
        [RegularExpression("^[AI]$", ErrorMessage = $"El campo estado debe ser {Globales.ESTADO_ACTIVO} o {Globales.ESTADO_INACTIVO}.")]
        public string Estado { get; set; } = null!;

        [Required(ErrorMessage = "La fecha de estado no puede ser nula.")]
        [Column("fecha_estado", TypeName = "timestamp(0) without time zone")]
        public DateTime FechaEstado { get; set; }

        [Column("observacion_estado")]
        [StringLength(2000, ErrorMessage = "La observación debe contener máximo 2000 caracteres.")]
        public string? ObservacionEstado { get; set; }

        [Required(ErrorMessage = "El campo usuario ingreso no puede ser nulo.")]
        [Column("usuario_ingreso")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El campo usuario ingreso debe ser un número entero mayor a cero.")]
        public long UsuarioIngreso { get; set; }

        [Required(ErrorMessage = "La fecha de ingreso no puede ser nula.")]
        [Column("fecha_ingreso", TypeName = "timestamp(0) without time zone")]
        public DateTime FechaIngreso { get; set; }

        [Required(ErrorMessage = "La ubicación de ingreso no puede ser nula.")]
        [Column("ubicacion_ingreso")]
        [RegularExpression("^[0-9.]+$", ErrorMessage = "La ubicación de ingreso debe contener máximo 200 caracteres.")]
        public string? UbicacionIngreso { get; set; }

        [Column("usuario_modificacion")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El usuario modificación debe ser un número entero mayor a cero.")]
        public long? UsuarioModificacion { get; set; }

        [Column("fecha_modificacion", TypeName = "timestamp(0) without time zone")]
        public DateTime? FechaModificacion { get; set; }

        [RegularExpression("^[0-9.]+$", ErrorMessage = "La ubicación de modificación debe contener máximo 200 caracteres. SSS")]
        [Column("ubicacion_modificacion")]
        public string? UbicacionModificacion { get; set; }

        [InverseProperty("AgeParGeCodigoNavigation")]
        public virtual ICollection<AgeLicenciatParamVigencias> AgeLicenciatParamVigencia { get; set; }
        [InverseProperty("AgeParGeCodigoNavigation")]
        public virtual ICollection<AgeParamGeneralVigencias> AgeParamGeneralVigencia { get; set; }
        [InverseProperty("AgeParGeCodigoNavigation")]
        public virtual ICollection<AgeUsuarioParamVigencias> AgeUsuarioParamVigencia { get; set; }
    }
}
