﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AGE.Utils;

namespace AGE.Entities
{
    [Table("age_notificaciones_push", Schema = "sasf_user_desa")]
    public partial class AgeNotificacionesPush
    {

        [Key]
        [Column("age_licenc_codigo")]
        [Required(ErrorMessage = "El código de licenciatario no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de licenciatario debe ser un número entero mayor a cero.")]
        public int AgeLicencCodigo { get; set; }


        [Key]
        [Column("codigo")]
        [Required(ErrorMessage = "El código de la notificación no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de la notificación debe ser un número entero mayor a cero.")]
        public long Codigo { get; set; }


        [Column("age_usuari_age_licenc_codigo")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de licenciatario del usuario debe ser un número entero mayor a cero.")]
        public int? AgeUsuariAgeLicencCodigo { get; set; }


        [Column("age_usuari_codigo")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código del usuario debe ser un número entero mayor a cero.")]
        public long? AgeUsuariCodigo { get; set; }


        [Column("data")]
        [StringLength(255, ErrorMessage = "La data debe contener máximo 255 caracteres.")]
        public string? Data { get; set; }


        [Column("id_firebase_user")]
        [StringLength(255, ErrorMessage = "El id debe contener máximo 255 caracteres.")]
        public string? IdFirebaseUser { get; set; }


        [Column("mensaje")]
        [Required(ErrorMessage = "El mensaje no puede ser nulo.")]
        [StringLength(255, ErrorMessage = "El mensaje debe contener máximo 255 caracteres.")]
        public string Mensaje { get; set; } = null!;


        [Column("titulo")]
        [Required(ErrorMessage = "El título no puede ser nulo.")]
        [StringLength(255, ErrorMessage = "El título debe contener máximo 255 caracteres.")]
        public string Titulo { get; set; } = null!;


        [Column("topico")]
        [StringLength(255, ErrorMessage = "El tópico debe contener máximo 255 caracteres.")]
        public string? Topico { get; set; }


        [Column("imagen")]
        [StringLength(255, ErrorMessage = "La imagen debe contener máximo 255 caracteres.")]
        public string? Imagen { get; set; }


        [Column("estado")]
        [Required(ErrorMessage = "El estado no puede ser nulo.")]
        [RegularExpression("^[AI]$", ErrorMessage = $"El campo estado debe ser {Globales.ESTADO_ACTIVO} o {Globales.ESTADO_INACTIVO}.")]
        public string Estado { get; set; } = null!;


        [Column("fecha_estado", TypeName = "timestamp(0) without time zone")]
        [Required(ErrorMessage = "La fecha de estado no puede ser nula.")]
        public DateTime FechaEstado { get; set; }


        [Column("observacion_estado")]
        [StringLength(2000, ErrorMessage = "La observación debe contener máximo 2000 caracteres.")]
        public string? ObservacionEstado { get; set; }


        [Required(ErrorMessage = "El campo usuario ingreso no puede ser nulo.")]
        [Column("usuario_ingreso")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El campo usuario ingreso debe ser un número entero mayor a cero.")]
        public long UsuarioIngreso { get; set; }


        [Required(ErrorMessage = "La fecha de ingreso no puede ser nula.")]
        [Column("fecha_ingreso", TypeName = "timestamp(0) without time zone")]
        public DateTime FechaIngreso { get; set; }


        [Required(ErrorMessage = "La ubicación de ingreso no puede ser nula.")]
        [Column("ubicacion_ingreso")]
        [RegularExpression("^[0-9.]+$", ErrorMessage = "La ubicación de ingreso debe contener máximo 200 caracteres.")]
        public string? UbicacionIngreso { get; set; }


        [Column("usuario_modificacion")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El usuario modificación debe ser un número entero mayor a cero.")]
        public long? UsuarioModificacion { get; set; }


        [Column("fecha_modificacion", TypeName = "timestamp(0) without time zone")]
        public DateTime? FechaModificacion { get; set; }


        [RegularExpression("^[0-9.]+$", ErrorMessage = "La ubicación de modificación debe contener máximo 200 caracteres.")]
        [Column("ubicacion_modificacion")]
        public string? UbicacionModificacion { get; set; }


    }
}
