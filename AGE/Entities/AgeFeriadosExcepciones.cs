﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AGE.Utils;

namespace AGE.Entities
{
    [Table("age_feriados_excepciones", Schema = "sasf_user_desa")]
    public partial class AgeFeriadosExcepciones
    {
        [Key]
        [Column("age_licenc_codigo")]
        [Required(ErrorMessage = "El código de licenciatario no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de licenciatario debe ser un número entero mayor a cero.")]
        public int AgeLicencCodigo { get; set; }

        [Key]
        [Column("codigo")]
        [Required(ErrorMessage = "El código de la excepción del feriado no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de la excepción del feriado debe ser un número entero mayor a cero.")]
        public int Codigo { get; set; }

        [Column("observacion")]
        [StringLength(2000, ErrorMessage = "La observación debe contener máximo 2000 caracteres.")]
        public string? Observacion { get; set; }

        [Required(ErrorMessage = "La fecha desde no puede ser nula.")]
        [Column("fecha_desde", TypeName = "timestamp(0) without time zone")]
        public DateTime FechaDesde { get; set; }

        [Required(ErrorMessage = "La fecha hasta no puede ser nula.")]
        [Column("fecha_hasta", TypeName = "timestamp(0) without time zone")]
        public DateTime FechaHasta { get; set; }

        [Column("hora_desde", TypeName = "timestamp(0) without time zone")]
        public DateTime? HoraDesde { get; set; }

        [Column("hora_hasta", TypeName = "timestamp(0) without time zone")]
        public DateTime? HoraHasta { get; set; }

        [Column("tipo_excepcion")]
        [Required(ErrorMessage = "El tipo de excepción no puede ser nulo.")]
        [RegularExpression("^[FN]$", ErrorMessage = $"El tipo de excepción debe ser {Globales.TIPO_EXCEPCION_F} o {Globales.TIPO_EXCEPCION_N}.")]
        public string TipoExcepcion { get; set; } = null!;

        [Column("estado")]
        [Required(ErrorMessage = "El estado no puede ser nulo.")]
        [RegularExpression("^[AI]$", ErrorMessage = $"El campo estado debe ser {Globales.ESTADO_ACTIVO} o {Globales.ESTADO_INACTIVO}.")]
        public string Estado { get; set; } = null!;

        [Column("fecha_estado", TypeName = "timestamp(0) without time zone")]
        [Required(ErrorMessage = "La fecha de estado no puede ser nula.")]
        public DateTime FechaEstado { get; set; }

        [Column("observacion_estado")]
        [StringLength(2000,ErrorMessage = "La observación debe contener máximo 2000 caracteres.")]
        public string? ObservacionEstado { get; set; }

        [Column("usuario_ingreso")]
        [Required(ErrorMessage = "El campo usuario ingreso no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El campo usuario ingreso debe ser un número entero mayor a cero.")]
        public long UsuarioIngreso { get; set; }

        [Column("fecha_ingreso", TypeName = "timestamp(0) without time zone")]
        [Required(ErrorMessage = "La fecha de ingreso no puede ser nula.")]
        public DateTime FechaIngreso { get; set; }

        [Column("ubicacion_ingreso")]
        [Required(ErrorMessage = "La ubicación de ingreso no puede ser nula.")]
        [RegularExpression("^[0-9.]+$", ErrorMessage = "La ubicación de ingreso debe contener máximo 200 caracteres.")]
        public string UbicacionIngreso { get; set; } = null!;

        [Column("usuario_modificacion")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El usuario modificación debe ser un número entero mayor a cero.")]
        public long? UsuarioModificacion { get; set; }

        [Column("fecha_modificacion", TypeName = "timestamp(0) without time zone")]
        public DateTime? FechaModificacion { get; set; }

        [Column("ubicacion_modificacion")]
        [RegularExpression("^[0-9.]+$", ErrorMessage = "La ubicación de modificación debe contener máximo 200 caracteres.")]
        public string? UbicacionModificacion { get; set; }

        [ForeignKey("AgeLicencCodigo")]
        [InverseProperty("AgeFeriadosExcepciones")]
        public virtual AgeLicenciatario AgeLicencCodigoNavigation { get; set; } = null!;
    }
}
