﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AGE.Utils;

namespace AGE.Entities
{
    [Table("age_log_errores", Schema = "sasf_user_desa")]
    public partial class AgeLogErrores
    {
        [Key]
        [Column("age_lic_ap_age_aplica_codigo")]
        [Required(ErrorMessage = "El código de la aplicación no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de la aplicación debe ser un número entero mayor a cero.")]
        public int AgeLicApAgeAplicaCodigo { get; set; }

        [Key]
        [Required(ErrorMessage = "El código de licenciatario no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de licenciatario debe ser un número entero mayor a cero.")]
        [Column("age_lic_ap_age_licenc_codigo")]
        public int AgeLicApAgeLicencCodigo { get; set; }

        [Key]
        [Required(ErrorMessage = "El código de log error no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de log error debe ser un número entero mayor a cero.")]
        [Column("codigo")]
        public long Codigo { get; set; }

        [Column("mensaje")]
        [StringLength(2000, ErrorMessage = "El mensaje debe contener máximo 2000 caracteres.")]
        [Required(ErrorMessage = "El mensaje no puede ser nula.")]
        public string Mensaje { get; set; } = null!;

        [Required(ErrorMessage = "Fecha no puede ser nula.")]
        [Column("fecha", TypeName = "timestamp without time zone")]
        public DateTime Fecha { get; set; }

        [Column("estado")]
        [Required(ErrorMessage = "El estado no puede ser nulo.")]
        [RegularExpression("^[AI]$", ErrorMessage = $"El campo estado debe ser {Globales.ESTADO_ACTIVO} o {Globales.ESTADO_INACTIVO}.")]
        public string? Estado { get; set; }

        [Required(ErrorMessage = "Fecha estado no puede ser nulo.")]
        [Column("fecha_estado", TypeName = "timestamp without time zone")]
        public DateTime FechaEstado { get; set; }

        [Required(ErrorMessage = "La fecha de ingreso no puede ser nula.")]
        [Column("fecha_ingreso", TypeName = "timestamp without time zone")]
        public DateTime? FechaIngreso { get; set; }

        [Column("fecha_modificacion", TypeName = "timestamp without time zone")]
        public DateTime? FechaModificacion { get; set; }

        [Column("observacion_estado")]
        [StringLength(2000,ErrorMessage = "La observación debe contener máximo 2000 caracteres.")]
        public string? ObservacionEstado { get; set; }

        [Column("ubicacion_ingreso")]
        [Required(ErrorMessage = "La ubicación de ingreso no puede ser nula.")]
        [RegularExpression("^[0-9.]+$", ErrorMessage = "La ubicación de ingreso debe contener máximo 200 caracteres.")]
        public string? UbicacionIngreso { get; set; }

        [Column("ubicacion_modificacion")]
        [RegularExpression("^[0-9.]+$", ErrorMessage = "La ubicación de modificación debe contener máximo 200 caracteres.")]
        public string? UbicacionModificacion { get; set; }

        [Column("usuario_ingreso")]
        [Required(ErrorMessage = "El campo usuario ingreso no puede ser nulo.")]
        [Range(1, long.MaxValue, ErrorMessage = "El código del usuario de ingreso debe ser mayor a 0.")]
        public long? UsuarioIngreso { get; set; }

        [Column("usuario_modificacion")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El usuario modificación debe ser un número entero mayor a cero.")]
        public long? UsuarioModificacion { get; set; }

        [ForeignKey("AgeLicApAgeLicencCodigo,AgeLicApAgeAplicaCodigo")]
        [InverseProperty("AgeLogErrores")]
        public virtual AgeLicenciatariosAplicaciones AgeLicApAge { get; set; } = null!;
    }
}
