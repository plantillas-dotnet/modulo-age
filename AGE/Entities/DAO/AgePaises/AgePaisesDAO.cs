﻿
namespace AGE.Entities.DAO.AgePaises
{
    public class AgePaisesDAO
    {
        public int Codigo { get; set; }

        public string Descripcion { get; set; } = null!;

        public string Estado { get; set; } = null!;
    }
}
