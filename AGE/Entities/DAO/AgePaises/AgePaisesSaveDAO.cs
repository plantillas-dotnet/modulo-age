﻿
using AGE.Entities.DAO.AgeCamposGenerales;

namespace AGE.Entities.DAO.AgePaises
{
    public class AgePaisesSaveDAO : AgeCamposGeneralesDAO
    {
        public int Codigo { get; set; }

        public string Descripcion { get; set; } = null!;

    }
}
