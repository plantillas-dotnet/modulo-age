﻿namespace AGE.Entities.DAO.AgeLicenciatParamVigencia
{
    public class AgeLicenciatParamVigenciasDAO
    {
        public AgeLicenciatParamVigenciasPKDAO Id { get; set; }
        public string? Observacion { get; set; }
        public DateTime FechaDesde { get; set; }
        public DateTime? FechaHasta { get; set; }
        public string ValorParametro { get; set; } = null!;
        public string Estado { get; set; } = null!;
    }
}
