﻿using AGE.Entities.DAO.AgeCamposGenerales;

namespace AGE.Entities.DAO.AgeLicenciatParamVigencia
{
    public class AgeLicenciatParamVigenciasSaveDAO : AgeCamposGeneralesDAO
    {
        public AgeLicenciatParamVigenciasPKDAO Id { get; set; }
        public string? Observacion { get; set; }
        public DateTime FechaDesde { get; set; }
        public DateTime? FechaHasta { get; set; }
        public string ValorParametro { get; set; } = null!;
    }
}
