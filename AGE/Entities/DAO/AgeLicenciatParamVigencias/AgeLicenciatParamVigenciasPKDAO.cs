﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;

namespace AGE.Entities.DAO.AgeLicenciatParamVigencia
{
    public class AgeLicenciatParamVigenciasPKDAO
    {
        [Required(ErrorMessage = "El código de licenciatario no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de licenciatario debe ser un número entero mayor a cero.")]
        [FromRoute(Name ="codigoLicenciatario")]
        public int AgeLicencCodigo { get; set; }

        [Required(ErrorMessage = "El código de parámetro general no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de parámetro general debe ser un número entero mayor a cero.")]
        [FromRoute(Name = "codigoParametro")]
        public int AgeParGeCodigo { get; set; }

        [Required(ErrorMessage = "El código de secuencia no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de secuencia debe ser un número entero mayor a cero.")]
        [FromRoute(Name = "id")]
        public long Codigo { get; set; }
    }
}
