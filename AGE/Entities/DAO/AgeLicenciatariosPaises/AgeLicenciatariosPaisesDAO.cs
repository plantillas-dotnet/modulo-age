﻿namespace AGE.Entities.DAO.AgeLicenciatariosPaises
{
    public class AgeLicenciatariosPaisesDAO
    {
        public AgeLicenciatariosPaisesPKDAO Id { get; set; }
        public string Principal { get; set; } = null!;
        public int? AgeLicPaAgeLicencCodigo { get; set; }
        public int? AgeLicPaAgePaisCodigo { get; set; }
        public string Estado { get; set; } = null!;
    }
}
