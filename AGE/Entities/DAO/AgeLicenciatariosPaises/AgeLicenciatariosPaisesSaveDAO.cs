﻿using AGE.Entities.DAO.AgeCamposGenerales;

namespace AGE.Entities.DAO.AgeLicenciatariosPaises
{
    public class AgeLicenciatariosPaisesSaveDAO : AgeCamposGeneralesDAO
    {
        public AgeLicenciatariosPaisesPKDAO Id { get; set; }
        public string Principal { get; set; } = null!;
        public int? AgeLicPaAgeLicencCodigo { get; set; }
        public int? AgeLicPaAgePaisCodigo { get; set; }
    }
}
