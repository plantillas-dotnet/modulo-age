﻿using AGE.Utils;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;

namespace AGE.Entities.DAO.AgePaisesIdioma
{
    public class AgePaisesIdiomasPKDAO
    {
        [Required(ErrorMessage = "El código de idioma no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código idioma debe ser un número entero mayor a cero.")]
        [FromRoute(Name ="codigoIdioma")]
        public int ageIdiomaCodigo { get; set; }

        [Required(ErrorMessage = "El código de país no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de país debe ser un número entero mayor a cero.")]
        [FromRoute(Name = "codigoPais")]
        public int agePaisCodigo { get; set; }

    }
}
