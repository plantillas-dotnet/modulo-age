﻿namespace AGE.Entities.DAO.AgePaisesIdioma
{
    public class AgePaisesIdiomasDAO
    {
        public AgePaisesIdiomasPKDAO Id { get; set; }
        public string? Descripcion { get; set; }
        public string TipoPrincipalSecundario { get; set; } = null!;
        public short? OrdenPrincipalSecundario { get; set; }
        public string Estado { get; set; } = null!;
    }
}
