﻿using AGE.Entities.DAO.AgeCamposGenerales;

namespace AGE.Entities.DAO.AgePaisesIdioma
{
    public class AgePaisesIdiomasSaveDAO : AgeCamposGeneralesDAO
    {
        public AgePaisesIdiomasPKDAO Id { get; set; }
        public string? Descripcion { get; set; }
        public string TipoPrincipalSecundario { get; set; } = null!;
        public short? OrdenPrincipalSecundario { get; set; }
    }
}
