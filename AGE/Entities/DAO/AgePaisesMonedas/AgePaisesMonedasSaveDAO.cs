﻿using AGE.Entities.DAO.AgeCamposGenerales;

namespace AGE.Entities.DAO.AgePaisesMonedas
{
    public class AgePaisesMonedasSaveDAO : AgeCamposGeneralesDAO
    {
        public AgePaisesMonedasPKDAO Id { get; set; }
        public string? Descripcion { get; set; }
        public string TipoPrincipalSecundario { get; set; } = null!;
        public short? OrdenPrincipalSecundario { get; set; }
    }
}
