﻿using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using System.Xml.Linq;

namespace AGE.Entities.DAO.AgePaisesMonedas
{
    public class AgePaisesMonedasPKDAO
    {
        [Required(ErrorMessage = "El código de moneda no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código idioma debe ser un número entero mayor a cero.")]
        [FromRoute(Name = "codigoMoneda")]
        public int AgeMonedaCodigo { get; set; }
        [Required(ErrorMessage = "El código de país no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de país debe ser un número entero mayor a cero.")]
        [FromRoute(Name = "codigoPais")]
        public int AgePaisCodigo { get; set; }

    }
}
