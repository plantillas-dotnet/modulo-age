﻿
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace AGE.Entities.DAO.AgeLicenciatariosAplicaSecu
{
    public class AgeLicenciatariosAplicaSecuPKDAO
    {
        [Required(ErrorMessage = "El código de licenciatario no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de licenciatario debe ser un número entero mayor a cero.")]
        [FromRoute(Name = "codigoLicenciatario")]
        public int ageLicencCodigo { get; set; }

        [Required(ErrorMessage = "El código de aplicación no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de aplicación debe ser un número entero mayor a cero.")]
        [FromRoute(Name = "codigoAplicacion")]
        public int ageAplicaCodigo { get; set; }

        [Required(ErrorMessage = "El código de secuencia no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de secuencia debe ser un número entero mayor a cero.")]
        [FromRoute(Name = "id")]
        public int codigo { get; set; }
    }
}
