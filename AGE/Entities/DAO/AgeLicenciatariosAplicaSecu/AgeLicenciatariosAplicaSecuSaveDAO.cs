﻿using AGE.Entities.DAO.AgeCamposGenerales;

namespace AGE.Entities.DAO.AgeLicenciatariosAplicaSecu
{
    public class AgeLicenciatariosAplicaSecuSaveDAO : AgeCamposGeneralesDAO
    {
        public AgeLicenciatariosAplicaSecuPKDAO Id { get; set; }

        public string Descripcion { get; set; }

        public long ValorInicial { get; set; }

        public int IncrementaEn { get; set; }

        public long? ValorActual { get; set; }

        public string Ciclica { get; set; }
    }
}
