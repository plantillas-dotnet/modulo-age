﻿
namespace AGE.Entities.DAO.AgeLicenciatariosAplicaSecu
{
    public class AgeLicenciatariosAplicaSecuDAO
    {
       
        public AgeLicenciatariosAplicaSecuPKDAO Id { get; set; }

        public string Descripcion { get; set; }

        
        public long ValorInicial { get; set; }

       
        public int IncrementaEn { get; set; }

       
        public long ValorActual { get; set; }

        
        public string Ciclica { get; set; }

       
        
        public string Estado { get; set; }

    }
}
