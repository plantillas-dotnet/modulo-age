﻿using AGE.Entities.DAO.AgeCamposGenerales;

namespace AGE.Entities.DAO.AgeTiposDireccione
{
    public class AgeTiposDireccionesSaveDAO : AgeCamposGeneralesDAO
    {
        public int Codigo { get; set; }
        public string Descripcion { get; set; } = null!;
    }
}
