﻿namespace AGE.Entities.DAO.AgeTiposDireccione
{
    public class AgeTiposDireccionesDAO
    {
        public int Codigo { get; set; }

        public string Descripcion { get; set; } = null!;

        public string Estado { get; set; } = null!;
    }
}
