﻿namespace AGE.Entities.DAO.AgeDiasFeriados
{
    public class AgeDiasFeriadosDAO
    {
        public string Estado { get; set; }

        public int Codigo { get; set; }

        public string Descripcion { get; set; } = null!;

        public DateTime FechaDesde { get; set; }

        public DateTime FechaHasta { get; set; }

        public DateTime? HoraDesde { get; set; }

        public DateTime? HoraHasta { get; set; }
    }
}
