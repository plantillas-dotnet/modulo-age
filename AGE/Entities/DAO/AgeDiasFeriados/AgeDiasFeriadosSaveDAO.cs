﻿using AGE.Entities.DAO.AgeCamposGenerales;

namespace AGE.Entities.DAO.AgeDiasFeriados
{
    public class AgeDiasFeriadosSaveDAO : AgeCamposGeneralesDAO
    {
        public int Codigo { get; set; } 

        public string Descripcion { get; set; } = null!;

        public DateTime FechaDesde { get; set; }

        public DateTime FechaHasta { get; set; }

        public DateTime? HoraDesde { get; set; }

        public DateTime? HoraHasta { get; set; }
    }
}
