﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;
using System.Xml.Linq;

namespace AGE.Entities.DAO.AgeMensajesIdiomas
{
    public class AgeMensajesIdiomaPKDAO
    {

        [Required(ErrorMessage = "El código del idioma no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código del idioma debe ser un número entero mayor a cero.")]
        [FromRoute(Name = "codigoIdioma")]
        public int AgeIdiomaCodigo { get; set; }


        [Required(ErrorMessage = "El código del mensaje no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código del mensaje debe ser un número entero mayor a cero.")]
        [FromRoute(Name = "codigoMensaje")]
        public long AgeMensajCodigo { get; set; }

    }
}
