﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace AGE.Entities.DAO.AgeMensajesIdiomas
{
    public class AgeMensajesIdiomaDAO
    {
        public AgeMensajesIdiomaPKDAO Id { get; set; }

        public string DescripcionMsg { get; set; } = null!;

        public string SolucionMsg { get; set; }

        public string Estado { get; set; } = null!;

    }
}
