﻿using AGE.Entities.DAO.AgeCamposGenerales;

namespace AGE.Entities.DAO.AgeMensajesIdiomas
{
    public class AgeMensajesIdiomaSaveDAO : AgeCamposGeneralesDAO
    {

        public AgeMensajesIdiomaPKDAO Id { get; set; }

        public string DescripcionMsg { get; set; } = null!;

        public string SolucionMsg { get; set; }

    }
}
