﻿using AGE.Entities.DAO.AgeCamposGenerales;

namespace AGE.Entities.DAO.AgeFeriadosLocalidade
{
    public class AgeFeriadosLocalidadesSaveDAO : AgeCamposGeneralesDAO
    {

        public AgeFeriadosLocalidadesPKDAO Id { get; set; }


        public DateTime FechaEjecucionDesde { get; set; }


        public DateTime FechaEjecucionHasta { get; set; }


        public DateTime? HoraEjecucionDesde { get; set; }


        public DateTime? HoraEjecucionHasta { get; set; }

    }
}
