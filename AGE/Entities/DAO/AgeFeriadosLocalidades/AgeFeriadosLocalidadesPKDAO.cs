﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;

namespace AGE.Entities.DAO.AgeFeriadosLocalidade
{
    public class AgeFeriadosLocalidadesPKDAO
    {

        [FromRoute(Name = "codigoPais")]
        [Required(ErrorMessage = "El código del país no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código del país debe ser un número entero mayor a cero.")]
        public int AgeAgeTipLoAgePaisCodigo { get; set; }


        [FromRoute(Name = "codigoTipoLocalidad")]
        [Required(ErrorMessage = "El código del tipo localidad no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código del tipo localidad debe ser un número entero mayor a cero.")]
        public int AgeLocaliAgeTipLoCodigo { get; set; }


        [FromRoute(Name = "codigoLocalidad")]
        [Required(ErrorMessage = "El código de la localidad no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de la localidad debe ser un número entero mayor a cero.")]
        public int AgeLocaliCodigo { get; set; }


        [FromRoute(Name = "codigoDiaFeriado")]
        [Required(ErrorMessage = "El código del día feriado no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código del día feriado debe ser un número entero mayor a cero.")]
        public int AgeDiaFeCodigo { get; set; }

    }
}
