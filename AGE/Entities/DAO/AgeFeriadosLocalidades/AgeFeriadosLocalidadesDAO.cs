﻿namespace AGE.Entities.DAO.AgeFeriadosLocalidade
{
    public class AgeFeriadosLocalidadesDAO
    {

        public AgeFeriadosLocalidadesPKDAO Id { get; set; }


        public DateTime FechaEjecucionDesde { get; set; }


        public DateTime FechaEjecucionHasta { get; set; }


        public DateTime? HoraEjecucionDesde { get; set; }


        public DateTime? HoraEjecucionHasta { get; set; }


        public string Estado { get; set; } = null!;

    }
}
