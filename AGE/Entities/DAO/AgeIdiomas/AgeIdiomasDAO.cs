﻿namespace AGE.Entities.DAO.AgeIdioma
{
    public class AgeIdiomasDAO
    {
        public int Codigo { get; set; }
        public string Descripcion { get; set; } = null!;
        public string Estado { get; set; } = null!;
    }
}
