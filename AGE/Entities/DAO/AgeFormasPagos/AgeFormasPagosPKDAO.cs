﻿using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace AGE.Entities.DAO.AgeFormasPago
{
    public class AgeFormasPagosPKDAO
    {

        [Required(ErrorMessage = "El código de licenciatario no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de licenciatario debe ser un número entero mayor a cero.")]
        [FromRoute(Name = "codigoLicenciatario")] 
        public int AgeLicencCodigo { get; set; }


        [Required(ErrorMessage = "El código de la forma de pago no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de la forma de pago debe ser un número entero mayor a cero.")]
        [FromRoute(Name = "id")]
        public int Codigo { get; set; }

    }
}
