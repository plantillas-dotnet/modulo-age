﻿namespace AGE.Entities.DAO.AgeFormasPago
{
    public class AgeFormasPagosDAO
    {

        public AgeFormasPagosPKDAO Id { get; set; }


        public int CodigoInstitucionControl { get; set; }


        public string Descripcion { get; set; } = null!;


        public string Retiene { get; set; } = null!;


        public string? PresentaCajaBancos { get; set; }


        public string Estado { get; set; } = null!;

    }
}
