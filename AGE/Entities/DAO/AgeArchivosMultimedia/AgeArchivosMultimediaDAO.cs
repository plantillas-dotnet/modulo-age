﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace AGE.Entities.DAO.AgeArchivosMultimedia
{
    public class AgeArchivosMultimediaDAO
    {
        public AgeArchivosMultimediaPKDAO Id { get; set; }
        public string? Estado { get; set; } = null;
        public string? Descripcion { get; set; }
        public string Ruta { get; set; } = null!;
        public string Tipo { get; set; } = null!;
        public string? Promocion { get; set; }
    }
}
