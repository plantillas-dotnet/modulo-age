﻿using AGE.Entities.DAO.AgeCamposGenerales;

namespace AGE.Entities.DAO.AgeLocalidades
{
    public class AgeLocalidadesSaveDAO : AgeCamposGeneralesDAO
    {
        public AgeLocalidadesPKDAO Id { get; set; }

        public int? AgeAgeTipLoAgePaisCodigo { get; set; }

        public int? AgeIdiomaCodigo { get; set; }

        public int? AgeLocaliAgeTipLoCodigo { get; set; }

        public int? AgeLocaliCodigo { get; set; }

        public int? AgeMonedaCodigo { get; set; }

        public string Descripcion { get; set; } = null!;

        public decimal? LatitudCentro { get; set; }

        public decimal? LongitudCentro { get; set; }

        public List<List<List<decimal>>>? Poligono { get; set; }

    }
}
