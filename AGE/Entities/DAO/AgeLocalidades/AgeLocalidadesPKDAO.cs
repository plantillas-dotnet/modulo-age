﻿using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using System.Xml.Linq;

namespace AGE.Entities.DAO.AgeLocalidades
{
    public class AgeLocalidadesPKDAO
    {
        [FromRoute(Name = "codigoPais")]
        [Required(ErrorMessage = "El código del país no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código del país debe ser un número entero mayor a cero.")]
        public int AgeTipLoAgePaisCodigo { get; set; }

        [FromRoute(Name = "codigoTipoLocalidad")]
        [Required(ErrorMessage = "El código del tipo localidad no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código del tipo localidad debe ser un número entero mayor a cero.")]
        public int AgeTipLoCodigo { get; set; }

        [FromRoute(Name = "id")]
        [Required(ErrorMessage = "El código de la localidad no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de la localidad debe ser un número entero mayor a cero.")]
        public int Codigo { get; set; }
    }
}
