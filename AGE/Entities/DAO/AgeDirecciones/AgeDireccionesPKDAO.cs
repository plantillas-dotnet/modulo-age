﻿using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using System.Xml.Linq;

namespace AGE.Entities.DAO.AgeDirecciones
{
    public class AgeDireccionesPKDAO
    {
        [Required(ErrorMessage = "El código de persona no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de persona debe ser un número entero mayor a cero.")]
        [FromRoute(Name = "codigoPersona")]
        public long AgePersonCodigo { get; set; }

        [Required(ErrorMessage = "El código de licenciatario no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El agePersonAgeLicencCodigo debe ser un número entero mayor a cero.")]
        [FromRoute(Name = "codigoLicenciatario")]
        public int AgePersonAgeLicencCodigo { get; set; }

        [Required(ErrorMessage = "El código de la ruta no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de la ruta debe ser un número entero mayor a cero.")]
        [FromRoute(Name = "id")]
        public int codigo { get; set; }
    }
}
