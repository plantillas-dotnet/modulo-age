﻿using AGE.Entities.DAO.AgeCamposGenerales;

namespace AGE.Entities.DAO.AgeDirecciones
{
    public class AgeDireccionesSaveDAO : AgeCamposGeneralesDAO
    {
        public AgeDireccionesPKDAO Id { get; set; }
        public int? AgeAgeTipLoAgePaisCodigo { get; set; }
        public int? AgeLocaliAgeTipLoCodigo { get; set; }
        public int? AgeLocaliCodigo { get; set; }
        public int AgeTipDiCodigo { get; set; }
        public string Descripcion { get; set; } = null!;
        public string? Telefono1 { get; set; }
        public string? Telefono2 { get; set; }
        public string? NombreContacto { get; set; }
        public string? CorreoElectronicoContacto { get; set; }
        public string? CelularContacto { get; set; }
        public string? Latitud { get; set; }
        public string? Longitud { get; set; }
    }
}
