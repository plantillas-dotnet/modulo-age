﻿using AGE.Utils;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace AGE.Entities.DAO.AgeFeriadosPaise
{
    public class AgeFeriadosPaisesDAO
    {

        public AgeFeriadosPaisesPKDAO Id { get; set; }


        public DateTime FechaEjecucionDesde { get; set; }


        public DateTime FechaEjecucionHasta { get; set; }


        public DateTime? HoraEjecucionDesde { get; set; }


        public DateTime? HoraEjecucionHasta { get; set; }


        public string Estado { get; set; } = null!;

    }
}
