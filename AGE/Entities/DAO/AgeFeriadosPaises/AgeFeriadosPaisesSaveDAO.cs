﻿using AGE.Entities.DAO.AgeCamposGenerales;

namespace AGE.Entities.DAO.AgeFeriadosPaise
{
    public class AgeFeriadosPaisesSaveDAO : AgeCamposGeneralesDAO
    {

        public AgeFeriadosPaisesPKDAO Id { get; set; }


        public DateTime FechaEjecucionDesde { get; set; }


        public DateTime FechaEjecucionHasta { get; set; }


        public DateTime? HoraEjecucionDesde { get; set; }


        public DateTime? HoraEjecucionHasta { get; set; }

    }
}
