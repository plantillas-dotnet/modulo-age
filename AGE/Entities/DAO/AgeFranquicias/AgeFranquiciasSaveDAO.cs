﻿using AGE.Entities.DAO.AgeCamposGenerales;

namespace AGE.Entities.DAO.AgeFranquicias
{
    public class AgeFranquiciasSaveDAO : AgeCamposGeneralesDAO
    {
        public int Codigo { get; set; }

        public string Descripcion { get; set; } = null!;
    }
}
