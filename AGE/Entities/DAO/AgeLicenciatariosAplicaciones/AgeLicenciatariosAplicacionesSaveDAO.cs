﻿using AGE.Entities.DAO.AgeCamposGenerales;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace AGE.Entities.DAO.AgeLicenciatarioAplicacion
{
    public class AgeLicenciatariosAplicacionesSaveDAO : AgeCamposGeneralesDAO
    {
        public AgeLicenciatariosAplicacionesPKDAO Id { get; set; }

        public int? AgeRutaAgeLicencCodigo { get; set; }

        public int? AgeRutaCodigo { get; set; }

    }
}
