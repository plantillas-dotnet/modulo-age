﻿namespace AGE.Entities.DAO.AgeLicenciatarioAplicacion
{
    public class AgeLicenciatariosAplicacionesDAO
    {

        public AgeLicenciatariosAplicacionesPKDAO Id { get; set; }

        public int? AgeRutaAgeLicencCodigo { get; set; }

        public int? AgeRutaCodigo { get; set; }

        public string Estado { get; set; }

    }
}
