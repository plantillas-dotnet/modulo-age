﻿using AGE.Entities.DAO.AgeCamposGenerales;

namespace AGE.Entities.DAO.AgeFormasPagosInstFina
{
    public class AgeFormasPagosInstFinaSaveDAO : AgeCamposGeneralesDAO
    {

        public AgeFormasPagosInstFinaPKDAO Id { get; set; }

        public string Descripcion { get; set; } = null!;

        public int AgeForPaAgeLicencCodigo { get; set; }

        public int AgeForPaCodigo { get; set; }

        public int? AgeFranquCodigo { get; set; }

        public int? AgeInsFiAgeLicencCodigo { get; set; }

        public int? AgeInsFiCodigo { get; set; }

    }
}
