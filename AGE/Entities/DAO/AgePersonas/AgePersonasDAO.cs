﻿namespace AGE.Entities.DAO.AgePersonas
{
    public class AgePersonasDAO
    {
        public AgePersonasPKDAO Id { get; set; }

        public int AgeTipIdCodigo { get; set; }
        public int? AgeLocaliAgeTipLoCodigo { get; set; }
        public int? AgeLocaliCodigo { get; set; }
        public int? AgeAgeTipLoAgePaisCodigo { get; set; }

        public string NumeroIdentificacion { get; set; } = null!;
        public string Nombres { get; set; } = null!;
        public string Apellidos { get; set; } = null!;
        public string? RazonSocial { get; set; }
        public string? NombreComercial { get; set; }
        public string? TelefonoCelular1 { get; set; }
        public string? TelefonoCelular2 { get; set; }
        public DateTime? FechaNacimiento { get; set; }
        public string? DireccionPrincipal { get; set; }
        public string? CorreoElectronico { get; set; }
        public string? Sexo { get; set; }
        public string? EstadoCivil { get; set; }
        public string? TelefonoPrincipal { get; set; }
        public string Estado { get; set; }
    }
}
