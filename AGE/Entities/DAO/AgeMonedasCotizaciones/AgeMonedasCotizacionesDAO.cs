﻿namespace AGE.Entities.DAO.AgeMonedasCotizaciones
{
    public class AgeMonedasCotizacionesDAO
    {
        public string Estado { get; set; } = null!;
        public AgeMonedasCotizacionesPKDAO Id { get; set; }
        public int AgeMonedaCodigo { get; set; }
        public int AgeMonedaCodigoSerA { get; set; }
        public DateTime FechaDesde { get; set; }
        public DateTime FechaHasta { get; set; }
        public decimal CotizacionMercado { get; set; }
        public decimal CotizacionCompra { get; set; }
        public decimal CotizacionVenta { get; set; }
    }
}
