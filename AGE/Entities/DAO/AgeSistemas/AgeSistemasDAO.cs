﻿namespace AGE.Entities.DAO.AgeSistemas
{
    public class AgeSistemasDAO
    {
        public int Codigo { get; set; }

        public string Descripcion { get; set; } = null!;

        public string Estado { get; set; } = null!;
    }
}
