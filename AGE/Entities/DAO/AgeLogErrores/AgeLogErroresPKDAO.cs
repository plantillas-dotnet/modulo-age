﻿using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using System.Xml.Linq;

namespace AGE.Entities.DAO.AgeLogErrores
{
    public class AgeLogErroresPKDAO
    {
        [Required(ErrorMessage = "El código de licenciatario no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de licenciatario debe ser un número entero mayor a cero.")]
        [FromRoute(Name = "ageLicApAgeLicencCodigo")]
        public int AgeLicApAgeLicencCodigo { get; set; }

        [Required(ErrorMessage = "El código de la aplicación no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de la aplicación debe ser un número entero mayor a cero.")]
        [FromRoute(Name = "ageLicApAgeAplicaCodigo")]
        public int AgeLicApAgeAplicaCodigo { get; set; }

        [Required(ErrorMessage = "El código de log error no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de log error debe ser un número entero mayor a cero.")]
        [FromRoute(Name = "codigo")]
        public long Codigo { get; set; }
    }
}
