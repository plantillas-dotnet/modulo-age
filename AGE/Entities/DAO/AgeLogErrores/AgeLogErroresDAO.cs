﻿namespace AGE.Entities.DAO.AgeLogErrores
{
    public class AgeLogErroresDAO
    { 
        public string Estado { get; set; }  

        public AgeLogErroresPKDAO Id { get;set; }
        
        public string Mensaje { get; set; }

        public DateTime Fecha { get; set; }
    }
}
