﻿using AGE.Entities.DAO.AgeCamposGenerales;

namespace AGE.Entities.DAO.AgeLogErrores
{
    public class AgeLogErroresSaveDAO : AgeCamposGeneralesDAO
    {
        public AgeLogErroresPKDAO Id { get; set; }

        public string Mensaje { get; set; }

        public DateTime Fecha { get; set; }
    }
}
