﻿namespace AGE.Entities.DAO.AgeParametrosGenerales
{
    public class AgeParametrosGeneralesDAO
    {
        public int Codigo { get; set; }
        public string Descripcion { get; set; } = null!;
        public string TipoDatoParametro { get; set; } = null!;

        public string Estado { get; set; }
    }
}
