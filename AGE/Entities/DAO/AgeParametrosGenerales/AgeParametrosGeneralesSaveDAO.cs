﻿using AGE.Entities.DAO.AgeCamposGenerales;

namespace AGE.Entities.DAO.AgeParametrosGenerales
{
    public class AgeParametrosGeneralesSaveDAO : AgeCamposGeneralesDAO
    {
        public int Codigo { get; set; }
        public string Descripcion { get; set; } = null!;
        public string TipoDatoParametro { get; set; } = null!;
    }
}
