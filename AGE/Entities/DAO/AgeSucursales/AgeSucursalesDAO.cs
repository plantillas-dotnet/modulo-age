﻿using AGE.Entities.DAO.AgeDepartamentos;

namespace AGE.Entities.DAO.AgeSucursales
{
    public class AgeSucursalesDAO
    {
        public string Estado { get; set; }

        public AgeSucursalesPKDAO Id { get; set; }

        public string Descripcion { get; set; } = null!;

        public int AgeAgeTipLoAgePaisCodigo { get; set; }

        public int AgeLocaliAgeTipLoCodigo { get; set; }

        public int AgeLocaliCodigo { get; set; }

        public int AgeTipSuAgeLicencCodigo { get; set; }

        public int AgeTipSuCodigo { get; set; }

        public string Direccion { get; set; } = null!;

        public string Telefono1 { get; set; } = null!;

        public string? EMail1 { get; set; }

        public string? EMail2 { get; set; }

        public int? AgeSucursAgeLicencCodigo { get; set; }

        public int? AgeSucursCodigo { get; set; }

        public short? CodigoEstablecimiento { get; set; }

        public string? Telefono2 { get; set; }

        public string? Observacion { get; set; }

        public string? Latitud { get; set; }

        public string? Longitud { get; set; }

        public string? CentroAcopio { get; set; }
    }
}
