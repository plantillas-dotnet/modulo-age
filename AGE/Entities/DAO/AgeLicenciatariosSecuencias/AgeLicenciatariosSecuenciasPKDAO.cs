﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;
using System.Xml.Linq;

namespace AGE.Entities.DAO.AgeLicenciatariosSecuencias
{
    public class AgeLicenciatariosSecuenciasPKDAO
    {
        [Required(ErrorMessage = "El código de licenciatario no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de licenciatario debe ser un número entero mayor a cero.")]
        [FromRoute(Name = "codigoLicenciatario")]
        public int ageLicencCodigo { get; set; }

        [Required(ErrorMessage = "El código de secuencia no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de secuencia debe ser un número entero mayor a cero.")]
        [FromRoute(Name = "id")]
        public int codigo { get; set; }

    }
}
