﻿using AGE.Entities.DAO.AgeCamposGenerales;

namespace AGE.Entities.DAO.AgeLicenciatariosSecuencias
{
    public class AgeLicenciatariosSecuenciasSaveDAO : AgeCamposGeneralesDAO
    {
        public AgeLicenciatariosSecuenciasPKDAO Id { get; set; }
        public string Descripcion { get; set; } = null!;
        public long ValorInicial { get; set; }
        public int IncrementaEn { get; set; }
        public long ValorActual { get; set; }
        public string Ciclica { get; set; } = null!;
    }
}
