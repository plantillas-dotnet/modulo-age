﻿
namespace AGE.Entities.DAO.AgeLicenciatariosSecuencias
{
    public class AgeLicenciatariosSecuenciasDAO
    {
        public AgeLicenciatariosSecuenciasPKDAO Id { get; set; }
        public string Descripcion { get; set; } = null!;
        public long ValorInicial { get; set; }
        public int IncrementaEn { get; set; }
        public long ValorActual { get; set; }
        public string Ciclica { get; set; } = null!;
        public string Estado { get; set; } = null!;
    }
}
