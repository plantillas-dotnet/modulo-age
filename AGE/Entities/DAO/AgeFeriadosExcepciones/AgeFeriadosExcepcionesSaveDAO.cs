﻿using AGE.Entities.DAO.AgeCamposGenerales;

namespace AGE.Entities.DAO.AgeFeriadosExcepciones
{
    public class AgeFeriadosExcepcionesSaveDAO : AgeCamposGeneralesDAO
    {
        public AgeFeriadosExcepcionesPKDAO Id { get; set; }

        public string? Observacion { get; set; }

        public DateTime FechaDesde { get; set; }

        public DateTime FechaHasta { get; set; }

        public DateTime? HoraDesde { get; set; }

        public DateTime? HoraHasta { get; set; }

        public string TipoExcepcion { get; set; } = null!;
    }
}
