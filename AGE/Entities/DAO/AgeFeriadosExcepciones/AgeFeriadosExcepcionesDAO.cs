﻿using AGE.Entities.DAO.AgeFirmasDigitales;

namespace AGE.Entities.DAO.AgeFeriadosExcepciones
{
    public class AgeFeriadosExcepcionesDAO
    {
        public string Estado { get; set; }

        public AgeFeriadosExcepcionesPKDAO Id { get; set; }

        public string? Observacion { get; set; }

        public DateTime FechaDesde { get; set; }

        public DateTime FechaHasta { get; set; }

        public DateTime? HoraDesde { get; set; }

        public DateTime? HoraHasta { get; set; }

        public string TipoExcepcion { get; set; } = null!;
    }
}
