﻿using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace AGE.Entities.DAO.AgeFeriadosExcepciones
{
    public class AgeFeriadosExcepcionesPKDAO
    {
        [FromRoute(Name = "codigoLicenciatario")]
        [Required(ErrorMessage = "El código de licenciatario no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de licenciatario debe ser un número entero mayor a cero.")]
        public int AgeLicencCodigo { get; set; }

        [FromRoute(Name = "id")]
        [Required(ErrorMessage = "El código de la excepción del feriado no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de la excepción del feriado debe ser un número entero mayor a cero.")]
        public int Codigo { get; set; }
    }
}
