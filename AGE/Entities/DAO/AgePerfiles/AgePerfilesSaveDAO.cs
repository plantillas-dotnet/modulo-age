﻿using AGE.Entities.DAO.AgeCamposGenerales;

namespace AGE.Entities.DAO.AgePerfiles
{
    public class AgePerfilesSaveDAO : AgeCamposGeneralesDAO
    {
        public AgePerfilesPKDAO Id { get; set; }

        public string Descripcion { get; set; }

    }
}
