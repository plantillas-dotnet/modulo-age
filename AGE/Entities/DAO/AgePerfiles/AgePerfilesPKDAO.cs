﻿
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace AGE.Entities.DAO.AgePerfiles
{
    public class AgePerfilesPKDAO
    {
        [Required(ErrorMessage = "El código de licenciatario no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de licenciatario debe ser un número entero mayor a cero.")]
        [FromRoute(Name = "codigoLicenciatario")]
        public int ageLicencCodigo { get; set; }

        [Required(ErrorMessage = "El código del perfil no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código del perfil debe ser un número entero mayor a cero.")]
        [FromRoute(Name = "id")]
        public int codigo { get; set; }
    }
}
