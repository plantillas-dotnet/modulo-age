﻿using AGE.Entities.DAO.AgeCamposGenerales;

namespace AGE.Entities.DAO.AgePerfiles
{
    public class AgePerfilesDAO
    {
        public AgePerfilesPKDAO Id { get; set; }

        public string Descripcion { get; set; }

        public string Estado { get; set; }
    }
}
