﻿using AGE.Entities.DAO.AgeCamposGenerales;

namespace AGE.Entities.DAO.AgeAplicaciones
{
    public class AgeAplicacionesSaveDAO : AgeCamposGeneralesDAO
    {
        public int Codigo { get; set; }

        public string CodigoExterno { get; set; } = null!;

        public string Descripcion { get; set; } = null!;

        public long InicioSecuTrxCe { get; set; }

        public int IncrementoSecuTrxCe { get; set; }

        public long ValorActualSecuTrxCe { get; set; }

        public string CiclicaSecuTrxCe { get; set; } = null!;

        public long InicioSecuTrxCi { get; set; }

        public long ValorActualSecuTrxCi { get; set; }

        public string CiclicaSecuTrxCi { get; set; } = null!;

        public short OrdenInstalacion { get; set; }
    }
}
