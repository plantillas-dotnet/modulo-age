﻿using AGE.Utils;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace AGE.Entities.DAO.AgeAplicaciones
{
    public class AgeAplicacionesDAO
    {
        public string Estado { get;set; }

        public int Codigo { get; set; }

        public string CodigoExterno { get; set; } = null!;

        public string Descripcion { get; set; } = null!;

        public long InicioSecuTrxCe { get; set; }

        public int IncrementoSecuTrxCe { get; set; }

        public long ValorActualSecuTrxCe { get; set; }

        public string CiclicaSecuTrxCe { get; set; } = null!;

        public long InicioSecuTrxCi { get; set; }

        public long ValorActualSecuTrxCi { get; set; }

        public string CiclicaSecuTrxCi { get; set; } = null!;

        public short OrdenInstalacion { get; set; }
    }
}
