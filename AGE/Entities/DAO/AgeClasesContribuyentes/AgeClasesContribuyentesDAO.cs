﻿namespace AGE.Entities.DAO.AgeClasesContribuyentes
{
    public class AgeClasesContribuyentesDAO
    {
        public string Estado { get; set; }

        public long Codigo { get; set; }

        public string Descripcion { get; set; } = null!;
    }
}
