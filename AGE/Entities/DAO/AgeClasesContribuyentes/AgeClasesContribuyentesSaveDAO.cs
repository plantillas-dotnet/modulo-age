﻿using AGE.Entities.DAO.AgeCamposGenerales;

namespace AGE.Entities.DAO.AgeClasesContribuyentes
{
    public class AgeClasesContribuyentesSaveDAO : AgeCamposGeneralesDAO
    {
        public long Codigo { get; set; }

        public string Descripcion { get; set; } = null!;
    }
}
