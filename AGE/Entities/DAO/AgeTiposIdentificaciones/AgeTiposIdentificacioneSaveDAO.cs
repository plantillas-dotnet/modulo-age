﻿using AGE.Entities.DAO.AgeCamposGenerales;

namespace AGE.Entities.DAO.AgeTiposIdentificaciones
{
    public class AgeTiposIdentificacioneSaveDAO : AgeCamposGeneralesDAO
    {

        public int Codigo { get; set; }

        public int CodigoInstitucionControl { get; set; }

        public string Siglas { get; set; } = null!;

        public string Descripcion { get; set; } = null!;


    }
}
