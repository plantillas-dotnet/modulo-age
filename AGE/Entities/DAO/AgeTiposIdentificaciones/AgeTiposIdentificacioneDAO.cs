﻿
namespace AGE.Entities.DAO.AgeTiposIdentificaciones
{
    public class AgeTiposIdentificacioneDAO 
    {
        public int Codigo { get; set; }

        public int CodigoInstitucionControl { get; set; }

        public string Siglas { get; set; } = null!;

        public string Descripcion { get; set; } = null!;

        public string Estado { get; set; } = null!;

    }
}
