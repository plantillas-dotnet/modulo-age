﻿using AGE.Entities.DAO.AgeUsuariosPuntoEmision;

namespace AGE.Entities.DAO.AgeUsuariosPuntoEmision
{
    public class AgeUsuariosPuntoEmisionDAO
    {
        public string Estado { get; set; }

        public AgeUsuariosPuntoEmisionPKDAO Id { get; set; }

        public DateTime FechaDesde { get; set; }

        public DateTime? FechaHasta { get; set; }
    }
}
