﻿using AGE.Entities.DAO.AgeCamposGenerales;

namespace AGE.Entities.DAO.AgeUsuariosPuntoEmision
{
    public class AgeUsuariosPuntoEmisionSaveDAO : AgeCamposGeneralesDAO
    {
        public AgeUsuariosPuntoEmisionPKDAO Id { get; set; }

        public DateTime FechaDesde { get; set; }

        public DateTime? FechaHasta { get; set; }
    }
}
