﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;

namespace AGE.Entities.DAO.AgeUsuariosPuntoEmision
{
    public class AgeUsuariosPuntoEmisionPKDAO
    {

        [Required(ErrorMessage = "El código de licenciatario no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de licenciatario debe ser un número entero mayor a cero.")]
        //[FromRoute(Name = "codigoLicenciatario")]
        public int AgageSucursAgeLicencCodigo { get; set; }

        [Required(ErrorMessage = "El código de la sucursal no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de la sucursal debe ser un número entero mayor a cero.")]
        //[FromRoute(Name = "codigoSucursal")]
        public int AgePunEmAgeSucursCodigo { get; set; }

        [Required(ErrorMessage = "El código del punto de emisión no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código del punto de emisión debe ser un número entero mayor a cero.")]
        //[FromRoute(Name = "codigoPuntoEmision")]
        public int AgePunEmCodigo { get; set; }

        [Required(ErrorMessage = "El código de licenciatario no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de licenciatario debe ser un número entero mayor a cero.")]
        //[FromRoute(Name = "codigoLicenciatario")]
        public int AgeUsuariAgeLicencCodigo { get; set; }

        [Required(ErrorMessage = "El código de usuario no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de usuario debe ser un número entero mayor a cero.")]
        //[FromRoute(Name = "codigoUsuario")]
        public long AgeUsuariCodigo { get; set; }
    }
}
