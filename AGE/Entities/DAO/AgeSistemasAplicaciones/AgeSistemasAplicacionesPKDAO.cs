﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;

namespace AGE.Entities.DAO.AgeSistemasAplicaciones
{
    public class AgeSistemasAplicacionesPKDAO
    {
        [FromRoute(Name = "codigoAplicacion")]
        [Required(ErrorMessage = "El código de la aplicación no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de la aplicación debe ser un número entero mayor a cero.")]
        public int AgeAplicaCodigo { get; set; }

        [FromRoute(Name = "codigoSistema")]
        [Required(ErrorMessage = "El código del sistema no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código del sistema debe ser un número entero mayor a cero.")]
        public int AgeSistemCodigo { get; set; }
    }
}
