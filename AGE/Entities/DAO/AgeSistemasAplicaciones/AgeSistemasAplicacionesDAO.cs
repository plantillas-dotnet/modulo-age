﻿using AGE.Entities.DAO.AgeUsuariosPerfiles;

namespace AGE.Entities.DAO.AgeSistemasAplicaciones
{
    public class AgeSistemasAplicacionesDAO
    {
        public string Estado { get; set; }

        public AgeSistemasAplicacionesPKDAO Id { get; set; }

        public DateTime FechaDesde { get; set; }

        public DateTime? FechaHasta { get; set; }
    }
}
