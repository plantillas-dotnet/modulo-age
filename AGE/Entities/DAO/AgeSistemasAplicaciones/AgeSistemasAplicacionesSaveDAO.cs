﻿using AGE.Entities.DAO.AgeCamposGenerales;

namespace AGE.Entities.DAO.AgeSistemasAplicaciones
{
    public class AgeSistemasAplicacionesSaveDAO : AgeCamposGeneralesDAO
    {
        public AgeSistemasAplicacionesPKDAO Id { get; set; }

        public DateTime FechaDesde { get; set; }

        public DateTime? FechaHasta { get; set; }
    }
}
