﻿using AGE.Entities.DAO.AgeCamposGenerales;

namespace AGE.Entities.DAO.AgeModalidades
{
    public class AgeModalidadesSaveDAO : AgeCamposGeneralesDAO
    {
        public AgeModalidadesPKDAO Id { get; set; }

        public string Descripcion { get; set; }

        public string codigoExterno { get; set; }
    }
}
