﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;
using System.Xml.Linq;

namespace AGE.Entities.DAO.AgeModalidades
{
    public class AgeModalidadesPKDAO
    {
        [FromRoute(Name = "codigoLicenciatario")]
        [Required(ErrorMessage = "El código licenciatario de cabeceras asientos no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código licenciatario de cabeceras debe ser un número entero mayor a cero.")]
        public int AgeLicencCodigo { get; set; }

        [FromRoute(Name = "id")]
        [Required(ErrorMessage = "El código de colaborador no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de colaborador debe ser un número entero mayor a cero.")]
        public long Codigo { get; set; }
    }
}
