﻿using AGE.Entities.DAO.AgeUnidadesTiempos;

namespace AGE.Entities.DAO.AgeModalidades
{
    public class AgeModalidadesDAO
    {
        public string Estado { get; set; }

        public AgeModalidadesPKDAO Id { get; set; }

        public string Descripcion { get; set; }

        public string codigoExterno { get; set; }
    }
}
