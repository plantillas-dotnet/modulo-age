﻿namespace AGE.Entities.DAO.AgePuntosEmision
{
    public class AgePuntosEmisionesDAO
    {
        public AgePuntosEmisionesPKDAO Id { get; set; }

        public string Descripcion { get; set; } = null!;

        public string Estado { get; set; } = null!;
    }
}
