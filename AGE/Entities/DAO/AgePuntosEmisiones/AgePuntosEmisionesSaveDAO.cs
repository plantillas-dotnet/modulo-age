﻿using AGE.Entities.DAO.AgeCamposGenerales;

namespace AGE.Entities.DAO.AgePuntosEmision
{
    public class AgePuntosEmisionesSaveDAO : AgeCamposGeneralesDAO
    {
        public AgePuntosEmisionesPKDAO Id { get; set; }
        public string Descripcion { get; set; } = null!;
    }
}
