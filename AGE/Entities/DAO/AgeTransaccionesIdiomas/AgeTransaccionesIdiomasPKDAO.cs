﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;

namespace AGE.Entities.DAO.AgeTransaccionesIdioma
{
    public class AgeTransaccionesIdiomasPKDAO
    {
        [Required(ErrorMessage = "El código de idioma no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de idioma debe ser un número entero mayor a cero.")]
        [FromRoute(Name ="codigoIdioma")]
        public int ageIdiomaCodigo { get; set; }

        [Required(ErrorMessage = "El código de aplicación no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de aplicación debe ser un número entero mayor a cero.")]
        [FromRoute(Name = "codigoAplicacion")]
        public int ageTransaAgeAplicaCodigo { get; set; }

        [Required(ErrorMessage = "El código de transacción no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código debe ser un número entero mayor a cero.")]
        [FromRoute(Name = "codigoTransaccion")] 
        public int ageTransaCodigo { get; set; }
    }
}
