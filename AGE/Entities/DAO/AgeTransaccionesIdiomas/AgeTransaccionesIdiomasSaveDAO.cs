﻿using AGE.Entities.DAO.AgeCamposGenerales;

namespace AGE.Entities.DAO.AgeTransaccionesIdioma
{
    public class AgeTransaccionesIdiomasSaveDAO : AgeCamposGeneralesDAO
    {
        public AgeTransaccionesIdiomasPKDAO Id { get; set; }
        public string Descripcion { get; set; } = null!;
    }
}
