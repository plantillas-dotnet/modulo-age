﻿namespace AGE.Entities.DAO.AgeTransaccionesIdioma
{
    public class AgeTransaccionesIdiomasDAO
    {
        public AgeTransaccionesIdiomasPKDAO Id { get; set; } 

        public string Descripcion { get; set; } = null!;

        public string Estado { get; set; } = null!;
    }
}
