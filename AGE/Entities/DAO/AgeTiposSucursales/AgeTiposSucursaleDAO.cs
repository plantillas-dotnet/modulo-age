﻿namespace AGE.Entities.DAO.AgeTiposSucursales
{
    public class AgeTiposSucursaleDAO
    {
        public AgeTiposSucursalePKDAO Id { get; set; }
        public string Descripcion { get; set; }
        public string Estado { get; set; }
    }
}
