﻿using AGE.Entities.DAO.AgeCamposGenerales;

namespace AGE.Entities.DAO.AgeTiposSucursales
{
    public class AgeTiposSucursaleSaveDAO : AgeCamposGeneralesDAO
    {
        public AgeTiposSucursalePKDAO Id { get; set; }
        public string Descripcion { get; set; }
    }
}
