﻿namespace AGE.Entities.DAO.AgeFavoritos
{
    public class AgeFavoritosDAO
    {
        public string Estado { get; set; }

        public AgeFavoritosPKDAO Id { get; set; }

        public DateTime FechaDesde { get; set; }

        public DateTime? FechaHasta { get; set; }
    }
}
