﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;

namespace AGE.Entities.DAO.AgeFavoritos
{
    public class AgeFavoritosPKDAO
    {
        [FromRoute(Name = "codigoAplicacion")]
        [Required(ErrorMessage = "El código de aplicación no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de aplicación debe ser un número entero mayor a cero.")]
        public int AgeTransaAgeAplicaCodigo { get; set; }

        [FromRoute(Name = "codigoTransaccion")]
        [Required(ErrorMessage = "El código de la transacción no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de la transacción debe ser un número entero mayor a cero.")]
        public int AgeTransaCodigo { get; set; }

        [FromRoute(Name = "codigoLicenciatario")]
        [Required(ErrorMessage = "El código de licenciatario no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de licenciatario debe ser un número entero mayor a cero.")]
        public int AgeUsuariAgeLicencCodigo { get; set; }

        [FromRoute(Name = "codigoUsuario")]
        [Required(ErrorMessage = "El código del usuario no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código del usuario debe ser un número entero mayor a cero.")]
        public long AgeUsuariCodigo { get; set; }

        [FromRoute(Name = "id")]
        [Required(ErrorMessage = "El código del favorito no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código del favorito debe ser un número entero mayor a cero.")]
        public int Codigo { get; set; }
    }
}
