﻿using AGE.Entities.DAO.AgeCamposGenerales;

namespace AGE.Entities.DAO.AgeFavoritos
{
    public class AgeFavoritosSaveDAO : AgeCamposGeneralesDAO
    {
        public AgeFavoritosPKDAO Id { get; set; }

        public DateTime FechaDesde { get; set; }

        public DateTime? FechaHasta { get; set; }
    }
}
