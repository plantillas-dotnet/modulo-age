﻿using AGE.Entities.DAO.AgeArchivosMultimedia;
using AGE.Entities.DAO.AgeCamposGenerales;

namespace AGE.Entities.DAO.AgeLicenciatarios
{
    public class AgeLicenciatarioSaveDAO : AgeCamposGeneralesDAO
    {
        public int Codigo { get; set; }
        public long AgeClaCoCodigo { get; set; }
        public int AgeTipIdCodigo { get; set; }
        public string NombreCorto { get; set; } = null!;
        public string RepresentanteLegal { get; set; } = null!;
        public string DireccionPrincipal { get; set; } = null!;
        public string Telefono1 { get; set; } = null!;
        public string? EMail2 { get; set; }
        public string? EsCorporacion { get; set; }
        public string? Observacion { get; set; }
        public string? PaginaWeb { get; set; }
        public string? ContribuyenteEspecialResoluc { get; set; }
        public string RutaLicenciatario { get; set; } = null!;
        public int? AgeLicencCodigo { get; set; }
        public string? RazonSocial { get; set; }
        public string NombreComercial { get; set; } = null!;
        public string NumeroIdentificacion { get; set; } = null!;
        public string? Telefono2 { get; set; }
        public string? EMail1 { get; set; }
        public string ObligadoLlevarContabilidad { get; set; } = null!;
        public string? Descripcion { get; set; }
    }
}
