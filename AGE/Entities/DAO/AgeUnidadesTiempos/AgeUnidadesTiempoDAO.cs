﻿using System.ComponentModel.DataAnnotations;

namespace AGE.Entities.DAO.AgeUnidadesTiempos
{
    public class AgeUnidadesTiempoDAO
    {
        public AgeUnidadesTiempoPKDAO Id { get; set; }

        public string Descripcion { get; set; }

        public string Estado { get; set; }

    }
}
