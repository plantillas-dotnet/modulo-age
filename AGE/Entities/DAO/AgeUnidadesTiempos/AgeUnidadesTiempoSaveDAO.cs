﻿using AGE.Entities.DAO.AgeCamposGenerales;

namespace AGE.Entities.DAO.AgeUnidadesTiempos
{
    public class AgeUnidadesTiempoSaveDAO : AgeCamposGeneralesDAO
    {
        public AgeUnidadesTiempoPKDAO Id { get; set; }

        public string Descripcion { get; set; }
    }
}
