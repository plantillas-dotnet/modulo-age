﻿using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using System.Xml.Linq;

namespace AGE.Entities.DAO.AgeUnidadesTiempos
{
    public class AgeUnidadesTiempoPKDAO
    {
        [Required(ErrorMessage = "El código de licenciatario no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de licenciatario debe ser un número entero mayor a cero.")]
        [FromRoute(Name = "codigoLicenciatario")]
        public int ageLicencCodigo { get; set; }

        [Required(ErrorMessage = "El código de unidad tiempo no puede ser nulo.")]
        [FromRoute(Name = "id")]
        public long codigo { get; set; }
    }
}
