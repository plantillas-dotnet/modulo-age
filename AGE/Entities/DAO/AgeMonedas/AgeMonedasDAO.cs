﻿namespace AGE.Entities.DAO.AgeMonedas
{
    public class AgeMonedasDAO
    {
        public int Codigo { get; set; }
        public string CodigoExterno { get; set; } = null!;
        public string Descripcion { get; set; } = null!;
        public string MonedaSimbolo { get; set; } = null!;
        public string MonedaSeparadorDecimal { get; set; } = null!;
        public string Estado { get; set; } = null!;
    }
}
