﻿using AGE.Entities.DAO.AgeCamposGenerales;

namespace AGE.Entities.DAO.AgeMonedas
{
    public class AgeMonedasSaveDAO : AgeCamposGeneralesDAO
    {
        public int Codigo { get; set; }
        public string CodigoExterno { get; set; } = null!;
        public string Descripcion { get; set; } = null!;
        public string MonedaSimbolo { get; set; } = null!;
        public string MonedaSeparadorDecimal { get; set; } = null!;
    }
}
