﻿
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;
namespace AGE.Entities.DAO.AgeUsuariosPerfiles
{
    public class AgeUsuariosPerfilePKDAO
    {

        [Required(ErrorMessage = "El código del licenciatario no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código del licenciatario debe ser un número entero mayor a cero.")]
        [FromRoute(Name = "codigoLicenciatario")]
        public int AgeUsuariAgeLicencCodigo { get; set; }

        [Required(ErrorMessage = "El código del usuario no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código del usuario perfil debe ser mayor a 0.")]
        [FromRoute(Name = "codigoUsuario")]
        public long AgeUsuariCodigo { get; set; }

        [Required(ErrorMessage = "El código del usuario perfil no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código del usuario perfil debe ser mayor a 0.")]
        [FromRoute(Name = "id")]
        public int Codigo { get; set; }

    }
}
