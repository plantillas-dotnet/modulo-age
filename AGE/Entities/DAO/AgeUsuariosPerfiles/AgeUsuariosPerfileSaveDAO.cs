﻿using AGE.Entities.DAO.AgeCamposGenerales;
using AGE.Entities.DAO.AgeUsuarios;

namespace AGE.Entities.DAO.AgeUsuariosPerfiles
{
    public class AgeUsuariosPerfileSaveDAO : AgeCamposGeneralesDAO
    {
        public AgeUsuariosPerfilePKDAO Id { get; set; }

        public int AgePerfilAgeLicencCodigo { get; set; }

        public int AgePerfilCodigo { get; set; }

        public DateTime FechaDesde { get; set; }

        public DateTime? FechaHasta { get; set; }

    }
}
