﻿

using AGE.Entities.DAO.AgeUsuarios;

namespace AGE.Entities.DAO.AgeUsuariosPerfiles
{
    public class AgeUsuariosPerfileDAO
    {
        public string Estado { get; set; }

        public AgeUsuariosPerfilePKDAO Id { get; set; }

        public int AgePerfilAgeLicencCodigo { get; set; }

        public int AgePerfilCodigo { get; set; }

        public DateTime FechaDesde { get; set; }

        public DateTime? FechaHasta { get; set; }

        public AgeUsuarioDAO? AgeUsuarios { get; set; }
    }
}