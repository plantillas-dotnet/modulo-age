﻿using AGE.Entities.DAO.AgeCamposGenerales;

namespace AGE.Entities.DAO.AgeNotificacionesPush
{
    public class AgeNotificacionesPushSaveDAO : AgeCamposGeneralesDAO
    {

        public AgeNotificacionesPushPKDAO Id { get; set; }

        public string Titulo { get; set; } = null!;

        public string Mensaje { get; set; } = null!;

        public string? Topico { get; set; }

        public string? Data { get; set; }

        public string? IdFirebaseUser { get; set; }

        public string? Imagen { get; set; }

        public int? AgeUsuariAgeLicencCodigo { get; set; }

        public long? AgeUsuariCodigo { get; set; }

    }
}
