﻿using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using System.Xml.Linq;

namespace AGE.Entities.DAO.AgeNotificacionesPush
{
    public class AgeNotificacionesPushPKDAO
    {

        [Required(ErrorMessage = "El código de licenciatario no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de licenciatario debe ser un número entero mayor a cero.")]
        [FromRoute(Name = "codigoLicenciatario")]
        public int AgeLicencCodigo { get; set; }

        [Required(ErrorMessage = "El código del perfil no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código del perfil debe ser un número entero mayor a cero.")]
        [FromRoute(Name = "codigo")]
        public long Codigo { get; set; }

    }
}
