﻿
namespace AGE.Entities.DAO.AgeSecuenciasPrimarias
{
    public class AgeSecuenciasPrimariaDAO
    {
        public string Estado { get; set; }
        public int Codigo { get; set; }
        public string Ciclica { get; set; }
        public string Descripcion { get; set; }
        public int IncrementaEn { get; set; }
        public int ValorActual { get; set; }
        public int ValorInicial { get; set; }

    }

}
