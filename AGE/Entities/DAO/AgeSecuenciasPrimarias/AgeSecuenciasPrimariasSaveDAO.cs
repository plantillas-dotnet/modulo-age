﻿using AGE.Entities.DAO.AgeCamposGenerales;

namespace AGE.Entities.DAO.AgeSecuenciasPrimarias
{
    public class AgeSecuenciasPrimariasSaveDAO : AgeCamposGeneralesDAO
    {
        public int Codigo { get; set; }
        
        public string Descripcion { get; set; } = null!;

        public int ValorInicial { get; set; }

        public int IncrementaEn { get; set; }

        public int ValorActual { get; set; }
        
        public string Ciclica { get; set; } = null!;
        
    }
}
