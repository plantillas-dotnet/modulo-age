﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;

namespace AGE.Entities.DAO.AgeAplicacionesDependencias
{
    public class AgeAplicacionesDependenciasPKDAO
    {
        [FromRoute(Name = "codigoAplicacionDe")]
        [Required(ErrorMessage = "El código de la aplicación no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de la aplicación debe ser un número entero mayor a cero.")]
        public int AgeAplicaCodigo { get; set; }

        [FromRoute(Name = "codigoAplicacionSer")]
        [Required(ErrorMessage = "El código de la aplicación ser no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de la aplicación ser debe ser un número entero mayor a cero.")]
        public int AgeAplicaCodigoSer { get; set; }

        [FromRoute(Name = "orden")]
        [Required(ErrorMessage = "El orden de dependencia no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El orden de dependencia debe ser un número entero mayor a cero.")]
        public long OrdenDependencia { get; set; }
    }
}
