﻿using AGE.Entities.DAO.AgeUsuariosPerfiles;

namespace AGE.Entities.DAO.AgeAplicacionesDependencias
{
    public class AgeAplicacionesDependenciasDAO
    {
        public string Estado { get; set; }

        public AgeAplicacionesDependenciasPKDAO Id { get; set; }
    }
}
