﻿using AGE.Entities.DAO.AgeCamposGenerales;

namespace AGE.Entities.DAO.AgeAplicacionesDependencias
{
    public class AgeAplicacionesDependenciasSaveDAO : AgeCamposGeneralesDAO
    {
        public AgeAplicacionesDependenciasPKDAO Id { get; set; }
    }
}
