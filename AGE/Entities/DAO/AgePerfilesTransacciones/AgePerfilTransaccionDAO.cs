﻿using AGE.Entities.DAO.AgePerfiles;
using AGE.Entities.DAO.AgeTransacciones;

namespace AGE.Entities.DAO.AgePerfilesTransacciones
{
    public class AgePerfilTransaccionDAO
    {
        public AgePerfilesDAO AgePerfiles { get; set; }

        public List<AgeTransaccioneDAO> AgeTransaccionesList { get; set; }

    }
}
