﻿using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using System.Xml.Linq;

namespace AGE.Entities.DAO.AgePerfilesTransacciones
{
    public class AgePerfilesTransaccionePKDAO
    {
        [Required(ErrorMessage = "El código licenciatario no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de licenciatario debe ser un número entero mayor a cero.")]
        [FromRoute(Name = "codigoLicenciatario")]
        public int agePerfilAgeLicencCodigo { get; set; }

        [Required(ErrorMessage = "El código de perfil no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de perfil debe ser un número entero mayor a cero.")]
        [FromRoute(Name = "codigoPerfil")]
        public int agePerfilCodigo { get; set; }

        [Required(ErrorMessage = "El código no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código debe ser un número entero mayor a cero.")]
        [FromRoute(Name = "id")]
        public long codigo { get; set; }
    }
}
