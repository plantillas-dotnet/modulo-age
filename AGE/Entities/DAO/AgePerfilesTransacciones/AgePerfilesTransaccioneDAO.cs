﻿using AGE.Entities.DAO.AgePerfiles;
using AGE.Entities.DAO.AgeTransacciones;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace AGE.Entities.DAO.AgePerfilesTransacciones
{
    public class AgePerfilesTransaccioneDAO
    {
        public AgePerfilesTransaccionePKDAO Id { get; set; }
        public int AgeTransaAgeAplicaCodigo { get; set; }
        public int AgeTransaCodigo { get; set; }
        public string Estado { get; set; }
        public AgePerfilesDAO AgePerfiles { get; set; } = null!;
        public AgeTransaccioneDAO AgeTransacciones { get; set; } = null!;
    }
}
