﻿using AGE.Entities.DAO.AgeCamposGenerales;

namespace AGE.Entities.DAO.AgePerfilesTransacciones
{
    public class AgePerfilesTransaccioneSaveDAO : AgeCamposGeneralesDAO
    {
        public AgePerfilesTransaccionePKDAO Id { get; set; }

        public int AgeTransaAgeAplicaCodigo { get; set; }

        public int AgeTransaCodigo { get; set; }
    }
}
