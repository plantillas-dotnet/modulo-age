﻿using AGE.Entities.DAO.AgeCamposGenerales;

namespace AGE.Entities.DAO.AgeUsuarios
{
    public class AgeUsuarioSaveDAO : AgeCamposGeneralesDAO
    {
        public AgeUsuarioPKDAO Id { get; set; }

        public int? AgePersonAgeLicencCodigo { get; set; }

        public long? AgePersonCodigo { get; set; }

        public int AgeSucursAgeLicencCodigo { get; set; }

        public int AgeSucursCodigo { get; set; }

        public int AgeTipIdCodigo { get; set; }

        public string? ArchivoFoto { get; set; }

        public string CodigoExterno { get; set; } = null!;

        public string Clave { get; set; } = null!;

        public string NumeroIdentificacion { get; set; } = null!;

        public string Nombres { get; set; } = null!;

        public string Apellidos { get; set; } = null!;

        public string MailPrincipal { get; set; } = null!;

        public string TelefonoCelular { get; set; } = null!;

        public string TipoUsuario { get; set; } = null!;

        public string PrimerIngreso { get; set; } = null!;

        public string? TipoRegistro { get; set; }

        public string? Direccion { get; set; } = null!;

        public int? AgeLocaliCodigo { get; set; }

        public int? AgeLocaliAgeTipLoCodigo { get; set; }

        public int? AgeAgeTipLoAgePaisCodigo { get; set; }

        public string? TokenFirebase { get; set; }
    }
}
