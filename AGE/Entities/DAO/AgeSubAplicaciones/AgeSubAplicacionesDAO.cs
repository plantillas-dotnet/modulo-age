﻿

namespace AGE.Entities.DAO.AgeSubAplicaciones
{
    public class AgeSubAplicacionesDAO
    {
        public string Estado { get; set; }

        public AgeSubAplicacionesPKDAO Id { get; set; }

        public string Descripcion { get; set; } = null!;

        public string Contabiliza { get; set; } = null!;

        public int? AgageLicApAgeAplicaCodigo { get; set; }

        public int? AgageLicApAgeLicencCodigo { get; set; }

        public int? AgeSubApCodigo { get; set; }
    }
}
