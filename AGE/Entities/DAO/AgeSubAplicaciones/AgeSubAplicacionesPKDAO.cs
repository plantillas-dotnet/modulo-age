﻿using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Xml.Linq;

namespace AGE.Entities.DAO.AgeSubAplicaciones
{
    public class AgeSubAplicacionesPKDAO
    {
        [FromRoute(Name = "codigoAplicacion")]
        [Required(ErrorMessage = "El código de la aplicación no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de la aplicación debe ser un número entero mayor a cero.")]
        public int AgeLicApAgeAplicaCodigo { get; set; }

        [FromRoute(Name = "codigoLicenciatario")]
        [Required(ErrorMessage = "El código de licenciatario no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de licenciatario debe ser un número entero mayor a cero.")]
        public int AgeLicApAgeLicencCodigo { get; set; }

        [FromRoute(Name = "id")]
        [Required(ErrorMessage = "El código de la subaplicación no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de la subaplicación debe ser un número entero mayor a cero.")]
        public int Codigo { get; set; }
    }
}
