﻿using AGE.Entities.DAO.AgeCamposGenerales;

namespace AGE.Entities.DAO.AgeSubAplicaciones
{
    public class AgeSubAplicacionesSaveDAO : AgeCamposGeneralesDAO
    {

        public AgeSubAplicacionesPKDAO Id { get; set; }

        public string Descripcion { get; set; } = null!;

        public string Contabiliza { get; set; } = null!;

        public int? AgageLicApAgeAplicaCodigo { get; set; }

        public int? AgageLicApAgeLicencCodigo { get; set; }

        public int? AgeSubApCodigo { get; set; }
    }
}
