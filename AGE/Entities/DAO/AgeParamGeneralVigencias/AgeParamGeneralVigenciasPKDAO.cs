﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace AGE.Entities.DAO.AgeParamGeneralVigencias
{
    public class AgeParamGeneralVigenciasPKDAO
    {
        public int AgeParGeCodigo { get; set; }
        public long Codigo { get; set; }
    }
}
