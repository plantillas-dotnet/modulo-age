﻿using AGE.Entities.DAO.AgeCamposGenerales;

namespace AGE.Entities.DAO.AgeParamGeneralVigencias
{
    public class AgeParamGeneralVigenciasSaveDAO : AgeCamposGeneralesDAO
    {
        public AgeParamGeneralVigenciasPKDAO Id { get; set; }
        public string? Observacion { get; set; }
        public DateTime FechaDesde { get; set; }
        public DateTime? FechaHasta { get; set; }
        public string ValorParametro { get; set; } = null!;
    }
}
