﻿namespace AGE.Entities.DAO.AgeParamGeneralVigencias
{
    public class AgeParamGeneralVigenciasDAO
    {
        public AgeParamGeneralVigenciasPKDAO Id { get; set; }
        public string? Observacion { get; set; }
        public DateTime FechaDesde { get; set; }
        public DateTime? FechaHasta { get; set; }
        public string ValorParametro { get; set; } = null!;
        public string Estado { get; set; } = null!;
    }
}
