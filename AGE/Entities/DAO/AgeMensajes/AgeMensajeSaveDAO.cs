﻿using AGE.Entities.DAO.AgeCamposGenerales;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace AGE.Entities.DAO.AgeMensajes
{
    public class AgeMensajeSaveDAO : AgeCamposGeneralesDAO
    { 
        public long Codigo { get; set; }

        public string AccionMsg { get; set; } = null!;
    }
}
