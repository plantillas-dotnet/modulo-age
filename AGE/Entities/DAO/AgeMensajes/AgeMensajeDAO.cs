﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace AGE.Entities.DAO.AgeMensajes
{
    public class AgeMensajeDAO
    {
        public string Estado { get; set; } = null!;

        public long Codigo { get; set; }

        public string AccionMsg { get; set; } = null!;
    }
}
