﻿using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace AGE.Entities.DAO.AgeUsuarioParamVigencia
{
    public class AgeUsuarioParamVigenciaPKDAO
    {
        [Required(ErrorMessage = "El código de parámetro general no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de parámetro general debe ser un número entero mayor a cero.")]
        [FromRoute(Name ="codigoPG")]
        public int AgeParGeCodigo { get; set; }

        [Required(ErrorMessage = "El código de licenciatario no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de licenciatario debe ser un número entero mayor a cero.")]
        [FromRoute(Name = "codigoLicenciatario")]
        public int AgeUsuariAgeLicencCodigo { get; set; }

        [Required(ErrorMessage = "El código de usuario no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de usuario debe ser un número entero mayor a cero.")]
        [FromRoute(Name = "codigoUsuario")]
        public long AgeUsuariCodigo { get; set; }

        [Required(ErrorMessage = "El código de secuencia no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de secuencia debe ser un número entero mayor a cero.")]
        [FromRoute(Name = "id")]
        public long Codigo { get; set; }
    }
}
