﻿using AGE.Entities.DAO.AgeCamposGenerales;

namespace AGE.Entities.DAO.AgeUsuarioParamVigencia
{
    public class AgeUsuarioParamVigenciaSaveDAO : AgeCamposGeneralesDAO
    {
        public AgeUsuarioParamVigenciaPKDAO Id { get; set; } = null!;
        public string? Observacion { get; set; }
        public DateTime FechaDesde { get; set; }
        public DateTime? FechaHasta { get; set; }
        public string ValorParametro { get; set; } = null!;
    }
}
