﻿using AGE.Entities.DAO.AgeCamposGenerales;

namespace AGE.Entities.DAO.AgeRutas
{
    public class AgeRutasSaveDAO : AgeCamposGeneralesDAO
    {
        public AgeRutasPKDAO Id { get; set; }

        public string Descripcion { get; set; }

        public string Ruta { get; set; } = null!;

    }
}
