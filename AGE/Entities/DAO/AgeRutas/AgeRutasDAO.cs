﻿
namespace AGE.Entities.DAO.AgeRutas
{
    public class AgeRutasDAO
    {
        public AgeRutasPKDAO Id { get; set; }

        public string Descripcion { get; set; }

        public string Ruta { get; set; }

        public string Estado { get; set; }
    }
}
