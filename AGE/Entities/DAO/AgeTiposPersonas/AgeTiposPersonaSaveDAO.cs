﻿using AGE.Entities.DAO.AgeCamposGenerales;

namespace AGE.Entities.DAO.AgeTiposPersonas
{
    public class AgeTiposPersonaSaveDAO : AgeCamposGeneralesDAO
    {
        public AgeTiposPersonaPKDAO Id { get; set; }

        public string Descripcion { get; set; }

        public string? codigoExterno { get; set; }

    }
}
