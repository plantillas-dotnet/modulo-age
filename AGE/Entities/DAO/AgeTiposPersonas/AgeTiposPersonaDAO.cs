﻿namespace AGE.Entities.DAO.AgeTiposPersonas
{
    public class AgeTiposPersonaDAO
    {
        public AgeTiposPersonaPKDAO Id { get; set; }

        public string Descripcion { get; set; }

        public string? CodigoExterno { get; set; }

        public string Estado { get; set; }

    }
}
