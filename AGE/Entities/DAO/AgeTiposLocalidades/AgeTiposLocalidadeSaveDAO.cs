﻿using AGE.Entities.DAO.AgeCamposGenerales;

namespace AGE.Entities.DAO.AgeTiposLocalidades
{
    public class AgeTiposLocalidadeSaveDAO : AgeCamposGeneralesDAO
    {
        public AgeTiposLocalidadePKDAO Id { get; set; }

        public string Descripcion { get; set; }

        public int? AgeTipLoAgePaisCodigo { get; set; }

        public int? AgeTipLoCodigo { get; set; }

    }
}

