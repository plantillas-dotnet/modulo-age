﻿using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace AGE.Entities.DAO.AgeTiposLocalidades
{
    public class AgeTiposLocalidadePKDAO
    {
        [Required(ErrorMessage = "El código de país no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código País debe ser un número entero mayor a cero.")]
        [FromRoute(Name = "agePaisCodigo")]
        public int agePaisCodigo { get; set; }

        [Required(ErrorMessage = "El código del usuario no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de secuencia debe ser un número entero mayor a cero.")]
        [FromRoute(Name = "id")]
        public int codigo { get; set; }
    }
}
