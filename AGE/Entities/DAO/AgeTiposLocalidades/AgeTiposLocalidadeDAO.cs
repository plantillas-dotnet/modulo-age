﻿
namespace AGE.Entities.DAO.AgeTiposLocalidades
{
    public class AgeTiposLocalidadeDAO
    {
        public AgeTiposLocalidadePKDAO Id { get; set; }

        public string Descripcion { get; set; }

        public int? AgeTipLoAgePaisCodigo { get; set; }

        public int? AgeTipLoCodigo { get; set; }

        public string Estado { get; set; }

    }
}