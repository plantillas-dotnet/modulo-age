﻿using AGE.Entities.DAO.AgeCamposGenerales;

namespace AGE.Entities.DAO.AgeInstitucionesFinancieras
{
    public class AgeInstitucionesFinancieraSaveDAO : AgeCamposGeneralesDAO
    {

        public AgeInstitucionesFinancieraPKDAO Id { get; set; }


        public string Descripcion { get; set; } = null!;


        public string? CodigoSegunBanco { get; set; }


        public string? SiglasBanco { get; set; }

    }
}
