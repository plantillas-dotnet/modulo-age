﻿using AGE.Utils;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace AGE.Entities.DAO.AgeInstitucionesFinancieras
{
    public class AgeInstitucionesFinancieraDAO
    {

        public AgeInstitucionesFinancieraPKDAO Id { get; set; }


        public string Descripcion { get; set; } = null!;


        public string? CodigoSegunBanco { get; set; }

    
        public string? SiglasBanco { get; set; }


        public string Estado { get; set; } = null!;

    }
}
