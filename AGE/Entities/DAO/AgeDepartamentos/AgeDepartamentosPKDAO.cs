﻿using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using System.Xml.Linq;

namespace AGE.Entities.DAO.AgeDepartamentos
{
    public class AgeDepartamentosPKDAO
    {
        [FromRoute(Name = "codigoLicenciatario")]
        [Required(ErrorMessage = "El código de licenciatario no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de licenciatario debe ser un número entero mayor a cero.")]
        public int AgeLicencCodigo { get; set; }

        [FromRoute(Name = "id")]
        [Required(ErrorMessage = "El código del departamento no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código del departamento debe ser un número entero mayor a cero.")]
        public int Codigo { get; set; }
    }
}
