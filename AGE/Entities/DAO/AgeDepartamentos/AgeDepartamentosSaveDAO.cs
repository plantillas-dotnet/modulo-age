﻿using AGE.Entities.DAO.AgeCamposGenerales;

namespace AGE.Entities.DAO.AgeDepartamentos
{
    public class AgeDepartamentosSaveDAO : AgeCamposGeneralesDAO
    {
        public AgeDepartamentosPKDAO Id { get; set; }

        public string Descripcion { get; set; } = null!;

        public int? AgeDepartAgeLicencCodigo { get; set; }

        public int? AgeDepartCodigo { get; set; }
    }
}
