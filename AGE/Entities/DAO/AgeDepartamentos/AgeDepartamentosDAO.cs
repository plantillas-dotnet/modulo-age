﻿
namespace AGE.Entities.DAO.AgeDepartamentos
{
    public class AgeDepartamentosDAO
    {
        public string Estado { get; set; }

        public AgeDepartamentosPKDAO Id { get; set; }

        public string Descripcion { get; set; } = null!;

        public int? AgeDepartAgeLicencCodigo { get; set; }

        public int? AgeDepartCodigo { get; set; }
    }
}
