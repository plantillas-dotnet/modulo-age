﻿using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using System.Xml.Linq;

namespace AGE.Entities.DAO.AgeFirmasDigitales
{
    public class AgeFirmasDigitalesPKDAO
    {
        [FromRoute(Name = "codigoLicenciatario")]
        [Required(ErrorMessage = "El código de licenciatario no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de licenciatario debe ser un número entero mayor a cero.")]
        public int AgeSucursAgeLicencCodigo { get; set; }

        [FromRoute(Name = "codigoSucursal")]
        [Required(ErrorMessage = "El código de la sucursal no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de la sucursal debe ser un número entero mayor a cero.")]
        public int AgeSucursCodigo { get; set; }

        [FromRoute(Name = "id")]
        [Required(ErrorMessage = "El código de la firma digital no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de la firma digital debe ser un número entero mayor a cero.")]
        public int Codigo { get; set; }
    }
}
