﻿using AGE.Entities.DAO.AgeCamposGenerales;

namespace AGE.Entities.DAO.AgeFirmasDigitales
{
    public class AgeFirmasDigitalesSaveDAO : AgeCamposGeneralesDAO
    {
        public AgeFirmasDigitalesPKDAO Id { get; set; }

        public string Nombres { get; set; } = null!;

        public string Apellidos { get; set; } = null!;

        public DateTime FechaDesde { get; set; }

        public DateTime? FechaHasta { get; set; }

        public string TipoFirmaDigital { get; set; } = null!;

        public string Clave { get; set; } = null!;

        public string? NombreArchivo { get; set; }

        public string? Url { get; set; }

        public DateTime? FechaUltimaNotificacion { get; set; }
    }
}
