﻿using AGE.Entities.DAO.AgeModalidades;

namespace AGE.Entities.DAO.AgeLogUsuarios
{
    public class AgeLogUsuariosDAO
    {
        public string Estado { get; set; }

        public AgeLogUsuariosPKDAO Id { get; set; }

        public string? Campo { get; set; }

        public string? ValorActual { get; set; }

        public string? ValorAnterior { get; set; }
    }
}
