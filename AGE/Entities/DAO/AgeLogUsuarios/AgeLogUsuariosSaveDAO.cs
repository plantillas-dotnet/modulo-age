﻿using AGE.Entities.DAO.AgeCamposGenerales;

namespace AGE.Entities.DAO.AgeLogUsuarios
{
    public class AgeLogUsuariosSaveDAO : AgeCamposGeneralesDAO
    {
        public AgeLogUsuariosPKDAO Id { get; set; }

        public string? Campo { get; set; }

        public string? ValorActual { get; set; }

        public string? ValorAnterior { get; set; }
    }
}
