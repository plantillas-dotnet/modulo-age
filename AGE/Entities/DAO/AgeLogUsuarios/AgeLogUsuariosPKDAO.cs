﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;

namespace AGE.Entities.DAO.AgeLogUsuarios
{
    public class AgeLogUsuariosPKDAO
    {
        [Required(ErrorMessage = "El código de licenciatario no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de licenciatario debe ser un número entero mayor a cero.")]
        [FromRoute(Name = "ageUsuariAgeLicencCodigo")]
        public int AgeUsuariAgeLicencCodigo { get; set; }

        [Required(ErrorMessage = "El código del usuario no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código del usuario debe ser un número entero mayor a cero.")]
        [FromRoute(Name = "ageUsuariCodigo")]
        public long AgeUsuariCodigo { get; set; }

        [Required(ErrorMessage = "El código de log usuario no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de log usuario debe ser un número entero mayor a cero.")]
        [FromRoute(Name = "codigo")]
        public long Codigo { get; set; }
    }
}
