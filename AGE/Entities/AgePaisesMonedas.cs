﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AGE.Utils;
using Microsoft.EntityFrameworkCore;

namespace AGE.Entities
{
    [Table("age_paises_monedas", Schema = "sasf_user_desa")]
    public partial class AgePaisesMonedas
    {
        [Key]
        [Column("age_moneda_codigo")]
        [Required(ErrorMessage = "El código de moneda no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de moneda debe ser un número entero mayor a cero.")]
        public int AgeMonedaCodigo { get; set; }
        [Key]
        [Column("age_pais_codigo")]
        [Required(ErrorMessage = "El código de país no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de país debe ser un número entero mayor a cero.")]
        public int AgePaisCodigo { get; set; }

        [Column("descripcion")]
        [StringLength(200, ErrorMessage = "La descripción debe contener máximo 200 caracteres.")]
        public string? Descripcion { get; set; }

        [Column("tipo_principal_secundario")]
        [Required(ErrorMessage = "El tipo de idioma no puede ser nulo.")]
        [RegularExpression("^[PS]$", ErrorMessage = $"El campo estado debe ser {Globales.PRINCIPAL} o {Globales.SECUNDARIO}.")]
        public string TipoPrincipalSecundario { get; set; } = null!;

        [Column("orden_principal_secundario")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El Orden Principal Secundario ingreso debe ser un número entero mayor a cero.")]
        public short? OrdenPrincipalSecundario { get; set; }

        [Column("estado")]
        [Required(ErrorMessage = "El estado no puede ser nulo.")]
        [RegularExpression("^[AI]$", ErrorMessage = $"El campo estado debe ser {Globales.ESTADO_ACTIVO} o {Globales.ESTADO_INACTIVO}.")]
        public string Estado { get; set; } = null!;

        [Column("fecha_estado", TypeName = "timestamp(0) without time zone")]
        [Required(ErrorMessage = "La fecha de estado no puede ser nula.")]
        public DateTime FechaEstado { get; set; }

        [Column("fecha_ingreso", TypeName = "timestamp(0) without time zone")]
        [Required(ErrorMessage = "La fecha de ingreso no puede ser nula.")]
        public DateTime FechaIngreso { get; set; }

        [Column("ubicacion_ingreso")]
        public string UbicacionIngreso { get; set; } = null!;

        [Column("observacion_estado")]
        [StringLength(2000,ErrorMessage = "La observación debe contener máximo 2000 caracteres.")]
        public string? ObservacionEstado { get; set; }

        [Column("usuario_ingreso")]
        [Required(ErrorMessage = "El campo usuario ingreso no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El campo usuario ingreso debe ser un número entero mayor a cero.")]
        public long UsuarioIngreso { get; set; }

        [Column("usuario_modificacion")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El usuario modificación debe ser un número entero mayor a cero.")]
        public long? UsuarioModificacion { get; set; }

        [Column("fecha_modificacion", TypeName = "timestamp(0) without time zone")]
        public DateTime? FechaModificacion { get; set; }

        [Column("ubicacion_modificacion")]
        [RegularExpression("^[0-9.]+$", ErrorMessage = "La ubicación de modificación debe contener máximo 200 caracteres.")]
        public string? UbicacionModificacion { get; set; }

        [ForeignKey("AgeMonedaCodigo")]
        [InverseProperty("AgePaisesMoneda")]
        public virtual AgeMonedas AgeMonedaCodigoNavigation { get; set; } = null!;
        [ForeignKey("AgePaisCodigo")]
        [InverseProperty("AgePaisesMoneda")]
        public virtual AgePaises AgePaisCodigoNavigation { get; set; } = null!;
    }
}
