﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AGE.Utils;
using Microsoft.EntityFrameworkCore;

namespace AGE.Entities
{
    [Table("age_sucursales", Schema = "sasf_user_desa")]
    public partial class AgeSucursales
    {
        public AgeSucursales()
        {
            AgeFirmasDigitales = new HashSet<AgeFirmasDigitales>();
            AgePuntosEmisions = new HashSet<AgePuntosEmisiones>();
            AgeUsuarios = new HashSet<AgeUsuarios>();
            InverseAgeSucurs = new HashSet<AgeSucursales>();
        }

        [Key]
        [Column("age_licenc_codigo")]
        [Required(ErrorMessage = "El código de licenciatario no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de licenciatario debe ser un número entero mayor a cero.")]
        public int AgeLicencCodigo { get; set; }

        [Key]
        [Column("codigo")]
        [Required(ErrorMessage = "El código de la sucursal no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de la sucursal debe ser un número entero mayor a cero.")]
        public int Codigo { get; set; }

        [Column("age_age_tip_lo_age_pais_codigo")]
        [Required(ErrorMessage = "El código del país no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código del país debe ser un número entero mayor a cero.")]
        public int AgeAgeTipLoAgePaisCodigo { get; set; }

        [Column("age_locali_age_tip_lo_codigo")]
        [Required(ErrorMessage = "El código del tipo localidad no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código del tipo localidad debe ser un número entero mayor a cero.")]
        public int AgeLocaliAgeTipLoCodigo { get; set; }

        [Column("age_locali_codigo")]
        [Required(ErrorMessage = "El código de la localidad no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de la localidad debe ser un número entero mayor a cero.")]
        public int AgeLocaliCodigo { get; set; }

        [Column("age_tip_su_age_licenc_codigo")]
        [Required(ErrorMessage = "El código de licenciatario del tipo sucursal no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de licenciatario del tipo sucursal debe ser un número entero mayor a cero.")]
        public int AgeTipSuAgeLicencCodigo { get; set; }

        [Column("age_tip_su_codigo")]
        [Required(ErrorMessage = "El código del tipo sucursal no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código del tipo sucursal debe ser un número entero mayor a cero.")]
        public int AgeTipSuCodigo { get; set; }

        [Column("direccion")]
        [StringLength(2000, ErrorMessage = "La dirección debe contener máximo 2000 caracteres.")]
        [Required(ErrorMessage = "La dirección no puede ser nula.")]
        public string Direccion { get; set; } = null!;

        [Column("telefono1")]
        [RegularExpression(@"^\d{10}$", ErrorMessage = "El teléfono 1 debe contener 10 caracteres númericos.")]
        [Required(ErrorMessage = "El teléfono 1 no puede ser nula.")]
        public string Telefono1 { get; set; } = null!;

        [Column("e_mail1")]
        [RegularExpression("^[a-zA-Z0-9_!#$%&’*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$", ErrorMessage = "El formato del email 1 no es válido.")]
        public string? EMail1 { get; set; }

        [Column("e_mail2")]
        [RegularExpression("^[a-zA-Z0-9_!#$%&’*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$", ErrorMessage = "El formato del email 2 no es válido.")]
        public string? EMail2 { get; set; }

        [Column("age_sucurs_age_licenc_codigo")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código del licenciatario debe ser un número entero mayor a cero.")]
        public int? AgeSucursAgeLicencCodigo { get; set; }

        [Column("age_sucurs_codigo")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de la sucursal debe ser un número entero mayor a cero.")]
        public int? AgeSucursCodigo { get; set; }

        [Column("codigo_establecimiento")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código del establecimiento debe ser un número entero mayor a cero.")]
        public short? CodigoEstablecimiento { get; set; }

        [Column("descripcion")]
        [StringLength(200, ErrorMessage = "La descripción debe contener máximo 200 caracteres.")]
        [Required(ErrorMessage = "La descripción no puede ser nula.")]
        public string Descripcion { get; set; } = null!;

        [Column("telefono2")]
        [RegularExpression(@"^\d{10}$", ErrorMessage = "El teléfono 2 debe contener 10 caracteres númericos.")]
        public string? Telefono2 { get; set; }

        [Column("observacion")]
        [StringLength(2000,ErrorMessage = "La observación debe contener máximo 2000 caracteres.")]
        public string? Observacion { get; set; }

        [Column("latitud")]
        [RegularExpression("^[0-9.-]{1,255}$", ErrorMessage = "El campo latitud solo debe contener números.")]
        public string? Latitud { get; set; }

        [Column("longitud")]
        [RegularExpression("^[0-9.-]{1,255}$", ErrorMessage = "El campo Longitud solo debe contener números.")]
        public string? Longitud { get; set; }

        [Column("centro_acopio")]
        [RegularExpression("^(S|N)$", ErrorMessage = $"El campo centro de acopio debe ser {Globales.SI} o {Globales.NO}.")]
        public string? CentroAcopio { get; set; }

        [Column("estado")]
        [Required(ErrorMessage = "El estado no puede ser nulo.")]
        [RegularExpression("^[AI]$", ErrorMessage = $"El campo estado debe ser {Globales.ESTADO_ACTIVO} o {Globales.ESTADO_INACTIVO}.")]
        public string Estado { get; set; } = null!;

        [Column("fecha_estado", TypeName = "timestamp(0) without time zone")]
        [Required(ErrorMessage = "La fecha de estado no puede ser nula.")]
        public DateTime FechaEstado { get; set; }

        [Column("observacion_estado")]
        [StringLength(2000,ErrorMessage = "La observación debe contener máximo 2000 caracteres.")]
        public string? ObservacionEstado { get; set; }

        [Column("usuario_ingreso")]
        [Required(ErrorMessage = "El campo usuario ingreso no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El campo usuario ingreso debe ser un número entero mayor a cero.")]
        public long UsuarioIngreso { get; set; }

        [Column("fecha_ingreso", TypeName = "timestamp(0) without time zone")]
        [Required(ErrorMessage = "La fecha de ingreso no puede ser nula.")]
        public DateTime FechaIngreso { get; set; }

        [Column("ubicacion_ingreso")]
        [Required(ErrorMessage = "La ubicación de ingreso no puede ser nula.")]
        [RegularExpression("^[0-9.]+$", ErrorMessage = "La ubicación de ingreso debe contener máximo 200 caracteres.")]
        public string UbicacionIngreso { get; set; } = null!;

        [Column("usuario_modificacion")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El usuario modificación debe ser un número entero mayor a cero.")]
        public long? UsuarioModificacion { get; set; }

        [Column("fecha_modificacion", TypeName = "timestamp(0) without time zone")]
        public DateTime? FechaModificacion { get; set; }

        [Column("ubicacion_modificacion")]
        [RegularExpression("^[0-9.]+$", ErrorMessage = "La ubicación de modificación debe contener máximo 200 caracteres.")]
        public string? UbicacionModificacion { get; set; }

        [ForeignKey("AgeLocaliCodigo,AgeLocaliAgeTipLoCodigo,AgeAgeTipLoAgePaisCodigo")]
        [InverseProperty("AgeSucursales")]
        public virtual AgeLocalidades Age { get; set; } = null!;
        [ForeignKey("AgeLicencCodigo")]
        [InverseProperty("AgeSucursales")]
        public virtual AgeLicenciatario AgeLicencCodigoNavigation { get; set; } = null!;
        [ForeignKey("AgeSucursCodigo,AgeSucursAgeLicencCodigo")]
        [InverseProperty("InverseAgeSucurs")]
        public virtual AgeSucursales? AgeSucurs { get; set; }
        [ForeignKey("AgeTipSuCodigo,AgeTipSuAgeLicencCodigo")]
        [InverseProperty("AgeSucursales")]
        public virtual AgeTiposSucursales AgeTipSu { get; set; } = null!;
        [InverseProperty("AgeSucurs")]
        public virtual ICollection<AgeFirmasDigitales> AgeFirmasDigitales { get; set; }
        [InverseProperty("AgeSucurs")]
        public virtual ICollection<AgePuntosEmisiones> AgePuntosEmisions { get; set; }
        [InverseProperty("AgeSucurs")]
        public virtual ICollection<AgeUsuarios> AgeUsuarios { get; set; }
        [InverseProperty("AgeSucurs")]
        public virtual ICollection<AgeSucursales> InverseAgeSucurs { get; set; }
    }
}
