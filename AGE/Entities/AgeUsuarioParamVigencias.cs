﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AGE.Utils;
using Microsoft.EntityFrameworkCore;

namespace AGE.Entities
{
    [Table("age_usuario_param_vigencias", Schema = "sasf_user_desa")]
    public partial class AgeUsuarioParamVigencias
    {
        [Key]
        [Column("age_par_ge_codigo")]
        [Required(ErrorMessage = "El código de parámetro general no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de parámetro general debe ser un número entero mayor a cero.")]
        public int AgeParGeCodigo { get; set; }

        [Key]
        [Column("age_usuari_age_licenc_codigo")]
        [Required(ErrorMessage = "El código de licenciatario no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de licenciatario debe ser un número entero mayor a cero.")]
        public int AgeUsuariAgeLicencCodigo { get; set; }

        [Key]
        [Column("age_usuari_codigo")]
        [Required(ErrorMessage = "El código de usuario no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de usuario debe ser un número entero mayor a cero.")]
        public long AgeUsuariCodigo { get; set; }

        [Key]
        [Column("codigo")]
        [Required(ErrorMessage = "El código de secuencia no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de secuencia debe ser un número entero mayor a cero.")]
        public long Codigo { get; set; }

        [Column("observacion")]
        [StringLength(2000,ErrorMessage = "La observación debe contener máximo 2000 caracteres.")]
        public string? Observacion { get; set; }

        [Column("fecha_desde", TypeName = "timestamp(0) without time zone")]
        [Required(ErrorMessage = "La fecha desde no puede ser nula.")]
        public DateTime FechaDesde { get; set; }

        [Column("fecha_hasta", TypeName = "timestamp(0) without time zone")]
        public DateTime? FechaHasta { get; set; }

        [Column("valor_parametro")]
        [Required(ErrorMessage = "El valor del parámetro no puede ser nulo.")]
        [StringLength(200, ErrorMessage = "El valor del parámetro debe contener máximo 200 caracteres.")]
        public string ValorParametro { get; set; } = null!;

        [Column("estado")]
        [Required(ErrorMessage = "El estado no puede ser nulo.")]
        [RegularExpression("^[AI]$", ErrorMessage = $"El campo estado debe ser {Globales.ESTADO_ACTIVO} o {Globales.ESTADO_INACTIVO}.")]
        public string Estado { get; set; } = null!;

        [Column("fecha_estado", TypeName = "timestamp(0) without time zone")]
        [Required(ErrorMessage = "La fecha de estado no puede ser nula.")]
        public DateTime FechaEstado { get; set; }

        [Column("fecha_ingreso", TypeName = "timestamp(0) without time zone")]
        [Required(ErrorMessage = "La fecha de ingreso no puede ser nula.")]
        public DateTime FechaIngreso { get; set; }

        [Column("ubicacion_ingreso")]
        public string UbicacionIngreso { get; set; } = null!;

        [Column("observacion_estado")]
        [StringLength(2000, ErrorMessage = "La observación de estado debe contener máximo 2000 caracteres.")]
        public string? ObservacionEstado { get; set; }

        [Column("usuario_ingreso")]
        [Required(ErrorMessage = "El campo usuario ingreso no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El campo usuario ingreso debe ser un número entero mayor a cero.")]
        public long UsuarioIngreso { get; set; }

        [Column("usuario_modificacion")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El usuario modificación debe ser un número entero mayor a cero.")]
        public long? UsuarioModificacion { get; set; }

        [Column("fecha_modificacion", TypeName = "timestamp(0) without time zone")]
        public DateTime? FechaModificacion { get; set; }

        [Column("ubicacion_modificacion")]
        [RegularExpression("^[0-9.]+$", ErrorMessage = "La ubicación de modificación debe contener máximo 200 caracteres.")]
        public string? UbicacionModificacion { get; set; }

        [ForeignKey("AgeParGeCodigo")]
        [InverseProperty("AgeUsuarioParamVigencia")]
        public virtual AgeParametrosGenerales AgeParGeCodigoNavigation { get; set; } = null!;
        [ForeignKey("AgeUsuariCodigo,AgeUsuariAgeLicencCodigo")]
        [InverseProperty("AgeUsuarioParamVigencia")]
        public virtual AgeUsuarios AgeUsuari { get; set; } = null!;
    }
}
