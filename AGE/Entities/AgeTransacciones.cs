using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AGE.Utils;
using Microsoft.EntityFrameworkCore;

namespace AGE.Entities
{
    [Table("age_transacciones", Schema = "sasf_user_desa")]
    public partial class AgeTransacciones
    {
        public AgeTransacciones()
        {
            AgeFavoritos = new HashSet<AgeFavoritos>();
            AgeLicenciatariTransacciones = new HashSet<AgeLicenciatariTransaccione>();
            AgePerfilesTransacciones = new HashSet<AgePerfilesTransacciones>();
            AgeTransaccionesIdiomas = new HashSet<AgeTransaccionesIdiomas>();
            InverseAgeTransa = new HashSet<AgeTransacciones>();
        }

        [Key]
        [Column("age_aplica_codigo")]
        [Required(ErrorMessage = "El código de aplicación no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de aplicación debe ser un número entero mayor a cero.")]
        public int AgeAplicaCodigo { get; set; }

        [Key]
        [Column("codigo")]
        [Required(ErrorMessage = "El código no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código debe ser un número entero mayor a cero.")]
        public int Codigo { get; set; }

        [Column("codigo_externo")]
        [Required(ErrorMessage = "El código externo no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código externo debe ser un número entero mayor a cero.")]
        public int CodigoExterno { get; set; }

        [Column("descripcion")]
        [Required(ErrorMessage = "La descripción no puede ser nulo.")]
        [StringLength(200, ErrorMessage = "La descripción debe contener máximo 200 caracteres.")]
        public string Descripcion { get; set; } = null!;

        [Column("transaccion")]
        [Required(ErrorMessage = "El campo transacción no puede ser nulo")]
        [RegularExpression("^[SNsn]$", ErrorMessage = "El campo transacción debe ser S o N")]
        public string? Transaccion { get; set; }

        [Column("orden_presentacion")]
        [RegularExpression("^[0-9]+$", ErrorMessage = "El orden de presentación debe ser un número mayor a 0.")]
        public short? OrdenPresentacion { get; set; }

        [Column("tipo_transaccion")]
        [Required(ErrorMessage = "El tipo transacción no puede ser nulo")]
        [RegularExpression("^[DCOdcod]$", ErrorMessage = "El tipo transacción debe ser D, C, O")]
        public string TipoTransaccion { get; set; } = null!;

        [Column("tipo_operacion")]
        [Required(ErrorMessage = "El tipo operación no puede ser nula")]
        [RegularExpression("^[IEie]$", ErrorMessage = "El tipo operación debe ser I o E")]
        public string TipoOperacion { get; set; } = null!;

        [Column("observacion_estado")]
        [StringLength(2000,ErrorMessage = "La observación debe contener máximo 2000 caracteres.")]
        public string? ObservacionEstado { get; set; }

        [Column("fecha_ingreso", TypeName = "timestamp(0) without time zone")]
        [Required(ErrorMessage = "La fecha de estado no puede ser nula.")]
        public DateTime FechaIngreso { get; set; }

        [Column("age_transa_age_aplica_codigo")]
        public int? AgeTransaAgeAplicaCodigo { get; set; }

        [Column("age_transa_codigo")]
        public int? AgeTransaCodigo { get; set; }

        [Column("opcion")]
        [RegularExpression("^[SNsn]$", ErrorMessage = "El campo opción debe ser S o N.")]
        public string? Opcion { get; set; }

        [Column("nivel")]
        [RegularExpression("^[0-9]+$", ErrorMessage = "El nivel debe ser mayor a 0.")]
        public short? Nivel { get; set; }

        [Column("url")]
        [StringLength(200,ErrorMessage = "La url puede tener máximo 200 caracteres.")]
        public string? Url { get; set; }

        [Column("parametros")]
        [StringLength(200, ErrorMessage = "El campo Parámetros puede tener máximo 200 caracteres.")]
        public string? Parametros { get; set; }

        [Required(ErrorMessage = "El estado no puede ser nulo.")]
        [Column("estado")]
        [RegularExpression("^[AI]$", ErrorMessage = $"El campo estado debe ser {Globales.ESTADO_ACTIVO} o {Globales.ESTADO_INACTIVO}.")]
        public string Estado { get; set; } = null!;

        [Column("fecha_estado", TypeName = "timestamp(0) without time zone")]
        [Required(ErrorMessage = "La fecha de estado no puede ser nula.")]
        public DateTime FechaEstado { get; set; }

        [Column("usuario_ingreso")]
        [Required(ErrorMessage = "El campo usuario ingreso no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El campo usuario ingreso debe ser un número entero mayor a cero.")]
        public long UsuarioIngreso { get; set; }

        [Column("ubicacion_ingreso")]
        [Required(ErrorMessage = "La ubicación de ingreso no puede ser nula.")]
        [RegularExpression("^[0-9.]+$", ErrorMessage = "La ubicación de ingreso debe contener máximo 200 caracteres.")]
        public string UbicacionIngreso { get; set; } = null!;

        [Column("usuario_modificacion")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El usuario modificación debe ser un número entero mayor a cero.")]
        public long? UsuarioModificacion { get; set; }

        [Column("fecha_modificacion", TypeName = "timestamp(0) without time zone")]
        public DateTime? FechaModificacion { get; set; }

        [Column("ubicacion_modificacion")]
        [RegularExpression("^[0-9.]+$", ErrorMessage = "La ubicación de modificación debe contener máximo 200 caracteres.")]
        public string? UbicacionModificacion { get; set; }

        [ForeignKey("AgeAplicaCodigo")]
        [InverseProperty("AgeTransacciones")]
        public virtual AgeAplicaciones AgeAplicaCodigoNavigation { get; set; } = null!;

        [ForeignKey("AgeTransaCodigo,AgeTransaAgeAplicaCodigo")]
        [InverseProperty("InverseAgeTransa")]
        public virtual AgeTransacciones? AgeTransa { get; set; }
        [InverseProperty("AgeTransa")]
        public virtual ICollection<AgeFavoritos> AgeFavoritos { get; set; }
        [InverseProperty("AgeTransa")]
        public virtual ICollection<AgeLicenciatariTransaccione> AgeLicenciatariTransacciones { get; set; }
        [InverseProperty("AgeTransa")]
        public virtual ICollection<AgePerfilesTransacciones> AgePerfilesTransacciones { get; set; }
        [InverseProperty("AgeTransa")]
        public virtual ICollection<AgeTransaccionesIdiomas> AgeTransaccionesIdiomas { get; set; }
        [InverseProperty("AgeTransa")]
        public virtual ICollection<AgeTransacciones> InverseAgeTransa { get; set; }
    }
}
