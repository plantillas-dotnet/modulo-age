﻿using AGE.Entities.DAO.AgePerfiles;
using AGE.Entities.DAO.AgePerfilesTransacciones;

namespace AGE.Entities.Composite.AgePerfilesTransacciones
{
    public class AgePerfilesTransaccionesCompositeSave
    {

        public AgePerfilesSaveDAO AgePerfiles { get; set; }

        public List<AgePerfilesTransaccioneSaveDAO> AgePerfilesTransaccionesList { get; set; }

    }
}
