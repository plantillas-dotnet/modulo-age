﻿using AGE.Entities.DAO.AgePerfiles;
using AGE.Entities.DAO.AgePerfilesTransacciones;
using AGE.Utils.WebLink;

namespace AGE.Entities.Composite.AgePerfilesTransacciones
{
    public class AgePerfilesTransaccionesComposite
    {

        public Resource<AgePerfilesDAO> AgePerfiles { get; set; }

        public List<Resource<AgePerfilesTransaccioneDAO>> AgePerfilesTransaccionesList { get; set; }

    }
}
