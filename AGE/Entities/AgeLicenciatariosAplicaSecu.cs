﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;
using AGE.Utils;

namespace AGE.Entities
{
    [Table("age_licenciatarios_aplica_secu", Schema = "sasf_user_desa")]
    public partial class AgeLicenciatariosAplicaSecu
    {
        [Key]
        [Column("age_lic_ap_age_aplica_codigo")]
        [Required(ErrorMessage = "El código de aplicación no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de aplicación debe ser un número entero mayor a cero.")]
        public int AgeLicApAgeAplicaCodigo { get; set; }


        [Key]
        [Required(ErrorMessage = "El código de licenciatario no puede ser nulo.")]
        [Column("age_lic_ap_age_licenc_codigo")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de licenciatario debe ser un número entero mayor a cero.")]
        public int AgeLicApAgeLicencCodigo { get; set; }


        [Key]
        [Required(ErrorMessage = "El código de secuencia no puede ser nulo.")]
        [Column("codigo")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de secuencia debe ser un número entero mayor a cero.")]
        public int Codigo { get; set; }


        [Required(ErrorMessage = "La descripción no puede ser nula.")]
        [Column("descripcion")]
        [StringLength(200, ErrorMessage = "La descripción debe contener máximo 200 caracteres.")]
        public string Descripcion { get; set; } = null!;


        [Required(ErrorMessage = "El valor inicial no puede ser nulo.")]
        [Column("valor_inicial")]
        [RegularExpression("^[0-9]\\d*$", ErrorMessage = "El valor inicial debe ser un número entero positivo.")]
        public long ValorInicial { get; set; }


        [Required(ErrorMessage = "El incremento no puede ser nulo.")]
        [Column("incrementa_en")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El incremento debe ser un número entero mayor a cero.")]
        public int IncrementaEn { get; set; }


        [RegularExpression("^[0-9]\\d*$", ErrorMessage = "El valor actual debe ser un número entero positivo.")]
        [Column("valor_actual")]
        public long ValorActual { get; set; }


        [Required(ErrorMessage = "El campo ciclica no puede ser nulo.")]
        [Column("ciclica")]
        [RegularExpression("^[NS]$", ErrorMessage = $"El campo ciclica debe ser {Globales.SI} o {Globales.NO}.")]
        public string Ciclica { get; set; } = null!;


        [Required(ErrorMessage = "El estado no puede ser nulo.")]
        [Column("estado")]
        [RegularExpression("^[AI]$", ErrorMessage = $"El campo estado debe ser {Globales.ESTADO_ACTIVO} o {Globales.ESTADO_INACTIVO}.")]
        public string Estado { get; set; } = null!;


        [Required(ErrorMessage = "La fecha de estado no puede ser nula.")]
        [Column("fecha_estado", TypeName = "timestamp(0) without time zone")]
        public DateTime FechaEstado { get; set; }


        [Column("observacion_estado")]
        [StringLength(2000,ErrorMessage = "La observación debe contener máximo 2000 caracteres.")]
        public string? ObservacionEstado { get; set; }


        [Required(ErrorMessage = "El campo usuario ingreso no puede ser nulo.")]
        [Column("usuario_ingreso")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El campo usuario ingreso debe ser un número entero mayor a cero.")]
        public long UsuarioIngreso { get; set; }


        [Required(ErrorMessage = "La fecha de ingreso no puede ser nula.")]
        [Column("fecha_ingreso", TypeName = "timestamp(0) without time zone")]
        public DateTime FechaIngreso { get; set; }


        [Required(ErrorMessage = "La ubicación de ingreso no puede ser nula.")]
        [Column("ubicacion_ingreso")]
        [RegularExpression("^[0-9.]+$", ErrorMessage = "La ubicación de ingreso debe contener máximo 200 caracteres.")]
        public string? UbicacionIngreso { get; set; }


        [Column("usuario_modificacion")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El usuario modificación debe ser un número entero mayor a cero.")]
        public long? UsuarioModificacion { get; set; }


        [Column("fecha_modificacion", TypeName = "timestamp(0) without time zone")]
        public DateTime? FechaModificacion { get; set; }


        [RegularExpression("^[0-9.]+$", ErrorMessage = "La ubicación de modificación debe contener máximo 200 caracteres.")]
        [Column("ubicacion_modificacion")]
        public string? UbicacionModificacion { get; set; }



        [ForeignKey("AgeLicApAgeLicencCodigo,AgeLicApAgeAplicaCodigo")]
        [InverseProperty("AgeLicenciatariosAplicaSecus")]
        public virtual AgeLicenciatariosAplicaciones AgeLicApAge { get; set; } = null!;

    }
}
