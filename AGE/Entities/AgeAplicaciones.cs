﻿using AGE.Utils;
using Microsoft.AspNetCore.Components.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AGE.Entities
{
    [Table("age_aplicaciones", Schema = "sasf_user_desa")]
    public partial class AgeAplicaciones
    {
        public AgeAplicaciones()
        {
            AgeAplicacionesDependenciaAgeAplicaCodigoNavigations = new HashSet<AgeAplicacionesDependencia>();
            AgeAplicacionesDependenciaAgeAplicaCodigoSerNavigations = new HashSet<AgeAplicacionesDependencia>();
            AgeLicenciatariosAplicacions = new HashSet<AgeLicenciatariosAplicaciones>();
            AgeSistemasAplicaciones = new HashSet<AgeSistemasAplicaciones>();
            AgeTransacciones = new HashSet<AgeTransacciones>();
        }

        [Key]
        [Column("codigo")]
        [Required(ErrorMessage = "El código de la aplicación no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de la aplicación debe ser un número entero mayor a cero.")]
        public int Codigo { get; set; }

        [Column("codigo_externo")]
        [Required(ErrorMessage = "El código externo no puede ser nulo.")]
        [StringLength(3,ErrorMessage = "El código externo debe contener máximo 3 caracteres.")]
        public string CodigoExterno { get; set; } = null!;

        [Column("descripcion")]
        [Required(ErrorMessage = "La descripción no puede ser nula.")]
        [StringLength(200, ErrorMessage = "La descripción debe contener máximo 200 caracteres.")]
        public string Descripcion { get; set; } = null!;

        [Column("inicio_secu_trx_ce")]
        [Required(ErrorMessage = "El inicio de la secuencia externa no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El inicio de la secuencia externa debe ser un número entero mayor a cero.")]
        public long InicioSecuTrxCe { get; set; }

        [Column("incremento_secu_trx_ce")]
        [Required(ErrorMessage = "El incremento de la secuencia externa no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El incremento de la secuencia externa debe ser un número entero mayor a cero.")]
        public int IncrementoSecuTrxCe { get; set; }

        [Column("valor_actual_secu_trx_ce")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El valor actual de la secuencia externa debe ser un número entero mayor a cero.")]
        public long ValorActualSecuTrxCe { get; set; }

        [Column("ciclica_secu_trx_ce")]
        [Required(ErrorMessage = "El campo ciclica de la secuencia código externa no puede ser nula.")]
        [RegularExpression("^[SN]$", ErrorMessage = $"El campo cíclica de la secuencia código externa debe ser {Globales.SI} o {Globales.NO}.")]
        public string CiclicaSecuTrxCe { get; set; } = null!;

        [Column("inicio_secu_trx_ci")]
        [Required(ErrorMessage = "El inicio de la secuencia interna no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El inicio de la secuencia interna debe ser un número entero mayor a cero.")]
        public long InicioSecuTrxCi { get; set; }

        [Column("valor_actual_secu_trx_ci")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El valor actual de la secuencia interna debe ser un número entero mayor a cero.")]
        public long ValorActualSecuTrxCi { get; set; }

        [Column("ciclica_secu_trx_ci")]
        [Required(ErrorMessage = "El campo ciclica de la secuencia código interno no puede ser nula.")]
        [RegularExpression("^[SN]$", ErrorMessage = $"El campo cíclica de la secuencia código interna debe ser {Globales.SI} o {Globales.NO}.")]
        public string CiclicaSecuTrxCi { get; set; } = null!;

        [Column("orden_instalacion")]
        [Required(ErrorMessage = "El orden de instalación no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El orden de instalación debe ser un número entero mayor a cero.")]
        public short OrdenInstalacion { get; set; }

        [Required(ErrorMessage = "El estado no puede ser nulo.")]
        [Column("estado")]
        [RegularExpression("^[AI]$", ErrorMessage = $"El campo estado debe ser {Globales.ESTADO_ACTIVO} o {Globales.ESTADO_INACTIVO}.")]
        public string Estado { get; set; } = null!;

        [Column("fecha_estado", TypeName = "timestamp(0) without time zone")]
        [Required(ErrorMessage = "La fecha de estado no puede ser nula.")]
        public DateTime FechaEstado { get; set; }

        [Column("observacion_estado")]
        [StringLength(2000,ErrorMessage = "La observación debe contener máximo 2000 caracteres.")]
        public string? ObservacionEstado { get; set; }

        [Column("usuario_ingreso")]
        [Required(ErrorMessage = "El campo usuario ingreso no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El campo usuario ingreso debe ser un número entero mayor a cero.")]
        public long UsuarioIngreso { get; set; }

        [Column("fecha_ingreso", TypeName = "timestamp(0) without time zone")]
        [Required(ErrorMessage = "La fecha de ingreso no puede ser nula.")]
        public DateTime FechaIngreso { get; set; }

        [Column("ubicacion_ingreso")]
        [Required(ErrorMessage = "La ubicación de ingreso no puede ser nula.")]
        [RegularExpression("^[0-9.]+$", ErrorMessage = "La ubicación de ingreso debe contener máximo 200 caracteres.")]
        public string UbicacionIngreso { get; set; } = null!;

        [Column("usuario_modificacion")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El usuario modificación debe ser un número entero mayor a cero.")]
        public long? UsuarioModificacion { get; set; }

        [Column("fecha_modificacion", TypeName = "timestamp(0) without time zone")]
        public DateTime? FechaModificacion { get; set; }

        [Column("ubicacion_modificacion")]
        [RegularExpression("^[0-9.]+$", ErrorMessage = "La ubicación de modificación debe contener máximo 200 caracteres.")]
        public string? UbicacionModificacion { get; set; }

        [InverseProperty("AgeAplicaCodigoNavigation")]
        public virtual ICollection<AgeAplicacionesDependencia> AgeAplicacionesDependenciaAgeAplicaCodigoNavigations { get; set; }
        [InverseProperty("AgeAplicaCodigoSerNavigation")]
        public virtual ICollection<AgeAplicacionesDependencia> AgeAplicacionesDependenciaAgeAplicaCodigoSerNavigations { get; set; }
        [InverseProperty("AgeAplicaCodigoNavigation")]
        public virtual ICollection<AgeLicenciatariosAplicaciones> AgeLicenciatariosAplicacions { get; set; }
        [InverseProperty("AgeAplicaCodigoNavigation")]
        public virtual ICollection<AgeSistemasAplicaciones> AgeSistemasAplicaciones { get; set; }
        [InverseProperty("AgeAplicaCodigoNavigation")]
        public virtual ICollection<AgeTransacciones> AgeTransacciones { get; set; }
    }
}
