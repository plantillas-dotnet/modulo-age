﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AGE.Utils;
using Microsoft.EntityFrameworkCore;

namespace AGE.Entities
{
    [Table("age_direcciones", Schema = "sasf_user_desa")]
    public partial class AgeDirecciones
    {
        [Key]
        [Column("age_person_age_licenc_codigo")]
        [Required(ErrorMessage = "El código de licenciatario no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de licenciatario debe ser un número entero mayor a cero.")]
        public int AgePersonAgeLicencCodigo { get; set; }
        
        [Key]
        [Column("age_person_codigo")]
        [Required(ErrorMessage = "El código de la persona no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de la persona debe ser un número entero mayor a cero.")]
        public long AgePersonCodigo { get; set; }

        [Key]
        [Column("codigo")]
        [Required(ErrorMessage = "El código de la dirección no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de la dirección debe ser un número entero mayor a cero.")]
        public int Codigo { get; set; }

        [Column("age_age_tip_lo_age_pais_codigo")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código del país debe ser un número entero mayor a cero.")]
        public int? AgeAgeTipLoAgePaisCodigo { get; set; }

        [Column("age_locali_age_tip_lo_codigo")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código del tipo localidad debe ser un número entero mayor a cero.")]
        public int? AgeLocaliAgeTipLoCodigo { get; set; }

        [Column("age_locali_codigo")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de localidad debe ser un número entero mayor a cero.")]
        public int? AgeLocaliCodigo { get; set; }

        [Column("age_tip_di_codigo")]
        [Required(ErrorMessage = "El código del tipo de dirección no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código del tipo direccion debe ser un número entero mayor a cero.")]
        public int AgeTipDiCodigo { get; set; }

        [Column("descripcion")]
        [Required(ErrorMessage = "La descripción de la dirección no puede ser nula.")]
        [StringLength(200,ErrorMessage = "La descripción de la dirección debe contener máximo 200 caracteres.")]
        public string Descripcion { get; set; } = null!;

        [Column("telefono_1")]
        [RegularExpression(@"^\d{10}$", ErrorMessage = "El número de teléfono 1 debe contener 10 caracteres númericos.")]
        public string? Telefono1 { get; set; }

        [Column("telefono_2")]
        [RegularExpression(@"^\d{7,20}$", ErrorMessage = "El número de teléfono 2 debe contener 10 caracteres númericos.")]
        public string? Telefono2 { get; set; }

        [Column("nombre_contacto")]
        [StringLength(200,ErrorMessage = "El nombre de contacto no puede tener más de 200 caracteres.")]
        public string? NombreContacto { get; set; }

        [Column("correo_electronico_contacto")]
        [RegularExpression("^[a-zA-Z0-9_!#$%&’*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$", ErrorMessage = "El formato del correo electrónico de contacto no es válido.")]
        public string? CorreoElectronicoContacto { get; set; }

        [Column("celular_contacto")]
        [RegularExpression(@"^\d{10}$", ErrorMessage = "El número de celular de contacto debe contener 10 caracteres númericos.")]
        public string? CelularContacto { get; set; }

        [Column("latitud")]
        [RegularExpression("^[0-9.-]{0,255}$", ErrorMessage = "El campo Latitud solo debe contener números.")]
        public string? Latitud { get; set; }

        [Column("longitud")]
        [RegularExpression("^[0-9.-]{0,255}$", ErrorMessage = "El campo Longitud solo debe contener números.")]
        public string? Longitud { get; set; }

        [Column("estado")]
        [Required(ErrorMessage = "El estado no puede ser nulo.")]
        [RegularExpression("^[AI]$", ErrorMessage = $"El campo estado debe ser {Globales.ESTADO_ACTIVO} o {Globales.ESTADO_INACTIVO}.")]
        public string Estado { get; set; } = null!;

        [Column("fecha_estado", TypeName = "timestamp(0) without time zone")]
        [Required(ErrorMessage = "La fecha de estado no puede ser nula.")]
        public DateTime FechaEstado { get; set; }

        [Column("observacion_estado")]
        [StringLength(2000,ErrorMessage = "La observación debe contener máximo 2000 caracteres.")]
        public string? ObservacionEstado { get; set; }

        [Column("usuario_ingreso")]
        [Required(ErrorMessage = "El campo usuario ingreso no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El campo usuario ingreso debe ser un número entero mayor a cero.")]
        public long UsuarioIngreso { get; set; }

        [Column("fecha_ingreso", TypeName = "timestamp(0) without time zone")]
        [Required(ErrorMessage = "La fecha de ingreso no puede ser nula.")]
        public DateTime FechaIngreso { get; set; }

        [Column("ubicacion_ingreso")]
        [Required(ErrorMessage = "La ubicación de ingreso no puede ser nula.")]
        [RegularExpression("^[0-9.]+$", ErrorMessage = "La ubicación de ingreso debe contener máximo 200 caracteres.")]
        public string UbicacionIngreso { get; set; } = null!;

        [Column("usuario_modificacion")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El usuario modificación debe ser un número entero mayor a cero.")]
        public long? UsuarioModificacion { get; set; }

        [Column("fecha_modificacion", TypeName = "timestamp(0) without time zone")]
        public DateTime? FechaModificacion { get; set; }

        [Column("ubicacion_modificacion")]
        [RegularExpression("^[0-9.]+$", ErrorMessage = "La ubicación de modificación debe contener máximo 200 caracteres.")]
        public string? UbicacionModificacion { get; set; }

        [ForeignKey("AgeLocaliCodigo,AgeLocaliAgeTipLoCodigo,AgeAgeTipLoAgePaisCodigo")]
        [InverseProperty("AgeDirecciones")]
        public virtual AgeLocalidades? Age { get; set; }
        [ForeignKey("AgePersonCodigo,AgePersonAgeLicencCodigo")]
        [InverseProperty("AgeDirecciones")]
        public virtual AgePersonas AgePerson { get; set; } = null!;
        [ForeignKey("AgeTipDiCodigo")]
        [InverseProperty("AgeDirecciones")]
        public virtual AgeTiposDirecciones AgeTipDiCodigoNavigation { get; set; } = null!;
    }
}
