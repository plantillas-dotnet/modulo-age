﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AGE.Utils;
using Microsoft.EntityFrameworkCore;

namespace AGE.Entities
{
    [Table("age_licenciatarios_aplicacion", Schema = "sasf_user_desa")]
    public partial class AgeLicenciatariosAplicaciones
    {
        public AgeLicenciatariosAplicaciones()
        {
            AgeLicenciatariosAplicaSecus = new HashSet<AgeLicenciatariosAplicaSecu>();
            AgeLogErrores = new HashSet<AgeLogErrores>();
            AgeSubAplicaciones = new HashSet<AgeSubAplicaciones>();
        }

        [Key]
        [Column("age_aplica_codigo")]
        [Required(ErrorMessage = "El código de la aplicación no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de la aplicación debe ser un número entero mayor a cero.")]
        public int AgeAplicaCodigo { get; set; }


        [Key]
        [Column("age_licenc_codigo")]
        [Required(ErrorMessage = "El código de licenciatario no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de licenciatario debe ser un número entero mayor a cero.")]
        public int AgeLicencCodigo { get; set; }


        [Column("age_ruta_age_licenc_codigo")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El codigo de licenciatario de la ruta debe ser un número entero mayor a cero.")]
        public int? AgeRutaAgeLicencCodigo { get; set; }


        [Column("age_ruta_codigo")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El codigo de la ruta debe ser un número entero mayor a cero.")]
        public int? AgeRutaCodigo { get; set; }


        [Required(ErrorMessage = "El estado no puede ser nulo.")]
        [Column("estado")]
        [RegularExpression("^[AI]$", ErrorMessage = $"El campo estado debe ser {Globales.ESTADO_ACTIVO} o {Globales.ESTADO_INACTIVO}.")]
        public string Estado { get; set; } = null!;


        [Required(ErrorMessage = "La fecha de estado no puede ser nula.")]
        [Column("fecha_estado", TypeName = "timestamp(0) without time zone")]
        public DateTime FechaEstado { get; set; }


        [Column("observacion_estado")]
        [StringLength(2000,ErrorMessage = "La observación debe contener máximo 2000 caracteres.")]
        public string? ObservacionEstado { get; set; }


        [Required(ErrorMessage = "El campo usuario ingreso no puede ser nulo.")]
        [Column("usuario_ingreso")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El campo usuario ingreso debe ser un número entero mayor a cero.")]
        public long UsuarioIngreso { get; set; }


        [Required(ErrorMessage = "La fecha de ingreso no puede ser nula.")]
        [Column("fecha_ingreso", TypeName = "timestamp(0) without time zone")]
        public DateTime FechaIngreso { get; set; }


        [Required(ErrorMessage = "La ubicación de ingreso no puede ser nula.")]
        [Column("ubicacion_ingreso")]
        [RegularExpression("^[0-9.]+$", ErrorMessage = "La ubicación de ingreso debe contener máximo 200 caracteres.")]
        public string? UbicacionIngreso { get; set; }


        [Column("usuario_modificacion")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El usuario modificación debe ser un número entero mayor a cero.")]
        public long? UsuarioModificacion { get; set; }


        [Column("fecha_modificacion", TypeName = "timestamp(0) without time zone")]
        public DateTime? FechaModificacion { get; set; }


        [RegularExpression("^[0-9.]+$", ErrorMessage = "La ubicación de modificación debe contener máximo 200 caracteres.")]
        [Column("ubicacion_modificacion")]
        public string? UbicacionModificacion { get; set; }



        [ForeignKey("AgeAplicaCodigo")]
        [InverseProperty("AgeLicenciatariosAplicacions")]
        public virtual AgeAplicaciones AgeAplicaCodigoNavigation { get; set; } = null!;
        [ForeignKey("AgeLicencCodigo")]
        [InverseProperty("AgeLicenciatariosAplicacions")]
        public virtual AgeLicenciatario AgeLicencCodigoNavigation { get; set; } = null!;
        [ForeignKey("AgeRutaAgeLicencCodigo,AgeRutaCodigo")]
        [InverseProperty("AgeLicenciatariosAplicacions")]
        public virtual AgeRutas? AgeRuta { get; set; }
        [InverseProperty("AgeLicApAge")]
        public virtual ICollection<AgeLicenciatariosAplicaSecu> AgeLicenciatariosAplicaSecus { get; set; }
        [InverseProperty("AgeLicApAge")]
        public virtual ICollection<AgeLogErrores> AgeLogErrores { get; set; }
        [InverseProperty("AgeLicApAge")]
        public virtual ICollection<AgeSubAplicaciones> AgeSubAplicaciones { get; set; }
    }
}
