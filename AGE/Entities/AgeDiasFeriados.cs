﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AGE.Utils;
using Microsoft.EntityFrameworkCore;

namespace AGE.Entities
{
    [Table("age_dias_feriados", Schema = "sasf_user_desa")]
    public partial class AgeDiasFeriados
    {
        public AgeDiasFeriados()
        {
            AgeFeriadosLocalidades = new HashSet<AgeFeriadosLocalidades>();
            AgeFeriadosPaises = new HashSet<AgeFeriadosPaises>();
        }

        [Key]
        [Column("codigo")]
        [Required(ErrorMessage = "El código de días de feriado no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El código de días de feriado debe ser un número entero mayor a cero.")]
        public int Codigo { get; set; }

        [Column("descripcion")]
        [Required(ErrorMessage = "La descripción no puede ser nula.")]
        [StringLength(2000, ErrorMessage = "La descripción debe contener máximo 2000 caracteres.")]
        public string Descripcion { get; set; } = null!;

        [Column("fecha_desde", TypeName = "timestamp(0) without time zone")]
        [Required(ErrorMessage = "La Fecha Desde no puede ser nula.")]
        public DateTime FechaDesde { get; set; }

        [Column("fecha_hasta", TypeName = "timestamp(0) without time zone")]
        [Required(ErrorMessage = "La Fecha Hasta no puede ser nula.")]
        public DateTime FechaHasta { get; set; }

        [Column("hora_desde", TypeName = "timestamp(0) without time zone")]
        public DateTime? HoraDesde { get; set; }

        [Column("hora_hasta", TypeName = "timestamp(0) without time zone")]
        public DateTime? HoraHasta { get; set; }

        [Column("estado")]
        [Required(ErrorMessage = "El estado no puede ser nulo.")]
        [RegularExpression("^[AI]$", ErrorMessage = $"El campo estado debe ser {Globales.ESTADO_ACTIVO} o {Globales.ESTADO_INACTIVO}.")]
        public string Estado { get; set; } = null!;

        [Column("fecha_estado", TypeName = "timestamp(0) without time zone")]
        [Required(ErrorMessage = "La fecha de estado no puede ser nula.")]
        public DateTime FechaEstado { get; set; }

        [Column("observacion_estado")]
        [StringLength(2000,ErrorMessage = "La observación debe contener máximo 2000 caracteres.")]
        public string? ObservacionEstado { get; set; }

        [Column("usuario_ingreso")]
        [Required(ErrorMessage = "El campo usuario ingreso no puede ser nulo.")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El campo usuario ingreso debe ser un número entero mayor a cero.")]
        public long UsuarioIngreso { get; set; }

        [Column("fecha_ingreso", TypeName = "timestamp(0) without time zone")]
        [Required(ErrorMessage = "La fecha de ingreso no puede ser nula.")]
        public DateTime FechaIngreso { get; set; }

        [Column("ubicacion_ingreso")]
        [Required(ErrorMessage = "La ubicación de ingreso no puede ser nula.")]
        [RegularExpression("^[0-9.]+$", ErrorMessage = "La ubicación de ingreso debe contener máximo 200 caracteres.")]
        public string UbicacionIngreso { get; set; } = null!;

        [Column("usuario_modificacion")]
        [RegularExpression("^[1-9]\\d*$", ErrorMessage = "El usuario modificación debe ser un número entero mayor a cero.")]
        public long? UsuarioModificacion { get; set; }

        [Column("fecha_modificacion", TypeName = "timestamp(0) without time zone")]
        public DateTime? FechaModificacion { get; set; }

        [Column("ubicacion_modificacion")]
        [RegularExpression("^[0-9.]+$", ErrorMessage = "La ubicación de modificación debe contener máximo 200 caracteres.")]
        public string? UbicacionModificacion {get; set;}

        [InverseProperty("AgeDiaFeCodigoNavigation")]
        public virtual ICollection<AgeFeriadosLocalidades> AgeFeriadosLocalidades { get; set; }
        [InverseProperty("AgeDiaFeCodigoNavigation")]
        public virtual ICollection<AgeFeriadosPaises> AgeFeriadosPaises { get; set; }
    }
}
