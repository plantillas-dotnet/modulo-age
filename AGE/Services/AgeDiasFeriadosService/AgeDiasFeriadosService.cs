﻿using AGE.Controllers;
using AGE.Entities.DAO.AgeDiasFeriados;
using AGE.Entities;
using AGE.Middleware.Exceptions.BadRequest;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Repositories.AgeDiasFeriadosRepository;
using AGE.Services.AgeSecuenciasPrimariasService;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using AGE.Utils;
using System.ComponentModel.DataAnnotations;
using System.Transactions;

namespace AGE.Services.AgeDiasFeriadosService
{
    public class AgeDiasFeriadosService : IAgeDiasFeriadosService
    {
        private readonly IAgeDiasFeriadosRepository _repository;
        private readonly IAgeSecuenciasPrimariasService _ageSecuenciasPrimariasService;


        public AgeDiasFeriadosService(
            IAgeDiasFeriadosRepository Repository,
            IAgeSecuenciasPrimariasService ageSecuenciasPrimariasService)
        {
            _repository = Repository;
            _ageSecuenciasPrimariasService = ageSecuenciasPrimariasService;
        }

        public Task<Page<AgeDiasFeriadosDAO>> ConsultarTodos(
            string descripcion,
            Pageable pageable)
        {
            pageable.Validar<AgeDiasFeriadosDAO>();

            return _repository.ConsultarTodos(descripcion, pageable);
        }

        public Task<AgeDiasFeriadosDAO> ConsultarPorId(
            int codigo)
        {
            if (codigo <= 0)
                throw new InvalidIdException();

            AgeDiasFeriadosDAO? ageDiasFeriadosDAO = _repository.ConsultarPorId(codigo).Result;

            return ageDiasFeriadosDAO == null
                ? throw new RegisterNotFoundException("Días Feriado con código: " + codigo + " no existe.")
                : Task.FromResult(ageDiasFeriadosDAO);
        }

        private AgeDiasFeriados ConsultarCompletePorId(
            int codigo)
        {
            if (codigo <= 0)
                throw new InvalidIdException();

            return _repository.ConsultarCompletePorId(codigo).Result;
        }

        public Task<Page<AgeDiasFeriadosDAO>> ConsultarListaFiltro(string filtro, Pageable pageable)
        {

            if (string.IsNullOrWhiteSpace(filtro))
                throw new InvalidFieldException("El filtro es requerido.");

            pageable.Validar<AgeDiasFeriadosDAO>();

            return _repository.ConsultarListaFiltro(filtro, pageable);

        }

        public async Task<Resource<AgeDiasFeriadosDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgeDiasFeriadosSaveDAO ageDiasFeriadosSaveDAO)
        {
            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    AgeDiasFeriados ageDiasFeriados = ValidarInsert(_httpContextAccessor, ageDiasFeriadosSaveDAO);

                    AgeDiasFeriadosDAO ageDiasFeriadosDAO = await _repository.Insertar(ageDiasFeriados);

                    Resource<AgeDiasFeriadosDAO> ageDiasFeriadosDAOWithResource = GetDataWithResource(_httpContextAccessor, ageDiasFeriadosDAO);

                    transactionScope.Complete();

                    return ageDiasFeriadosDAOWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public async Task<List<Resource<AgeDiasFeriadosDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeDiasFeriadosSaveDAO> ageDiasFeriadosSaveDAOList)
        {
            if (ageDiasFeriadosSaveDAOList == null)
                throw new InvalidSintaxisException();

            List<AgeDiasFeriados> ageDiasFeriadosList = new List<AgeDiasFeriados>();

            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    foreach (AgeDiasFeriadosSaveDAO ageDiasFeriadosSaveDAO in ageDiasFeriadosSaveDAOList)
                    {
                        AgeDiasFeriados AgeDiasFeriado = ValidarInsert(_httpContextAccessor, ageDiasFeriadosSaveDAO);
                        ageDiasFeriadosList.Add(AgeDiasFeriado);
                    }

                    List<AgeDiasFeriadosDAO> ageDiasFeriadosDAOList = await _repository.InsertarVarios(ageDiasFeriadosList);

                    List<Resource<AgeDiasFeriadosDAO>> ageDiasFeriadosDAOListWithResource = new List<Resource<AgeDiasFeriadosDAO>>();

                    foreach (AgeDiasFeriadosDAO ageDiasFeriadosDAO in ageDiasFeriadosDAOList)
                    {
                        ageDiasFeriadosDAOListWithResource.Add(GetDataWithResource(_httpContextAccessor, ageDiasFeriadosDAO));
                    }

                    transactionScope.Complete();

                    return ageDiasFeriadosDAOListWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public Task<AgeDiasFeriadosDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeDiasFeriadosSaveDAO ageDiasFeriadosSaveDAO)
        {
            AgeDiasFeriados ageDiasFeriados = ValidarUpdate(_httpContextAccessor, ageDiasFeriadosSaveDAO);

            return _repository.Actualizar(ageDiasFeriados);
        }

        public Task<List<AgeDiasFeriadosDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeDiasFeriadosSaveDAO> ageDiasFeriadosSaveDAOList)
        {
            if (ageDiasFeriadosSaveDAOList == null)
                throw new InvalidSintaxisException();

            ValidateKeys.ValidarPKDuplicadas(ageDiasFeriadosSaveDAOList, p => new { p.Codigo }, "Codigo");

            List<AgeDiasFeriados> ageDiasFeriadosList = new List<AgeDiasFeriados>();
            AgeDiasFeriados ageDiasFeriados;

            foreach (AgeDiasFeriadosSaveDAO ageRutasSaveDAO in ageDiasFeriadosSaveDAOList)
            {
                ageDiasFeriados = ValidarUpdate(_httpContextAccessor, ageRutasSaveDAO);
                ageDiasFeriadosList.Add(ageDiasFeriados);
            }

            return _repository.ActualizarVarios(ageDiasFeriadosList);
        }

        private static Resource<AgeDiasFeriadosDAO> GetDataWithResource(
            IHttpContextAccessor _httpContextAccessor,
            AgeDiasFeriadosDAO ageDiasFeriadosDAO)
        {
            string rutaSegmentoFinal = $"{ageDiasFeriadosDAO.Codigo}";

            return Resource<AgeDiasFeriadosDAO>.GetDataWithResource<AgeDiasFeriadosController>(
                _httpContextAccessor, rutaSegmentoFinal, ageDiasFeriadosDAO);
        }

        private AgeDiasFeriados ValidarInsert(
            IHttpContextAccessor _httpContextAccessor,
            AgeDiasFeriadosSaveDAO ageDiasFeriadosSaveDAO)
        {
            if (ageDiasFeriadosSaveDAO == null)
                throw new InvalidSintaxisException();

            Validator.ValidateObject(ageDiasFeriadosSaveDAO, new ValidationContext(ageDiasFeriadosSaveDAO), true);

            if (ageDiasFeriadosSaveDAO.FechaDesde == default)
                throw new InvalidFieldException("La fecha desde no puede ser nula.");

            if (ageDiasFeriadosSaveDAO.FechaHasta == default)
                throw new InvalidFieldException("La fecha hasta no puede ser nula.");

            FuncionesSecuencias fg = new FuncionesSecuencias(_ageSecuenciasPrimariasService);

            ageDiasFeriadosSaveDAO.Codigo = fg.ObtenerSecuenciaPrimaria(
                        _httpContextAccessor,
                        Globales.CODIGO_SECUENCIA_DIA_FERIADO,
                        ageDiasFeriadosSaveDAO.UsuarioIngreso);

            ValidarPK(ageDiasFeriadosSaveDAO.Codigo);

            AgeDiasFeriados entityObject = FromDAOToEntity(_httpContextAccessor,ageDiasFeriadosSaveDAO);

            Validator.ValidateObject(entityObject, new ValidationContext(entityObject), true);

            return entityObject;
        }

        private AgeDiasFeriados ValidarUpdate(
            IHttpContextAccessor _httpContextAccessor,
            AgeDiasFeriadosSaveDAO ageDiasFeriadosSaveDAO)
        {
            if (ageDiasFeriadosSaveDAO == null)
                throw new InvalidSintaxisException();

            if (ageDiasFeriadosSaveDAO.UsuarioModificacion == null || ageDiasFeriadosSaveDAO.UsuarioModificacion <= 0)
                throw new InvalidFieldException("El usuario de modificación debe ser un número entero mayor a cero.");

            AgeDiasFeriados ageDiasFeriados = ConsultarCompletePorId(ageDiasFeriadosSaveDAO.Codigo) ??
                throw new RegisterNotFoundException($"Días Feriados con código: {ageDiasFeriadosSaveDAO.Codigo} no existe.");

            ageDiasFeriados.Estado = !string.IsNullOrWhiteSpace(ageDiasFeriadosSaveDAO.Estado) ? ageDiasFeriadosSaveDAO.Estado : ageDiasFeriados.Estado;
            ageDiasFeriados.FechaEstado = !string.IsNullOrWhiteSpace(ageDiasFeriadosSaveDAO.Estado) ? DateTime.Now : ageDiasFeriados.FechaEstado;
            ageDiasFeriados.Descripcion = !string.IsNullOrWhiteSpace(ageDiasFeriadosSaveDAO.Descripcion) ? ageDiasFeriadosSaveDAO.Descripcion : ageDiasFeriados.Descripcion;
            ageDiasFeriados.ObservacionEstado = !string.IsNullOrWhiteSpace(ageDiasFeriadosSaveDAO.ObservacionEstado) ? ageDiasFeriadosSaveDAO.ObservacionEstado : ageDiasFeriados.ObservacionEstado;

            ageDiasFeriados.FechaDesde = ageDiasFeriadosSaveDAO.FechaDesde != default ? ageDiasFeriadosSaveDAO.FechaDesde : ageDiasFeriados.FechaDesde;
            ageDiasFeriados.FechaHasta = ageDiasFeriadosSaveDAO.FechaHasta != default ? ageDiasFeriadosSaveDAO.FechaHasta : ageDiasFeriados.FechaHasta;
            ageDiasFeriados.HoraDesde = ageDiasFeriadosSaveDAO.HoraDesde != null && ageDiasFeriadosSaveDAO.HoraDesde != default ? ageDiasFeriadosSaveDAO.HoraDesde : ageDiasFeriados.HoraDesde;
            ageDiasFeriados.HoraHasta = ageDiasFeriadosSaveDAO.HoraHasta != null && ageDiasFeriadosSaveDAO.HoraHasta != default ? ageDiasFeriadosSaveDAO.HoraHasta : ageDiasFeriados.HoraHasta;
            ageDiasFeriados.FechaModificacion = DateTime.Now;
            ageDiasFeriados.UbicacionModificacion = Ubicacion.getIpAdress(_httpContextAccessor);
            ageDiasFeriados.UsuarioModificacion = ageDiasFeriadosSaveDAO.UsuarioModificacion;

            Validator.ValidateObject(ageDiasFeriados, new ValidationContext(ageDiasFeriados), true);

            return ageDiasFeriados;
        }

        private AgeDiasFeriados FromDAOToEntity(
           IHttpContextAccessor _httpContextAccessor,
           AgeDiasFeriadosSaveDAO ageDiasFeriadosSaveDAO)
        {

            return new AgeDiasFeriados
            {
                Codigo = ageDiasFeriadosSaveDAO.Codigo,
                Descripcion = ageDiasFeriadosSaveDAO.Descripcion,
                FechaDesde = ageDiasFeriadosSaveDAO.FechaDesde,
                FechaHasta = ageDiasFeriadosSaveDAO.FechaHasta,
                HoraDesde = ageDiasFeriadosSaveDAO.HoraDesde == default
                                        ? null : ageDiasFeriadosSaveDAO.HoraDesde,
                HoraHasta = ageDiasFeriadosSaveDAO.HoraHasta == default
                                        ? null : ageDiasFeriadosSaveDAO.HoraHasta,
                Estado = ageDiasFeriadosSaveDAO.Estado.ToUpper().ToString(),
                FechaEstado = DateTime.Now,
                FechaIngreso = DateTime.Now,
                ObservacionEstado = ageDiasFeriadosSaveDAO.ObservacionEstado,
                UsuarioIngreso = ageDiasFeriadosSaveDAO.UsuarioIngreso,
                UbicacionIngreso = Ubicacion.getIpAdress(_httpContextAccessor)
            };
        }

        private void ValidarPK(int codigoDiasFeriado)
        {
            ValidateKeys.ValidarNoExistenciaKey(codigoDiasFeriado,
                $"Días feriado con código: {codigoDiasFeriado} ya existe.",
                _repository.ConsultarPorId);
        }
    }
}
