﻿using AGE.Entities.DAO.AgeDiasFeriados;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;

namespace AGE.Services.AgeDiasFeriadosService
{
    public interface IAgeDiasFeriadosService
    {
        Task<Page<AgeDiasFeriadosDAO>> ConsultarTodos(
            string Descripcion,
            Pageable pageable);

        Task<Page<AgeDiasFeriadosDAO>> ConsultarListaFiltro(
            string filtro,
            Pageable pageable);

        Task<AgeDiasFeriadosDAO> ConsultarPorId(int Codigo);

        Task<Resource<AgeDiasFeriadosDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgeDiasFeriadosSaveDAO AgeDiasFeriadosSaveDAO);

        Task<List<Resource<AgeDiasFeriadosDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeDiasFeriadosSaveDAO> AgeDiasFeriadosSaveDAOList);

        Task<AgeDiasFeriadosDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeDiasFeriadosSaveDAO AgeDiasFeriadosSaveDAO);

        Task<List<AgeDiasFeriadosDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeDiasFeriadosSaveDAO> AgeDiasFeriadosSaveDAOList);
    }
}
