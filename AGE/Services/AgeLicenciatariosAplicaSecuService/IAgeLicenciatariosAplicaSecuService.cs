﻿
using AGE.Entities;
using AGE.Entities.DAO.AgeLicenciatariosAplicaSecu;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using Microsoft.EntityFrameworkCore.Storage;

namespace AGE.Services.AgeLicenciatariosAplicaSecuService
{
    public interface IAgeLicenciatariosAplicaSecuService
    {
        Task<Page<AgeLicenciatariosAplicaSecuDAO>> ConsultarTodos(
            int codigoLicenciatario,
            int codigo,
            int ageLicApAgeAplicaCodigo,
            string ciclica,
            string descripcion,
            Pageable pageable);

        Task<Page<AgeLicenciatariosAplicaSecuDAO>> ConsultarListaFiltro(
            int codigoLicenciatario,
            string filtro,
            Pageable pageable);

        Task<AgeLicenciatariosAplicaSecuDAO> ConsultarPorId(
            AgeLicenciatariosAplicaSecuPKDAO ageLicenciatariosAplicaSecuPKDAO);

        Task<AgeLicenciatariosAplicaSecu> ConsultarPorSecuencia(
            AgeLicenciatariosAplicaSecuPKDAO ageLicenciatariosAplicaSecuPKDAO);

        Task<Resource<AgeLicenciatariosAplicaSecuDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgeLicenciatariosAplicaSecuSaveDAO ageLicenciatariosAplicaSecuSaveDAO);

        Task<List<Resource<AgeLicenciatariosAplicaSecuDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeLicenciatariosAplicaSecuSaveDAO> ageLicenciatariosAplicaSecuSaveDAOList);

        Task<AgeLicenciatariosAplicaSecuDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeLicenciatariosAplicaSecuSaveDAO ageLicenciatariosAplicaSecuSaveDAO);

        Task<List<AgeLicenciatariosAplicaSecuDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeLicenciatariosAplicaSecuSaveDAO> ageLicenciatariosAplicaSecuSaveDAOList);

        void ActualizarSecuencia(
            IHttpContextAccessor _httpContextAccessor, 
            AgeLicenciatariosAplicaSecu ageLicenciatariosAplicaSecu);
        /*
        void ActualizarSecuencia(
            IHttpContextAccessor _httpContextAccessor,
            IDbContextTransaction transaction,
            AgeLicenciatariosAplicaSecu ageLicenciatariosAplicaSecu);*/
    }
}
