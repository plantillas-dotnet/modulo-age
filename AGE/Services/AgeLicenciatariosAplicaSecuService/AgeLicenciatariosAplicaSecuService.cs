﻿
using AGE.Entities;
using AGE.Entities.DAO.AgeLicenciatariosAplicaSecu;
using AGE.Repositories.AgeLicenciatariosAplicaSecuRepository;
using AGE.Utils;
using AGE.Utils.Paginacion;
using AGE.Middleware.Exceptions.BadRequest;
using AGE.Middleware.Exceptions.NotFound;
using System.ComponentModel.DataAnnotations;
using AGE.Utils.WebLink;
using AGE.Controllers;
using AGE.Entities.DAO.AgeLicenciatarioAplicacion;
using AGE.Repositories.AgeLicenciatariosAplicacionRepository;
using System.Transactions;

namespace AGE.Services.AgeLicenciatariosAplicaSecuService
{
    public class AgeLicenciatariosAplicaSecuService : IAgeLicenciatariosAplicaSecuService
    {

        private readonly IAgeLicenciatariosAplicaSecuRepository _repository;
        private readonly IAgeLicenciatarioAplicacionRepository _repositoryLicenciatarioAplicacion;

        public AgeLicenciatariosAplicaSecuService(IAgeLicenciatariosAplicaSecuRepository Repository,
                                                    IAgeLicenciatarioAplicacionRepository RepositoryLicenciatarioAplicacion)
        {
            _repository = Repository;
            _repositoryLicenciatarioAplicacion = RepositoryLicenciatarioAplicacion;
        }

        public Task<Page<AgeLicenciatariosAplicaSecuDAO>> ConsultarTodos(
            int codigoLicenciatario,
            int codigo,
            int ageLicApAgeAplicaCodigo,
            string ciclica,
            string descripcion,
            Pageable pageable)
        {

            if (codigoLicenciatario <= 0)
                throw new InvalidFieldException("El código de licenciatario debe ser un número entero mayor a cero.");

            pageable.Validar<AgeLicenciatariosAplicaSecuDAO>();

            return _repository.ConsultarTodos(codigoLicenciatario, codigo, ageLicApAgeAplicaCodigo, ciclica, descripcion, pageable);
        }

        public Task<AgeLicenciatariosAplicaSecu> ConsultarPorSecuencia(
            AgeLicenciatariosAplicaSecuPKDAO ageLicenciatariosAplicaSecuPKDAO)
        {
            Validator.ValidateObject(ageLicenciatariosAplicaSecuPKDAO, new ValidationContext(ageLicenciatariosAplicaSecuPKDAO), true);

            AgeLicenciatariosAplicaSecu ageLicenciatariosAplicaSecu = _repository.ConsultarPorSecuencia(ageLicenciatariosAplicaSecuPKDAO).Result;

            return Task.FromResult(ageLicenciatariosAplicaSecu);
        }

        public Task<AgeLicenciatariosAplicaSecuDAO> ConsultarPorId(
            AgeLicenciatariosAplicaSecuPKDAO ageLicenciatariosAplicaSecuPKDAO)
        {

            Validator.ValidateObject(ageLicenciatariosAplicaSecuPKDAO, new ValidationContext(ageLicenciatariosAplicaSecuPKDAO), true);

            AgeLicenciatariosAplicaSecuDAO ageLicenciatariosAplicaSecuDAO = _repository.ConsultarPorId(ageLicenciatariosAplicaSecuPKDAO).Result;

            return ageLicenciatariosAplicaSecuDAO == null
                ? throw new RegisterNotFoundException("Secuencia con código: " + ageLicenciatariosAplicaSecuPKDAO.codigo + " no existe.")
                : Task.FromResult(ageLicenciatariosAplicaSecuDAO);
        }

        public Task<Page<AgeLicenciatariosAplicaSecuDAO>> ConsultarListaFiltro(
            int codigoLicenciatario,
            string filtro,
            Pageable pageable)
        {
            if (codigoLicenciatario <= 0)
                throw new InvalidFieldException("El código de licenciatario debe ser un número entero mayor a cero.");

            if (string.IsNullOrWhiteSpace(filtro))
                throw new InvalidFieldException("El filtro es requerido.");

            pageable.Validar<AgeLicenciatariosAplicaSecuDAO>();

            return _repository.ConsultarListaFiltro(codigoLicenciatario, filtro.ToLower(), pageable);
        }

        public async Task<Resource<AgeLicenciatariosAplicaSecuDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgeLicenciatariosAplicaSecuSaveDAO ageLicenciatariosAplicaSecuSaveDAO)
        {
            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    AgeLicenciatariosAplicaSecu ageLicenciatariosAplicaSecu = ValidarInsert(_httpContextAccessor, ageLicenciatariosAplicaSecuSaveDAO);

                    AgeLicenciatariosAplicaSecuDAO ageLicenciatariosAplicaSecuDAO = await _repository.Insertar(ageLicenciatariosAplicaSecu);

                    Resource<AgeLicenciatariosAplicaSecuDAO> ageLicenciatariosAplicaSecuDAOWithReource = GetDataWithResource(_httpContextAccessor, ageLicenciatariosAplicaSecuDAO);

                    transactionScope.Complete();

                    return ageLicenciatariosAplicaSecuDAOWithReource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public async Task<List<Resource<AgeLicenciatariosAplicaSecuDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeLicenciatariosAplicaSecuSaveDAO> ageLicenciatariosAplicaSecuSaveDAOList)
        {
            if (ageLicenciatariosAplicaSecuSaveDAOList == null)
                throw new InvalidSintaxisException();

            ValidateKeys.ValidarPKDuplicadas(ageLicenciatariosAplicaSecuSaveDAOList, p => new
            {
                p.Id.ageAplicaCodigo,
                p.Id.ageLicencCodigo,
                p.Id.codigo
            }, "Id");

            List<AgeLicenciatariosAplicaSecu> ageLicenciatariosAplicaSecuList = new List<AgeLicenciatariosAplicaSecu>();

            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    foreach (AgeLicenciatariosAplicaSecuSaveDAO ageLicenciatariosAplicaSecuSaveDAO in ageLicenciatariosAplicaSecuSaveDAOList)
                    {
                        AgeLicenciatariosAplicaSecu ageLicenciatariosAplicaSecu = ValidarInsert(_httpContextAccessor, ageLicenciatariosAplicaSecuSaveDAO);
                        ageLicenciatariosAplicaSecuList.Add(ageLicenciatariosAplicaSecu);
                    }

                    List<AgeLicenciatariosAplicaSecuDAO> ageLicenciatariosAplicaSecuDAOList = await _repository.InsertarVarios(ageLicenciatariosAplicaSecuList);

                    List<Resource<AgeLicenciatariosAplicaSecuDAO>> agePerfilesDAOListWithResource = new List<Resource<AgeLicenciatariosAplicaSecuDAO>>();

                    foreach (AgeLicenciatariosAplicaSecuDAO ageLicenciatariosAplicaSecuDAO in ageLicenciatariosAplicaSecuDAOList)
                    {
                        agePerfilesDAOListWithResource.Add(GetDataWithResource(_httpContextAccessor, ageLicenciatariosAplicaSecuDAO));
                    }

                    transactionScope.Complete();

                    return agePerfilesDAOListWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public Task<AgeLicenciatariosAplicaSecuDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeLicenciatariosAplicaSecuSaveDAO ageLicenciatariosAplicaSecuSaveDAO)
        {
            AgeLicenciatariosAplicaSecu ageLicenciatariosAplicaSecu = ValidarUpdate(_httpContextAccessor, ageLicenciatariosAplicaSecuSaveDAO);

            return _repository.Actualizar(ageLicenciatariosAplicaSecu);
        }

        public Task<List<AgeLicenciatariosAplicaSecuDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeLicenciatariosAplicaSecuSaveDAO> ageLicenciatariosAplicaSecuSaveDAOList)
        {
            if (ageLicenciatariosAplicaSecuSaveDAOList == null)
                throw new InvalidSintaxisException();

            ValidateKeys.ValidarPKDuplicadas(ageLicenciatariosAplicaSecuSaveDAOList, p => new
            {
                p.Id.ageAplicaCodigo,
                p.Id.ageLicencCodigo,
                p.Id.codigo
            }, "Id");

            List<AgeLicenciatariosAplicaSecu> ageLicenciatariosAplicaSecuList = new List<AgeLicenciatariosAplicaSecu>();
            AgeLicenciatariosAplicaSecu ageLicenciatariosAplicaSecu;

            foreach (AgeLicenciatariosAplicaSecuSaveDAO ageLicenciatariosAplicaSecuSaveDAO in ageLicenciatariosAplicaSecuSaveDAOList)
            {
                ageLicenciatariosAplicaSecu = ValidarUpdate(_httpContextAccessor, ageLicenciatariosAplicaSecuSaveDAO);
                ageLicenciatariosAplicaSecuList.Add(ageLicenciatariosAplicaSecu);
            }

            return _repository.ActualizarVarios(ageLicenciatariosAplicaSecuList);
        }

        public void ActualizarSecuencia(
            IHttpContextAccessor _httpContextAccessor,
            AgeLicenciatariosAplicaSecu ageLicenciatariosAplicaSecu)
        {
            ageLicenciatariosAplicaSecu.FechaModificacion = DateTime.Now;
            ageLicenciatariosAplicaSecu.UbicacionModificacion = Ubicacion.getIpAdress(_httpContextAccessor);

            Validator.ValidateObject(ageLicenciatariosAplicaSecu, new ValidationContext(ageLicenciatariosAplicaSecu), true);

            _repository.ActualizarSecuencia(ageLicenciatariosAplicaSecu);
        }

        private static Resource<AgeLicenciatariosAplicaSecuDAO> GetDataWithResource(
            IHttpContextAccessor _httpContextAccessor,
            AgeLicenciatariosAplicaSecuDAO ageLicenciatariosAplicaSecuDAO)
        {
            string rutaSegmentoFinal = $"{ageLicenciatariosAplicaSecuDAO.Id.ageLicencCodigo}/{ageLicenciatariosAplicaSecuDAO.Id.ageAplicaCodigo}/{ageLicenciatariosAplicaSecuDAO.Id.codigo}";

            return Resource<AgeLicenciatariosAplicaSecuDAO>.GetDataWithResource<AgeLicenciatarioAplicaSecuController>(
                _httpContextAccessor, rutaSegmentoFinal, ageLicenciatariosAplicaSecuDAO);
        }

        private AgeLicenciatariosAplicaSecu ValidarInsert(
            IHttpContextAccessor _httpContextAccessor,
            AgeLicenciatariosAplicaSecuSaveDAO ageLicenciatariosAplicaSecuSaveDAO)
        {
            if (ageLicenciatariosAplicaSecuSaveDAO == null)
                throw new InvalidSintaxisException();

            ValidarKeys(ageLicenciatariosAplicaSecuSaveDAO.Id);

            AgeLicenciatariosAplicaSecu entityObject = FromDAOToEntity(_httpContextAccessor,ageLicenciatariosAplicaSecuSaveDAO);

            Validator.ValidateObject(entityObject, new ValidationContext(entityObject), true);

            return entityObject;
        }

        private AgeLicenciatariosAplicaSecu ValidarUpdate(
            IHttpContextAccessor _httpContextAccessor,
            AgeLicenciatariosAplicaSecuSaveDAO ageLicenciatariosAplicaSecuSaveDAO)
        {
            if (ageLicenciatariosAplicaSecuSaveDAO == null)
                throw new InvalidSintaxisException();

            if (ageLicenciatariosAplicaSecuSaveDAO.UsuarioModificacion == null ||
                ageLicenciatariosAplicaSecuSaveDAO.UsuarioModificacion <= 0)
                throw new InvalidFieldException("El usuario de modificación debe ser un número entero mayor a cero.");

            AgeLicenciatariosAplicaSecu ageLicenciatariosAplicaSecu = ConsultarPorSecuencia(ageLicenciatariosAplicaSecuSaveDAO.Id).Result 
                ?? throw new RegisterNotFoundException($"Secuencia con código: {ageLicenciatariosAplicaSecuSaveDAO.Id.codigo}" +
                $", código licenciatario: {ageLicenciatariosAplicaSecuSaveDAO.Id.ageLicencCodigo} " +
                $"y código aplciación: {ageLicenciatariosAplicaSecuSaveDAO.Id.ageAplicaCodigo} no existe.");

            if (ageLicenciatariosAplicaSecuSaveDAO.ValorActual > 0 && ageLicenciatariosAplicaSecuSaveDAO.ValorActual != null)
                ageLicenciatariosAplicaSecu.ValorActual = (long)ageLicenciatariosAplicaSecuSaveDAO.ValorActual;

            if (ageLicenciatariosAplicaSecuSaveDAO.ValorInicial > 0)
                ageLicenciatariosAplicaSecu.ValorInicial = (long)ageLicenciatariosAplicaSecuSaveDAO.ValorInicial;

            if (!string.IsNullOrWhiteSpace(ageLicenciatariosAplicaSecuSaveDAO.Estado))
            {
                ageLicenciatariosAplicaSecu.Estado = ageLicenciatariosAplicaSecuSaveDAO.Estado;
                ageLicenciatariosAplicaSecu.FechaEstado = DateTime.Now;
            }

            if (!string.IsNullOrWhiteSpace(ageLicenciatariosAplicaSecuSaveDAO.Ciclica))
                ageLicenciatariosAplicaSecu.Ciclica = ageLicenciatariosAplicaSecuSaveDAO.Ciclica;

            if (!string.IsNullOrWhiteSpace(ageLicenciatariosAplicaSecuSaveDAO.Descripcion))
                ageLicenciatariosAplicaSecu.Descripcion = ageLicenciatariosAplicaSecuSaveDAO.Descripcion;

            if (!string.IsNullOrWhiteSpace(ageLicenciatariosAplicaSecuSaveDAO.ObservacionEstado))
                ageLicenciatariosAplicaSecu.ObservacionEstado = ageLicenciatariosAplicaSecuSaveDAO.ObservacionEstado;

            ageLicenciatariosAplicaSecu.FechaModificacion = DateTime.Now;
            ageLicenciatariosAplicaSecu.UbicacionModificacion = Ubicacion.getIpAdress(_httpContextAccessor);
            ageLicenciatariosAplicaSecu.UsuarioModificacion = ageLicenciatariosAplicaSecuSaveDAO.UsuarioModificacion;

            Validator.ValidateObject(ageLicenciatariosAplicaSecu, new ValidationContext(ageLicenciatariosAplicaSecu), true);

            return ageLicenciatariosAplicaSecu;
        }

        private AgeLicenciatariosAplicaSecu FromDAOToEntity(
           IHttpContextAccessor _httpContextAccessor,
           AgeLicenciatariosAplicaSecuSaveDAO ageLicenciatariosAplicaSecuSaveDAO)
        {

            return new AgeLicenciatariosAplicaSecu
            {
                AgeLicApAgeLicencCodigo = ageLicenciatariosAplicaSecuSaveDAO.Id.ageLicencCodigo,
                AgeLicApAgeAplicaCodigo = ageLicenciatariosAplicaSecuSaveDAO.Id.ageAplicaCodigo,
                Codigo = ageLicenciatariosAplicaSecuSaveDAO.Id.codigo,
                Descripcion = ageLicenciatariosAplicaSecuSaveDAO.Descripcion,
                ValorInicial = ageLicenciatariosAplicaSecuSaveDAO.ValorInicial,
                ValorActual = ageLicenciatariosAplicaSecuSaveDAO.ValorActual ?? 0L,
                IncrementaEn = ageLicenciatariosAplicaSecuSaveDAO.IncrementaEn,
                Ciclica = ageLicenciatariosAplicaSecuSaveDAO.Ciclica,
                Estado = ageLicenciatariosAplicaSecuSaveDAO.Estado,
                FechaEstado = DateTime.Now,
                FechaIngreso = DateTime.Now,
                ObservacionEstado = ageLicenciatariosAplicaSecuSaveDAO.ObservacionEstado,
                UbicacionIngreso = Ubicacion.getIpAdress(_httpContextAccessor),
                UsuarioIngreso = ageLicenciatariosAplicaSecuSaveDAO.UsuarioIngreso
            };
        }

        private void ValidarKeys(AgeLicenciatariosAplicaSecuPKDAO ageLicenciatariosAplicaSecuPKDAO)
        {
            ValidarPK(ageLicenciatariosAplicaSecuPKDAO);
            ValidarFKLicenciatarioAplicacion(ageLicenciatariosAplicaSecuPKDAO);
        }

        private void ValidarPK(AgeLicenciatariosAplicaSecuPKDAO ageLicenciatariosAplicaSecuPKDAO)
        {
            ValidateKeys.ValidarNoExistenciaKey(ageLicenciatariosAplicaSecuPKDAO,
                $"Secuencia con código: {ageLicenciatariosAplicaSecuPKDAO.codigo}, " +
                $"código licenciatario: {ageLicenciatariosAplicaSecuPKDAO.ageLicencCodigo} " +
                $"y código de aplicación: {ageLicenciatariosAplicaSecuPKDAO.ageAplicaCodigo} ya existe.",
                _repository.ConsultarPorId);
        }

        private void ValidarFKLicenciatarioAplicacion(AgeLicenciatariosAplicaSecuPKDAO ageLicenciatariosAplicaSecuPKDAO)
        {
            AgeLicenciatariosAplicacionesPKDAO ageLicenciatariosAplicacionPKDAO = new AgeLicenciatariosAplicacionesPKDAO
            {
                AgeAplicaCodigo = ageLicenciatariosAplicaSecuPKDAO.ageAplicaCodigo,
                AgeLicencCodigo = ageLicenciatariosAplicaSecuPKDAO.ageLicencCodigo
            };

            ValidateKeys.ValidarExistenciaKey(ageLicenciatariosAplicacionPKDAO,
                $"Licenciatario aplicación con código aplicación: {ageLicenciatariosAplicacionPKDAO.AgeAplicaCodigo} " +
                $"y código licenciatario: {ageLicenciatariosAplicacionPKDAO.AgeLicencCodigo} no existe.",
                _repositoryLicenciatarioAplicacion.ConsultarCompletePorId);
        }
    }
}
