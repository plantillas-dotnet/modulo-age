﻿using AGE.Entities.DAO.AgeParamGeneralVigencias;
using AGE.Entities;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;

namespace AGE.Services.AgeParamGeneralVigenciasService
{
    public interface IAgeParamGeneralVigenciasService
    {
        Task<Page<AgeParamGeneralVigenciasDAO>> ConsultarTodos(
            int codigoPG,
            int codigo, 
            string descripcion, 
            string valorParametro,
            Pageable pageable);

        Task<Page<AgeParamGeneralVigenciasDAO>> ConsultarListaFiltro(
            string filtro,
            Pageable pageable);

        Task<AgeParamGeneralVigenciasDAO> ConsultarPorId(
            int codigoPG,
            long id);

        AgeParamGeneralVigencias ConsultarCompletePorId(
            AgeParamGeneralVigenciasPKDAO ageParamGeneralVigenciasPKDAO);

        Task<Resource<AgeParamGeneralVigenciasDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgeParamGeneralVigenciasSaveDAO ageParamGeneralVigenciasSaveDAO);

        Task<List<Resource<AgeParamGeneralVigenciasDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeParamGeneralVigenciasSaveDAO> ageParamGeneralVigenciasSaveDAOList);

        Task<AgeParamGeneralVigenciasDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeParamGeneralVigenciasSaveDAO ageParamGeneralVigenciasSaveDAO);

        Task<List<AgeParamGeneralVigenciasDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeParamGeneralVigenciasSaveDAO> ageParamGeneralVigenciasSaveDAOList);
    }
}
