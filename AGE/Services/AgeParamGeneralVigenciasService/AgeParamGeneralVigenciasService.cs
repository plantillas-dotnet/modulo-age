﻿using AGE.Controllers;
using AGE.Entities;
using AGE.Entities.DAO.AgeParamGeneralVigencias;
using AGE.Middleware.Exceptions.BadRequest;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Repositories.AgeParametrosGeneralesRepository;
using AGE.Repositories.AgeParamGeneralVigenciasRepository;
using AGE.Services.AgeSecuenciasPrimariasService;
using AGE.Utils;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using System.ComponentModel.DataAnnotations;
using System.Transactions;

namespace AGE.Services.AgeParamGeneralVigenciasService
{
    public class AgeParamGeneralVigenciasService : IAgeParamGeneralVigenciasService
    {
        private readonly IAgeParamGeneralVigenciasRepository _repository;
        private readonly IAgeSecuenciasPrimariasService _ageSecuenciasPrimariasService;
        private readonly IAgeParametrosGeneralesRepository _repositoryParametroGeneral;

        public AgeParamGeneralVigenciasService(
            IAgeParamGeneralVigenciasRepository Repository,
            IAgeSecuenciasPrimariasService ageSecuenciasPrimariasService,
            IAgeParametrosGeneralesRepository repositoryParametroGeneral)
        {
            _repository = Repository;
            _ageSecuenciasPrimariasService = ageSecuenciasPrimariasService;
            _repositoryParametroGeneral = repositoryParametroGeneral;
        }


        public Task<Page<AgeParamGeneralVigenciasDAO>> ConsultarTodos(
            int codigoPG,
            int codigo,
            string descripcion,
            string valorParametro,
            Pageable pageable)
        {

            pageable.Validar<AgeParamGeneralVigenciasDAO>();

            return _repository.ConsultarTodos(codigoPG, codigo, descripcion, valorParametro, pageable);
        }


        public Task<Page<AgeParamGeneralVigenciasDAO>> ConsultarListaFiltro(
            string filtro,
            Pageable pageable)
        {

            if (string.IsNullOrWhiteSpace(filtro))
            {
                throw new InvalidFieldException("El filtro es requerido.");
            }

            pageable.Validar<AgeParamGeneralVigenciasDAO>();

            return _repository.ConsultarListaFiltro(filtro.ToLower(), pageable);
        }

        public Task<AgeParamGeneralVigenciasDAO> ConsultarPorId(
            int codigoPG,
            long id)
        {

            if (codigoPG == 0)
            {
                throw new RegisterNotFoundException("Parámetro General con código " + codigoPG + " no existe");
            }

            if (id == 0)
            {
                throw new RegisterNotFoundException("Parámetro General Vigente con código " + id + " no existe");
            }
            
            return _repository.ConsultarPorId(codigoPG, id);
        }


        public AgeParamGeneralVigencias ConsultarCompletePorId(AgeParamGeneralVigenciasPKDAO ageParamGeneralVigenciasPKDAO)
        {
            Validator.ValidateObject(ageParamGeneralVigenciasPKDAO, new ValidationContext(ageParamGeneralVigenciasPKDAO), true);

            AgeParamGeneralVigencias ageParamGeneralVigencias = _repository.ConsultarCompletePorId(ageParamGeneralVigenciasPKDAO).Result;

            return ageParamGeneralVigencias;
        }

        public async Task<Resource<AgeParamGeneralVigenciasDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgeParamGeneralVigenciasSaveDAO ageParamGeneralVigenciaSaveDAO)
        {
            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    AgeParamGeneralVigencias ageParamGeneralVigencia = ValidarInsert(_httpContextAccessor, ageParamGeneralVigenciaSaveDAO);

                    AgeParamGeneralVigenciasDAO AgeParamGeneralVigenciasDAO = await _repository.Insertar(ageParamGeneralVigencia);

                    Resource<AgeParamGeneralVigenciasDAO> AgeParamGeneralVigenciasDAOWithResource = GetDataWithResource(_httpContextAccessor, AgeParamGeneralVigenciasDAO);

                    transactionScope.Complete();

                    return AgeParamGeneralVigenciasDAOWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public async Task<List<Resource<AgeParamGeneralVigenciasDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeParamGeneralVigenciasSaveDAO> ageParamGeneralVigenciaSaveDAOList)
        {
            if (ageParamGeneralVigenciaSaveDAOList == null)
            {
                throw new InvalidSintaxisException();
            }

            List<AgeParamGeneralVigencias> ageParamGeneralVigenciaList = new List<AgeParamGeneralVigencias>();

            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    foreach (AgeParamGeneralVigenciasSaveDAO ageParamGeneralVigenciaSaveDAO in ageParamGeneralVigenciaSaveDAOList)
                    {
                        AgeParamGeneralVigencias ageMensaje = ValidarInsert(_httpContextAccessor, ageParamGeneralVigenciaSaveDAO);
                        ageParamGeneralVigenciaList.Add(ageMensaje);
                    }

                    List<AgeParamGeneralVigenciasDAO> ageMensajesDAOList = await _repository.InsertarVarios(ageParamGeneralVigenciaList);

                    List<Resource<AgeParamGeneralVigenciasDAO>> ageMensajesDAOListWithResource = new List<Resource<AgeParamGeneralVigenciasDAO>>();

                    foreach (AgeParamGeneralVigenciasDAO AgeParamGeneralVigenciasDAO in ageMensajesDAOList)
                    {
                        ageMensajesDAOListWithResource.Add(GetDataWithResource(_httpContextAccessor, AgeParamGeneralVigenciasDAO));
                    }

                    transactionScope.Complete();

                    return ageMensajesDAOListWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public Task<AgeParamGeneralVigenciasDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeParamGeneralVigenciasSaveDAO ageParamGeneralVigenciaSaveDAO)
        {
            AgeParamGeneralVigencias ageParamGeneralVigencia = ValidarUpdate(_httpContextAccessor, ageParamGeneralVigenciaSaveDAO);

            return _repository.Actualizar(ageParamGeneralVigencia);
        }

        public Task<List<AgeParamGeneralVigenciasDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeParamGeneralVigenciasSaveDAO> ageParamGeneralVigenciaSaveDAOList)
        {
            if (ageParamGeneralVigenciaSaveDAOList == null)
                throw new InvalidSintaxisException();

            ValidateKeys.ValidarPKDuplicadas(ageParamGeneralVigenciaSaveDAOList,
                                             p => new { p.Id.AgeParGeCodigo, p.Id.Codigo },
                                             "Id");

            List<AgeParamGeneralVigencias> ageParamGeneralVigenciaList = new List<AgeParamGeneralVigencias>();
            AgeParamGeneralVigencias ageParamGeneralVigencia;

            foreach (AgeParamGeneralVigenciasSaveDAO ageParamGeneralVigenciaSaveDAO in ageParamGeneralVigenciaSaveDAOList)
            {
                ageParamGeneralVigencia = ValidarUpdate(_httpContextAccessor, ageParamGeneralVigenciaSaveDAO);
                ageParamGeneralVigenciaList.Add(ageParamGeneralVigencia);
            }

            return _repository.ActualizarVarios(ageParamGeneralVigenciaList);
        }


        private static Resource<AgeParamGeneralVigenciasDAO> GetDataWithResource(
            IHttpContextAccessor _httpContextAccessor,
            AgeParamGeneralVigenciasDAO ageParamGeneralVigenciasDAO)
        {
            string rutaSegmentoFinal = $"{ageParamGeneralVigenciasDAO.Id.AgeParGeCodigo}/{ageParamGeneralVigenciasDAO.Id.Codigo}";

            return Resource<AgeParamGeneralVigenciasDAO>.GetDataWithResource<AgePGVigenciasController>(
                _httpContextAccessor, rutaSegmentoFinal, ageParamGeneralVigenciasDAO);
        }

        private AgeParamGeneralVigencias ValidarInsert(
        IHttpContextAccessor _httpContextAccessor,
        AgeParamGeneralVigenciasSaveDAO ageParamGeneralVigenciasSaveDAO)
        {

            if (ageParamGeneralVigenciasSaveDAO == null)
                throw new InvalidSintaxisException();

            Validator.ValidateObject(ageParamGeneralVigenciasSaveDAO, new ValidationContext(ageParamGeneralVigenciasSaveDAO), true);

            FuncionesSecuencias fg = new FuncionesSecuencias(_ageSecuenciasPrimariasService);

            ageParamGeneralVigenciasSaveDAO.Id.Codigo = fg.ObtenerSecuenciaPrimaria(
                                                                            _httpContextAccessor,
                                                                            Globales.CODIGO_SECUENCIA_PARAMETRO_VIGENCIA,
                                                                            ageParamGeneralVigenciasSaveDAO.UsuarioIngreso);

            ValidarKeys(ageParamGeneralVigenciasSaveDAO);

            AgeParamGeneralVigencias entityObject = FromDAOToEntity(_httpContextAccessor, ageParamGeneralVigenciasSaveDAO);

            Validator.ValidateObject(entityObject, new ValidationContext(entityObject), true);

            return entityObject;
        }


        private AgeParamGeneralVigencias ValidarUpdate(
            IHttpContextAccessor _httpContextAccessor,
            AgeParamGeneralVigenciasSaveDAO ageParamGeneralVigenciasSaveDAO)
        {
            if (ageParamGeneralVigenciasSaveDAO == null)
            {
                throw new InvalidSintaxisException();
            }

            if (ageParamGeneralVigenciasSaveDAO.UsuarioModificacion == null || ageParamGeneralVigenciasSaveDAO.UsuarioModificacion <= 0)
            {
                throw new InvalidFieldException("El usuario de modificación debe ser un número entero mayor a cero.");
            }

            AgeParamGeneralVigencias ageParamGeneralVigencias = ConsultarCompletePorId(new AgeParamGeneralVigenciasPKDAO
            {
                AgeParGeCodigo = ageParamGeneralVigenciasSaveDAO.Id.AgeParGeCodigo,
                Codigo = ageParamGeneralVigenciasSaveDAO.Id.Codigo
            });

            if (ageParamGeneralVigencias == null)
            {
                throw new RegisterNotFoundException("Parámetro General Vigente con código " + ageParamGeneralVigenciasSaveDAO.Id.Codigo + " no existe");
            }

            if (!string.IsNullOrWhiteSpace(ageParamGeneralVigenciasSaveDAO.Estado))
            {
                ageParamGeneralVigencias.Estado = ageParamGeneralVigenciasSaveDAO.Estado;
                ageParamGeneralVigencias.FechaEstado = DateTime.Now;
            }

            if (!string.IsNullOrWhiteSpace(ageParamGeneralVigenciasSaveDAO.Observacion))
                ageParamGeneralVigencias.Observacion = ageParamGeneralVigenciasSaveDAO.Observacion;

            if (!string.IsNullOrWhiteSpace(ageParamGeneralVigenciasSaveDAO.ValorParametro))
                ageParamGeneralVigencias.ValorParametro = 
                    ageParamGeneralVigenciasSaveDAO.ValorParametro;

            ageParamGeneralVigencias.FechaModificacion = DateTime.Now;
            ageParamGeneralVigencias.UbicacionModificacion = Ubicacion.getIpAdress(_httpContextAccessor);
            ageParamGeneralVigencias.UsuarioModificacion = ageParamGeneralVigenciasSaveDAO.UsuarioModificacion;

            Validator.ValidateObject(ageParamGeneralVigencias, new ValidationContext(ageParamGeneralVigencias), true);

            return ageParamGeneralVigencias;
        }

        private AgeParamGeneralVigencias FromDAOToEntity(
        IHttpContextAccessor _httpContextAccessor,
        AgeParamGeneralVigenciasSaveDAO ageParamGeneralVigenciasSaveDAO)
        {

            return new AgeParamGeneralVigencias
            {
                AgeParGeCodigo = ageParamGeneralVigenciasSaveDAO.Id.AgeParGeCodigo,
                Codigo = ageParamGeneralVigenciasSaveDAO.Id.Codigo,
                Observacion = ageParamGeneralVigenciasSaveDAO.Observacion,
                FechaDesde = ageParamGeneralVigenciasSaveDAO.FechaDesde,
                FechaHasta = ageParamGeneralVigenciasSaveDAO.FechaHasta,
                ValorParametro = ageParamGeneralVigenciasSaveDAO.ValorParametro,
                Estado = ageParamGeneralVigenciasSaveDAO.Estado,
                FechaEstado = DateTime.Now,
                FechaIngreso = DateTime.Now,
                ObservacionEstado = ageParamGeneralVigenciasSaveDAO.ObservacionEstado,
                UbicacionIngreso = Ubicacion.getIpAdress(_httpContextAccessor),
                UsuarioIngreso = ageParamGeneralVigenciasSaveDAO.UsuarioIngreso
            };
        }

        private void ValidarKeys(AgeParamGeneralVigenciasSaveDAO ageParamGeneralVigenciaSaveDAO)
        {
            ValidarPK(ageParamGeneralVigenciaSaveDAO.Id);
            ValidarFKParametroGeneral(ageParamGeneralVigenciaSaveDAO.Id.AgeParGeCodigo);
        }

        private void ValidarPK(AgeParamGeneralVigenciasPKDAO ageParamGeneralVigenciasPKDAO)
        {

            ValidateKeys.ValidarNoExistenciaKey(
               ageParamGeneralVigenciasPKDAO,
               $"Parámetro General Vigente con código {ageParamGeneralVigenciasPKDAO.Codigo} ya existe.",
               _repository.ConsultarCompletePorId);
        }

        private void ValidarFKParametroGeneral(int codigoParametroGeneral)
        {
            ValidateKeys.ValidarExistenciaKey(codigoParametroGeneral,
                $"Parámetro general con código: {codigoParametroGeneral} no existe.",
                _repositoryParametroGeneral.ConsultarCompletePorId);
        }

    }
}
