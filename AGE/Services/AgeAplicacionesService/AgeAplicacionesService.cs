﻿using AGE.Entities.DAO.AgeAplicaciones;
using AGE.Entities;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Utils.Paginacion;
using AGE.Utils;
using AGE.Middleware.Exceptions.BadRequest;
using AGE.Repositories.AgeAplicacionesRepository;
using AGE.Services.AgeSecuenciasPrimariasService;
using AGE.Utils.WebLink;
using System.ComponentModel.DataAnnotations;
using System.Transactions;
using AGE.Controllers;
namespace AGE.Services.AgeAplicacionesService
{
    public class AgeAplicacionesService : IAgeAplicacionesService
    {
        private readonly IAgeAplicacionesRepository _repository;
        private readonly IAgeSecuenciasPrimariasService _ageSecuenciasPrimariasService;


        public AgeAplicacionesService(
            IAgeAplicacionesRepository Repository,
            IAgeSecuenciasPrimariasService ageSecuenciasPrimariasService)
        {
            _repository = Repository;
            _ageSecuenciasPrimariasService = ageSecuenciasPrimariasService;
        }


        public Task<Page<AgeAplicacionesDAO>> ConsultarTodos(
            string codigoExterno,
            string descripcion,
            Pageable pageable)
        {
            pageable.Validar<AgeAplicacionesDAO>();

            return _repository.ConsultarTodos(codigoExterno, descripcion, pageable);
        }


        public Task<Page<AgeAplicacionesDAO>> ConsultarListaFiltro(string filtro, Pageable pageable)
        {

            if (string.IsNullOrWhiteSpace(filtro))
                throw new InvalidFieldException("El filtro es requerido.");

            pageable.Validar<AgeAplicacionesDAO>();

            return _repository.ConsultarListaFiltro(filtro, pageable);

        }

        public Task<AgeAplicacionesDAO> ConsultarPorId(
            int codigo)
        {

            if (codigo <= 0)
                throw new InvalidIdException();

            AgeAplicacionesDAO? ageAplicacionesDAO = _repository.ConsultarPorId(codigo).Result;

            if (ageAplicacionesDAO == null)
                throw new RegisterNotFoundException("Aplicación con código " + codigo + " no existe");

            return Task.FromResult(ageAplicacionesDAO);
        }


        private AgeAplicaciones ConsultarCompletePorId(
            int codigo)
        {

            if (codigo <= 0)
                throw new InvalidIdException();

            return _repository.ConsultarCompletePorId(codigo).Result;
        }


        public async Task<Resource<AgeAplicacionesDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgeAplicacionesSaveDAO ageAplicacionesSaveDAO)
        {
            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    AgeAplicaciones ageAplicaciones = ValidarInsert(_httpContextAccessor, ageAplicacionesSaveDAO);

                    AgeAplicacionesDAO ageAplicacionesDAO = await _repository.Insertar(ageAplicaciones);

                    Resource<AgeAplicacionesDAO> ageAplicacionesDAOWithResource = GetDataWithResource(_httpContextAccessor, ageAplicacionesDAO);

                    transactionScope.Complete();

                    return ageAplicacionesDAOWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }


        public Task<AgeAplicacionesDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeAplicacionesSaveDAO ageAplicacionesSaveDAO)
        {

            AgeAplicaciones ageAplicaciones = ValidarUpdate(_httpContextAccessor, ageAplicacionesSaveDAO);

            return _repository.Actualizar(ageAplicaciones);
        }


        public async Task<List<Resource<AgeAplicacionesDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeAplicacionesSaveDAO> ageAplicacionesSaveDAOList)
        {
            if (ageAplicacionesSaveDAOList == null)
                throw new InvalidSintaxisException();

            List<AgeAplicaciones> ageAplicacionesList = new List<AgeAplicaciones>();

            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    foreach (AgeAplicacionesSaveDAO ageAplicacionesSaveDAO in ageAplicacionesSaveDAOList)
                    {
                        AgeAplicaciones AgeAplicacione = ValidarInsert(_httpContextAccessor, ageAplicacionesSaveDAO);
                        ageAplicacionesList.Add(AgeAplicacione);
                    }

                    List<AgeAplicacionesDAO> ageAplicacionesDAOList = await _repository.InsertarVarios(ageAplicacionesList);

                    List<Resource<AgeAplicacionesDAO>> ageAplicacionesDAOListWithResource = new List<Resource<AgeAplicacionesDAO>>();

                    foreach (AgeAplicacionesDAO ageAplicacionesDAO in ageAplicacionesDAOList)
                    {
                        ageAplicacionesDAOListWithResource.Add(GetDataWithResource(_httpContextAccessor, ageAplicacionesDAO));
                    }

                    transactionScope.Complete();

                    return ageAplicacionesDAOListWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }


        public Task<List<AgeAplicacionesDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeAplicacionesSaveDAO> ageAplicacionesSaveDAOList)
        {
            if (ageAplicacionesSaveDAOList == null)
                throw new InvalidSintaxisException();

            ValidateKeys.ValidarPKDuplicadas(ageAplicacionesSaveDAOList,
                                                p => new { p.Codigo },
                                                "Código");

            List<AgeAplicaciones> ageAplicacionesList = new List<AgeAplicaciones>();
            AgeAplicaciones ageAplicaciones;

            foreach (AgeAplicacionesSaveDAO ageRutasSaveDAO in ageAplicacionesSaveDAOList)
            {
                ageAplicaciones = ValidarUpdate(_httpContextAccessor, ageRutasSaveDAO);
                ageAplicacionesList.Add(ageAplicaciones);
            }

            return _repository.ActualizarVarios(ageAplicacionesList);
        }


        private static Resource<AgeAplicacionesDAO> GetDataWithResource(
            IHttpContextAccessor _httpContextAccessor,
            AgeAplicacionesDAO ageAplicacionesDAO)
        {
            string rutaSegmentoFinal = $"{ageAplicacionesDAO.Codigo}";

            return Resource<AgeAplicacionesDAO>.GetDataWithResource<AgeAplicacionesController>(
                _httpContextAccessor, rutaSegmentoFinal, ageAplicacionesDAO);
        }


        private AgeAplicaciones ValidarInsert(
            IHttpContextAccessor _httpContextAccessor,
            AgeAplicacionesSaveDAO ageAplicacionesSaveDAO)
        {
            if (ageAplicacionesSaveDAO == null)
                throw new InvalidSintaxisException();

            Validator.ValidateObject(ageAplicacionesSaveDAO, new ValidationContext(ageAplicacionesSaveDAO), true);

            FuncionesSecuencias fg = new FuncionesSecuencias(_ageSecuenciasPrimariasService);

            ageAplicacionesSaveDAO.Codigo = fg.ObtenerSecuenciaPrimaria(
                                            _httpContextAccessor,
                                            Globales.CODIGO_SECUENCIA_APLICACION,
                                            ageAplicacionesSaveDAO.UsuarioIngreso); 

            ValidarPK(ageAplicacionesSaveDAO.Codigo);

            AgeAplicaciones entityObject = FromDAOToEntity(_httpContextAccessor, ageAplicacionesSaveDAO);

            Validator.ValidateObject(entityObject, new ValidationContext(entityObject), true);

            return entityObject;
        }


        private AgeAplicaciones ValidarUpdate(
            IHttpContextAccessor _httpContextAccessor,
            AgeAplicacionesSaveDAO ageAplicacionesSaveDAO)
        {
            if (ageAplicacionesSaveDAO == null)
                throw new InvalidSintaxisException();

            if (ageAplicacionesSaveDAO.UsuarioModificacion == null || ageAplicacionesSaveDAO.UsuarioModificacion <= 0)
                throw new InvalidFieldException("El usuario de modificación debe ser un número entero mayor a cero.");

            AgeAplicaciones ageAplicaciones = ConsultarCompletePorId(ageAplicacionesSaveDAO.Codigo) 
                ?? throw new RegisterNotFoundException($"Aplicación con código {ageAplicacionesSaveDAO.Codigo} no existe.");

            if (!string.IsNullOrWhiteSpace(ageAplicacionesSaveDAO.Estado))
            {
                ageAplicaciones.Estado = ageAplicacionesSaveDAO.Estado;
                ageAplicaciones.FechaEstado = DateTime.Now;
            }

            if (!string.IsNullOrWhiteSpace(ageAplicacionesSaveDAO.Descripcion))
                ageAplicaciones.Descripcion = ageAplicacionesSaveDAO.Descripcion;

            if (!string.IsNullOrWhiteSpace(ageAplicacionesSaveDAO.ObservacionEstado))
                ageAplicaciones.ObservacionEstado = ageAplicacionesSaveDAO.ObservacionEstado;

            if (!string.IsNullOrWhiteSpace(ageAplicacionesSaveDAO.CodigoExterno))
                ageAplicaciones.CodigoExterno = ageAplicacionesSaveDAO.CodigoExterno;

            if (ageAplicacionesSaveDAO.InicioSecuTrxCe > 0)
                ageAplicaciones.InicioSecuTrxCe = ageAplicacionesSaveDAO.InicioSecuTrxCe;

            if (ageAplicacionesSaveDAO.IncrementoSecuTrxCe > 0)
                ageAplicaciones.IncrementoSecuTrxCe = ageAplicacionesSaveDAO.IncrementoSecuTrxCe;

            if (ageAplicacionesSaveDAO.ValorActualSecuTrxCe > 0)
                ageAplicaciones.ValorActualSecuTrxCe = ageAplicacionesSaveDAO.ValorActualSecuTrxCe;

            if (!string.IsNullOrWhiteSpace(ageAplicacionesSaveDAO.CiclicaSecuTrxCe))
                ageAplicaciones.CiclicaSecuTrxCe = ageAplicacionesSaveDAO.CiclicaSecuTrxCe;

            if (ageAplicacionesSaveDAO.InicioSecuTrxCi > 0)
                ageAplicaciones.InicioSecuTrxCi = ageAplicacionesSaveDAO.InicioSecuTrxCi;

            if (ageAplicacionesSaveDAO.ValorActualSecuTrxCi > 0)
                ageAplicaciones.ValorActualSecuTrxCi = ageAplicacionesSaveDAO.ValorActualSecuTrxCi;

            if (!string.IsNullOrWhiteSpace(ageAplicacionesSaveDAO.CiclicaSecuTrxCi))
                ageAplicaciones.CiclicaSecuTrxCi = ageAplicacionesSaveDAO.CiclicaSecuTrxCi;

            if (ageAplicacionesSaveDAO.OrdenInstalacion > 0)
                ageAplicaciones.OrdenInstalacion = ageAplicacionesSaveDAO.OrdenInstalacion;

            ageAplicaciones.FechaModificacion = DateTime.Now;
            ageAplicaciones.UbicacionModificacion = Ubicacion.getIpAdress(_httpContextAccessor);
            ageAplicaciones.UsuarioModificacion = ageAplicacionesSaveDAO.UsuarioModificacion;

            Validator.ValidateObject(ageAplicaciones, new ValidationContext(ageAplicaciones), true);

            return ageAplicaciones;
        }

        private void ValidarPK(int codigoAplicacion)
        {
            ValidateKeys.ValidarNoExistenciaKey(
               codigoAplicacion,
               $"Aplicación con código: {codigoAplicacion} ya existe.",
               _repository.ConsultarCompletePorId);
        }

        private AgeAplicaciones FromDAOToEntity(
            IHttpContextAccessor _httpContextAccessor, 
            AgeAplicacionesSaveDAO ageAplicacionesSaveDAO)
        {
            return new AgeAplicaciones
            {
                Codigo = ageAplicacionesSaveDAO.Codigo,
                Descripcion = ageAplicacionesSaveDAO.Descripcion,
                CodigoExterno = ageAplicacionesSaveDAO.CodigoExterno,
                Estado = ageAplicacionesSaveDAO.Estado.ToUpper().ToString(),
                FechaEstado = DateTime.Now,
                FechaIngreso = DateTime.Now,
                ObservacionEstado = ageAplicacionesSaveDAO.ObservacionEstado,
                UsuarioIngreso = ageAplicacionesSaveDAO.UsuarioIngreso,
                UbicacionIngreso = Ubicacion.getIpAdress(_httpContextAccessor),
                InicioSecuTrxCe = ageAplicacionesSaveDAO.InicioSecuTrxCe,
                IncrementoSecuTrxCe = ageAplicacionesSaveDAO.IncrementoSecuTrxCe,
                ValorActualSecuTrxCe = ageAplicacionesSaveDAO.ValorActualSecuTrxCe,
                CiclicaSecuTrxCe = ageAplicacionesSaveDAO.CiclicaSecuTrxCe,
                InicioSecuTrxCi = ageAplicacionesSaveDAO.InicioSecuTrxCi,
                ValorActualSecuTrxCi = ageAplicacionesSaveDAO.ValorActualSecuTrxCi,
                CiclicaSecuTrxCi = ageAplicacionesSaveDAO.CiclicaSecuTrxCi,
                OrdenInstalacion = ageAplicacionesSaveDAO.OrdenInstalacion
            };
        }
    }
}
