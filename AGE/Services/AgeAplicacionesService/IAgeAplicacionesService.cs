﻿using AGE.Entities.DAO.AgeAplicaciones;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;

namespace AGE.Services.AgeAplicacionesService
{
    public interface IAgeAplicacionesService
    {
        Task<Page<AgeAplicacionesDAO>> ConsultarTodos(
            string codigoExterno,
            string descripcion,
            Pageable pageable);

        Task<Page<AgeAplicacionesDAO>> ConsultarListaFiltro(
            string filtro,
            Pageable pageable);

        Task<AgeAplicacionesDAO> ConsultarPorId(int Codigo);

        Task<Resource<AgeAplicacionesDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgeAplicacionesSaveDAO AgeAplicacionesSaveDAO);

        Task<List<Resource<AgeAplicacionesDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeAplicacionesSaveDAO> AgeAplicacionesSaveDAOList);

        Task<AgeAplicacionesDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeAplicacionesSaveDAO AgeAplicacionesSaveDAO);

        Task<List<AgeAplicacionesDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeAplicacionesSaveDAO> AgeAplicacionesSaveDAOList);
    }
}
