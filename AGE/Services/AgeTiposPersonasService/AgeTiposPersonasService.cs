﻿using AGE.Controllers;
using AGE.Entities;
using AGE.Entities.DAO.AgeTiposPersonas;
using AGE.Repositories.AgeTiposPersonaRepository;
using AGE.Services.AgeLicenciatariosAplicaSecuService;
using AGE.Middleware.Exceptions.BadRequest;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Utils;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using System.ComponentModel.DataAnnotations;
using System.Transactions;

namespace AGE.Services.AgeTiposPersonaService
{
    public class AgeTiposPersonasService : IAgeTiposPersonasService
    {
        private readonly IAgeTiposPersonasRepository _repository;

        private readonly IAgeLicenciatariosAplicaSecuService _ageLicenciatariosAplicaSecuService;


        public AgeTiposPersonasService(
            IAgeTiposPersonasRepository Repository,
            IAgeLicenciatariosAplicaSecuService ageLicenciatariosAplicaSecuService)
        {
            _repository = Repository;
            _ageLicenciatariosAplicaSecuService = ageLicenciatariosAplicaSecuService;
        }


        public Task<Page<AgeTiposPersonaDAO>> ConsultarTodos(
            int codigoLicenciatario,
            int codigo, string codigoExterno,
            string descripcion,
            Pageable pageable)
        {
            if (codigoLicenciatario <= 0)
                throw new InvalidFieldException("El código de licenciatario debe ser un número entero mayor a cero.");

            pageable.Validar<AgeTiposPersonaDAO>();

            return _repository.ConsultarTodos(codigoLicenciatario, codigo, codigoExterno, descripcion, pageable);
        }


        public Task<Page<AgeTiposPersonaDAO>> ConsultarListaFiltro(
            int codigoLicenciatario,
            string filtro,
            Pageable pageable)
        {
            if (codigoLicenciatario == 0)
                throw new InvalidFieldException("El código de licenciatario debe ser un número entero mayor a cero.");

            if (string.IsNullOrWhiteSpace(filtro))
                throw new InvalidFieldException("El filtro es requerido.");

            pageable.Validar<AgeTiposPersonaDAO>();

            return _repository.ConsultarListaFiltro(codigoLicenciatario, filtro.ToLower(), pageable);
        }


        public Task<AgeTiposPersonaDAO> ConsultarPorId(
            AgeTiposPersonaPKDAO ageTiposPersonaPKDAO)
        {
            Validator.ValidateObject(ageTiposPersonaPKDAO, new ValidationContext(ageTiposPersonaPKDAO), true);

            AgeTiposPersonaDAO ageTiposPersonaDAO = _repository.ConsultarPorId(ageTiposPersonaPKDAO).Result;

            if (ageTiposPersonaDAO == null)
                throw new RegisterNotFoundException("Tipo de Persona con código " + ageTiposPersonaPKDAO.codigo + " no existe");

            return Task.FromResult(ageTiposPersonaDAO);
        }


        private AgeTiposPersonas ConsultarCompletePorId(
            AgeTiposPersonaPKDAO ageTiposPersonaPKDAO)
        {
            Validator.ValidateObject(ageTiposPersonaPKDAO, new ValidationContext(ageTiposPersonaPKDAO), true);

            AgeTiposPersonas? ageTiposPersona = _repository.ConsultarCompletePorId(ageTiposPersonaPKDAO).Result;

            return ageTiposPersona;
        }

        public async Task<Resource<AgeTiposPersonaDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgeTiposPersonaSaveDAO ageTiposPersonaSaveDAO)
        {
            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    AgeTiposPersonas ageTiposPersona = ValidarInsert(_httpContextAccessor, ageTiposPersonaSaveDAO);

                    AgeTiposPersonaDAO ageTiposPersonaDAO = await _repository.Insertar(ageTiposPersona);

                    Resource<AgeTiposPersonaDAO> ageTiposPersonaDAOWithResource = GetDataWithResource(_httpContextAccessor, ageTiposPersonaDAO);

                    transactionScope.Complete();

                    return ageTiposPersonaDAOWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public async Task<List<Resource<AgeTiposPersonaDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeTiposPersonaSaveDAO> ageTiposPersonaSaveDAOList)
        {
            if (ageTiposPersonaSaveDAOList == null)
            {
                throw new InvalidSintaxisException();
            }

            List<AgeTiposPersonas> ageTiposPersonaList = new List<AgeTiposPersonas>();

            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    foreach (AgeTiposPersonaSaveDAO ageTiposPersonaSaveDAO in ageTiposPersonaSaveDAOList)
                    {
                        AgeTiposPersonas ageTiposPersona = ValidarInsert(_httpContextAccessor, ageTiposPersonaSaveDAO);
                        ageTiposPersonaList.Add(ageTiposPersona);
                    }

                    List<AgeTiposPersonaDAO> ageTiposPersonaDAOList = await _repository.InsertarVarios(ageTiposPersonaList);

                    List<Resource<AgeTiposPersonaDAO>> ageTiposPersonaDAOListWithResource = new List<Resource<AgeTiposPersonaDAO>>();

                    foreach (AgeTiposPersonaDAO ageTiposPersonaDAO in ageTiposPersonaDAOList)
                    {
                        ageTiposPersonaDAOListWithResource.Add(GetDataWithResource(_httpContextAccessor, ageTiposPersonaDAO));
                    }

                    transactionScope.Complete();

                    return ageTiposPersonaDAOListWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public Task<AgeTiposPersonaDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeTiposPersonaSaveDAO ageTiposPersonaSaveDAO)
        {
            AgeTiposPersonas ageTiposPersona = ValidarUpdate(_httpContextAccessor, ageTiposPersonaSaveDAO);

            return _repository.Actualizar(ageTiposPersona);
        }

        public Task<List<AgeTiposPersonaDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeTiposPersonaSaveDAO> ageTiposPersonaSaveDAOList)
        {
            if (ageTiposPersonaSaveDAOList == null)
                throw new InvalidSintaxisException();

            ValidateKeys.ValidarPKDuplicadas(ageTiposPersonaSaveDAOList,
                                             p => new { p.Id.ageLicencCodigo, p.Id.codigo },
                                             "Id");

            List<AgeTiposPersonas> ageTiposPersonaList = new List<AgeTiposPersonas>();
            AgeTiposPersonas ageTiposPersona;

            foreach (AgeTiposPersonaSaveDAO ageTiposPersonaSaveDAO in ageTiposPersonaSaveDAOList)
            {
                ageTiposPersona = ValidarUpdate(_httpContextAccessor, ageTiposPersonaSaveDAO);
                ageTiposPersonaList.Add(ageTiposPersona);
            }

            return _repository.ActualizarVarios(ageTiposPersonaList);
        }

        private static Resource<AgeTiposPersonaDAO> GetDataWithResource(
            IHttpContextAccessor _httpContextAccessor,
            AgeTiposPersonaDAO ageTiposPersonaDAO)
        {
            string tpersonaSegmentoFinal = $"{ageTiposPersonaDAO.Id.ageLicencCodigo}/{ageTiposPersonaDAO.Id.codigo}";

            return Resource<AgeTiposPersonaDAO>.GetDataWithResource<AgeTiposPersonasController>(
                _httpContextAccessor, tpersonaSegmentoFinal, ageTiposPersonaDAO);
        }

        private AgeTiposPersonas ValidarInsert(
            IHttpContextAccessor _httpContextAccessor,
            AgeTiposPersonaSaveDAO ageTiposPersonaSaveDAO)
        {

            if (ageTiposPersonaSaveDAO == null)
                throw new InvalidSintaxisException();

            Validator.ValidateObject(ageTiposPersonaSaveDAO, new ValidationContext(ageTiposPersonaSaveDAO), true);

            FuncionesSecuencias fg = new FuncionesSecuencias(_ageLicenciatariosAplicaSecuService);

            ageTiposPersonaSaveDAO.Id.codigo = fg.ObtenerSecuencia(_httpContextAccessor, 
                                                   ageTiposPersonaSaveDAO.Id.ageLicencCodigo,
                                                   Globales.CODIGO_SECUENCIA_TIPO_PERSONA,
            ageTiposPersonaSaveDAO.UsuarioIngreso);

            ValidarPK(ageTiposPersonaSaveDAO.Id);

            AgeTiposPersonas entityObject = FromDAOToEntity(_httpContextAccessor, ageTiposPersonaSaveDAO); 
            
            Validator.ValidateObject(entityObject, new ValidationContext(entityObject), true);

            return entityObject;
        }


        private AgeTiposPersonas ValidarUpdate(
            IHttpContextAccessor _httpContextAccessor,
            AgeTiposPersonaSaveDAO ageTiposPersonaSaveDAO)
        {
            if (ageTiposPersonaSaveDAO == null)
                throw new InvalidSintaxisException();

            if (ageTiposPersonaSaveDAO.UsuarioModificacion == null || ageTiposPersonaSaveDAO.UsuarioModificacion <= 0)
                throw new InvalidFieldException("El usuario de modificación debe ser un número entero mayor a cero.");

            AgeTiposPersonas? ageTiposPersona = ConsultarCompletePorId(new AgeTiposPersonaPKDAO
            {
                ageLicencCodigo = ageTiposPersonaSaveDAO.Id.ageLicencCodigo,
                codigo = ageTiposPersonaSaveDAO.Id.codigo
            });

            if (ageTiposPersona == null)
                throw new RegisterNotFoundException("Tipo de Persona con código " + ageTiposPersonaSaveDAO.Id.codigo + " no existe");

            if (!string.IsNullOrWhiteSpace(ageTiposPersonaSaveDAO.Estado))
            {
                ageTiposPersona.Estado = ageTiposPersonaSaveDAO.Estado;
                ageTiposPersona.FechaEstado = DateTime.Now;
            }

            if (!string.IsNullOrWhiteSpace(ageTiposPersonaSaveDAO.codigoExterno))
                ageTiposPersona.CodigoExterno = ageTiposPersonaSaveDAO.codigoExterno;

            if (!string.IsNullOrWhiteSpace(ageTiposPersonaSaveDAO.Descripcion))
                ageTiposPersona.Descripcion = ageTiposPersonaSaveDAO.Descripcion;

            if (!string.IsNullOrWhiteSpace(ageTiposPersonaSaveDAO.ObservacionEstado))
                ageTiposPersona.ObservacionEstado = ageTiposPersonaSaveDAO.ObservacionEstado;

            ageTiposPersona.FechaModificacion = DateTime.Now;
            ageTiposPersona.UbicacionModificacion = Ubicacion.getIpAdress(_httpContextAccessor);
            ageTiposPersona.UsuarioModificacion = ageTiposPersonaSaveDAO.UsuarioModificacion;

            Validator.ValidateObject(ageTiposPersona, new ValidationContext(ageTiposPersona), true);

            return ageTiposPersona;
        }

        private void ValidarPK(AgeTiposPersonaPKDAO ageTiposPersonaPKDAO)
        {

            ValidateKeys.ValidarNoExistenciaKey(
               ageTiposPersonaPKDAO,
               $"Tipo de Persona con código {ageTiposPersonaPKDAO.codigo} ya existe.",
               _repository.ConsultarCompletePorId);
        }


        private AgeTiposPersonas FromDAOToEntity(
            IHttpContextAccessor _httpContextAccessor, 
            AgeTiposPersonaSaveDAO ageTiposPersonaSaveDAO)
        {
            return new AgeTiposPersonas
            {
                AgeLicencCodigo = ageTiposPersonaSaveDAO.Id.ageLicencCodigo,
                Codigo = ageTiposPersonaSaveDAO.Id.codigo,
                Descripcion = ageTiposPersonaSaveDAO.Descripcion,
                CodigoExterno = ageTiposPersonaSaveDAO.codigoExterno,
                Estado = ageTiposPersonaSaveDAO.Estado,
                FechaEstado = DateTime.Now,
                FechaIngreso = DateTime.Now,
                ObservacionEstado = ageTiposPersonaSaveDAO.ObservacionEstado,
                UbicacionIngreso = Ubicacion.getIpAdress(_httpContextAccessor),
                UsuarioIngreso = ageTiposPersonaSaveDAO.UsuarioIngreso
            };
        }


    }
}
