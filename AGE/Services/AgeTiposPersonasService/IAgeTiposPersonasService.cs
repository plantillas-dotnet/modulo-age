﻿using AGE.Entities.DAO.AgeTiposPersonas;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;

namespace AGE.Services.AgeTiposPersonaService
{
    public interface IAgeTiposPersonasService
    {
        Task<Page<AgeTiposPersonaDAO>> ConsultarTodos(
        int codigoLicenciatario,
        int codigo,
        string codigoExterno,
        string descripcion,
        Pageable pageable);

        Task<Page<AgeTiposPersonaDAO>> ConsultarListaFiltro(
            int codigoLicenciatario,
            string filtro,
            Pageable pageable);

        Task<AgeTiposPersonaDAO> ConsultarPorId(
            AgeTiposPersonaPKDAO AgeTiposPersonaPKDAO);

        Task<Resource<AgeTiposPersonaDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgeTiposPersonaSaveDAO AgeTiposPersonaSaveDAO);

        Task<List<Resource<AgeTiposPersonaDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeTiposPersonaSaveDAO> AgeTiposPersonaSaveDAOList);

        Task<AgeTiposPersonaDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeTiposPersonaSaveDAO AgeTiposPersonaSaveDAO);

        Task<List<AgeTiposPersonaDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeTiposPersonaSaveDAO> AgeTiposPersonaSaveDAOList);
    }
}
