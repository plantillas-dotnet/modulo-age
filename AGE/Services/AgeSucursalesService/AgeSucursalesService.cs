﻿using AGE.Controllers;
using AGE.Entities.DAO.AgeSucursales;
using AGE.Entities;
using AGE.Middleware.Exceptions.BadRequest;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Repositories.AgeSucursalesRepository;
using AGE.Repositories.AgeLicenciatariosRepository;
using AGE.Services.AgeLicenciatariosAplicaSecuService;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using AGE.Utils;
using System.ComponentModel.DataAnnotations;
using System.Transactions;
using AGE.Repositories.AgeLocalidadesRepository;
using AGE.Repositories.AgeTiposSucursaleRepository;
using AGE.Entities.DAO.AgeLocalidades;
using AGE.Entities.DAO.AgeTiposSucursales;

namespace AGE.Services.AgeSucursalesService
{
    public class AgeSucursalesService : IAgeSucursalesService
    {
        private readonly IAgeSucursalesRepository _repository;
        private readonly IAgeLicenciatariosRepository _repositoryLicenciatarios; 
        private readonly IAgeLocalidadesRepository _repositoryLocalidad;
        private readonly IAgeTiposSucursalesRepository _repositoryTipoSucursal;
        private readonly IAgeLicenciatariosAplicaSecuService _ageLicenciatariosAplicaSecuService;

        public AgeSucursalesService(IAgeSucursalesRepository Repository,
            IAgeLicenciatariosRepository RepositoryLicenciatarios, 
            IAgeLocalidadesRepository RepositoryLocalidad,
            IAgeTiposSucursalesRepository RepositoryTipoSucursal,
            IAgeLicenciatariosAplicaSecuService ageLicenciatariosAplicaSecuService)
        {
            _repository = Repository;
            _repositoryLicenciatarios = RepositoryLicenciatarios;
            _repositoryLocalidad = RepositoryLocalidad;
            _repositoryTipoSucursal = RepositoryTipoSucursal;
            _ageLicenciatariosAplicaSecuService = ageLicenciatariosAplicaSecuService;
        }

        public Task<Page<AgeSucursalesDAO>> ConsultarTodos(int codigoLicenciatario,
                                                            int? codigo,
                                                            string? descripcion,
                                                            string? direccion,
                                                            Pageable pageable)
        {
            pageable.Validar<AgeSucursalesDAO>();

            return _repository.ConsultarTodos(codigoLicenciatario,codigo,descripcion,direccion, pageable);
        }

        public Task<AgeSucursalesDAO> ConsultarPorId(
            AgeSucursalesPKDAO ageSucursalesPKDAO)
        {

            Validator.ValidateObject(ageSucursalesPKDAO, new ValidationContext(ageSucursalesPKDAO), true);

            AgeSucursalesDAO ageSucursalesDAO = _repository.ConsultarPorId(ageSucursalesPKDAO).Result;

            return ageSucursalesDAO == null
                ? throw new RegisterNotFoundException("Sucursal con código: " + ageSucursalesPKDAO.Codigo + " no existe.")
                : Task.FromResult(ageSucursalesDAO);
        }

        private AgeSucursales ConsultarCompletePorId(AgeSucursalesPKDAO ageSucursalesPKDAO)
        {
            Validator.ValidateObject(ageSucursalesPKDAO, new ValidationContext(ageSucursalesPKDAO), true);

            AgeSucursales? ageSucursale = _repository.ConsultarCompletePorId(ageSucursalesPKDAO).Result;

            return ageSucursale;
        }

        public Task<Page<AgeSucursalesDAO>> ConsultarListaFiltro(int codigoLicenciatario,
                                                                    string filtro,
                                                                    Pageable pageable)
        {
            if (codigoLicenciatario == 0)
                throw new InvalidFieldException("El código de licenciatario debe ser un número entero mayor a cero.");

            if (string.IsNullOrWhiteSpace(filtro))
                throw new InvalidFieldException("El filtro es requerido.");

            pageable.Validar<AgeSucursalesDAO>();

            return _repository.ConsultarListaFiltro(codigoLicenciatario, filtro.ToLower(), pageable);
        }

        public async Task<Resource<AgeSucursalesDAO>> Insertar(IHttpContextAccessor _httpContextAccessor, AgeSucursalesSaveDAO AgeSucursalesSaveDAO)
        {

            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    AgeSucursales ageSucursale = ValidarInsert(_httpContextAccessor, AgeSucursalesSaveDAO);
                    AgeSucursalesDAO ageSucursalesDAO = await _repository.Insertar(ageSucursale);

                    Resource<AgeSucursalesDAO> ageSucursaleDAOWithResource = GetDataWithResource(_httpContextAccessor, ageSucursalesDAO);

                    transactionScope.Complete();

                    return ageSucursaleDAOWithResource;
                }
                catch (Exception)
                { 
                    throw;
                }
            }
        }

        public async Task<List<Resource<AgeSucursalesDAO>>> InsertarVarios(IHttpContextAccessor _httpContextAccessor, List<AgeSucursalesSaveDAO> AgeSucursalesSaveDAOList)
        {
            if (AgeSucursalesSaveDAOList == null)
                throw new InvalidSintaxisException();

            List<AgeSucursales> ageSucursalesList = new();

            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    foreach (AgeSucursalesSaveDAO ageSucursalesSaveDAO in AgeSucursalesSaveDAOList)
                    {
                        AgeSucursales ageSucursale = ValidarInsert(_httpContextAccessor, ageSucursalesSaveDAO);
                        ageSucursalesList.Add(ageSucursale);
                    }

                    List<AgeSucursalesDAO> ageSucursalesDAOList = await _repository.InsertarVarios(ageSucursalesList);

                    List<Resource<AgeSucursalesDAO>> ageSucursalesDAOListWithResource = new();

                    foreach (AgeSucursalesDAO ageSucursaleDAO in ageSucursalesDAOList)
                    {
                        ageSucursalesDAOListWithResource.Add(GetDataWithResource(_httpContextAccessor, ageSucursaleDAO));
                    }

                    transactionScope.Complete();

                    return ageSucursalesDAOListWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public Task<AgeSucursalesDAO> Actualizar(IHttpContextAccessor _httpContextAccessor, AgeSucursalesSaveDAO AgeSucursalesSaveDAO)
        {
            AgeSucursales ageSucursale = ValidarUpdate(_httpContextAccessor, AgeSucursalesSaveDAO);

            return _repository.Actualizar(ageSucursale);
        }

        public Task<List<AgeSucursalesDAO>> ActualizarVarios(IHttpContextAccessor _httpContextAccessor, List<AgeSucursalesSaveDAO> AgeSucursalesSaveDAOList)
        {
            if (AgeSucursalesSaveDAOList == null)
                throw new InvalidSintaxisException();

            ValidateKeys.ValidarPKDuplicadas(AgeSucursalesSaveDAOList, p => new { p.Id.AgeLicencCodigo, p.Id.Codigo }, "Id");

            List<AgeSucursales> ageSucursalesList = new();
            AgeSucursales ageSucursale;

            foreach (AgeSucursalesSaveDAO ageSucursalesSaveDAO in AgeSucursalesSaveDAOList)
            {
                ageSucursale = ValidarUpdate(_httpContextAccessor, ageSucursalesSaveDAO);
                ageSucursalesList.Add(ageSucursale);
            }

            return _repository.ActualizarVarios(ageSucursalesList);
        }

        private Resource<AgeSucursalesDAO> GetDataWithResource(IHttpContextAccessor _httpContextAccessor, AgeSucursalesDAO ageSucursalesDAO)
        {
            string rutaSegmentoFinal = $"{ageSucursalesDAO.Id.AgeLicencCodigo}/{ageSucursalesDAO.Id.Codigo}";

            return Resource<AgeSucursalesDAO>.GetDataWithResource<AgeSucursalesController>(
                _httpContextAccessor, rutaSegmentoFinal, ageSucursalesDAO);
        }

        private AgeSucursales ValidarInsert(IHttpContextAccessor _httpContextAccessor, AgeSucursalesSaveDAO ageSucursalesSaveDAO)
        {
            if (ageSucursalesSaveDAO == null) throw new InvalidSintaxisException();

            Validator.ValidateObject(ageSucursalesSaveDAO, new ValidationContext(ageSucursalesSaveDAO), true);

            FuncionesSecuencias fg = new(_ageLicenciatariosAplicaSecuService);

            ageSucursalesSaveDAO.Id.Codigo = fg.ObtenerSecuencia(
                                            _httpContextAccessor,
                                            ageSucursalesSaveDAO.Id.AgeLicencCodigo,
                                            Globales.CODIGO_SECUENCIA_SUCURSAL,
                                            ageSucursalesSaveDAO.UsuarioIngreso);

            ValidarKeys(ageSucursalesSaveDAO);

            AgeSucursales entityObject = FromDAOToEntity(_httpContextAccessor,ageSucursalesSaveDAO);

            Validator.ValidateObject(entityObject, new ValidationContext(entityObject), true);

            return entityObject;
        }

        private AgeSucursales ValidarUpdate(IHttpContextAccessor _httpContextAccessor, AgeSucursalesSaveDAO ageSucursalesSaveDAO)
        {
            if (ageSucursalesSaveDAO == null)
                throw new InvalidSintaxisException();

            if (ageSucursalesSaveDAO.UsuarioModificacion == null || ageSucursalesSaveDAO.UsuarioModificacion <= 0) 
                throw new InvalidFieldException("El usuario de modificación debe ser un número entero mayor a cero.");

            ValidarFKS(ageSucursalesSaveDAO);

            AgeSucursales? ageSucursale = ConsultarCompletePorId(ageSucursalesSaveDAO.Id) 
                ?? throw new RegisterNotFoundException("Sucursal con código: " + ageSucursalesSaveDAO.Id.Codigo + " no existe.");

            ageSucursale.Estado = !string.IsNullOrWhiteSpace(ageSucursalesSaveDAO.Estado) ? ageSucursalesSaveDAO.Estado : ageSucursale.Estado;
            ageSucursale.FechaEstado = !string.IsNullOrWhiteSpace(ageSucursalesSaveDAO.Estado) ? DateTime.Now : ageSucursale.FechaEstado;
            ageSucursale.Descripcion = !string.IsNullOrWhiteSpace(ageSucursalesSaveDAO.Descripcion) ? ageSucursalesSaveDAO.Descripcion : ageSucursale.Descripcion;
            ageSucursale.ObservacionEstado = !string.IsNullOrWhiteSpace(ageSucursalesSaveDAO.ObservacionEstado) ? ageSucursalesSaveDAO.ObservacionEstado : ageSucursale.ObservacionEstado;

            ageSucursale.FechaModificacion = DateTime.Now;
            ageSucursale.UbicacionModificacion = Ubicacion.getIpAdress(_httpContextAccessor);
            ageSucursale.UsuarioModificacion = ageSucursalesSaveDAO.UsuarioModificacion;

            ageSucursale.AgeAgeTipLoAgePaisCodigo = ageSucursalesSaveDAO.AgeAgeTipLoAgePaisCodigo > 0  ? ageSucursalesSaveDAO.AgeAgeTipLoAgePaisCodigo : ageSucursale.AgeAgeTipLoAgePaisCodigo;
            ageSucursale.AgeLocaliAgeTipLoCodigo = ageSucursalesSaveDAO.AgeLocaliAgeTipLoCodigo > 0 ? ageSucursalesSaveDAO.AgeLocaliAgeTipLoCodigo : ageSucursale.AgeLocaliAgeTipLoCodigo;
            ageSucursale.AgeLocaliCodigo = ageSucursalesSaveDAO.AgeLocaliCodigo > 0 ? ageSucursalesSaveDAO.AgeLocaliCodigo : ageSucursale.AgeLocaliCodigo;
            ageSucursale.AgeTipSuAgeLicencCodigo = ageSucursalesSaveDAO.AgeTipSuAgeLicencCodigo > 0 ? ageSucursalesSaveDAO.AgeTipSuAgeLicencCodigo : ageSucursale.AgeTipSuAgeLicencCodigo;
            ageSucursale.AgeTipSuCodigo = ageSucursalesSaveDAO.AgeTipSuCodigo > 0 ? ageSucursalesSaveDAO.AgeTipSuCodigo : ageSucursale.AgeTipSuCodigo;
            ageSucursale.Direccion = !string.IsNullOrWhiteSpace(ageSucursalesSaveDAO.Direccion) ? ageSucursalesSaveDAO.Direccion : ageSucursale.Direccion;
            ageSucursale.Telefono1 = !string.IsNullOrWhiteSpace(ageSucursalesSaveDAO.Telefono1) ? ageSucursalesSaveDAO.Telefono1 : ageSucursale.Telefono1;
            ageSucursale.EMail1 = !string.IsNullOrWhiteSpace(ageSucursalesSaveDAO.EMail1) ? ageSucursalesSaveDAO.EMail1 : ageSucursale.EMail1;
            ageSucursale.EMail2 = !string.IsNullOrWhiteSpace(ageSucursalesSaveDAO.EMail2) ? ageSucursalesSaveDAO.EMail2 : ageSucursale.EMail2;
            ageSucursale.AgeSucursAgeLicencCodigo = ageSucursalesSaveDAO.AgeSucursAgeLicencCodigo != null && ageSucursalesSaveDAO.AgeSucursAgeLicencCodigo > 0 ? ageSucursalesSaveDAO.AgeSucursAgeLicencCodigo : ageSucursale.AgeSucursAgeLicencCodigo;
            ageSucursale.AgeSucursCodigo = ageSucursalesSaveDAO.AgeSucursCodigo != null && ageSucursalesSaveDAO.AgeSucursCodigo > 0 ? ageSucursalesSaveDAO.AgeSucursCodigo : ageSucursale.AgeSucursCodigo;
            ageSucursale.CodigoEstablecimiento = ageSucursalesSaveDAO.CodigoEstablecimiento != null && ageSucursalesSaveDAO.CodigoEstablecimiento > 0 ? ageSucursalesSaveDAO.CodigoEstablecimiento : ageSucursale.CodigoEstablecimiento;
            ageSucursale.Telefono2 = !string.IsNullOrWhiteSpace(ageSucursalesSaveDAO.Telefono2) ? ageSucursalesSaveDAO.Telefono2 : ageSucursale.Telefono2;
            ageSucursale.Observacion = !string.IsNullOrWhiteSpace(ageSucursalesSaveDAO.Observacion) ? ageSucursalesSaveDAO.Observacion : ageSucursale.Observacion;
            ageSucursale.Latitud = !string.IsNullOrWhiteSpace(ageSucursalesSaveDAO.Latitud) ? ageSucursalesSaveDAO.Latitud : ageSucursale.Latitud;
            ageSucursale.Longitud = !string.IsNullOrWhiteSpace(ageSucursalesSaveDAO.Longitud) ? ageSucursalesSaveDAO.Longitud : ageSucursale.Longitud;
            ageSucursale.CentroAcopio = !string.IsNullOrWhiteSpace(ageSucursalesSaveDAO.CentroAcopio) ? ageSucursalesSaveDAO.CentroAcopio : ageSucursale.CentroAcopio;
            
            Validator.ValidateObject(ageSucursale, new ValidationContext(ageSucursale), true);

            return ageSucursale;
        }

        private AgeSucursales FromDAOToEntity(
            IHttpContextAccessor _httpContextAccessor,
            AgeSucursalesSaveDAO ageSucursalesSaveDAO)
        {
            return new AgeSucursales()
            {
                Codigo = ageSucursalesSaveDAO.Id.Codigo,
                AgeLicencCodigo = ageSucursalesSaveDAO.Id.AgeLicencCodigo,
                Descripcion = ageSucursalesSaveDAO.Descripcion,
                Estado = ageSucursalesSaveDAO.Estado,
                FechaEstado = DateTime.Now,
                FechaIngreso = DateTime.Now,
                ObservacionEstado = ageSucursalesSaveDAO.ObservacionEstado,
                UbicacionIngreso = Ubicacion.getIpAdress(_httpContextAccessor),
                UsuarioIngreso = ageSucursalesSaveDAO.UsuarioIngreso,
                AgeAgeTipLoAgePaisCodigo = ageSucursalesSaveDAO.AgeAgeTipLoAgePaisCodigo,
                AgeLocaliAgeTipLoCodigo = ageSucursalesSaveDAO.AgeLocaliAgeTipLoCodigo,
                AgeLocaliCodigo = ageSucursalesSaveDAO.AgeLocaliCodigo,
                AgeTipSuAgeLicencCodigo = ageSucursalesSaveDAO.AgeTipSuAgeLicencCodigo,
                AgeTipSuCodigo = ageSucursalesSaveDAO.AgeTipSuCodigo,
                Direccion = ageSucursalesSaveDAO.Direccion,
                Telefono1 = ageSucursalesSaveDAO.Telefono1,
                EMail1 = ageSucursalesSaveDAO.EMail1,
                EMail2 = ageSucursalesSaveDAO.EMail2,
                AgeSucursAgeLicencCodigo = ageSucursalesSaveDAO.AgeSucursAgeLicencCodigo,
                AgeSucursCodigo = ageSucursalesSaveDAO.AgeSucursCodigo,
                CodigoEstablecimiento = ageSucursalesSaveDAO.CodigoEstablecimiento,
                Telefono2 = ageSucursalesSaveDAO.Telefono2,
                Observacion = ageSucursalesSaveDAO.Observacion,
                Latitud = ageSucursalesSaveDAO.Latitud,
                Longitud = ageSucursalesSaveDAO.Longitud,
                CentroAcopio = ageSucursalesSaveDAO.CentroAcopio
            };
        }

        private void ValidarKeys(AgeSucursalesSaveDAO ageSucursalesSaveDAO) 
        {  
            ValidarPK(ageSucursalesSaveDAO.Id);
            ValidarFKS(ageSucursalesSaveDAO);
        }

        private void ValidarPK(AgeSucursalesPKDAO ageSucursalesPKDAO)
        {
            ValidateKeys.ValidarNoExistenciaKey(ageSucursalesPKDAO, 
                $"Sucursal con código: {ageSucursalesPKDAO.Codigo} " +
                $"y código licenciatario: {ageSucursalesPKDAO.AgeLicencCodigo} ya existe.",
                _repository.ConsultarCompletePorId);
        }

        private void ValidarFKS(AgeSucursalesSaveDAO ageSucursalesSaveDAO)
        {
            if ((ageSucursalesSaveDAO.AgeSucursCodigo > 0 && ageSucursalesSaveDAO.AgeSucursCodigo != null) ||
                (ageSucursalesSaveDAO.AgeSucursAgeLicencCodigo > 0 && ageSucursalesSaveDAO.AgeSucursAgeLicencCodigo != null))
            {
                AgeSucursalesPKDAO ageSucursalesPKDAO = new()
                {
                    Codigo = ageSucursalesSaveDAO.AgeSucursCodigo ?? 0,
                    AgeLicencCodigo = ageSucursalesSaveDAO.AgeSucursAgeLicencCodigo ?? 0
                };

                ValidarFKSucursal(ageSucursalesPKDAO);
            }

            ValidarFKLicenciatario(ageSucursalesSaveDAO);

            if ((ageSucursalesSaveDAO.AgeLocaliCodigo > 0) ||
                (ageSucursalesSaveDAO.AgeLocaliAgeTipLoCodigo > 0) ||
                (ageSucursalesSaveDAO.AgeAgeTipLoAgePaisCodigo > 0))
            {
                AgeLocalidadesPKDAO ageLocalidadesPKDAO = new()
                {
                    Codigo = ageSucursalesSaveDAO.AgeLocaliCodigo,
                    AgeTipLoAgePaisCodigo = ageSucursalesSaveDAO.AgeAgeTipLoAgePaisCodigo,
                    AgeTipLoCodigo = ageSucursalesSaveDAO.AgeLocaliAgeTipLoCodigo
                };

                ValidarFKLocalidad(ageLocalidadesPKDAO);
            }

            if ((ageSucursalesSaveDAO.AgeTipSuCodigo > 0) ||
                (ageSucursalesSaveDAO.AgeTipSuAgeLicencCodigo > 0))
            {
                AgeTiposSucursalePKDAO ageTiposSucursalePKDAO = new()
                {
                    codigo = ageSucursalesSaveDAO.AgeTipSuCodigo,
                    ageLicencCodigo = ageSucursalesSaveDAO.AgeTipSuAgeLicencCodigo
                };

                ValidarFKTipoSucursal(ageTiposSucursalePKDAO);
            }
        }

        private void ValidarFKSucursal(AgeSucursalesPKDAO ageSucursalesPKDAO)
        {
            ValidateKeys.ValidarExistenciaKey(ageSucursalesPKDAO, $"Sucursal con código: {ageSucursalesPKDAO.Codigo}" +
                                                                $" y código de licenciatario: {ageSucursalesPKDAO.AgeLicencCodigo} no existe.",
                                                                _repository.ConsultarCompletePorId);
        }

        private void ValidarFKLicenciatario(AgeSucursalesSaveDAO ageSucursalesSaveDAO)
        {
            ValidateKeys.ValidarExistenciaKey(ageSucursalesSaveDAO.Id.AgeLicencCodigo,
                $"Licenciatario con código: {ageSucursalesSaveDAO.Id.AgeLicencCodigo} no existe."
                , _repositoryLicenciatarios.ConsultarCompletePorId);
        }

        private void ValidarFKLocalidad(AgeLocalidadesPKDAO ageLocalidadesPKDAO)
        {
            ValidateKeys.ValidarExistenciaKey(ageLocalidadesPKDAO, $"Localidad con código: {ageLocalidadesPKDAO.Codigo}" +
                         $", código país: {ageLocalidadesPKDAO.AgeTipLoAgePaisCodigo} " +
                         $" y código tipo localidad: {ageLocalidadesPKDAO.AgeTipLoCodigo} no existe."
                         , _repositoryLocalidad.ConsultarCompletePorId);
        }

        private void ValidarFKTipoSucursal(AgeTiposSucursalePKDAO ageTiposSucursalePKDAO)
        {
            ValidateKeys.ValidarExistenciaKey(ageTiposSucursalePKDAO, $"Tipo Sucursal con código: {ageTiposSucursalePKDAO.codigo}" +
                         $" y código licenciatario: {ageTiposSucursalePKDAO.ageLicencCodigo} no existe."
                         , _repositoryTipoSucursal.ConsultarCompletePorId);
        }
    }
}
