﻿using AGE.Entities.DAO.AgeSucursales;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;

namespace AGE.Services.AgeSucursalesService
{
    public interface IAgeSucursalesService
    {
        Task<Page<AgeSucursalesDAO>> ConsultarTodos(int codigoLicenciatario,
                                                    int? codigo,
                                                    string? descripcion,
                                                    string? direccion,
                                                    Pageable pageable);

        Task<AgeSucursalesDAO> ConsultarPorId(AgeSucursalesPKDAO ageSucursalesPKDAO);

        Task<Page<AgeSucursalesDAO>> ConsultarListaFiltro(int codigoLicenciatario,
                                                            string filtro,
                                                            Pageable pageable);

        Task<Resource<AgeSucursalesDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgeSucursalesSaveDAO AgeSucursalesSaveDAO);

        Task<List<Resource<AgeSucursalesDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeSucursalesSaveDAO> AgeSucursalesSaveDAOList);

        Task<AgeSucursalesDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeSucursalesSaveDAO AgeSucursalesSaveDAO);

        Task<List<AgeSucursalesDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeSucursalesSaveDAO> AgeSucursalesSaveDAOList);
    }
}
