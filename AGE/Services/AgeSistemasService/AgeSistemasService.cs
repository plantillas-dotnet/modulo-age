﻿using AGE.Controllers;
using AGE.Entities.DAO.AgeSistemas;
using AGE.Entities;
using AGE.Middleware.Exceptions.BadRequest;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Repositories.AgeSistemasRepository;
using AGE.Services.AgeSecuenciasPrimariasService;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using AGE.Utils;
using System.ComponentModel.DataAnnotations;
using System.Transactions;

namespace AGE.Services.AgeSistemasService
{
    public class AgeSistemasService : IAgeSistemasService
    {
        private readonly IAgeSistemasRepository _repository;
        private readonly IAgeSecuenciasPrimariasService _ageSecuenciasPrimariasService;


        public AgeSistemasService(
            IAgeSistemasRepository Repository,
            IAgeSecuenciasPrimariasService ageSecuenciasPrimariasService)
        {
            _repository = Repository;
            _ageSecuenciasPrimariasService = ageSecuenciasPrimariasService;
        }

        public Task<Page<AgeSistemasDAO>> ConsultarTodos(
            int codigo,
            string descripcion,
            Pageable pageable)
        {
            pageable.Validar<AgeSistemasDAO>();

            return _repository.ConsultarTodos(codigo, descripcion, pageable);
        }

        public Task<AgeSistemasDAO> ConsultarPorId(
            int codigo)
        {
            if (codigo <= 0)
                throw new InvalidIdException();

            AgeSistemasDAO? ageSistemasDAO = _repository.ConsultarPorId(codigo).Result;

            return ageSistemasDAO == null
                ? throw new RegisterNotFoundException("Sistema con código: " + codigo + " no existe.")
                : Task.FromResult(ageSistemasDAO);
        }

        private AgeSistemas ConsultarCompletePorId(
            int codigo)
        {
            if (codigo <= 0)
                throw new InvalidIdException();

            return _repository.ConsultarCompletePorId(codigo).Result;
        }

        public Task<Page<AgeSistemasDAO>> ConsultarListaFiltro(string filtro, Pageable pageable)
        {

            if (string.IsNullOrWhiteSpace(filtro))
                throw new InvalidFieldException("El filtro es requerido.");

            pageable.Validar<AgeSistemasDAO>();

            return _repository.ConsultarListaFiltro(filtro, pageable);

        }

        public async Task<Resource<AgeSistemasDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgeSistemasSaveDAO ageSistemasSaveDAO)
        {
            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    AgeSistemas ageSistemas = ValidarInsert(_httpContextAccessor, ageSistemasSaveDAO);

                    AgeSistemasDAO ageSistemasDAO = await _repository.Insertar(ageSistemas);

                    Resource<AgeSistemasDAO> ageSistemasDAOWithResource = GetDataWithResource(_httpContextAccessor, ageSistemasDAO);

                    transactionScope.Complete();

                    return ageSistemasDAOWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public Task<AgeSistemasDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeSistemasSaveDAO ageSistemasSaveDAO)
        {

            AgeSistemas ageSistemas = ValidarUpdate(_httpContextAccessor, ageSistemasSaveDAO);

            return _repository.Actualizar(ageSistemas);
        }

        public async Task<List<Resource<AgeSistemasDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeSistemasSaveDAO> ageSistemasSaveDAOList)
        {
            if (ageSistemasSaveDAOList == null)
                throw new InvalidSintaxisException();

            List<AgeSistemas> ageSistemasList = new List<AgeSistemas>();

            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    foreach (AgeSistemasSaveDAO ageSistemasSaveDAO in ageSistemasSaveDAOList)
                    {
                        AgeSistemas AgeSistema = ValidarInsert(_httpContextAccessor, ageSistemasSaveDAO);
                        ageSistemasList.Add(AgeSistema);
                    }

                    List<AgeSistemasDAO> ageSistemasDAOList = await _repository.InsertarVarios(ageSistemasList);

                    List<Resource<AgeSistemasDAO>> ageSistemasDAOListWithResource = new List<Resource<AgeSistemasDAO>>();

                    foreach (AgeSistemasDAO ageSistemasDAO in ageSistemasDAOList)
                    {
                        ageSistemasDAOListWithResource.Add(GetDataWithResource(_httpContextAccessor, ageSistemasDAO));
                    }

                    transactionScope.Complete();

                    return ageSistemasDAOListWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public Task<List<AgeSistemasDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeSistemasSaveDAO> ageSistemasSaveDAOList)
        {
            if (ageSistemasSaveDAOList == null)
                throw new InvalidSintaxisException();

            ValidateKeys.ValidarPKDuplicadas(ageSistemasSaveDAOList,
                                             p => new { p.Codigo},
                                             "Id");

            List<AgeSistemas> ageSistemasList = new List<AgeSistemas>();
            AgeSistemas ageSistemas;

            foreach (AgeSistemasSaveDAO ageRutasSaveDAO in ageSistemasSaveDAOList)
            {
                ageSistemas = ValidarUpdate(_httpContextAccessor, ageRutasSaveDAO);
                ageSistemasList.Add(ageSistemas);
            }

            return _repository.ActualizarVarios(ageSistemasList);
        }
        
        private static Resource<AgeSistemasDAO> GetDataWithResource(
            IHttpContextAccessor _httpContextAccessor,
            AgeSistemasDAO ageSistemasDAO)
        {
            string rutaSegmentoFinal = $"{ageSistemasDAO.Codigo}";

            return Resource<AgeSistemasDAO>.GetDataWithResource<AgeSistemasController>(
                _httpContextAccessor, rutaSegmentoFinal, ageSistemasDAO);
        }

        private AgeSistemas ValidarInsert(
            IHttpContextAccessor _httpContextAccessor,
            AgeSistemasSaveDAO ageSistemasSaveDAO)
        {
            if (ageSistemasSaveDAO == null)
                throw new InvalidSintaxisException();

            Validator.ValidateObject(ageSistemasSaveDAO, new ValidationContext(ageSistemasSaveDAO), true);

            FuncionesSecuencias fg = new FuncionesSecuencias(_ageSecuenciasPrimariasService);

            ageSistemasSaveDAO.Codigo = fg.ObtenerSecuenciaPrimaria(
                _httpContextAccessor,
                Globales.CODIGO_SECUENCIA_SISTEMA,
                ageSistemasSaveDAO.UsuarioIngreso);

            ValidarPK(ageSistemasSaveDAO.Codigo);

            AgeSistemas entityObject = FromDAOToEntity(_httpContextAccessor, ageSistemasSaveDAO);

            Validator.ValidateObject(entityObject, new ValidationContext(entityObject), true);

            return entityObject;
        }

        private AgeSistemas ValidarUpdate(
            IHttpContextAccessor _httpContextAccessor,
            AgeSistemasSaveDAO ageSistemasSaveDAO)
        {
            if (ageSistemasSaveDAO == null)
                throw new InvalidSintaxisException();

            if (ageSistemasSaveDAO.UsuarioModificacion == null || ageSistemasSaveDAO.UsuarioModificacion <= 0)
                throw new InvalidFieldException("El usuario de modificación debe ser un número entero mayor a cero.");

            if (!string.IsNullOrWhiteSpace(ageSistemasSaveDAO.Descripcion))
                throw new InvalidFieldException("La descripción no debe ser nula.");

            AgeSistemas ageSistemas = ConsultarCompletePorId(ageSistemasSaveDAO.Codigo) 
                ?? throw new RegisterNotFoundException($"Sistema con código: {ageSistemasSaveDAO.Codigo} no existe.");

            ageSistemas.Estado = !string.IsNullOrWhiteSpace(ageSistemasSaveDAO.Estado) ? ageSistemasSaveDAO.Estado : ageSistemas.Estado;
            ageSistemas.FechaEstado = !string.IsNullOrWhiteSpace(ageSistemasSaveDAO.Estado) ? DateTime.Now : ageSistemas.FechaEstado;
            ageSistemas.Descripcion = !string.IsNullOrWhiteSpace(ageSistemasSaveDAO.Descripcion) ? ageSistemasSaveDAO.Descripcion : ageSistemas.Descripcion;
            ageSistemas.ObservacionEstado = !string.IsNullOrWhiteSpace(ageSistemasSaveDAO.ObservacionEstado) ? ageSistemasSaveDAO.ObservacionEstado : ageSistemas.ObservacionEstado;

            ageSistemas.FechaModificacion = DateTime.Now;
            ageSistemas.UbicacionModificacion = Ubicacion.getIpAdress(_httpContextAccessor);
            ageSistemas.UsuarioModificacion = ageSistemasSaveDAO.UsuarioModificacion;

            Validator.ValidateObject(ageSistemas, new ValidationContext(ageSistemas), true);

            return ageSistemas;
        }

        private AgeSistemas FromDAOToEntity(
            IHttpContextAccessor _httpContextAccessor,
            AgeSistemasSaveDAO ageSistemasSaveDAO)
        {

            return new AgeSistemas()
            {
                Codigo = ageSistemasSaveDAO.Codigo,
                Descripcion = ageSistemasSaveDAO.Descripcion,
                Estado = ageSistemasSaveDAO.Estado.ToUpper().ToString(),
                FechaEstado = DateTime.Now,
                FechaIngreso = DateTime.Now,
                ObservacionEstado = ageSistemasSaveDAO.ObservacionEstado,
                UsuarioIngreso = ageSistemasSaveDAO.UsuarioIngreso,
                UbicacionIngreso = Ubicacion.getIpAdress(_httpContextAccessor)
            };
        }

        private void ValidarPK(int codigoSistema)
        {
            ValidateKeys.ValidarNoExistenciaKey(
               codigoSistema,
               $"Sistema con código: {codigoSistema} ya existe.",
               _repository.ConsultarCompletePorId);
        }
    }
}
