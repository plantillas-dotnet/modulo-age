﻿using AGE.Entities.DAO.AgeSistemas;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;

namespace AGE.Services.AgeSistemasService
{
    public interface IAgeSistemasService
    {
        Task<Page<AgeSistemasDAO>> ConsultarTodos(
            int codigo,
            string descripcion,
            Pageable pageable);

        Task<Page<AgeSistemasDAO>> ConsultarListaFiltro(
            string filtro,
            Pageable pageable);

        Task<AgeSistemasDAO> ConsultarPorId(
            int codigo);

        Task<Resource<AgeSistemasDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgeSistemasSaveDAO ageSistemasSaveDAO);

        Task<AgeSistemasDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeSistemasSaveDAO ageSistemasSaveDAO);

        Task<List<Resource<AgeSistemasDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeSistemasSaveDAO> ageSistemasSaveDAOList);

        Task<List<AgeSistemasDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeSistemasSaveDAO> ageSistemasSaveDAOList);
    }
}
