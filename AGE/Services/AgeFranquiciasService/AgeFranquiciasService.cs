﻿using AGE.Controllers;
using AGE.Entities.DAO.AgeFranquicias;
using AGE.Entities;
using AGE.Middleware.Exceptions.BadRequest;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Repositories.AgeFranquiciasRepository;
using AGE.Services.AgeSecuenciasPrimariasService;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using AGE.Utils;
using System.ComponentModel.DataAnnotations;
using System.Transactions;

namespace AGE.Services.AgeFranquiciasService
{
    public class AgeFranquiciasService : IAgeFranquiciasService
    {
        private readonly IAgeFranquiciasRepository _repository;
        private readonly IAgeSecuenciasPrimariasService _ageSecuenciasPrimariasService;


        public AgeFranquiciasService(
            IAgeFranquiciasRepository Repository,
            IAgeSecuenciasPrimariasService ageSecuenciasPrimariasService)
        {
            _repository = Repository;
            _ageSecuenciasPrimariasService = ageSecuenciasPrimariasService;
        }

        public Task<Page<AgeFranquiciasDAO>> ConsultarTodos(
            int codigo,
            string descripcion,
            Pageable pageable)
        {
            pageable.Validar<AgeFranquiciasDAO>();

            return _repository.ConsultarTodos(codigo, descripcion, pageable);
        }

        public Task<AgeFranquiciasDAO> ConsultarPorId(
            int codigo)
        {

            if (codigo <= 0)
                throw new InvalidIdException();

            AgeFranquiciasDAO? ageFranquiciasDAO = _repository.ConsultarPorId(codigo).Result;

            return ageFranquiciasDAO == null
                ? throw new RegisterNotFoundException("Franquicia con código: " + codigo + " no existe.")
                : Task.FromResult(ageFranquiciasDAO);
        }

        private AgeFranquicias ConsultarCompletePorId(
            int codigo)
        {

            if (codigo <= 0)
                throw new InvalidIdException();

            return _repository.ConsultarCompletePorId(codigo).Result;
        }

        public Task<Page<AgeFranquiciasDAO>> ConsultarListaFiltro(string filtro, Pageable pageable)
        {

            if (string.IsNullOrWhiteSpace(filtro))
                throw new InvalidFieldException("El filtro es requerido.");

            pageable.Validar<AgeFranquiciasDAO>();

            return _repository.ConsultarListaFiltro(filtro, pageable);

        }

        public async Task<Resource<AgeFranquiciasDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgeFranquiciasSaveDAO ageFranquiciasSaveDAO)
        {
            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    AgeFranquicias ageFranquicias = ValidarInsert(_httpContextAccessor, ageFranquiciasSaveDAO);

                    AgeFranquiciasDAO ageFranquiciasDAO = await _repository.Insertar(ageFranquicias);

                    Resource<AgeFranquiciasDAO> ageFranquiciasDAOWithResource = GetDataWithResource(_httpContextAccessor, ageFranquiciasDAO);

                    transactionScope.Complete();

                    return ageFranquiciasDAOWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public async Task<List<Resource<AgeFranquiciasDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeFranquiciasSaveDAO> ageFranquiciasSaveDAOList)
        {
            if (ageFranquiciasSaveDAOList == null)
                throw new InvalidSintaxisException();

            List<AgeFranquicias> ageFranquiciasList = new List<AgeFranquicias>();

            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    foreach (AgeFranquiciasSaveDAO ageFranquiciasSaveDAO in ageFranquiciasSaveDAOList)
                    {
                        AgeFranquicias AgeFranquicia = ValidarInsert(_httpContextAccessor, ageFranquiciasSaveDAO);
                        ageFranquiciasList.Add(AgeFranquicia);
                    }

                    List<AgeFranquiciasDAO> ageFranquiciasDAOList = await _repository.InsertarVarios(ageFranquiciasList);

                    List<Resource<AgeFranquiciasDAO>> ageFranquiciasDAOListWithResource = new List<Resource<AgeFranquiciasDAO>>();

                    foreach (AgeFranquiciasDAO ageFranquiciasDAO in ageFranquiciasDAOList)
                    {
                        ageFranquiciasDAOListWithResource.Add(GetDataWithResource(_httpContextAccessor, ageFranquiciasDAO));
                    }

                    transactionScope.Complete();

                    return ageFranquiciasDAOListWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public Task<AgeFranquiciasDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeFranquiciasSaveDAO ageFranquiciasSaveDAO)
        {

            AgeFranquicias ageFranquicias = ValidarUpdate(_httpContextAccessor, ageFranquiciasSaveDAO);

            return _repository.Actualizar(ageFranquicias);
        }

        public Task<List<AgeFranquiciasDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeFranquiciasSaveDAO> ageFranquiciasSaveDAOList)
        {
            if (ageFranquiciasSaveDAOList == null)
                throw new InvalidSintaxisException();

            ValidateKeys.ValidarPKDuplicadas(ageFranquiciasSaveDAOList, p => new { p.Codigo }, "Codigo");

            List<AgeFranquicias> ageFranquiciasList = new List<AgeFranquicias>();
            AgeFranquicias ageFranquicias;

            foreach (AgeFranquiciasSaveDAO ageRutasSaveDAO in ageFranquiciasSaveDAOList)
            {
                ageFranquicias = ValidarUpdate(_httpContextAccessor, ageRutasSaveDAO);
                ageFranquiciasList.Add(ageFranquicias);
            }

            return _repository.ActualizarVarios(ageFranquiciasList);
        }
        
        private static Resource<AgeFranquiciasDAO> GetDataWithResource(
            IHttpContextAccessor _httpContextAccessor,
            AgeFranquiciasDAO ageFranquiciasDAO)
        {
            string rutaSegmentoFinal = $"{ageFranquiciasDAO.Codigo}";

            return Resource<AgeFranquiciasDAO>.GetDataWithResource<AgeFranquiciasController>(
                _httpContextAccessor, rutaSegmentoFinal, ageFranquiciasDAO);
        }

        private AgeFranquicias ValidarInsert(
            IHttpContextAccessor _httpContextAccessor,
            AgeFranquiciasSaveDAO ageFranquiciasSaveDAO)
        {
            if (ageFranquiciasSaveDAO == null)
                throw new InvalidSintaxisException();

            Validator.ValidateObject(ageFranquiciasSaveDAO, new ValidationContext(ageFranquiciasSaveDAO), true);

            FuncionesSecuencias fg = new FuncionesSecuencias(_ageSecuenciasPrimariasService);

            ageFranquiciasSaveDAO.Codigo = fg.ObtenerSecuenciaPrimaria(
                        _httpContextAccessor,
                        Globales.CODIGO_SECUENCIA_FRANQUICIA,
                        ageFranquiciasSaveDAO.UsuarioIngreso);

            ValidarPK(ageFranquiciasSaveDAO.Codigo);

            AgeFranquicias entityObject = FromDAOToEntity(_httpContextAccessor,ageFranquiciasSaveDAO);

            Validator.ValidateObject(entityObject, new ValidationContext(entityObject), true);

            return entityObject;
        }

        private AgeFranquicias ValidarUpdate(
            IHttpContextAccessor _httpContextAccessor,
            AgeFranquiciasSaveDAO ageFranquiciasSaveDAO)
        {
            if (ageFranquiciasSaveDAO == null)
                throw new InvalidSintaxisException();

            if (ageFranquiciasSaveDAO.UsuarioModificacion == null || ageFranquiciasSaveDAO.UsuarioModificacion <= 0)
                throw new InvalidFieldException("El usuario de modificación debe ser un número entero mayor a cero.");

            AgeFranquicias ageFranquicias = ConsultarCompletePorId(ageFranquiciasSaveDAO.Codigo) ?? 
                throw new RegisterNotFoundException($"Franquicia con código: {ageFranquiciasSaveDAO.Codigo} no existe.");

            if (!string.IsNullOrWhiteSpace(ageFranquiciasSaveDAO.Estado))
            {
                ageFranquicias.Estado = ageFranquiciasSaveDAO.Estado;
                ageFranquicias.FechaEstado = DateTime.Now;
            }

            if (!string.IsNullOrWhiteSpace(ageFranquiciasSaveDAO.Descripcion))
                ageFranquicias.Descripcion = ageFranquiciasSaveDAO.Descripcion;

            if (!string.IsNullOrWhiteSpace(ageFranquiciasSaveDAO.ObservacionEstado))
                ageFranquicias.ObservacionEstado = ageFranquiciasSaveDAO.ObservacionEstado;

            ageFranquicias.FechaModificacion = DateTime.Now;
            ageFranquicias.UbicacionModificacion = Ubicacion.getIpAdress(_httpContextAccessor);
            ageFranquicias.UsuarioModificacion = ageFranquiciasSaveDAO.UsuarioModificacion;

            Validator.ValidateObject(ageFranquicias, new ValidationContext(ageFranquicias), true);

            return ageFranquicias;
        }

        private AgeFranquicias FromDAOToEntity(
           IHttpContextAccessor _httpContextAccessor,
           AgeFranquiciasSaveDAO ageFranquiciasSaveDAO)
        {

            return new AgeFranquicias
            {
                Codigo = ageFranquiciasSaveDAO.Codigo,
                Descripcion = ageFranquiciasSaveDAO.Descripcion,
                Estado = ageFranquiciasSaveDAO.Estado.ToUpper().ToString(),
                FechaEstado = DateTime.Now,
                FechaIngreso = DateTime.Now,
                ObservacionEstado = ageFranquiciasSaveDAO.ObservacionEstado,
                UsuarioIngreso = ageFranquiciasSaveDAO.UsuarioIngreso,
                UbicacionIngreso = Ubicacion.getIpAdress(_httpContextAccessor)
            };
        }

        private void ValidarPK(int codigoFranquicia)
        {
            ValidateKeys.ValidarNoExistenciaKey(codigoFranquicia,
                $"Franquicia con código: {codigoFranquicia} ya existe.",
                _repository.ConsultarPorId);
        }
    }
}
