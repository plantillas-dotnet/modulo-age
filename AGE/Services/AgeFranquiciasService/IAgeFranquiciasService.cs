﻿using AGE.Entities.DAO.AgeFranquicias;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;

namespace AGE.Services.AgeFranquiciasService
{
    public interface IAgeFranquiciasService
    {
        Task<Page<AgeFranquiciasDAO>> ConsultarTodos(
            int Codigo,
            string Descripcion,
            Pageable pageable);

        Task<Page<AgeFranquiciasDAO>> ConsultarListaFiltro(
            string filtro,
            Pageable pageable);

        Task<AgeFranquiciasDAO> ConsultarPorId(int Codigo);

        Task<Resource<AgeFranquiciasDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgeFranquiciasSaveDAO AgeFranquiciasSaveDAO);

        Task<List<Resource<AgeFranquiciasDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeFranquiciasSaveDAO> AgeFranquiciasSaveDAOList);

        Task<AgeFranquiciasDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeFranquiciasSaveDAO AgeFranquiciasSaveDAO);

        Task<List<AgeFranquiciasDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeFranquiciasSaveDAO> AgeFranquiciasSaveDAOList);
    }
}
