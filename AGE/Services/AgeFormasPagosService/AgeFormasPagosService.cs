﻿using AGE.Controllers;
using AGE.Entities;
using AGE.Entities.DAO.AgeFormasPago;
using AGE.Middleware.Exceptions.BadRequest;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Repositories.AgeFormasPagoRepository;
using AGE.Repositories.AgeLicenciatariosRepository;
using AGE.Services.AgeLicenciatariosAplicaSecuService;
using AGE.Utils;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using System.ComponentModel.DataAnnotations;
using System.Transactions;

namespace AGE.Services.AgeFormasPagoService
{
    public class AgeFormasPagosService : IAgeFormasPagosService
    {

        private readonly IAgeFormasPagosRepository _repository;
        private readonly IAgeLicenciatariosRepository _repositoryLicenciatario;
        private readonly IAgeLicenciatariosAplicaSecuService _ageLicenciatariosAplicaSecuService;


        public AgeFormasPagosService(
            IAgeFormasPagosRepository Repository,
            IAgeLicenciatariosRepository repositoryLicenciatario,
            IAgeLicenciatariosAplicaSecuService ageLicenciatariosAplicaSecuService)
        {
            _repository = Repository;
            _ageLicenciatariosAplicaSecuService = ageLicenciatariosAplicaSecuService;
            _repositoryLicenciatario = repositoryLicenciatario;
        }

        public Task<Page<AgeFormasPagosDAO>> ConsultarTodos(
            int codigoLicenciatario,
            int codigoInstitucionControl,
            string descripcion,
            Pageable pageable)
        {
            if (codigoLicenciatario <= 0)
                throw new InvalidFieldException("El código de licenciatario debe ser un número entero mayor a cero.");

            pageable.Validar<AgeFormasPagosDAO>();

            return _repository.ConsultarTodos(codigoLicenciatario, codigoInstitucionControl, descripcion, pageable);
        }

        public Task<AgeFormasPagosDAO> ConsultarPorId(
            AgeFormasPagosPKDAO ageFormasPagoPKDAO)
        {
            Validator.ValidateObject(ageFormasPagoPKDAO, new ValidationContext(ageFormasPagoPKDAO), true);

            AgeFormasPagosDAO ageFormasPagoDAO = _repository.ConsultarPorId(ageFormasPagoPKDAO).Result;

            return ageFormasPagoDAO == null
                ? throw new RegisterNotFoundException("Forma de pago con código: " + ageFormasPagoPKDAO.Codigo + " no existe.")
                : Task.FromResult(ageFormasPagoDAO);
        }

        private AgeFormasPagos ConsultarCompletePorId(
           AgeFormasPagosPKDAO ageFormasPagoPKDAO)
        {
            Validator.ValidateObject(ageFormasPagoPKDAO, new ValidationContext(ageFormasPagoPKDAO), true);

            AgeFormasPagos? ageFormasPago = _repository.ConsultarCompletePorId(ageFormasPagoPKDAO).Result;

            return ageFormasPago;
        }

        public Task<Page<AgeFormasPagosDAO>> ConsultarListaFiltro(
            int codigoLicenciatario,
            string filtro,
            Pageable pageable)
        {
            if (codigoLicenciatario == 0)
                throw new InvalidFieldException("El código de licenciatario debe ser un número entero mayor a cero.");

            if (string.IsNullOrWhiteSpace(filtro))
                throw new InvalidFieldException("El filtro es requerido.");

            pageable.Validar<AgeFormasPagosDAO>();

            return _repository.ConsultarListaFiltro(codigoLicenciatario, filtro.ToLower(), pageable);
        }

        public async Task<Resource<AgeFormasPagosDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgeFormasPagosSaveDAO ageFormasPagoSaveDAO)
        {
            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    AgeFormasPagos ageFormasPago = ValidarInsert(_httpContextAccessor, ageFormasPagoSaveDAO);

                    AgeFormasPagosDAO ageFormasPagoDAO = await _repository.Insertar(ageFormasPago);
                    
                    Resource<AgeFormasPagosDAO> ageFormasPagoDAOWithResource = GetDataWithResource(_httpContextAccessor, ageFormasPagoDAO);

                    transactionScope.Complete();

                    return ageFormasPagoDAOWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public async Task<List<Resource<AgeFormasPagosDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeFormasPagosSaveDAO> ageFormasPagoSaveDAOList)
        {
            if (ageFormasPagoSaveDAOList == null)
                throw new InvalidSintaxisException();

            List<AgeFormasPagos> ageFormasPagoList = new List<AgeFormasPagos>();

            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    foreach (AgeFormasPagosSaveDAO ageFormasPagoSaveDAO in ageFormasPagoSaveDAOList)
                    {
                        AgeFormasPagos ageMensaje = ValidarInsert(_httpContextAccessor, ageFormasPagoSaveDAO);
                        ageFormasPagoList.Add(ageMensaje);
                    }

                    List<AgeFormasPagosDAO> ageMensajesDAOList = await _repository.InsertarVarios(ageFormasPagoList);

                    List<Resource<AgeFormasPagosDAO>> ageMensajesDAOListWithResource = new List<Resource<AgeFormasPagosDAO>>();

                    foreach (AgeFormasPagosDAO ageFormasPagoDAO in ageMensajesDAOList)
                    {
                        ageMensajesDAOListWithResource.Add(GetDataWithResource(_httpContextAccessor, ageFormasPagoDAO));
                    }

                    transactionScope.Complete();

                    return ageMensajesDAOListWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public Task<AgeFormasPagosDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeFormasPagosSaveDAO ageFormasPagoSaveDAO)
        {
            AgeFormasPagos ageFormasPago = ValidarUpdate(_httpContextAccessor, ageFormasPagoSaveDAO);

            return _repository.Actualizar(ageFormasPago);
        }

        public Task<List<AgeFormasPagosDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeFormasPagosSaveDAO> ageFormasPagoSaveDAOList)
        {
            if (ageFormasPagoSaveDAOList == null)
                throw new InvalidSintaxisException();

            ValidateKeys.ValidarPKDuplicadas(ageFormasPagoSaveDAOList, p => new { p.Id.AgeLicencCodigo, p.Id.Codigo}, "Id");

            List<AgeFormasPagos> ageFormasPagoList = new List<AgeFormasPagos>();
            AgeFormasPagos ageFormasPago;

            foreach (AgeFormasPagosSaveDAO ageFormasPagoSaveDAO in ageFormasPagoSaveDAOList)
            {
                ageFormasPago = ValidarUpdate(_httpContextAccessor, ageFormasPagoSaveDAO);
                ageFormasPagoList.Add(ageFormasPago);
            }

            return _repository.ActualizarVarios(ageFormasPagoList);
        }

        private static Resource<AgeFormasPagosDAO> GetDataWithResource(
            IHttpContextAccessor _httpContextAccessor,
            AgeFormasPagosDAO ageFormasPagoDAO)
        {
            string tpersonaSegmentoFinal = $"{ageFormasPagoDAO.Id.AgeLicencCodigo}/{ageFormasPagoDAO.Id.Codigo}";

            return Resource<AgeFormasPagosDAO>.GetDataWithResource<AgeFormasPagosController>(
                _httpContextAccessor, tpersonaSegmentoFinal, ageFormasPagoDAO);
        }

        private AgeFormasPagos ValidarInsert(
            IHttpContextAccessor _httpContextAccessor,
            AgeFormasPagosSaveDAO ageFormasPagoSaveDAO)
        {

            if (ageFormasPagoSaveDAO == null)
                throw new InvalidSintaxisException();

            Validator.ValidateObject(ageFormasPagoSaveDAO, new ValidationContext(ageFormasPagoSaveDAO), true);

            FuncionesSecuencias fg = new FuncionesSecuencias(_ageLicenciatariosAplicaSecuService);

            ageFormasPagoSaveDAO.Id.Codigo = fg.ObtenerSecuencia(
                                                _httpContextAccessor,
                                                ageFormasPagoSaveDAO.Id.AgeLicencCodigo,
                                                Globales.CODIGO_SECUENCIA_FORMA_PAGO,
                                                ageFormasPagoSaveDAO.UsuarioIngreso);

            ValidarKeys(ageFormasPagoSaveDAO.Id);

            AgeFormasPagos entityObject = FromDAOToEntity(_httpContextAccessor, ageFormasPagoSaveDAO);

            Validator.ValidateObject(entityObject, new ValidationContext(entityObject), true);

            return entityObject;
        }

        private AgeFormasPagos ValidarUpdate(
            IHttpContextAccessor _httpContextAccessor,
            AgeFormasPagosSaveDAO ageFormasPagoSaveDAO)
        {
            if (ageFormasPagoSaveDAO == null)
                throw new InvalidSintaxisException();

            if (ageFormasPagoSaveDAO.UsuarioModificacion == null || ageFormasPagoSaveDAO.UsuarioModificacion <= 0)
                throw new InvalidFieldException("El usuario de modificación debe ser un número entero mayor a cero.");

            AgeFormasPagos? ageFormasPago = ConsultarCompletePorId(ageFormasPagoSaveDAO.Id) 
                ?? throw new RegisterNotFoundException("Forma de pago con código: " + ageFormasPagoSaveDAO.Id.Codigo + " no existe.");

            if (!string.IsNullOrWhiteSpace(ageFormasPagoSaveDAO.Estado))
            {
                ageFormasPago.Estado = ageFormasPagoSaveDAO.Estado;
                ageFormasPago.FechaEstado = DateTime.Now;
            }

            if (ageFormasPagoSaveDAO.CodigoInstitucionControl > 0)
                ageFormasPago.CodigoInstitucionControl = ageFormasPagoSaveDAO.CodigoInstitucionControl;

            if (!string.IsNullOrWhiteSpace(ageFormasPagoSaveDAO.Descripcion))
                ageFormasPago.Descripcion = ageFormasPagoSaveDAO.Descripcion;

            if (!string.IsNullOrWhiteSpace(ageFormasPagoSaveDAO.Retiene))
                ageFormasPago.Retiene = ageFormasPagoSaveDAO.Retiene;

            if (!string.IsNullOrWhiteSpace(ageFormasPagoSaveDAO.PresentaCajaBancos))
                ageFormasPago.PresentaCajaBancos = ageFormasPagoSaveDAO.PresentaCajaBancos;

            if (!string.IsNullOrWhiteSpace(ageFormasPagoSaveDAO.ObservacionEstado))
                ageFormasPago.ObservacionEstado = ageFormasPagoSaveDAO.ObservacionEstado;

            ageFormasPago.FechaModificacion = DateTime.Now;
            ageFormasPago.UbicacionModificacion = Ubicacion.getIpAdress(_httpContextAccessor);
            ageFormasPago.UsuarioModificacion = ageFormasPagoSaveDAO.UsuarioModificacion;

            Validator.ValidateObject(ageFormasPago, new ValidationContext(ageFormasPago), true);

            return ageFormasPago;
        }

        private AgeFormasPagos FromDAOToEntity(
            IHttpContextAccessor _httpContextAccessor,
            AgeFormasPagosSaveDAO ageFormasPagoSaveDAO)
        {
            return new AgeFormasPagos
            {
                AgeLicencCodigo = ageFormasPagoSaveDAO.Id.AgeLicencCodigo,
                Codigo = ageFormasPagoSaveDAO.Id.Codigo,
                Descripcion = ageFormasPagoSaveDAO.Descripcion,
                CodigoInstitucionControl = ageFormasPagoSaveDAO.CodigoInstitucionControl,
                Retiene = ageFormasPagoSaveDAO.Retiene,
                PresentaCajaBancos = ageFormasPagoSaveDAO.PresentaCajaBancos,
                Estado = ageFormasPagoSaveDAO.Estado,
                FechaEstado = DateTime.Now,
                FechaIngreso = DateTime.Now,
                ObservacionEstado = ageFormasPagoSaveDAO.ObservacionEstado,
                UbicacionIngreso = Ubicacion.getIpAdress(_httpContextAccessor),
                UsuarioIngreso = ageFormasPagoSaveDAO.UsuarioIngreso
            };
        }

        private void ValidarKeys(AgeFormasPagosPKDAO ageFormasPagoPKDAO)
        {
            ValidarPK(ageFormasPagoPKDAO);

            ValidarFKLicenciatario(ageFormasPagoPKDAO.AgeLicencCodigo);
        }

        private void ValidarPK(AgeFormasPagosPKDAO ageFormasPagoPKDAO)
        {
            ValidateKeys.ValidarNoExistenciaKey(
               ageFormasPagoPKDAO,
                $"Forma de pago con código: {ageFormasPagoPKDAO.Codigo} ya existe.",
                _repository.ConsultarCompletePorId);
        }

        private void ValidarFKLicenciatario(int codigoLicenciatario)
        {
            ValidateKeys.ValidarExistenciaKey(
                codigoLicenciatario,
                $"Licenciatario con código: {codigoLicenciatario} no existe.",
                _repositoryLicenciatario.ConsultarPorId);
        }

    }
}
