﻿using AGE.Entities.DAO.AgeFormasPago;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;

namespace AGE.Services.AgeFormasPagoService
{
    public interface IAgeFormasPagosService
    {

        Task<Page<AgeFormasPagosDAO>> ConsultarTodos(
            int codigoLicenciatario,
            int codigoInstitucionControl,
            string descripcion,
            Pageable pageable);

        Task<Page<AgeFormasPagosDAO>> ConsultarListaFiltro(
            int codigoLicenciatario,
            string filtro,
            Pageable pageable);

        Task<AgeFormasPagosDAO> ConsultarPorId(
            AgeFormasPagosPKDAO ageFormasPagoPKDAO);

        Task<Resource<AgeFormasPagosDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgeFormasPagosSaveDAO ageFormasPagoSaveDAO);

        Task<List<Resource<AgeFormasPagosDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeFormasPagosSaveDAO> ageFormasPagoSaveDAOList);

        Task<AgeFormasPagosDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeFormasPagosSaveDAO ageFormasPagoSaveDAO);

        Task<List<AgeFormasPagosDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeFormasPagosSaveDAO> ageFormasPagoSaveDAOList);

    }
}
