﻿using AGE.Controllers;
using AGE.Entities.DAO.AgeSubAplicaciones;
using AGE.Entities;
using AGE.Middleware.Exceptions.BadRequest;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Repositories.AgeSubAplicacionesRepository;
using AGE.Services.AgeLicenciatariosAplicaSecuService;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using AGE.Utils;
using System.ComponentModel.DataAnnotations;
using System.Transactions;
using AGE.Repositories.AgeLicenciatariosAplicacionRepository;
using AGE.Entities.DAO.AgeLicenciatarioAplicacion;

namespace AGE.Services.AgeSubAplicacionesService
{
    public class AgeSubAplicacionesService : IAgeSubAplicacionesService
    {
        private readonly IAgeSubAplicacionesRepository _repository;
        private readonly IAgeLicenciatarioAplicacionRepository _repositoryLicenciatariosAplicacion;
        private readonly IAgeLicenciatariosAplicaSecuService _ageLicenciatariosAplicaSecuService;

        public AgeSubAplicacionesService(IAgeSubAplicacionesRepository Repository,
            IAgeLicenciatarioAplicacionRepository RepositoryLicenciatariosAplicacion,
            IAgeLicenciatariosAplicaSecuService ageLicenciatariosAplicaSecuService)
        {
            _repository = Repository;
            _repositoryLicenciatariosAplicacion = RepositoryLicenciatariosAplicacion;
            _ageLicenciatariosAplicaSecuService = ageLicenciatariosAplicaSecuService;
        }

        public Task<Page<AgeSubAplicacionesDAO>> ConsultarTodos(int codigoLicenciatario,
                                                                int codigo,
                                                                int ageLicApAgeAplicaCodigo,
                                                                string contabiliza,
                                                                string descripcion,
                                                                Pageable pageable)
        {
            pageable.Validar<AgeSubAplicacionesDAO>();

            return _repository.ConsultarTodos(codigoLicenciatario, codigo, ageLicApAgeAplicaCodigo, contabiliza, descripcion, pageable);
        }

        public Task<AgeSubAplicacionesDAO> ConsultarPorId(
            AgeSubAplicacionesPKDAO ageSubAplicacionesPKDAO)
        {

            Validator.ValidateObject(ageSubAplicacionesPKDAO, new ValidationContext(ageSubAplicacionesPKDAO), true);

            AgeSubAplicacionesDAO ageSubAplicacionesDAO = _repository.ConsultarPorId(ageSubAplicacionesPKDAO).Result;

            return ageSubAplicacionesDAO == null
                ? throw new RegisterNotFoundException("Sub Aplicación con código: " + ageSubAplicacionesPKDAO.Codigo + " no existe.")
                : Task.FromResult(ageSubAplicacionesDAO);
        }

        private AgeSubAplicaciones ConsultarCompletePorId(AgeSubAplicacionesPKDAO ageSubAplicacionesPKDAO)
        {
            Validator.ValidateObject(ageSubAplicacionesPKDAO, new ValidationContext(ageSubAplicacionesPKDAO), true);

            AgeSubAplicaciones? ageSubAplicaciones = _repository.ConsultarCompletePorId(ageSubAplicacionesPKDAO).Result;

            return ageSubAplicaciones;
        }

        public Task<Page<AgeSubAplicacionesDAO>> ConsultarListaFiltro(int codigoLicenciatario,
                                                                    string filtro,
                                                                    Pageable pageable)
        {
            if (codigoLicenciatario == 0)
                throw new InvalidFieldException("El código de licenciatario debe ser un número entero mayor a cero.");

            if (string.IsNullOrWhiteSpace(filtro))
                throw new InvalidFieldException("El filtro es requerido.");

            pageable.Validar<AgeSubAplicacionesDAO>();

            return _repository.ConsultarListaFiltro(codigoLicenciatario, filtro.ToLower(), pageable);
        }

        public async Task<Resource<AgeSubAplicacionesDAO>> Insertar(IHttpContextAccessor _httpContextAccessor, AgeSubAplicacionesSaveDAO AgeSubAplicacionesSaveDAO)
        {

            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    AgeSubAplicaciones ageSubAplicaciones = ValidarInsert(_httpContextAccessor, AgeSubAplicacionesSaveDAO);
                    AgeSubAplicacionesDAO ageSubAplicacionesDAO = await _repository.Insertar(ageSubAplicaciones);

                    Resource<AgeSubAplicacionesDAO> ageSubAplicacionDAOWithResource = GetDataWithResource(_httpContextAccessor, ageSubAplicacionesDAO);

                    transactionScope.Complete();

                    return ageSubAplicacionDAOWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public async Task<List<Resource<AgeSubAplicacionesDAO>>> InsertarVarios(IHttpContextAccessor _httpContextAccessor, List<AgeSubAplicacionesSaveDAO> AgeSubAplicacionesSaveDAOList)
        {
            if (AgeSubAplicacionesSaveDAOList == null)
                throw new InvalidSintaxisException();

            List<AgeSubAplicaciones> ageSubAplicacionesList = new();

            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    foreach (AgeSubAplicacionesSaveDAO ageSubAplicacionesSaveDAO in AgeSubAplicacionesSaveDAOList)
                    {
                        AgeSubAplicaciones ageSubAplicacion = ValidarInsert(_httpContextAccessor, ageSubAplicacionesSaveDAO);
                        ageSubAplicacionesList.Add(ageSubAplicacion);
                    }

                    List<AgeSubAplicacionesDAO> ageSubAplicacionesDAOList = await _repository.InsertarVarios(ageSubAplicacionesList);

                    List<Resource<AgeSubAplicacionesDAO>> ageSubAplicacionesDAOListWithResource = new();

                    foreach (AgeSubAplicacionesDAO ageSubAplicacionDAO in ageSubAplicacionesDAOList)
                    {
                        ageSubAplicacionesDAOListWithResource.Add(GetDataWithResource(_httpContextAccessor, ageSubAplicacionDAO));
                    }

                    transactionScope.Complete();

                    return ageSubAplicacionesDAOListWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public Task<AgeSubAplicacionesDAO> Actualizar(IHttpContextAccessor _httpContextAccessor, AgeSubAplicacionesSaveDAO AgeSubAplicacionesSaveDAO)
        {
            AgeSubAplicaciones ageSubAplicacion = ValidarUpdate(_httpContextAccessor, AgeSubAplicacionesSaveDAO);

            return _repository.Actualizar(ageSubAplicacion);
        }

        public Task<List<AgeSubAplicacionesDAO>> ActualizarVarios(IHttpContextAccessor _httpContextAccessor, List<AgeSubAplicacionesSaveDAO> AgeSubAplicacionesSaveDAOList)
        {
            if (AgeSubAplicacionesSaveDAOList == null)
                throw new InvalidSintaxisException();

            ValidateKeys.ValidarPKDuplicadas(AgeSubAplicacionesSaveDAOList,
                                             p => new { p.Id.AgeLicApAgeAplicaCodigo, p.Id.AgeLicApAgeLicencCodigo, p.Id.Codigo },
                                             "Id");

            List<AgeSubAplicaciones> ageSubAplicacionesList = new();
            AgeSubAplicaciones ageSubAplicacion;

            foreach (AgeSubAplicacionesSaveDAO ageSubAplicacionesSaveDAO in AgeSubAplicacionesSaveDAOList)
            {
                ageSubAplicacion = ValidarUpdate(_httpContextAccessor, ageSubAplicacionesSaveDAO);
                ageSubAplicacionesList.Add(ageSubAplicacion);
            }

            return _repository.ActualizarVarios(ageSubAplicacionesList);
        }

        private Resource<AgeSubAplicacionesDAO> GetDataWithResource(IHttpContextAccessor _httpContextAccessor, AgeSubAplicacionesDAO ageSubAplicacionesDAO)
        {
            string rutaSegmentoFinal = $"{ageSubAplicacionesDAO.Id.AgeLicApAgeLicencCodigo}/{ageSubAplicacionesDAO.Id.AgeLicApAgeAplicaCodigo}/{ageSubAplicacionesDAO.Id.Codigo}";

            return Resource<AgeSubAplicacionesDAO>.GetDataWithResource<AgeSubAplicacionesController>(
                _httpContextAccessor, rutaSegmentoFinal, ageSubAplicacionesDAO);
        }

        private AgeSubAplicaciones ValidarInsert(IHttpContextAccessor _httpContextAccessor, AgeSubAplicacionesSaveDAO ageSubAplicacionesSaveDAO)
        {
            if (ageSubAplicacionesSaveDAO == null) throw new InvalidSintaxisException();

            Validator.ValidateObject(ageSubAplicacionesSaveDAO, new ValidationContext(ageSubAplicacionesSaveDAO), true);

            FuncionesSecuencias fg = new(_ageLicenciatariosAplicaSecuService);

            ageSubAplicacionesSaveDAO.Id.Codigo = fg.ObtenerSecuencia(_httpContextAccessor,
                ageSubAplicacionesSaveDAO.Id.AgeLicApAgeLicencCodigo,
                Globales.CODIGO_SECUENCIA_SUB_APLICACION,
                ageSubAplicacionesSaveDAO.UsuarioIngreso);

            ValidarKeys(ageSubAplicacionesSaveDAO);

            AgeSubAplicaciones entityObject = FromDAOToEntity(_httpContextAccessor, ageSubAplicacionesSaveDAO);

            Validator.ValidateObject(entityObject, new ValidationContext(entityObject), true);

            return entityObject;
        }

        private AgeSubAplicaciones ValidarUpdate(IHttpContextAccessor _httpContextAccessor, AgeSubAplicacionesSaveDAO ageSubAplicacionesSaveDAO)
        {
            if (ageSubAplicacionesSaveDAO == null)
                throw new InvalidSintaxisException();

            if (ageSubAplicacionesSaveDAO.UsuarioModificacion == null || ageSubAplicacionesSaveDAO.UsuarioModificacion <= 0)
                throw new InvalidFieldException("El usuario de modificación debe ser un número entero mayor a cero.");

            // Validar PK
            AgeSubAplicaciones? ageSubAplicacion = ConsultarCompletePorId(ageSubAplicacionesSaveDAO.Id) 
                ?? throw new RegisterNotFoundException("Sub Aplicación con código: " + ageSubAplicacionesSaveDAO.Id.Codigo + " no existe.");

            ValidarFKS(ageSubAplicacionesSaveDAO);

            // Modificar campos
            ageSubAplicacion.Estado = !string.IsNullOrWhiteSpace(ageSubAplicacionesSaveDAO.Estado) ? ageSubAplicacionesSaveDAO.Estado : ageSubAplicacion.Estado;
            ageSubAplicacion.FechaEstado = !string.IsNullOrWhiteSpace(ageSubAplicacionesSaveDAO.Estado) ? DateTime.Now : ageSubAplicacion.FechaEstado;
            ageSubAplicacion.ObservacionEstado = !string.IsNullOrWhiteSpace(ageSubAplicacionesSaveDAO.ObservacionEstado) ? ageSubAplicacionesSaveDAO.ObservacionEstado : ageSubAplicacion.ObservacionEstado;

            ageSubAplicacion.FechaModificacion = DateTime.Now;
            ageSubAplicacion.UbicacionModificacion = Ubicacion.getIpAdress(_httpContextAccessor);
            ageSubAplicacion.UsuarioModificacion = ageSubAplicacionesSaveDAO.UsuarioModificacion;

            ageSubAplicacion.Descripcion = !string.IsNullOrWhiteSpace(ageSubAplicacionesSaveDAO.Descripcion) ? ageSubAplicacionesSaveDAO.Descripcion : ageSubAplicacion.Descripcion;
            ageSubAplicacion.Contabiliza = !string.IsNullOrWhiteSpace(ageSubAplicacionesSaveDAO.Contabiliza) ? ageSubAplicacionesSaveDAO.Contabiliza : ageSubAplicacion.Contabiliza;
            ageSubAplicacion.AgageLicApAgeAplicaCodigo = ageSubAplicacionesSaveDAO.AgageLicApAgeAplicaCodigo != null && ageSubAplicacionesSaveDAO.AgageLicApAgeAplicaCodigo > 0
                ? ageSubAplicacionesSaveDAO.AgageLicApAgeAplicaCodigo : ageSubAplicacion.AgageLicApAgeAplicaCodigo;
            ageSubAplicacion.AgageLicApAgeLicencCodigo = ageSubAplicacionesSaveDAO.AgageLicApAgeLicencCodigo != null && ageSubAplicacionesSaveDAO.AgageLicApAgeAplicaCodigo > 0
                ? ageSubAplicacionesSaveDAO.AgageLicApAgeLicencCodigo : ageSubAplicacion.AgageLicApAgeLicencCodigo;
            ageSubAplicacion.AgeSubApCodigo = ageSubAplicacionesSaveDAO.AgeSubApCodigo != null && ageSubAplicacionesSaveDAO.AgageLicApAgeAplicaCodigo > 0
                ? ageSubAplicacionesSaveDAO.AgeSubApCodigo : ageSubAplicacion.AgeSubApCodigo;

            Validator.ValidateObject(ageSubAplicacion, new ValidationContext(ageSubAplicacion), true);

            return ageSubAplicacion;
        }

        private AgeSubAplicaciones FromDAOToEntity(
            IHttpContextAccessor _httpContextAccessor,
            AgeSubAplicacionesSaveDAO ageSubAplicacionesSaveDAO)
        {

            return new AgeSubAplicaciones()
            {
                AgeLicApAgeAplicaCodigo = ageSubAplicacionesSaveDAO.Id.AgeLicApAgeAplicaCodigo,
                Codigo = ageSubAplicacionesSaveDAO.Id.Codigo,
                AgeLicApAgeLicencCodigo = ageSubAplicacionesSaveDAO.Id.AgeLicApAgeLicencCodigo,
                Descripcion = ageSubAplicacionesSaveDAO.Descripcion,
                Contabiliza = ageSubAplicacionesSaveDAO.Contabiliza,
                AgageLicApAgeAplicaCodigo = ageSubAplicacionesSaveDAO.AgageLicApAgeAplicaCodigo,
                AgageLicApAgeLicencCodigo = ageSubAplicacionesSaveDAO.AgageLicApAgeLicencCodigo,
                AgeSubApCodigo = ageSubAplicacionesSaveDAO.AgeSubApCodigo,
                Estado = ageSubAplicacionesSaveDAO.Estado,
                FechaEstado = DateTime.Now,
                FechaIngreso = DateTime.Now,
                ObservacionEstado = ageSubAplicacionesSaveDAO.ObservacionEstado,
                UbicacionIngreso = Ubicacion.getIpAdress(_httpContextAccessor),
                UsuarioIngreso = ageSubAplicacionesSaveDAO.UsuarioIngreso
            };
        }

        private void ValidarKeys(AgeSubAplicacionesSaveDAO ageSubAplicacionesSaveDAO)
        {
            ValidarPK(ageSubAplicacionesSaveDAO.Id);
            ValidarFKS(ageSubAplicacionesSaveDAO);
        }

        private void ValidarPK(AgeSubAplicacionesPKDAO ageSubAplicacionesPKDAO)
        {
            ValidateKeys.ValidarNoExistenciaKey(ageSubAplicacionesPKDAO,
                    $"Sub Aplicación con código: {ageSubAplicacionesPKDAO.Codigo}," +
                    $" código de aplicación: {ageSubAplicacionesPKDAO.AgeLicApAgeAplicaCodigo}" +
                    $" y código de licenciatario: {ageSubAplicacionesPKDAO.AgeLicApAgeLicencCodigo} ya existe."
                    , _repository.ConsultarCompletePorId);
        }

        private void ValidarFKS(AgeSubAplicacionesSaveDAO ageSubAplicacionesSaveDAO)
        {
            // Validar FK Sub Aplicacion
            if ((ageSubAplicacionesSaveDAO.AgeSubApCodigo > 0 && ageSubAplicacionesSaveDAO.AgeSubApCodigo != null) ||
                (ageSubAplicacionesSaveDAO.AgageLicApAgeAplicaCodigo > 0 && ageSubAplicacionesSaveDAO.AgageLicApAgeAplicaCodigo != null) ||
                (ageSubAplicacionesSaveDAO.AgageLicApAgeLicencCodigo > 0 && ageSubAplicacionesSaveDAO.AgageLicApAgeLicencCodigo != null))
            {
                // Validar FK Sub Aplicacion
                AgeSubAplicacionesPKDAO ageSubAplicacionesPKDAO = new()
                {
                    AgeLicApAgeAplicaCodigo = ageSubAplicacionesSaveDAO.AgageLicApAgeAplicaCodigo ?? 0,
                    Codigo = ageSubAplicacionesSaveDAO.AgeSubApCodigo ?? 0,
                    AgeLicApAgeLicencCodigo = ageSubAplicacionesSaveDAO.AgageLicApAgeLicencCodigo ?? 0
                };

                ValidarFKSubAplicacion(ageSubAplicacionesPKDAO);
            }

            // FK licenciatario Aplicacion
            ValidarFKLicenciatarioAplicacion(ageSubAplicacionesSaveDAO);
        }

        private void ValidarFKSubAplicacion(AgeSubAplicacionesPKDAO ageSubAplicacionesPKDAO)
        {
            ValidateKeys.ValidarExistenciaKey(ageSubAplicacionesPKDAO,
                    $"Sub Aplicación con código: {ageSubAplicacionesPKDAO.Codigo}," +
                    $" código de aplicación: {ageSubAplicacionesPKDAO.AgeLicApAgeAplicaCodigo}" +
                    $" y código de licenciatario: {ageSubAplicacionesPKDAO.AgeLicApAgeLicencCodigo} no existe."
                    , _repository.ConsultarCompletePorId);
        }

        private void ValidarFKLicenciatarioAplicacion(AgeSubAplicacionesSaveDAO ageSubAplicacionesSaveDAO)
        {
            AgeLicenciatariosAplicacionesPKDAO ageLicenciatarioAplicacionPKDAO = new()
            {
                AgeLicencCodigo = ageSubAplicacionesSaveDAO.Id.AgeLicApAgeLicencCodigo,
                AgeAplicaCodigo = ageSubAplicacionesSaveDAO.Id.AgeLicApAgeAplicaCodigo
            };

            ValidateKeys.ValidarExistenciaKey(ageLicenciatarioAplicacionPKDAO,
                $"Licenciatario aplicación con código: {ageLicenciatarioAplicacionPKDAO.AgeAplicaCodigo} " +
                $"y código licenciatario: {ageLicenciatarioAplicacionPKDAO.AgeLicencCodigo} no existe."
                , _repositoryLicenciatariosAplicacion.ConsultarCompletePorId);
        }

    }
}
