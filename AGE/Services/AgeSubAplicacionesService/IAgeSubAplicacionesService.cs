﻿using AGE.Entities.DAO.AgeSubAplicaciones;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;

namespace AGE.Services.AgeSubAplicacionesService
{
    public interface IAgeSubAplicacionesService 
    {
        Task<Page<AgeSubAplicacionesDAO>> ConsultarTodos(int codigoLicenciatario,
                                                    int codigo,
                                                    int ageLicApAgeAplicaCodigo,
                                                    string contabiliza,
                                                    string descripcion,
                                                    Pageable pageable);

        Task<AgeSubAplicacionesDAO> ConsultarPorId(AgeSubAplicacionesPKDAO ageSubAplicacionesPKDAO);

        Task<Page<AgeSubAplicacionesDAO>> ConsultarListaFiltro(int codigoLicenciatario,
                                                                string filtro,
                                                                Pageable pageable);

        Task<Resource<AgeSubAplicacionesDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgeSubAplicacionesSaveDAO AgeSubAplicacionesSaveDAO);

        Task<List<Resource<AgeSubAplicacionesDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeSubAplicacionesSaveDAO> AgeSubAplicacionesSaveDAOList);

        Task<AgeSubAplicacionesDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeSubAplicacionesSaveDAO AgeSubAplicacionesSaveDAO);

        Task<List<AgeSubAplicacionesDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeSubAplicacionesSaveDAO> AgeSubAplicacionesSaveDAOList);
    }
}
