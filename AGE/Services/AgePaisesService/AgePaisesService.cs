﻿using AGE.Controllers;
using AGE.Entities;
using AGE.Entities.DAO.AgePaises;
using AGE.Middleware.Exceptions.BadRequest;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Repositories.AgePaisesRepository;
using AGE.Services.AgeSecuenciasPrimariasService;
using AGE.Utils;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using System.ComponentModel.DataAnnotations;
using System.Transactions;

namespace AGE.Services.AgePaisesService
{
    public class AgePaisesService : IAgePaisesService
    {
        
        private readonly IAgePaisesRepository _repository;
        private readonly IAgeSecuenciasPrimariasService _ageSecuenciasPrimariasService;


        public AgePaisesService(
            IAgePaisesRepository Repository, 
            IAgeSecuenciasPrimariasService ageSecuenciasPrimariasService)
        {
            _repository = Repository;
            _ageSecuenciasPrimariasService = ageSecuenciasPrimariasService;
        }

        public Task<Page<AgePaisesDAO>> ConsultarTodos( 
            int codigo,
            string descripcion, 
            Pageable pageable)
        {
            pageable.Validar<AgePaisesDAO>();

            return _repository.ConsultarTodos(codigo,descripcion,pageable);
        }

        public Task<AgePaisesDAO> ConsultarPorId(
            int codigo)
        {

            if(codigo <= 0)
                throw new InvalidIdException();

            AgePaisesDAO? agePaisesDAO = _repository.ConsultarPorId(codigo).Result;

            return agePaisesDAO == null
                ? throw new RegisterNotFoundException("País con código: " + codigo + " no existe.")
                : Task.FromResult(agePaisesDAO);
        }

        private AgePaises ConsultarCompletePorId(
            int codigo)
        {

            if (codigo <= 0)
                throw new InvalidIdException();

            return _repository.ConsultarCompletePorId(codigo).Result;
        }

        public Task<Page<AgePaisesDAO>> ConsultarListaFiltro(string filtro, Pageable pageable)
        {

            if (string.IsNullOrWhiteSpace(filtro))
                throw new InvalidFieldException("El filtro es requerido.");

            pageable.Validar<AgePaisesDAO>();

            return _repository.ConsultarListaFiltro(filtro, pageable);

        }


        public async Task<Resource<AgePaisesDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor, 
            AgePaisesSaveDAO agePaisesSaveDAO)
        {
            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    AgePaises agePaises = ValidarInsert(_httpContextAccessor, agePaisesSaveDAO);

                    AgePaisesDAO agePaisesDAO = await _repository.Insertar(agePaises);

                    Resource<AgePaisesDAO> agePaisesDAOWithResource = GetDataWithResource(_httpContextAccessor, agePaisesDAO);

                    transactionScope.Complete();

                    return agePaisesDAOWithResource;

                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public async Task<List<Resource<AgePaisesDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor, 
            List<AgePaisesSaveDAO> agePaisesSaveDAOList)
        {
            if (agePaisesSaveDAOList == null)
                throw new InvalidSintaxisException();

            List<AgePaises> agePaisesList = new List<AgePaises>();

            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    foreach (AgePaisesSaveDAO agePaisesSaveDAO in agePaisesSaveDAOList)
                    {
                        AgePaises AgePaise = ValidarInsert(_httpContextAccessor, agePaisesSaveDAO);
                        agePaisesList.Add(AgePaise);
                    }

                    List<AgePaisesDAO> agePaisesDAOList = await _repository.InsertarVarios(agePaisesList);

                    List<Resource<AgePaisesDAO>> agePaisesDAOListWithResource = new List<Resource<AgePaisesDAO>>();

                    foreach (AgePaisesDAO agePaisesDAO in agePaisesDAOList)
                    {
                        agePaisesDAOListWithResource.Add(GetDataWithResource(_httpContextAccessor, agePaisesDAO));
                    }

                    transactionScope.Complete();

                    return agePaisesDAOListWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public Task<AgePaisesDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgePaisesSaveDAO agePaisesSaveDAO)
        {
            AgePaises agePaises = ValidarUpdate(_httpContextAccessor, agePaisesSaveDAO);

            return _repository.Actualizar(agePaises);
        }

        public Task<List<AgePaisesDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor, 
            List<AgePaisesSaveDAO> agePaisesSaveDAOList)
        {
            if (agePaisesSaveDAOList == null)
                throw new InvalidSintaxisException();

            ValidateKeys.ValidarPKDuplicadas(agePaisesSaveDAOList, p => new { p.Codigo }, "Codigo");

            List<AgePaises> agePaisesList = new List<AgePaises>();
            AgePaises agePaises;

            foreach (AgePaisesSaveDAO ageRutasSaveDAO in agePaisesSaveDAOList)
            {
                agePaises = ValidarUpdate(_httpContextAccessor, ageRutasSaveDAO);
                agePaisesList.Add(agePaises);
            }

            return _repository.ActualizarVarios(agePaisesList);
        }

        private static Resource<AgePaisesDAO> GetDataWithResource(
            IHttpContextAccessor _httpContextAccessor,
            AgePaisesDAO agePaisesDAO)
        {
            string rutaSegmentoFinal = $"{agePaisesDAO.Codigo}";

            return Resource<AgePaisesDAO>.GetDataWithResource<AgePaisesController>(
                _httpContextAccessor, rutaSegmentoFinal, agePaisesDAO);
        }

        private AgePaises ValidarInsert(
            IHttpContextAccessor _httpContextAccessor, 
            AgePaisesSaveDAO agePaisesSaveDAO)
        {
            if (agePaisesSaveDAO == null)
                throw new InvalidSintaxisException();

            Validator.ValidateObject(agePaisesSaveDAO, new ValidationContext(agePaisesSaveDAO), true);

            FuncionesSecuencias fg = new FuncionesSecuencias(_ageSecuenciasPrimariasService);

            agePaisesSaveDAO.Codigo = fg.ObtenerSecuenciaPrimaria(
                        _httpContextAccessor,
                        Globales.CODIGO_SECUENCIA_PAIS,
                        agePaisesSaveDAO.UsuarioIngreso); 

            ValidarPK(agePaisesSaveDAO.Codigo);

            AgePaises entityObject = FromDAOToEntity(_httpContextAccessor,agePaisesSaveDAO);

            Validator.ValidateObject(entityObject, new ValidationContext(entityObject), true);

            return entityObject;
        }

        private AgePaises ValidarUpdate(
            IHttpContextAccessor _httpContextAccessor, 
            AgePaisesSaveDAO agePaisesSaveDAO)
        {
            if (agePaisesSaveDAO == null)
                throw new InvalidSintaxisException();

            if (agePaisesSaveDAO.UsuarioModificacion == null || agePaisesSaveDAO.UsuarioModificacion <= 0)
                throw new InvalidFieldException("El usuario de modificación debe ser un número entero mayor a cero.");

            AgePaises agePaises = ConsultarCompletePorId(agePaisesSaveDAO.Codigo) ?? 
                throw new RegisterNotFoundException($"País con código: {agePaisesSaveDAO.Codigo} no existe.");
            
            if (!string.IsNullOrWhiteSpace(agePaisesSaveDAO.Estado))
            {
                agePaises.Estado = agePaisesSaveDAO.Estado;
                agePaises.FechaEstado = DateTime.Now;
            }

            if (!string.IsNullOrWhiteSpace(agePaisesSaveDAO.Descripcion))
                agePaises.Descripcion = agePaisesSaveDAO.Descripcion;

            if (!string.IsNullOrWhiteSpace(agePaisesSaveDAO.ObservacionEstado))
                agePaises.ObservacionEstado = agePaisesSaveDAO.ObservacionEstado;

            agePaises.FechaModificacion = DateTime.Now;
            agePaises.UbicacionModificacion = Ubicacion.getIpAdress(_httpContextAccessor);
            agePaises.UsuarioModificacion = agePaisesSaveDAO.UsuarioModificacion;

            Validator.ValidateObject(agePaises, new ValidationContext(agePaises), true);

            return agePaises;
        }

        private AgePaises FromDAOToEntity(
            IHttpContextAccessor _httpContextAccessor,
            AgePaisesSaveDAO agePaisesSaveDAO)
        {

            return new AgePaises
            {
                Codigo = agePaisesSaveDAO.Codigo,
                Descripcion = agePaisesSaveDAO.Descripcion,
                Estado = agePaisesSaveDAO.Estado.ToUpper().ToString(),
                FechaEstado = DateTime.Now,
                FechaIngreso = DateTime.Now,
                ObservacionEstado = agePaisesSaveDAO.ObservacionEstado,
                UsuarioIngreso = agePaisesSaveDAO.UsuarioIngreso,
                UbicacionIngreso = Ubicacion.getIpAdress(_httpContextAccessor)
            };
        }

        private void ValidarPK(int codigoPais)
        {
            ValidateKeys.ValidarNoExistenciaKey(
               codigoPais,
               $"País con código: {codigoPais} ya existe.",
               _repository.ConsultarCompletePorId);
        }
    }
}
