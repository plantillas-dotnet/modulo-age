﻿
using AGE.Entities.DAO.AgePaises;
using AGE.Entities.DAO.AgePerfiles;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;

namespace AGE.Services.AgePaisesService
{
    public interface IAgePaisesService
    {
        Task<Page<AgePaisesDAO>> ConsultarTodos(
            int codigo,
            string descripcion,
            Pageable pageable);

        Task<Page<AgePaisesDAO>> ConsultarListaFiltro(
            string filtro,
            Pageable pageable);

        Task<AgePaisesDAO> ConsultarPorId(
            int codigo);

        Task<Resource<AgePaisesDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor, 
            AgePaisesSaveDAO agePaisesSaveDAO);

        Task<AgePaisesDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgePaisesSaveDAO agePaisesSaveDAO);

        Task<List<Resource<AgePaisesDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgePaisesSaveDAO> agePaisesSaveDAOList);

        Task<List<AgePaisesDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgePaisesSaveDAO> agePaisesSaveDAOList);
    }
}
