﻿using AGE.Entities;
using AGE.Entities.DAO.AgeLicenciatParamVigencia;
using AGE.Utils;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AGE.Services.AgeLicenciatParamVigenciaService
{
    public interface IAgeLicenciatParamVigenciasService
    {
        Task<Page<AgeLicenciatParamVigenciasDAO>> ConsultarTodos(
            int codigoLicenciatario,
            int? codigoParametro,
            int? id,
            string? observacion,
            string? valorParametro,
            Pageable pageable);

        Task<Page<AgeLicenciatParamVigenciasDAO>> ConsultarListaFiltro(
            int codigoLicenciatario,
            string filtro,
            Pageable pageable);

        Task<AgeLicenciatParamVigenciasDAO> ConsultarPorId(
            AgeLicenciatParamVigenciasPKDAO ageLicenciatParamVigenciaPKDAO);

        AgeLicenciatParamVigencias ConsultarCompletePorId(
            AgeLicenciatParamVigenciasPKDAO ageLicenciatParamVigenciaPKDAO);

        Task<Resource<AgeLicenciatParamVigenciasDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgeLicenciatParamVigenciasSaveDAO ageLicenciatParamVigenciaSaveDAO);

        Task<List<Resource<AgeLicenciatParamVigenciasDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeLicenciatParamVigenciasSaveDAO> ageLicenciatParamVigenciaSaveDAOList);

        Task<AgeLicenciatParamVigenciasDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeLicenciatParamVigenciasSaveDAO ageLicenciatParamVigenciaSaveDAO);

        Task<List<AgeLicenciatParamVigenciasDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeLicenciatParamVigenciasSaveDAO> ageLicenciatParamVigenciaSaveDAOList);
    }
}
