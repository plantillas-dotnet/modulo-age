﻿using AGE.Controllers;
using AGE.Entities;
using AGE.Entities.DAO.AgeLicenciatParamVigencia;
using AGE.Middleware.Exceptions.BadRequest;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Repositories.AgeLicenciatParamVigenciaRepository;
using AGE.Services.AgeLicenciatariosAplicaSecuService;
using AGE.Utils;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using System.ComponentModel.DataAnnotations;
using System.Transactions;
using AGE.Repositories.AgeLicenciatariosRepository;
using AGE.Repositories.AgeParametrosGeneralesRepository;

namespace AGE.Services.AgeLicenciatParamVigenciaService
{
    public class AgeLicenciatParamVigenciasService : IAgeLicenciatParamVigenciasService
    {
        private readonly IAgeLicenciatParamVigenciasRepository _repository;
        private readonly IAgeLicenciatariosAplicaSecuService _ageLicenciatariosAplicaSecuService;
        private readonly IAgeLicenciatariosRepository _repositoryLicenciatario;
        private readonly IAgeParametrosGeneralesRepository _repositoryParametroGeneral;

        public AgeLicenciatParamVigenciasService(
            IAgeLicenciatParamVigenciasRepository Repository,
            IAgeLicenciatariosAplicaSecuService ageLicenciatariosAplicaSecuService,
            IAgeLicenciatariosRepository ageLicenciatariosRepository,
            IAgeParametrosGeneralesRepository ageParametrosGeneralesRepository)
        {
            _repository = Repository;
            _ageLicenciatariosAplicaSecuService = ageLicenciatariosAplicaSecuService;
            _repositoryLicenciatario = ageLicenciatariosRepository;
            _repositoryParametroGeneral = ageParametrosGeneralesRepository;
        }


        public Task<Page<AgeLicenciatParamVigenciasDAO>> ConsultarTodos(
            int codigoLicenciatario,
            int? codigoParametro,
            int? id,
            string? observacion,
            string? valorParametro,
            Pageable pageable)
        {

            pageable.Validar<AgeLicenciatParamVigenciasDAO>();

            return _repository.ConsultarTodos(codigoLicenciatario, codigoParametro ?? 0, id ?? 0, observacion ?? "", valorParametro ?? "", pageable);
        }


        public Task<Page<AgeLicenciatParamVigenciasDAO>> ConsultarListaFiltro(
        int codigoLicenciatario,
        string filtro,
        Pageable pageable)
        {
            if (codigoLicenciatario <= 0)
            {
                throw new InvalidFieldException("El código licenciatario es requerido.");
            }

            if (string.IsNullOrWhiteSpace(filtro))
            {
                throw new InvalidFieldException("El filtro es requerido.");
            }

            pageable.Validar<AgeLicenciatParamVigenciasDAO>();

            return _repository.ConsultarListaFiltro(codigoLicenciatario, filtro.ToLower(), pageable);
        }

        public Task<AgeLicenciatParamVigenciasDAO> ConsultarPorId(
            AgeLicenciatParamVigenciasPKDAO ageLicenciatParamVigenciaPKDAO)
        {
            Validator.ValidateObject(ageLicenciatParamVigenciaPKDAO, new ValidationContext(ageLicenciatParamVigenciaPKDAO), true);

            AgeLicenciatParamVigenciasDAO? ageLicenciatParamVigenciaDAO = _repository.ConsultarPorId(ageLicenciatParamVigenciaPKDAO).Result;

            if (ageLicenciatParamVigenciaDAO == null)
            {
                throw new RegisterNotFoundException("Parámetro con código " + ageLicenciatParamVigenciaPKDAO.Codigo + " no existe");
            }

            return Task.FromResult(ageLicenciatParamVigenciaDAO);
        }

        public AgeLicenciatParamVigencias ConsultarCompletePorId(AgeLicenciatParamVigenciasPKDAO ageLicenciatParamVigenciaPKDAO)
        {
            Validator.ValidateObject(ageLicenciatParamVigenciaPKDAO, new ValidationContext(ageLicenciatParamVigenciaPKDAO), true);

            AgeLicenciatParamVigencias ageLicenciatParamVigencia = _repository.ConsultarCompletePorId(ageLicenciatParamVigenciaPKDAO).Result;

            return ageLicenciatParamVigencia;
        }

        public async Task<Resource<AgeLicenciatParamVigenciasDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgeLicenciatParamVigenciasSaveDAO ageLicenciatParamVigenciaSaveDAO)
        {
            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    AgeLicenciatParamVigencias ageLicenciatParamVigencia = ValidarInsert(_httpContextAccessor, ageLicenciatParamVigenciaSaveDAO);

                    AgeLicenciatParamVigenciasDAO ageLicenciatParamVigenciaDAO = await _repository.Insertar(ageLicenciatParamVigencia);

                    Resource<AgeLicenciatParamVigenciasDAO> ageLicenciatParamVigenciaDAOWithResource = GetDataWithResource(_httpContextAccessor, ageLicenciatParamVigenciaDAO);

                    transactionScope.Complete();

                    return ageLicenciatParamVigenciaDAOWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public async Task<List<Resource<AgeLicenciatParamVigenciasDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeLicenciatParamVigenciasSaveDAO> ageLicenciatParamVigenciaSaveDAOList)
        {
            if (ageLicenciatParamVigenciaSaveDAOList == null)
            {
                throw new InvalidSintaxisException();
            }

            List<AgeLicenciatParamVigencias> ageLicenciatParamVigenciaList = new List<AgeLicenciatParamVigencias>();

            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    foreach (AgeLicenciatParamVigenciasSaveDAO ageLicenciatParamVigenciaSaveDAO in ageLicenciatParamVigenciaSaveDAOList)
                    {
                        AgeLicenciatParamVigencias ageLicenciatParamVigencia = ValidarInsert(_httpContextAccessor, ageLicenciatParamVigenciaSaveDAO);
                        ageLicenciatParamVigenciaList.Add(ageLicenciatParamVigencia);
                    }

                    List<AgeLicenciatParamVigenciasDAO> ageLicenciatParamVigenciaDAOList = await _repository.InsertarVarios(ageLicenciatParamVigenciaList);

                    List<Resource<AgeLicenciatParamVigenciasDAO>> ageLicenciatParamVigenciaDAOListWithResource = new List<Resource<AgeLicenciatParamVigenciasDAO>>();

                    foreach (AgeLicenciatParamVigenciasDAO ageLicenciatParamVigenciaDAO in ageLicenciatParamVigenciaDAOList)
                    {
                        ageLicenciatParamVigenciaDAOListWithResource.Add(GetDataWithResource(_httpContextAccessor, ageLicenciatParamVigenciaDAO));
                    }

                    transactionScope.Complete();

                    return ageLicenciatParamVigenciaDAOListWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public Task<AgeLicenciatParamVigenciasDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeLicenciatParamVigenciasSaveDAO ageLicenciatParamVigenciaSaveDAO)
        {
            AgeLicenciatParamVigencias ageLicenciatParamVigencia = ValidarUpdate(_httpContextAccessor, ageLicenciatParamVigenciaSaveDAO);

            return _repository.Actualizar(ageLicenciatParamVigencia);
        }

        public Task<List<AgeLicenciatParamVigenciasDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeLicenciatParamVigenciasSaveDAO> ageLicenciatParamVigenciaSaveDAOList)
        {
            if (ageLicenciatParamVigenciaSaveDAOList == null)
                throw new InvalidSintaxisException();

            ValidateKeys.ValidarPKDuplicadas(ageLicenciatParamVigenciaSaveDAOList,
                                             p => new { p.Id.AgeLicencCodigo, p.Id.AgeParGeCodigo, p.Id.Codigo },
                                             "Id");

            List<AgeLicenciatParamVigencias> ageLicenciatParamVigenciaList = new List<AgeLicenciatParamVigencias>();
            AgeLicenciatParamVigencias ageLicenciatParamVigencia;

            foreach (AgeLicenciatParamVigenciasSaveDAO ageLicenciatParamVigenciaSaveDAO in ageLicenciatParamVigenciaSaveDAOList)
            {
                ageLicenciatParamVigencia = ValidarUpdate(_httpContextAccessor, ageLicenciatParamVigenciaSaveDAO);
                ageLicenciatParamVigenciaList.Add(ageLicenciatParamVigencia);
            }

            return _repository.ActualizarVarios(ageLicenciatParamVigenciaList);
        }

        private static Resource<AgeLicenciatParamVigenciasDAO> GetDataWithResource(
            IHttpContextAccessor _httpContextAccessor,
            AgeLicenciatParamVigenciasDAO ageLicenciatParamVigenciaDAO)
        {
            string idtipoSegmentoFinal = $"{ageLicenciatParamVigenciaDAO.Id.AgeLicencCodigo}/" +
                $"{ageLicenciatParamVigenciaDAO.Id.AgeParGeCodigo}/" +
                $"{ageLicenciatParamVigenciaDAO.Id.Codigo}";

            return Resource<AgeLicenciatParamVigenciasDAO>.GetDataWithResource<AgeLicenciatParamVigenciasController>(
                _httpContextAccessor, idtipoSegmentoFinal, ageLicenciatParamVigenciaDAO);
        }

        private AgeLicenciatParamVigencias ValidarInsert(
        IHttpContextAccessor _httpContextAccessor,
        AgeLicenciatParamVigenciasSaveDAO ageLicenciatParamVigenciaSaveDAO)
        {
            if (ageLicenciatParamVigenciaSaveDAO == null)
                throw new InvalidSintaxisException();

           

            Validator.ValidateObject(ageLicenciatParamVigenciaSaveDAO, new ValidationContext(ageLicenciatParamVigenciaSaveDAO), true);

            FuncionesSecuencias fg = new FuncionesSecuencias(_ageLicenciatariosAplicaSecuService);

            ageLicenciatParamVigenciaSaveDAO.Id.Codigo = fg.ObtenerSecuencia(
                                                            _httpContextAccessor,
                                                            ageLicenciatParamVigenciaSaveDAO.Id.AgeLicencCodigo,
                                                            Globales.CODIGO_SECUENCIA_LICEN_PARAM_VIGENCIA,
                                                            ageLicenciatParamVigenciaSaveDAO.UsuarioIngreso);

            ValidarKeys(ageLicenciatParamVigenciaSaveDAO);

            AgeLicenciatParamVigencias entityObject = FromDAOToEntity(_httpContextAccessor, ageLicenciatParamVigenciaSaveDAO);

            Validator.ValidateObject(entityObject, new ValidationContext(entityObject), true);

            return entityObject;
        }

        private AgeLicenciatParamVigencias ValidarUpdate(
            IHttpContextAccessor _httpContextAccessor,
            AgeLicenciatParamVigenciasSaveDAO ageLicenciatParamVigenciaSaveDAO)
        {
            if (ageLicenciatParamVigenciaSaveDAO == null)
                throw new InvalidSintaxisException();


            if (ageLicenciatParamVigenciaSaveDAO.UsuarioModificacion == null || ageLicenciatParamVigenciaSaveDAO.UsuarioModificacion <= 0)
                throw new InvalidFieldException("El usuario de modificación debe ser un número entero mayor a cero.");

            AgeLicenciatParamVigencias? ageLicenciatParamVigencia = ConsultarCompletePorId(new AgeLicenciatParamVigenciasPKDAO
            {
                AgeLicencCodigo = ageLicenciatParamVigenciaSaveDAO.Id.AgeLicencCodigo,
                AgeParGeCodigo = ageLicenciatParamVigenciaSaveDAO.Id.AgeParGeCodigo,
                Codigo = ageLicenciatParamVigenciaSaveDAO.Id.Codigo
            });

            ValidarFKs(ageLicenciatParamVigenciaSaveDAO);

            if (ageLicenciatParamVigencia == null)
                throw new RegisterNotFoundException("Parámetro con código " + ageLicenciatParamVigenciaSaveDAO.Id.Codigo + " no existe");

            if (!string.IsNullOrWhiteSpace(ageLicenciatParamVigenciaSaveDAO.Observacion))
                ageLicenciatParamVigencia.Observacion = ageLicenciatParamVigenciaSaveDAO.Observacion;

            if (ageLicenciatParamVigenciaSaveDAO.FechaDesde != default)
                ageLicenciatParamVigencia.FechaDesde = ageLicenciatParamVigenciaSaveDAO.FechaDesde;

            if (ageLicenciatParamVigenciaSaveDAO.FechaHasta.HasValue)
                ageLicenciatParamVigencia.FechaHasta = ageLicenciatParamVigenciaSaveDAO.FechaHasta;

            if (!string.IsNullOrWhiteSpace(ageLicenciatParamVigenciaSaveDAO.ValorParametro))
                ageLicenciatParamVigencia.ValorParametro = ageLicenciatParamVigenciaSaveDAO.ValorParametro;

            if (!string.IsNullOrWhiteSpace(ageLicenciatParamVigenciaSaveDAO.ObservacionEstado))
                ageLicenciatParamVigencia.ObservacionEstado = ageLicenciatParamVigenciaSaveDAO.ObservacionEstado;

            ageLicenciatParamVigencia.FechaModificacion = DateTime.Now;
            ageLicenciatParamVigencia.UbicacionModificacion = Ubicacion.getIpAdress(_httpContextAccessor);
            ageLicenciatParamVigencia.UsuarioModificacion = ageLicenciatParamVigenciaSaveDAO.UsuarioModificacion;

            Validator.ValidateObject(ageLicenciatParamVigencia, new ValidationContext(ageLicenciatParamVigencia), true);

            return ageLicenciatParamVigencia;
        }

        private AgeLicenciatParamVigencias FromDAOToEntity(
        IHttpContextAccessor _httpContextAccessor,
        AgeLicenciatParamVigenciasSaveDAO ageLicenciatParamVigenciaSaveDAO)
        {

            return new AgeLicenciatParamVigencias
            {
                AgeLicencCodigo = ageLicenciatParamVigenciaSaveDAO.Id.AgeLicencCodigo,
                AgeParGeCodigo = ageLicenciatParamVigenciaSaveDAO.Id.AgeParGeCodigo,
                Codigo = ageLicenciatParamVigenciaSaveDAO.Id.Codigo,
                Observacion = ageLicenciatParamVigenciaSaveDAO.Observacion,
                FechaDesde = ageLicenciatParamVigenciaSaveDAO.FechaDesde,
                FechaHasta = ageLicenciatParamVigenciaSaveDAO.FechaHasta,
                ValorParametro = ageLicenciatParamVigenciaSaveDAO.ValorParametro,
                Estado = ageLicenciatParamVigenciaSaveDAO.Estado,
                FechaEstado = DateTime.Now,
                FechaIngreso = DateTime.Now,
                ObservacionEstado = ageLicenciatParamVigenciaSaveDAO.ObservacionEstado,
                UbicacionIngreso = Ubicacion.getIpAdress(_httpContextAccessor),
                UsuarioIngreso = ageLicenciatParamVigenciaSaveDAO.UsuarioIngreso
            };
        }

        private void ValidarKeys(AgeLicenciatParamVigenciasSaveDAO ageLicenciatParamVigenciaSaveDAO)
        {
            ValidarPK(ageLicenciatParamVigenciaSaveDAO.Id);
            ValidarFKs(ageLicenciatParamVigenciaSaveDAO);
        }

        private void ValidarPK(AgeLicenciatParamVigenciasPKDAO ageLicenciatParamVigenciaPKDAO)
        {
            ValidateKeys.ValidarNoExistenciaKey(
               ageLicenciatParamVigenciaPKDAO,
               $"Parámetro Vigencia de Licenciatario con código {ageLicenciatParamVigenciaPKDAO.Codigo} ya existe.",
               _repository.ConsultarCompletePorId);
        }

        private void ValidarFKs(AgeLicenciatParamVigenciasSaveDAO ageLicenciatParamVigenciaSaveDAO)
        {
            ValidarFKLicenciatario(ageLicenciatParamVigenciaSaveDAO.Id.AgeLicencCodigo);
            ValidarFKParametroGeneral(ageLicenciatParamVigenciaSaveDAO.Id.AgeParGeCodigo);
        }

        private void ValidarFKLicenciatario(int codigoLicenciatario)
        {
            ValidateKeys.ValidarExistenciaKey(codigoLicenciatario,
                $"Licenciatario con código: {codigoLicenciatario} no existe.",
                _repositoryLicenciatario.ConsultarCompletePorId);
        }

        private void ValidarFKParametroGeneral(int codigoParametroGeneral)
        {
            ValidateKeys.ValidarExistenciaKey(codigoParametroGeneral,
                $"Parámetro general con código: {codigoParametroGeneral} no existe.",
                _repositoryParametroGeneral.ConsultarCompletePorId);
        }

    }

}

