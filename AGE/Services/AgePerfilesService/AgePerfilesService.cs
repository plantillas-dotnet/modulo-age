﻿using AGE.Controllers;
using AGE.Entities;
using AGE.Entities.DAO.AgePerfiles;
using AGE.Middleware.Exceptions.BadRequest;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Repositories.AgeLicenciatariosRepository;
using AGE.Repositories.AgePerfilesRepository;
using AGE.Services.AgeLicenciatariosAplicaSecuService;
using AGE.Utils;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using System.ComponentModel.DataAnnotations;
using System.Transactions;

namespace AGE.Services.AgePerfilesService
{
    public class AgePerfilesService : IAgePerfilesService
    {
        private readonly IAgePerfilesRepository _repository;
        private readonly IAgeLicenciatariosRepository _repositoryLicenciatario;
        private readonly IAgeLicenciatariosAplicaSecuService _ageLicenciatariosAplicaSecuService;

        public AgePerfilesService(
            IAgePerfilesRepository Repository,
            IAgeLicenciatariosRepository repositoryLicenciatario,
            IAgeLicenciatariosAplicaSecuService ageLicenciatariosAplicaSecuService)
        {
            _repository = Repository;
            _ageLicenciatariosAplicaSecuService = ageLicenciatariosAplicaSecuService;
            _repositoryLicenciatario = repositoryLicenciatario;
        }


        public Task<Page<AgePerfilesDAO>> ConsultarTodos(
            int codigoLicenciatario, 
            int codigo, 
            string descripcion, 
            Pageable pageable)
        {
            if (codigoLicenciatario <= 0)
                throw new InvalidFieldException("El código de licenciatario debe ser un número entero mayor a cero.");

            pageable.Validar<AgePerfilesDAO>();

            return _repository.ConsultarTodos(codigoLicenciatario, codigo, descripcion, pageable);
        }


        public Task<Page<AgePerfilesDAO>> ConsultarListaFiltro(
            int codigoLicenciatario, 
            string filtro, 
            Pageable pageable)
        {
            if (codigoLicenciatario == 0)
                throw new InvalidFieldException("El código de licenciatario debe ser un número entero mayor a cero.");

            if (string.IsNullOrWhiteSpace(filtro))
                throw new InvalidFieldException("El filtro es requerido.");

            pageable.Validar<AgePerfilesDAO>();

            return _repository.ConsultarListaFiltro(codigoLicenciatario, filtro.ToLower(), pageable);
        }

        public Task<AgePerfilesDAO> ConsultarPorId(
            AgePerfilesPKDAO agePerfilesPKDAO)
        {

            Validator.ValidateObject(agePerfilesPKDAO, new ValidationContext(agePerfilesPKDAO), true);

            AgePerfilesDAO agePerfilesDAO = _repository.ConsultarPorId(agePerfilesPKDAO).Result;

            if (agePerfilesDAO == null)
                throw new RegisterNotFoundException("Perfil con código " + agePerfilesPKDAO.codigo + " no existe");

            return Task.FromResult(agePerfilesDAO);
        }


        private AgePerfiles ConsultarCompletePorId(
            AgePerfilesPKDAO agePerfilesPKDAO)
        {
            Validator.ValidateObject(agePerfilesPKDAO, new ValidationContext(agePerfilesPKDAO), true);

            AgePerfiles agePerfiles = _repository.ConsultarCompletePorId(agePerfilesPKDAO).Result;

            return agePerfiles;
        }




        public async Task<Resource<AgePerfilesDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor, 
            AgePerfilesSaveDAO agePerfilesSaveDAO)
        {
            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    AgePerfiles agePerfiles = ValidarInsert(_httpContextAccessor, agePerfilesSaveDAO);

                    AgePerfilesDAO agePerfilesDAO = await _repository.Insertar(agePerfiles);
                
                    Resource<AgePerfilesDAO> agePerfilesDAOWithResource = GetDataWithResource(_httpContextAccessor, agePerfilesDAO);
                    
                    transactionScope.Complete();

                    return agePerfilesDAOWithResource;
                }
                catch (Exception)
                {
                    throw;
                }

            } 
        }

        public async Task<List<Resource<AgePerfilesDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgePerfilesSaveDAO> agePerfilesSaveDAOList)
        {
            if (agePerfilesSaveDAOList == null)
                throw new InvalidSintaxisException();

            List<AgePerfiles> agePerfilesList = new List<AgePerfiles>();

            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    foreach (AgePerfilesSaveDAO agePerfilesSaveDAO in agePerfilesSaveDAOList)
                    {
                        AgePerfiles agePerfile = ValidarInsert(_httpContextAccessor, agePerfilesSaveDAO);
                        agePerfilesList.Add(agePerfile);
                    }

                    List<AgePerfilesDAO> agePerfilesDAOList = await _repository.InsertarVarios(agePerfilesList);

                    List<Resource<AgePerfilesDAO>> agePerfilesDAOListWithResource = new List<Resource<AgePerfilesDAO>>();

                    foreach (AgePerfilesDAO agePerfilesDAO in agePerfilesDAOList)
                    {
                        agePerfilesDAOListWithResource.Add(GetDataWithResource(_httpContextAccessor, agePerfilesDAO));
                    }

                    transactionScope.Complete();

                    return agePerfilesDAOListWithResource;
                }
                catch (Exception)
                {   
                    throw;
                }
            }
        }



        public Task<AgePerfilesDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor, 
            AgePerfilesSaveDAO agePerfilesSaveDAO)
        {
            AgePerfiles agePerfiles = ValidarUpdate(_httpContextAccessor, agePerfilesSaveDAO);

            return _repository.Actualizar(agePerfiles);
        }


        public Task<List<AgePerfilesDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor, 
            List<AgePerfilesSaveDAO> agePerfilesSaveDAOList)
        {
            if (agePerfilesSaveDAOList == null)
                throw new InvalidSintaxisException();

            ValidateKeys.ValidarPKDuplicadas(agePerfilesSaveDAOList,
                                                p => new { p.Id.ageLicencCodigo, p.Id.codigo },
                                                "Id");

            List<AgePerfiles> agePerfilesList = new List<AgePerfiles>();

            foreach (AgePerfilesSaveDAO agePerfilesSaveDAO in agePerfilesSaveDAOList)
            {
                AgePerfiles agePerfiles = ValidarUpdate(_httpContextAccessor, agePerfilesSaveDAO);
                agePerfilesList.Add(agePerfiles);
            }

            return _repository.ActualizarVarios(agePerfilesList);
        }


        private static Resource<AgePerfilesDAO> GetDataWithResource(
            IHttpContextAccessor _httpContextAccessor, 
            AgePerfilesDAO agePerfilesDAO)
        {
            string rutaSegmentoFinal = $"{agePerfilesDAO.Id.ageLicencCodigo}/{agePerfilesDAO.Id.codigo}";

            return Resource<AgePerfilesDAO>.GetDataWithResource<AgePerfilesController>(
                _httpContextAccessor, rutaSegmentoFinal, agePerfilesDAO);
        }


        private AgePerfiles ValidarInsert(
            IHttpContextAccessor _httpContextAccessor,
            AgePerfilesSaveDAO agePerfilesSaveDAO)
        {

            if (agePerfilesSaveDAO == null)
                throw new InvalidSintaxisException();

            Validator.ValidateObject(agePerfilesSaveDAO, new ValidationContext(agePerfilesSaveDAO), true);

            FuncionesSecuencias fg = new FuncionesSecuencias(_ageLicenciatariosAplicaSecuService);

            agePerfilesSaveDAO.Id.codigo = fg.ObtenerSecuencia( _httpContextAccessor,
                                                                agePerfilesSaveDAO.Id.ageLicencCodigo,
                                                                Globales.CODIGO_SECUENCIA_PERFIL,
                                                                agePerfilesSaveDAO.UsuarioIngreso);

            ValidarKeys(agePerfilesSaveDAO);

            AgePerfiles entityObject = FromDAOToEntity(_httpContextAccessor, agePerfilesSaveDAO);

            Validator.ValidateObject(entityObject, new ValidationContext(entityObject), true);

            return entityObject;
        }


        private AgePerfiles ValidarUpdate(
            IHttpContextAccessor _httpContextAccessor, 
            AgePerfilesSaveDAO agePerfilesSaveDAO)
        {
            if (agePerfilesSaveDAO == null)
                throw new InvalidSintaxisException();

            if (agePerfilesSaveDAO.UsuarioModificacion == null || agePerfilesSaveDAO.UsuarioModificacion <= 0)
                throw new InvalidFieldException("El usuario de modificación debe ser un número entero mayor a cero.");

            AgePerfiles agePerfiles = ConsultarCompletePorId(new AgePerfilesPKDAO
            {
                ageLicencCodigo = agePerfilesSaveDAO.Id.ageLicencCodigo,
                codigo = agePerfilesSaveDAO.Id.codigo
            });

            if (agePerfiles == null)
                throw new RegisterNotFoundException("Perfil con código " + agePerfilesSaveDAO.Id.codigo + " no existe.");

            if (!string.IsNullOrWhiteSpace(agePerfilesSaveDAO.Estado))
            {
                agePerfiles.Estado = agePerfilesSaveDAO.Estado;
                agePerfiles.FechaEstado = DateTime.Now;
            }

            if (!string.IsNullOrWhiteSpace(agePerfilesSaveDAO.Descripcion))
                agePerfiles.Descripcion = agePerfilesSaveDAO.Descripcion;

            if (!string.IsNullOrWhiteSpace(agePerfilesSaveDAO.ObservacionEstado))
                agePerfiles.ObservacionEstado = agePerfilesSaveDAO.ObservacionEstado;

            agePerfiles.FechaModificacion = DateTime.Now;
            agePerfiles.UbicacionModificacion = Ubicacion.getIpAdress(_httpContextAccessor);
            agePerfiles.UsuarioModificacion = agePerfilesSaveDAO.UsuarioModificacion;

            Validator.ValidateObject(agePerfiles, new ValidationContext(agePerfiles), true);

            return agePerfiles;
        }


        private void ValidarKeys(AgePerfilesSaveDAO agePerfilesSaveDAO)
        {
            ValidarPK(agePerfilesSaveDAO.Id);

            ValidarFKLicenciatario(agePerfilesSaveDAO.Id.ageLicencCodigo);

        }

        private void ValidarPK(AgePerfilesPKDAO agePerfilesPKDAO)
        {

            ValidateKeys.ValidarNoExistenciaKey(
               agePerfilesPKDAO,
               $"Perfil con código {agePerfilesPKDAO.codigo} ya existe.",
               _repository.ConsultarCompletePorId);
        }


        private void ValidarFKLicenciatario(int codigoLicenciatario)
        {

            ValidateKeys.ValidarExistenciaKey(
                codigoLicenciatario,
                $"Licenciatario con código {codigoLicenciatario} no existe.",
                _repositoryLicenciatario.ConsultarCompletePorId);
        }

        private AgePerfiles FromDAOToEntity(
            IHttpContextAccessor _httpContextAccessor, 
            AgePerfilesSaveDAO agePerfilesSaveDAO)
        {
            return new AgePerfiles
            {
                AgeLicencCodigo = agePerfilesSaveDAO.Id.ageLicencCodigo,
                Codigo = agePerfilesSaveDAO.Id.codigo,
                Descripcion = agePerfilesSaveDAO.Descripcion,
                Estado = agePerfilesSaveDAO.Estado,
                FechaEstado = DateTime.Now,
                FechaIngreso = DateTime.Now,
                ObservacionEstado = agePerfilesSaveDAO.ObservacionEstado,
                UbicacionIngreso = Ubicacion.getIpAdress(_httpContextAccessor),
                UsuarioIngreso = agePerfilesSaveDAO.UsuarioIngreso
            };
        }


    }
}
