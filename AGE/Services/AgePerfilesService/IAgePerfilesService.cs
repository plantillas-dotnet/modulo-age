﻿using AGE.Entities.DAO.AgePerfiles;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using Microsoft.EntityFrameworkCore.Storage;

namespace AGE.Services.AgePerfilesService
{
    public interface IAgePerfilesService
    {
        Task<Page<AgePerfilesDAO>> ConsultarTodos(
            int codigoLicenciatario,
            int codigo,
            string descripcion,
            Pageable pageable);

        Task<Page<AgePerfilesDAO>> ConsultarListaFiltro(
            int codigoLicenciatario,
            string filtro,
            Pageable pageable);

        Task<AgePerfilesDAO> ConsultarPorId(AgePerfilesPKDAO AgePerfilesPKDAO);

        Task<Resource<AgePerfilesDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgePerfilesSaveDAO AgePerfilesSaveDAO);

        Task<List<Resource<AgePerfilesDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgePerfilesSaveDAO> AgePerfilesSaveDAOList);

        Task<AgePerfilesDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgePerfilesSaveDAO AgePerfilesSaveDAO);

        Task<List<AgePerfilesDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgePerfilesSaveDAO> AgePerfilesSaveDAOList);
    }
}
