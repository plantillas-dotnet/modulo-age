﻿using AGE.Controllers;
using AGE.Entities;
using AGE.Entities.DAO.AgeLicenciatariosPaises;
using AGE.Middleware.Exceptions.BadRequest;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Repositories.AgeLicenciatariosPaisesRepository;
using AGE.Repositories.AgeLicenciatariosRepository;
using AGE.Repositories.AgePaisesRepository;
using AGE.Utils;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using System.ComponentModel.DataAnnotations;
using System.Transactions;

namespace AGE.Services.AgeLicenciatariosPaiseService
{
    public class AgeLicenciatariosPaisesService : IAgeLicenciatariosPaisesService
    {
        private readonly IAgeLicenciatariosPaisesRepository _repository;
        private readonly IAgePaisesRepository _repositoryPais;
        private readonly IAgeLicenciatariosRepository _repositoryLicenciatario;
        public AgeLicenciatariosPaisesService(
            IAgeLicenciatariosPaisesRepository Repository,
            IAgePaisesRepository repositoryPais,
            IAgeLicenciatariosRepository repositoryLicenciatario)
        {
            _repository = Repository;
            _repositoryPais = repositoryPais;
            _repositoryLicenciatario = repositoryLicenciatario;
        }


        public Task<Page<AgeLicenciatariosPaisesDAO>> ConsultarTodos(
            int codigoLicenciatario,
            int? codigoPais,
            string? principal,
            int? AgeLicPaageLicencCodigo,
            int? AgeLicPaagePaisCodigo,
            Pageable pageable)
        {

            pageable.Validar<AgeLicenciatariosPaisesDAO>();

            return _repository.ConsultarTodos(codigoLicenciatario, codigoPais ?? 0, principal ?? "",
                                              AgeLicPaageLicencCodigo ?? 0, AgeLicPaagePaisCodigo ?? 0, pageable);
        }

        public Task<Page<AgeLicenciatariosPaisesDAO>> ConsultarListaFiltro(
            string filtro,
            Pageable pageable)
        {

            if (string.IsNullOrWhiteSpace(filtro))
            {
                throw new InvalidFieldException("El filtro es requerido.");
            }

            pageable.Validar<AgeLicenciatariosPaisesDAO>();

            return _repository.ConsultarListaFiltro(filtro.ToLower(), pageable);
        }

        public Task<AgeLicenciatariosPaisesDAO> ConsultarPorId(
            AgeLicenciatariosPaisesPKDAO ageLicenciatariosPaisePKDAO)
        {
            Validator.ValidateObject(ageLicenciatariosPaisePKDAO, new ValidationContext(ageLicenciatariosPaisePKDAO), true);

            AgeLicenciatariosPaisesDAO? ageLicenciatariosPaiseDAO = _repository.ConsultarPorId(ageLicenciatariosPaisePKDAO).Result;

            if (ageLicenciatariosPaiseDAO == null)
            {
                throw new RegisterNotFoundException("País con código " + ageLicenciatariosPaisePKDAO.agePaisCodigo + " no existe");
            }

            return Task.FromResult(ageLicenciatariosPaiseDAO);
        }

        public AgeLicenciatariosPaise ConsultarCompletePorId(AgeLicenciatariosPaisesPKDAO ageLicenciatariosPaisesPKDAO)
        {
            Validator.ValidateObject(ageLicenciatariosPaisesPKDAO, new ValidationContext(ageLicenciatariosPaisesPKDAO), true);

            AgeLicenciatariosPaise ageLicenciatariosPaises = _repository.ConsultarCompletePorId(ageLicenciatariosPaisesPKDAO).Result;

            return ageLicenciatariosPaises;
        }

        public async Task<Resource<AgeLicenciatariosPaisesDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgeLicenciatariosPaisesSaveDAO ageLicenciatariosPaiseSaveDAO)
        {
            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    AgeLicenciatariosPaise ageLicenciatariosPaise = ValidarInsert(_httpContextAccessor, ageLicenciatariosPaiseSaveDAO);

                    AgeLicenciatariosPaisesDAO ageLicenciatariosPaiseDAO = await _repository.Insertar(ageLicenciatariosPaise);

                    Resource<AgeLicenciatariosPaisesDAO> ageLicenciatariosPaiseDAOWithResource = GetDataWithResource(_httpContextAccessor, ageLicenciatariosPaiseDAO);

                    transactionScope.Complete();

                    return ageLicenciatariosPaiseDAOWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }
        public async Task<List<Resource<AgeLicenciatariosPaisesDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeLicenciatariosPaisesSaveDAO> ageLicenciatariosPaiseSaveDAOList)
        {
            if (ageLicenciatariosPaiseSaveDAOList == null)
            {
                throw new InvalidSintaxisException();
            }

            List<AgeLicenciatariosPaise> ageLicenciatariosPaiseList = new List<AgeLicenciatariosPaise>();

            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    foreach (AgeLicenciatariosPaisesSaveDAO ageLicenciatariosPaiseSaveDAO in ageLicenciatariosPaiseSaveDAOList)
                    {
                        AgeLicenciatariosPaise ageMensaje = ValidarInsert(_httpContextAccessor, ageLicenciatariosPaiseSaveDAO);
                        ageLicenciatariosPaiseList.Add(ageMensaje);
                    }

                    List<AgeLicenciatariosPaisesDAO> ageLicenciatariosPaiseDAOList = await _repository.InsertarVarios(ageLicenciatariosPaiseList);

                    List<Resource<AgeLicenciatariosPaisesDAO>> ageLicenciatariosPaiseDAOListWithResource = new List<Resource<AgeLicenciatariosPaisesDAO>>();

                    foreach (AgeLicenciatariosPaisesDAO ageLicenciatariosPaiseDAO in ageLicenciatariosPaiseDAOList)
                    {
                        ageLicenciatariosPaiseDAOListWithResource.Add(GetDataWithResource(_httpContextAccessor, ageLicenciatariosPaiseDAO));
                    }

                    transactionScope.Complete();

                    return ageLicenciatariosPaiseDAOListWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public Task<AgeLicenciatariosPaisesDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeLicenciatariosPaisesSaveDAO ageLicenciatariosPaiseSaveDAO)
        {
            AgeLicenciatariosPaise ageLicenciatariosPaise = ValidarUpdate(_httpContextAccessor, ageLicenciatariosPaiseSaveDAO);

            return _repository.Actualizar(ageLicenciatariosPaise);
        }

        public Task<List<AgeLicenciatariosPaisesDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeLicenciatariosPaisesSaveDAO> ageLicenciatariosPaiseSaveDAOList)
        {
            if (ageLicenciatariosPaiseSaveDAOList == null)
                throw new InvalidSintaxisException();

            ValidateKeys.ValidarPKDuplicadas(ageLicenciatariosPaiseSaveDAOList,
                                             p => new { p.Id.ageLicencCodigo, p.Id.agePaisCodigo },
                                             "Id");

            List<AgeLicenciatariosPaise> ageLicenciatariosPaiseList = new List<AgeLicenciatariosPaise>();
            AgeLicenciatariosPaise ageLicenciatariosPaise;

            foreach (AgeLicenciatariosPaisesSaveDAO ageLicenciatariosPaiseSaveDAO in ageLicenciatariosPaiseSaveDAOList)
            {
                ageLicenciatariosPaise = ValidarUpdate(_httpContextAccessor, ageLicenciatariosPaiseSaveDAO);
                ageLicenciatariosPaiseList.Add(ageLicenciatariosPaise);
            }

            return _repository.ActualizarVarios(ageLicenciatariosPaiseList);
        }

        private static Resource<AgeLicenciatariosPaisesDAO> GetDataWithResource(
            IHttpContextAccessor _httpContextAccessor,
            AgeLicenciatariosPaisesDAO ageLicenciatariosPaiseDAO)
        {
            string idtipoSegmentoFinal = $"{ageLicenciatariosPaiseDAO.Id.ageLicencCodigo}/{ageLicenciatariosPaiseDAO.Id.agePaisCodigo}";

            return Resource<AgeLicenciatariosPaisesDAO>.GetDataWithResource<AgeLicenciatariosPaisesController>(
                _httpContextAccessor, idtipoSegmentoFinal, ageLicenciatariosPaiseDAO);
        }



        private AgeLicenciatariosPaise ValidarInsert(
            IHttpContextAccessor _httpContextAccessor,
            AgeLicenciatariosPaisesSaveDAO ageLicenciatariosPaiseSaveDAO)
        {
            if (ageLicenciatariosPaiseSaveDAO == null)
                throw new InvalidSintaxisException();

            Validator.ValidateObject(ageLicenciatariosPaiseSaveDAO, new ValidationContext(ageLicenciatariosPaiseSaveDAO), true);

            ValidarKeys(ageLicenciatariosPaiseSaveDAO);

            AgeLicenciatariosPaise entityObject = FromDAOToEntity(_httpContextAccessor, ageLicenciatariosPaiseSaveDAO);

            Validator.ValidateObject(entityObject, new ValidationContext(entityObject), true);

            return entityObject;
        }



        private AgeLicenciatariosPaise ValidarUpdate(
            IHttpContextAccessor _httpContextAccessor,
            AgeLicenciatariosPaisesSaveDAO ageLicenciatariosPaiseSaveDAO)
        {
            if (ageLicenciatariosPaiseSaveDAO == null)
            {
                throw new InvalidSintaxisException();
            }

            if (ageLicenciatariosPaiseSaveDAO.UsuarioModificacion == null || ageLicenciatariosPaiseSaveDAO.UsuarioModificacion <= 0)
            {
                throw new InvalidFieldException("El usuario de modificación debe ser un número entero mayor a cero.");
            }

            AgeLicenciatariosPaise? ageLicenciatariosPaise = ConsultarCompletePorId(new AgeLicenciatariosPaisesPKDAO
            {
                ageLicencCodigo = ageLicenciatariosPaiseSaveDAO.Id.ageLicencCodigo,
                agePaisCodigo = ageLicenciatariosPaiseSaveDAO.Id.agePaisCodigo
            });

            if (ageLicenciatariosPaise == null)
            {
                throw new RegisterNotFoundException("Licenciatario con código " + ageLicenciatariosPaiseSaveDAO.Id.ageLicencCodigo + " e País con código "
                    + ageLicenciatariosPaiseSaveDAO.Id.agePaisCodigo + " no existe");
            }

            if (!string.IsNullOrWhiteSpace(ageLicenciatariosPaiseSaveDAO.Estado))
            {
                ageLicenciatariosPaise.Estado = ageLicenciatariosPaiseSaveDAO.Estado;
                ageLicenciatariosPaise.FechaEstado = DateTime.Now;
            }

            if (ageLicenciatariosPaiseSaveDAO.AgeLicPaAgeLicencCodigo != null && ageLicenciatariosPaiseSaveDAO.AgeLicPaAgeLicencCodigo > 0)
                ageLicenciatariosPaise.AgeLicPaAgeLicencCodigo = ageLicenciatariosPaiseSaveDAO.AgeLicPaAgeLicencCodigo;

            if (ageLicenciatariosPaiseSaveDAO.AgeLicPaAgePaisCodigo != null && ageLicenciatariosPaiseSaveDAO.AgeLicPaAgePaisCodigo > 0)
                ageLicenciatariosPaise.AgeLicPaAgePaisCodigo = ageLicenciatariosPaiseSaveDAO.AgeLicPaAgePaisCodigo;

            if (!string.IsNullOrWhiteSpace(ageLicenciatariosPaiseSaveDAO.Principal))
                ageLicenciatariosPaise.Principal = ageLicenciatariosPaiseSaveDAO.Principal;

            if (!string.IsNullOrWhiteSpace(ageLicenciatariosPaiseSaveDAO.ObservacionEstado))
                ageLicenciatariosPaise.ObservacionEstado = ageLicenciatariosPaiseSaveDAO.ObservacionEstado;

            ageLicenciatariosPaise.FechaModificacion = DateTime.Now;
            ageLicenciatariosPaise.UbicacionModificacion = Ubicacion.getIpAdress(_httpContextAccessor);
            ageLicenciatariosPaise.UsuarioModificacion = ageLicenciatariosPaiseSaveDAO.UsuarioModificacion;

            Validator.ValidateObject(ageLicenciatariosPaise, new ValidationContext(ageLicenciatariosPaise), true);

            return ageLicenciatariosPaise;
        }

        private AgeLicenciatariosPaise FromDAOToEntity(
        IHttpContextAccessor _httpContextAccessor,
        AgeLicenciatariosPaisesSaveDAO ageLicenciatariosPaiseSaveDAO)
        {
            return new AgeLicenciatariosPaise
            {
                AgeLicencCodigo = ageLicenciatariosPaiseSaveDAO.Id.ageLicencCodigo,
                AgePaisCodigo = ageLicenciatariosPaiseSaveDAO.Id.agePaisCodigo,
                Principal = ageLicenciatariosPaiseSaveDAO.Principal,
                AgeLicPaAgeLicencCodigo = ageLicenciatariosPaiseSaveDAO.AgeLicPaAgeLicencCodigo,
                AgeLicPaAgePaisCodigo = ageLicenciatariosPaiseSaveDAO.AgeLicPaAgePaisCodigo,
                Estado = ageLicenciatariosPaiseSaveDAO.Estado,
                FechaEstado = DateTime.Now,
                FechaIngreso = DateTime.Now,
                ObservacionEstado = ageLicenciatariosPaiseSaveDAO.ObservacionEstado,
                UbicacionIngreso = Ubicacion.getIpAdress(_httpContextAccessor),
                UsuarioIngreso = ageLicenciatariosPaiseSaveDAO.UsuarioIngreso
            };
        }

        private void ValidarKeys(AgeLicenciatariosPaisesSaveDAO ageLicenciatariosPaiseSaveDAO)
        {
            ValidarPK(ageLicenciatariosPaiseSaveDAO.Id);
            ValidarFKLicenciatario(ageLicenciatariosPaiseSaveDAO.Id.ageLicencCodigo);
            ValidarFKPais(ageLicenciatariosPaiseSaveDAO.Id.agePaisCodigo);

            //if (ageFormasPagosInstFinaSaveDAO.AgeFranquCodigo != null)
            //    ValidarFKFranquicias(ageFormasPagosInstFinaSaveDAO.AgeFranquCodigo ?? 0);

            if ((ageLicenciatariosPaiseSaveDAO.AgeLicPaAgeLicencCodigo != null) ||
                (ageLicenciatariosPaiseSaveDAO.AgeLicPaAgePaisCodigo != null))
                ValidarFKLicenciatarioPaises(ageLicenciatariosPaiseSaveDAO.AgeLicPaAgeLicencCodigo ?? 0,
                                                 ageLicenciatariosPaiseSaveDAO.AgeLicPaAgePaisCodigo ?? 0);

        }

        private void ValidarPK(AgeLicenciatariosPaisesPKDAO ageLicenciatariosPaisePKDAO)
        {
            ValidateKeys.ValidarNoExistenciaKey(
            ageLicenciatariosPaisePKDAO,
                 $"Licenciatario con código {ageLicenciatariosPaisePKDAO.ageLicencCodigo} para País con código" +
                 $" {ageLicenciatariosPaisePKDAO.agePaisCodigo} ya existe.",
                _repository.ConsultarCompletePorId);
        }

        private void ValidarFKLicenciatario(int codigoLicenciatario)
        {

            ValidateKeys.ValidarExistenciaKey(
                codigoLicenciatario,
                $"Licenciatario con código {codigoLicenciatario} no existe.",
                _repositoryLicenciatario.ConsultarCompletePorId);
        }

        private void ValidarFKPais(int codigoPais)
        {

            ValidateKeys.ValidarExistenciaKey(
                codigoPais,
                $"País con código {codigoPais} no existe.",
                _repositoryPais.ConsultarCompletePorId);
        }

        private void ValidarFKLicenciatarioPaises(int codigoLicenciatario, int codigoPais)
        {

            AgeLicenciatariosPaisesPKDAO ageLicenciatariosPaisesPKDAO = new AgeLicenciatariosPaisesPKDAO
            {
                ageLicencCodigo = codigoLicenciatario,
                agePaisCodigo = codigoPais
            };

            ValidateKeys.ValidarExistenciaKey(
               ageLicenciatariosPaisesPKDAO,
               $"Licenciatario con código {ageLicenciatariosPaisesPKDAO.ageLicencCodigo} " +
               $"y País con código {ageLicenciatariosPaisesPKDAO.agePaisCodigo} no existe.",
               _repository.ConsultarCompletePorId);

        }

    }
}

