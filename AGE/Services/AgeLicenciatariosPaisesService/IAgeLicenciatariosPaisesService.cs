﻿using AGE.Entities;
using AGE.Entities.DAO.AgeLicenciatariosPaises;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AGE.Services.AgeLicenciatariosPaiseService
{
    public interface IAgeLicenciatariosPaisesService
    {
        Task<Page<AgeLicenciatariosPaisesDAO>> ConsultarTodos(
            int codigoLicenciatario,
            int? codigoPais,
            string? principal,
            int? AgeLicPaAgeLicencCodigo,
            int? AgeLicPaAgePaisCodigo,
            Pageable pageable);

        Task<Page<AgeLicenciatariosPaisesDAO>> ConsultarListaFiltro(
            string filtro,
            Pageable pageable);

        Task<AgeLicenciatariosPaisesDAO> ConsultarPorId(
            AgeLicenciatariosPaisesPKDAO ageLicenciatariosPaisePKDAO);

        AgeLicenciatariosPaise ConsultarCompletePorId(AgeLicenciatariosPaisesPKDAO ageLicenciatariosPaisesPKDAO);

        Task<Resource<AgeLicenciatariosPaisesDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgeLicenciatariosPaisesSaveDAO ageLicenciatariosPaiseSaveDAO);

        Task<List<Resource<AgeLicenciatariosPaisesDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeLicenciatariosPaisesSaveDAO> ageLicenciatariosPaiseSaveDAOList);

        Task<AgeLicenciatariosPaisesDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeLicenciatariosPaisesSaveDAO ageLicenciatariosPaiseSaveDAO);

        Task<List<AgeLicenciatariosPaisesDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeLicenciatariosPaisesSaveDAO> ageLicenciatariosPaiseSaveDAOList);
    }
}
