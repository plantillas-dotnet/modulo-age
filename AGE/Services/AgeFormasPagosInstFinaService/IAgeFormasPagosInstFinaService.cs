﻿using AGE.Entities.DAO.AgeFormasPagosInstFina;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;

namespace AGE.Services.AgeFormasPagoInstFinaService
{
    public interface IAgeFormasPagosInstFinaService
    {

        Task<Page<AgeFormasPagosInstFinaDAO>> ConsultarTodos(
            int codigoLicenciatario,
            int ageForPaAgeLicencCodigo,
            int ageForPaCodigo,
            string descripcion,
            Pageable pageable);

        Task<Page<AgeFormasPagosInstFinaDAO>> ConsultarListaFiltro(
            int codigoLicenciatario,
            string filtro,
            Pageable pageable);

        Task<AgeFormasPagosInstFinaDAO> ConsultarPorId(
            AgeFormasPagosInstFinaPKDAO ageFormasPagosInstFinaPKDAO);

        Task<Resource<AgeFormasPagosInstFinaDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgeFormasPagosInstFinaSaveDAO ageFormasPagosInstFinaSaveDAO);

        Task<List<Resource<AgeFormasPagosInstFinaDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeFormasPagosInstFinaSaveDAO> ageFormasPagosInstFinaSaveDAOList);

        Task<AgeFormasPagosInstFinaDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeFormasPagosInstFinaSaveDAO ageFormasPagosInstFinaSaveDAO);

        Task<List<AgeFormasPagosInstFinaDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeFormasPagosInstFinaSaveDAO> ageFormasPagosInstFinaSaveDAOList);

    }
}
