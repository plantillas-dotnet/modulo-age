﻿using AGE.Controllers;
using AGE.Entities;
using AGE.Entities.DAO.AgeFormasPago;
using AGE.Entities.DAO.AgeFormasPagosInstFina;
using AGE.Entities.DAO.AgeInstitucionesFinancieras;
using AGE.Middleware.Exceptions.BadRequest;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Repositories.AgeFormasPagoRepository;
using AGE.Repositories.AgeFormasPagosInstFinasInstFinaRepository;
using AGE.Repositories.AgeFranquiciasRepository;
using AGE.Repositories.AgeInstitucionesFinancierasRepository;
using AGE.Repositories.AgeLicenciatariosRepository;
using AGE.Services.AgeFormasPagoInstFinaService;
using AGE.Services.AgeLicenciatariosAplicaSecuService;
using AGE.Utils;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using System.ComponentModel.DataAnnotations;
using System.Transactions;

namespace AGE.Services.AgeFormasPagosInstFinaService
{
    public class AgeFormasPagosInstFinaService : IAgeFormasPagosInstFinaService
    {

        private readonly IAgeFormasPagosInstFinaRepository _repository;
        private readonly IAgeFormasPagosRepository _repositoryFormasPago;
        private readonly IAgeLicenciatariosRepository _repositoryLicenciatario;
        private readonly IAgeInstitucionesFinancieraRepository _repositoryInstFina;
        private readonly IAgeLicenciatariosAplicaSecuService _ageLicenciatariosAplicaSecuService;
        private readonly IAgeFranquiciasRepository _repositoryFranquicias;


        public AgeFormasPagosInstFinaService(
            IAgeFormasPagosRepository repositoryFormasPago,
            IAgeLicenciatariosRepository repositoryLicenciatario,
            IAgeLicenciatariosAplicaSecuService ageLicenciatariosAplicaSecuService,
            IAgeInstitucionesFinancieraRepository repositoryInstFina,
            IAgeFranquiciasRepository repositoryFranquicias,
            IAgeFormasPagosInstFinaRepository repository)
        {
            _repositoryFormasPago = repositoryFormasPago;            
            _ageLicenciatariosAplicaSecuService = ageLicenciatariosAplicaSecuService;
            _repositoryLicenciatario = repositoryLicenciatario;
            _repositoryInstFina = repositoryInstFina;
            _repositoryFranquicias = repositoryFranquicias;
            _repository = repository;
        }

        public Task<Page<AgeFormasPagosInstFinaDAO>> ConsultarTodos(
            int codigoLicenciatario, 
            int ageForPaAgeLicencCodigo, 
            int ageForPaCodigo, 
            string descripcion, 
            Pageable pageable)
        {
            if (codigoLicenciatario <= 0)
                throw new InvalidFieldException("El código de licenciatario debe ser un número entero mayor a cero.");

            pageable.Validar<AgeFormasPagosInstFinaDAO>();

            return _repository.ConsultarTodos(codigoLicenciatario, ageForPaAgeLicencCodigo, ageForPaCodigo, descripcion, pageable);
        }

        public Task<AgeFormasPagosInstFinaDAO> ConsultarPorId(
            AgeFormasPagosInstFinaPKDAO ageFormasPagosInstFinaPKDAO)
        {
            Validator.ValidateObject(ageFormasPagosInstFinaPKDAO, new ValidationContext(ageFormasPagosInstFinaPKDAO), true);

            AgeFormasPagosInstFinaDAO ageFormasPagosInstFinaDAO = _repository.ConsultarPorId(ageFormasPagosInstFinaPKDAO).Result;

            if (ageFormasPagosInstFinaDAO == null)
                throw new RegisterNotFoundException("Institución financiera con código " + ageFormasPagosInstFinaPKDAO.Codigo + " no existe");

            return Task.FromResult(ageFormasPagosInstFinaDAO);
        }


        private AgeFormasPagosInstFina ConsultarCompletePorId(
           AgeFormasPagosInstFinaPKDAO ageFormasPagosInstFinaPKDAO)
        {
            Validator.ValidateObject(ageFormasPagosInstFinaPKDAO, new ValidationContext(ageFormasPagosInstFinaPKDAO), true);

            AgeFormasPagosInstFina? ageFormasPagosInstFina = _repository.ConsultarCompletePorId(ageFormasPagosInstFinaPKDAO).Result;

            return ageFormasPagosInstFina;
        }


        public Task<Page<AgeFormasPagosInstFinaDAO>> ConsultarListaFiltro(
            int codigoLicenciatario, 
            string filtro, 
            Pageable pageable)
        {
            if (codigoLicenciatario == 0)
                throw new InvalidFieldException("El código de licenciatario debe ser un número entero mayor a cero.");

            if (string.IsNullOrWhiteSpace(filtro))
                throw new InvalidFieldException("El filtro es requerido.");

            pageable.Validar<AgeFormasPagosInstFinaDAO>();

            return _repository.ConsultarListaFiltro(codigoLicenciatario, filtro.ToLower(), pageable);
        }


        public async Task<Resource<AgeFormasPagosInstFinaDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor, 
            AgeFormasPagosInstFinaSaveDAO ageFormasPagosInstFinaSaveDAO)
        {
            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    AgeFormasPagosInstFina ageFormasPagosInstFina = ValidarInsert(_httpContextAccessor, ageFormasPagosInstFinaSaveDAO);

                    AgeFormasPagosInstFinaDAO ageFormasPagosInstFinaDAO = await _repository.Insertar(ageFormasPagosInstFina);

                    Resource<AgeFormasPagosInstFinaDAO> ageFormasPagosInstFinaDAOWithResource = GetDataWithResource(_httpContextAccessor, ageFormasPagosInstFinaDAO);
                    
                    transactionScope.Complete();
                    
                    return ageFormasPagosInstFinaDAOWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }


        public async Task<List<Resource<AgeFormasPagosInstFinaDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor, 
            List<AgeFormasPagosInstFinaSaveDAO> ageFormasPagosInstFinaSaveDAOList)
        {
            if (ageFormasPagosInstFinaSaveDAOList == null)
            {
                throw new InvalidSintaxisException();
            }

            List<AgeFormasPagosInstFina> ageFormasPagosInstFinaList = new List<AgeFormasPagosInstFina>();

            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    foreach (AgeFormasPagosInstFinaSaveDAO ageFormasPagosInstFinaSaveDAO in ageFormasPagosInstFinaSaveDAOList)
                    {
                        AgeFormasPagosInstFina ageMensaje = ValidarInsert(_httpContextAccessor, ageFormasPagosInstFinaSaveDAO);
                        ageFormasPagosInstFinaList.Add(ageMensaje);
                    }

                    List<AgeFormasPagosInstFinaDAO> ageMensajesDAOList = await _repository.InsertarVarios(ageFormasPagosInstFinaList);

                    List<Resource<AgeFormasPagosInstFinaDAO>> ageMensajesDAOListWithResource = new List<Resource<AgeFormasPagosInstFinaDAO>>();

                    foreach (AgeFormasPagosInstFinaDAO ageFormasPagosInstFinaDAO in ageMensajesDAOList)
                    {
                        ageMensajesDAOListWithResource.Add(GetDataWithResource(_httpContextAccessor, ageFormasPagosInstFinaDAO));
                    }

                    transactionScope.Complete();

                    return ageMensajesDAOListWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }


        public Task<AgeFormasPagosInstFinaDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor, 
            AgeFormasPagosInstFinaSaveDAO ageFormasPagosInstFinaSaveDAO)
        {
            AgeFormasPagosInstFina ageFormasPagosInstFina = ValidarUpdate(_httpContextAccessor, ageFormasPagosInstFinaSaveDAO);

            return _repository.Actualizar(ageFormasPagosInstFina);
        }


        public Task<List<AgeFormasPagosInstFinaDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor, 
            List<AgeFormasPagosInstFinaSaveDAO> ageFormasPagosInstFinaSaveDAOList)
        {
            if (ageFormasPagosInstFinaSaveDAOList == null)
                throw new InvalidSintaxisException();

            ValidateKeys.ValidarPKDuplicadas(ageFormasPagosInstFinaSaveDAOList,
                                             p => new { p.Id.AgeLicencCodigo, p.Id.Codigo },
                                             "Id");

            List<AgeFormasPagosInstFina> ageFormasPagosInstFinaList = new List<AgeFormasPagosInstFina>();
            AgeFormasPagosInstFina ageFormasPagosInstFina;

            foreach (AgeFormasPagosInstFinaSaveDAO ageFormasPagosInstFinaSaveDAO in ageFormasPagosInstFinaSaveDAOList)
            {
                ageFormasPagosInstFina = ValidarUpdate(_httpContextAccessor, ageFormasPagosInstFinaSaveDAO);
                ageFormasPagosInstFinaList.Add(ageFormasPagosInstFina);
            }

            return _repository.ActualizarVarios(ageFormasPagosInstFinaList);
        }


        private static Resource<AgeFormasPagosInstFinaDAO> GetDataWithResource(
            IHttpContextAccessor _httpContextAccessor,
            AgeFormasPagosInstFinaDAO ageFormasPagosInstFinaDAO)
        {
            string rutaSegmentoFinal = $"{ageFormasPagosInstFinaDAO.Id.AgeLicencCodigo}/{ageFormasPagosInstFinaDAO.Id.Codigo}";

            return Resource<AgeFormasPagosInstFinaDAO>.GetDataWithResource<AgeFormasPagosInstFinaController>(
                _httpContextAccessor, rutaSegmentoFinal, ageFormasPagosInstFinaDAO);
        }


        private AgeFormasPagosInstFina ValidarInsert(
            IHttpContextAccessor _httpContextAccessor,
            AgeFormasPagosInstFinaSaveDAO ageFormasPagosInstFinaSaveDAO)
        {

            if (ageFormasPagosInstFinaSaveDAO == null)
                throw new InvalidSintaxisException();

            Validator.ValidateObject(ageFormasPagosInstFinaSaveDAO, new ValidationContext(ageFormasPagosInstFinaSaveDAO), true);

            FuncionesSecuencias fg = new FuncionesSecuencias(_ageLicenciatariosAplicaSecuService);
            
            ageFormasPagosInstFinaSaveDAO.Id.Codigo = fg.ObtenerSecuencia(
                                                        _httpContextAccessor, 
                                                        ageFormasPagosInstFinaSaveDAO.Id.AgeLicencCodigo, 
                                                        Globales.CODIGO_SECUENCIA_FORMA_PAGO_INST_FINAN, 
                                                        ageFormasPagosInstFinaSaveDAO.UsuarioIngreso);

            ValidarKeys(ageFormasPagosInstFinaSaveDAO);

            AgeFormasPagosInstFina entityObject = FromDAOToEntity(_httpContextAccessor, ageFormasPagosInstFinaSaveDAO);

            Validator.ValidateObject(entityObject, new ValidationContext(entityObject), true);

            return entityObject;
        }


        private AgeFormasPagosInstFina ValidarUpdate(
            IHttpContextAccessor _httpContextAccessor,
            AgeFormasPagosInstFinaSaveDAO ageFormasPagosInstFinaSaveDAO)
        {

            if (ageFormasPagosInstFinaSaveDAO == null)
                throw new InvalidSintaxisException();

            if (ageFormasPagosInstFinaSaveDAO.UsuarioModificacion == null || ageFormasPagosInstFinaSaveDAO.UsuarioModificacion <= 0)
                throw new InvalidFieldException("El usuario de modificación debe ser un número entero mayor a cero.");


            AgeFormasPagosInstFina? ageFormasPagosInstFina = ConsultarCompletePorId(new AgeFormasPagosInstFinaPKDAO
            {
                AgeLicencCodigo = ageFormasPagosInstFinaSaveDAO.Id.AgeLicencCodigo,
                Codigo = ageFormasPagosInstFinaSaveDAO.Id.Codigo
            }) ?? throw new RegisterNotFoundException("Institución financiera con código " + ageFormasPagosInstFinaSaveDAO.Id.Codigo + " no existe");


            if (!string.IsNullOrWhiteSpace(ageFormasPagosInstFinaSaveDAO.Estado))
            {
                ageFormasPagosInstFina.Estado = ageFormasPagosInstFinaSaveDAO.Estado;
                ageFormasPagosInstFina.FechaEstado = DateTime.Now;
            }

            if ((ageFormasPagosInstFinaSaveDAO.AgeForPaAgeLicencCodigo > 0) ||
                (ageFormasPagosInstFinaSaveDAO.AgeForPaCodigo > 0))
            {
                ValidarFKFormasPago(ageFormasPagosInstFinaSaveDAO.AgeForPaAgeLicencCodigo,
                                    ageFormasPagosInstFinaSaveDAO.AgeForPaCodigo);
                ageFormasPagosInstFina.AgeForPaAgeLicencCodigo = ageFormasPagosInstFinaSaveDAO.AgeForPaAgeLicencCodigo;
                ageFormasPagosInstFina.AgeForPaCodigo = ageFormasPagosInstFinaSaveDAO.AgeForPaCodigo;
            }
                
            if (ageFormasPagosInstFinaSaveDAO.AgeFranquCodigo != null)
            {
                ValidarFKFranquicias(ageFormasPagosInstFinaSaveDAO.AgeFranquCodigo ?? 0);
                ageFormasPagosInstFina.AgeFranquCodigo = ageFormasPagosInstFinaSaveDAO.AgeFranquCodigo;
            }

            if ((ageFormasPagosInstFinaSaveDAO.AgeInsFiAgeLicencCodigo != null) ||
                (ageFormasPagosInstFinaSaveDAO.AgeInsFiCodigo != null))
            {
                ValidarFKInstitucionesFinanciera(ageFormasPagosInstFinaSaveDAO.AgeInsFiAgeLicencCodigo ?? 0,
                                                 ageFormasPagosInstFinaSaveDAO.AgeInsFiCodigo ?? 0);
                ageFormasPagosInstFina.AgeInsFiAgeLicencCodigo = ageFormasPagosInstFinaSaveDAO.AgeInsFiAgeLicencCodigo;
                ageFormasPagosInstFina.AgeInsFiCodigo = ageFormasPagosInstFinaSaveDAO.AgeInsFiCodigo;
            }

            if (!string.IsNullOrWhiteSpace(ageFormasPagosInstFinaSaveDAO.Descripcion))
                ageFormasPagosInstFina.Descripcion = ageFormasPagosInstFinaSaveDAO.Descripcion;

            if (!string.IsNullOrWhiteSpace(ageFormasPagosInstFinaSaveDAO.ObservacionEstado))
                ageFormasPagosInstFina.ObservacionEstado = ageFormasPagosInstFinaSaveDAO.ObservacionEstado;

            ageFormasPagosInstFina.FechaModificacion = DateTime.Now;
            ageFormasPagosInstFina.UbicacionModificacion = Ubicacion.getIpAdress(_httpContextAccessor);
            ageFormasPagosInstFina.UsuarioModificacion = ageFormasPagosInstFinaSaveDAO.UsuarioModificacion;

            Validator.ValidateObject(ageFormasPagosInstFina, new ValidationContext(ageFormasPagosInstFina), true);

            return ageFormasPagosInstFina;
        }

        private AgeFormasPagosInstFina FromDAOToEntity(
            IHttpContextAccessor _httpContextAccessor,
            AgeFormasPagosInstFinaSaveDAO ageFormasPagosInstFinaSaveDAO)
        {

            return new AgeFormasPagosInstFina
            {
                AgeLicencCodigo = ageFormasPagosInstFinaSaveDAO.Id.AgeLicencCodigo,
                Codigo = ageFormasPagosInstFinaSaveDAO.Id.Codigo,
                Descripcion = ageFormasPagosInstFinaSaveDAO.Descripcion,
                AgeForPaAgeLicencCodigo = ageFormasPagosInstFinaSaveDAO.AgeForPaAgeLicencCodigo,
                AgeForPaCodigo = ageFormasPagosInstFinaSaveDAO.AgeForPaCodigo,
                AgeInsFiAgeLicencCodigo = ageFormasPagosInstFinaSaveDAO.AgeInsFiAgeLicencCodigo,
                AgeInsFiCodigo = ageFormasPagosInstFinaSaveDAO.AgeInsFiCodigo,
                AgeFranquCodigo = ageFormasPagosInstFinaSaveDAO.AgeFranquCodigo,
                Estado = ageFormasPagosInstFinaSaveDAO.Estado,
                FechaEstado = DateTime.Now,
                FechaIngreso = DateTime.Now,
                ObservacionEstado = ageFormasPagosInstFinaSaveDAO.ObservacionEstado,
                UbicacionIngreso = Ubicacion.getIpAdress(_httpContextAccessor),
                UsuarioIngreso = ageFormasPagosInstFinaSaveDAO.UsuarioIngreso
            };
        }


        private void ValidarKeys(AgeFormasPagosInstFinaSaveDAO ageFormasPagosInstFinaSaveDAO)
        {
            ValidarPK(ageFormasPagosInstFinaSaveDAO.Id);

            ValidarFKLicenciatario(ageFormasPagosInstFinaSaveDAO.Id.AgeLicencCodigo);

            ValidarFKFormasPago(ageFormasPagosInstFinaSaveDAO.AgeForPaAgeLicencCodigo,
                                ageFormasPagosInstFinaSaveDAO.AgeForPaCodigo);

            if (ageFormasPagosInstFinaSaveDAO.AgeFranquCodigo != null)
                ValidarFKFranquicias(ageFormasPagosInstFinaSaveDAO.AgeFranquCodigo ?? 0);

            if ((ageFormasPagosInstFinaSaveDAO.AgeInsFiAgeLicencCodigo != null) ||
                (ageFormasPagosInstFinaSaveDAO.AgeInsFiCodigo != null))
                ValidarFKInstitucionesFinanciera(ageFormasPagosInstFinaSaveDAO.AgeInsFiAgeLicencCodigo ?? 0 ,
                                                 ageFormasPagosInstFinaSaveDAO.AgeInsFiCodigo ?? 0);
        }


        private void ValidarPK(AgeFormasPagosInstFinaPKDAO ageFormasPagosInstFinaPKDAO)
        {

            ValidateKeys.ValidarNoExistenciaKey(
               ageFormasPagosInstFinaPKDAO,
               $"Forma de pago institución financiera con código {ageFormasPagosInstFinaPKDAO.Codigo} ya existe.",
               _repository.ConsultarCompletePorId);
        }


        private void ValidarFKLicenciatario(int codigoLicenciatario)
        {

            ValidateKeys.ValidarExistenciaKey(
                codigoLicenciatario,
                $"Licenciatario con código {codigoLicenciatario} no existe.",
                _repositoryLicenciatario.ConsultarCompletePorId);
        }


        private void ValidarFKFormasPago(int codigoLicenciatario, int codigo)
        {
            
            AgeFormasPagosPKDAO ageFormasPagoPKDAO = new AgeFormasPagosPKDAO
            {
                AgeLicencCodigo = codigoLicenciatario,
                Codigo = codigo
            };

            ValidateKeys.ValidarExistenciaKey(
               ageFormasPagoPKDAO,
               $"Forma de pago con código {ageFormasPagoPKDAO.Codigo} no existe.",
               _repositoryFormasPago.ConsultarCompletePorId);
        }

        private void ValidarFKInstitucionesFinanciera(int codigoLicenciatario, int codigo)
        {
            
            AgeInstitucionesFinancieraPKDAO ageInstitucionesFinancieraPKDAO = new AgeInstitucionesFinancieraPKDAO
            {
                AgeLicencCodigo = codigoLicenciatario,
                Codigo = codigo
            };

            ValidateKeys.ValidarExistenciaKey(
               ageInstitucionesFinancieraPKDAO,
               $"Institución financiera con código {ageInstitucionesFinancieraPKDAO.Codigo} no existe.",
               _repositoryInstFina.ConsultarCompletePorId);

        }

        private void ValidarFKFranquicias(int codigoFranquicia)
        {
            
            ValidateKeys.ValidarExistenciaKey(
               codigoFranquicia,
               $"Franquicia con código {codigoFranquicia} no existe.",
               _repositoryFranquicias.ConsultarCompletePorId);
        }

    }
}
