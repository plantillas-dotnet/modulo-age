﻿using AGE.Entities.DAO.AgeFirmasDigitales;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;

namespace AGE.Services.AgeFirmasDigitalesService
{
    public interface IAgeFirmasDigitalesService
    {
        Task<Page<AgeFirmasDigitalesDAO>> ConsultarTodos(int codigoLicenciatario,
                                                    string? nombres,
                                                    string? apellidos,
                                                    string? tipoFirmaDigital,
                                                    Pageable pageable);

        Task<AgeFirmasDigitalesDAO> ConsultarPorId(AgeFirmasDigitalesPKDAO ageFirmasDigitalesPKDAO);

        Task<Page<AgeFirmasDigitalesDAO>> ConsultarListaFiltro(int codigoLicenciatario,
                                                            string filtro,
                                                            Pageable pageable);

        Task<Resource<AgeFirmasDigitalesDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgeFirmasDigitalesSaveDAO AgeFirmasDigitalesSaveDAO);

        Task<List<Resource<AgeFirmasDigitalesDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeFirmasDigitalesSaveDAO> AgeFirmasDigitalesSaveDAOList);

        Task<AgeFirmasDigitalesDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeFirmasDigitalesSaveDAO AgeFirmasDigitalesSaveDAO);

        Task<List<AgeFirmasDigitalesDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeFirmasDigitalesSaveDAO> AgeFirmasDigitalesSaveDAOList);
    }
}
