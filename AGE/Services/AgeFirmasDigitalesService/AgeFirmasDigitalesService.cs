﻿using AGE.Controllers;
using AGE.Entities.DAO.AgeFirmasDigitales;
using AGE.Entities;
using AGE.Middleware.Exceptions.BadRequest;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Repositories.AgeFirmasDigitalesRepository;
using AGE.Services.AgeLicenciatariosAplicaSecuService;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using AGE.Utils;
using System.ComponentModel.DataAnnotations;
using System.Transactions;
using AGE.Entities.DAO.AgeSucursales;
using AGE.Repositories.AgeSucursalesRepository;
using AGE.Repositories.AgeLicenciatariosRepository;

namespace AGE.Services.AgeFirmasDigitalesService
{
    public class AgeFirmasDigitalesService : IAgeFirmasDigitalesService
    {
        private readonly IAgeFirmasDigitalesRepository _repository;
        private readonly IAgeSucursalesRepository _repositorySucursales;
        private readonly IAgeLicenciatariosRepository _repositoryLicenciatarios;
        private readonly IAgeLicenciatariosAplicaSecuService _ageLicenciatariosAplicaSecuService;

        public AgeFirmasDigitalesService(IAgeFirmasDigitalesRepository Repository,
            IAgeSucursalesRepository RepositorySucursales,
            IAgeLicenciatariosRepository RepositoryLicenciatarios,
            IAgeLicenciatariosAplicaSecuService ageLicenciatariosAplicaSecuService)
        {
            _repository = Repository;
            _repositoryLicenciatarios = RepositoryLicenciatarios;
            _repositorySucursales = RepositorySucursales;
            _ageLicenciatariosAplicaSecuService = ageLicenciatariosAplicaSecuService;
        }

        public Task<Page<AgeFirmasDigitalesDAO>> ConsultarTodos(int codigoLicenciatario,
                                                                string? nombres,
                                                                string? apellidos,
                                                                string? tipoFirmaDigital,
                                                                Pageable pageable)
        {
            pageable.Validar<AgeFirmasDigitalesDAO>();

            return _repository.ConsultarTodos(codigoLicenciatario, nombres, apellidos, tipoFirmaDigital, pageable);
        }

        public Task<AgeFirmasDigitalesDAO> ConsultarPorId(
            AgeFirmasDigitalesPKDAO ageFirmasDigitalesPKDAO)
        {

            Validator.ValidateObject(ageFirmasDigitalesPKDAO, new ValidationContext(ageFirmasDigitalesPKDAO), true);

            AgeFirmasDigitalesDAO ageFirmasDigitalesDAO = _repository.ConsultarPorId(ageFirmasDigitalesPKDAO).Result;

            return ageFirmasDigitalesDAO == null
                ? throw new RegisterNotFoundException("Firmas Digitales con código " + ageFirmasDigitalesPKDAO.Codigo + " no existe")
                : Task.FromResult(ageFirmasDigitalesDAO);
        }

        private AgeFirmasDigitales ConsultarCompletePorId(AgeFirmasDigitalesPKDAO ageFirmasDigitalesPKDAO)
        {
            Validator.ValidateObject(ageFirmasDigitalesPKDAO, new ValidationContext(ageFirmasDigitalesPKDAO), true);

            AgeFirmasDigitales? ageFirmasDigitale = _repository.ConsultarCompletePorId(ageFirmasDigitalesPKDAO).Result;

            return ageFirmasDigitale;
        }

        public Task<Page<AgeFirmasDigitalesDAO>> ConsultarListaFiltro(int codigoLicenciatario,
                                                                    string filtro,
                                                                    Pageable pageable)
        {
            if (codigoLicenciatario == 0)
            {
                throw new InvalidFieldException("El código de licenciatario debe ser un número entero mayor a cero.");
            }

            if (string.IsNullOrWhiteSpace(filtro))
            {
                throw new InvalidFieldException("El filtro es requerido.");
            }

            pageable.Validar<AgeFirmasDigitalesDAO>();

            return _repository.ConsultarListaFiltro(codigoLicenciatario, filtro.ToLower(), pageable);
        }

        public async Task<Resource<AgeFirmasDigitalesDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgeFirmasDigitalesSaveDAO ageFirmasDigitalesSaveDAO)
        {
            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    AgeFirmasDigitales ageFirmasDigitales = ValidarInsert(_httpContextAccessor, ageFirmasDigitalesSaveDAO);

                    AgeFirmasDigitalesDAO ageFirmasDigitalesDAO = await _repository.Insertar(ageFirmasDigitales);

                    Resource<AgeFirmasDigitalesDAO> ageFirmasDigitalesDAOWithResource = GetDataWithResource(_httpContextAccessor, ageFirmasDigitalesDAO);

                    transactionScope.Complete();

                    return ageFirmasDigitalesDAOWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public async Task<List<Resource<AgeFirmasDigitalesDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeFirmasDigitalesSaveDAO> ageFirmasDigitalesSaveDAOList)
        {
            if (ageFirmasDigitalesSaveDAOList == null)
            {
                throw new InvalidSintaxisException();
            }

            List<AgeFirmasDigitales> ageFirmasDigitalesList = new List<AgeFirmasDigitales>();

            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    foreach (AgeFirmasDigitalesSaveDAO ageFirmasDigitalesSaveDAO in ageFirmasDigitalesSaveDAOList)
                    {
                        AgeFirmasDigitales ageFirma = ValidarInsert(_httpContextAccessor, ageFirmasDigitalesSaveDAO);
                        ageFirmasDigitalesList.Add(ageFirma);
                    }

                    List<AgeFirmasDigitalesDAO> ageFirmasDigitalesDAOList = await _repository.InsertarVarios(ageFirmasDigitalesList);

                    List<Resource<AgeFirmasDigitalesDAO>> ageFirmasDigitalesDAOListWithResource = new List<Resource<AgeFirmasDigitalesDAO>>();

                    foreach (AgeFirmasDigitalesDAO ageFirmasDigitalesDAO in ageFirmasDigitalesDAOList)
                    {
                        ageFirmasDigitalesDAOListWithResource.Add(GetDataWithResource(_httpContextAccessor, ageFirmasDigitalesDAO));
                    }

                    transactionScope.Complete();

                    return ageFirmasDigitalesDAOListWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public Task<AgeFirmasDigitalesDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeFirmasDigitalesSaveDAO ageFirmasDigitalesSaveDAO)
        {
            AgeFirmasDigitales ageFirmasDigitales = ValidarUpdate(_httpContextAccessor, ageFirmasDigitalesSaveDAO);

            return _repository.Actualizar(ageFirmasDigitales);
        }

        public Task<List<AgeFirmasDigitalesDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeFirmasDigitalesSaveDAO> ageFirmasDigitalesSaveDAOList)
        {
            if (ageFirmasDigitalesSaveDAOList == null)
                throw new InvalidSintaxisException();

            ValidateKeys.ValidarPKDuplicadas(ageFirmasDigitalesSaveDAOList,
                                             p => new { p.Id.AgeSucursAgeLicencCodigo, p.Id.AgeSucursCodigo, p.Id.Codigo },
                                             "Id");

            List<AgeFirmasDigitales> ageFirmasDigitalesList = new List<AgeFirmasDigitales>();
            AgeFirmasDigitales ageFirmasDigitales;

            foreach (AgeFirmasDigitalesSaveDAO ageFormasPagosInstFinaSaveDAO in ageFirmasDigitalesSaveDAOList)
            {
                ageFirmasDigitales = ValidarUpdate(_httpContextAccessor, ageFormasPagosInstFinaSaveDAO);
                ageFirmasDigitalesList.Add(ageFirmasDigitales);
            }

            return _repository.ActualizarVarios(ageFirmasDigitalesList);
        }

        private Resource<AgeFirmasDigitalesDAO> GetDataWithResource(
            IHttpContextAccessor _httpContextAccessor, 
            AgeFirmasDigitalesDAO ageFirmasDigitalesDAO)
        {
            string rutaSegmentoFinal = $"{ageFirmasDigitalesDAO.Id.AgeSucursAgeLicencCodigo}" +
                $"/{ageFirmasDigitalesDAO.Id.AgeSucursCodigo}" +
                $"/{ageFirmasDigitalesDAO.Id.Codigo}";

            return Resource<AgeFirmasDigitalesDAO>.GetDataWithResource<AgeFirmasDigitalesController>(
                _httpContextAccessor, rutaSegmentoFinal, ageFirmasDigitalesDAO);
        }

        private AgeFirmasDigitales ValidarInsert(
            IHttpContextAccessor _httpContextAccessor,
            AgeFirmasDigitalesSaveDAO ageFirmasDigitalesSaveDAO)
        {

            if (ageFirmasDigitalesSaveDAO == null)
                throw new InvalidSintaxisException();

            Validator.ValidateObject(ageFirmasDigitalesSaveDAO, new ValidationContext(ageFirmasDigitalesSaveDAO), true);

            FuncionesSecuencias fg = new FuncionesSecuencias(_ageLicenciatariosAplicaSecuService);

            ageFirmasDigitalesSaveDAO.Id.Codigo = fg.ObtenerSecuencia(
                                                        _httpContextAccessor,
                                                        ageFirmasDigitalesSaveDAO.Id.AgeSucursAgeLicencCodigo,
                                                        Globales.CODIGO_SECUENCIA_FIRMA_DIGITAL,
                                                        ageFirmasDigitalesSaveDAO.UsuarioIngreso);

            ValidarKeys(ageFirmasDigitalesSaveDAO);

            AgeFirmasDigitales entityObject = FromDAOToEntity(_httpContextAccessor, ageFirmasDigitalesSaveDAO);

            Validator.ValidateObject(entityObject, new ValidationContext(entityObject), true);

            return entityObject;
        }

        private AgeFirmasDigitales ValidarUpdate(
            IHttpContextAccessor _httpContextAccessor, 
            AgeFirmasDigitalesSaveDAO ageFirmasDigitalesSaveDAO)
        {
            if (ageFirmasDigitalesSaveDAO == null)
            {
                throw new InvalidSintaxisException();
            }

            if (ageFirmasDigitalesSaveDAO.UsuarioModificacion == null || ageFirmasDigitalesSaveDAO.UsuarioModificacion <= 0)
                throw new InvalidFieldException("El usuario de modificación debe ser un número entero mayor a cero.");

            AgeFirmasDigitales? ageFirmasDigitale = ConsultarCompletePorId(new AgeFirmasDigitalesPKDAO
            {
                Codigo = ageFirmasDigitalesSaveDAO.Id.Codigo,
                AgeSucursCodigo = ageFirmasDigitalesSaveDAO.Id.AgeSucursCodigo,
                AgeSucursAgeLicencCodigo = ageFirmasDigitalesSaveDAO.Id.AgeSucursAgeLicencCodigo
            }) ?? throw new RegisterNotFoundException("Firma Digital con código: " + ageFirmasDigitalesSaveDAO.Id.Codigo + " no existe");

            if (!string.IsNullOrWhiteSpace(ageFirmasDigitalesSaveDAO.Estado))
            {
                ageFirmasDigitale.Estado = ageFirmasDigitalesSaveDAO.Estado;
                ageFirmasDigitale.FechaEstado = DateTime.Now;
            }

            if (!string.IsNullOrWhiteSpace(ageFirmasDigitalesSaveDAO.Clave))
                ageFirmasDigitale.Clave = ageFirmasDigitalesSaveDAO.Clave;

            if (!string.IsNullOrWhiteSpace(ageFirmasDigitalesSaveDAO.Nombres))
                ageFirmasDigitale.Nombres = ageFirmasDigitalesSaveDAO.Nombres;

            if (!string.IsNullOrWhiteSpace(ageFirmasDigitalesSaveDAO.Apellidos))
                ageFirmasDigitale.Apellidos = ageFirmasDigitalesSaveDAO.Apellidos;

            if (!string.IsNullOrWhiteSpace(ageFirmasDigitalesSaveDAO.NombreArchivo))
                ageFirmasDigitale.NombreArchivo = ageFirmasDigitalesSaveDAO.NombreArchivo;

            if (ageFirmasDigitalesSaveDAO.FechaDesde != default)
                ageFirmasDigitale.FechaDesde = ageFirmasDigitalesSaveDAO.FechaDesde;

            if (ageFirmasDigitalesSaveDAO.FechaHasta != null && ageFirmasDigitalesSaveDAO.FechaHasta != default)
                ageFirmasDigitale.FechaHasta = ageFirmasDigitalesSaveDAO.FechaHasta;
            else
                ageFirmasDigitale.FechaHasta = null;

            if (!string.IsNullOrWhiteSpace(ageFirmasDigitalesSaveDAO.ObservacionEstado))
                ageFirmasDigitale.ObservacionEstado = ageFirmasDigitalesSaveDAO.ObservacionEstado;

            ageFirmasDigitale.FechaModificacion = DateTime.Now;
            ageFirmasDigitale.UbicacionModificacion = Ubicacion.getIpAdress(_httpContextAccessor);
            ageFirmasDigitale.UsuarioModificacion = ageFirmasDigitalesSaveDAO.UsuarioModificacion;

            ageFirmasDigitale.FechaUltimaNotificacion = ageFirmasDigitalesSaveDAO.FechaUltimaNotificacion != null && ageFirmasDigitalesSaveDAO.FechaUltimaNotificacion != default
                ? ageFirmasDigitalesSaveDAO.FechaUltimaNotificacion
                : null;

            if (!string.IsNullOrWhiteSpace(ageFirmasDigitalesSaveDAO.Url))
                ageFirmasDigitale.Url = ageFirmasDigitalesSaveDAO.Url;

            if (!string.IsNullOrWhiteSpace(ageFirmasDigitalesSaveDAO.TipoFirmaDigital))
                ageFirmasDigitale.TipoFirmaDigital = ageFirmasDigitalesSaveDAO.TipoFirmaDigital;

            Validator.ValidateObject(ageFirmasDigitale, new ValidationContext(ageFirmasDigitale), true);

            return ageFirmasDigitale;
        }

        private AgeFirmasDigitales FromDAOToEntity(
        IHttpContextAccessor _httpContextAccessor,
        AgeFirmasDigitalesSaveDAO ageFirmasDigitalesSaveDAO)
        {
            return new AgeFirmasDigitales
            {
                Codigo = ageFirmasDigitalesSaveDAO.Id.Codigo,
                AgeSucursCodigo = ageFirmasDigitalesSaveDAO.Id.AgeSucursCodigo,
                AgeSucursAgeLicencCodigo = ageFirmasDigitalesSaveDAO.Id.AgeSucursAgeLicencCodigo,
                Clave = ageFirmasDigitalesSaveDAO.Clave,
                Nombres = ageFirmasDigitalesSaveDAO.Nombres,
                Apellidos = ageFirmasDigitalesSaveDAO.Apellidos,
                NombreArchivo = ageFirmasDigitalesSaveDAO.NombreArchivo,
                Estado = ageFirmasDigitalesSaveDAO.Estado,
                FechaEstado = DateTime.Now,
                FechaDesde = ageFirmasDigitalesSaveDAO.FechaDesde != default ? ageFirmasDigitalesSaveDAO.FechaDesde : DateTime.Now,
                FechaHasta = ageFirmasDigitalesSaveDAO.FechaHasta != null && ageFirmasDigitalesSaveDAO.FechaHasta != default ? ageFirmasDigitalesSaveDAO.FechaHasta : null,
                ObservacionEstado = ageFirmasDigitalesSaveDAO.ObservacionEstado,
                FechaIngreso = DateTime.Now,
                UbicacionIngreso = Ubicacion.getIpAdress(_httpContextAccessor),
                UsuarioIngreso = ageFirmasDigitalesSaveDAO.UsuarioIngreso,
                FechaUltimaNotificacion = ageFirmasDigitalesSaveDAO.FechaUltimaNotificacion != null && ageFirmasDigitalesSaveDAO.FechaUltimaNotificacion != default ? ageFirmasDigitalesSaveDAO.FechaUltimaNotificacion : null,
                Url = ageFirmasDigitalesSaveDAO.Url,
                TipoFirmaDigital = ageFirmasDigitalesSaveDAO.TipoFirmaDigital
            };
        }

        private void ValidarKeys(AgeFirmasDigitalesSaveDAO ageFirmasDigitalesSaveDAO)
        {
            ValidarPK(ageFirmasDigitalesSaveDAO.Id);
            ValidarFKSucursal(ageFirmasDigitalesSaveDAO.Id.AgeSucursCodigo, ageFirmasDigitalesSaveDAO.Id.AgeSucursAgeLicencCodigo);
            ValidarFKLicenciatario(ageFirmasDigitalesSaveDAO.Id.AgeSucursAgeLicencCodigo);
        }

        private void ValidarPK(AgeFirmasDigitalesPKDAO ageFirmasDigitalesPKDAO)
        {
            ValidateKeys.ValidarNoExistenciaKey(
                ageFirmasDigitalesPKDAO,
                $"Firma Digital con código {ageFirmasDigitalesPKDAO.Codigo} ya existe.",
                _repository.ConsultarCompletePorId);
        }

        private void ValidarFKSucursal(int codigoSucursal, int codigoLicenciatario)
        {
            AgeSucursalesPKDAO ageSucursalesPKDAO = new()
            {
                Codigo = codigoSucursal,
                AgeLicencCodigo = codigoLicenciatario
            };

            string mensaje = $"Sucursal con código: {ageSucursalesPKDAO.Codigo}" +
                            $" y código licenciatario: {ageSucursalesPKDAO.AgeLicencCodigo} no existe.";
            ValidateKeys.ValidarExistenciaKey(ageSucursalesPKDAO, mensaje, _repositorySucursales.ConsultarCompletePorId);
        }

        private void ValidarFKLicenciatario(int ageSucursAgeLicencCodigo)
        {
            string mensaje = $"Licenciatario con código: {ageSucursAgeLicencCodigo} no existe.";
            ValidateKeys.ValidarExistenciaKey(ageSucursAgeLicencCodigo, mensaje, _repositoryLicenciatarios.ConsultarCompletePorId);
        }

    }
}
