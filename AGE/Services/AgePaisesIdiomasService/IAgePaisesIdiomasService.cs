﻿using AGE.Entities;
using AGE.Entities.DAO.AgePaisesIdioma;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AGE.Services.AgePaisesIdiomaService
{
    public interface IAgePaisesIdiomasService
    {
        Task<Page<AgePaisesIdiomasDAO>> ConsultarTodos(
            int codigoIdioma,
            int codigoPais,
            string descripcion,
            Pageable pageable);

        Task<Page<AgePaisesIdiomasDAO>> ConsultarListaFiltro(
            string filtro, 
            Pageable pageable);

        Task<AgePaisesIdiomasDAO> ConsultarPorId(
            AgePaisesIdiomasPKDAO agePaisesIdiomaPKDAO);

        Task<Resource<AgePaisesIdiomasDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor, 
            AgePaisesIdiomasSaveDAO agePaisesIdiomaSaveDAO);

        Task<List<Resource<AgePaisesIdiomasDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor, 
            List<AgePaisesIdiomasSaveDAO> agePaisesIdiomaSaveDAOList);

        Task<AgePaisesIdiomasDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgePaisesIdiomasSaveDAO agePaisesIdiomaSaveDAO);

        Task<List<AgePaisesIdiomasDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor, 
            List<AgePaisesIdiomasSaveDAO> agePaisesIdiomaSaveDAOList);
    }
}
