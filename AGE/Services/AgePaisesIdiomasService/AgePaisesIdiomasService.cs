﻿using AGE.Controllers;
using AGE.Entities;
using AGE.Entities.DAO.AgePaisesIdioma;
using AGE.Middleware.Exceptions.BadRequest;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Repositories.AgeIdiomaRepository;
using AGE.Repositories.AgePaisesIdiomaRepository;
using AGE.Repositories.AgePaisesRepository;
using AGE.Utils;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using System.ComponentModel.DataAnnotations;
using System.Transactions;

namespace AGE.Services.AgePaisesIdiomaService
{
    public class AgePaisesIdiomasService : IAgePaisesIdiomasService
    {
        private readonly IAgePaisesIdiomasRepository _repository;
        private readonly IAgePaisesRepository _repositoryPais;
        private readonly IAgeIdiomasRepository _repositoryIdioma;
        public AgePaisesIdiomasService(
            IAgePaisesIdiomasRepository Repository,
            IAgePaisesRepository repositoryPais,
            IAgeIdiomasRepository repositoryIdioma)
        {
            _repository = Repository;
            _repositoryPais = repositoryPais;
            _repositoryIdioma = repositoryIdioma;
        }


        public Task<Page<AgePaisesIdiomasDAO>> ConsultarTodos(
            int codigoIdioma,
            int codigoPais,
            string descripcion,
            Pageable pageable)
        {

            pageable.Validar<AgePaisesIdiomasDAO>();

            return _repository.ConsultarTodos(codigoIdioma, codigoPais, descripcion, pageable);
        }


        public Task<Page<AgePaisesIdiomasDAO>> ConsultarListaFiltro(
            string filtro,
            Pageable pageable)
        {

            if (string.IsNullOrWhiteSpace(filtro))
            {
                throw new InvalidFieldException("El filtro es requerido.");
            }

            pageable.Validar<AgePaisesIdiomasDAO>();

            return _repository.ConsultarListaFiltro(filtro.ToLower(), pageable);
        }

        public Task<AgePaisesIdiomasDAO> ConsultarPorId(
            AgePaisesIdiomasPKDAO agePaisesIdiomaPKDAO)
        {
            Validator.ValidateObject(agePaisesIdiomaPKDAO, new ValidationContext(agePaisesIdiomaPKDAO), true);

            AgePaisesIdiomasDAO? agePaisesIdiomaDAO = _repository.ConsultarPorId(agePaisesIdiomaPKDAO).Result;

            if (agePaisesIdiomaDAO == null)
            {
                throw new RegisterNotFoundException("Idioma con código " + agePaisesIdiomaPKDAO.ageIdiomaCodigo + " no existe.");
            }

            return Task.FromResult(agePaisesIdiomaDAO);
        }

        private AgePaisesIdiomas ConsultarCompletePorId(
            AgePaisesIdiomasPKDAO agePaisesIdiomaPKDAO)
        {
            Validator.ValidateObject(agePaisesIdiomaPKDAO, new ValidationContext(agePaisesIdiomaPKDAO), true);

            AgePaisesIdiomas? agePaisesIdioma = _repository.ConsultarCompletePorId(agePaisesIdiomaPKDAO).Result;

            return agePaisesIdioma;
        }

        public async Task<Resource<AgePaisesIdiomasDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgePaisesIdiomasSaveDAO agePaisesIdiomaSaveDAO)
        {
            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    AgePaisesIdiomas agePaisesIdioma = ValidarInsert(_httpContextAccessor, agePaisesIdiomaSaveDAO);

                    AgePaisesIdiomasDAO agePaisesIdiomaDAO = await _repository.Insertar(agePaisesIdioma);

                    Resource<AgePaisesIdiomasDAO> agePaisesIdiomaDAOWithResource = GetDataWithResource(_httpContextAccessor, agePaisesIdiomaDAO);

                    transactionScope.Complete();

                    return agePaisesIdiomaDAOWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }
        public async Task<List<Resource<AgePaisesIdiomasDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgePaisesIdiomasSaveDAO> agePaisesIdiomaSaveDAOList)
        {
            if (agePaisesIdiomaSaveDAOList == null)
            {
                throw new InvalidSintaxisException();
            }

            List<AgePaisesIdiomas> agePaisesIdiomaList = new List<AgePaisesIdiomas>();

            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    foreach (AgePaisesIdiomasSaveDAO agePaisesIdiomaSaveDAO in agePaisesIdiomaSaveDAOList)
                    {
                        AgePaisesIdiomas ageMensaje = ValidarInsert(_httpContextAccessor, agePaisesIdiomaSaveDAO);
                        agePaisesIdiomaList.Add(ageMensaje);
                    }

                    List<AgePaisesIdiomasDAO> agePaisesIdiomaDAOList = await _repository.InsertarVarios(agePaisesIdiomaList);

                    List<Resource<AgePaisesIdiomasDAO>> agePaisesIdiomaDAOListWithResource = new List<Resource<AgePaisesIdiomasDAO>>();

                    foreach (AgePaisesIdiomasDAO agePaisesIdiomaDAO in agePaisesIdiomaDAOList)
                    {
                        agePaisesIdiomaDAOListWithResource.Add(GetDataWithResource(_httpContextAccessor, agePaisesIdiomaDAO));
                    }

                    transactionScope.Complete();

                    return agePaisesIdiomaDAOListWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public Task<AgePaisesIdiomasDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgePaisesIdiomasSaveDAO agePaisesIdiomaSaveDAO)
        {
            AgePaisesIdiomas agePaisesIdioma = ValidarUpdate(_httpContextAccessor, agePaisesIdiomaSaveDAO);

            return _repository.Actualizar(agePaisesIdioma);
        }

        public Task<List<AgePaisesIdiomasDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgePaisesIdiomasSaveDAO> agePaisesIdiomaSaveDAOList)
        {
            if (agePaisesIdiomaSaveDAOList == null)
                throw new InvalidSintaxisException();

            ValidateKeys.ValidarPKDuplicadas(agePaisesIdiomaSaveDAOList,
                                             p => new { p.Id.ageIdiomaCodigo , p.Id.agePaisCodigo },
                                             "Id");

            List<AgePaisesIdiomas> agePaisesIdiomaList = new List<AgePaisesIdiomas>();
            AgePaisesIdiomas agePaisesIdioma;

            foreach (AgePaisesIdiomasSaveDAO agePaisesIdiomaSaveDAO in agePaisesIdiomaSaveDAOList)
            {
                agePaisesIdioma = ValidarUpdate(_httpContextAccessor, agePaisesIdiomaSaveDAO);
                agePaisesIdiomaList.Add(agePaisesIdioma);
            }

            return _repository.ActualizarVarios(agePaisesIdiomaList);
        }

        private static Resource<AgePaisesIdiomasDAO> GetDataWithResource(
            IHttpContextAccessor _httpContextAccessor,
            AgePaisesIdiomasDAO agePaisesIdiomaDAO)
        {
            string idtipoSegmentoFinal = $"{agePaisesIdiomaDAO.Id.ageIdiomaCodigo}/{agePaisesIdiomaDAO.Id.agePaisCodigo}";

            return Resource<AgePaisesIdiomasDAO>.GetDataWithResource<AgePaisesIdiomasController>(
                _httpContextAccessor, idtipoSegmentoFinal, agePaisesIdiomaDAO);
        }



        private AgePaisesIdiomas ValidarInsert(
            IHttpContextAccessor _httpContextAccessor,
            AgePaisesIdiomasSaveDAO agePaisesIdiomaSaveDAO)
        {
            if (agePaisesIdiomaSaveDAO == null)
                throw new InvalidSintaxisException();

            Validator.ValidateObject(agePaisesIdiomaSaveDAO, new ValidationContext(agePaisesIdiomaSaveDAO), true);

            ValidarKeys(agePaisesIdiomaSaveDAO);

            AgePaisesIdiomas entityObject = FromDAOToEntity(_httpContextAccessor, agePaisesIdiomaSaveDAO);

            Validator.ValidateObject(entityObject, new ValidationContext(entityObject), true);

            return entityObject;
        }



        private AgePaisesIdiomas ValidarUpdate(
            IHttpContextAccessor _httpContextAccessor,
            AgePaisesIdiomasSaveDAO agePaisesIdiomaSaveDAO)
        {
            if (agePaisesIdiomaSaveDAO == null)
            {
                throw new InvalidSintaxisException();
            }

            if (agePaisesIdiomaSaveDAO.UsuarioModificacion == null || agePaisesIdiomaSaveDAO.UsuarioModificacion <= 0)
            {
                throw new InvalidFieldException("El usuario de modificación debe ser un número entero mayor a cero.");
            }

            AgePaisesIdiomas? agePaisesIdioma = ConsultarCompletePorId(new AgePaisesIdiomasPKDAO
            {
                ageIdiomaCodigo = agePaisesIdiomaSaveDAO.Id.ageIdiomaCodigo,
                agePaisCodigo = agePaisesIdiomaSaveDAO.Id.agePaisCodigo
            });

            if (agePaisesIdioma == null)
            {
                throw new RegisterNotFoundException("País con código " + agePaisesIdiomaSaveDAO.Id.agePaisCodigo + " e Idioma con código " 
                    + agePaisesIdiomaSaveDAO.Id.ageIdiomaCodigo + " no existe.");
            }

            if (!string.IsNullOrWhiteSpace(agePaisesIdiomaSaveDAO.Estado))
            {
                agePaisesIdioma.Estado = agePaisesIdiomaSaveDAO.Estado;
                agePaisesIdioma.FechaEstado = DateTime.Now;
            }

            if (agePaisesIdiomaSaveDAO.OrdenPrincipalSecundario != null && agePaisesIdiomaSaveDAO.OrdenPrincipalSecundario > 0)
                agePaisesIdioma.OrdenPrincipalSecundario = agePaisesIdiomaSaveDAO.OrdenPrincipalSecundario;

            if (!string.IsNullOrWhiteSpace(agePaisesIdiomaSaveDAO.TipoPrincipalSecundario))
                agePaisesIdioma.TipoPrincipalSecundario = agePaisesIdiomaSaveDAO.TipoPrincipalSecundario;

            if (!string.IsNullOrWhiteSpace(agePaisesIdiomaSaveDAO.Descripcion))
                agePaisesIdioma.Descripcion = agePaisesIdiomaSaveDAO.Descripcion;

            if (!string.IsNullOrWhiteSpace(agePaisesIdiomaSaveDAO.ObservacionEstado))
                agePaisesIdioma.ObservacionEstado = agePaisesIdiomaSaveDAO.ObservacionEstado;

            agePaisesIdioma.FechaModificacion = DateTime.Now;
            agePaisesIdioma.UbicacionModificacion = Ubicacion.getIpAdress(_httpContextAccessor);
            agePaisesIdioma.UsuarioModificacion = agePaisesIdiomaSaveDAO.UsuarioModificacion;

            Validator.ValidateObject(agePaisesIdioma, new ValidationContext(agePaisesIdioma), true);

            return agePaisesIdioma;
        }

        private AgePaisesIdiomas FromDAOToEntity(
        IHttpContextAccessor _httpContextAccessor,
        AgePaisesIdiomasSaveDAO agePaisesIdiomaSaveDAO)
        {
            return new AgePaisesIdiomas
            {
                AgeIdiomaCodigo = agePaisesIdiomaSaveDAO.Id.ageIdiomaCodigo,
                AgePaisCodigo = agePaisesIdiomaSaveDAO.Id.agePaisCodigo,
                Descripcion = agePaisesIdiomaSaveDAO.Descripcion,
                TipoPrincipalSecundario = agePaisesIdiomaSaveDAO.TipoPrincipalSecundario,
                OrdenPrincipalSecundario = agePaisesIdiomaSaveDAO.OrdenPrincipalSecundario,
                Estado = agePaisesIdiomaSaveDAO.Estado,
                FechaEstado = DateTime.Now,
                FechaIngreso = DateTime.Now,
                ObservacionEstado = agePaisesIdiomaSaveDAO.ObservacionEstado,
                UbicacionIngreso = Ubicacion.getIpAdress(_httpContextAccessor),
                UsuarioIngreso = agePaisesIdiomaSaveDAO.UsuarioIngreso
            };
        }

        private void ValidarKeys(AgePaisesIdiomasSaveDAO agePaisesIdiomaSaveDAO)
        {
            ValidarPK(agePaisesIdiomaSaveDAO.Id);
            ValidarFKPais(agePaisesIdiomaSaveDAO.Id.agePaisCodigo);
            ValidarFKIdioma(agePaisesIdiomaSaveDAO.Id.ageIdiomaCodigo);

        }

        private void ValidarPK(AgePaisesIdiomasPKDAO agePaisesIdiomaPKDAO)
        {
            ValidateKeys.ValidarNoExistenciaKey(
            agePaisesIdiomaPKDAO,
                 $"Idioma con código {agePaisesIdiomaPKDAO.ageIdiomaCodigo} para País con código" +
                 $" {agePaisesIdiomaPKDAO.agePaisCodigo} ya existe.",
                _repository.ConsultarCompletePorId);
        }

        private void ValidarFKPais(int codigoPais)
        {

            ValidateKeys.ValidarExistenciaKey(
                codigoPais,
                $"País con código {codigoPais} no existe.",
                _repositoryPais.ConsultarCompletePorId);
        }

        private void ValidarFKIdioma(int codigoIdioma)
        {

            ValidateKeys.ValidarExistenciaKey(
                codigoIdioma,
                $"Idioma con código {codigoIdioma} no existe.",
                _repositoryIdioma.ConsultarCompletePorId);
        }
    }

}

