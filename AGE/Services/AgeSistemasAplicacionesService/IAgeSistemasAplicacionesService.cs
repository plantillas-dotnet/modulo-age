﻿using AGE.Entities.DAO.AgeSistemasAplicaciones;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;

namespace AGE.Services.AgeSistemasAplicacionesService
{
    public interface IAgeSistemasAplicacionesService
    {
        Task<Page<AgeSistemasAplicacionesDAO>> ConsultarTodos(
            int ageAplicaCodigo,
            int ageSistemCodigo,
            DateTime fechaDesde,
            DateTime fechaHasta,
            Pageable pageable);

        Task<Page<AgeSistemasAplicacionesDAO>> ConsultarListaFiltro(
            string filtro,
            Pageable pageable);

        Task<AgeSistemasAplicacionesDAO> ConsultarPorId(AgeSistemasAplicacionesPKDAO ageSistemasAplicacionesPKDAO);

        Task<Resource<AgeSistemasAplicacionesDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgeSistemasAplicacionesSaveDAO AgeSistemasAplicacionesSaveDAO);

        Task<List<Resource<AgeSistemasAplicacionesDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeSistemasAplicacionesSaveDAO> AgeSistemasAplicacionesSaveDAOList);

        Task<AgeSistemasAplicacionesDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeSistemasAplicacionesSaveDAO AgeSistemasAplicacionesSaveDAO);

        Task<List<AgeSistemasAplicacionesDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeSistemasAplicacionesSaveDAO> AgeSistemasAplicacionesSaveDAOList);
    }
}
