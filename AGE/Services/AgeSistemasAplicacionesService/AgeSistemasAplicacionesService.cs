﻿using AGE.Controllers;
using AGE.Entities.DAO.AgeSistemasAplicaciones;
using AGE.Entities;
using AGE.Middleware.Exceptions.BadRequest;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Repositories.AgeSistemasAplicacionesRepository;
using AGE.Repositories.AgeAplicacionesRepository;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using AGE.Utils;
using System.ComponentModel.DataAnnotations;
using System.Transactions;
using AGE.Repositories.AgeSistemasRepository;

namespace AGE.Services.AgeSistemasAplicacionesService
{
    public class AgeSistemasAplicacionesService : IAgeSistemasAplicacionesService
    {
        private readonly IAgeSistemasAplicacionesRepository _repository;
        private readonly IAgeAplicacionesRepository _repositoryAplica;
        private readonly IAgeSistemasRepository _repositorySistemas;

        public AgeSistemasAplicacionesService(
            IAgeSistemasAplicacionesRepository Repository,
            IAgeAplicacionesRepository RepositoryAplica,
            IAgeSistemasRepository RepositorySistemas)
        {
            _repository = Repository;
            _repositoryAplica = RepositoryAplica;
            _repositorySistemas = RepositorySistemas;
        }

        public Task<Page<AgeSistemasAplicacionesDAO>> ConsultarTodos(
            int ageAplicaCodigo,
            int ageSistemCodigo,
            DateTime fechaDesde,
            DateTime fechaHasta,
            Pageable pageable)
        {

            pageable.Validar<AgeSistemasAplicacionesDAO>();

            return _repository.ConsultarTodos(ageAplicaCodigo, ageSistemCodigo, fechaDesde, fechaHasta, pageable);
        }

        public Task<AgeSistemasAplicacionesDAO> ConsultarPorId(
            AgeSistemasAplicacionesPKDAO ageSistemasAplicacionesPKDAO)
        {
            Validator.ValidateObject(ageSistemasAplicacionesPKDAO, new ValidationContext(ageSistemasAplicacionesPKDAO), true);

            AgeSistemasAplicacionesDAO ageSistemasAplicacionesDAO = _repository.ConsultarPorId(ageSistemasAplicacionesPKDAO).Result;

            return ageSistemasAplicacionesDAO == null
                ? throw new RegisterNotFoundException("Aplicación sistema con código aplicación: "
                    + ageSistemasAplicacionesPKDAO.AgeAplicaCodigo + " y código sistema: "
                    + ageSistemasAplicacionesPKDAO.AgeSistemCodigo + " no existe.")
                : Task.FromResult(ageSistemasAplicacionesDAO);
        }

        private AgeSistemasAplicaciones ConsultarCompletePorId(
            AgeSistemasAplicacionesPKDAO ageSistemasAplicacionesPKDAO)
        {
            Validator.ValidateObject(ageSistemasAplicacionesPKDAO, new ValidationContext(ageSistemasAplicacionesPKDAO), true);

            AgeSistemasAplicaciones ageSistemasAplicaciones = _repository.ConsultarCompletePorId(ageSistemasAplicacionesPKDAO).Result;

            return ageSistemasAplicaciones;
        }

        public Task<Page<AgeSistemasAplicacionesDAO>> ConsultarListaFiltro(
            string filtro,
            Pageable pageable)
        {
            if (string.IsNullOrWhiteSpace(filtro))
                throw new InvalidFieldException("El filtro es requerido.");

            pageable.Validar<AgeSistemasAplicacionesDAO>();

            return _repository.ConsultarListaFiltro(filtro.ToLower(), pageable);
        }


        public async Task<Resource<AgeSistemasAplicacionesDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgeSistemasAplicacionesSaveDAO ageSistemasAplicacionesSaveDAO)
        {
            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    AgeSistemasAplicaciones ageSistemasAplicaciones = ValidarInsert(_httpContextAccessor, ageSistemasAplicacionesSaveDAO);

                    AgeSistemasAplicacionesDAO ageSistemasAplicacionesDAO = await _repository.Insertar(ageSistemasAplicaciones);

                    Resource<AgeSistemasAplicacionesDAO> ageSistemasAplicacionesDAOWithResource = GetDataWithResource(_httpContextAccessor, ageSistemasAplicacionesDAO);

                    transactionScope.Complete();

                    return ageSistemasAplicacionesDAOWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public async Task<List<Resource<AgeSistemasAplicacionesDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeSistemasAplicacionesSaveDAO> ageSistemasAplicacionesSaveDAOList)
        {
            if (ageSistemasAplicacionesSaveDAOList == null)
                throw new InvalidSintaxisException();

            ValidateKeys.ValidarPKDuplicadas(ageSistemasAplicacionesSaveDAOList,
                                             p => new { p.Id.AgeAplicaCodigo, p.Id.AgeSistemCodigo },
                                             "Id");

            List<AgeSistemasAplicaciones> ageSistemasAplicacionesList = new List<AgeSistemasAplicaciones>();

            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    foreach (AgeSistemasAplicacionesSaveDAO ageSistemasAplicacionesSaveDAO in ageSistemasAplicacionesSaveDAOList)
                    {
                        AgeSistemasAplicaciones AgeSistemasAplicacione = ValidarInsert(_httpContextAccessor, ageSistemasAplicacionesSaveDAO);
                        ageSistemasAplicacionesList.Add(AgeSistemasAplicacione);
                    }

                    List<AgeSistemasAplicacionesDAO> ageSistemasAplicacionesDAOList = await _repository.InsertarVarios(ageSistemasAplicacionesList);

                    List<Resource<AgeSistemasAplicacionesDAO>> ageSistemasAplicacionesDAOListWithResource = new List<Resource<AgeSistemasAplicacionesDAO>>();

                    foreach (AgeSistemasAplicacionesDAO ageSistemasAplicacionesDAO in ageSistemasAplicacionesDAOList)
                    {
                        ageSistemasAplicacionesDAOListWithResource.Add(GetDataWithResource(_httpContextAccessor, ageSistemasAplicacionesDAO));
                    }

                    transactionScope.Complete();

                    return ageSistemasAplicacionesDAOListWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public Task<AgeSistemasAplicacionesDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeSistemasAplicacionesSaveDAO ageSistemasAplicacionesSaveDAO)
        {
            AgeSistemasAplicaciones ageSistemasAplicaciones = ValidarUpdate(_httpContextAccessor, ageSistemasAplicacionesSaveDAO);

            return _repository.Actualizar(ageSistemasAplicaciones);
        }

        public Task<List<AgeSistemasAplicacionesDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeSistemasAplicacionesSaveDAO> ageSistemasAplicacionesSaveDAOList)
        {
            if (ageSistemasAplicacionesSaveDAOList == null)
                throw new InvalidSintaxisException();

            ValidateKeys.ValidarPKDuplicadas(ageSistemasAplicacionesSaveDAOList,
                                             p => new { p.Id.AgeAplicaCodigo, p.Id.AgeSistemCodigo },
                                             "Id");

            List<AgeSistemasAplicaciones> ageSistemasAplicacionesList = new List<AgeSistemasAplicaciones>();
            AgeSistemasAplicaciones ageSistemasAplicaciones;

            foreach (AgeSistemasAplicacionesSaveDAO ageSistemasAplicacionesSaveDAO in ageSistemasAplicacionesSaveDAOList)
            {
                ageSistemasAplicaciones = ValidarUpdate(_httpContextAccessor, ageSistemasAplicacionesSaveDAO);
                ageSistemasAplicacionesList.Add(ageSistemasAplicaciones);
            }

            return _repository.ActualizarVarios(ageSistemasAplicacionesList);
        }


        private static Resource<AgeSistemasAplicacionesDAO> GetDataWithResource(
            IHttpContextAccessor _httpContextAccessor,
            AgeSistemasAplicacionesDAO ageSistemasAplicacionesDAO)
        {
            string rutaSegmentoFinal = $"{ageSistemasAplicacionesDAO.Id.AgeSistemCodigo}/{ageSistemasAplicacionesDAO.Id.AgeAplicaCodigo}";

            return Resource<AgeSistemasAplicacionesDAO>.GetDataWithResource<AgeSistemasAplicacionesController>(
                _httpContextAccessor, rutaSegmentoFinal, ageSistemasAplicacionesDAO);
        }

        private AgeSistemasAplicaciones ValidarInsert(
            IHttpContextAccessor _httpContextAccessor,
            AgeSistemasAplicacionesSaveDAO ageSistemasAplicacionesSaveDAO)
        {
            if (ageSistemasAplicacionesSaveDAO == null)
                throw new InvalidSintaxisException();

            Validator.ValidateObject(ageSistemasAplicacionesSaveDAO, new ValidationContext(ageSistemasAplicacionesSaveDAO), true);

            ValidarKeys(ageSistemasAplicacionesSaveDAO.Id);

            if (ageSistemasAplicacionesSaveDAO.FechaDesde == default)
                throw new InvalidFieldException("La fecha desde no puede ser nula.");

            AgeSistemasAplicaciones entityObject = FromDAOToEntity(_httpContextAccessor,ageSistemasAplicacionesSaveDAO);

            Validator.ValidateObject(entityObject, new ValidationContext(entityObject), true);

            return entityObject;
        }
    
        private AgeSistemasAplicaciones ValidarUpdate(
            IHttpContextAccessor _httpContextAccessor,
            AgeSistemasAplicacionesSaveDAO ageSistemasAplicacionesSaveDAO)
        {
            if (ageSistemasAplicacionesSaveDAO == null)
                throw new InvalidSintaxisException();

            if (ageSistemasAplicacionesSaveDAO.UsuarioModificacion == null || ageSistemasAplicacionesSaveDAO.UsuarioModificacion <= 0)
                throw new InvalidFieldException("El usuario de modificación debe ser un número entero mayor a cero.");

            AgeSistemasAplicaciones ageSistemasAplicaciones = ConsultarCompletePorId(new AgeSistemasAplicacionesPKDAO
            {
                AgeAplicaCodigo = ageSistemasAplicacionesSaveDAO.Id.AgeAplicaCodigo,
                AgeSistemCodigo = ageSistemasAplicacionesSaveDAO.Id.AgeSistemCodigo
            }) ?? throw new RegisterNotFoundException($"Aplicación sistema con código aplicación:" +
                                                        $" {ageSistemasAplicacionesSaveDAO.Id.AgeAplicaCodigo} y código sistema:" +
                                                        $" {ageSistemasAplicacionesSaveDAO.Id.AgeSistemCodigo} no existe");

            ageSistemasAplicaciones.Estado = !string.IsNullOrWhiteSpace(ageSistemasAplicacionesSaveDAO.Estado) ? ageSistemasAplicacionesSaveDAO.Estado : ageSistemasAplicaciones.Estado;
            ageSistemasAplicaciones.FechaEstado = !string.IsNullOrWhiteSpace(ageSistemasAplicacionesSaveDAO.Estado) ? DateTime.Now : ageSistemasAplicaciones.FechaEstado;
            ageSistemasAplicaciones.ObservacionEstado = !string.IsNullOrWhiteSpace(ageSistemasAplicacionesSaveDAO.ObservacionEstado) ? ageSistemasAplicacionesSaveDAO.ObservacionEstado : ageSistemasAplicaciones.ObservacionEstado;

            ageSistemasAplicaciones.FechaDesde = ageSistemasAplicacionesSaveDAO.FechaDesde != default ? ageSistemasAplicacionesSaveDAO.FechaDesde : ageSistemasAplicaciones.FechaDesde;
            ageSistemasAplicaciones.FechaHasta = ageSistemasAplicacionesSaveDAO.FechaHasta != null && ageSistemasAplicacionesSaveDAO.FechaHasta != default ? ageSistemasAplicacionesSaveDAO.FechaHasta : ageSistemasAplicaciones.FechaHasta;
            ageSistemasAplicaciones.FechaModificacion = DateTime.Now;
            ageSistemasAplicaciones.UbicacionModificacion = Ubicacion.getIpAdress(_httpContextAccessor);
            ageSistemasAplicaciones.UsuarioModificacion = ageSistemasAplicacionesSaveDAO.UsuarioModificacion;
            Validator.ValidateObject(ageSistemasAplicaciones, new ValidationContext(ageSistemasAplicaciones), true);

            return ageSistemasAplicaciones;
        }

        private AgeSistemasAplicaciones FromDAOToEntity(
            IHttpContextAccessor _httpContextAccessor,
            AgeSistemasAplicacionesSaveDAO ageSistemasAplicacionesSaveDAO)
        {

            return new AgeSistemasAplicaciones
            {
                AgeAplicaCodigo = ageSistemasAplicacionesSaveDAO.Id.AgeAplicaCodigo,
                AgeSistemCodigo = ageSistemasAplicacionesSaveDAO.Id.AgeSistemCodigo,
                FechaDesde = ageSistemasAplicacionesSaveDAO.FechaDesde,
                FechaHasta = ageSistemasAplicacionesSaveDAO.FechaHasta == default
                                        ? null : ageSistemasAplicacionesSaveDAO.FechaHasta,
                Estado = ageSistemasAplicacionesSaveDAO.Estado,
                FechaEstado = DateTime.Now,
                FechaIngreso = DateTime.Now,
                ObservacionEstado = ageSistemasAplicacionesSaveDAO.ObservacionEstado,
                UbicacionIngreso = Ubicacion.getIpAdress(_httpContextAccessor),
                UsuarioIngreso = ageSistemasAplicacionesSaveDAO.UsuarioIngreso
            };
        }

        private void ValidarKeys(AgeSistemasAplicacionesPKDAO ageSistemasAplicacionesPKDAO)
        {
            ValidarPK(ageSistemasAplicacionesPKDAO);

            ValidarFKAplicaciones(ageSistemasAplicacionesPKDAO.AgeAplicaCodigo);

            ValidarFKSistema(ageSistemasAplicacionesPKDAO.AgeSistemCodigo);
        }

        private void ValidarPK(AgeSistemasAplicacionesPKDAO ageSistemasAplicacionesPKDAO)
        {

            ValidateKeys.ValidarNoExistenciaKey(
               ageSistemasAplicacionesPKDAO,
               $"Sistema aplicación con código de aplicación: {ageSistemasAplicacionesPKDAO.AgeAplicaCodigo}" +
               $" y código sistema: {ageSistemasAplicacionesPKDAO.AgeSistemCodigo} ya existe.",
               _repository.ConsultarCompletePorId);
        }

        private void ValidarFKAplicaciones(int codigoAplicacion)
        {

            ValidateKeys.ValidarExistenciaKey(
               codigoAplicacion,
               $"Aplicación con código: {codigoAplicacion} no existe.",
               _repositoryAplica.ConsultarCompletePorId);
        }

        private void ValidarFKSistema(int codigoSistema)
        {

            ValidateKeys.ValidarExistenciaKey(
               codigoSistema,
               $"Sistema con código: {codigoSistema} no existe.",
               _repositorySistemas.ConsultarCompletePorId);
        }
    }
}
