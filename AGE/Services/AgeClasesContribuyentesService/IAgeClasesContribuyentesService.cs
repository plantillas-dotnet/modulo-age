﻿using AGE.Entities.DAO.AgeClasesContribuyentes;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;

namespace AGE.Services.AgeClasesContribuyentesService
{
    public interface IAgeClasesContribuyentesService
    {
        Task<Page<AgeClasesContribuyentesDAO>> ConsultarTodos(
            string descripcion,
            Pageable pageable);

        Task<Page<AgeClasesContribuyentesDAO>> ConsultarListaFiltro(
            string filtro,
            Pageable pageable);

        Task<AgeClasesContribuyentesDAO> ConsultarPorId(long Codigo);

        Task<Resource<AgeClasesContribuyentesDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgeClasesContribuyentesSaveDAO AgeClasesContribuyentesSaveDAO);

        Task<List<Resource<AgeClasesContribuyentesDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeClasesContribuyentesSaveDAO> AgeClasesContribuyentesSaveDAOList);

        Task<AgeClasesContribuyentesDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeClasesContribuyentesSaveDAO AgeClasesContribuyentesSaveDAO);

        Task<List<AgeClasesContribuyentesDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeClasesContribuyentesSaveDAO> AgeClasesContribuyentesSaveDAOList);
    }
}
