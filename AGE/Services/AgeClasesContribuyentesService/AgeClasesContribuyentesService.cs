﻿using AGE.Controllers;
using AGE.Entities.DAO.AgeClasesContribuyentes;
using AGE.Entities;
using AGE.Middleware.Exceptions.BadRequest;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Repositories.AgeClasesContribuyentesRepository;
using AGE.Services.AgeSecuenciasPrimariasService;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using AGE.Utils;
using System.ComponentModel.DataAnnotations;
using System.Transactions;

namespace AGE.Services.AgeClasesContribuyentesService
{
    public class AgeClasesContribuyentesService : IAgeClasesContribuyentesService
    {
        private readonly IAgeClasesContribuyentesRepository _repository;
        private readonly IAgeSecuenciasPrimariasService _ageSecuenciasPrimariasService;

        public AgeClasesContribuyentesService(
            IAgeClasesContribuyentesRepository Repository,
            IAgeSecuenciasPrimariasService ageSecuenciasPrimariasService)
        {
            _repository = Repository;
            _ageSecuenciasPrimariasService = ageSecuenciasPrimariasService;
        }

        public Task<Page<AgeClasesContribuyentesDAO>> ConsultarTodos(
            string descripcion,
            Pageable pageable)
        {
            pageable.Validar<AgeClasesContribuyentesDAO>();

            return _repository.ConsultarTodos(descripcion, pageable);
        }

        public Task<Page<AgeClasesContribuyentesDAO>> ConsultarListaFiltro(string filtro, Pageable pageable)
        {
            if (string.IsNullOrWhiteSpace(filtro))
            {
                throw new InvalidFieldException("El filtro es requerido.");
            }

            pageable.Validar<AgeClasesContribuyentesDAO>();

            return _repository.ConsultarListaFiltro(filtro, pageable);
        }

        public Task<AgeClasesContribuyentesDAO> ConsultarPorId(
            long codigo)
        {
            if (codigo <= 0)
            {
                throw new InvalidIdException();
            }

            AgeClasesContribuyentesDAO? ageClasesContribuyentesDAO = _repository.ConsultarPorId(codigo).Result;

            return ageClasesContribuyentesDAO == null
                ? throw new RegisterNotFoundException("Clase Contribuyente con código " + codigo + " no existe")
                : Task.FromResult(ageClasesContribuyentesDAO);
        }

        private AgeClasesContribuyentes ConsultarCompletePorId(
            long codigo)
        {

            if (codigo <= 0)
            {
                throw new InvalidIdException();
            }

            return _repository.ConsultarCompletePorId(codigo).Result;
        }

        public async Task<Resource<AgeClasesContribuyentesDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgeClasesContribuyentesSaveDAO ageClasesContribuyentesSaveDAO)
        {
            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    AgeClasesContribuyentes ageClasesContribuyentes = ValidarInsert(_httpContextAccessor, ageClasesContribuyentesSaveDAO);

                    AgeClasesContribuyentesDAO ageClasesContribuyentesDAO = await _repository.Insertar(ageClasesContribuyentes);

                    Resource<AgeClasesContribuyentesDAO> ageClasesContribuyentesDAOWithResource = GetDataWithResource(_httpContextAccessor, ageClasesContribuyentesDAO);

                    transactionScope.Complete();

                    return ageClasesContribuyentesDAOWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public async Task<List<Resource<AgeClasesContribuyentesDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeClasesContribuyentesSaveDAO> ageClasesContribuyentesSaveDAOList)
        {
            if (ageClasesContribuyentesSaveDAOList == null)
            {
                throw new InvalidSintaxisException();
            }

            List<AgeClasesContribuyentes> ageClasesContribuyentesList = new List<AgeClasesContribuyentes>();

            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    foreach (AgeClasesContribuyentesSaveDAO ageClasesContribuyentesSaveDAO in ageClasesContribuyentesSaveDAOList)
                    {
                        AgeClasesContribuyentes ageClaseContribuyente = ValidarInsert(_httpContextAccessor, ageClasesContribuyentesSaveDAO);
                        ageClasesContribuyentesList.Add(ageClaseContribuyente);
                    }

                    List<AgeClasesContribuyentesDAO> ageClasesContribuyentesDAOList = await _repository.InsertarVarios(ageClasesContribuyentesList);

                    List<Resource<AgeClasesContribuyentesDAO>> ageClasesContribuyentesDAOListWithResource = new List<Resource<AgeClasesContribuyentesDAO>>();

                    foreach (AgeClasesContribuyentesDAO ageClasesContribuyentesDAO in ageClasesContribuyentesDAOList)
                    {
                        ageClasesContribuyentesDAOListWithResource.Add(GetDataWithResource(_httpContextAccessor, ageClasesContribuyentesDAO));
                    }

                    transactionScope.Complete();

                    return ageClasesContribuyentesDAOListWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }
        public Task<AgeClasesContribuyentesDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeClasesContribuyentesSaveDAO ageClasesContribuyentesSaveDAO)
        {
            AgeClasesContribuyentes ageClasesContribuyentes = ValidarUpdate(_httpContextAccessor, ageClasesContribuyentesSaveDAO);

            return _repository.Actualizar(ageClasesContribuyentes);
        }

        public Task<List<AgeClasesContribuyentesDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeClasesContribuyentesSaveDAO> ageClasesContribuyentesSaveDAOList)
        {
            if (ageClasesContribuyentesSaveDAOList == null)
                throw new InvalidSintaxisException();

            ValidateKeys.ValidarPKDuplicadas(ageClasesContribuyentesSaveDAOList,
                                             p => new { p.Codigo },
                                             "Id");

            List<AgeClasesContribuyentes> ageClasesContribuyentesList = new List<AgeClasesContribuyentes>();
            AgeClasesContribuyentes ageClasesContribuyentes;

            foreach (AgeClasesContribuyentesSaveDAO ageClasesContribuyentesSaveDAO in ageClasesContribuyentesSaveDAOList)
            {
                ageClasesContribuyentes = ValidarUpdate(_httpContextAccessor, ageClasesContribuyentesSaveDAO);
                ageClasesContribuyentesList.Add(ageClasesContribuyentes);
            }

            return _repository.ActualizarVarios(ageClasesContribuyentesList);
        }

        private static Resource<AgeClasesContribuyentesDAO> GetDataWithResource(
            IHttpContextAccessor _httpContextAccessor,
            AgeClasesContribuyentesDAO ageClasesContribuyentesDAO)
        {
            string rutaSegmentoFinal = $"{ageClasesContribuyentesDAO.Codigo}";

            return Resource<AgeClasesContribuyentesDAO>.GetDataWithResource<AgeClasesContribuyentesController>(
                _httpContextAccessor, rutaSegmentoFinal, ageClasesContribuyentesDAO);
        }

        private AgeClasesContribuyentes ValidarUpdate(
            IHttpContextAccessor _httpContextAccessor,
            AgeClasesContribuyentesSaveDAO ageClasesContribuyentesSaveDAO)
        {
            if (ageClasesContribuyentesSaveDAO == null)
                throw new InvalidSintaxisException();
            
            if (ageClasesContribuyentesSaveDAO.UsuarioModificacion == null || ageClasesContribuyentesSaveDAO.UsuarioModificacion <= 0)
                throw new InvalidFieldException("El usuario de modificación debe ser un número entero mayor a cero.");

            AgeClasesContribuyentes ageClasesContribuyentes = ConsultarCompletePorId(ageClasesContribuyentesSaveDAO.Codigo) ?? 
                throw new RegisterNotFoundException($"Clase Contribuyente con código {ageClasesContribuyentesSaveDAO.Codigo} no existe.");

            if (!string.IsNullOrWhiteSpace(ageClasesContribuyentesSaveDAO.Estado))
            {
                ageClasesContribuyentes.Estado = ageClasesContribuyentesSaveDAO.Estado;
                ageClasesContribuyentes.FechaEstado = DateTime.Now;
            }

            if (!string.IsNullOrWhiteSpace(ageClasesContribuyentesSaveDAO.Descripcion))
                ageClasesContribuyentes.Descripcion = ageClasesContribuyentesSaveDAO.Descripcion;

            if (!string.IsNullOrWhiteSpace(ageClasesContribuyentesSaveDAO.ObservacionEstado))
                ageClasesContribuyentes.ObservacionEstado = ageClasesContribuyentesSaveDAO.ObservacionEstado;

            ageClasesContribuyentes.FechaModificacion = DateTime.Now;
            ageClasesContribuyentes.UbicacionModificacion = Ubicacion.getIpAdress(_httpContextAccessor);
            ageClasesContribuyentes.UsuarioModificacion = ageClasesContribuyentesSaveDAO.UsuarioModificacion;

            Validator.ValidateObject(ageClasesContribuyentes, new ValidationContext(ageClasesContribuyentes), true);

            return ageClasesContribuyentes;
        }

        private AgeClasesContribuyentes ValidarInsert(
        IHttpContextAccessor _httpContextAccessor,
        AgeClasesContribuyentesSaveDAO ageClasesContribuyentesSaveDAO)
        {
            if (ageClasesContribuyentesSaveDAO == null)
                throw new InvalidSintaxisException();

            ValidarPK(ageClasesContribuyentesSaveDAO.Codigo);

            Validator.ValidateObject(ageClasesContribuyentesSaveDAO, new ValidationContext(ageClasesContribuyentesSaveDAO), true);

            FuncionesSecuencias fg = new FuncionesSecuencias(_ageSecuenciasPrimariasService);

            ageClasesContribuyentesSaveDAO.Codigo = fg.ObtenerSecuenciaPrimaria(
                                                                                _httpContextAccessor,
                                                                                Globales.CODIGO_SECUENCIA_CLASE_CONTRIBUYENTE,
                                                                                ageClasesContribuyentesSaveDAO.UsuarioIngreso);


            AgeClasesContribuyentes entityObject = FromDAOToEntity(_httpContextAccessor, ageClasesContribuyentesSaveDAO);

            Validator.ValidateObject(entityObject, new ValidationContext(entityObject), true);

            return entityObject;
        }

        private AgeClasesContribuyentes FromDAOToEntity(
        IHttpContextAccessor _httpContextAccessor,
        AgeClasesContribuyentesSaveDAO ageClasesContribuyentesSaveDAO)
        {
            return new AgeClasesContribuyentes
            {
                Codigo = ageClasesContribuyentesSaveDAO.Codigo,
                Descripcion = ageClasesContribuyentesSaveDAO.Descripcion,
                Estado = ageClasesContribuyentesSaveDAO.Estado.ToUpper().ToString(),
                FechaEstado = DateTime.Now,
                FechaIngreso = DateTime.Now,
                ObservacionEstado = ageClasesContribuyentesSaveDAO.ObservacionEstado,
                UsuarioIngreso = ageClasesContribuyentesSaveDAO.UsuarioIngreso,
                UbicacionIngreso = Ubicacion.getIpAdress(_httpContextAccessor)
            };
        }

        private void ValidarPK(long codigo)
        {
            ValidateKeys.ValidarNoExistenciaKey(codigo,
                $"Clase Contribuyente con código: {codigo} ya existe.",
                _repository.ConsultarPorId);
        }
    }
}
