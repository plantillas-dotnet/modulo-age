using AGE.Controllers;
using AGE.Entities.DAO.AgeLocalidades;
using AGE.Entities;
using AGE.Middleware.Exceptions.BadRequest;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Repositories.AgeLocalidadesRepository;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using AGE.Utils;
using System.ComponentModel.DataAnnotations;
using System.Transactions;
using AGE.Repositories.AgeIdiomaRepository;
using AGE.Repositories.AgeTiposLocalidadeRepository;
using AGE.Entities.DAO.AgeTiposLocalidades;
using AGE.Services.AgeSecuenciasPrimariasService;
using Newtonsoft.Json;
using AGE.Repositories.AgeMonedasRepository;

namespace AGE.Services.AgeLocalidadesService
{
    public class AgeLocalidadesService : IAgeLocalidadesService
    {
        private readonly IAgeLocalidadesRepository _repository;
        private readonly IAgeIdiomasRepository _repositoryIdioma;
        private readonly IAgeTiposLocalidadesRepository _repositoryTiposLocalidade;
        private readonly IAgeMonedasRepository _repositoryMoneda;
        private readonly IAgeSecuenciasPrimariasService _ageSecuenciasPrimariasService;

        public AgeLocalidadesService(
            IAgeLocalidadesRepository Repository,
            IAgeIdiomasRepository RepositoryIdioma,
            IAgeTiposLocalidadesRepository RepositoryTiposLocalidade,
            IAgeMonedasRepository RepositoryMoneda,
            IAgeSecuenciasPrimariasService ageSecuenciasPrimariasService)
        {
            _repository = Repository;
            _repositoryIdioma = RepositoryIdioma;
            _repositoryTiposLocalidade = RepositoryTiposLocalidade;
            _repositoryMoneda = RepositoryMoneda;
            _ageSecuenciasPrimariasService = ageSecuenciasPrimariasService;
        }

        public Task<Page<AgeLocalidadesDAO>> ConsultarTodos(
            string descripcion,
            int ageTipLoCodigo,
            int ageTipLoAgePaisCodigo,
            int ageLocaliCodigo,
            int ageAgeTipLoAgePaisCodigo,
            int ageLocaliAgeTipLoCodigo,
            Pageable pageable)
        {

            pageable.Validar<AgeLocalidadesDAO>();

            return _repository.ConsultarTodos(descripcion, ageTipLoCodigo, ageTipLoAgePaisCodigo, ageLocaliCodigo, ageAgeTipLoAgePaisCodigo, ageLocaliAgeTipLoCodigo, pageable);
        
        }

        public Task<AgeLocalidadesDAO> ConsultarPorId(
            AgeLocalidadesPKDAO ageLocalidadesPKDAO)
        {
            Validator.ValidateObject(ageLocalidadesPKDAO, new ValidationContext(ageLocalidadesPKDAO), true);

            AgeLocalidadesDAO ageLocalidadesDAO = _repository.ConsultarPorId(ageLocalidadesPKDAO).Result;

            return ageLocalidadesDAO == null
                ? throw new RegisterNotFoundException("Localidad con código: "
                    + ageLocalidadesPKDAO.Codigo + " no existe.")
                : Task.FromResult(ageLocalidadesDAO);
        }

        private AgeLocalidades ConsultarCompletePorId(
            AgeLocalidadesPKDAO ageLocalidadesPKDAO)
        {
            Validator.ValidateObject(ageLocalidadesPKDAO, new ValidationContext(ageLocalidadesPKDAO), true);

            AgeLocalidades ageLocalidades = _repository.ConsultarCompletePorId(ageLocalidadesPKDAO).Result;

            return ageLocalidades;
        }

        public Task<Page<AgeLocalidadesDAO>> ConsultarListaFiltro(
            string filtro,
            Pageable pageable)
        {
            if (string.IsNullOrWhiteSpace(filtro))
                throw new InvalidFieldException("El filtro es requerido.");

            pageable.Validar<AgeLocalidadesDAO>();

            return _repository.ConsultarListaFiltro(filtro.ToLower(), pageable);
        }

        public async Task<Resource<AgeLocalidadesDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgeLocalidadesSaveDAO ageLocalidadesSaveDAO)
        {
            
            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    AgeLocalidades ageLocalidades = ValidarInsert(_httpContextAccessor, ageLocalidadesSaveDAO);

                    AgeLocalidadesDAO ageLocalidadesDAO = await _repository.Insertar(ageLocalidades);
                    
                    Resource<AgeLocalidadesDAO> ageLocalidadesDAOWithResource = GetDataWithResource(_httpContextAccessor, ageLocalidadesDAO);

                    transactionScope.Complete();

                    return ageLocalidadesDAOWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public async Task<List<Resource<AgeLocalidadesDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeLocalidadesSaveDAO> ageLocalidadesSaveDAOList)
        {
            if (ageLocalidadesSaveDAOList == null)
                throw new InvalidSintaxisException();

            List<AgeLocalidades> ageLocalidadesList = new List<AgeLocalidades>();

            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    foreach (AgeLocalidadesSaveDAO ageLocalidadesSaveDAO in ageLocalidadesSaveDAOList)
                    {
                        AgeLocalidades AgeLocalidade = ValidarInsert(_httpContextAccessor, ageLocalidadesSaveDAO);
                        ageLocalidadesList.Add(AgeLocalidade);
                    }

                    List<AgeLocalidadesDAO> ageLocalidadesDAOList = await _repository.InsertarVarios(ageLocalidadesList);

                    List<Resource<AgeLocalidadesDAO>> ageLocalidadesDAOListWithResource = new List<Resource<AgeLocalidadesDAO>>();

                    foreach (AgeLocalidadesDAO ageLocalidadesDAO in ageLocalidadesDAOList)
                    {
                        ageLocalidadesDAOListWithResource.Add(GetDataWithResource(_httpContextAccessor, ageLocalidadesDAO));
                    }

                    transactionScope.Complete();

                    return ageLocalidadesDAOListWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public Task<AgeLocalidadesDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeLocalidadesSaveDAO ageLocalidadesSaveDAO)
        {
            AgeLocalidades ageLocalidades = ValidarUpdate(_httpContextAccessor, ageLocalidadesSaveDAO);

            return _repository.Actualizar(ageLocalidades);
        }

        public Task<List<AgeLocalidadesDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeLocalidadesSaveDAO> ageLocalidadesSaveDAOList)
        {
            if (ageLocalidadesSaveDAOList == null)
                throw new InvalidSintaxisException();

            ValidateKeys.ValidarPKDuplicadas(ageLocalidadesSaveDAOList, p => new
            {
                p.Id.AgeTipLoCodigo,
                p.Id.AgeTipLoAgePaisCodigo,
                p.Id.Codigo
            }, "Id");

            List<AgeLocalidades> ageLocalidadesList = new List<AgeLocalidades>();
            AgeLocalidades ageLocalidades;

            foreach (AgeLocalidadesSaveDAO ageLocalidadesSaveDAO in ageLocalidadesSaveDAOList)
            {
                ageLocalidades = ValidarUpdate(_httpContextAccessor, ageLocalidadesSaveDAO);
                ageLocalidadesList.Add(ageLocalidades);
            }

            return _repository.ActualizarVarios(ageLocalidadesList);
        }

        private static Resource<AgeLocalidadesDAO> GetDataWithResource(
            IHttpContextAccessor _httpContextAccessor,
            AgeLocalidadesDAO ageLocalidadesDAO)
        {
            string rutaSegmentoFinal = $"{ageLocalidadesDAO.Id.AgeTipLoAgePaisCodigo}/{ageLocalidadesDAO.Id.AgeTipLoCodigo}/{ageLocalidadesDAO.Id.Codigo}";

            return Resource<AgeLocalidadesDAO>.GetDataWithResource<AgeLocalidadesController>(
                _httpContextAccessor, rutaSegmentoFinal, ageLocalidadesDAO);
        }

        private AgeLocalidades ValidarInsert(
                IHttpContextAccessor _httpContextAccessor,
                AgeLocalidadesSaveDAO ageLocalidadesSaveDAO)
        {
            if (ageLocalidadesSaveDAO == null)
                throw new InvalidSintaxisException();

            Validator.ValidateObject(ageLocalidadesSaveDAO, new ValidationContext(ageLocalidadesSaveDAO), true);

            FuncionesSecuencias fg = new FuncionesSecuencias(_ageSecuenciasPrimariasService);

            ageLocalidadesSaveDAO.Id.Codigo = fg.ObtenerSecuenciaPrimaria(
                                                _httpContextAccessor,
                                                Globales.CODIGO_SECUENCIA_LOCALIDAD,
                                                ageLocalidadesSaveDAO.UsuarioIngreso);

            ValidarKeys(ageLocalidadesSaveDAO);

            AgeLocalidades entityObject = FromDAOToEntity(_httpContextAccessor,ageLocalidadesSaveDAO);

            Validator.ValidateObject(entityObject, new ValidationContext(entityObject), true);

            return entityObject;
        }


        private AgeLocalidades ValidarUpdate(IHttpContextAccessor _httpContextAccessor,
                                            AgeLocalidadesSaveDAO ageLocalidadesSaveDAO)
        {
            if (ageLocalidadesSaveDAO == null)
                throw new InvalidSintaxisException();

            if (ageLocalidadesSaveDAO.UsuarioModificacion == null || ageLocalidadesSaveDAO.UsuarioModificacion <= 0)
                throw new InvalidFieldException("El usuario de modificación debe ser un número entero mayor a cero.");

            ValidarFKS(ageLocalidadesSaveDAO);

            AgeLocalidades ageLocalidades = ConsultarCompletePorId(ageLocalidadesSaveDAO.Id) 
                ?? throw new RegisterNotFoundException($"Localidad con código: " +
                $"{ageLocalidadesSaveDAO.Id.Codigo}, código tipo de localidad:" +
                $" {ageLocalidadesSaveDAO.Id.AgeTipLoCodigo} y código país:" +
                $" {ageLocalidadesSaveDAO.Id.AgeTipLoAgePaisCodigo} no existe.");

            ageLocalidades.Estado = !string.IsNullOrWhiteSpace(ageLocalidadesSaveDAO.Estado) ? ageLocalidadesSaveDAO.Estado : ageLocalidades.Estado;
            ageLocalidades.FechaEstado = !string.IsNullOrWhiteSpace(ageLocalidadesSaveDAO.Estado) ? DateTime.Now : ageLocalidades.FechaEstado;
            ageLocalidades.ObservacionEstado = !string.IsNullOrWhiteSpace(ageLocalidadesSaveDAO.ObservacionEstado) ? ageLocalidadesSaveDAO.ObservacionEstado : ageLocalidades.ObservacionEstado;
            ageLocalidades.Descripcion = !string.IsNullOrWhiteSpace(ageLocalidadesSaveDAO.Descripcion) ? ageLocalidadesSaveDAO.Descripcion : ageLocalidades.Descripcion;
            ageLocalidades.AgeAgeTipLoAgePaisCodigo = ageLocalidadesSaveDAO.AgeAgeTipLoAgePaisCodigo != null && ageLocalidadesSaveDAO.AgeAgeTipLoAgePaisCodigo > 0 ? ageLocalidadesSaveDAO.AgeAgeTipLoAgePaisCodigo : ageLocalidades.AgeAgeTipLoAgePaisCodigo;
            ageLocalidades.AgeIdiomaCodigo = ageLocalidadesSaveDAO.AgeIdiomaCodigo != null && ageLocalidadesSaveDAO.AgeIdiomaCodigo > 0 ? ageLocalidadesSaveDAO.AgeIdiomaCodigo : ageLocalidades.AgeIdiomaCodigo;
            ageLocalidades.AgeLocaliCodigo = ageLocalidadesSaveDAO.AgeLocaliCodigo != null && ageLocalidadesSaveDAO.AgeLocaliCodigo > 0 ? ageLocalidadesSaveDAO.AgeLocaliCodigo : ageLocalidades.AgeLocaliCodigo;
            ageLocalidades.AgeMonedaCodigo = ageLocalidadesSaveDAO.AgeMonedaCodigo != null && ageLocalidadesSaveDAO.AgeMonedaCodigo > 0 ? ageLocalidadesSaveDAO.AgeMonedaCodigo : ageLocalidades.AgeMonedaCodigo;
            ageLocalidades.LatitudCentro = ageLocalidadesSaveDAO.LatitudCentro != null && ageLocalidadesSaveDAO.LatitudCentro > 0 ? ageLocalidadesSaveDAO.LatitudCentro : ageLocalidades.LatitudCentro;
            ageLocalidades.LongitudCentro = ageLocalidadesSaveDAO.LongitudCentro != null && ageLocalidadesSaveDAO.LongitudCentro > 0 ? ageLocalidadesSaveDAO.LongitudCentro : ageLocalidades.LongitudCentro;
            ageLocalidades.Poligono = ConvertListToByteArray(ageLocalidadesSaveDAO.Poligono) ?? ageLocalidades.Poligono;

            ageLocalidades.FechaModificacion = DateTime.Now;
            ageLocalidades.UbicacionModificacion = Ubicacion.getIpAdress(_httpContextAccessor);
            ageLocalidades.UsuarioModificacion = ageLocalidadesSaveDAO.UsuarioModificacion;
            Validator.ValidateObject(ageLocalidades, new ValidationContext(ageLocalidades), true);

            return ageLocalidades;
        }

        private AgeLocalidades FromDAOToEntity(
           IHttpContextAccessor _httpContextAccessor,
           AgeLocalidadesSaveDAO ageLocalidadesSaveDAO)
        {
            return new AgeLocalidades
            {
                AgeTipLoAgePaisCodigo = ageLocalidadesSaveDAO.Id.AgeTipLoAgePaisCodigo,
                AgeTipLoCodigo = ageLocalidadesSaveDAO.Id.AgeTipLoCodigo,
                Codigo = ageLocalidadesSaveDAO.Id.Codigo,
                Descripcion = ageLocalidadesSaveDAO.Descripcion,
                AgeAgeTipLoAgePaisCodigo = ageLocalidadesSaveDAO.AgeAgeTipLoAgePaisCodigo,
                AgeLocaliAgeTipLoCodigo = ageLocalidadesSaveDAO.AgeLocaliAgeTipLoCodigo,
                AgeIdiomaCodigo = ageLocalidadesSaveDAO.AgeIdiomaCodigo,
                AgeLocaliCodigo = ageLocalidadesSaveDAO.AgeLocaliCodigo,
                AgeMonedaCodigo = ageLocalidadesSaveDAO.AgeMonedaCodigo,
                LatitudCentro = ageLocalidadesSaveDAO.LatitudCentro,
                LongitudCentro = ageLocalidadesSaveDAO.LongitudCentro,
                Poligono = ConvertListToByteArray(ageLocalidadesSaveDAO.Poligono),
                Estado = ageLocalidadesSaveDAO.Estado,
                FechaEstado = DateTime.Now,
                FechaIngreso = DateTime.Now,
                ObservacionEstado = ageLocalidadesSaveDAO.ObservacionEstado,
                UbicacionIngreso = Ubicacion.getIpAdress(_httpContextAccessor),
                UsuarioIngreso = ageLocalidadesSaveDAO.UsuarioIngreso
            };
        }
        
        private void ValidarKeys(AgeLocalidadesSaveDAO ageLocalidadesSaveDAO)
        {
            ValidarPK(ageLocalidadesSaveDAO.Id);
            ValidarFKS(ageLocalidadesSaveDAO);
        }

        private void ValidarPK(AgeLocalidadesPKDAO ageLocalidadesPKDAO)
        {
            ValidateKeys.ValidarNoExistenciaKey(ageLocalidadesPKDAO,
                    $"Localidad con código: {ageLocalidadesPKDAO.Codigo}," +
                    $" código de país por tipo de localidad: {ageLocalidadesPKDAO.AgeTipLoAgePaisCodigo}" +
                    $" y código de tipo de localidad: {ageLocalidadesPKDAO.AgeTipLoCodigo} ya existe."
                    , _repository.ConsultarCompletePorId);
        }

        private void ValidarFKS(AgeLocalidadesSaveDAO ageLocalidadesSaveDAO)
        {

            if ((ageLocalidadesSaveDAO.AgeLocaliCodigo > 0 && ageLocalidadesSaveDAO.AgeLocaliCodigo != null) ||
                (ageLocalidadesSaveDAO.AgeLocaliAgeTipLoCodigo > 0 && ageLocalidadesSaveDAO.AgeLocaliAgeTipLoCodigo != null) ||
                (ageLocalidadesSaveDAO.AgeAgeTipLoAgePaisCodigo > 0 && ageLocalidadesSaveDAO.AgeAgeTipLoAgePaisCodigo != null))
            {
                AgeLocalidadesPKDAO ageLocalidadesPKDAO = new AgeLocalidadesPKDAO
                {
                    AgeTipLoAgePaisCodigo = ageLocalidadesSaveDAO.AgeAgeTipLoAgePaisCodigo ?? 0,
                    AgeTipLoCodigo = ageLocalidadesSaveDAO.AgeLocaliAgeTipLoCodigo ?? 0,
                    Codigo = ageLocalidadesSaveDAO.AgeLocaliCodigo ?? 0
                };
                ValidarFKLocalidad(ageLocalidadesPKDAO);
            }

            if (ageLocalidadesSaveDAO.AgeIdiomaCodigo > 0 && ageLocalidadesSaveDAO.AgeIdiomaCodigo != null)
                ValidarFKIdioma(ageLocalidadesSaveDAO.AgeIdiomaCodigo ?? 0);

            AgeTiposLocalidadePKDAO ageTiposLocalidadePKDAO = new AgeTiposLocalidadePKDAO
            {
                agePaisCodigo = ageLocalidadesSaveDAO.Id.AgeTipLoAgePaisCodigo,
                codigo = ageLocalidadesSaveDAO.Id.AgeTipLoCodigo
            };
            ValidarFKTipoLocalidad(ageTiposLocalidadePKDAO);

            if(ageLocalidadesSaveDAO.AgeMonedaCodigo > 0 && ageLocalidadesSaveDAO.AgeMonedaCodigo != null)
                ValidarFKMoneda(ageLocalidadesSaveDAO.AgeMonedaCodigo ?? 0);
        }

        private void ValidarFKLocalidad(AgeLocalidadesPKDAO ageLocalidadesPKDAO)
        {
            ValidateKeys.ValidarExistenciaKey(ageLocalidadesPKDAO,
                    $"Localidad con código: {ageLocalidadesPKDAO.Codigo}, " +
                    $"código país: {ageLocalidadesPKDAO.AgeTipLoAgePaisCodigo} " +
                    $"y código de tipo localidad: {ageLocalidadesPKDAO.AgeTipLoCodigo} no existe."
                    , _repository.ConsultarCompletePorId);
        }
            

        private void ValidarFKIdioma(int ageIdiomaCodigo)
        {
            ValidateKeys.ValidarExistenciaKey(ageIdiomaCodigo,
                    $"Idioma con código: {ageIdiomaCodigo} no existe."
                    , _repositoryIdioma.ConsultarCompletePorId);
        }

        private void ValidarFKTipoLocalidad(AgeTiposLocalidadePKDAO ageTiposLocalidadePKDAO)
        {
            ValidateKeys.ValidarExistenciaKey(ageTiposLocalidadePKDAO,
                        $"Tipo Localidad con código: {ageTiposLocalidadePKDAO.codigo} " +
                        $"y código país: {ageTiposLocalidadePKDAO.agePaisCodigo} no existe."
                        , _repositoryTiposLocalidade.ConsultarCompletePorId);
        }

        private void ValidarFKMoneda(int codigoMoneda)
        {
            ValidateKeys.ValidarExistenciaKey(codigoMoneda,
                    $"Moneda con código: {codigoMoneda} no existe."
                    , _repositoryMoneda.ConsultarCompletePorId);
        }

        private byte[]? ConvertListToByteArray(List<List<List<decimal>>> poligonoList)
        {
            if (poligonoList == null)
                return null;
            try
            {
                string jsonString = JsonConvert.SerializeObject(poligonoList);
                return System.Text.Encoding.UTF8.GetBytes(jsonString);
            }
            catch (Exception)
            {
                throw new JsonDeserializationException("Campo polígono no se puede deserializar.");
            }
        }
    }
}
