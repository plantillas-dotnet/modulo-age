﻿using AGE.Entities.DAO.AgeLocalidades;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;

namespace AGE.Services.AgeLocalidadesService
{
    public interface IAgeLocalidadesService
    {
        Task<Page<AgeLocalidadesDAO>> ConsultarTodos(
            string descripcion,
            int ageTipLoCodigo,
            int ageTipLoAgePaisCodigo,
            int ageLocaliCodigo,
            int ageAgeTipLoAgePaisCodigo,
            int ageLocaliAgeTipLoCodigo,
            Pageable pageable);

        Task<Page<AgeLocalidadesDAO>> ConsultarListaFiltro(
            string filtro,
            Pageable pageable);

        Task<AgeLocalidadesDAO> ConsultarPorId(AgeLocalidadesPKDAO ageLocalidadesPKDAO);

        Task<Resource<AgeLocalidadesDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgeLocalidadesSaveDAO AgeLocalidadesSaveDAO);

        Task<List<Resource<AgeLocalidadesDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeLocalidadesSaveDAO> AgeLocalidadesSaveDAOList);

        Task<AgeLocalidadesDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeLocalidadesSaveDAO AgeLocalidadesSaveDAO);

        Task<List<AgeLocalidadesDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeLocalidadesSaveDAO> AgeLocalidadesSaveDAOList);
    }
}
