﻿using AGE.Entities.DAO.AgeLicenciatarios;
using AGE.Repositories.AgeLicenciatariosRepository;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using System.ComponentModel.DataAnnotations;
using AGE.Services.AgeSecuenciasPrimariasService;
using AGE.Entities;
using AGE.Middleware.Exceptions.BadRequest;
using AGE.Utils;
using AGE.Controllers;
using System.Transactions;
using AGE.Repositories.AgeClasesContribuyentesRepository;
using AGE.Repositories.AgeTiposIdentificacioneRepository;

namespace AGE.Services.AgeLicenciatariosService
{
    public class AgeLicenciatariosService : IAgeLicenciatariosService
    {
        private readonly IAgeLicenciatariosRepository _repository;
        private readonly IAgeClasesContribuyentesRepository _repositoryClaseContribuyente;
        private readonly IAgeTiposIdentificacionesRepository _repositoryTipoIdentificacion;
        private readonly IAgeSecuenciasPrimariasService _ageSecuenciasPrimariasService;

        public AgeLicenciatariosService(
            IAgeLicenciatariosRepository Repository,
            IAgeClasesContribuyentesRepository RepositoryClaseContribuyente,
            IAgeTiposIdentificacionesRepository RepositoryTiposIdentificacione,
            IAgeSecuenciasPrimariasService ageSecuenciasPrimariasService)
        {
            _repository = Repository;
            _repositoryClaseContribuyente = RepositoryClaseContribuyente;
            _repositoryTipoIdentificacion = RepositoryTiposIdentificacione;
            _ageSecuenciasPrimariasService = ageSecuenciasPrimariasService;
        }
        public Task<Page<AgeLicenciatarioDAO>> ConsultarTodos(int codigo, string descripcion, Pageable pageable)
        {
            pageable.Validar<AgeLicenciatarioDAO>();

            return _repository.ConsultarTodos(codigo, descripcion, pageable);
        }

        public Task<AgeLicenciatarioDAO> ConsultarPorId(int codigo)
        {
            if (codigo <= 0)
                throw new InvalidIdException();

            AgeLicenciatarioDAO? ageLicenciatariosDAO = _repository.ConsultarPorId(codigo).Result;

            return ageLicenciatariosDAO == null
                ? throw new RegisterNotFoundException("Licenciatario con código: " + codigo + " no existe.")
                : Task.FromResult(ageLicenciatariosDAO);
        }

        private AgeLicenciatario ConsultarCompletePorId(
            int codigo)
        {
            if (codigo <= 0)
                throw new InvalidIdException();

            return _repository.ConsultarCompletePorId(codigo).Result;
        }

        public Task<Page<AgeLicenciatarioDAO>> ConsultarListaFiltro(string filtro, Pageable pageable)
        {
            if (string.IsNullOrWhiteSpace(filtro))
                throw new InvalidFieldException("El filtro es requerido.");

            pageable.Validar<AgeLicenciatarioDAO>();

            return _repository.ConsultarListaFiltro(filtro, pageable);
        }

        public async Task<Resource<AgeLicenciatarioDAO>> Insertar(IHttpContextAccessor _httpContextAccessor, AgeLicenciatarioSaveDAO ageLicenciatariosSaveDAO)
        {
            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    AgeLicenciatario ageLicenciatario = ValidarInsert(_httpContextAccessor, ageLicenciatariosSaveDAO);

                    AgeLicenciatarioDAO ageLicenciatariosDAO = await _repository.Insertar(ageLicenciatario);

                    Resource<AgeLicenciatarioDAO> AgeLicenciatariosDAOWithResource = GetDataWithResource(_httpContextAccessor, ageLicenciatariosDAO);

                    transactionScope.Complete();

                    return AgeLicenciatariosDAOWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public async Task<List<Resource<AgeLicenciatarioDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor, 
            List<AgeLicenciatarioSaveDAO> ageLicenciatariosSaveDAOList)
        {
            if (ageLicenciatariosSaveDAOList == null)
                throw new InvalidSintaxisException();

            List<AgeLicenciatario> ageLicenciatarioList = new List<AgeLicenciatario>();

            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    foreach (AgeLicenciatarioSaveDAO ageLicenciatarioSaveDAO in ageLicenciatariosSaveDAOList)
                    {
                        AgeLicenciatario AgeLicenciatario = ValidarInsert(_httpContextAccessor, ageLicenciatarioSaveDAO);
                        ageLicenciatarioList.Add(AgeLicenciatario);
                    }

                    List<AgeLicenciatarioDAO> ageLicenciatariosDAOList = await _repository.InsertarVarios(ageLicenciatarioList);

                    List<Resource<AgeLicenciatarioDAO>> ageLicenciatariosDAOListWithResource = new List<Resource<AgeLicenciatarioDAO>>();

                    foreach (AgeLicenciatarioDAO ageLicenciatariosDAO in ageLicenciatariosDAOList)
                    {
                        ageLicenciatariosDAOListWithResource.Add(GetDataWithResource(_httpContextAccessor, ageLicenciatariosDAO));
                    }

                    transactionScope.Complete();

                    return ageLicenciatariosDAOListWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public Task<AgeLicenciatarioDAO> Actualizar(IHttpContextAccessor _httpContextAccessor, AgeLicenciatarioSaveDAO ageLicenciatariosSaveDAO)
        {
            AgeLicenciatario ageLicenciatario = ValidarUpdate(_httpContextAccessor, ageLicenciatariosSaveDAO);

            return _repository.Actualizar(ageLicenciatario);
        }

        public Task<List<AgeLicenciatarioDAO>> ActualizarVarios(IHttpContextAccessor _httpContextAccessor, List<AgeLicenciatarioSaveDAO> ageLicenciatariosSaveDAOList)
        {
            if (ageLicenciatariosSaveDAOList == null)
                throw new InvalidSintaxisException();

            ValidateKeys.ValidarPKDuplicadas(ageLicenciatariosSaveDAOList, p => new { p.Codigo }, "Codigo");

            List<AgeLicenciatario> ageLicenciatarioList = new List<AgeLicenciatario>();
            AgeLicenciatario ageLicenciatario;

            foreach (AgeLicenciatarioSaveDAO ageLicenciatariosSaveDAO in ageLicenciatariosSaveDAOList)
            {
                ageLicenciatario = ValidarUpdate(_httpContextAccessor, ageLicenciatariosSaveDAO);
                ageLicenciatarioList.Add(ageLicenciatario);
            }

            return _repository.ActualizarVarios(ageLicenciatarioList);
        }

        private static Resource<AgeLicenciatarioDAO> GetDataWithResource(
        IHttpContextAccessor _httpContextAccessor,
        AgeLicenciatarioDAO ageLicenciatariosDAO)
        {
            string rutaSegmentoFinal = $"{ageLicenciatariosDAO.Codigo}";

            return Resource<AgeLicenciatarioDAO>.GetDataWithResource<AgeLicenciatariosController>(
                _httpContextAccessor, rutaSegmentoFinal, ageLicenciatariosDAO);
        }

        private AgeLicenciatario ValidarInsert(
            IHttpContextAccessor _httpContextAccessor,
            AgeLicenciatarioSaveDAO ageLicenciatariosSaveDAO)
        {
            if (ageLicenciatariosSaveDAO == null)
                throw new InvalidSintaxisException();

            Validator.ValidateObject(ageLicenciatariosSaveDAO, new ValidationContext(ageLicenciatariosSaveDAO), true);

            FuncionesSecuencias fg = new FuncionesSecuencias(_ageSecuenciasPrimariasService);

            ageLicenciatariosSaveDAO.Codigo = fg.ObtenerSecuenciaPrimaria(
                        _httpContextAccessor,
                        Globales.CODIGO_SECUENCIA_LICENCIATARIO,
                        ageLicenciatariosSaveDAO.UsuarioIngreso);

            ValidarKeys(ageLicenciatariosSaveDAO);

            AgeLicenciatario ageLicenciatario = FromDAOToEntity(_httpContextAccessor,ageLicenciatariosSaveDAO);

            Validator.ValidateObject(ageLicenciatario, new ValidationContext(ageLicenciatario), true);

            return ageLicenciatario;
        }

        private AgeLicenciatario ValidarUpdate(
            IHttpContextAccessor _httpContextAccessor,
            AgeLicenciatarioSaveDAO ageLicenciatariosSaveDAO)
        {
            if (ageLicenciatariosSaveDAO == null)
                throw new InvalidSintaxisException();

            if (ageLicenciatariosSaveDAO.UsuarioModificacion == null || ageLicenciatariosSaveDAO.UsuarioModificacion <= 0)
                throw new InvalidFieldException("El usuario de modificación debe ser un número entero mayor a cero.");

            ValidarFKs(ageLicenciatariosSaveDAO);

            AgeLicenciatario AgeLicenciatarios = ConsultarCompletePorId(ageLicenciatariosSaveDAO.Codigo) 
                ?? throw new RegisterNotFoundException($"Licenciatario con código {ageLicenciatariosSaveDAO.Codigo} no existe.");
            
            if (!string.IsNullOrWhiteSpace(ageLicenciatariosSaveDAO.NombreCorto))
                AgeLicenciatarios.NombreCorto = ageLicenciatariosSaveDAO.NombreCorto;

            if (!string.IsNullOrWhiteSpace(ageLicenciatariosSaveDAO.RepresentanteLegal))
                AgeLicenciatarios.RepresentanteLegal = ageLicenciatariosSaveDAO.RepresentanteLegal;

            if (!string.IsNullOrWhiteSpace(ageLicenciatariosSaveDAO.DireccionPrincipal))
                AgeLicenciatarios.DireccionPrincipal = ageLicenciatariosSaveDAO.DireccionPrincipal;

            if (!string.IsNullOrWhiteSpace(ageLicenciatariosSaveDAO.Telefono1))
                AgeLicenciatarios.Telefono1 = ageLicenciatariosSaveDAO.Telefono1;

            if (!string.IsNullOrWhiteSpace(ageLicenciatariosSaveDAO.EMail2))
                AgeLicenciatarios.EMail2 = ageLicenciatariosSaveDAO.EMail2;

            if (!string.IsNullOrWhiteSpace(ageLicenciatariosSaveDAO.EsCorporacion))
                AgeLicenciatarios.EsCorporacion = ageLicenciatariosSaveDAO.EsCorporacion;

            if (!string.IsNullOrWhiteSpace(ageLicenciatariosSaveDAO.Observacion))
                AgeLicenciatarios.Observacion = ageLicenciatariosSaveDAO.Observacion;

            if (!string.IsNullOrWhiteSpace(ageLicenciatariosSaveDAO.PaginaWeb))
                AgeLicenciatarios.PaginaWeb = ageLicenciatariosSaveDAO.PaginaWeb;

            if (!string.IsNullOrWhiteSpace(ageLicenciatariosSaveDAO.ContribuyenteEspecialResoluc))
                AgeLicenciatarios.ContribuyenteEspecialResoluc = ageLicenciatariosSaveDAO.ContribuyenteEspecialResoluc;

            if (!string.IsNullOrWhiteSpace(ageLicenciatariosSaveDAO.RutaLicenciatario))
                AgeLicenciatarios.RutaLicenciatario = ageLicenciatariosSaveDAO.RutaLicenciatario;

            if (!string.IsNullOrWhiteSpace(ageLicenciatariosSaveDAO.RazonSocial))
                AgeLicenciatarios.RazonSocial = ageLicenciatariosSaveDAO.RazonSocial;

            if (!string.IsNullOrWhiteSpace(ageLicenciatariosSaveDAO.NombreComercial))
                AgeLicenciatarios.NombreComercial = ageLicenciatariosSaveDAO.NombreComercial;

            if (!string.IsNullOrWhiteSpace(ageLicenciatariosSaveDAO.NumeroIdentificacion))
                AgeLicenciatarios.NumeroIdentificacion = ageLicenciatariosSaveDAO.NumeroIdentificacion;

            if (!string.IsNullOrWhiteSpace(ageLicenciatariosSaveDAO.Telefono2))
                AgeLicenciatarios.Telefono2 = ageLicenciatariosSaveDAO.Telefono2;

            if (!string.IsNullOrWhiteSpace(ageLicenciatariosSaveDAO.EMail1))
                AgeLicenciatarios.EMail1 = ageLicenciatariosSaveDAO.EMail1;

            if (!string.IsNullOrWhiteSpace(ageLicenciatariosSaveDAO.Descripcion))
                AgeLicenciatarios.Descripcion = ageLicenciatariosSaveDAO.Descripcion;

            if (!string.IsNullOrWhiteSpace(ageLicenciatariosSaveDAO.Estado))
            {
                AgeLicenciatarios.Estado = ageLicenciatariosSaveDAO.Estado;
                AgeLicenciatarios.FechaEstado = DateTime.Now;
            }

            if (ageLicenciatariosSaveDAO.AgeLicencCodigo > 0 && ageLicenciatariosSaveDAO.AgeLicencCodigo == null)
                AgeLicenciatarios.AgeLicencCodigo = ageLicenciatariosSaveDAO.AgeLicencCodigo;

            if (!string.IsNullOrWhiteSpace(ageLicenciatariosSaveDAO.Descripcion))
                AgeLicenciatarios.Descripcion = ageLicenciatariosSaveDAO.Descripcion;

            if (!string.IsNullOrWhiteSpace(ageLicenciatariosSaveDAO.ObservacionEstado))
                AgeLicenciatarios.ObservacionEstado = ageLicenciatariosSaveDAO.ObservacionEstado;

            AgeLicenciatarios.FechaModificacion = DateTime.Now;
            AgeLicenciatarios.UbicacionModificacion = Ubicacion.getIpAdress(_httpContextAccessor);
            AgeLicenciatarios.UsuarioModificacion = ageLicenciatariosSaveDAO.UsuarioModificacion;

            Validator.ValidateObject(AgeLicenciatarios, new ValidationContext(AgeLicenciatarios), true);

            return AgeLicenciatarios;
        }

        private AgeLicenciatario FromDAOToEntity(
           IHttpContextAccessor _httpContextAccessor,
           AgeLicenciatarioSaveDAO ageLicenciatariosSaveDAO)
        {

            return new AgeLicenciatario()
            {
                Codigo = ageLicenciatariosSaveDAO.Codigo,
                AgeClaCoCodigo = ageLicenciatariosSaveDAO.AgeClaCoCodigo,
                AgeTipIdCodigo = ageLicenciatariosSaveDAO.AgeTipIdCodigo,
                NombreCorto = ageLicenciatariosSaveDAO.NombreCorto,
                RepresentanteLegal = ageLicenciatariosSaveDAO.RepresentanteLegal,
                DireccionPrincipal = ageLicenciatariosSaveDAO.DireccionPrincipal,
                Telefono1 = ageLicenciatariosSaveDAO.Telefono1,
                EMail2 = ageLicenciatariosSaveDAO.EMail2,
                EsCorporacion = ageLicenciatariosSaveDAO.EsCorporacion,
                Observacion = ageLicenciatariosSaveDAO.Observacion,
                PaginaWeb = ageLicenciatariosSaveDAO.PaginaWeb,
                ContribuyenteEspecialResoluc = ageLicenciatariosSaveDAO.ContribuyenteEspecialResoluc,
                RutaLicenciatario = ageLicenciatariosSaveDAO.RutaLicenciatario,
                AgeLicencCodigo = ageLicenciatariosSaveDAO.AgeLicencCodigo,
                RazonSocial = ageLicenciatariosSaveDAO.RazonSocial,
                NombreComercial = ageLicenciatariosSaveDAO.NombreComercial,
                NumeroIdentificacion = ageLicenciatariosSaveDAO.NumeroIdentificacion,
                Telefono2 = ageLicenciatariosSaveDAO.Telefono2,
                EMail1 = ageLicenciatariosSaveDAO.EMail1,
                ObligadoLlevarContabilidad = ageLicenciatariosSaveDAO.ObligadoLlevarContabilidad,
                Descripcion = ageLicenciatariosSaveDAO.Descripcion,
                Estado = ageLicenciatariosSaveDAO.Estado,
                FechaEstado = DateTime.Now,
                FechaIngreso = DateTime.Now,
                ObservacionEstado = ageLicenciatariosSaveDAO.ObservacionEstado,
                UbicacionIngreso = Ubicacion.getIpAdress(_httpContextAccessor),
                UsuarioIngreso = ageLicenciatariosSaveDAO.UsuarioIngreso
            };
        }

        private void ValidarKeys(AgeLicenciatarioSaveDAO ageLicenciatariosSaveDAO)
        {
            ValidarPK(ageLicenciatariosSaveDAO.Codigo);
            ValidarFKs(ageLicenciatariosSaveDAO);
        }

        private void ValidarPK(int codigoLicenciatario)
        {
            ValidateKeys.ValidarNoExistenciaKey(codigoLicenciatario,
                $"Licenciatario con código: {codigoLicenciatario} ya existe.",
                _repository.ConsultarPorId);
        }

        private void ValidarFKs(AgeLicenciatarioSaveDAO ageLicenciatariosSaveDAO)
        {
            if (ageLicenciatariosSaveDAO.AgeLicencCodigo != null && ageLicenciatariosSaveDAO.AgeLicencCodigo > 0)
                ValidarFKLicenciatario(ageLicenciatariosSaveDAO.AgeLicencCodigo ?? 0);

            ValidarFKClaseContribuyente(ageLicenciatariosSaveDAO.AgeClaCoCodigo);
            ValidarFKTipoIdentificacion(ageLicenciatariosSaveDAO.AgeTipIdCodigo);
        }

        private void ValidarFKLicenciatario(int codigoLicenciatario)
        {
            ValidateKeys.ValidarExistenciaKey(codigoLicenciatario,
                $"Licenciatario con código: {codigoLicenciatario} no existe.",
                _repository.ConsultarPorId);
        }

        private void ValidarFKClaseContribuyente(long codigoClaseContribuyente)
        {
            ValidateKeys.ValidarExistenciaKey(codigoClaseContribuyente,
                $"Clase contribuyente con código: {codigoClaseContribuyente} no existe.",
                _repositoryClaseContribuyente.ConsultarPorId);
        }

        private void ValidarFKTipoIdentificacion(int codigoTipoIdentificacion)
        {
            ValidateKeys.ValidarExistenciaKey(codigoTipoIdentificacion,
                $"Tipo Identificación con código: {codigoTipoIdentificacion} no existe.",
                _repositoryTipoIdentificacion.ConsultarPorId);
        }
    }
}
