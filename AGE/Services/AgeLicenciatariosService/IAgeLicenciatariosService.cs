﻿using AGE.Entities;
using AGE.Entities.DAO.AgeLicenciatarios;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;

namespace AGE.Services.AgeLicenciatariosService
{
    public interface IAgeLicenciatariosService
    {
        Task<Page<AgeLicenciatarioDAO>> ConsultarTodos(int codigo, string descripcion, Pageable pageable);

        Task<Page<AgeLicenciatarioDAO>> ConsultarListaFiltro(
            string filtro,
            Pageable pageable);

        Task<AgeLicenciatarioDAO> ConsultarPorId(int codigo);

        Task<Resource<AgeLicenciatarioDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgeLicenciatarioSaveDAO ageLicenciatariosSaveDAO);

        Task<List<Resource<AgeLicenciatarioDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeLicenciatarioSaveDAO> ageLicenciatariosSaveDAOList);

        Task<AgeLicenciatarioDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeLicenciatarioSaveDAO ageLicenciatariosSaveDAO);

        Task<List<AgeLicenciatarioDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeLicenciatarioSaveDAO> ageLicenciatariosSaveDAOList);
    }
}
