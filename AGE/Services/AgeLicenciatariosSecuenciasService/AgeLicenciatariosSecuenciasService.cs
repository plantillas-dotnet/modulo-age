﻿
using AGE.Entities;
using AGE.Utils;
using AGE.Utils.Paginacion;
using AGE.Middleware.Exceptions.BadRequest;
using AGE.Middleware.Exceptions.NotFound;
using System.ComponentModel.DataAnnotations;
using AGE.Utils.WebLink;
using AGE.Controllers;
using AGE.Entities.DAO.AgeLicenciatarioAplicacion;
using AGE.Repositories.AgeLicenciatariosAplicacionRepository;
using System.Transactions;
using AGE.Repositories.AgeLicenciatariosSecuenciasRepository;
using AGE.Entities.DAO.AgeLicenciatariosSecuencias;
using AGE.Repositories.AgeLicenciatariosRepository;
using AGE.Entities.DAO.AgeUsuarios;

namespace AGE.Services.AgeLicenciatariosSecuenciaService
{
    public class AgeLicenciatariosSecuenciasService : IAgeLicenciatariosSecuenciasService
    {

        private readonly IAgeLicenciatariosSecuenciasRepository _repository;
        private readonly IAgeLicenciatariosRepository _repositoryLicenciatario;

        public AgeLicenciatariosSecuenciasService(IAgeLicenciatariosSecuenciasRepository Repository,
                                                    IAgeLicenciatariosRepository RepositoryLicenciatario)
        {
            _repository = Repository;
            _repositoryLicenciatario = RepositoryLicenciatario;
        }

        public Task<Page<AgeLicenciatariosSecuenciasDAO>> ConsultarTodos(
            int codigoLicenciatario,
            int codigo,
            string ciclica,
            string descripcion,
            Pageable pageable)
        {

            if (codigoLicenciatario <= 0)
                throw new InvalidFieldException("El código de licenciatario debe ser un número entero mayor a cero.");

            pageable.Validar<AgeLicenciatariosSecuenciasDAO>();

            return _repository.ConsultarTodos(codigoLicenciatario, codigo, ciclica, descripcion, pageable);
        }

        public Task<AgeLicenciatariosSecuencia> ConsultarPorSecuencia(
            AgeLicenciatariosSecuenciasPKDAO ageLicenciatariosSecuenciaPKDAO)
        {
            Validator.ValidateObject(ageLicenciatariosSecuenciaPKDAO, new ValidationContext(ageLicenciatariosSecuenciaPKDAO), true);

            AgeLicenciatariosSecuencia ageLicenciatariosSecuencia = _repository.ConsultarPorSecuencia(ageLicenciatariosSecuenciaPKDAO).Result;

            return Task.FromResult(ageLicenciatariosSecuencia);
        }

        public Task<AgeLicenciatariosSecuenciasDAO> ConsultarPorId(
            AgeLicenciatariosSecuenciasPKDAO ageLicenciatariosSecuenciaPKDAO)
        {

            Validator.ValidateObject(ageLicenciatariosSecuenciaPKDAO, new ValidationContext(ageLicenciatariosSecuenciaPKDAO), true);

            AgeLicenciatariosSecuenciasDAO ageLicenciatariosSecuenciaDAO = _repository.ConsultarPorId(ageLicenciatariosSecuenciaPKDAO).Result;

            return ageLicenciatariosSecuenciaDAO == null
                ? throw new RegisterNotFoundException("Secuencia con código: " + ageLicenciatariosSecuenciaPKDAO.codigo + " no existe.")
                : Task.FromResult(ageLicenciatariosSecuenciaDAO);
        }

        public Task<Page<AgeLicenciatariosSecuenciasDAO>> ConsultarListaFiltro(
            int codigoLicenciatario,
            string filtro,
            Pageable pageable)
        {
            if (codigoLicenciatario < 0)
                throw new InvalidFieldException("El código de licenciatario debe ser un número entero mayor a cero.");

            if (string.IsNullOrWhiteSpace(filtro))
                throw new InvalidFieldException("El filtro es requerido.");

            pageable.Validar<AgeLicenciatariosSecuenciasDAO>();

            return _repository.ConsultarListaFiltro(codigoLicenciatario, filtro.ToLower(), pageable);
        }

        public async Task<Resource<AgeLicenciatariosSecuenciasDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgeLicenciatariosSecuenciasSaveDAO ageLicenciatariosSecuenciasSaveDAO)
        {
            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    AgeLicenciatariosSecuencia ageLicenciatariosSecuencia = ValidarInsert(_httpContextAccessor, ageLicenciatariosSecuenciasSaveDAO);

                    AgeLicenciatariosSecuenciasDAO ageLicenciatariosSecuenciaDAO = await _repository.Insertar(ageLicenciatariosSecuencia);

                    Resource<AgeLicenciatariosSecuenciasDAO> ageLicenciatariosSecuenciaDAOWithReource = GetDataWithResource(_httpContextAccessor, ageLicenciatariosSecuenciaDAO);

                    transactionScope.Complete();

                    return ageLicenciatariosSecuenciaDAOWithReource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public async Task<List<Resource<AgeLicenciatariosSecuenciasDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeLicenciatariosSecuenciasSaveDAO> ageLicenciatariosSecuenciasSaveDAOList)
        {
            if (ageLicenciatariosSecuenciasSaveDAOList == null)
                throw new InvalidSintaxisException();

            // Se valida las PK duplicadas porque no hay secuencia
            ValidateKeys.ValidarPKDuplicadas(ageLicenciatariosSecuenciasSaveDAOList, p => new
            {
                p.Id.ageLicencCodigo,
                p.Id.codigo
            }, "Id");

            List<AgeLicenciatariosSecuencia> ageLicenciatariosSecuenciasList = new List<AgeLicenciatariosSecuencia>();

            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    foreach (AgeLicenciatariosSecuenciasSaveDAO ageLicenciatariosSecuenciasSaveDAO in ageLicenciatariosSecuenciasSaveDAOList)
                    {
                        AgeLicenciatariosSecuencia ageLicenciatariosSecuencia = ValidarInsert(_httpContextAccessor, ageLicenciatariosSecuenciasSaveDAO);
                        ageLicenciatariosSecuenciasList.Add(ageLicenciatariosSecuencia);
                    }

                    List<AgeLicenciatariosSecuenciasDAO> ageLicenciatariosSecuenciaDAOList = await _repository.InsertarVarios(ageLicenciatariosSecuenciasList);

                    List<Resource<AgeLicenciatariosSecuenciasDAO>> agePerfilesDAOListWithResource = new List<Resource<AgeLicenciatariosSecuenciasDAO>>();

                    foreach (AgeLicenciatariosSecuenciasDAO ageLicenciatariosSecuenciaDAO in ageLicenciatariosSecuenciaDAOList)
                    {
                        agePerfilesDAOListWithResource.Add(GetDataWithResource(_httpContextAccessor, ageLicenciatariosSecuenciaDAO));
                    }

                    transactionScope.Complete();

                    return agePerfilesDAOListWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public Task<AgeLicenciatariosSecuenciasDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeLicenciatariosSecuenciasSaveDAO ageLicenciatariosSecuenciasSaveDAO)
        {
            AgeLicenciatariosSecuencia ageLicenciatariosSecuencia = ValidarUpdate(_httpContextAccessor, ageLicenciatariosSecuenciasSaveDAO);

            return _repository.Actualizar(ageLicenciatariosSecuencia);
        }

        public Task<List<AgeLicenciatariosSecuenciasDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeLicenciatariosSecuenciasSaveDAO> ageLicenciatariosSecuenciasSaveDAOList)
        {
            if (ageLicenciatariosSecuenciasSaveDAOList == null)
                throw new InvalidSintaxisException();

            ValidateKeys.ValidarPKDuplicadas(ageLicenciatariosSecuenciasSaveDAOList, p => new
            {
                p.Id.ageLicencCodigo,
                p.Id.codigo
            }, "Id");

            List<AgeLicenciatariosSecuencia> ageLicenciatariosSecuenciasList = new List<AgeLicenciatariosSecuencia>();
            AgeLicenciatariosSecuencia ageLicenciatariosSecuencia;

            foreach (AgeLicenciatariosSecuenciasSaveDAO ageLicenciatariosSecuenciasSaveDAO in ageLicenciatariosSecuenciasSaveDAOList)
            {
                ageLicenciatariosSecuencia = ValidarUpdate(_httpContextAccessor, ageLicenciatariosSecuenciasSaveDAO);
                ageLicenciatariosSecuenciasList.Add(ageLicenciatariosSecuencia);
            }

            return _repository.ActualizarVarios(ageLicenciatariosSecuenciasList);
        }

        public void ActualizarSecuencia(
            IHttpContextAccessor _httpContextAccessor,
            AgeLicenciatariosSecuencia ageLicenciatariosSecuencia)
        {
            ageLicenciatariosSecuencia.FechaModificacion = DateTime.Now;
            ageLicenciatariosSecuencia.UbicacionModificacion = Ubicacion.getIpAdress(_httpContextAccessor);

            Validator.ValidateObject(ageLicenciatariosSecuencia, new ValidationContext(ageLicenciatariosSecuencia), true);

            _repository.ActualizarSecuencia(ageLicenciatariosSecuencia);
        }

        private static Resource<AgeLicenciatariosSecuenciasDAO> GetDataWithResource(
            IHttpContextAccessor _httpContextAccessor,
            AgeLicenciatariosSecuenciasDAO ageLicenciatariosSecuenciasDAO)
        {
            string rutaSegmentoFinal = $"{ageLicenciatariosSecuenciasDAO.Id.ageLicencCodigo}/{ageLicenciatariosSecuenciasDAO.Id.codigo}";

            return Resource<AgeLicenciatariosSecuenciasDAO>.GetDataWithResource<AgeLicenciatariosSecuenciasController>(
                _httpContextAccessor, rutaSegmentoFinal, ageLicenciatariosSecuenciasDAO);
        }

        private AgeLicenciatariosSecuencia ValidarInsert(
            IHttpContextAccessor _httpContextAccessor,
            AgeLicenciatariosSecuenciasSaveDAO ageLicenciatariosSecuenciasSaveDAO)
        {
            if (ageLicenciatariosSecuenciasSaveDAO == null)
                throw new InvalidSintaxisException();

            ValidarKeys(ageLicenciatariosSecuenciasSaveDAO.Id);

            AgeLicenciatariosSecuencia entityObject = FromDAOToEntity(_httpContextAccessor, ageLicenciatariosSecuenciasSaveDAO);

            Validator.ValidateObject(entityObject, new ValidationContext(entityObject), true);

            return entityObject;
        }

        private AgeLicenciatariosSecuencia ValidarUpdate(
            IHttpContextAccessor _httpContextAccessor,
            AgeLicenciatariosSecuenciasSaveDAO ageLicenciatariosSecuenciasSaveDAO)
        {
            if (ageLicenciatariosSecuenciasSaveDAO == null)
                throw new InvalidSintaxisException();

            if (ageLicenciatariosSecuenciasSaveDAO.UsuarioModificacion == null ||
                ageLicenciatariosSecuenciasSaveDAO.UsuarioModificacion <= 0)
                throw new InvalidFieldException("El usuario de modificación debe ser un número entero mayor a cero.");

            AgeLicenciatariosSecuencia ageLicenciatariosSecuencia = ConsultarPorSecuencia(ageLicenciatariosSecuenciasSaveDAO.Id).Result
                ?? throw new RegisterNotFoundException($"Secuencia con código: {ageLicenciatariosSecuenciasSaveDAO.Id.codigo}" +
                $", código licenciatario: {ageLicenciatariosSecuenciasSaveDAO.Id.ageLicencCodigo} no existe.");

            if (ageLicenciatariosSecuenciasSaveDAO.ValorActual > 0 && ageLicenciatariosSecuenciasSaveDAO.ValorActual != null)
                ageLicenciatariosSecuencia.ValorActual = (long)ageLicenciatariosSecuenciasSaveDAO.ValorActual;

            if (ageLicenciatariosSecuenciasSaveDAO.ValorInicial > 0)
                ageLicenciatariosSecuencia.ValorInicial = (long)ageLicenciatariosSecuenciasSaveDAO.ValorInicial;

            if (!string.IsNullOrWhiteSpace(ageLicenciatariosSecuenciasSaveDAO.Estado))
            {
                ageLicenciatariosSecuencia.Estado = ageLicenciatariosSecuenciasSaveDAO.Estado;
                ageLicenciatariosSecuencia.FechaEstado = DateTime.Now;
            }

            if (!string.IsNullOrWhiteSpace(ageLicenciatariosSecuenciasSaveDAO.Ciclica))
                ageLicenciatariosSecuencia.Ciclica = ageLicenciatariosSecuenciasSaveDAO.Ciclica;

            if (!string.IsNullOrWhiteSpace(ageLicenciatariosSecuenciasSaveDAO.Descripcion))
                ageLicenciatariosSecuencia.Descripcion = ageLicenciatariosSecuenciasSaveDAO.Descripcion;

            if (!string.IsNullOrWhiteSpace(ageLicenciatariosSecuenciasSaveDAO.ObservacionEstado))
                ageLicenciatariosSecuencia.ObservacionEstado = ageLicenciatariosSecuenciasSaveDAO.ObservacionEstado;

            ageLicenciatariosSecuencia.FechaModificacion = DateTime.Now;
            ageLicenciatariosSecuencia.UbicacionModificacion = Ubicacion.getIpAdress(_httpContextAccessor);
            ageLicenciatariosSecuencia.UsuarioModificacion = ageLicenciatariosSecuenciasSaveDAO.UsuarioModificacion;

            Validator.ValidateObject(ageLicenciatariosSecuencia, new ValidationContext(ageLicenciatariosSecuencia), true);

            return ageLicenciatariosSecuencia;
        }

        private AgeLicenciatariosSecuencia FromDAOToEntity(
           IHttpContextAccessor _httpContextAccessor,
           AgeLicenciatariosSecuenciasSaveDAO ageLicenciatariosSecuenciasSaveDAO)
        {

            return new AgeLicenciatariosSecuencia
            {
                AgeLicencCodigo = ageLicenciatariosSecuenciasSaveDAO.Id.ageLicencCodigo,
                Codigo = ageLicenciatariosSecuenciasSaveDAO.Id.codigo,
                Descripcion = ageLicenciatariosSecuenciasSaveDAO.Descripcion,
                ValorInicial = ageLicenciatariosSecuenciasSaveDAO.ValorInicial,
                ValorActual = ageLicenciatariosSecuenciasSaveDAO.ValorActual,
                IncrementaEn = ageLicenciatariosSecuenciasSaveDAO.IncrementaEn,
                Ciclica = ageLicenciatariosSecuenciasSaveDAO.Ciclica,
                Estado = ageLicenciatariosSecuenciasSaveDAO.Estado,
                FechaEstado = DateTime.Now,
                FechaIngreso = DateTime.Now,
                ObservacionEstado = ageLicenciatariosSecuenciasSaveDAO.ObservacionEstado,
                UbicacionIngreso = Ubicacion.getIpAdress(_httpContextAccessor),
                UsuarioIngreso = ageLicenciatariosSecuenciasSaveDAO.UsuarioIngreso
            };
        }

        private void ValidarKeys(AgeLicenciatariosSecuenciasPKDAO ageLicenciatariosSecuenciaPKDAO)
        {
            ValidarPK(ageLicenciatariosSecuenciaPKDAO);
            ValidarFKLicenciatario(ageLicenciatariosSecuenciaPKDAO.ageLicencCodigo);
        }

        private void ValidarPK(AgeLicenciatariosSecuenciasPKDAO ageLicenciatariosSecuenciaPKDAO)
        {
            ValidateKeys.ValidarNoExistenciaKey(ageLicenciatariosSecuenciaPKDAO,
                $"Secuencia con código: {ageLicenciatariosSecuenciaPKDAO.codigo}, " +
                $"código licenciatario: {ageLicenciatariosSecuenciaPKDAO.ageLicencCodigo} ya existe.",
                _repository.ConsultarPorId);
        }

        private void ValidarFKLicenciatario(int codigoLicenciatario)
        {
            ValidateKeys.ValidarExistenciaKey(codigoLicenciatario,
                $"Licenciatario con código: {codigoLicenciatario} no existe."
                , _repositoryLicenciatario.ConsultarCompletePorId);
        }
    }
}
