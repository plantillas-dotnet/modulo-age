﻿using AGE.Entities;
using AGE.Entities.DAO.AgeLicenciatariosSecuencias;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AGE.Services.AgeLicenciatariosSecuenciaService
{
    public interface IAgeLicenciatariosSecuenciasService
    {
        Task<Page<AgeLicenciatariosSecuenciasDAO>> ConsultarTodos(
            int codigoLicenciatario,
            int codigo, 
            string ciclica,
            string descripcion,
            Pageable pageable);
        Task<AgeLicenciatariosSecuencia> ConsultarPorSecuencia(
            AgeLicenciatariosSecuenciasPKDAO ageLicenciatariosSecuenciaPKDAO);
        Task<AgeLicenciatariosSecuenciasDAO> ConsultarPorId(
            AgeLicenciatariosSecuenciasPKDAO ageLicenciatariosSecuenciaPKDAO);
        Task<Page<AgeLicenciatariosSecuenciasDAO>> ConsultarListaFiltro(
            int codigoLicenciatario, 
            string filtro, 
            Pageable pageable);
        Task<Resource<AgeLicenciatariosSecuenciasDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor, 
            AgeLicenciatariosSecuenciasSaveDAO ageLicenciatariosSecuenciasSaveDAO);
        Task<List<Resource<AgeLicenciatariosSecuenciasDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor, 
            List<AgeLicenciatariosSecuenciasSaveDAO> ageLicenciatariosSecuenciasSaveDAOList);
        Task<AgeLicenciatariosSecuenciasDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor, 
            AgeLicenciatariosSecuenciasSaveDAO ageLicenciatariosSecuenciasSaveDAO);
        Task<List<AgeLicenciatariosSecuenciasDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor, 
            List<AgeLicenciatariosSecuenciasSaveDAO> ageLicenciatariosSecuenciasSaveDAOList);
        void ActualizarSecuencia(
            IHttpContextAccessor _httpContextAccessor, 
            AgeLicenciatariosSecuencia ageLicenciatariosSecuencia);
    }
}
