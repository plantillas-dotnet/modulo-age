﻿using AGE.Controllers;
using AGE.Entities;
using AGE.Entities.DAO.AgeLicenciatarioAplicacion;
using AGE.Entities.DAO.AgeRutas;
using AGE.Middleware.Exceptions.BadRequest;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Repositories.AgeLicenciatariosAplicacionRepository;
using AGE.Repositories.AgeRutasRepository;
using AGE.Utils;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using System.ComponentModel.DataAnnotations;
using System.Transactions;
using AGE.Repositories.AgeLicenciatariosRepository;
using AGE.Repositories.AgeAplicacionesRepository;

namespace AGE.Services.AgeLicenciatariosAplicacionService
{
    public class AgeLicenciatarioAplicacionService : IAgeLicenciatarioAplicacionService
    {

        private readonly IAgeLicenciatarioAplicacionRepository _repository;

        private readonly IAgeRutasRepository _repositoryRutas;
        private readonly IAgeLicenciatariosRepository _repositoryLicenciatario;
        private readonly IAgeAplicacionesRepository _repositoryAplicacion;


        public AgeLicenciatarioAplicacionService(
            IAgeLicenciatarioAplicacionRepository Repository,
            IAgeRutasRepository repositoryRutas,
            IAgeLicenciatariosRepository repositoryLicenciatario,
            IAgeAplicacionesRepository repositoryAplicacion)
        {
            _repository = Repository;
            _repositoryRutas = repositoryRutas;
            _repositoryLicenciatario = repositoryLicenciatario;
            _repositoryAplicacion = repositoryAplicacion;
        }

        
        public Task<Page<AgeLicenciatariosAplicacionesDAO>> ConsultarTodos(
            int codigoLicenciatario,
            int ageAplicaCodigo,
            int ageRutaCodigo,
            Pageable pageable)
        {
            if (codigoLicenciatario <= 0)
            {
                throw new InvalidFieldException("El código de licenciatario debe ser un número entero mayor a cero.");
            }
            pageable.Validar<AgeLicenciatariosAplicacionesDAO>();

            return _repository.ConsultarTodos(codigoLicenciatario, ageAplicaCodigo, ageRutaCodigo, pageable);
        }


        public Task<Page<AgeLicenciatariosAplicacionesDAO>> ConsultarListaFiltro(
            int codigoLicenciatario,
            string filtro,
            Pageable pageable)
        {
            if (codigoLicenciatario <= 0)
            {
                throw new InvalidFieldException("El código de licenciatario debe ser un número entero mayor a cero.");
            }

            if (string.IsNullOrWhiteSpace(filtro))
            {
                throw new InvalidFieldException("El filtro es requerido.");
            }

            pageable.Validar<AgeLicenciatariosAplicacionesDAO>();

            return _repository.ConsultarListaFiltro(codigoLicenciatario, filtro.ToLower(), pageable);
        }


        public Task<AgeLicenciatariosAplicacionesDAO> ConsultarPorId(
            AgeLicenciatariosAplicacionesPKDAO ageLicenciatariosAplicacionPKDAO)
        {
            Validator.ValidateObject(ageLicenciatariosAplicacionPKDAO, new ValidationContext(ageLicenciatariosAplicacionPKDAO), true);

            AgeLicenciatariosAplicacionesDAO ageLicenciatariosAplicacionDAO = _repository.ConsultarPorId(ageLicenciatariosAplicacionPKDAO).Result;

            if (ageLicenciatariosAplicacionDAO == null)
            {
                throw new RegisterNotFoundException($"Licenciatario con código {ageLicenciatariosAplicacionPKDAO.AgeLicencCodigo} y Aplicacion con código {ageLicenciatariosAplicacionPKDAO.AgeAplicaCodigo} no existe.");
            }

            return Task.FromResult(ageLicenciatariosAplicacionDAO);
        }


        private async Task<AgeLicenciatariosAplicaciones> ConsultarCompletePorId(
            AgeLicenciatariosAplicacionesPKDAO ageLicenciatariosAplicacionPKDAO)
        {
            Validator.ValidateObject(ageLicenciatariosAplicacionPKDAO, new ValidationContext(ageLicenciatariosAplicacionPKDAO), true);

            AgeLicenciatariosAplicaciones? ageLicenciatariosAplicacion = _repository.ConsultarCompletePorId(ageLicenciatariosAplicacionPKDAO).Result;

            return ageLicenciatariosAplicacion;
        }


        public async Task<Resource<AgeLicenciatariosAplicacionesDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgeLicenciatariosAplicacionesSaveDAO ageLicenciatariosAplicacionSaveDAO)
        {
            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    AgeLicenciatariosAplicaciones ageLicenciatariosAplicacion = ValidarInsert(_httpContextAccessor, ageLicenciatariosAplicacionSaveDAO);

                    AgeLicenciatariosAplicacionesDAO ageLicenciatariosAplicacionDAO = await _repository.Insertar(ageLicenciatariosAplicacion);

                    Resource<AgeLicenciatariosAplicacionesDAO> ageLicenciatariosAplicacionDAOWithResource = GetDataWithResource(_httpContextAccessor, ageLicenciatariosAplicacionDAO);

                    transactionScope.Complete();

                    return ageLicenciatariosAplicacionDAOWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public async Task<List<Resource<AgeLicenciatariosAplicacionesDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeLicenciatariosAplicacionesSaveDAO> ageLicenciatariosAplicacionSaveDAOList)
        {
            if (ageLicenciatariosAplicacionSaveDAOList == null)
            {
                throw new InvalidSintaxisException();
            }

            List<AgeLicenciatariosAplicaciones> ageLicenciatariosAplicacionList = new List<AgeLicenciatariosAplicaciones>();

            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    foreach (AgeLicenciatariosAplicacionesSaveDAO ageLicenciatariosAplicacionSaveDAO in ageLicenciatariosAplicacionSaveDAOList)
                    {
                        AgeLicenciatariosAplicaciones ageLicenciatario = ValidarInsert(_httpContextAccessor, ageLicenciatariosAplicacionSaveDAO);
                        ageLicenciatariosAplicacionList.Add(ageLicenciatario);
                    }

                    List<AgeLicenciatariosAplicacionesDAO> ageLicenciatariosAplicacionDAOList = await _repository.InsertarVarios(ageLicenciatariosAplicacionList);

                    List<Resource<AgeLicenciatariosAplicacionesDAO>> ageLicenciatariosAplicacionDAOListWithResource = new List<Resource<AgeLicenciatariosAplicacionesDAO>>();

                    foreach (AgeLicenciatariosAplicacionesDAO ageLicenciatariosAplicacionDAO in ageLicenciatariosAplicacionDAOList)
                    {
                        ageLicenciatariosAplicacionDAOListWithResource.Add(GetDataWithResource(_httpContextAccessor, ageLicenciatariosAplicacionDAO));
                    }

                    transactionScope.Complete();

                    return ageLicenciatariosAplicacionDAOListWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public Task<AgeLicenciatariosAplicacionesDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeLicenciatariosAplicacionesSaveDAO ageLicenciatariosAplicacionSaveDAO)
        {
            AgeLicenciatariosAplicaciones ageLicenciatariosAplicacion = ValidarUpdate(_httpContextAccessor, ageLicenciatariosAplicacionSaveDAO);

            return _repository.Actualizar(ageLicenciatariosAplicacion);
        }

        public Task<List<AgeLicenciatariosAplicacionesDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeLicenciatariosAplicacionesSaveDAO> ageLicenciatariosAplicacionSaveDAOList)
        {
            if (ageLicenciatariosAplicacionSaveDAOList == null)
                throw new InvalidSintaxisException();

            ValidateKeys.ValidarPKDuplicadas(ageLicenciatariosAplicacionSaveDAOList,
                                             p => new { p.Id.AgeLicencCodigo, p.Id.AgeAplicaCodigo },
                                             "Id");

            List<AgeLicenciatariosAplicaciones> ageLicenciatariosAplicacionList = new List<AgeLicenciatariosAplicaciones>();
            AgeLicenciatariosAplicaciones ageLicenciatariosAplicacion;

            foreach (AgeLicenciatariosAplicacionesSaveDAO ageLicenciatariosAplicacionSaveDAO in ageLicenciatariosAplicacionSaveDAOList)
            {
                ageLicenciatariosAplicacion = ValidarUpdate(_httpContextAccessor, ageLicenciatariosAplicacionSaveDAO);
                ageLicenciatariosAplicacionList.Add(ageLicenciatariosAplicacion);
            }

            return _repository.ActualizarVarios(ageLicenciatariosAplicacionList);
        }

        private static Resource<AgeLicenciatariosAplicacionesDAO> GetDataWithResource(
            IHttpContextAccessor _httpContextAccessor,
            AgeLicenciatariosAplicacionesDAO ageLicenciatariosAplicacionDAO)
        {
            string rutaSegmentoFinal = $"{ageLicenciatariosAplicacionDAO.Id.AgeLicencCodigo}/{ageLicenciatariosAplicacionDAO.Id.AgeAplicaCodigo}";

            return Resource<AgeLicenciatariosAplicacionesDAO>.GetDataWithResource<AgeLicenciatarioAplicacionController>(
                _httpContextAccessor, rutaSegmentoFinal, ageLicenciatariosAplicacionDAO);
        }

        private AgeLicenciatariosAplicaciones ValidarInsert(
        IHttpContextAccessor _httpContextAccessor,
        AgeLicenciatariosAplicacionesSaveDAO ageLicenciatariosAplicacionSaveDAO)
        {
            if (ageLicenciatariosAplicacionSaveDAO == null)
                throw new InvalidSintaxisException();

            Validator.ValidateObject(ageLicenciatariosAplicacionSaveDAO, new ValidationContext(ageLicenciatariosAplicacionSaveDAO), true);

            ValidarKeys(ageLicenciatariosAplicacionSaveDAO);

            AgeLicenciatariosAplicaciones entityObject = FromDAOToEntity(_httpContextAccessor, ageLicenciatariosAplicacionSaveDAO);

            Validator.ValidateObject(entityObject, new ValidationContext(entityObject), true);

            return entityObject;
        }

        private AgeLicenciatariosAplicaciones FromDAOToEntity(
        IHttpContextAccessor _httpContextAccessor,
        AgeLicenciatariosAplicacionesSaveDAO ageLicenciatariosAplicacionSaveDAO)
        {
            return new AgeLicenciatariosAplicaciones
            {
                AgeLicencCodigo = ageLicenciatariosAplicacionSaveDAO.Id.AgeLicencCodigo,
                AgeAplicaCodigo = ageLicenciatariosAplicacionSaveDAO.Id.AgeAplicaCodigo,
                AgeRutaAgeLicencCodigo = ageLicenciatariosAplicacionSaveDAO.AgeRutaAgeLicencCodigo,
                AgeRutaCodigo = ageLicenciatariosAplicacionSaveDAO.AgeRutaCodigo,
                Estado = ageLicenciatariosAplicacionSaveDAO.Estado,
                FechaEstado = DateTime.Now,
                FechaIngreso = DateTime.Now,
                ObservacionEstado = ageLicenciatariosAplicacionSaveDAO.ObservacionEstado,
                UbicacionIngreso = Ubicacion.getIpAdress(_httpContextAccessor),
                UsuarioIngreso = ageLicenciatariosAplicacionSaveDAO.UsuarioIngreso
            };
        }

        private AgeLicenciatariosAplicaciones ValidarUpdate(
            IHttpContextAccessor _httpContextAccessor,
            AgeLicenciatariosAplicacionesSaveDAO ageLicenciatariosAplicacionSaveDAO)
        {
            if (ageLicenciatariosAplicacionSaveDAO == null)
            {
                throw new InvalidSintaxisException();
            }

            if (ageLicenciatariosAplicacionSaveDAO.UsuarioModificacion == null || ageLicenciatariosAplicacionSaveDAO.UsuarioModificacion <= 0)
            {
                throw new InvalidFieldException("El usuario de modificación debe ser un número entero mayor a cero.");
            }

            AgeLicenciatariosAplicaciones? ageLicenciatariosAplicacion = ConsultarCompletePorId(new AgeLicenciatariosAplicacionesPKDAO
            {
                AgeLicencCodigo = ageLicenciatariosAplicacionSaveDAO.Id.AgeLicencCodigo,
                AgeAplicaCodigo = ageLicenciatariosAplicacionSaveDAO.Id.AgeAplicaCodigo
            }).Result;

            if (ageLicenciatariosAplicacion == null)
            {
                throw new RegisterNotFoundException($"Licenciatario con código {ageLicenciatariosAplicacionSaveDAO.Id.AgeLicencCodigo} y Aplicacion con código {ageLicenciatariosAplicacionSaveDAO.Id.AgeAplicaCodigo} no existe.");
            }

            if (!string.IsNullOrWhiteSpace(ageLicenciatariosAplicacionSaveDAO.Estado))
            {
                ageLicenciatariosAplicacion.Estado = ageLicenciatariosAplicacionSaveDAO.Estado;
                ageLicenciatariosAplicacion.FechaEstado = DateTime.Now;
            }

            if (!string.IsNullOrWhiteSpace(ageLicenciatariosAplicacionSaveDAO.AgeRutaAgeLicencCodigo.ToString()) &&
                !string.IsNullOrWhiteSpace(ageLicenciatariosAplicacionSaveDAO.AgeRutaCodigo.ToString()))
                ValidarFKRuta(ageLicenciatariosAplicacionSaveDAO);
            

            if (!string.IsNullOrWhiteSpace(ageLicenciatariosAplicacionSaveDAO.ObservacionEstado))
                ageLicenciatariosAplicacion.ObservacionEstado = ageLicenciatariosAplicacionSaveDAO.ObservacionEstado;

            ageLicenciatariosAplicacion.FechaModificacion = DateTime.Now;
            ageLicenciatariosAplicacion.UbicacionModificacion = Ubicacion.getIpAdress(_httpContextAccessor);
            ageLicenciatariosAplicacion.UsuarioModificacion = ageLicenciatariosAplicacionSaveDAO.UsuarioModificacion;

            Validator.ValidateObject(ageLicenciatariosAplicacion, new ValidationContext(ageLicenciatariosAplicacion), true);

            return ageLicenciatariosAplicacion;
        }

        private void ValidarKeys(AgeLicenciatariosAplicacionesSaveDAO ageLicenciatariosAplicacionSaveDAO)
        {
            ValidarPK(ageLicenciatariosAplicacionSaveDAO.Id);
            ValidarFKLicenciatario(ageLicenciatariosAplicacionSaveDAO.Id);
            ValidarFKAplicacion(ageLicenciatariosAplicacionSaveDAO.Id);

            if (ageLicenciatariosAplicacionSaveDAO.AgeRutaAgeLicencCodigo != null &&
                ageLicenciatariosAplicacionSaveDAO.AgeRutaCodigo != null)
            {
                ValidarFKRuta(ageLicenciatariosAplicacionSaveDAO);
            }
        }

        private void ValidarPK(AgeLicenciatariosAplicacionesPKDAO ageLicenciatariosAplicacionPKDAO)
        {
            ValidateKeys.ValidarNoExistenciaKey(
            ageLicenciatariosAplicacionPKDAO,
            $"Licenciatario con código {ageLicenciatariosAplicacionPKDAO.AgeLicencCodigo}" +
            $" y Aplicacion con código {ageLicenciatariosAplicacionPKDAO.AgeAplicaCodigo} ya existe.",
            _repository.ConsultarPorId);

        }

        private void ValidarFKLicenciatario(AgeLicenciatariosAplicacionesPKDAO ageLicenciatariosAplicacionPKDAO)
        {
            ValidateKeys.ValidarExistenciaKey(
                ageLicenciatariosAplicacionPKDAO.AgeLicencCodigo,
                $"Licenciatario con código {ageLicenciatariosAplicacionPKDAO.AgeLicencCodigo} no existe.",
                _repositoryLicenciatario.ConsultarPorId);
        }

        private void ValidarFKAplicacion(AgeLicenciatariosAplicacionesPKDAO ageLicenciatariosAplicacionPKDAO)
        {
            ValidateKeys.ValidarExistenciaKey(
                ageLicenciatariosAplicacionPKDAO.AgeAplicaCodigo,
                $"Aplicacion con código {ageLicenciatariosAplicacionPKDAO.AgeAplicaCodigo} no existe.",
                _repositoryAplicacion.ConsultarPorId);
        }

        private void ValidarFKRuta(AgeLicenciatariosAplicacionesSaveDAO ageLicenciatariosAplicacionSaveDAO)
        {
            AgeRutasPKDAO ageRutasPKDAO = new AgeRutasPKDAO()
            {
                ageLicencCodigo = ageLicenciatariosAplicacionSaveDAO.AgeRutaAgeLicencCodigo ?? 0,
                codigo = ageLicenciatariosAplicacionSaveDAO.AgeRutaCodigo ?? 0
            };

            ValidateKeys.ValidarExistenciaKey(
                ageRutasPKDAO,
                $"Ruta con licenciatario {ageRutasPKDAO.ageLicencCodigo} y ruta con código {ageRutasPKDAO.codigo} no existe.",
                _repositoryRutas.ConsultarPorId);
        }
    }
}
