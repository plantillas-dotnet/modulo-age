﻿using AGE.Entities.DAO.AgeLicenciatarioAplicacion;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;

namespace AGE.Services.AgeLicenciatariosAplicacionService
{
    public interface IAgeLicenciatarioAplicacionService
    {

        Task<Page<AgeLicenciatariosAplicacionesDAO>> ConsultarTodos(
            int codigoLicenciatario,
            int ageAplicaCodigo,
            int ageRutaCodigo,
            Pageable pageable);

        Task<Page<AgeLicenciatariosAplicacionesDAO>> ConsultarListaFiltro(
            int codigoLicenciatario,
            string filtro,
            Pageable pageable);

        Task<AgeLicenciatariosAplicacionesDAO> ConsultarPorId(
            AgeLicenciatariosAplicacionesPKDAO ageLicenciatariosAplicacionPKDAO);

        Task<Resource<AgeLicenciatariosAplicacionesDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgeLicenciatariosAplicacionesSaveDAO ageLicenciatariosAplicacionSaveDAO);

        Task<AgeLicenciatariosAplicacionesDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeLicenciatariosAplicacionesSaveDAO ageLicenciatariosAplicacionSaveDAO);

        Task<List<Resource<AgeLicenciatariosAplicacionesDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeLicenciatariosAplicacionesSaveDAO> ageLicenciatariosAplicacionSaveDAOList);

        Task<List<AgeLicenciatariosAplicacionesDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeLicenciatariosAplicacionesSaveDAO> ageLicenciatariosAplicacionSaveDAOList);

    }
}
