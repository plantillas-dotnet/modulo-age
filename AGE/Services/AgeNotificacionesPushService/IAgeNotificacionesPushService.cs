﻿using AGE.Entities.DAO.AgeNotificacionesPush;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;

namespace AGE.Services.AgeNotificacionesPushService
{
    public interface IAgeNotificacionesPushService
    {

        Task<Page<AgeNotificacionesPushDAO>> ConsultarTodos(
            int codigoLicenciatario,
            int codigo,
            string mensaje,
            string topico,
            Pageable pageable);

        Task<Page<AgeNotificacionesPushDAO>> ConsultarListaFiltro(
            int codigoLicenciatario,
            string filtro,
            Pageable pageable);

        Task<AgeNotificacionesPushDAO> ConsultarPorId(AgeNotificacionesPushPKDAO ageNotificacionesPushPKDAO);

        Task<Resource<AgeNotificacionesPushDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgeNotificacionesPushSaveDAO ageNotificacionesPushSaveDAO);

        Task<List<Resource<AgeNotificacionesPushDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeNotificacionesPushSaveDAO> agePerfilesSaveDAOList);


    }
}
