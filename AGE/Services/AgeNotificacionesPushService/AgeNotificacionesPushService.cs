﻿using AGE.Controllers;
using AGE.Entities;
using AGE.Entities.DAO.AgeNotificacionesPush;
using AGE.Entities.DAO.AgeUsuarios;
using AGE.Firebase.Models;
using AGE.Firebase.Services.PushNotificationService;
using AGE.Middleware.Exceptions.BadRequest;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Repositories.AgeLicenciatariosRepository;
using AGE.Repositories.AgeNotificacionesPushRepository;
using AGE.Repositories.AgeUsuarioRepository;
using AGE.Services.AgeLicenciatariosAplicaSecuService;
using AGE.Services.AgeUsuarioService;
using AGE.Utils;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using System.ComponentModel.DataAnnotations;
using System.Text.Json;
using System.Transactions;

namespace AGE.Services.AgeNotificacionesPushService
{
    public class AgeNotificacionesPushService : IAgeNotificacionesPushService
    {

        private readonly IAgeNotificacionesPushRepository _repository;
        private readonly IPushNotificationService _service;
        private readonly IAgeUsuariosRepository _repositoryUsuario;
        private readonly IAgeUsuariosService _serviceUsuario;
        private readonly IAgeLicenciatariosRepository _repositoryLicenciatario;
        private readonly IAgeLicenciatariosAplicaSecuService _ageLicenciatariosAplicaSecuService;

        public AgeNotificacionesPushService(
            IAgeNotificacionesPushRepository repository,
            IPushNotificationService service,
            IAgeUsuariosRepository repositoryUsuario,
            IAgeUsuariosService serviceUsuario,
            IAgeLicenciatariosRepository repositoryLicenciatario,
            IAgeLicenciatariosAplicaSecuService ageLicenciatariosAplicaSecuService)
        {
            _repository = repository;
            _service = service;
            _serviceUsuario = serviceUsuario;
            _repositoryUsuario = repositoryUsuario;
            _ageLicenciatariosAplicaSecuService = ageLicenciatariosAplicaSecuService;
            _repositoryLicenciatario = repositoryLicenciatario;
        }


        public Task<Page<AgeNotificacionesPushDAO>> ConsultarTodos(
            int codigoLicenciatario, 
            int codigo, 
            string mensaje, 
            string topico, 
            Pageable pageable)
        {
            if (codigoLicenciatario <= 0)
                throw new InvalidFieldException("El código de licenciatario debe ser un número entero mayor a cero.");

            pageable.Validar<AgeNotificacionesPushDAO>();

            return _repository.ConsultarTodos(codigoLicenciatario, codigo, mensaje, topico, pageable);
        }


        public Task<Page<AgeNotificacionesPushDAO>> ConsultarListaFiltro(
            int codigoLicenciatario, 
            string filtro, 
            Pageable pageable)
        {
            if (codigoLicenciatario == 0)
                throw new InvalidFieldException("El código de licenciatario debe ser un número entero mayor a cero.");

            if (string.IsNullOrWhiteSpace(filtro))
                throw new InvalidFieldException("El filtro es requerido.");

            pageable.Validar<AgeNotificacionesPushDAO>();

            return _repository.ConsultarListaFiltro(codigoLicenciatario, filtro.ToLower(), pageable);
        }


        public Task<AgeNotificacionesPushDAO> ConsultarPorId(
            AgeNotificacionesPushPKDAO ageNotificacionesPushPKDAO)
        {
            Validator.ValidateObject(ageNotificacionesPushPKDAO, new ValidationContext(ageNotificacionesPushPKDAO), true);

            AgeNotificacionesPushDAO ageNotificacionesPushDAO = _repository.ConsultarPorId(ageNotificacionesPushPKDAO).Result;

            if (ageNotificacionesPushDAO == null)
                throw new RegisterNotFoundException("Notificación con código " + ageNotificacionesPushPKDAO.Codigo + " no existe");

            return Task.FromResult(ageNotificacionesPushDAO);
        }


        public async Task<Resource<AgeNotificacionesPushDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor, 
            AgeNotificacionesPushSaveDAO ageNotificacionesPushSaveDAO)
        {
            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    AgeNotificacionesPush ageNotificacionesPush = ValidarInsert(_httpContextAccessor, ageNotificacionesPushSaveDAO);

                    AgeNotificacionesPushDAO ageNotificacionesPushDAO = await _repository.Insertar(ageNotificacionesPush);

                    Resource<AgeNotificacionesPushDAO> agePerfilesDAOWithResource = GetDataWithResource(_httpContextAccessor, ageNotificacionesPushDAO);

                    transactionScope.Complete();

                    return agePerfilesDAOWithResource;
                }
                catch (Exception)
                {
                    throw;
                }

            }
        }


        public async Task<List<Resource<AgeNotificacionesPushDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor, 
            List<AgeNotificacionesPushSaveDAO> ageNotificacionesPushSaveDAOList)
        {
            if (ageNotificacionesPushSaveDAOList == null)
                throw new InvalidSintaxisException();

            List<AgeNotificacionesPush> ageNotificacionesPushList = new List<AgeNotificacionesPush>();

            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    foreach (AgeNotificacionesPushSaveDAO ageNotificacionesPushSaveDAO in ageNotificacionesPushSaveDAOList)
                    {
                        AgeNotificacionesPush agePerfile = ValidarInsert(_httpContextAccessor, ageNotificacionesPushSaveDAO);
                        ageNotificacionesPushList.Add(agePerfile);
                    }

                    List<AgeNotificacionesPushDAO> agePerfilesDAOList = await _repository.InsertarVarios(ageNotificacionesPushList);

                    List<Resource<AgeNotificacionesPushDAO>> agePerfilesDAOListWithResource = new List<Resource<AgeNotificacionesPushDAO>>();

                    foreach (AgeNotificacionesPushDAO ageNotificacionesPushDAO in agePerfilesDAOList)
                    {
                        agePerfilesDAOListWithResource.Add(GetDataWithResource(_httpContextAccessor, ageNotificacionesPushDAO));
                    }

                    transactionScope.Complete();

                    return agePerfilesDAOListWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }



        private static Resource<AgeNotificacionesPushDAO> GetDataWithResource(
            IHttpContextAccessor _httpContextAccessor,
            AgeNotificacionesPushDAO ageNotificacionesPushDAO)
        {
            string rutaSegmentoFinal = $"{ageNotificacionesPushDAO.Id.AgeLicencCodigo}/{ageNotificacionesPushDAO.Id.Codigo}";

            return Resource<AgeNotificacionesPushDAO>.GetDataWithResource<AgeNotificacionesPushController>(
                _httpContextAccessor, rutaSegmentoFinal, ageNotificacionesPushDAO);
        }


        private AgeNotificacionesPush ValidarInsert(
            IHttpContextAccessor _httpContextAccessor,
            AgeNotificacionesPushSaveDAO ageNotificacionesPushSaveDAO)
        {

            if (ageNotificacionesPushSaveDAO == null)
                throw new InvalidSintaxisException();

            Validator.ValidateObject(ageNotificacionesPushSaveDAO, new ValidationContext(ageNotificacionesPushSaveDAO), true);

            PushNotificationRequest pushNotificationRequest = new PushNotificationRequest
            {
                Title = ageNotificacionesPushSaveDAO.Titulo,
                Message = ageNotificacionesPushSaveDAO.Mensaje,
                Imagen = !string.IsNullOrWhiteSpace(ageNotificacionesPushSaveDAO.Imagen) ? ageNotificacionesPushSaveDAO.Imagen : ""
            };

            JsonDocument? jsonDocument = null;

            if (!string.IsNullOrWhiteSpace(ageNotificacionesPushSaveDAO.Data))
            {
                try
                {
                    jsonDocument = JsonDocument.Parse(ageNotificacionesPushSaveDAO.Data);
                }
                catch (JsonException)
                {
                    throw new InvalidFieldException("El formato del campo Data no es válido.");
                }
            }

            if (ageNotificacionesPushSaveDAO.AgeUsuariAgeLicencCodigo != null &&
                ageNotificacionesPushSaveDAO.AgeUsuariCodigo != null)
            {
                AgeUsuarioDAO ageUsuarioDAO = _serviceUsuario.ConsultarPorId(new AgeUsuarioPKDAO
                {
                    ageLicencCodigo = ageNotificacionesPushSaveDAO.AgeUsuariAgeLicencCodigo ?? 0,
                    codigo = ageNotificacionesPushSaveDAO.AgeUsuariCodigo ?? 0
                }).Result;
                if (string.IsNullOrWhiteSpace(ageUsuarioDAO?.TokenFirebase))
                    throw new PushNotificationException("Usuario no tiene configurado notificaciones");

                pushNotificationRequest.Token = ageUsuarioDAO.TokenFirebase;
                ageNotificacionesPushSaveDAO.IdFirebaseUser = ageUsuarioDAO.TokenFirebase;

                EnviarNotificacion(jsonDocument, pushNotificationRequest);

            }
            else
            {
                if (string.IsNullOrWhiteSpace(ageNotificacionesPushSaveDAO.Topico))
                    throw new PushNotificationException("La notificación debe ser enviada a un usuario o a un topico ");

                pushNotificationRequest.Topic = ageNotificacionesPushSaveDAO.Topico;

                EnviarNotificacion(jsonDocument, pushNotificationRequest);

            }

            FuncionesSecuencias fg = new FuncionesSecuencias(_ageLicenciatariosAplicaSecuService);

            ageNotificacionesPushSaveDAO.Id.Codigo = fg.ObtenerSecuencia(_httpContextAccessor,
                                                                ageNotificacionesPushSaveDAO.Id.AgeLicencCodigo,
                                                                Globales.CODIGO_SECUENCIA_NOTIFICACIONES_PUSH,
                                                                ageNotificacionesPushSaveDAO.UsuarioIngreso);

            ValidarKeys(ageNotificacionesPushSaveDAO);

            AgeNotificacionesPush entityObject = FromDAOToEntity(_httpContextAccessor, ageNotificacionesPushSaveDAO);

            Validator.ValidateObject(entityObject, new ValidationContext(entityObject), true);

            return entityObject;

        }


        private void EnviarNotificacion(
            JsonDocument jsonDocument,
            PushNotificationRequest pushNotificationRequest)
        {

            if (jsonDocument != null)
            {
                Dictionary<string, string> pushData = new Dictionary<string, string>();
                JsonElement rootElement = jsonDocument.RootElement;

                pushData.Add("notification_foreground", "true");

                foreach (JsonProperty property in rootElement.EnumerateObject())
                {
                    pushData.Add(property.Name, property.Value.ToString());
                }

                _service.SendPushNotificationToken(pushNotificationRequest, pushData);
            }
            else
            {
                _service.SendPushNotificationToToken(pushNotificationRequest);
            }
        }


        private void ValidarKeys(AgeNotificacionesPushSaveDAO ageNotificacionesPushSaveDAO)
        {
            ValidarPK(ageNotificacionesPushSaveDAO.Id);

            ValidarFKLicenciatario(ageNotificacionesPushSaveDAO.Id.AgeLicencCodigo);

            ValidarFKUsuario(ageNotificacionesPushSaveDAO.AgeUsuariAgeLicencCodigo ?? 0,
                ageNotificacionesPushSaveDAO.AgeUsuariCodigo ?? 0);

        }

        private void ValidarPK(AgeNotificacionesPushPKDAO ageNotificacionesPushPKDAO)
        {

            ValidateKeys.ValidarNoExistenciaKey(
               ageNotificacionesPushPKDAO,
               $"Notificación con código {ageNotificacionesPushPKDAO.Codigo} ya existe.",
               _repository.ConsultarPorId);
        }


        private void ValidarFKLicenciatario(int codigoLicenciatario)
        {

            ValidateKeys.ValidarExistenciaKey(
                codigoLicenciatario,
                $"Licenciatario con código {codigoLicenciatario} no existe.",
                _repositoryLicenciatario.ConsultarCompletePorId);
        }


        private void ValidarFKUsuario(int codigoLicenciatario, long codigo)
        {
            AgeUsuarioPKDAO ageUsuarioPKDAO = new AgeUsuarioPKDAO
            {
                ageLicencCodigo = codigoLicenciatario,
                codigo = codigo
            };

            ValidateKeys.ValidarExistenciaKey(
                ageUsuarioPKDAO,
                $"Usuario con código {ageUsuarioPKDAO.codigo} no existe.",
                _repositoryUsuario.ConsultarCompletePorId);
        }

        private AgeNotificacionesPush FromDAOToEntity(
            IHttpContextAccessor _httpContextAccessor,
            AgeNotificacionesPushSaveDAO ageNotificacionesPushSaveDAO)
        {
            return new AgeNotificacionesPush
            {
                AgeLicencCodigo = ageNotificacionesPushSaveDAO.Id.AgeLicencCodigo,
                Codigo = ageNotificacionesPushSaveDAO.Id.Codigo,
                Mensaje = ageNotificacionesPushSaveDAO.Mensaje,
                Topico = ageNotificacionesPushSaveDAO.Topico,
                Titulo = ageNotificacionesPushSaveDAO.Titulo,
                Imagen = ageNotificacionesPushSaveDAO.Imagen,
                IdFirebaseUser = ageNotificacionesPushSaveDAO.IdFirebaseUser,
                Data = ageNotificacionesPushSaveDAO.Data,
                AgeUsuariAgeLicencCodigo = ageNotificacionesPushSaveDAO.AgeUsuariAgeLicencCodigo,
                AgeUsuariCodigo = ageNotificacionesPushSaveDAO.AgeUsuariCodigo,
                Estado = ageNotificacionesPushSaveDAO.Estado,
                FechaEstado = DateTime.Now,
                FechaIngreso = DateTime.Now,
                ObservacionEstado = ageNotificacionesPushSaveDAO.ObservacionEstado,
                UbicacionIngreso = Ubicacion.getIpAdress(_httpContextAccessor),
                UsuarioIngreso = ageNotificacionesPushSaveDAO.UsuarioIngreso
            };
        }



    }
}
