﻿using AGE.Controllers;
using AGE.Entities;
using AGE.Entities.DAO.AgeArchivosMultimedia;
using AGE.Middleware.Exceptions.BadRequest;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Repositories.AgeArchivosMultimediaRepository;
using AGE.Services.AgeLicenciatariosAplicaSecuService;
using AGE.Utils;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using System.ComponentModel.DataAnnotations;
using System.Transactions;

namespace AGE.Services.AgeArchivosMultimediaService
{
    public class AgeArchivosMultimediaService : IAgeArchivosMultimediaService
    {
        private readonly IAgeArchivosMultimediaRepository _repository;

        private readonly IAgeLicenciatariosAplicaSecuService _ageLicenciatariosAplicaSecuService;

        public AgeArchivosMultimediaService(
            IAgeArchivosMultimediaRepository Repository,
            IAgeLicenciatariosAplicaSecuService ageLicenciatariosAplicaSecuService) 
        {
            _repository = Repository;
            _ageLicenciatariosAplicaSecuService = ageLicenciatariosAplicaSecuService;
        }

        public Task<Page<AgeArchivosMultimediaDAO>> ConsultarTodos(int codigoLicenciatario, int codigo, string ruta, string descripcion, string tipo, Pageable pageable)
        {
            if (codigoLicenciatario <= 0)
                throw new InvalidFieldException("El código de licenciatario debe ser un número entero mayor a cero.");

            pageable.Validar<AgeArchivosMultimediaDAO>();

            return _repository.ConsultarTodos(codigoLicenciatario, codigo, ruta, descripcion, tipo, pageable);
        }

        public Task<Page<AgeArchivosMultimediaDAO>> ConsultarLicenciatario(
            int codigoLicenciatario,
            int? codigo, 
            string? ruta, 
            string? descripcion, 
            string? tipo, 
            Pageable pageable)
        {
            if (codigoLicenciatario <= 0)
                throw new InvalidFieldException("El código de licenciatario debe ser un número entero mayor a cero.");
            pageable.Validar<AgeArchivosMultimediaDAO>();

            return _repository.ConsultarTodos(codigoLicenciatario, codigo ?? 0, ruta ?? "", descripcion ?? "", tipo ?? "", pageable);
        }

        public Task<AgeArchivosMultimediaDAO> ConsultarPorId(
            AgeArchivosMultimediaPKDAO ageArchivosMultimediaPKDAO)
        {
            Validator.ValidateObject(ageArchivosMultimediaPKDAO,
                new ValidationContext(ageArchivosMultimediaPKDAO), true);

            AgeArchivosMultimediaDAO ageArchivosMultimediaDAO = _repository.ConsultarPorId(ageArchivosMultimediaPKDAO).Result;

            return ageArchivosMultimediaDAO == null
                ? throw new RegisterNotFoundException("Archivo multimedia con código: " + ageArchivosMultimediaPKDAO.codigo + " no existe.")
                : Task.FromResult(ageArchivosMultimediaDAO);
        }

        private AgeArchivosMultimedia ConsultarCompletePorId(
            AgeArchivosMultimediaPKDAO ageArchivosMultimediaPKDAO)
        {
            Validator.ValidateObject(ageArchivosMultimediaPKDAO,
                new ValidationContext(ageArchivosMultimediaPKDAO), true);

            AgeArchivosMultimedia? ageArchivosMultimedium = _repository.ConsultarCompletePorId(ageArchivosMultimediaPKDAO).Result;

            return ageArchivosMultimedium;
        }

        public Task<Page<AgeArchivosMultimediaDAO>> ConsultarListaFiltro(
        int codigoLicenciatario,
        string filtro,
        Pageable pageable)
        {
            if (codigoLicenciatario <= 0)
                throw new InvalidFieldException("El código de licenciatario debe ser un número entero mayor a cero.");

            if (string.IsNullOrWhiteSpace(filtro))
                throw new InvalidFieldException("El filtro es requerido.");

            pageable.Validar<AgeArchivosMultimediaDAO>();

            return _repository.ConsultarListaFiltro(codigoLicenciatario, filtro.ToLower(), pageable);
        }


        public async Task<Resource<AgeArchivosMultimediaDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgeArchivosMultimediaSaveDAO ageArchivosMultimediaSaveDAO)
        {
            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    AgeArchivosMultimedia ageArchivosMultimedium = ValidarInsert(_httpContextAccessor, ageArchivosMultimediaSaveDAO);

                    AgeArchivosMultimediaDAO ageArchivosMultimediaDAO = await _repository.Insertar(ageArchivosMultimedium);

                    Resource<AgeArchivosMultimediaDAO> ageArchivosMultimediaDAOWithResource = GetDataWithResource(_httpContextAccessor, ageArchivosMultimediaDAO);

                    transactionScope.Complete();

                    return ageArchivosMultimediaDAOWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public async Task<List<Resource<AgeArchivosMultimediaDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeArchivosMultimediaSaveDAO> ageArchivosMultimediaSaveDAOList)
        {
            if (ageArchivosMultimediaSaveDAOList == null)
                throw new InvalidSintaxisException();

            List<AgeArchivosMultimedia> ageArchivosMultimediaList = new List<AgeArchivosMultimedia>();

            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    foreach (AgeArchivosMultimediaSaveDAO ageArchivosMultimediaSaveDAO in ageArchivosMultimediaSaveDAOList)
                    {
                        AgeArchivosMultimedia ageArchivoMultimedia = ValidarInsert(_httpContextAccessor, ageArchivosMultimediaSaveDAO);
                        ageArchivosMultimediaList.Add(ageArchivoMultimedia);
                    }

                    List<AgeArchivosMultimediaDAO> ageArchivosMultimediaDAOList = await _repository.InsertarVarios(ageArchivosMultimediaList);

                    List<Resource<AgeArchivosMultimediaDAO>> ageArchivosMultimediaDAOListWithResource = new List<Resource<AgeArchivosMultimediaDAO>>();

                    foreach (AgeArchivosMultimediaDAO ageArchivosMultimediaDAO in ageArchivosMultimediaDAOList)
                    {
                        ageArchivosMultimediaDAOListWithResource.Add(GetDataWithResource(_httpContextAccessor, ageArchivosMultimediaDAO));
                    }

                    transactionScope.Complete();

                    return ageArchivosMultimediaDAOListWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public Task<AgeArchivosMultimediaDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeArchivosMultimediaSaveDAO ageArchivosMultimediaSaveDAO)
        {
            AgeArchivosMultimedia ageArchivosMultimedia = ValidarUpdate(_httpContextAccessor, ageArchivosMultimediaSaveDAO);

            return _repository.Actualizar(ageArchivosMultimedia);
        }

        public Task<List<AgeArchivosMultimediaDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeArchivosMultimediaSaveDAO> ageArchivosMultimediaSaveDAOList)
        {
            if (ageArchivosMultimediaSaveDAOList == null)
                throw new InvalidSintaxisException();

            ValidateKeys.ValidarPKDuplicadas(ageArchivosMultimediaSaveDAOList, p => new
            {
                p.Id.ageLicencCodigo,
                p.Id.codigo
            }, "Id");

            List<AgeArchivosMultimedia> ageArchivosMultimediaList = new List<AgeArchivosMultimedia>();
            AgeArchivosMultimedia ageArchivoMultimedia;

            foreach (AgeArchivosMultimediaSaveDAO ageArchivosMultimediaSaveDAO in ageArchivosMultimediaSaveDAOList)
            {
                ageArchivoMultimedia = ValidarUpdate(_httpContextAccessor, ageArchivosMultimediaSaveDAO);
                ageArchivosMultimediaList.Add(ageArchivoMultimedia);
            }

            return _repository.ActualizarVarios(ageArchivosMultimediaList);
        }

        private static Resource<AgeArchivosMultimediaDAO> GetDataWithResource(
            IHttpContextAccessor _httpContextAccessor,
            AgeArchivosMultimediaDAO ageArchivosMultimediaDAO)
        {
            string rutaSegmentoFinal = $"{ageArchivosMultimediaDAO.Id.ageLicencCodigo}/{ageArchivosMultimediaDAO.Id.codigo}";

            return Resource<AgeArchivosMultimediaDAO>.GetDataWithResource<AgeArchivosMultimediaController>(
                _httpContextAccessor, rutaSegmentoFinal, ageArchivosMultimediaDAO);
        }

        private AgeArchivosMultimedia ValidarInsert(
        IHttpContextAccessor _httpContextAccessor,
        AgeArchivosMultimediaSaveDAO ageArchivosMultimediaSaveDAO)
        {
            if (ageArchivosMultimediaSaveDAO == null)
                throw new InvalidSintaxisException();

            Validator.ValidateObject(ageArchivosMultimediaSaveDAO, new ValidationContext(ageArchivosMultimediaSaveDAO), true);

            FuncionesSecuencias fg = new FuncionesSecuencias(_ageLicenciatariosAplicaSecuService);

            ageArchivosMultimediaSaveDAO.Id.codigo = fg.ObtenerSecuencia(_httpContextAccessor,
                ageArchivosMultimediaSaveDAO.Id.ageLicencCodigo,
                Globales.CODIGO_SECUENCIA_ARCHIVOS_MULTIMEDIA,
                ageArchivosMultimediaSaveDAO.UsuarioIngreso);

            ValidarPK(ageArchivosMultimediaSaveDAO.Id);

            AgeArchivosMultimedia entityObject = FromDAOToEntity(_httpContextAccessor,ageArchivosMultimediaSaveDAO);

            Validator.ValidateObject(entityObject, new ValidationContext(entityObject), true);

            return entityObject;
        }

        private AgeArchivosMultimedia ValidarUpdate(
        IHttpContextAccessor _httpContextAccessor,
        AgeArchivosMultimediaSaveDAO ageArchivosMultimediaSaveDAO)
        {
            if (ageArchivosMultimediaSaveDAO == null)
                throw new InvalidSintaxisException();

            if (ageArchivosMultimediaSaveDAO.UsuarioModificacion == null || ageArchivosMultimediaSaveDAO.UsuarioModificacion <= 0)
                throw new InvalidFieldException("El usuario de modificación debe ser un número entero mayor a cero.");

            AgeArchivosMultimedia? ageArchivosMultimedia = ConsultarCompletePorId(ageArchivosMultimediaSaveDAO.Id) 
                ?? throw new RegisterNotFoundException("Archivo multimedia con código: " + ageArchivosMultimediaSaveDAO.Id.codigo + " no existe.");
            
            if (!string.IsNullOrWhiteSpace(ageArchivosMultimediaSaveDAO.Estado))
            {
                ageArchivosMultimedia.Estado = ageArchivosMultimediaSaveDAO.Estado;
                ageArchivosMultimedia.FechaEstado = DateTime.Now;
            }

            if (!string.IsNullOrWhiteSpace(ageArchivosMultimediaSaveDAO.Ruta))
                ageArchivosMultimedia.Ruta = ageArchivosMultimediaSaveDAO.Ruta;

            if (!string.IsNullOrWhiteSpace(ageArchivosMultimediaSaveDAO.Descripcion))
                ageArchivosMultimedia.Descripcion = ageArchivosMultimediaSaveDAO.Descripcion;

            if (!string.IsNullOrWhiteSpace(ageArchivosMultimediaSaveDAO.ObservacionEstado))
                ageArchivosMultimedia.ObservacionEstado = ageArchivosMultimediaSaveDAO.ObservacionEstado;

            ageArchivosMultimedia.FechaModificacion = DateTime.Now;
            ageArchivosMultimedia.UbicacionModificacion = Ubicacion.getIpAdress(_httpContextAccessor);
            ageArchivosMultimedia.UsuarioModificacion = ageArchivosMultimediaSaveDAO.UsuarioModificacion;

            Validator.ValidateObject(ageArchivosMultimedia, new ValidationContext(ageArchivosMultimedia), true);

            return ageArchivosMultimedia;
        }

        private AgeArchivosMultimedia FromDAOToEntity(
            IHttpContextAccessor _httpContextAccessor,
            AgeArchivosMultimediaSaveDAO ageArchivosMultimediaSaveDAO)
        {

            return new AgeArchivosMultimedia
            {
                AgeLicencCodigo = ageArchivosMultimediaSaveDAO.Id.ageLicencCodigo,
                Codigo = ageArchivosMultimediaSaveDAO.Id.codigo,
                Descripcion = ageArchivosMultimediaSaveDAO.Descripcion,
                Ruta = ageArchivosMultimediaSaveDAO.Ruta,
                Promocion = ageArchivosMultimediaSaveDAO.Promocion,
                Tipo = ageArchivosMultimediaSaveDAO.Tipo,
                Estado = ageArchivosMultimediaSaveDAO.Estado,
                FechaEstado = DateTime.Now,
                FechaIngreso = DateTime.Now,
                ObservacionEstado = ageArchivosMultimediaSaveDAO.ObservacionEstado,
                UbicacionIngreso = Ubicacion.getIpAdress(_httpContextAccessor),
                UsuarioIngreso = ageArchivosMultimediaSaveDAO.UsuarioIngreso
            };
        }

        private void ValidarPK(AgeArchivosMultimediaPKDAO archivosMultimediaPKDAO){
            ValidateKeys.ValidarNoExistenciaKey(archivosMultimediaPKDAO,
                $"Archivo multimedia con código: {archivosMultimediaPKDAO.codigo} " +
                $"y código licenciatario: {archivosMultimediaPKDAO.ageLicencCodigo} ya existe.",
                _repository.ConsultarCompletePorId);
        }
    }
}
