﻿using AGE.Entities;
using AGE.Entities.DAO.AgeArchivosMultimedia;
using AGE.Entities.DAO.AgeRutas;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;

namespace AGE.Services.AgeArchivosMultimediaService
{
    public interface IAgeArchivosMultimediaService
    {
        Task<Page<AgeArchivosMultimediaDAO>> ConsultarTodos(
            int codigoLicenciatario, int codigo, string ruta, string descripcion, 
            string tipo, Pageable pageable);

        Task<Page<AgeArchivosMultimediaDAO>> ConsultarLicenciatario(
        int codigoLicenciatario,
        int? codigo, 
        string? ruta, 
        string? descripcion,
        string? tipo, 
        Pageable pageable);

        Task<Page<AgeArchivosMultimediaDAO>> ConsultarListaFiltro(
            int codigoLicenciatario,
            string filtro,
            Pageable pageable);

        Task<AgeArchivosMultimediaDAO> ConsultarPorId(
            AgeArchivosMultimediaPKDAO codigo);

        Task<Resource<AgeArchivosMultimediaDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgeArchivosMultimediaSaveDAO agePaisesSaveDAO);

        Task<AgeArchivosMultimediaDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeArchivosMultimediaSaveDAO agePaisesSaveDAO);

        Task<List<Resource<AgeArchivosMultimediaDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeArchivosMultimediaSaveDAO> ageArchivosMultimediaSaveDAOList);

        Task<List<AgeArchivosMultimediaDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeArchivosMultimediaSaveDAO> ageRutasSaveDAOList);
    }
}
