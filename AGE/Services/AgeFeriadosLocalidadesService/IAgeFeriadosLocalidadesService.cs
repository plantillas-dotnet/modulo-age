﻿using AGE.Entities.DAO.AgeFeriadosLocalidade;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;

namespace AGE.Services.AgeFeriadosLocalidadeService
{
    public interface IAgeFeriadosLocalidadesService
    {

        Task<Page<AgeFeriadosLocalidadesDAO>> ConsultarTodos(
            Pageable pageable);

        Task<Page<AgeFeriadosLocalidadesDAO>> ConsultarListaFiltro(
            string filtro,
            Pageable pageable);

        Task<AgeFeriadosLocalidadesDAO> ConsultarPorId(
            AgeFeriadosLocalidadesPKDAO ageFeriadosLocalidadePKDAO);

        Task<Resource<AgeFeriadosLocalidadesDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgeFeriadosLocalidadesSaveDAO ageFeriadosLocalidadeSaveDAO);

        Task<List<Resource<AgeFeriadosLocalidadesDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeFeriadosLocalidadesSaveDAO> ageFeriadosLocalidadeSaveDAOList);

        Task<AgeFeriadosLocalidadesDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeFeriadosLocalidadesSaveDAO ageFeriadosLocalidadeSaveDAO);

        Task<List<AgeFeriadosLocalidadesDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeFeriadosLocalidadesSaveDAO> ageFeriadosLocalidadeSaveDAOList);

    }
}
