﻿using AGE.Controllers;
using AGE.Entities;
using AGE.Entities.DAO.AgeFeriadosLocalidade;
using AGE.Entities.DAO.AgeLocalidades;
using AGE.Entities.DAO.AgeTiposLocalidades;
using AGE.Middleware.Exceptions.BadRequest;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Repositories.AgeDiasFeriadosRepository;
using AGE.Repositories.AgeFeriadosLocalidadeRepository;
using AGE.Repositories.AgeLocalidadesRepository;
using AGE.Repositories.AgePaisesRepository;
using AGE.Repositories.AgeTiposLocalidadeRepository;
using AGE.Utils;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using System.ComponentModel.DataAnnotations;
using System.Transactions;

namespace AGE.Services.AgeFeriadosLocalidadeService
{
    public class AgeFeriadosLocalidadesService : IAgeFeriadosLocalidadesService
    {

        private readonly IAgeFeriadosLocalidadesRepository _repository;
        private readonly IAgeDiasFeriadosRepository _repositoryDiasFeriados;
        private readonly IAgePaisesRepository _repositoryPaises;
        private readonly IAgeLocalidadesRepository _repositoryLocalidades;
        private readonly IAgeTiposLocalidadesRepository _repositoryTiposLocalidades;


        public AgeFeriadosLocalidadesService(
            IAgeFeriadosLocalidadesRepository Repository,
            IAgeDiasFeriadosRepository repositoryDiasFeriados,
            IAgePaisesRepository repositoryPaises,
            IAgeLocalidadesRepository repositoryLocalidades,
            IAgeTiposLocalidadesRepository repositoryTiposLocalidades)
        {
            _repository = Repository;
            _repositoryDiasFeriados = repositoryDiasFeriados;
            _repositoryPaises = repositoryPaises;
            _repositoryLocalidades = repositoryLocalidades;
            _repositoryTiposLocalidades = repositoryTiposLocalidades;
        }



        public Task<Page<AgeFeriadosLocalidadesDAO>> ConsultarTodos(
            Pageable pageable)
        {
            pageable.Validar<AgeFeriadosLocalidadesDAO>();

            return _repository.ConsultarTodos(pageable);
        }


        public Task<AgeFeriadosLocalidadesDAO> ConsultarPorId(
            AgeFeriadosLocalidadesPKDAO ageFeriadosLocalidadePKDAO)
        {
            Validator.ValidateObject(ageFeriadosLocalidadePKDAO, new ValidationContext(ageFeriadosLocalidadePKDAO), true);

            AgeFeriadosLocalidadesDAO? ageFeriadosLocalidadeDAO = _repository.ConsultarPorId(ageFeriadosLocalidadePKDAO).Result;

            if (ageFeriadosLocalidadeDAO == null)
            {
                throw new RegisterNotFoundException("Feriado localidad con código " + ageFeriadosLocalidadePKDAO.AgeDiaFeCodigo + " no existe");
            }

            return Task.FromResult(ageFeriadosLocalidadeDAO);
        }


        private AgeFeriadosLocalidades ConsultarCompletePorId(
            AgeFeriadosLocalidadesPKDAO ageFeriadosLocalidadePKDAO)
        {
            Validator.ValidateObject(ageFeriadosLocalidadePKDAO, new ValidationContext(ageFeriadosLocalidadePKDAO), true);

            AgeFeriadosLocalidades? ageFeriadosLocalidade = _repository.ConsultarCompletePorId(ageFeriadosLocalidadePKDAO).Result;

            return ageFeriadosLocalidade;
        }

        public Task<Page<AgeFeriadosLocalidadesDAO>> ConsultarListaFiltro(
            string filtro, 
            Pageable pageable)
        {
            if (string.IsNullOrWhiteSpace(filtro))
            {
                throw new InvalidFieldException("El filtro es requerido.");
            }

            pageable.Validar<AgeFeriadosLocalidadesDAO>();

            return _repository.ConsultarListaFiltro(filtro.ToLower(), pageable);
        }

        public async Task<Resource<AgeFeriadosLocalidadesDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgeFeriadosLocalidadesSaveDAO ageFeriadosLocalidadeSaveDAO)
        {
            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    AgeFeriadosLocalidades ageFeriadosLocalidade = ValidarInsert(_httpContextAccessor, ageFeriadosLocalidadeSaveDAO);

                    AgeFeriadosLocalidadesDAO ageFeriadosLocalidadeDAO = await _repository.Insertar(ageFeriadosLocalidade);

                    Resource<AgeFeriadosLocalidadesDAO> ageFeriadosLocalidadeDAOWithResource = GetDataWithResource(_httpContextAccessor, ageFeriadosLocalidadeDAO);

                    transactionScope.Complete();

                    return ageFeriadosLocalidadeDAOWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }
        public async Task<List<Resource<AgeFeriadosLocalidadesDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeFeriadosLocalidadesSaveDAO> ageFeriadosLocalidadeSaveDAOList)
        {
            if (ageFeriadosLocalidadeSaveDAOList == null)
            {
                throw new InvalidSintaxisException();
            }

            List<AgeFeriadosLocalidades> ageFeriadosLocalidadeList = new List<AgeFeriadosLocalidades>();

            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    foreach (AgeFeriadosLocalidadesSaveDAO ageFeriadosLocalidadeSaveDAO in ageFeriadosLocalidadeSaveDAOList)
                    {
                        AgeFeriadosLocalidades ageFeriadosLocalidade = ValidarInsert(_httpContextAccessor, ageFeriadosLocalidadeSaveDAO);
                        ageFeriadosLocalidadeList.Add(ageFeriadosLocalidade);
                    }

                    List<AgeFeriadosLocalidadesDAO> ageFeriadosLocalidadeDAOList = await _repository.InsertarVarios(ageFeriadosLocalidadeList);

                    List<Resource<AgeFeriadosLocalidadesDAO>> ageFeriadosLocalidadeDAOListWithResource = new List<Resource<AgeFeriadosLocalidadesDAO>>();

                    foreach (AgeFeriadosLocalidadesDAO ageFeriadosLocalidadeDAO in ageFeriadosLocalidadeDAOList)
                    {
                        ageFeriadosLocalidadeDAOListWithResource.Add(GetDataWithResource(_httpContextAccessor, ageFeriadosLocalidadeDAO));
                    }

                    transactionScope.Complete();

                    return ageFeriadosLocalidadeDAOListWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public Task<AgeFeriadosLocalidadesDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeFeriadosLocalidadesSaveDAO ageFeriadosLocalidadeSaveDAO)
        {
            AgeFeriadosLocalidades ageFeriadosLocalidade = ValidarUpdate(_httpContextAccessor, ageFeriadosLocalidadeSaveDAO);

            return _repository.Actualizar(ageFeriadosLocalidade);
        }

        public Task<List<AgeFeriadosLocalidadesDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeFeriadosLocalidadesSaveDAO> ageFeriadosLocalidadeSaveDAOList)
        {
            if (ageFeriadosLocalidadeSaveDAOList == null)
                throw new InvalidSintaxisException();

            ValidateKeys.ValidarPKDuplicadas(ageFeriadosLocalidadeSaveDAOList,
                                             p => new { p.Id.AgeAgeTipLoAgePaisCodigo, p.Id.AgeLocaliAgeTipLoCodigo,
                                                        p.Id.AgeLocaliCodigo, p.Id.AgeDiaFeCodigo},
                                             "Id");

            List<AgeFeriadosLocalidades> ageFeriadosLocalidadeList = new List<AgeFeriadosLocalidades>();
            AgeFeriadosLocalidades ageFeriadosLocalidade;

            foreach (AgeFeriadosLocalidadesSaveDAO ageFeriadosLocalidadeSaveDAO in ageFeriadosLocalidadeSaveDAOList)
            {
                ageFeriadosLocalidade = ValidarUpdate(_httpContextAccessor, ageFeriadosLocalidadeSaveDAO);
                ageFeriadosLocalidadeList.Add(ageFeriadosLocalidade);
            }

            return _repository.ActualizarVarios(ageFeriadosLocalidadeList);
        }


        private static Resource<AgeFeriadosLocalidadesDAO> GetDataWithResource(
            IHttpContextAccessor _httpContextAccessor,
            AgeFeriadosLocalidadesDAO ageFeriadosLocalidadeDAO)
        {
            string rutaSegmentoFinal = $"{ageFeriadosLocalidadeDAO.Id.AgeAgeTipLoAgePaisCodigo}/{ageFeriadosLocalidadeDAO.Id.AgeLocaliAgeTipLoCodigo}/{ageFeriadosLocalidadeDAO.Id.AgeLocaliCodigo}/{ageFeriadosLocalidadeDAO.Id.AgeDiaFeCodigo}";

            return Resource<AgeFeriadosLocalidadesDAO>.GetDataWithResource<AgeFeriadosLocalidadesController>(
                _httpContextAccessor, rutaSegmentoFinal, ageFeriadosLocalidadeDAO);
        }

        private AgeFeriadosLocalidades ValidarInsert(
        IHttpContextAccessor _httpContextAccessor,
        AgeFeriadosLocalidadesSaveDAO ageFeriadosLocalidadeSaveDAO)
        {
            if (ageFeriadosLocalidadeSaveDAO == null)
                throw new InvalidSintaxisException();

            Validator.ValidateObject(ageFeriadosLocalidadeSaveDAO, new ValidationContext(ageFeriadosLocalidadeSaveDAO), true);

            ValidarKeys(ageFeriadosLocalidadeSaveDAO);

            AgeFeriadosLocalidades entityObject = FromDAOToEntity(_httpContextAccessor, ageFeriadosLocalidadeSaveDAO);

            Validator.ValidateObject(entityObject, new ValidationContext(entityObject), true);

            return entityObject;
        }

        private AgeFeriadosLocalidades ValidarUpdate(
            IHttpContextAccessor _httpContextAccessor,
            AgeFeriadosLocalidadesSaveDAO ageFeriadosLocalidadeSaveDAO)
        {

            if (ageFeriadosLocalidadeSaveDAO == null)
                throw new InvalidSintaxisException();

            if (ageFeriadosLocalidadeSaveDAO.UsuarioModificacion == null || ageFeriadosLocalidadeSaveDAO.UsuarioModificacion <= 0)
                throw new InvalidFieldException("El usuario de modificación debe ser un número entero mayor a cero.");


            AgeFeriadosLocalidades ageFeriadosLocalidade = ConsultarCompletePorId(new AgeFeriadosLocalidadesPKDAO
            {
                AgeAgeTipLoAgePaisCodigo = ageFeriadosLocalidadeSaveDAO.Id.AgeAgeTipLoAgePaisCodigo,
                AgeDiaFeCodigo = ageFeriadosLocalidadeSaveDAO.Id.AgeDiaFeCodigo,
                AgeLocaliAgeTipLoCodigo = ageFeriadosLocalidadeSaveDAO.Id.AgeLocaliAgeTipLoCodigo,
                AgeLocaliCodigo = ageFeriadosLocalidadeSaveDAO.Id.AgeLocaliCodigo
            });

            if (ageFeriadosLocalidade == null)
                throw new RegisterNotFoundException("Feriado localidad con código " + ageFeriadosLocalidadeSaveDAO.Id.AgeDiaFeCodigo + " no existe");


            if (!string.IsNullOrWhiteSpace(ageFeriadosLocalidadeSaveDAO.Estado))
            {
                ageFeriadosLocalidade.Estado = ageFeriadosLocalidadeSaveDAO.Estado;
                ageFeriadosLocalidade.FechaEstado = DateTime.Now;
            }

            if(ageFeriadosLocalidadeSaveDAO.FechaEjecucionDesde != default)
                ageFeriadosLocalidade.FechaEjecucionDesde = ageFeriadosLocalidadeSaveDAO.FechaEjecucionDesde;

            if (ageFeriadosLocalidadeSaveDAO.FechaEjecucionHasta != default)
                ageFeriadosLocalidade.FechaEjecucionHasta = ageFeriadosLocalidadeSaveDAO.FechaEjecucionHasta;

            if (ageFeriadosLocalidadeSaveDAO.HoraEjecucionDesde != default)
                ageFeriadosLocalidade.HoraEjecucionDesde = ageFeriadosLocalidadeSaveDAO.HoraEjecucionDesde;

            if (ageFeriadosLocalidadeSaveDAO.HoraEjecucionHasta != default)
                ageFeriadosLocalidade.HoraEjecucionHasta = ageFeriadosLocalidadeSaveDAO.HoraEjecucionHasta;

            if (!string.IsNullOrWhiteSpace(ageFeriadosLocalidadeSaveDAO.ObservacionEstado))
                ageFeriadosLocalidade.ObservacionEstado = ageFeriadosLocalidadeSaveDAO.ObservacionEstado;

            ageFeriadosLocalidade.FechaModificacion = DateTime.Now;
            ageFeriadosLocalidade.UbicacionModificacion = Ubicacion.getIpAdress(_httpContextAccessor);
            ageFeriadosLocalidade.UsuarioModificacion = ageFeriadosLocalidadeSaveDAO.UsuarioModificacion;

            Validator.ValidateObject(ageFeriadosLocalidade, new ValidationContext(ageFeriadosLocalidade), true);

            return ageFeriadosLocalidade;
        }

        private AgeFeriadosLocalidades FromDAOToEntity(
        IHttpContextAccessor _httpContextAccessor,
        AgeFeriadosLocalidadesSaveDAO ageFeriadosLocalidadeSaveDAO)
        {
            return new AgeFeriadosLocalidades
            {
                AgeAgeTipLoAgePaisCodigo = ageFeriadosLocalidadeSaveDAO.Id.AgeAgeTipLoAgePaisCodigo,
                AgeDiaFeCodigo = ageFeriadosLocalidadeSaveDAO.Id.AgeDiaFeCodigo,
                AgeLocaliAgeTipLoCodigo = ageFeriadosLocalidadeSaveDAO.Id.AgeLocaliAgeTipLoCodigo,
                AgeLocaliCodigo = ageFeriadosLocalidadeSaveDAO.Id.AgeLocaliCodigo,
                FechaEjecucionDesde = ageFeriadosLocalidadeSaveDAO.FechaEjecucionDesde,
                FechaEjecucionHasta = ageFeriadosLocalidadeSaveDAO.FechaEjecucionHasta,
                HoraEjecucionDesde = ageFeriadosLocalidadeSaveDAO.HoraEjecucionDesde == default
                                        ? null : ageFeriadosLocalidadeSaveDAO.HoraEjecucionDesde,
                HoraEjecucionHasta = ageFeriadosLocalidadeSaveDAO.HoraEjecucionHasta == default
                                        ? null : ageFeriadosLocalidadeSaveDAO.HoraEjecucionHasta,
                Estado = ageFeriadosLocalidadeSaveDAO.Estado,
                FechaEstado = DateTime.Now,
                FechaIngreso = DateTime.Now,
                ObservacionEstado = ageFeriadosLocalidadeSaveDAO.ObservacionEstado,
                UbicacionIngreso = Ubicacion.getIpAdress(_httpContextAccessor),
                UsuarioIngreso = ageFeriadosLocalidadeSaveDAO.UsuarioIngreso
            };
        }
        private void ValidarKeys(AgeFeriadosLocalidadesSaveDAO ageFeriadosLocalidadeSaveDAO)
        {
            ValidarPK(ageFeriadosLocalidadeSaveDAO.Id);
            ValidarFKPais(
                ageFeriadosLocalidadeSaveDAO.Id.AgeAgeTipLoAgePaisCodigo);
            ValidarFKTipoLocalidad(
                ageFeriadosLocalidadeSaveDAO.Id.AgeAgeTipLoAgePaisCodigo, 
                ageFeriadosLocalidadeSaveDAO.Id.AgeLocaliAgeTipLoCodigo);
            ValidarFKDiasFeriados(
                ageFeriadosLocalidadeSaveDAO.Id.AgeDiaFeCodigo);
            ValidarFKLocalidad(
                ageFeriadosLocalidadeSaveDAO.Id.AgeAgeTipLoAgePaisCodigo, 
                ageFeriadosLocalidadeSaveDAO.Id.AgeLocaliAgeTipLoCodigo, 
                ageFeriadosLocalidadeSaveDAO.Id.AgeLocaliCodigo);
        }

        private void ValidarPK(AgeFeriadosLocalidadesPKDAO ageFeriadosLocalidadePKDAO)
        {
            ValidateKeys.ValidarNoExistenciaKey(
                ageFeriadosLocalidadePKDAO,
                $"Feriado localidad con código {ageFeriadosLocalidadePKDAO.AgeDiaFeCodigo} ya existe.",
                _repository.ConsultarCompletePorId);
        }

        private void ValidarFKPais(int agePaisCodigo)
        {
            ValidateKeys.ValidarExistenciaKey(
                agePaisCodigo,
                $"País con código {agePaisCodigo} no existe.",
                _repositoryPaises.ConsultarCompletePorId);
        }

        private void ValidarFKTipoLocalidad(int agePaisCodigo, int ageTipoLocalidadCodigo)
        {
            AgeTiposLocalidadePKDAO ageTiposLocalidadePKDAO = new AgeTiposLocalidadePKDAO
            {
                agePaisCodigo = agePaisCodigo,
                codigo = ageTipoLocalidadCodigo
            };

            ValidateKeys.ValidarExistenciaKey(
                ageTiposLocalidadePKDAO,
                $"Tipo localidad con código {ageTipoLocalidadCodigo} no existe.",
                _repositoryTiposLocalidades.ConsultarCompletePorId);
        }

        private void ValidarFKDiasFeriados(int ageDiaFeCodigo)
        {
            ValidateKeys.ValidarExistenciaKey(
                ageDiaFeCodigo,
                $"Días Feriado con código {ageDiaFeCodigo} no existe.",
                _repositoryDiasFeriados.ConsultarCompletePorId);
        }

        private void ValidarFKLocalidad(int agePaisCodigo, int ageTipoLocalidadCodigo, int ageLocalidadCodigo)
        {
            AgeLocalidadesPKDAO ageLocalidadesPKDAO = new AgeLocalidadesPKDAO
            {
                AgeTipLoAgePaisCodigo = agePaisCodigo,
                AgeTipLoCodigo = ageTipoLocalidadCodigo,
                Codigo = ageLocalidadCodigo
            };

            ValidateKeys.ValidarExistenciaKey(
                ageLocalidadesPKDAO,
                $"Localidad con código {ageLocalidadCodigo} no existe.",
                _repositoryLocalidades.ConsultarCompletePorId);
        }

    }
}
