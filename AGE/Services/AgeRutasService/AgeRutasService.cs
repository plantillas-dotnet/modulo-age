﻿using AGE.Controllers;
using AGE.Entities;
using AGE.Entities.DAO.AgeRutas;
using AGE.Middleware.Exceptions.BadRequest;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Repositories.AgeLicenciatariosRepository;
using AGE.Repositories.AgeRutasRepository;
using AGE.Services.AgeLicenciatariosAplicaSecuService;
using AGE.Utils;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using System.ComponentModel.DataAnnotations;
using System.Transactions;

namespace AGE.Services.AgeRutasService
{
    public class AgeRutasService : IAgeRutasService
    {
        private readonly IAgeRutasRepository _repository;
        private readonly IAgeLicenciatariosRepository _repositoryLicenciatario;
        private readonly IAgeLicenciatariosAplicaSecuService _ageLicenciatariosAplicaSecuService;


        public AgeRutasService(
            IAgeRutasRepository Repository, 
            IAgeLicenciatariosAplicaSecuService ageLicenciatariosAplicaSecuService,
            IAgeLicenciatariosRepository repositoryLicenciatario)
        {
            _repository = Repository;
            _ageLicenciatariosAplicaSecuService = ageLicenciatariosAplicaSecuService;
            _repositoryLicenciatario = repositoryLicenciatario;
        }


        public Task<Page<AgeRutasDAO>> ConsultarTodos(
            int codigoLicenciatario, 
            int codigo, string ruta, 
            string descripcion, 
            Pageable pageable)
        {
            if (codigoLicenciatario <= 0)
                throw new InvalidFieldException("El código de licenciatario debe ser un número entero mayor a cero.");

            pageable.Validar<AgeRutasDAO>();

            return _repository.ConsultarTodos(codigoLicenciatario, codigo, ruta, descripcion, pageable);
        }
        
        
        public Task<Page<AgeRutasDAO>> ConsultarListaFiltro(
            int codigoLicenciatario, 
            string filtro, 
            Pageable pageable)
        {
            if (codigoLicenciatario == 0)
                throw new InvalidFieldException("El código de licenciatario debe ser un número entero mayor a cero.");

            if (string.IsNullOrWhiteSpace(filtro))
                throw new InvalidFieldException("El filtro es requerido.");

            pageable.Validar<AgeRutasDAO>();

            return _repository.ConsultarListaFiltro(codigoLicenciatario, filtro.ToLower(), pageable);
        }


        public Task<AgeRutasDAO> ConsultarPorId(
            AgeRutasPKDAO ageRutasPKDAO)
        {
            Validator.ValidateObject(ageRutasPKDAO, new ValidationContext(ageRutasPKDAO), true);

            AgeRutasDAO ageRutasDAO = _repository.ConsultarPorId(ageRutasPKDAO).Result;

            if (ageRutasDAO == null)
                throw new RegisterNotFoundException("Ruta con código " + ageRutasPKDAO.codigo + " no existe");

            return Task.FromResult(ageRutasDAO);
        }


        private AgeRutas ConsultarCompletePorId(
            AgeRutasPKDAO ageRutasPKDAO)
        {
            Validator.ValidateObject(ageRutasPKDAO, new ValidationContext(ageRutasPKDAO), true);

            AgeRutas? ageRuta = _repository.ConsultarCompletePorId(ageRutasPKDAO).Result;

            return ageRuta;
        }

        public async Task<Resource<AgeRutasDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgeRutasSaveDAO ageRutasSaveDAO)
        {
            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    AgeRutas ageRutas = ValidarInsert(_httpContextAccessor, ageRutasSaveDAO);

                    AgeRutasDAO ageRutasDAO = await _repository.Insertar(ageRutas);

                    Resource<AgeRutasDAO> ageRutasDAOWithResource = GetDataWithResource(_httpContextAccessor, ageRutasDAO);

                    transactionScope.Complete();

                    return ageRutasDAOWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public async Task<List<Resource<AgeRutasDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeRutasSaveDAO> ageRutasSaveDAOList)
        {
            if (ageRutasSaveDAOList == null)
            {
                throw new InvalidSintaxisException();
            }

            List<AgeRutas> ageRutasList = new List<AgeRutas>();

            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    foreach (AgeRutasSaveDAO ageRutasSaveDAO in ageRutasSaveDAOList)
                    {
                        AgeRutas ageRutas = ValidarInsert(_httpContextAccessor, ageRutasSaveDAO);
                        ageRutasList.Add(ageRutas);
                    }

                    List<AgeRutasDAO> ageRutasDAOList = await _repository.InsertarVarios(ageRutasList);

                    List<Resource<AgeRutasDAO>> ageRutasDAOListWithResource = new List<Resource<AgeRutasDAO>>();

                    foreach (AgeRutasDAO ageRutasDAO in ageRutasDAOList)
                    {
                        ageRutasDAOListWithResource.Add(GetDataWithResource(_httpContextAccessor, ageRutasDAO));
                    }

                    transactionScope.Complete();

                    return ageRutasDAOListWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public Task<AgeRutasDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeRutasSaveDAO ageRutasSaveDAO)
        {
            AgeRutas ageRutas = ValidarUpdate(_httpContextAccessor, ageRutasSaveDAO);

            return _repository.Actualizar(ageRutas);
        }

        public Task<List<AgeRutasDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeRutasSaveDAO> ageRutasSaveDAOList)
        {
            if (ageRutasSaveDAOList == null)
                throw new InvalidSintaxisException();

            ValidateKeys.ValidarPKDuplicadas(ageRutasSaveDAOList,
                                             p => new { p.Id.ageLicencCodigo, p.Id.codigo },
                                             "Id");

            List<AgeRutas> ageRutasList = new List<AgeRutas>();
            AgeRutas ageRutas;

            foreach (AgeRutasSaveDAO ageRutasSaveDAO in ageRutasSaveDAOList)
            {
                ageRutas = ValidarUpdate(_httpContextAccessor, ageRutasSaveDAO);
                ageRutasList.Add(ageRutas);
            }

            return _repository.ActualizarVarios(ageRutasList);
        }


        private static Resource<AgeRutasDAO> GetDataWithResource(
            IHttpContextAccessor _httpContextAccessor, 
            AgeRutasDAO ageRutasDAO)
        {
            string rutaSegmentoFinal = $"{ageRutasDAO.Id.ageLicencCodigo}/{ageRutasDAO.Id.codigo}";

            return Resource<AgeRutasDAO>.GetDataWithResource<AgeRutasController>(
                _httpContextAccessor, rutaSegmentoFinal, ageRutasDAO);
        }

        private AgeRutas ValidarInsert(
            IHttpContextAccessor _httpContextAccessor, 
            AgeRutasSaveDAO ageRutasSaveDAO)
        {

            if (ageRutasSaveDAO == null)
                throw new InvalidSintaxisException();

            Validator.ValidateObject(ageRutasSaveDAO, new ValidationContext(ageRutasSaveDAO), true);

            FuncionesSecuencias fg = new FuncionesSecuencias(_ageLicenciatariosAplicaSecuService);

            ageRutasSaveDAO.Id.codigo = fg.ObtenerSecuencia(_httpContextAccessor,
                                            ageRutasSaveDAO.Id.ageLicencCodigo,
                                            Globales.CODIGO_SECUENCIA_RUTA,
                                            ageRutasSaveDAO.UsuarioIngreso);

            ValidarKeys(ageRutasSaveDAO);

            AgeRutas entityObject = FromDAOToEntity(_httpContextAccessor, ageRutasSaveDAO);

            Validator.ValidateObject(entityObject, new ValidationContext(entityObject), true);

            return entityObject;
        }


        private AgeRutas ValidarUpdate(
            IHttpContextAccessor _httpContextAccessor, 
            AgeRutasSaveDAO ageRutasSaveDAO)
        {
            if (ageRutasSaveDAO == null)
                throw new InvalidSintaxisException();

            if (ageRutasSaveDAO.UsuarioModificacion == null || ageRutasSaveDAO.UsuarioModificacion <= 0)
                throw new InvalidFieldException("El usuario de modificación debe ser un número entero mayor a cero.");

            AgeRutas? ageRutas = ConsultarCompletePorId(new AgeRutasPKDAO
            {
                ageLicencCodigo = ageRutasSaveDAO.Id.ageLicencCodigo,
                codigo = ageRutasSaveDAO.Id.codigo
            });

            if (ageRutas == null)
                throw new RegisterNotFoundException("Ruta con código " + ageRutasSaveDAO.Id.codigo + " no existe");

            if (!string.IsNullOrWhiteSpace(ageRutasSaveDAO.Estado))
            {
                ageRutas.Estado = ageRutasSaveDAO.Estado;
                ageRutas.FechaEstado = DateTime.Now;
            }

            if (!string.IsNullOrWhiteSpace(ageRutasSaveDAO.Ruta))
                ageRutas.Ruta = ageRutasSaveDAO.Ruta;

            if (!string.IsNullOrWhiteSpace(ageRutasSaveDAO.Descripcion))
                ageRutas.Descripcion = ageRutasSaveDAO.Descripcion;

            if (!string.IsNullOrWhiteSpace(ageRutasSaveDAO.ObservacionEstado))
                ageRutas.ObservacionEstado = ageRutasSaveDAO.ObservacionEstado;

            ageRutas.FechaModificacion = DateTime.Now;
            ageRutas.UbicacionModificacion = Ubicacion.getIpAdress(_httpContextAccessor);
            ageRutas.UsuarioModificacion = ageRutasSaveDAO.UsuarioModificacion;

            Validator.ValidateObject(ageRutas, new ValidationContext(ageRutas), true);

            return ageRutas;
        }


        private void ValidarKeys(AgeRutasSaveDAO ageRutasSaveDAO)
        {
            ValidarPK(ageRutasSaveDAO.Id);

            ValidarFKLicenciatario(ageRutasSaveDAO.Id.ageLicencCodigo);

        }

        private void ValidarPK(AgeRutasPKDAO ageRutasPKDAO)
        {

            ValidateKeys.ValidarNoExistenciaKey(
               ageRutasPKDAO,
               $"Ruta con código {ageRutasPKDAO.codigo} ya existe.",
               _repository.ConsultarCompletePorId);
        }

        private void ValidarFKLicenciatario(int codigoLicenciatario)
        {

            ValidateKeys.ValidarExistenciaKey(
                codigoLicenciatario,
                $"Licenciatario con código {codigoLicenciatario} no existe.",
                _repositoryLicenciatario.ConsultarCompletePorId);
        }

        private AgeRutas FromDAOToEntity(
            IHttpContextAccessor _httpContextAccessor,
            AgeRutasSaveDAO ageRutasSaveDAO)
        {
            return new AgeRutas
            {
                AgeLicencCodigo = ageRutasSaveDAO.Id.ageLicencCodigo,
                Codigo = ageRutasSaveDAO.Id.codigo,
                Descripcion = ageRutasSaveDAO.Descripcion,
                Ruta = ageRutasSaveDAO.Ruta,
                Estado = ageRutasSaveDAO.Estado,
                FechaEstado = DateTime.Now,
                FechaIngreso = DateTime.Now,
                ObservacionEstado = ageRutasSaveDAO.ObservacionEstado,
                UbicacionIngreso = Ubicacion.getIpAdress(_httpContextAccessor),
                UsuarioIngreso = ageRutasSaveDAO.UsuarioIngreso
            };
        }
    }
}
