﻿using AGE.Entities.DAO.AgeRutas;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;

namespace AGE.Services.AgeRutasService
{
    public interface IAgeRutasService
    {
        Task<Page<AgeRutasDAO>> ConsultarTodos(
            int codigoLicenciatario,
            int codigo,
            string ruta,
            string descripcion,
            Pageable pageable);

        Task<Page<AgeRutasDAO>> ConsultarListaFiltro(
            int codigoLicenciatario,
            string filtro,
            Pageable pageable);

        Task<AgeRutasDAO> ConsultarPorId(
            AgeRutasPKDAO AgeRutasPKDAO);

        Task<Resource<AgeRutasDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgeRutasSaveDAO AgeRutasSaveDAO);

        Task<List<Resource<AgeRutasDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeRutasSaveDAO> AgeRutaSaveDAOList);

        Task<AgeRutasDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeRutasSaveDAO AgeRutaSaveDAO);

        Task<List<AgeRutasDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeRutasSaveDAO> AgeRutaSaveDAOList);
    }
}
