﻿using AGE.Entities.Composite.AgePerfilesTransacciones;
using AGE.Entities.DAO.AgePerfilesTransacciones;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;

namespace AGE.Services.AgePerfilesTransaccioneService
{
    public interface IAgePerfilesTransaccionesService
    {
        Task<Page<AgePerfilesTransaccioneDAO>> ConsultarTodos(
            int codigoLicenciatario,
            int codigoPerfil,
            int codigo,
            int codigoTransaccion,
            int codigoAplicacion,
            Pageable pageable);

        Task<Page<AgePerfilesTransaccioneDAO>> ObtenerTodos(
            int codigoLicenciatario,
            int codigo,
            int agePerfilCodigo,
            int ageTransaAgeAplicaCodigo,
            int ageTransaCodigo,
            Pageable pageable);

        Task<AgePerfilTransaccionDAO> ConsultarPorLicenciatarioPerfil(
            int codigoLicenciatario,
            int codigoPerfil,
            int codigoAplicacion);

        Task<Page<AgePerfilesTransaccioneDAO>> ConsultarListaFiltro(
            int codigoLicenciatario,
            string filtro,
            Pageable pageable);

        Task<AgePerfilesTransaccioneDAO> ConsultarPorId(
            AgePerfilesTransaccionePKDAO agePerfilesTransaccionePKDAO);

        Task<Resource<AgePerfilesTransaccioneDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgePerfilesTransaccioneSaveDAO agePerfilesTransaccioneSaveDAO);


        Task<AgePerfilesTransaccionesComposite> InsertarComposite(
            IHttpContextAccessor _httpContextAccessor,
            AgePerfilesTransaccionesCompositeSave agePerfilesTransaccionesCompositeSave);


        Task<List<Resource<AgePerfilesTransaccioneDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgePerfilesTransaccioneSaveDAO> agePerfilesTransaccioneSaveDAOList);

        Task<AgePerfilesTransaccioneDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgePerfilesTransaccioneSaveDAO agePerfilesTransaccioneSaveDAO);

        Task<List<AgePerfilesTransaccioneDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgePerfilesTransaccioneSaveDAO> agePerfilesTransaccioneSaveDAOList);





    }
}
