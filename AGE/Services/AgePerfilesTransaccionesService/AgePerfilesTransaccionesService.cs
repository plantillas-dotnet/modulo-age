﻿using AGE.Controllers;
using AGE.Entities;
using AGE.Entities.Composite.AgePerfilesTransacciones;
using AGE.Entities.DAO.AgePerfiles;
using AGE.Entities.DAO.AgePerfilesTransacciones;
using AGE.Entities.DAO.AgeTransacciones;
using AGE.Middleware.Exceptions.BadRequest;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Repositories.AgePerfilesRepository;
using AGE.Repositories.AgePerfilesTransaccioneRepository;
using AGE.Repositories.AgeTransaccioneRepository;
using AGE.Services.AgeLicenciatariosAplicaSecuService;
using AGE.Services.AgePerfilesService;
using AGE.Utils;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using System.ComponentModel.DataAnnotations;
using System.Transactions;

namespace AGE.Services.AgePerfilesTransaccioneService
{
    public class AgePerfilesTransaccionesService : IAgePerfilesTransaccionesService
    {

        private readonly IAgePerfilesTransaccionesRepository _repository;
        private readonly IAgeTransaccionesRepository _repositoryTransaccion;
        private readonly IAgePerfilesRepository _repositoryPerfiles;
        private readonly IAgeLicenciatariosAplicaSecuService _ageLicenciatariosAplicaSecuService;
        private readonly IAgePerfilesService _servicePerfiles;


        public AgePerfilesTransaccionesService(
            IAgePerfilesTransaccionesRepository Repository,
            IAgeLicenciatariosAplicaSecuService agePerfilAgeLicencCodigo,
            IAgeTransaccionesRepository repositoryTransaccion,
            IAgePerfilesRepository repositoryPerfiles,
            IAgePerfilesService servicePerfiles)
        {
            _repository = Repository;
            _ageLicenciatariosAplicaSecuService = agePerfilAgeLicencCodigo;
            _repositoryTransaccion = repositoryTransaccion;
            _repositoryPerfiles = repositoryPerfiles;
            _servicePerfiles = servicePerfiles;
        }

        public Task<Page<AgePerfilesTransaccioneDAO>> ConsultarTodos(
            int codigoLicenciatario,
            int codigoPerfil,
            int codigo,
            int codigoTransaccion,
            int codigoAplicacion,
            Pageable pageable)
        {
            if (codigoLicenciatario <= 0)
                throw new InvalidFieldException("El código de licenciatario debe ser un número entero mayor a cero.");

            pageable.Validar<AgePerfilesTransaccioneDAO>();

            return _repository.ConsultarTodos(codigoLicenciatario, codigoPerfil, codigo, codigoTransaccion, codigoAplicacion,pageable);
        }

        public Task<Page<AgePerfilesTransaccioneDAO>> ObtenerTodos(
            int codigoLicenciatario,
            int codigo,
            int agePerfilCodigo,
            int ageTransaAgeAplicaCodigo,
            int ageTransaCodigo,
            Pageable pageable)
        {
            if (codigoLicenciatario <= 0)
                throw new InvalidFieldException("El código de licenciatario debe ser un número entero mayor a cero.");

            pageable.Validar<AgePerfilesTransaccioneDAO>();

            return _repository.ConsultarTodos(codigoLicenciatario, codigo, agePerfilCodigo, ageTransaAgeAplicaCodigo, ageTransaCodigo, pageable);
        }

        public Task<AgePerfilTransaccionDAO> ConsultarPorLicenciatarioPerfil(
            int codigoLicenciatario,
            int codigoPerfil,
            int codigoAplicacion)

        {
            if (codigoLicenciatario <= 0)
                throw new InvalidFieldException("El código de licenciatario debe ser un número entero mayor a cero.");

            if (codigoPerfil <= 0)
                throw new InvalidFieldException("El código de perfil debe ser un número entero mayor a cero.");
            
            return _repository.ConsultarPorLicenciatarioPerfil(codigoLicenciatario, codigoPerfil, codigoAplicacion);
        }   

        public Task<Page<AgePerfilesTransaccioneDAO>> ConsultarListaFiltro(
            int codigoLicenciatario,
            string filtro,
            Pageable pageable)
        {
            if (codigoLicenciatario == 0)
                throw new InvalidFieldException("El código de licenciatario debe ser un número entero mayor a cero.");
            
            if (string.IsNullOrWhiteSpace(filtro))
                throw new InvalidFieldException("El filtro es requerido.");

            pageable.Validar<AgePerfilesTransaccioneDAO>();

            return _repository.ConsultarListaFiltro(codigoLicenciatario, filtro.ToLower(), pageable);
        }

        public Task<AgePerfilesTransaccioneDAO> ConsultarPorId(
            AgePerfilesTransaccionePKDAO agePerfilesTransaccionePKDAO)
        {

            Validator.ValidateObject(agePerfilesTransaccionePKDAO, new ValidationContext(agePerfilesTransaccionePKDAO), true);

            AgePerfilesTransaccioneDAO agePerfilesTransaccioneDAO = _repository.ConsultarPorId(agePerfilesTransaccionePKDAO).Result;

            return agePerfilesTransaccioneDAO == null
                ? throw new RegisterNotFoundException("Transacción para perfil con código " + agePerfilesTransaccionePKDAO.codigo + " no existe")
                : Task.FromResult(agePerfilesTransaccioneDAO);
        }

        private AgePerfilesTransacciones ConsultarCompletePorId(
            AgePerfilesTransaccionePKDAO agePerfilesTransaccionePKDAO)
        {
            Validator.ValidateObject(agePerfilesTransaccionePKDAO, new ValidationContext(agePerfilesTransaccionePKDAO), true);

            AgePerfilesTransacciones agePerfilesTransaccione = _repository.ConsultarCompletePorId(agePerfilesTransaccionePKDAO).Result;

            return agePerfilesTransaccione;
        }

        public async Task<Resource<AgePerfilesTransaccioneDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgePerfilesTransaccioneSaveDAO agePerfilesTransaccioneSaveDAO)
        {
            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    AgePerfilesTransacciones agePerfilesTransaccione = ValidarInsert(_httpContextAccessor, agePerfilesTransaccioneSaveDAO);

                    AgePerfilesTransaccioneDAO agePerfilesTransaccioneDAO = await _repository.Insertar(agePerfilesTransaccione);

                    Resource<AgePerfilesTransaccioneDAO> agePerfilesTransaccioneDAOWithResource = GetDataWithResource(_httpContextAccessor, agePerfilesTransaccioneDAO);

                    transactionScope.Complete();

                    return agePerfilesTransaccioneDAOWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public async Task<AgePerfilesTransaccionesComposite> InsertarComposite(
            IHttpContextAccessor _httpContextAccessor,
            AgePerfilesTransaccionesCompositeSave agePerfilesTransaccionesCompositeSave)
        {
            AgePerfilesTransaccionesComposite agePerfilesTransaccionesComposite = new AgePerfilesTransaccionesComposite();

            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                { 
                    Resource<AgePerfilesDAO> agePerfilesDAO = await _servicePerfiles.Insertar(
                    _httpContextAccessor, agePerfilesTransaccionesCompositeSave.AgePerfiles);

                    agePerfilesTransaccionesComposite.AgePerfiles = agePerfilesDAO;

                    foreach (AgePerfilesTransaccioneSaveDAO agePerfilesTransaccioneSaveDAO
                                in agePerfilesTransaccionesCompositeSave.AgePerfilesTransaccionesList)
                    {
                        agePerfilesTransaccioneSaveDAO.Id.agePerfilCodigo = agePerfilesDAO.content.Id.codigo;

                        if (agePerfilesTransaccioneSaveDAO.Id.agePerfilAgeLicencCodigo <= 0)
                            agePerfilesTransaccioneSaveDAO.Id.agePerfilAgeLicencCodigo = agePerfilesDAO.content.Id.ageLicencCodigo;

                    }

                    List<Resource<AgePerfilesTransaccioneDAO>> agePerfilesTransaccioneDAOWithResource =
                        await InsertarVarios(_httpContextAccessor,
                                            agePerfilesTransaccionesCompositeSave.AgePerfilesTransaccionesList);

                    agePerfilesTransaccionesComposite.AgePerfilesTransaccionesList = new List<Resource<AgePerfilesTransaccioneDAO>>();

                    foreach (Resource<AgePerfilesTransaccioneDAO> agePerfilesTransaccioneSaveDAOWithResouce
                                in agePerfilesTransaccioneDAOWithResource)
                    {
                        agePerfilesTransaccionesComposite.AgePerfilesTransaccionesList
                            .Add(agePerfilesTransaccioneSaveDAOWithResouce);
                    }

                    transactionScope.Complete();

                    return agePerfilesTransaccionesComposite;
                }
                catch (Exception)
                {
                    throw;
                }

            }
        }

        public async Task<List<Resource<AgePerfilesTransaccioneDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgePerfilesTransaccioneSaveDAO> agePerfilesTransaccioneSaveDAOList)
        {
            if (agePerfilesTransaccioneSaveDAOList == null)
                throw new InvalidSintaxisException();

            List<AgePerfilesTransacciones> agePerfilesTransaccioneList = new List<AgePerfilesTransacciones>();

            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    foreach (AgePerfilesTransaccioneSaveDAO agePerfilesTransaccioneSaveDAO in agePerfilesTransaccioneSaveDAOList)
                    {
                        AgePerfilesTransacciones agePerfilesTransaccione = ValidarInsert(_httpContextAccessor, agePerfilesTransaccioneSaveDAO);
                        agePerfilesTransaccioneList.Add(agePerfilesTransaccione);
                    }

                    List<AgePerfilesTransaccioneDAO> agePerfilesTransaccioneDAOList = await _repository.InsertarVarios(agePerfilesTransaccioneList);

                    List<Resource<AgePerfilesTransaccioneDAO>> agePerfilesTransaccioneDAOListWithResource = new List<Resource<AgePerfilesTransaccioneDAO>>();

                    foreach (AgePerfilesTransaccioneDAO agePerfilesTransaccioneDAO in agePerfilesTransaccioneDAOList)
                    {
                        agePerfilesTransaccioneDAOListWithResource.Add(GetDataWithResource(_httpContextAccessor, agePerfilesTransaccioneDAO));
                    }

                    transactionScope.Complete();

                    return agePerfilesTransaccioneDAOListWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public Task<AgePerfilesTransaccioneDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgePerfilesTransaccioneSaveDAO agePerfilesTransaccioneSaveDAO)
        {
            AgePerfilesTransacciones agePerfilesTransaccione = ValidarUpdate(_httpContextAccessor, agePerfilesTransaccioneSaveDAO);

            return _repository.Actualizar(agePerfilesTransaccione);
        }

        public Task<List<AgePerfilesTransaccioneDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgePerfilesTransaccioneSaveDAO> agePerfilesTransaccioneSaveDAOList)
        {
            if (agePerfilesTransaccioneSaveDAOList == null)
                throw new InvalidSintaxisException();

            ValidateKeys.ValidarPKDuplicadas(agePerfilesTransaccioneSaveDAOList, 
                                                p => new { p.Id.agePerfilCodigo,p.Id.agePerfilAgeLicencCodigo,p.Id.codigo }, 
                                                "Id");

            List<AgePerfilesTransacciones> agePerfilesTransaccioneList = new List<AgePerfilesTransacciones>();
            AgePerfilesTransacciones agePerfilesTransaccione;

            foreach (AgePerfilesTransaccioneSaveDAO agePerfilesTransaccioneSaveDAO in agePerfilesTransaccioneSaveDAOList)
            {
                agePerfilesTransaccione = ValidarUpdate(_httpContextAccessor, agePerfilesTransaccioneSaveDAO);
                agePerfilesTransaccioneList.Add(agePerfilesTransaccione);
            }

            return _repository.ActualizarVarios(agePerfilesTransaccioneList);
        }

        private static Resource<AgePerfilesTransaccioneDAO> GetDataWithResource(
            IHttpContextAccessor _httpContextAccessor,
            AgePerfilesTransaccioneDAO agePerfilesTransaccioneDAO)
        {
            string rutaSegmentoFinal = $"{agePerfilesTransaccioneDAO.Id.agePerfilAgeLicencCodigo}/" +
                $"{agePerfilesTransaccioneDAO.Id.agePerfilCodigo}/{agePerfilesTransaccioneDAO.Id.codigo}";

            return Resource<AgePerfilesTransaccioneDAO>.GetDataWithResource<AgePerfilesTransaccionesController>(
                _httpContextAccessor, rutaSegmentoFinal, agePerfilesTransaccioneDAO);
        }

        private AgePerfilesTransacciones ValidarInsert(
            IHttpContextAccessor _httpContextAccessor,
            AgePerfilesTransaccioneSaveDAO agePerfilesTransaccioneSaveDAO)
        {
            if (agePerfilesTransaccioneSaveDAO == null)
                throw new InvalidSintaxisException();

            Validator.ValidateObject(agePerfilesTransaccioneSaveDAO, new ValidationContext(agePerfilesTransaccioneSaveDAO), true);

            FuncionesSecuencias fg = new FuncionesSecuencias(_ageLicenciatariosAplicaSecuService);

            agePerfilesTransaccioneSaveDAO.Id.codigo = fg.ObtenerSecuencia(_httpContextAccessor,
                                                            agePerfilesTransaccioneSaveDAO.Id.agePerfilAgeLicencCodigo,
                                                            Globales.CODIGO_SECUENCIA_PERFIL_TRX,
                                                            agePerfilesTransaccioneSaveDAO.UsuarioIngreso);

            ValidarKeys(agePerfilesTransaccioneSaveDAO);

            AgePerfilesTransacciones entityObject = FromDAOToEntity(_httpContextAccessor, agePerfilesTransaccioneSaveDAO);

            Validator.ValidateObject(entityObject, new ValidationContext(entityObject), true);

            return entityObject;
        }

        private AgePerfilesTransacciones ValidarUpdate(
            IHttpContextAccessor _httpContextAccessor,
            AgePerfilesTransaccioneSaveDAO agePerfilesTransaccioneSaveDAO)
        {
            if (agePerfilesTransaccioneSaveDAO == null)
                throw new InvalidSintaxisException();

            if (agePerfilesTransaccioneSaveDAO.UsuarioModificacion == null || agePerfilesTransaccioneSaveDAO.UsuarioModificacion <= 0)
                throw new InvalidFieldException("El usuario de modificación debe ser un número entero mayor a cero.");

            AgePerfilesTransacciones agePerfilesTransaccione = ConsultarCompletePorId(new AgePerfilesTransaccionePKDAO
            {
                agePerfilAgeLicencCodigo = agePerfilesTransaccioneSaveDAO.Id.agePerfilAgeLicencCodigo,
                agePerfilCodigo = agePerfilesTransaccioneSaveDAO.Id.agePerfilCodigo,
                codigo = agePerfilesTransaccioneSaveDAO.Id.codigo
            }) ?? throw new RegisterNotFoundException("Transacción para perfil con código: " + agePerfilesTransaccioneSaveDAO.Id.codigo + " no existe");

            ValidarFKPerfiles(agePerfilesTransaccioneSaveDAO.Id.agePerfilAgeLicencCodigo,
                                agePerfilesTransaccioneSaveDAO.Id.agePerfilCodigo);

            if (agePerfilesTransaccioneSaveDAO.AgeTransaAgeAplicaCodigo != 0 || agePerfilesTransaccioneSaveDAO.AgeTransaCodigo != 0)
                ValidarFKTransacciones(agePerfilesTransaccioneSaveDAO.AgeTransaAgeAplicaCodigo,
                                       agePerfilesTransaccioneSaveDAO.AgeTransaCodigo);

            if (!string.IsNullOrWhiteSpace(agePerfilesTransaccioneSaveDAO.Estado))
            {
                agePerfilesTransaccione.Estado = agePerfilesTransaccioneSaveDAO.Estado;
                agePerfilesTransaccione.FechaEstado = DateTime.Now;
            }

            if (!string.IsNullOrWhiteSpace(agePerfilesTransaccioneSaveDAO.ObservacionEstado))
                agePerfilesTransaccione.ObservacionEstado = agePerfilesTransaccioneSaveDAO.ObservacionEstado;

            agePerfilesTransaccione.FechaModificacion = DateTime.Now;
            agePerfilesTransaccione.UbicacionModificacion = Ubicacion.getIpAdress(_httpContextAccessor);
            agePerfilesTransaccione.UsuarioModificacion = agePerfilesTransaccioneSaveDAO.UsuarioModificacion;

            Validator.ValidateObject(agePerfilesTransaccione, new ValidationContext(agePerfilesTransaccione), true);

            return agePerfilesTransaccione;
        }

        private AgePerfilesTransacciones FromDAOToEntity(
            IHttpContextAccessor _httpContextAccessor,
            AgePerfilesTransaccioneSaveDAO agePerfilesTransaccioneSaveDAO)
        {
            return new AgePerfilesTransacciones
            {
                AgePerfilAgeLicencCodigo = agePerfilesTransaccioneSaveDAO.Id.agePerfilAgeLicencCodigo,
                AgePerfilCodigo = agePerfilesTransaccioneSaveDAO.Id.agePerfilCodigo,
                Codigo = agePerfilesTransaccioneSaveDAO.Id.codigo,
                AgeTransaAgeAplicaCodigo = agePerfilesTransaccioneSaveDAO.AgeTransaAgeAplicaCodigo,
                AgeTransaCodigo = agePerfilesTransaccioneSaveDAO.AgeTransaCodigo,
                Estado = agePerfilesTransaccioneSaveDAO.Estado,
                FechaEstado = DateTime.Now,
                FechaIngreso = DateTime.Now,
                ObservacionEstado = agePerfilesTransaccioneSaveDAO.ObservacionEstado,
                UbicacionIngreso = Ubicacion.getIpAdress(_httpContextAccessor),
                UsuarioIngreso = agePerfilesTransaccioneSaveDAO.UsuarioIngreso
            };
        }

        private void ValidarKeys(AgePerfilesTransaccioneSaveDAO agePerfilesTransaccioneSaveDAO)
        {
            ValidarPK(agePerfilesTransaccioneSaveDAO.Id);

            ValidarFKPerfiles(agePerfilesTransaccioneSaveDAO.Id.agePerfilAgeLicencCodigo,
                                agePerfilesTransaccioneSaveDAO.Id.agePerfilCodigo);

            ValidarFKTransacciones(agePerfilesTransaccioneSaveDAO.AgeTransaAgeAplicaCodigo, 
                                   agePerfilesTransaccioneSaveDAO.AgeTransaCodigo);

        }

        private void ValidarPK(AgePerfilesTransaccionePKDAO agePerfilesTransaccionePKDAO)
        {

            ValidateKeys.ValidarNoExistenciaKey(
               agePerfilesTransaccionePKDAO,
               $"Perfil transacción con código: {agePerfilesTransaccionePKDAO.codigo}," +
               $" código licenciatario: {agePerfilesTransaccionePKDAO.agePerfilAgeLicencCodigo} " +
               $"y código perfil: {agePerfilesTransaccionePKDAO.agePerfilCodigo} ya existe.",
               _repository.ConsultarCompletePorId);
        }

        private void ValidarFKTransacciones(int codigoAplicacion, int codigo)
        {
            AgeTransaccionePKDAO ageTransaccionePKDAO = new AgeTransaccionePKDAO
            {
                ageAplicaCodigo = codigoAplicacion,
                codigo = codigo
            };

            ValidateKeys.ValidarExistenciaKey(
                ageTransaccionePKDAO,
                $"Transacción con código: {ageTransaccionePKDAO.codigo}" +
                $" y código de aplicación: {ageTransaccionePKDAO.ageAplicaCodigo} no existe.",
                _repositoryTransaccion.ConsultarCompletePorId);
        }

        private void ValidarFKPerfiles(int codigoLicenciatario, int codigo)
        {
            AgePerfilesPKDAO agePerfilesPKDAO = new AgePerfilesPKDAO
            {
                ageLicencCodigo = codigoLicenciatario,
                codigo = codigo
            };

            ValidateKeys.ValidarExistenciaKey(
                agePerfilesPKDAO,
                $"Perfil con código: {agePerfilesPKDAO.codigo}" +
                $" y código licenciatario: {agePerfilesPKDAO.ageLicencCodigo} no existe.",
                _repositoryPerfiles.ConsultarCompletePorId);
        }

    }
}
