﻿using AGE.Entities.DAO.AgeLogErrores;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;

namespace AGE.Services.AgeLogErroresService
{
    public interface IAgeLogErroresService
    {
        Task<Page<AgeLogErroresDAO>> ConsultarTodos(int ageLicApAgeLicencCodigo,
            int? ageLicApAgeAplicaCodigo,
            long? codigo,
            DateTime? fechaDesde,
            DateTime? fechaHasta,
            string? mensaje,
            Pageable pageable);

        Task<AgeLogErroresDAO> ConsultarPorId(AgeLogErroresPKDAO ageLogErroresPKDAO);

        Task<Page<AgeLogErroresDAO>> ConsultarListaFiltro(int codigoLicenciatario,
                                                        string filtro,
                                                        Pageable pageable);

        Task<Resource<AgeLogErroresDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgeLogErroresSaveDAO AgeLogErroresSaveDAO);

        Task<List<Resource<AgeLogErroresDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeLogErroresSaveDAO> AgeLogErroresSaveDAOList);

        Task<AgeLogErroresDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeLogErroresSaveDAO AgeLogErroresSaveDAO);

        Task<List<AgeLogErroresDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeLogErroresSaveDAO> AgeLogErroresSaveDAOList);
    }
}
