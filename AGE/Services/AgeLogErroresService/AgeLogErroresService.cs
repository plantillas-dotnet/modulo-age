﻿using AGE.Controllers;
using AGE.Entities.DAO.AgeLogErrores;
using AGE.Entities;
using AGE.Middleware.Exceptions.BadRequest;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Repositories.AgeLogErroresRepository;
using AGE.Services.AgeLicenciatariosAplicaSecuService;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using AGE.Utils;
using System.ComponentModel.DataAnnotations;
using System.Transactions;
using AGE.Entities.DAO.AgeLicenciatarioAplicacion;
using AGE.Repositories.AgeLicenciatariosAplicacionRepository;

namespace AGE.Services.AgeLogErroresService
{
    public class AgeLogErroresService : IAgeLogErroresService
    {
        private readonly IAgeLogErroresRepository _repository;
        private readonly IAgeLicenciatarioAplicacionRepository _repositoryLicenciatarioAplica;
        private readonly IAgeLicenciatariosAplicaSecuService _ageLicenciatariosAplicaSecuService;

        public AgeLogErroresService(
            IAgeLogErroresRepository Repository,
            IAgeLicenciatarioAplicacionRepository RepositoryLicenciatarioAplica,
            IAgeLicenciatariosAplicaSecuService ageLicenciatariosAplicaSecuService)
        {
            _repository = Repository;
            _repositoryLicenciatarioAplica = RepositoryLicenciatarioAplica;
            _ageLicenciatariosAplicaSecuService = ageLicenciatariosAplicaSecuService;
        }

        public Task<Page<AgeLogErroresDAO>> ConsultarTodos(int ageLicApAgeLicencCodigo,
                                                            int? ageLicApAgeAplicaCodigo,
                                                            long? codigo,
                                                            DateTime? fechaDesde,
                                                            DateTime? fechaHasta,
                                                            string? mensaje,
                                                            Pageable pageable)
        {
            if (ageLicApAgeLicencCodigo <= 0)
                throw new InvalidFieldException("El código de licenciatario debe ser un número entero mayor a cero.");

            pageable.Validar<AgeLogErroresDAO>();

            return _repository.ConsultarTodos(ageLicApAgeLicencCodigo,ageLicApAgeAplicaCodigo, codigo, fechaDesde, fechaHasta, mensaje, pageable);
        }

        public Task<AgeLogErroresDAO> ConsultarPorId(AgeLogErroresPKDAO ageLogErrores)
        {
            Validator.ValidateObject(ageLogErrores, new ValidationContext(ageLogErrores), true);

            AgeLogErroresDAO ageLogErroresDAO = _repository.ConsultarPorId(ageLogErrores).Result;

            return ageLogErroresDAO == null
                ? throw new RegisterNotFoundException("Log error con código: " + ageLogErrores.Codigo +
                                                        ", código licenciatario: " + ageLogErrores.AgeLicApAgeLicencCodigo +
                                                        " y código de aplicación: " + ageLogErrores.AgeLicApAgeAplicaCodigo + " no existe.")
                : Task.FromResult(ageLogErroresDAO);
        }

        private AgeLogErrores ConsultarCompletePorId(AgeLogErroresPKDAO ageLogErrorePKDAO)
        {
            Validator.ValidateObject(ageLogErrorePKDAO, new ValidationContext(ageLogErrorePKDAO), true);

            AgeLogErrores ageLogErrore = _repository.ConsultarCompletePorId(ageLogErrorePKDAO).Result;

            return ageLogErrore;
        }

        public Task<Page<AgeLogErroresDAO>> ConsultarListaFiltro(int CodigoLicenciatario, string filtro, Pageable pageable)
        {
            if (CodigoLicenciatario <= 0)
                throw new InvalidFieldException("El código de licenciatario debe ser un número entero mayor a cero.");

            if (string.IsNullOrWhiteSpace(filtro))
                throw new InvalidFieldException("El filtro es requerido.");

            pageable.Validar<AgeLogErroresDAO>();

            return _repository.ConsultarListaFiltro(CodigoLicenciatario, filtro, pageable);
        }

        public async Task<Resource<AgeLogErroresDAO>> Insertar(IHttpContextAccessor _httpContextAccessor,
                                                    AgeLogErroresSaveDAO AgeLogErroresSaveDAO)
        {
            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    AgeLogErrores ageLogErrores = ValidarInsert(_httpContextAccessor, AgeLogErroresSaveDAO);
                    AgeLogErroresDAO ageLogErroresDAO = await _repository.Insertar(ageLogErrores);

                    Resource<AgeLogErroresDAO> ageLogErroresDAOWithResource = GetDataWithResource(_httpContextAccessor, ageLogErroresDAO);

                    transactionScope.Complete();

                    return ageLogErroresDAOWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }

        }

        public async Task<List<Resource<AgeLogErroresDAO>>> InsertarVarios(IHttpContextAccessor _httpContextAccessor,
                                                                List<AgeLogErroresSaveDAO> AgeLogErroresSaveDAOList)
        {
            if (AgeLogErroresSaveDAOList == null)
                throw new InvalidSintaxisException();

            List<AgeLogErrores> ageLogErroresList = new();

            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    foreach (AgeLogErroresSaveDAO ageLogErroresSaveDAO in AgeLogErroresSaveDAOList)
                    {
                        AgeLogErrores ageLogErrores = ValidarInsert(_httpContextAccessor, ageLogErroresSaveDAO);
                        ageLogErroresList.Add(ageLogErrores);
                    }

                    List<AgeLogErroresDAO> ageLogErroresDAOList = await _repository.InsertarVarios(ageLogErroresList);

                    List<Resource<AgeLogErroresDAO>> ageLogErroresDAOListWithResource = new();

                    foreach (AgeLogErroresDAO ageLogErroresDAO in ageLogErroresDAOList)
                    {
                        ageLogErroresDAOListWithResource.Add(GetDataWithResource(_httpContextAccessor, ageLogErroresDAO));
                    }

                    transactionScope.Complete();

                    return ageLogErroresDAOListWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public Task<AgeLogErroresDAO> Actualizar(IHttpContextAccessor _httpContextAccessor,
            AgeLogErroresSaveDAO AgeLogErroresSaveDAO)
        {
            AgeLogErrores ageLogErrores = ValidarUpdate(_httpContextAccessor, AgeLogErroresSaveDAO);

            return _repository.Actualizar(ageLogErrores);
        }

        public Task<List<AgeLogErroresDAO>> ActualizarVarios(IHttpContextAccessor _httpContextAccessor,
                                                                List<AgeLogErroresSaveDAO> AgeLogErroresSaveDAOList)
        {
            if (AgeLogErroresSaveDAOList == null)
                throw new InvalidSintaxisException();

            ValidateKeys.ValidarPKDuplicadas(AgeLogErroresSaveDAOList, p => new { p.Id.AgeLicApAgeAplicaCodigo, 
                                                                                p.Id.AgeLicApAgeLicencCodigo, 
                                                                                p.Id.Codigo }, "Id");

            List<AgeLogErrores> ageLogErroresList = new();
            AgeLogErrores ageLogErrores;

            foreach (AgeLogErroresSaveDAO ageLogErroresSave in AgeLogErroresSaveDAOList)
            {
                ageLogErrores = ValidarUpdate(_httpContextAccessor, ageLogErroresSave);
                ageLogErroresList.Add(ageLogErrores);
            }

            return _repository.ActualizarVarios(ageLogErroresList);
        }

        private static Resource<AgeLogErroresDAO> GetDataWithResource(
            IHttpContextAccessor _httpContextAccessor,
            AgeLogErroresDAO ageLogErroresDAO)
        {
            string rutaSegmentoFinal = $"{ageLogErroresDAO.Id.AgeLicApAgeLicencCodigo}/{ageLogErroresDAO.Id.AgeLicApAgeAplicaCodigo}/{ageLogErroresDAO.Id.Codigo}";

            return Resource<AgeLogErroresDAO>.GetDataWithResource<AgeLogErroresController>(
                _httpContextAccessor, rutaSegmentoFinal, ageLogErroresDAO);
        }

        private AgeLogErrores ValidarInsert(
            IHttpContextAccessor _httpContextAccessor,
            AgeLogErroresSaveDAO ageLogErroresSaveDAO)
        {
            if (ageLogErroresSaveDAO == null) throw new InvalidSintaxisException();

            Validator.ValidateObject(ageLogErroresSaveDAO, new ValidationContext(ageLogErroresSaveDAO), true);

            if (ageLogErroresSaveDAO.Fecha == default)
                throw new InvalidFieldException("La fecha no puede ser nula.");

            FuncionesSecuencias fg = new(_ageLicenciatariosAplicaSecuService);

            ageLogErroresSaveDAO.Id.Codigo = fg.ObtenerSecuencia(_httpContextAccessor,
                ageLogErroresSaveDAO.Id.AgeLicApAgeLicencCodigo,
                Globales.CODIGO_SECUENCIA_LOG_ERROR,
                ageLogErroresSaveDAO.UsuarioIngreso);

            ValidarKeys(ageLogErroresSaveDAO.Id);

            AgeLogErrores entityObject = FromDAOToEntity(_httpContextAccessor,ageLogErroresSaveDAO);

            Validator.ValidateObject(entityObject, new ValidationContext(entityObject), true);

            return entityObject;
        }

        private AgeLogErrores ValidarUpdate(IHttpContextAccessor httpContextAccessor, AgeLogErroresSaveDAO ageLogErroresSaveDAO)
        {
            if (ageLogErroresSaveDAO == null)
                throw new InvalidSintaxisException();

            if (ageLogErroresSaveDAO.UsuarioModificacion == null || ageLogErroresSaveDAO.UsuarioModificacion <= 0)
                throw new InvalidFieldException("El usuario de modificación debe ser un número entero mayor a cero.");

            if (ageLogErroresSaveDAO.Id == null)
                throw new InvalidFieldException("El código de licenciatario y el código de modalidad no pueden ser nulos.");

            AgeLogErrores ageLogErrores = ConsultarCompletePorId(ageLogErroresSaveDAO.Id) 
                ?? throw new RegisterNotFoundException("Log error con código: " + ageLogErroresSaveDAO.Id.Codigo + " no existe.");

            ageLogErrores.Estado = !string.IsNullOrWhiteSpace(ageLogErroresSaveDAO.Estado) ? ageLogErroresSaveDAO.Estado : ageLogErrores.Estado;
            ageLogErrores.FechaEstado = !string.IsNullOrWhiteSpace(ageLogErroresSaveDAO.Estado) ? DateTime.Now : ageLogErrores.FechaEstado;
            ageLogErrores.Mensaje = !string.IsNullOrWhiteSpace(ageLogErroresSaveDAO.Mensaje) ? ageLogErroresSaveDAO.Mensaje : ageLogErrores.Mensaje;
            ageLogErrores.ObservacionEstado = !string.IsNullOrWhiteSpace(ageLogErroresSaveDAO.ObservacionEstado) ? ageLogErroresSaveDAO.ObservacionEstado : ageLogErrores.ObservacionEstado;

            ageLogErrores.Fecha = ageLogErroresSaveDAO.Fecha != default ? ageLogErroresSaveDAO.Fecha : ageLogErrores.Fecha;
            ageLogErrores.FechaModificacion = DateTime.Now;
            ageLogErrores.UbicacionModificacion = Ubicacion.getIpAdress(httpContextAccessor);
            ageLogErrores.UsuarioModificacion = ageLogErroresSaveDAO.UsuarioModificacion;

            Validator.ValidateObject(ageLogErrores, new ValidationContext(ageLogErrores), true);
            return ageLogErrores;
        }

        private AgeLogErrores FromDAOToEntity(
           IHttpContextAccessor _httpContextAccessor,
          AgeLogErroresSaveDAO ageLogErroresSaveDAO)
        {

            return new AgeLogErrores()
            {
                AgeLicApAgeLicencCodigo = ageLogErroresSaveDAO.Id.AgeLicApAgeLicencCodigo,
                AgeLicApAgeAplicaCodigo = ageLogErroresSaveDAO.Id.AgeLicApAgeAplicaCodigo,
                Codigo = ageLogErroresSaveDAO.Id.Codigo,
                Mensaje = ageLogErroresSaveDAO.Mensaje,
                Fecha = ageLogErroresSaveDAO.Fecha,
                Estado = ageLogErroresSaveDAO.Estado,
                FechaEstado = DateTime.Now,
                FechaIngreso = DateTime.Now,
                ObservacionEstado = ageLogErroresSaveDAO.ObservacionEstado,
                UbicacionIngreso = Ubicacion.getIpAdress(_httpContextAccessor),
                UsuarioIngreso = ageLogErroresSaveDAO.UsuarioIngreso
            };
        }

        private void ValidarKeys(AgeLogErroresPKDAO ageLogErroresPKDAO)
        {
            ValidarPK(ageLogErroresPKDAO);
            ValidarFkLicenciatarioAplicacion(ageLogErroresPKDAO);
        }

        private void ValidarPK(AgeLogErroresPKDAO ageLogErroresPKDAO)
        {
            ValidateKeys.ValidarNoExistenciaKey(ageLogErroresPKDAO, $"Log Error con código: {ageLogErroresPKDAO.Codigo}" +
                      $", código licenciatario: {ageLogErroresPKDAO.AgeLicApAgeLicencCodigo} " +
                      $"y código aplicación: {ageLogErroresPKDAO.AgeLicApAgeAplicaCodigo} ya existe."
                      , _repository.ConsultarCompletePorId);
        }

        private void ValidarFkLicenciatarioAplicacion(AgeLogErroresPKDAO ageLogErroresPKDAO)
        {
            AgeLicenciatariosAplicacionesPKDAO ageLicenciatariosAplicacionPKDAO = new AgeLicenciatariosAplicacionesPKDAO
            {
                AgeAplicaCodigo = ageLogErroresPKDAO.AgeLicApAgeAplicaCodigo,
                AgeLicencCodigo = ageLogErroresPKDAO.AgeLicApAgeLicencCodigo
            };

            ValidateKeys.ValidarExistenciaKey(ageLicenciatariosAplicacionPKDAO,
                $"Licenciatario Aplicación con código licenciatario: {ageLicenciatariosAplicacionPKDAO.AgeLicencCodigo} " +
                $"y código aplicación: {ageLicenciatariosAplicacionPKDAO.AgeAplicaCodigo} ya existe.",
                _repositoryLicenciatarioAplica.ConsultarCompletePorId);
        }
    }
}
