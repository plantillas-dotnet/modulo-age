﻿using AGE.Entities.DAO.AgeMonedas;
using AGE.Entities;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;

namespace AGE.Services.AgeMonedasService
{
    public interface IAgeMonedasService
    {
        Task<Page<AgeMonedasDAO>> ConsultarTodos(
            int codigo,
            string descripcion,
            string codigoExterno,
            string monedaSeparadorDecimal,
            string monedaSimbolo,
            Pageable pageable);
        Task<Page<AgeMonedasDAO>> ConsultarListaFiltro(string filtro, Pageable pageable);
        Task<AgeMonedasDAO> ConsultarPorId(
            int codigo);
        AgeMonedas ConsultarCompletePorId(
            int codigo);
        Task<Resource<AgeMonedasDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgeMonedasSaveDAO ageMonedasSaveDAO);
        Task<AgeMonedasDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeMonedasSaveDAO ageMonedasSaveDAO);
        Task<List<Resource<AgeMonedasDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeMonedasSaveDAO> ageMonedasSaveDAOList);
        Task<List<AgeMonedasDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeMonedasSaveDAO> ageMonedasSaveDAOList);
    }
}
