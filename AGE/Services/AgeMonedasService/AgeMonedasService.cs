﻿using AGE.Controllers;
using AGE.Entities;
using AGE.Entities.DAO.AgeMonedas;
using AGE.Middleware.Exceptions.BadRequest;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Repositories.AgeMonedasRepository;
using AGE.Services.AgeSecuenciasPrimariasService;
using AGE.Utils;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using System.ComponentModel.DataAnnotations;
using System.Transactions;

namespace AGE.Services.AgeMonedasService
{
    public class AgeMonedasService : IAgeMonedasService
    {

        private readonly IAgeMonedasRepository _repository;
        private readonly IAgeSecuenciasPrimariasService _ageSecuenciasPrimariasService;

        public AgeMonedasService(
            IAgeMonedasRepository Repository,
            IAgeSecuenciasPrimariasService ageSecuenciasPrimariasService)
        {
            _repository = Repository;
            _ageSecuenciasPrimariasService = ageSecuenciasPrimariasService;
        }

        public Task<Page<AgeMonedasDAO>> ConsultarTodos(
            int codigo,
            string descripcion,
            string codigoExterno,
            string monedaSeparadorDecimal,
            string monedaSimbolo,
            Pageable pageable)
        {
            pageable.Validar<AgeMonedasDAO>();

            return _repository.ConsultarTodos(codigo, descripcion, codigoExterno, 
                monedaSeparadorDecimal, monedaSimbolo, pageable);
        }

        public Task<AgeMonedasDAO> ConsultarPorId(
            int codigo)
        {
            if (codigo <= 0)
                throw new InvalidIdException();

            AgeMonedasDAO? ageMonedasDAO = _repository.ConsultarPorId(codigo).Result;

            return ageMonedasDAO == null
                ? throw new RegisterNotFoundException("Moneda con código: " + codigo + " no existe.")
                : Task.FromResult(ageMonedasDAO);
        }

        public AgeMonedas ConsultarCompletePorId(
            int codigo)
        {
            if (codigo <= 0)
                throw new InvalidIdException();

            return _repository.ConsultarCompletePorId(codigo).Result;
        }

        public Task<Page<AgeMonedasDAO>> ConsultarListaFiltro(string filtro, Pageable pageable)
        {
            if (string.IsNullOrWhiteSpace(filtro))
                throw new InvalidFieldException("El filtro es requerido.");

            pageable.Validar<AgeMonedasDAO>();

            return _repository.ConsultarListaFiltro(filtro, pageable);
        }

        public async Task<Resource<AgeMonedasDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgeMonedasSaveDAO ageMonedasSaveDAO)
        {
            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    AgeMonedas ageMonedas = ValidarInsert(_httpContextAccessor, ageMonedasSaveDAO);

                    AgeMonedasDAO ageMonedasDAO = await _repository.Insertar(ageMonedas);

                    Resource<AgeMonedasDAO> ageMonedasDAOWithResource = GetDataWithResource(_httpContextAccessor, ageMonedasDAO);

                    transactionScope.Complete();

                    return ageMonedasDAOWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public async Task<List<Resource<AgeMonedasDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeMonedasSaveDAO> ageMonedasSaveDAOList)
        {
            if (ageMonedasSaveDAOList == null)
                throw new InvalidSintaxisException();

            List<AgeMonedas> ageMonedasList = new List<AgeMonedas>();

            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    foreach (AgeMonedasSaveDAO ageMonedasSaveDAO in ageMonedasSaveDAOList)
                    {
                        AgeMonedas AgeMoneda = ValidarInsert(_httpContextAccessor, ageMonedasSaveDAO);
                        ageMonedasList.Add(AgeMoneda);
                    }

                    List<AgeMonedasDAO> ageMonedasDAOList = await _repository.InsertarVarios(ageMonedasList);

                    List<Resource<AgeMonedasDAO>> ageMonedasDAOListWithResource = new List<Resource<AgeMonedasDAO>>();

                    foreach (AgeMonedasDAO ageMonedasDAO in ageMonedasDAOList)
                    {
                        ageMonedasDAOListWithResource.Add(GetDataWithResource(_httpContextAccessor, ageMonedasDAO));
                    }

                    transactionScope.Complete();

                    return ageMonedasDAOListWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public Task<AgeMonedasDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeMonedasSaveDAO ageMonedasSaveDAO)
        {
            AgeMonedas ageMonedas = ValidarUpdate(_httpContextAccessor, ageMonedasSaveDAO);

            return _repository.Actualizar(ageMonedas);
        }

        public Task<List<AgeMonedasDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeMonedasSaveDAO> ageMonedasSaveDAOList)
        {
            if (ageMonedasSaveDAOList == null)
                throw new InvalidSintaxisException();

            ValidateKeys.ValidarPKDuplicadas(ageMonedasSaveDAOList, p => new { p.Codigo }, "Codigo");

            List<AgeMonedas> ageMonedasList = new List<AgeMonedas>();
            AgeMonedas ageMonedas;

            foreach (AgeMonedasSaveDAO ageRutasSaveDAO in ageMonedasSaveDAOList)
            {
                ageMonedas = ValidarUpdate(_httpContextAccessor, ageRutasSaveDAO);
                ageMonedasList.Add(ageMonedas);
            }

            return _repository.ActualizarVarios(ageMonedasList);
        }

        private static Resource<AgeMonedasDAO> GetDataWithResource(
            IHttpContextAccessor _httpContextAccessor,
            AgeMonedasDAO ageMonedasDAO)
        {
            string rutaSegmentoFinal = $"{ageMonedasDAO.Codigo}";

            return Resource<AgeMonedasDAO>.GetDataWithResource<AgeMonedasController>(
                _httpContextAccessor, rutaSegmentoFinal, ageMonedasDAO);
        }

        private AgeMonedas ValidarInsert(
            IHttpContextAccessor _httpContextAccessor,
            AgeMonedasSaveDAO ageMonedasSaveDAO)
        {
            if (ageMonedasSaveDAO == null)
                throw new InvalidSintaxisException();

            Validator.ValidateObject(ageMonedasSaveDAO, new ValidationContext(ageMonedasSaveDAO), true);

            FuncionesSecuencias fg = new FuncionesSecuencias(_ageSecuenciasPrimariasService);

            ageMonedasSaveDAO.Codigo = fg.ObtenerSecuenciaPrimaria(
                        _httpContextAccessor,
                        Globales.CODIGO_SECUENCIA_MONEDA,
                        ageMonedasSaveDAO.UsuarioIngreso);

            ValidarPK(ageMonedasSaveDAO.Codigo);

            AgeMonedas entityObject = FromDAOToEntity(_httpContextAccessor,ageMonedasSaveDAO);

            Validator.ValidateObject(entityObject, new ValidationContext(entityObject), true);

            return entityObject;
        }

        private AgeMonedas ValidarUpdate(
            IHttpContextAccessor _httpContextAccessor,
            AgeMonedasSaveDAO ageMonedasSaveDAO)
        {
            if (ageMonedasSaveDAO == null)
                throw new InvalidSintaxisException();

            if (ageMonedasSaveDAO.UsuarioModificacion == null || ageMonedasSaveDAO.UsuarioModificacion <= 0)
                throw new InvalidFieldException("El usuario de modificación debe ser un número entero mayor a cero.");

            AgeMonedas ageMonedas = ConsultarCompletePorId(ageMonedasSaveDAO.Codigo) ?? 
                throw new RegisterNotFoundException($"Moneda con código: {ageMonedasSaveDAO.Codigo} no existe.");

            if (!string.IsNullOrWhiteSpace(ageMonedasSaveDAO.Estado))
            {
                ageMonedas.Estado = ageMonedasSaveDAO.Estado;
                ageMonedas.FechaEstado = DateTime.Now;
            }

            if (!string.IsNullOrWhiteSpace(ageMonedasSaveDAO.Descripcion))
                ageMonedas.Descripcion = ageMonedasSaveDAO.Descripcion;

            if (!string.IsNullOrWhiteSpace(ageMonedasSaveDAO.ObservacionEstado))
                ageMonedas.ObservacionEstado = ageMonedasSaveDAO.ObservacionEstado;

            if(!string.IsNullOrEmpty(ageMonedasSaveDAO.CodigoExterno))
                ageMonedas.CodigoExterno = ageMonedasSaveDAO.CodigoExterno;

            if(!string.IsNullOrEmpty(ageMonedasSaveDAO.MonedaSimbolo))
                ageMonedas.MonedaSimbolo = ageMonedasSaveDAO.MonedaSimbolo;

            if(!string.IsNullOrWhiteSpace(ageMonedasSaveDAO.MonedaSeparadorDecimal))
                ageMonedas.MonedaSeparadorDecimal = ageMonedasSaveDAO.MonedaSeparadorDecimal;

            ageMonedas.FechaModificacion = DateTime.Now;
            ageMonedas.UbicacionModificacion = Ubicacion.getIpAdress(_httpContextAccessor);
            ageMonedas.UsuarioModificacion = ageMonedasSaveDAO.UsuarioModificacion;

            Validator.ValidateObject(ageMonedas, new ValidationContext(ageMonedas), true);

            return ageMonedas;
        }

        private AgeMonedas FromDAOToEntity(
           IHttpContextAccessor _httpContextAccessor,
           AgeMonedasSaveDAO ageMonedasSaveDAO)
        {

            return new AgeMonedas
            {
                Codigo = ageMonedasSaveDAO.Codigo,
                Descripcion = ageMonedasSaveDAO.Descripcion,
                CodigoExterno = ageMonedasSaveDAO.CodigoExterno,
                MonedaSeparadorDecimal = ageMonedasSaveDAO.MonedaSeparadorDecimal,
                MonedaSimbolo = ageMonedasSaveDAO.MonedaSimbolo,
                Estado = ageMonedasSaveDAO.Estado.ToUpper().ToString(),
                FechaEstado = DateTime.Now,
                FechaIngreso = DateTime.Now,
                ObservacionEstado = ageMonedasSaveDAO.ObservacionEstado,
                UsuarioIngreso = ageMonedasSaveDAO.UsuarioIngreso,
                UbicacionIngreso = Ubicacion.getIpAdress(_httpContextAccessor)
            };
        }
        private void ValidarPK(int codigoMonedas)
        {
            ValidateKeys.ValidarNoExistenciaKey(codigoMonedas,
                $"Moneda con código: {codigoMonedas} ya existe.",
                _repository.ConsultarPorId);
        }
    }
}
