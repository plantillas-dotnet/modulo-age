﻿
using AGE.Entities.DAO.AgeMensajesIdiomas;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;

namespace AGE.Services.AgeMensajesIdiomasService
{
    public interface IAgeMensajesIdiomaService
    {

        Task<Page<AgeMensajesIdiomaDAO>> ConsultarTodos(
            long ageMensajCodigo,
            int ageIdiomaCodigo,
            string descripcionMsg,
            string solucionMsg,
            Pageable pageable);

        Task<Page<AgeMensajesIdiomaDAO>> ConsultarListaFiltro(
            string filtro,
            Pageable pageable);

        Task<AgeMensajesIdiomaDAO> ConsultarPorId(
            AgeMensajesIdiomaPKDAO ageMensajesIdiomaPKDAO);

        Task<Resource<AgeMensajesIdiomaDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgeMensajesIdiomaSaveDAO ageMensajesIdiomaSaveDAO);

        Task<List<Resource<AgeMensajesIdiomaDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeMensajesIdiomaSaveDAO> ageMensajesIdiomaSaveDAOList);

        Task<AgeMensajesIdiomaDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeMensajesIdiomaSaveDAO ageMensajesIdiomaSaveDAO);

        Task<List<AgeMensajesIdiomaDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeMensajesIdiomaSaveDAO> ageMensajesIdiomaSaveDAOList);

    }
}
