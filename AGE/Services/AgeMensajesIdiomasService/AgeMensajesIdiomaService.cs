﻿using AGE.Controllers;
using AGE.Entities;
using AGE.Entities.DAO.AgeMensajesIdiomas;
using AGE.Middleware.Exceptions.BadRequest;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Utils;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using System.ComponentModel.DataAnnotations;
using System.Transactions;
using AGE.Repositories.AgeMensajesIdiomasRepository;
using AGE.Repositories.AgeMensajesRepository;
using AGE.Repositories.AgeIdiomaRepository;

namespace AGE.Services.AgeMensajesIdiomasService
{
    public class AgeMensajesIdiomaService : IAgeMensajesIdiomaService
    {

        private readonly IAgeMensajesIdiomaRepository _repository;
        private readonly IAgeMensajeRepository _repositoryMensaje;
        private readonly IAgeIdiomasRepository _repositoryIdioma;

        public AgeMensajesIdiomaService(
            IAgeMensajesIdiomaRepository Repository,
            IAgeMensajeRepository repositoryMensaje,
            IAgeIdiomasRepository repositoryIdioma)
        {
            _repository = Repository;
            _repositoryMensaje = repositoryMensaje;
            _repositoryIdioma = repositoryIdioma;
        }

        public Task<Page<AgeMensajesIdiomaDAO>> ConsultarTodos(
            long ageMensajCodigo,
            int ageIdiomaCodigo,
            string descripcionMsg,
            string solucionMsg,
            Pageable pageable)
        {
            pageable.Validar<AgeMensajesIdiomaDAO>();

            return _repository.ConsultarTodos(ageMensajCodigo, ageIdiomaCodigo, descripcionMsg, solucionMsg, pageable);
        }

        public Task<AgeMensajesIdiomaDAO> ConsultarPorId(
            AgeMensajesIdiomaPKDAO ageMensajesIdiomaPKDAO)
        {
            Validator.ValidateObject(ageMensajesIdiomaPKDAO, new ValidationContext(ageMensajesIdiomaPKDAO), true);

            AgeMensajesIdiomaDAO? ageMensajesIdiomaDAO = _repository.ConsultarPorId(ageMensajesIdiomaPKDAO).Result;

            return ageMensajesIdiomaDAO == null
                ? throw new RegisterNotFoundException("Mensaje Idioma con código: " + ageMensajesIdiomaPKDAO.AgeMensajCodigo + " no existe.")
                : Task.FromResult(ageMensajesIdiomaDAO);
        }

        private AgeMensajesIdiomas ConsultarCompletePorId(
            AgeMensajesIdiomaPKDAO ageMensajesIdiomaPKDAO)
        {
            Validator.ValidateObject(ageMensajesIdiomaPKDAO, new ValidationContext(ageMensajesIdiomaPKDAO), true);

            AgeMensajesIdiomas? ageMensajesIdioma = _repository.ConsultarCompletePorId(ageMensajesIdiomaPKDAO).Result;

            return ageMensajesIdioma;
        }

        public Task<Page<AgeMensajesIdiomaDAO>> ConsultarListaFiltro(
            string filtro, 
            Pageable pageable)
        {
            if (string.IsNullOrWhiteSpace(filtro))
                throw new InvalidFieldException("El filtro es requerido.");

            pageable.Validar<AgeMensajesIdiomaDAO>();

            return _repository.ConsultarListaFiltro(filtro.ToLower(), pageable);
        }

        public async Task<Resource<AgeMensajesIdiomaDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor, 
            AgeMensajesIdiomaSaveDAO ageMensajesIdiomaSaveDAO)
        {
            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    AgeMensajesIdiomas ageMensajesIdioma = ValidarInsert(_httpContextAccessor, ageMensajesIdiomaSaveDAO);

                    AgeMensajesIdiomaDAO ageMensajesIdiomaDAO = await _repository.Insertar(ageMensajesIdioma);

                    Resource<AgeMensajesIdiomaDAO> ageMensajesIdiomaDAOWithResource = GetDataWithResource(_httpContextAccessor, ageMensajesIdiomaDAO);

                    transactionScope.Complete();

                    return ageMensajesIdiomaDAOWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public async Task<List<Resource<AgeMensajesIdiomaDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor, 
            List<AgeMensajesIdiomaSaveDAO> ageMensajesIdiomaSaveDAOList)
        {
            if (ageMensajesIdiomaSaveDAOList == null)
                throw new InvalidSintaxisException();

            ValidateKeys.ValidarPKDuplicadas(ageMensajesIdiomaSaveDAOList, p => new { p.Id.AgeIdiomaCodigo, p.Id.AgeMensajCodigo }, "Id");

            List<AgeMensajesIdiomas> ageMensajesIdiomaList = new List<AgeMensajesIdiomas>();

            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    foreach (AgeMensajesIdiomaSaveDAO ageMensajesIdiomaSaveDAO in ageMensajesIdiomaSaveDAOList)
                    {
                        AgeMensajesIdiomas ageMensaje = ValidarInsert(_httpContextAccessor, ageMensajesIdiomaSaveDAO);
                        ageMensajesIdiomaList.Add(ageMensaje);
                    }

                    List<AgeMensajesIdiomaDAO> ageMensajesDAOList = await _repository.InsertarVarios(ageMensajesIdiomaList);

                    List<Resource<AgeMensajesIdiomaDAO>> ageMensajesDAOListWithResource = new List<Resource<AgeMensajesIdiomaDAO>>();

                    foreach (AgeMensajesIdiomaDAO ageMensajesIdiomaDAO in ageMensajesDAOList)
                    {
                        ageMensajesDAOListWithResource.Add(GetDataWithResource(_httpContextAccessor, ageMensajesIdiomaDAO));
                    }

                    transactionScope.Complete();

                    return ageMensajesDAOListWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public Task<AgeMensajesIdiomaDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeMensajesIdiomaSaveDAO ageMensajesIdiomaSaveDAO)
        {
            AgeMensajesIdiomas ageMensajesIdioma = ValidarUpdate(_httpContextAccessor, ageMensajesIdiomaSaveDAO);

            return _repository.Actualizar(ageMensajesIdioma);
        }

        public Task<List<AgeMensajesIdiomaDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeMensajesIdiomaSaveDAO> ageMensajesIdiomaSaveDAOList)
        {
            if (ageMensajesIdiomaSaveDAOList == null)
                throw new InvalidSintaxisException();

            ValidateKeys.ValidarPKDuplicadas(ageMensajesIdiomaSaveDAOList, p => new { p.Id.AgeIdiomaCodigo,p.Id.AgeMensajCodigo }, "Id");

            List<AgeMensajesIdiomas> ageMensajesIdiomaList = new List<AgeMensajesIdiomas>();
            AgeMensajesIdiomas ageMensajesIdioma;

            foreach (AgeMensajesIdiomaSaveDAO ageMensajesIdiomaSaveDAO in ageMensajesIdiomaSaveDAOList)
            {
                ageMensajesIdioma = ValidarUpdate(_httpContextAccessor, ageMensajesIdiomaSaveDAO);
                ageMensajesIdiomaList.Add(ageMensajesIdioma);
            }

            return _repository.ActualizarVarios(ageMensajesIdiomaList);
        }

        private static Resource<AgeMensajesIdiomaDAO> GetDataWithResource(
            IHttpContextAccessor _httpContextAccessor,
            AgeMensajesIdiomaDAO ageMensajesIdiomaDAO)
        {
            string rutaSegmentoFinal = $"{ageMensajesIdiomaDAO.Id.AgeMensajCodigo}/{ageMensajesIdiomaDAO.Id.AgeIdiomaCodigo}";

            return Resource<AgeMensajesIdiomaDAO>.GetDataWithResource<AgeMensajesIdiomasController>(
                _httpContextAccessor, rutaSegmentoFinal, ageMensajesIdiomaDAO);
        }

        private AgeMensajesIdiomas ValidarInsert(
            IHttpContextAccessor _httpContextAccessor,
            AgeMensajesIdiomaSaveDAO ageMensajesIdiomaSaveDAO)
        {

            if (ageMensajesIdiomaSaveDAO == null)
                throw new InvalidSintaxisException();

            Validator.ValidateObject(ageMensajesIdiomaSaveDAO, new ValidationContext(ageMensajesIdiomaSaveDAO), true);

            ValidarKeys(ageMensajesIdiomaSaveDAO.Id);

            AgeMensajesIdiomas entityObject = FromDAOToEntity(_httpContextAccessor,ageMensajesIdiomaSaveDAO);

            Validator.ValidateObject(entityObject, new ValidationContext(entityObject), true);

            return entityObject;
        }

        private AgeMensajesIdiomas ValidarUpdate(
            IHttpContextAccessor _httpContextAccessor,
            AgeMensajesIdiomaSaveDAO ageMensajesIdiomaSaveDAO)
        {
            if (ageMensajesIdiomaSaveDAO == null)
                throw new InvalidSintaxisException();

            if (ageMensajesIdiomaSaveDAO.UsuarioModificacion == null || ageMensajesIdiomaSaveDAO.UsuarioModificacion <= 0)
                throw new InvalidFieldException("El usuario de modificación debe ser un número entero mayor a cero.");

            AgeMensajesIdiomas ageMensajesIdioma = ConsultarCompletePorId(ageMensajesIdiomaSaveDAO.Id) 
                ?? throw new RegisterNotFoundException("Mensaje con código: " + ageMensajesIdiomaSaveDAO.Id.AgeMensajCodigo + " no existe.");

            if (!string.IsNullOrWhiteSpace(ageMensajesIdiomaSaveDAO.Estado))
            {
                ageMensajesIdioma.Estado = ageMensajesIdiomaSaveDAO.Estado;
                ageMensajesIdioma.FechaEstado = DateTime.Now;
            }

            if (!string.IsNullOrWhiteSpace(ageMensajesIdiomaSaveDAO.DescripcionMsg))
                ageMensajesIdioma.DescripcionMsg = ageMensajesIdiomaSaveDAO.DescripcionMsg;

            if (!string.IsNullOrWhiteSpace(ageMensajesIdiomaSaveDAO.SolucionMsg))
                ageMensajesIdioma.SolucionMsg = ageMensajesIdiomaSaveDAO.SolucionMsg;

            if (!string.IsNullOrWhiteSpace(ageMensajesIdiomaSaveDAO.ObservacionEstado))
                ageMensajesIdioma.ObservacionEstado = ageMensajesIdiomaSaveDAO.ObservacionEstado;

            ageMensajesIdioma.FechaModificacion = DateTime.Now;
            ageMensajesIdioma.UbicacionModificacion = Ubicacion.getIpAdress(_httpContextAccessor);
            ageMensajesIdioma.UsuarioModificacion = ageMensajesIdiomaSaveDAO.UsuarioModificacion;

            Validator.ValidateObject(ageMensajesIdioma, new ValidationContext(ageMensajesIdioma), true);

            return ageMensajesIdioma;
        }

        private AgeMensajesIdiomas FromDAOToEntity(
            IHttpContextAccessor _httpContextAccessor,
            AgeMensajesIdiomaSaveDAO ageMensajesIdiomaSaveDAO)
        {

            return new AgeMensajesIdiomas
            {
                AgeMensajCodigo = ageMensajesIdiomaSaveDAO.Id.AgeMensajCodigo,
                AgeIdiomaCodigo = ageMensajesIdiomaSaveDAO.Id.AgeIdiomaCodigo,
                DescripcionMsg = ageMensajesIdiomaSaveDAO.DescripcionMsg,
                SolucionMsg = ageMensajesIdiomaSaveDAO.SolucionMsg,
                Estado = ageMensajesIdiomaSaveDAO.Estado,
                FechaEstado = DateTime.Now,
                FechaIngreso = DateTime.Now,
                ObservacionEstado = ageMensajesIdiomaSaveDAO.ObservacionEstado,
                UbicacionIngreso = Ubicacion.getIpAdress(_httpContextAccessor),
                UsuarioIngreso = ageMensajesIdiomaSaveDAO.UsuarioIngreso
            };
        }

        private void ValidarKeys(AgeMensajesIdiomaPKDAO ageMensajesIdiomaPKDAO)
        {
            ValidarPK(ageMensajesIdiomaPKDAO);
            ValidarFkIdioma(ageMensajesIdiomaPKDAO.AgeIdiomaCodigo);
            ValidarFkMensaje(ageMensajesIdiomaPKDAO.AgeMensajCodigo);
        }

        private void ValidarPK(AgeMensajesIdiomaPKDAO ageMensajesIdiomaPKDAO)
        {
            ValidateKeys.ValidarNoExistenciaKey(
               ageMensajesIdiomaPKDAO,
               $"Mensaje con código: {ageMensajesIdiomaPKDAO.AgeMensajCodigo} ya existe.",
               _repository.ConsultarCompletePorId);
        }

        private void ValidarFkIdioma(int codigoIdioma)
        {
            ValidateKeys.ValidarExistenciaKey(
               codigoIdioma,
               $"Idioma con código: {codigoIdioma} no existe.",
               _repositoryIdioma.ConsultarCompletePorId);
        }

        private void ValidarFkMensaje(long codigoMensaje)
        {
            ValidateKeys.ValidarExistenciaKey(
               codigoMensaje,
               $"Mensaje con código: {codigoMensaje} no existe.",
               _repositoryMensaje.ConsultarCompletePorId);
        }
    }
}
