﻿using AGE.Controllers;
using AGE.Entities.DAO.AgeIdioma;
using AGE.Entities;
using AGE.Middleware.Exceptions.BadRequest;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Repositories.AgeIdiomaRepository;
using AGE.Services.AgeSecuenciasPrimariasService;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using AGE.Utils;
using System.ComponentModel.DataAnnotations;
using System.Transactions;

namespace AGE.Services.AgeIdiomaService
{
    public class AgeIdiomasService : IAgeIdiomasService
    {
        private readonly IAgeIdiomasRepository _repository;
        private readonly IAgeSecuenciasPrimariasService _ageSecuenciasPrimariasService;

        public AgeIdiomasService(
            IAgeIdiomasRepository Repository,
            IAgeSecuenciasPrimariasService ageSecuenciasPrimariasService)
        {
            _repository = Repository;
            _ageSecuenciasPrimariasService = ageSecuenciasPrimariasService;
        }

        public Task<Page<AgeIdiomasDAO>> ConsultarTodos(
            int codigo,
            string descripcion,
            Pageable pageable)
        {
            pageable.Validar<AgeIdiomasDAO>();
             
            return _repository.ConsultarTodos(codigo, descripcion, pageable);
        }

        public Task<AgeIdiomasDAO> ConsultarPorId(int codigo)
        {
            if (codigo <= 0)
                throw new InvalidIdException();

            AgeIdiomasDAO? ageIdiomaDAO = _repository.ConsultarPorId(codigo).Result;

            if (ageIdiomaDAO == null)
                throw new RegisterNotFoundException("Idioma con código: " + ageIdiomaDAO + " no existe.");

            return Task.FromResult(ageIdiomaDAO);
        }

        public Task<AgeIdiomas> ConsultarCompletePorId(int codigo)
        {
            if (codigo <= 0)
                throw new InvalidIdException();

            return _repository.ConsultarCompletePorId(codigo);
        }

        public Task<Page<AgeIdiomasDAO>> ConsultarListaFiltro(
            string filtro,
            Pageable pageable)
        {
            if (string.IsNullOrWhiteSpace(filtro))
                throw new InvalidFieldException("El filtro es requerido.");

            pageable.Validar<AgeIdiomasDAO>();

            return _repository.ConsultarListaFiltro(filtro.ToLower(), pageable);
        }

        public async Task<Resource<AgeIdiomasDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgeIdiomasSaveDAO ageIdiomaSaveDAO)
        {
            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    AgeIdiomas ageIdioma = ValidarInsert(_httpContextAccessor, ageIdiomaSaveDAO);

                    AgeIdiomasDAO ageIdiomaDAO = await _repository.Insertar(ageIdioma);

                    Resource<AgeIdiomasDAO> ageIdiomaDAOWithResource = GetDataWithResource(_httpContextAccessor, ageIdiomaDAO);

                    transactionScope.Complete();

                    return ageIdiomaDAOWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }
        
        public async Task<List<Resource<AgeIdiomasDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeIdiomasSaveDAO> ageIdiomaSaveDAOList)
        {
            if (ageIdiomaSaveDAOList == null)
                throw new InvalidSintaxisException();

            List<AgeIdiomas> ageIdiomaList = new List<AgeIdiomas>();
            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    foreach (AgeIdiomasSaveDAO ageIdiomaSaveDAO in ageIdiomaSaveDAOList)
                    {
                        AgeIdiomas ageIdioma = ValidarInsert(_httpContextAccessor, ageIdiomaSaveDAO);
                        ageIdiomaList.Add(ageIdioma);
                    }

                    List<AgeIdiomasDAO> ageIdiomaDAOList = await _repository.InsertarVarios(ageIdiomaList);

                    List<Resource<AgeIdiomasDAO>> ageIdiomaDAOListWithResource = new List<Resource<AgeIdiomasDAO>>();

                    foreach (AgeIdiomasDAO ageIdiomaDAO in ageIdiomaDAOList)
                    {
                        ageIdiomaDAOListWithResource.Add(GetDataWithResource(_httpContextAccessor, ageIdiomaDAO));
                    }

                    transactionScope.Complete();

                    return ageIdiomaDAOListWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public Task<AgeIdiomasDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeIdiomasSaveDAO ageIdiomaSaveDAO)
        {
            AgeIdiomas ageIdioma = ValidarUpdate(_httpContextAccessor, ageIdiomaSaveDAO);

            return _repository.Actualizar(ageIdioma);
        }

        public Task<List<AgeIdiomasDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeIdiomasSaveDAO> ageIdiomaSaveDAOList)
        {
            if (ageIdiomaSaveDAOList == null)
                throw new InvalidSintaxisException();

            ValidateKeys.ValidarPKDuplicadas(ageIdiomaSaveDAOList, p => new { p.Codigo }, "Codigo");

            List<AgeIdiomas> ageIdiomaList = new List<AgeIdiomas>();
            AgeIdiomas ageIdioma;

            foreach (AgeIdiomasSaveDAO ageIdiomaSaveDAO in ageIdiomaSaveDAOList)
            {
                ageIdioma = ValidarUpdate(_httpContextAccessor, ageIdiomaSaveDAO);
                ageIdiomaList.Add(ageIdioma);
            }

            return _repository.ActualizarVarios(ageIdiomaList);
        }

        private static Resource<AgeIdiomasDAO> GetDataWithResource(
            IHttpContextAccessor _httpContextAccessor,
            AgeIdiomasDAO ageIdiomaDAO)
        {
            string idtipoSegmentoFinal = $"{ageIdiomaDAO.Codigo}";

            return Resource<AgeIdiomasDAO>.GetDataWithResource<AgeIdiomasController>(
                _httpContextAccessor, idtipoSegmentoFinal, ageIdiomaDAO);
        }

        private AgeIdiomas ValidarInsert(
            IHttpContextAccessor _httpContextAccessor,
            AgeIdiomasSaveDAO ageIdiomaSaveDAO)
        {
            if (ageIdiomaSaveDAO == null)
                throw new InvalidSintaxisException();

            Validator.ValidateObject(ageIdiomaSaveDAO, new ValidationContext(ageIdiomaSaveDAO), true);

            FuncionesSecuencias fg = new FuncionesSecuencias(_ageSecuenciasPrimariasService);

            ageIdiomaSaveDAO.Codigo = fg.ObtenerSecuenciaPrimaria(_httpContextAccessor, 
                Globales.CODIGO_SECUENCIA_IDIOMA, 
                ageIdiomaSaveDAO.UsuarioIngreso);

            ValidarPK(ageIdiomaSaveDAO.Codigo);

            AgeIdiomas entityObject = FromDAOToEntity(_httpContextAccessor,ageIdiomaSaveDAO);

            Validator.ValidateObject(entityObject, new ValidationContext(entityObject), true);

            return entityObject;
        }

        private AgeIdiomas ValidarUpdate(
            IHttpContextAccessor _httpContextAccessor,
            AgeIdiomasSaveDAO ageIdiomaSaveDAO)
        {
            if (ageIdiomaSaveDAO == null)
                throw new InvalidSintaxisException();

            if (ageIdiomaSaveDAO.UsuarioModificacion == null || ageIdiomaSaveDAO.UsuarioModificacion <= 0)
                throw new InvalidFieldException("El usuario de modificación debe ser un número entero mayor a cero.");

            AgeIdiomas ageIdioma = ConsultarCompletePorId(ageIdiomaSaveDAO.Codigo).Result ?? 
                throw new RegisterNotFoundException("Idioma con código: " + ageIdiomaSaveDAO.Codigo + " no existe.");
            
            if (!string.IsNullOrWhiteSpace(ageIdiomaSaveDAO.Estado))
            {
                ageIdioma.Estado = ageIdiomaSaveDAO.Estado;
                ageIdioma.FechaEstado = DateTime.Now;
            }

            if (!string.IsNullOrWhiteSpace(ageIdiomaSaveDAO.Descripcion))
                ageIdioma.Descripcion = ageIdiomaSaveDAO.Descripcion;

            if (!string.IsNullOrWhiteSpace(ageIdiomaSaveDAO.ObservacionEstado))
                ageIdioma.ObservacionEstado = ageIdiomaSaveDAO.ObservacionEstado;

            ageIdioma.FechaModificacion = DateTime.Now;
            ageIdioma.UbicacionModificacion = Ubicacion.getIpAdress(_httpContextAccessor);
            ageIdioma.UsuarioModificacion = ageIdiomaSaveDAO.UsuarioModificacion;

            Validator.ValidateObject(ageIdioma, new ValidationContext(ageIdioma), true);

            return ageIdioma;
        }

        private AgeIdiomas FromDAOToEntity(
            IHttpContextAccessor _httpContextAccessor,
            AgeIdiomasSaveDAO ageIdiomaSaveDAO)
        {

            return new AgeIdiomas
            {
                Codigo = ageIdiomaSaveDAO.Codigo,
                Descripcion = ageIdiomaSaveDAO.Descripcion,
                Estado = ageIdiomaSaveDAO.Estado,
                FechaEstado = DateTime.Now,
                FechaIngreso = DateTime.Now,
                ObservacionEstado = ageIdiomaSaveDAO.ObservacionEstado,
                UbicacionIngreso = Ubicacion.getIpAdress(_httpContextAccessor),
                UsuarioIngreso = ageIdiomaSaveDAO.UsuarioIngreso
            };
        }

        private void ValidarPK(int codigoIdioma)
        {
            ValidateKeys.ValidarNoExistenciaKey(codigoIdioma,
                $"Idioma con código: {codigoIdioma} ya existe.",
                _repository.ConsultarPorId);
        }
    }
}

