﻿using AGE.Entities;
using AGE.Entities.DAO.AgeIdioma;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AGE.Services.AgeIdiomaService
{
    public interface IAgeIdiomasService
    {

        Task<Page<AgeIdiomasDAO>> ConsultarTodos(
            int codigo,
            string descripcion,
            Pageable pageable);

        Task<Page<AgeIdiomasDAO>> ConsultarListaFiltro(
            string filtro, 
            Pageable pageable);

        Task<AgeIdiomasDAO> ConsultarPorId(
            int codigo);

        Task<AgeIdiomas> ConsultarCompletePorId(
            int codigo);

        Task<Resource<AgeIdiomasDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor, 
            AgeIdiomasSaveDAO ageIdiomaSaveDAO);

        Task<List<Resource<AgeIdiomasDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor, 
            List<AgeIdiomasSaveDAO> ageIdiomaSaveDAOList);

        Task<AgeIdiomasDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeIdiomasSaveDAO ageIdiomaSaveDAO);

        Task<List<AgeIdiomasDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor, 
            List<AgeIdiomasSaveDAO> ageIdiomaSaveDAOList);
    }
}
