﻿using AGE.Controllers;
using AGE.Entities.DAO.AgePuntosEmision;
using AGE.Entities;
using AGE.Middleware.Exceptions.BadRequest;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Repositories.AgePuntosEmisionRepository;
using AGE.Services.AgeLicenciatariosAplicaSecuService;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using AGE.Utils;
using System.ComponentModel.DataAnnotations;
using AGE.Entities.DAO.AgeSucursales;
using AGE.Repositories.AgeSucursalesRepository;
using System.Transactions;

namespace AGE.Services.AgePuntosEmisionService
{
    public class AgePuntosEmisionesService : IAgePuntosEmisionesService
    {
        private readonly IAgePuntosEmisionesRepository _repository;
        private readonly IAgeSucursalesRepository _repositorySucursales;

        private readonly IAgeLicenciatariosAplicaSecuService _ageLicenciatariosAplicaSecuService;


        public AgePuntosEmisionesService(
            IAgePuntosEmisionesRepository Repository,
            IAgeLicenciatariosAplicaSecuService ageLicenciatariosAplicaSecuService,
            IAgeSucursalesRepository repositorySucursales)
        {
            _repository = Repository;
            _ageLicenciatariosAplicaSecuService = ageLicenciatariosAplicaSecuService;
            _repositorySucursales = repositorySucursales;
        }


        public Task<Page<AgePuntosEmisionesDAO>> ConsultarTodos(
            int codigoLicenciatario,
            int? codigo,
            int? ageSucursCodigo,
            Pageable pageable)
        {
            if (codigoLicenciatario <= 0)
                throw new InvalidFieldException("El código de licenciatario debe ser un número entero mayor a cero.");

            pageable.Validar<AgePuntosEmisionesDAO>();

            return _repository.ConsultarTodos(codigoLicenciatario, codigo ?? 0, ageSucursCodigo ?? 0, pageable);
        }


        public Task<Page<AgePuntosEmisionesDAO>> ConsultarListaFiltro(
            int codigoLicenciatario,
            string filtro,
            Pageable pageable)
        {
            if (codigoLicenciatario == 0)
                throw new InvalidFieldException("El código de licenciatario debe ser un número entero mayor a cero.");

            if (string.IsNullOrWhiteSpace(filtro))
                throw new InvalidFieldException("El filtro es requerido.");

            pageable.Validar<AgePuntosEmisionesDAO>();

            return _repository.ConsultarListaFiltro(codigoLicenciatario, filtro.ToLower(), pageable);
        }


        public Task<AgePuntosEmisionesDAO> ConsultarPorId(
            AgePuntosEmisionesPKDAO agePuntosEmisionPKDAO)
        {
            Validator.ValidateObject(agePuntosEmisionPKDAO, new ValidationContext(agePuntosEmisionPKDAO), true);

            AgePuntosEmisionesDAO agePuntosEmisionDAO = _repository.ConsultarPorId(agePuntosEmisionPKDAO).Result;

            if (agePuntosEmisionDAO == null)
                throw new RegisterNotFoundException("Punto de emisión con código " + agePuntosEmisionPKDAO.codigo + " no existe");

            return Task.FromResult(agePuntosEmisionDAO);
        }


        private AgePuntosEmisiones ConsultarCompletePorId(
            AgePuntosEmisionesPKDAO agePuntosEmisionDAO)
        {
            Validator.ValidateObject(agePuntosEmisionDAO, new ValidationContext(agePuntosEmisionDAO), true);

            AgePuntosEmisiones? agePuntosEmision = _repository.ConsultarCompletePorId(agePuntosEmisionDAO).Result;

            return agePuntosEmision;
        }

        public async Task<Resource<AgePuntosEmisionesDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgePuntosEmisionesSaveDAO agePuntosEmisionSaveDAO)
        {
            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    AgePuntosEmisiones agePuntosEmision = ValidarInsert(_httpContextAccessor, agePuntosEmisionSaveDAO);

                    AgePuntosEmisionesDAO agePuntosEmisionDAO = await _repository.Insertar(agePuntosEmision);

                    Resource<AgePuntosEmisionesDAO> agePuntosEmisionDAOWithResource = GetDataWithResource(_httpContextAccessor, agePuntosEmisionDAO);

                    transactionScope.Complete();

                    return agePuntosEmisionDAOWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public async Task<List<Resource<AgePuntosEmisionesDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgePuntosEmisionesSaveDAO> agePuntosEmisionSaveDAOList)
        {
            if (agePuntosEmisionSaveDAOList == null)
            {
                throw new InvalidSintaxisException();
            }

            List<AgePuntosEmisiones> agePuntosEmisionList = new List<AgePuntosEmisiones>();

            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    foreach (AgePuntosEmisionesSaveDAO agePuntosEmisionSaveDAO in agePuntosEmisionSaveDAOList)
                    {
                        AgePuntosEmisiones agePuntosEmision = ValidarInsert(_httpContextAccessor, agePuntosEmisionSaveDAO);
                        agePuntosEmisionList.Add(agePuntosEmision);
                    }

                    List<AgePuntosEmisionesDAO> agePuntosEmisionDAOList = await _repository.InsertarVarios(agePuntosEmisionList);

                    List<Resource<AgePuntosEmisionesDAO>> agePuntosEmisionDAOListWithResource = new List<Resource<AgePuntosEmisionesDAO>>();

                    foreach (AgePuntosEmisionesDAO agePuntosEmisionDAO in agePuntosEmisionDAOList)
                    {
                        agePuntosEmisionDAOListWithResource.Add(GetDataWithResource(_httpContextAccessor, agePuntosEmisionDAO));
                    }

                    transactionScope.Complete();

                    return agePuntosEmisionDAOListWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public Task<AgePuntosEmisionesDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgePuntosEmisionesSaveDAO agePuntosEmisionSaveDAO)
        {
            AgePuntosEmisiones agePuntosEmision = ValidarUpdate(_httpContextAccessor, agePuntosEmisionSaveDAO);

            return _repository.Actualizar(agePuntosEmision);
        }

        public Task<List<AgePuntosEmisionesDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgePuntosEmisionesSaveDAO> agePuntosEmisionSaveDAOList)
        {
            if (agePuntosEmisionSaveDAOList == null)
                throw new InvalidSintaxisException();

            ValidateKeys.ValidarPKDuplicadas(agePuntosEmisionSaveDAOList,
                                             p => new { p.Id.ageSucursAgeLicencCodigo, p.Id.ageSucursCodigo, p.Id.codigo },
                                             "Id");

            List<AgePuntosEmisiones> agePuntosEmisionList = new List<AgePuntosEmisiones>();
            AgePuntosEmisiones agePuntosEmision;

            foreach (AgePuntosEmisionesSaveDAO agePuntosEmisionSaveDAO in agePuntosEmisionSaveDAOList)
            {
                agePuntosEmision = ValidarUpdate(_httpContextAccessor, agePuntosEmisionSaveDAO);
                agePuntosEmisionList.Add(agePuntosEmision);
            }

            return _repository.ActualizarVarios(agePuntosEmisionList);
        }


        private static Resource<AgePuntosEmisionesDAO> GetDataWithResource(
            IHttpContextAccessor _httpContextAccessor,
            AgePuntosEmisionesDAO agePuntosEmisionDAO)
        {
            string tpuntoemisionSegmentoFinal = $"{agePuntosEmisionDAO.Id.ageSucursAgeLicencCodigo}/" +
                $"{agePuntosEmisionDAO.Id.ageSucursCodigo}/{agePuntosEmisionDAO.Id.codigo}";

            return Resource<AgePuntosEmisionesDAO>.GetDataWithResource<AgePuntosEmisionesController>(
                _httpContextAccessor, tpuntoemisionSegmentoFinal, agePuntosEmisionDAO);
        }

        private AgePuntosEmisiones ValidarInsert(
            IHttpContextAccessor _httpContextAccessor,
            AgePuntosEmisionesSaveDAO agePuntosEmisionSaveDAO)
        {

            if (agePuntosEmisionSaveDAO == null)
                throw new InvalidSintaxisException();

            Validator.ValidateObject(agePuntosEmisionSaveDAO, new ValidationContext(agePuntosEmisionSaveDAO), true);

            FuncionesSecuencias fg = new FuncionesSecuencias(_ageLicenciatariosAplicaSecuService);

            agePuntosEmisionSaveDAO.Id.codigo = fg.ObtenerSecuencia(_httpContextAccessor,
                                                    agePuntosEmisionSaveDAO.Id.ageSucursAgeLicencCodigo,
                                                    Globales.CODIGO_SECUENCIA_PUNTO_EMISION,
                                                    agePuntosEmisionSaveDAO.UsuarioIngreso);

            ValidarKeys(agePuntosEmisionSaveDAO);

            AgePuntosEmisiones entityObject = FromDAOToEntity(_httpContextAccessor, agePuntosEmisionSaveDAO);

            Validator.ValidateObject(entityObject, new ValidationContext(entityObject), true);

            return entityObject;
        }


        private AgePuntosEmisiones ValidarUpdate(
            IHttpContextAccessor _httpContextAccessor,
            AgePuntosEmisionesSaveDAO agePuntosEmisionSaveDAO)
        {
            if (agePuntosEmisionSaveDAO == null)
                throw new InvalidSintaxisException();

            if (agePuntosEmisionSaveDAO.UsuarioModificacion == null || agePuntosEmisionSaveDAO.UsuarioModificacion <= 0)
                throw new InvalidFieldException("El usuario de modificación debe ser un número entero mayor a cero.");

            AgePuntosEmisiones? agePuntosEmision = ConsultarCompletePorId(new AgePuntosEmisionesPKDAO
            {
                ageSucursAgeLicencCodigo = agePuntosEmisionSaveDAO.Id.ageSucursAgeLicencCodigo,
                codigo = agePuntosEmisionSaveDAO.Id.codigo,
                ageSucursCodigo = agePuntosEmisionSaveDAO.Id.ageSucursCodigo
            });

            if (agePuntosEmision == null)
                throw new RegisterNotFoundException("Punto de Emisión con código " + agePuntosEmisionSaveDAO.Id.codigo + " no existe");

            if (!string.IsNullOrWhiteSpace(agePuntosEmisionSaveDAO.Estado))
            {
                agePuntosEmision.Estado = agePuntosEmisionSaveDAO.Estado;
                agePuntosEmision.FechaEstado = DateTime.Now;
            }

            if (!string.IsNullOrWhiteSpace(agePuntosEmisionSaveDAO.Descripcion))
                agePuntosEmision.Descripcion = agePuntosEmisionSaveDAO.Descripcion;

            if (!string.IsNullOrWhiteSpace(agePuntosEmisionSaveDAO.ObservacionEstado))
                agePuntosEmision.ObservacionEstado = agePuntosEmisionSaveDAO.ObservacionEstado;

            agePuntosEmision.FechaModificacion = DateTime.Now;
            agePuntosEmision.UbicacionModificacion = Ubicacion.getIpAdress(_httpContextAccessor);
            agePuntosEmision.UsuarioModificacion = agePuntosEmisionSaveDAO.UsuarioModificacion;

            Validator.ValidateObject(agePuntosEmision, new ValidationContext(agePuntosEmision), true);

            return agePuntosEmision;
        }


        private void ValidarKeys(AgePuntosEmisionesSaveDAO agePuntosEmisionSaveDAO)
        {
            ValidarPK(agePuntosEmisionSaveDAO.Id);

            ValidarFKSucursal(agePuntosEmisionSaveDAO.Id.ageSucursAgeLicencCodigo,
                                agePuntosEmisionSaveDAO.Id.ageSucursCodigo);

        }


        private void ValidarPK(AgePuntosEmisionesPKDAO agePuntosEmisionPKDAO)
        {

            ValidateKeys.ValidarNoExistenciaKey(
               agePuntosEmisionPKDAO,
               $"Punto emisión con código {agePuntosEmisionPKDAO.codigo} ya existe.",
               _repository.ConsultarCompletePorId);
        }


        private void ValidarFKSucursal(int codigoSucursal, int codigoLicenciatario)
        {
            AgeSucursalesPKDAO ageSucursalesPKDAO = new()
            {
                Codigo = codigoSucursal,
                AgeLicencCodigo = codigoLicenciatario
            };

            ValidateKeys.ValidarExistenciaKey(
                ageSucursalesPKDAO,
                $"Sucursal con código: {ageSucursalesPKDAO.Codigo} y código licenciatario: {ageSucursalesPKDAO.AgeLicencCodigo} no existe.",
                _repositorySucursales.ConsultarCompletePorId);
        }


        private AgePuntosEmisiones FromDAOToEntity(
            IHttpContextAccessor _httpContextAccessor,
            AgePuntosEmisionesSaveDAO agePuntosEmisionSaveDAO)
        {
            return new AgePuntosEmisiones
            {
                AgeSucursAgeLicencCodigo = agePuntosEmisionSaveDAO.Id.ageSucursAgeLicencCodigo,
                Codigo = agePuntosEmisionSaveDAO.Id.codigo,
                AgeSucursCodigo = agePuntosEmisionSaveDAO.Id.ageSucursCodigo,
                Descripcion = agePuntosEmisionSaveDAO.Descripcion,
                Estado = agePuntosEmisionSaveDAO.Estado,
                FechaEstado = DateTime.Now,
                FechaIngreso = DateTime.Now,
                ObservacionEstado = agePuntosEmisionSaveDAO.ObservacionEstado,
                UbicacionIngreso = Ubicacion.getIpAdress(_httpContextAccessor),
                UsuarioIngreso = agePuntosEmisionSaveDAO.UsuarioIngreso
            };
        }
    }
}
