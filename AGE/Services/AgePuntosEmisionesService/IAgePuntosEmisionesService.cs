﻿using AGE.Entities.DAO.AgePuntosEmision;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;

namespace AGE.Services.AgePuntosEmisionService
{
    public interface IAgePuntosEmisionesService
    {
        Task<Page<AgePuntosEmisionesDAO>> ConsultarTodos(
            int codigoLicenciatario,
            int? codigo,
            int? ageSucursCodigo,
            Pageable pageable);

        Task<Page<AgePuntosEmisionesDAO>> ConsultarListaFiltro(
            int codigoLicenciatario,
            string filtro,
            Pageable pageable);

        Task<AgePuntosEmisionesDAO> ConsultarPorId(AgePuntosEmisionesPKDAO agePuntosEmisionPKDAO);

        Task<Resource<AgePuntosEmisionesDAO>> Insertar(
            IHttpContextAccessor httpContextAccessor,
            AgePuntosEmisionesSaveDAO agePuntosEmisionSaveDAO);

        Task<List<Resource<AgePuntosEmisionesDAO>>> InsertarVarios(
            IHttpContextAccessor httpContextAccessor,
            List<AgePuntosEmisionesSaveDAO> agePuntosEmisionSaveDAOList);

        Task<AgePuntosEmisionesDAO> Actualizar(
            IHttpContextAccessor httpContextAccessor,
            AgePuntosEmisionesSaveDAO agePuntosEmisionSaveDAO);

        Task<List<AgePuntosEmisionesDAO>> ActualizarVarios(
            IHttpContextAccessor httpContextAccessor,
            List<AgePuntosEmisionesSaveDAO> agePuntosEmisionSaveDAOList);
    }
}
