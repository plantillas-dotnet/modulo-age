﻿using AGE.Controllers;
using AGE.Entities;
using AGE.Entities.DAO.AgeUsuarioParamVigencia;
using AGE.Middleware.Exceptions.BadRequest;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Repositories.AgeUsuarioParamVigenciaRepository;
using AGE.Services.AgeLicenciatariosAplicaSecuService;
using AGE.Utils;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using System.ComponentModel.DataAnnotations;
using System.Transactions;
using AGE.Repositories.AgeLicenciatariosRepository;
using AGE.Repositories.AgeParametrosGeneralesRepository;
using AGE.Repositories.AgeUsuarioRepository;
using AGE.Entities.DAO.AgeUsuarios;

namespace AGE.Services.AgeUsuarioParamVigenciaService
{
    public class AgeUsuarioParamVigenciasService : IAgeUsuarioParamVigenciasService
    {
        private readonly IAgeUsuarioParamVigenciasRepository _repository;
        private readonly IAgeLicenciatariosAplicaSecuService _ageLicenciatariosAplicaSecuService;
        private readonly IAgeLicenciatariosRepository _repositoryLicenciatario;
        private readonly IAgeUsuariosRepository _repositoryUsuari;
        private readonly IAgeParametrosGeneralesRepository _repositoryParametroGeneral;

        public AgeUsuarioParamVigenciasService(
            IAgeUsuarioParamVigenciasRepository Repository,
            IAgeLicenciatariosAplicaSecuService ageLicenciatariosAplicaSecuService,
            IAgeLicenciatariosRepository repositoryLicenciatario,
            IAgeUsuariosRepository repositoryUsuari,
            IAgeParametrosGeneralesRepository repositoryParametroGeneral)
        {
            _repository = Repository;
            _ageLicenciatariosAplicaSecuService = ageLicenciatariosAplicaSecuService;
            _repositoryLicenciatario = repositoryLicenciatario;
            _repositoryUsuari = repositoryUsuari;
            _repositoryParametroGeneral = repositoryParametroGeneral;
        }


        public Task<Page<AgeUsuarioParamVigenciaDAO>> ConsultarTodos(
            int codigoLicenciatario,
            Pageable pageable)
        {

            pageable.Validar<AgeUsuarioParamVigenciaDAO>();

            return _repository.ConsultarTodos(codigoLicenciatario, pageable);
        }


        public Task<Page<AgeUsuarioParamVigenciaDAO>> ConsultarListaFiltro(
        int codigoLicenciatario,
        string filtro,
        Pageable pageable)
        {
            if (codigoLicenciatario <= 0) {
                throw new InvalidFieldException("El código licenciatario es requerido.");
            }

            if (string.IsNullOrWhiteSpace(filtro))
            {
                throw new InvalidFieldException("El filtro es requerido.");
            }

            pageable.Validar<AgeUsuarioParamVigenciaDAO>();

            return _repository.ConsultarListaFiltro(codigoLicenciatario, filtro.ToLower(), pageable);
        }

        public Task<AgeUsuarioParamVigenciaDAO> ConsultarPorId(
            AgeUsuarioParamVigenciaPKDAO ageUsuarioParamVigenciaPKDAO)
        {
            Validator.ValidateObject(ageUsuarioParamVigenciaPKDAO, new ValidationContext(ageUsuarioParamVigenciaPKDAO), true);

            AgeUsuarioParamVigenciaDAO? ageUsuarioParamVigenciaDAO = _repository.ConsultarPorId(ageUsuarioParamVigenciaPKDAO).Result;

            if (ageUsuarioParamVigenciaDAO == null)
            {
                throw new RegisterNotFoundException("Parámetro con código " + ageUsuarioParamVigenciaPKDAO.Codigo + " no existe");
            }

            return Task.FromResult(ageUsuarioParamVigenciaDAO);
        }

        public AgeUsuarioParamVigencias ConsultarCompletePorId(AgeUsuarioParamVigenciaPKDAO ageUsuarioParamVigenciaPKDAO)
        {
            Validator.ValidateObject(ageUsuarioParamVigenciaPKDAO, new ValidationContext(ageUsuarioParamVigenciaPKDAO), true);

            AgeUsuarioParamVigencias ageUsuarioParamVigencia = _repository.ConsultarCompletePorId(ageUsuarioParamVigenciaPKDAO).Result;

            return ageUsuarioParamVigencia;
        }


        public async Task<Resource<AgeUsuarioParamVigenciaDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgeUsuarioParamVigenciaSaveDAO ageUsuarioParamVigenciaSaveDAO)
        {
            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    AgeUsuarioParamVigencias ageUsuarioParamVigencia = ValidarInsert(_httpContextAccessor, ageUsuarioParamVigenciaSaveDAO);

                    AgeUsuarioParamVigenciaDAO ageUsuarioParamVigenciaDAO = await _repository.Insertar(ageUsuarioParamVigencia);

                    Resource<AgeUsuarioParamVigenciaDAO> ageUsuarioParamVigenciaDAOWithResource = GetDataWithResource(_httpContextAccessor, ageUsuarioParamVigenciaDAO);

                    transactionScope.Complete();

                    return ageUsuarioParamVigenciaDAOWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public async Task<List<Resource<AgeUsuarioParamVigenciaDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeUsuarioParamVigenciaSaveDAO> ageUsuarioParamVigenciaSaveDAOList)
        {
            if (ageUsuarioParamVigenciaSaveDAOList == null)
            {
                throw new InvalidSintaxisException();
            }

            List<AgeUsuarioParamVigencias> ageUsuarioParamVigenciaList = new List<AgeUsuarioParamVigencias>();

            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    foreach (AgeUsuarioParamVigenciaSaveDAO ageUsuarioParamVigenciaSaveDAO in ageUsuarioParamVigenciaSaveDAOList)
                    {
                        AgeUsuarioParamVigencias ageUsuarioParamVigencia = ValidarInsert(_httpContextAccessor, ageUsuarioParamVigenciaSaveDAO);
                        ageUsuarioParamVigenciaList.Add(ageUsuarioParamVigencia);
                    }

                    List<AgeUsuarioParamVigenciaDAO> ageUsuarioParamVigenciaDAOList = await _repository.InsertarVarios(ageUsuarioParamVigenciaList);

                    List<Resource<AgeUsuarioParamVigenciaDAO>> ageUsuarioParamVigenciaDAOListWithResource = new List<Resource<AgeUsuarioParamVigenciaDAO>>();

                    foreach (AgeUsuarioParamVigenciaDAO ageUsuarioParamVigenciaDAO in ageUsuarioParamVigenciaDAOList)
                    {
                        ageUsuarioParamVigenciaDAOListWithResource.Add(GetDataWithResource(_httpContextAccessor, ageUsuarioParamVigenciaDAO));
                    }

                    transactionScope.Complete();

                    return ageUsuarioParamVigenciaDAOListWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }


        public Task<AgeUsuarioParamVigenciaDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeUsuarioParamVigenciaSaveDAO ageUsuarioParamVigenciaSaveDAO)
        {
            AgeUsuarioParamVigencias ageUsuarioParamVigencia = ValidarUpdate(_httpContextAccessor, ageUsuarioParamVigenciaSaveDAO);

            return _repository.Actualizar(ageUsuarioParamVigencia);
        }


        public Task<List<AgeUsuarioParamVigenciaDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeUsuarioParamVigenciaSaveDAO> ageUsuarioParamVigenciaSaveDAOList)
        {
            if (ageUsuarioParamVigenciaSaveDAOList == null)
                throw new InvalidSintaxisException();

            ValidateKeys.ValidarPKDuplicadas(ageUsuarioParamVigenciaSaveDAOList,
                                             p => new { p.Id.AgeUsuariAgeLicencCodigo, 
                                             p.Id.AgeParGeCodigo, p.Id.AgeUsuariCodigo, p.Id.Codigo },
                                             "Id");

            List<AgeUsuarioParamVigencias> ageUsuarioParamVigenciaList = new List<AgeUsuarioParamVigencias>();
            AgeUsuarioParamVigencias ageUsuarioParamVigencia;

            foreach (AgeUsuarioParamVigenciaSaveDAO ageUsuarioParamVigenciaSaveDAO in ageUsuarioParamVigenciaSaveDAOList)
            {
                ageUsuarioParamVigencia = ValidarUpdate(_httpContextAccessor, ageUsuarioParamVigenciaSaveDAO);
                ageUsuarioParamVigenciaList.Add(ageUsuarioParamVigencia);
            }

            return _repository.ActualizarVarios(ageUsuarioParamVigenciaList);
        }


        private static Resource<AgeUsuarioParamVigenciaDAO> GetDataWithResource(
            IHttpContextAccessor _httpContextAccessor,
            AgeUsuarioParamVigenciaDAO ageUsuarioParamVigenciaDAO)
        {
            string idtipoSegmentoFinal = $"{ageUsuarioParamVigenciaDAO.Id.AgeUsuariAgeLicencCodigo}/" +
                $"{ageUsuarioParamVigenciaDAO.Id.AgeUsuariCodigo}/" +
                $"{ageUsuarioParamVigenciaDAO.Id.AgeParGeCodigo}/" +
                $"{ageUsuarioParamVigenciaDAO.Id.Codigo}";

            return Resource<AgeUsuarioParamVigenciaDAO>.GetDataWithResource<AgeUsuarioParamVigenciasController>(
                _httpContextAccessor, idtipoSegmentoFinal, ageUsuarioParamVigenciaDAO);
        }

        private AgeUsuarioParamVigencias ValidarInsert(
        IHttpContextAccessor _httpContextAccessor,
        AgeUsuarioParamVigenciaSaveDAO ageUsuarioParamVigenciaSaveDAO)
        {
            if (ageUsuarioParamVigenciaSaveDAO == null)
                throw new InvalidSintaxisException();

            

            Validator.ValidateObject(ageUsuarioParamVigenciaSaveDAO, new ValidationContext(ageUsuarioParamVigenciaSaveDAO), true);

            FuncionesSecuencias fg = new FuncionesSecuencias(_ageLicenciatariosAplicaSecuService);

            ageUsuarioParamVigenciaSaveDAO.Id.Codigo = fg.ObtenerSecuencia(
                                                        _httpContextAccessor,
                                                        ageUsuarioParamVigenciaSaveDAO.Id.AgeUsuariAgeLicencCodigo,
                                                        Globales.CODIGO_SECUENCIA_USUARIO_PARAM_VIGENCIA,
                                                        ageUsuarioParamVigenciaSaveDAO.UsuarioIngreso);
            ValidarKeys(ageUsuarioParamVigenciaSaveDAO);

            AgeUsuarioParamVigencias entityObject = FromDAOToEntity(_httpContextAccessor, ageUsuarioParamVigenciaSaveDAO);

            Validator.ValidateObject(entityObject, new ValidationContext(entityObject), true);

            return entityObject;
        }

        private AgeUsuarioParamVigencias ValidarUpdate(
            IHttpContextAccessor _httpContextAccessor,
            AgeUsuarioParamVigenciaSaveDAO ageUsuarioParamVigenciaSaveDAO)
        {
            if (ageUsuarioParamVigenciaSaveDAO == null)
                throw new InvalidSintaxisException();

            if (ageUsuarioParamVigenciaSaveDAO.UsuarioModificacion == null || ageUsuarioParamVigenciaSaveDAO.UsuarioModificacion <= 0)
                throw new InvalidFieldException("El usuario de modificación debe ser un número entero mayor a cero.");

            AgeUsuarioParamVigencias? ageUsuarioParamVigencia = ConsultarCompletePorId(new AgeUsuarioParamVigenciaPKDAO
            {
                AgeUsuariAgeLicencCodigo = ageUsuarioParamVigenciaSaveDAO.Id.AgeUsuariAgeLicencCodigo,
                AgeUsuariCodigo = ageUsuarioParamVigenciaSaveDAO.Id.AgeUsuariCodigo,
                AgeParGeCodigo = ageUsuarioParamVigenciaSaveDAO.Id.AgeParGeCodigo,
                Codigo = ageUsuarioParamVigenciaSaveDAO.Id.Codigo
            });

            ValidarFKs(ageUsuarioParamVigenciaSaveDAO);

            if (ageUsuarioParamVigencia == null)
                throw new RegisterNotFoundException("Parámetro con código " + ageUsuarioParamVigenciaSaveDAO.Id.Codigo + " no existe");

            if (!string.IsNullOrWhiteSpace(ageUsuarioParamVigenciaSaveDAO.Observacion))
                ageUsuarioParamVigencia.Observacion = ageUsuarioParamVigenciaSaveDAO.Observacion;

            if (ageUsuarioParamVigenciaSaveDAO.FechaDesde != default)
                ageUsuarioParamVigencia.FechaDesde = ageUsuarioParamVigenciaSaveDAO.FechaDesde;

            if (ageUsuarioParamVigenciaSaveDAO.FechaHasta.HasValue)
                ageUsuarioParamVigencia.FechaHasta = ageUsuarioParamVigenciaSaveDAO.FechaHasta;

            if (!string.IsNullOrWhiteSpace(ageUsuarioParamVigenciaSaveDAO.ValorParametro))
                ageUsuarioParamVigencia.ValorParametro = ageUsuarioParamVigenciaSaveDAO.ValorParametro;

            if (!string.IsNullOrWhiteSpace(ageUsuarioParamVigenciaSaveDAO.ObservacionEstado))
                ageUsuarioParamVigencia.ObservacionEstado = ageUsuarioParamVigenciaSaveDAO.ObservacionEstado;

            ageUsuarioParamVigencia.FechaModificacion = DateTime.Now;
            ageUsuarioParamVigencia.UbicacionModificacion = Ubicacion.getIpAdress(_httpContextAccessor);
            ageUsuarioParamVigencia.UsuarioModificacion = ageUsuarioParamVigenciaSaveDAO.UsuarioModificacion;

            Validator.ValidateObject(ageUsuarioParamVigencia, new ValidationContext(ageUsuarioParamVigencia), true);

            return ageUsuarioParamVigencia;
        }

        private AgeUsuarioParamVigencias FromDAOToEntity(
        IHttpContextAccessor _httpContextAccessor,
        AgeUsuarioParamVigenciaSaveDAO ageUsuarioParamVigenciaSaveDAO)
        {

            return new AgeUsuarioParamVigencias
            {
                AgeUsuariAgeLicencCodigo = ageUsuarioParamVigenciaSaveDAO.Id.AgeUsuariAgeLicencCodigo,
                AgeUsuariCodigo = ageUsuarioParamVigenciaSaveDAO.Id.AgeUsuariCodigo,
                AgeParGeCodigo = ageUsuarioParamVigenciaSaveDAO.Id.AgeParGeCodigo,
                Codigo = ageUsuarioParamVigenciaSaveDAO.Id.Codigo,
                Observacion = ageUsuarioParamVigenciaSaveDAO.Observacion,
                FechaDesde = ageUsuarioParamVigenciaSaveDAO.FechaDesde,
                FechaHasta = ageUsuarioParamVigenciaSaveDAO.FechaHasta,
                ValorParametro = ageUsuarioParamVigenciaSaveDAO.ValorParametro,
                Estado = ageUsuarioParamVigenciaSaveDAO.Estado,
                FechaEstado = DateTime.Now,
                FechaIngreso = DateTime.Now,
                ObservacionEstado = ageUsuarioParamVigenciaSaveDAO.ObservacionEstado,
                UbicacionIngreso = Ubicacion.getIpAdress(_httpContextAccessor),
                UsuarioIngreso = ageUsuarioParamVigenciaSaveDAO.UsuarioIngreso
            };
        }

        private void ValidarKeys(AgeUsuarioParamVigenciaSaveDAO ageUsuarioParamVigenciaSaveDAO)
        {
            ValidarPK(ageUsuarioParamVigenciaSaveDAO.Id);
            ValidarFKs(ageUsuarioParamVigenciaSaveDAO);
        }

        private void ValidarPK(AgeUsuarioParamVigenciaPKDAO ageUsuarioParamVigenciaPKDAO)
        {
            ValidateKeys.ValidarNoExistenciaKey(
               ageUsuarioParamVigenciaPKDAO,
               $"Usuario Parámetro Vigencia con código {ageUsuarioParamVigenciaPKDAO.Codigo} ya existe.",
               _repository.ConsultarCompletePorId);
        }

        private void ValidarFKs(AgeUsuarioParamVigenciaSaveDAO ageUsuarioParamVigenciaSaveDAO)
        {
            ValidarFKLicenciatario(ageUsuarioParamVigenciaSaveDAO.Id.AgeUsuariAgeLicencCodigo);
            ValidarFKUsuario(ageUsuarioParamVigenciaSaveDAO);
            ValidarFKParametroGeneral(ageUsuarioParamVigenciaSaveDAO.Id.AgeParGeCodigo);
        }

        private void ValidarFKLicenciatario(int codigoLicenciatario)
        {
            ValidateKeys.ValidarExistenciaKey(codigoLicenciatario,
                $"Licenciatario aplicación con código: {codigoLicenciatario} no existe."
                , _repositoryLicenciatario.ConsultarCompletePorId);
        }

        private void ValidarFKUsuario(AgeUsuarioParamVigenciaSaveDAO ageUsuarioParamVigenciaSaveDAO)
        {
            AgeUsuarioPKDAO ageUsuarioPKDAO = new AgeUsuarioPKDAO()
            {
                ageLicencCodigo = ageUsuarioParamVigenciaSaveDAO.Id.AgeUsuariAgeLicencCodigo,
                codigo = ageUsuarioParamVigenciaSaveDAO.Id.AgeUsuariCodigo
            };

            ValidateKeys.ValidarExistenciaKey(
                ageUsuarioPKDAO,
                $"Usuario con código: {ageUsuarioPKDAO.codigo} " +
                $"y código licenciatario: {ageUsuarioPKDAO.ageLicencCodigo} no existe.",
                _repositoryUsuari.ConsultarCompletePorId);
        }

        private void ValidarFKParametroGeneral(int codigoParametroGeneral)
        {
            ValidateKeys.ValidarExistenciaKey(codigoParametroGeneral,
                               $"Parámetro general con código: {codigoParametroGeneral} no existe."
                                              , _repositoryParametroGeneral.ConsultarCompletePorId);
        }

    }
}

