﻿using AGE.Entities;
using AGE.Entities.DAO.AgeUsuarioParamVigencia;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AGE.Services.AgeUsuarioParamVigenciaService
{
    public interface IAgeUsuarioParamVigenciasService
    {
        Task<Page<AgeUsuarioParamVigenciaDAO>> ConsultarTodos(
            int codigoLicenciatario,
            Pageable pageable);

        Task<Page<AgeUsuarioParamVigenciaDAO>> ConsultarListaFiltro(
            int codigoLicenciatario,
            string filtro,
            Pageable pageable);

        Task<AgeUsuarioParamVigenciaDAO> ConsultarPorId(AgeUsuarioParamVigenciaPKDAO ageUsuarioParamVigenciaPKDAO);

        AgeUsuarioParamVigencias ConsultarCompletePorId(AgeUsuarioParamVigenciaPKDAO ageUsuarioParamVigenciasPKDAO);

        Task<Resource<AgeUsuarioParamVigenciaDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgeUsuarioParamVigenciaSaveDAO ageUsuarioParamVigenciaSaveDAO);

        Task<List<Resource<AgeUsuarioParamVigenciaDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeUsuarioParamVigenciaSaveDAO> ageUsuarioParamVigenciaSaveDAOList);

        Task<AgeUsuarioParamVigenciaDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeUsuarioParamVigenciaSaveDAO ageUsuarioParamVigenciaSaveDAO);

        Task<List<AgeUsuarioParamVigenciaDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeUsuarioParamVigenciaSaveDAO> ageUsuarioParamVigenciaSaveDAOList);
    }
}
