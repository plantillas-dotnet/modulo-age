﻿using AGE.Entities.DAO.AgeParametrosGenerales;
using AGE.Entities;
using AGE.Middleware.Exceptions.BadRequest;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using System.Transactions;

namespace AGE.Services.AgeParametrosGeneralesService
{
    public interface IAgeParametrosGeneralesService
    {
       Task<Page<AgeParametrosGeneralesDAO>> ConsultarTodos(
            int codigo,
            string descripcion,
            Pageable pageable);

        Task<Page<AgeParametrosGeneralesDAO>> ConsultarListaFiltro(string filtro, Pageable pageable);
        Task<AgeParametrosGeneralesDAO> ConsultarPorId(
            int codigo);

        AgeParametrosGenerales ConsultarCompletePorId(
            int codigo);
        Task<Resource<AgeParametrosGeneralesDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgeParametrosGeneralesSaveDAO ageParametrosGeneralesSaveDAO);
        Task<AgeParametrosGeneralesDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeParametrosGeneralesSaveDAO ageParametrosGeneralesSaveDAO);
        Task<List<Resource<AgeParametrosGeneralesDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeParametrosGeneralesSaveDAO> ageParametrosGeneralesSaveDAOList);
        Task<List<AgeParametrosGeneralesDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeParametrosGeneralesSaveDAO> ageParametrosGeneralesSaveDAOList);
    }
}
