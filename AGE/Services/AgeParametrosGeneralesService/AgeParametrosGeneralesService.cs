﻿using AGE.Controllers;
using AGE.Entities;
using AGE.Entities.DAO.AgeParametrosGenerales;
using AGE.Middleware.Exceptions.BadRequest;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Repositories.AgeParametrosGeneralesRepository;
using AGE.Services.AgeSecuenciasPrimariasService;
using AGE.Utils;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using System.ComponentModel.DataAnnotations;
using System.Transactions;

namespace AGE.Services.AgeParametrosGeneralesService
{
    public class AgeParametrosGeneralesService : IAgeParametrosGeneralesService
    {

        private readonly IAgeParametrosGeneralesRepository _repository;
        private readonly IAgeSecuenciasPrimariasService _ageSecuenciasPrimariasService;


        public AgeParametrosGeneralesService(
            IAgeParametrosGeneralesRepository Repository,
            IAgeSecuenciasPrimariasService ageSecuenciasPrimariasService)
        {
            _repository = Repository;
            _ageSecuenciasPrimariasService = ageSecuenciasPrimariasService;
        }


        public Task<Page<AgeParametrosGeneralesDAO>> ConsultarTodos(
            int codigo,
            string descripcion,
            Pageable pageable)
        {
            pageable.Validar<AgeParametrosGeneralesDAO>();

            return _repository.ConsultarTodos(codigo, descripcion, pageable);
        }


        public Task<Page<AgeParametrosGeneralesDAO>> ConsultarListaFiltro(string filtro, Pageable pageable)
        {

            if (string.IsNullOrWhiteSpace(filtro))
            {
                throw new InvalidFieldException("El filtro es requerido.");
            }

            pageable.Validar<AgeParametrosGeneralesDAO>();

            return _repository.ConsultarListaFiltro(filtro, pageable);

        }

        public Task<AgeParametrosGeneralesDAO> ConsultarPorId(
            int codigo)
        {

            if (codigo <= 0)
            {
                throw new InvalidIdException();
            }

            AgeParametrosGeneralesDAO? ageParametrosGeneralesDAO = _repository.ConsultarPorId(codigo).Result;

            if (ageParametrosGeneralesDAO == null)
            {
                throw new RegisterNotFoundException("Parametro General con código " + codigo + " no existe");
            }

            return Task.FromResult(ageParametrosGeneralesDAO);
        }


        public AgeParametrosGenerales ConsultarCompletePorId(
            int codigo)
        {

            if (codigo <= 0)
            {
                throw new InvalidIdException();
            }

            return _repository.ConsultarCompletePorId(codigo).Result;
        }
        public async Task<Resource<AgeParametrosGeneralesDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgeParametrosGeneralesSaveDAO ageParametrosGeneralesSaveDAO)
        {
            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    AgeParametrosGenerales ageParametrosGenerales = ValidarInsert(_httpContextAccessor, ageParametrosGeneralesSaveDAO);

                    AgeParametrosGeneralesDAO ageParametrosGeneralesDAO = await _repository.Insertar(ageParametrosGenerales);

                    Resource<AgeParametrosGeneralesDAO> ageParametrosGeneralesDAOWithResource = GetDataWithResource(_httpContextAccessor, ageParametrosGeneralesDAO);

                    transactionScope.Complete();

                    return ageParametrosGeneralesDAOWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public async Task<List<Resource<AgeParametrosGeneralesDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeParametrosGeneralesSaveDAO> ageParametrosGeneralesSaveDAOList)
        {
            if (ageParametrosGeneralesSaveDAOList == null)
            {
                throw new InvalidSintaxisException();
            }

            List<AgeParametrosGenerales> ageParametrosGeneralesList = new List<AgeParametrosGenerales>();

            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    foreach (AgeParametrosGeneralesSaveDAO ageParametrosGeneralesSaveDAO in ageParametrosGeneralesSaveDAOList)
                    {
                        AgeParametrosGenerales ageParametrosGenerales = ValidarInsert(_httpContextAccessor, ageParametrosGeneralesSaveDAO);
                        ageParametrosGeneralesList.Add(ageParametrosGenerales);
                    }

                    List<AgeParametrosGeneralesDAO> ageParametrosGeneralesDAOList = await _repository.InsertarVarios(ageParametrosGeneralesList);

                    List<Resource<AgeParametrosGeneralesDAO>> ageParametrosGeneralesDAOListWithResource = new List<Resource<AgeParametrosGeneralesDAO>>();

                    foreach (AgeParametrosGeneralesDAO ageParametrosGeneralesDAO in ageParametrosGeneralesDAOList)
                    {
                        ageParametrosGeneralesDAOListWithResource.Add(GetDataWithResource(_httpContextAccessor, ageParametrosGeneralesDAO));
                    }

                    transactionScope.Complete();

                    return ageParametrosGeneralesDAOListWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public Task<AgeParametrosGeneralesDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeParametrosGeneralesSaveDAO ageParametrosGeneralesSaveDAO)
        {
            AgeParametrosGenerales ageParametrosGenerales = ValidarUpdate(_httpContextAccessor, ageParametrosGeneralesSaveDAO);

            return _repository.Actualizar(ageParametrosGenerales);
        }

        public Task<List<AgeParametrosGeneralesDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeParametrosGeneralesSaveDAO> ageParametrosGeneralesSaveDAOList)
        {
            if (ageParametrosGeneralesSaveDAOList == null)
                throw new InvalidSintaxisException();

            ValidateKeys.ValidarPKDuplicadas(ageParametrosGeneralesSaveDAOList,
                                             p => new { p.Codigo },
                                             "Id");

            List<AgeParametrosGenerales> ageParametrosGeneralesList = new List<AgeParametrosGenerales>();
            AgeParametrosGenerales ageParametrosGenerales;

            foreach (AgeParametrosGeneralesSaveDAO ageParametrosGeneralesSaveDAO in ageParametrosGeneralesSaveDAOList)
            {
                ageParametrosGenerales = ValidarUpdate(_httpContextAccessor, ageParametrosGeneralesSaveDAO);
                ageParametrosGeneralesList.Add(ageParametrosGenerales);
            }

            return _repository.ActualizarVarios(ageParametrosGeneralesList);
        }

        private static Resource<AgeParametrosGeneralesDAO> GetDataWithResource(
            IHttpContextAccessor _httpContextAccessor,
            AgeParametrosGeneralesDAO ageParametrosGeneralesDAO)
        {
            string rutaSegmentoFinal = $"{ageParametrosGeneralesDAO.Codigo}";

            return Resource<AgeParametrosGeneralesDAO>.GetDataWithResource<AgeParametroGeneralesController>(
                _httpContextAccessor, rutaSegmentoFinal, ageParametrosGeneralesDAO);
        }


        private AgeParametrosGenerales ValidarInsert(
            IHttpContextAccessor _httpContextAccessor,
            AgeParametrosGeneralesSaveDAO ageParametrosGeneralesSaveDAO)
        {
            if (ageParametrosGeneralesSaveDAO == null)
                throw new InvalidSintaxisException();

            Validator.ValidateObject(ageParametrosGeneralesSaveDAO, new ValidationContext(ageParametrosGeneralesSaveDAO), true);


            FuncionesSecuencias fg = new FuncionesSecuencias(_ageSecuenciasPrimariasService);

            ageParametrosGeneralesSaveDAO.Codigo = fg.ObtenerSecuenciaPrimaria(
                                                                                _httpContextAccessor,
                                                                                Globales.CODIGO_SECUENCIA_PARAMETRO_GENERAL,
                                                                                ageParametrosGeneralesSaveDAO.UsuarioIngreso);

            ValidarPK(ageParametrosGeneralesSaveDAO.Codigo);

            AgeParametrosGenerales entityObject = FromDAOToEntity(_httpContextAccessor, ageParametrosGeneralesSaveDAO);

            Validator.ValidateObject(entityObject, new ValidationContext(entityObject), true);

            return entityObject;
        }

        private AgeParametrosGenerales ValidarUpdate(
            IHttpContextAccessor _httpContextAccessor,
            AgeParametrosGeneralesSaveDAO ageParametrosGeneralesSaveDAO)
        {
            if (ageParametrosGeneralesSaveDAO == null)
                throw new InvalidSintaxisException();

            if (ageParametrosGeneralesSaveDAO.UsuarioModificacion == null || ageParametrosGeneralesSaveDAO.UsuarioModificacion <= 0)
                throw new InvalidFieldException("El usuario de modificación debe ser un número entero mayor a cero.");

            AgeParametrosGenerales ageParametrosGenerales = ConsultarCompletePorId(ageParametrosGeneralesSaveDAO.Codigo);

            if (ageParametrosGenerales == null)
                throw new RegisterNotFoundException($"Parámetro General con código {ageParametrosGeneralesSaveDAO.Codigo} no existe.");

            if (!string.IsNullOrWhiteSpace(ageParametrosGeneralesSaveDAO.Estado))
            {
                ageParametrosGenerales.Estado = ageParametrosGeneralesSaveDAO.Estado;
                ageParametrosGenerales.FechaEstado = DateTime.Now;
            }

            if (!string.IsNullOrWhiteSpace(ageParametrosGeneralesSaveDAO.TipoDatoParametro))
                ageParametrosGenerales.TipoDatoParametro = ageParametrosGeneralesSaveDAO.TipoDatoParametro;

            if (!string.IsNullOrWhiteSpace(ageParametrosGeneralesSaveDAO.Descripcion))
                ageParametrosGenerales.Descripcion = ageParametrosGeneralesSaveDAO.Descripcion;

            if (!string.IsNullOrWhiteSpace(ageParametrosGeneralesSaveDAO.ObservacionEstado))
                ageParametrosGenerales.ObservacionEstado = ageParametrosGeneralesSaveDAO.ObservacionEstado;

            ageParametrosGenerales.FechaModificacion = DateTime.Now;
            ageParametrosGenerales.UbicacionModificacion = Ubicacion.getIpAdress(_httpContextAccessor);
            ageParametrosGenerales.UsuarioModificacion = ageParametrosGeneralesSaveDAO.UsuarioModificacion;

            Validator.ValidateObject(ageParametrosGenerales, new ValidationContext(ageParametrosGenerales), true);

            return ageParametrosGenerales;
        }

        private AgeParametrosGenerales FromDAOToEntity(
        IHttpContextAccessor _httpContextAccessor,
        AgeParametrosGeneralesSaveDAO ageParametrosGeneralesSaveDAO)
        {
            return new AgeParametrosGenerales
            {
                Codigo = ageParametrosGeneralesSaveDAO.Codigo,
                Descripcion = ageParametrosGeneralesSaveDAO.Descripcion,
                Estado = ageParametrosGeneralesSaveDAO.Estado.ToUpper().ToString(),
                FechaEstado = DateTime.Now,
                FechaIngreso = DateTime.Now,
                TipoDatoParametro = ageParametrosGeneralesSaveDAO.TipoDatoParametro,
                ObservacionEstado = ageParametrosGeneralesSaveDAO.ObservacionEstado,
                UsuarioIngreso = ageParametrosGeneralesSaveDAO.UsuarioIngreso,
                UbicacionIngreso = Ubicacion.getIpAdress(_httpContextAccessor)
            };
        }

        private void ValidarPK(int codigo)
        {
            ValidateKeys.ValidarNoExistenciaKey(
                codigo,
                $"Parámetro general con código {codigo} ya existe.",
                _repository.ConsultarCompletePorId);
        }

    }
}
