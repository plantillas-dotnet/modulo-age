﻿using AGE.Controllers;
using AGE.Entities;
using AGE.Entities.DAO.AgeDirecciones;
using AGE.Middleware.Exceptions.BadRequest;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Repositories.AgeDireccionesRepository;
using AGE.Services.AgeLicenciatariosAplicaSecuService;
using AGE.Utils;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using System.ComponentModel.DataAnnotations;
using System.Transactions;
using AGE.Repositories.AgeLicenciatariosRepository;
using AGE.Repositories.AgePersonasRepository;
using AGE.Entities.DAO.AgePersonas;
using AGE.Repositories.AgeTiposDireccioneRepository;
using AGE.Entities.DAO.AgeLocalidades;
using AGE.Repositories.AgeLocalidadesRepository;

namespace AGE.Services.AgeDireccionesService
{
    public class AgeDireccionesService : IAgeDireccionesService
    {
        private readonly IAgeDireccionesRepository _repository;
        private readonly IAgeLicenciatariosAplicaSecuService _ageLicenciatariosAplicaSecuService;
        private readonly IAgeLicenciatariosRepository _repositoryLicenciatario;
        private readonly IAgePersonasRepository _repositoryPersona;
        private readonly IAgeTiposDireccionesRepository _repositoryTipDi;
        private readonly IAgeLocalidadesRepository _repositoryLocalidades;

        public AgeDireccionesService(
            IAgeDireccionesRepository Repository,
            IAgeLicenciatariosAplicaSecuService ageLicenciatariosAplicaSecuService,
            IAgeLicenciatariosRepository repositoryLicenciatario,
            IAgePersonasRepository repositoryPersona,
            IAgeTiposDireccionesRepository repositoryTipDi,
            IAgeLocalidadesRepository repositoryLocalidades)
        {
            _repository = Repository;
            _ageLicenciatariosAplicaSecuService = ageLicenciatariosAplicaSecuService;
            _repositoryLicenciatario = repositoryLicenciatario;
            _repositoryPersona = repositoryPersona;
            _repositoryTipDi = repositoryTipDi;
            _repositoryLocalidades = repositoryLocalidades;
        }


        public Task<Page<AgeDireccionesDAO>> ConsultarTodos(
            int codigoLicenciatario,
            int? codigoPersona,
            int? codigo,
            int? ageTipDiCodigo,
            string? descripcion,
            Pageable pageable)
        {
            if (codigoLicenciatario <= 0)
            {
                throw new InvalidFieldException("El código de licenciatario debe ser un número entero mayor a cero.");
            }

            pageable.Validar<AgeDireccionesDAO>();

            return _repository.ConsultarTodos(codigoLicenciatario, codigoPersona ?? 0, 
                codigo ?? 0, ageTipDiCodigo ?? 0, descripcion ?? "", pageable);
        }


        public Task<Page<AgeDireccionesDAO>> ConsultarListaFiltro(
            int codigoLicenciatario,
            string filtro,
            Pageable pageable)
        {
            if (codigoLicenciatario == 0)
            {
                throw new InvalidFieldException("El código de licenciatario debe ser un número entero mayor a cero.");
            }

            if (string.IsNullOrWhiteSpace(filtro))
            {
                throw new InvalidFieldException("El filtro es requerido.");
            }

            pageable.Validar<AgeDireccionesDAO>();

            return _repository.ConsultarListaFiltro(codigoLicenciatario, filtro.ToLower(), pageable);
        }


        public Task<AgeDireccionesDAO> ConsultarPorId(
            AgeDireccionesPKDAO ageDireccionesPKDAO)
        {
            Validator.ValidateObject(ageDireccionesPKDAO, new ValidationContext(ageDireccionesPKDAO), true);

            AgeDireccionesDAO ageDireccionesDAO = _repository.ConsultarPorId(ageDireccionesPKDAO).Result;

            if (ageDireccionesDAO == null)
            {
                throw new RegisterNotFoundException("Direccion con código " + ageDireccionesPKDAO.codigo + " no existe");
            }

            return Task.FromResult(ageDireccionesDAO);
        }


        public AgeDirecciones ConsultarCompletePorId(
            AgeDireccionesPKDAO ageDireccionesPKDAO)
        {
            Validator.ValidateObject(ageDireccionesPKDAO, new ValidationContext(ageDireccionesPKDAO), true);

            AgeDirecciones? ageDireccione = _repository.ConsultarCompletePorId(ageDireccionesPKDAO).Result;

            return ageDireccione;
        }

        public async Task<Resource<AgeDireccionesDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgeDireccionesSaveDAO ageDireccionesSaveDAO)
        {
            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    AgeDirecciones ageDireccione = ValidarInsert(_httpContextAccessor, ageDireccionesSaveDAO);

                    AgeDireccionesDAO ageDireccionesDAO = await _repository.Insertar(ageDireccione);

                    Resource<AgeDireccionesDAO> ageDireccionesDAOWithResource = GetDataWithResource(_httpContextAccessor, ageDireccionesDAO);

                    transactionScope.Complete();

                    return ageDireccionesDAOWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public async Task<List<Resource<AgeDireccionesDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeDireccionesSaveDAO> ageDireccionesSaveDAOList)
        {
            if (ageDireccionesSaveDAOList == null)
            {
                throw new InvalidSintaxisException();
            }

            List<AgeDirecciones> ageDireccioneList = new List<AgeDirecciones>();

            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    foreach (AgeDireccionesSaveDAO ageDireccionesSaveDAO in ageDireccionesSaveDAOList)
                    {
                        AgeDirecciones ageDireccion = ValidarInsert(_httpContextAccessor, ageDireccionesSaveDAO);
                        ageDireccioneList.Add(ageDireccion);
                    }

                    List<AgeDireccionesDAO> ageDireccionesDAOList = await _repository.InsertarVarios(ageDireccioneList);

                    List<Resource<AgeDireccionesDAO>> ageDireccionesDAOListWithResource = new List<Resource<AgeDireccionesDAO>>();

                    foreach (AgeDireccionesDAO ageDireccionesDAO in ageDireccionesDAOList)
                    {
                        ageDireccionesDAOListWithResource.Add(GetDataWithResource(_httpContextAccessor, ageDireccionesDAO));
                    }

                    transactionScope.Complete();

                    return ageDireccionesDAOListWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }


        public Task<AgeDireccionesDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeDireccionesSaveDAO ageDireccionesSaveDAO)
        {
            AgeDirecciones ageDireccione = ValidarUpdate(_httpContextAccessor, ageDireccionesSaveDAO);

            return _repository.Actualizar(ageDireccione);
        }


        public Task<List<AgeDireccionesDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeDireccionesSaveDAO> ageDireccionesSaveDAOList)
        {
            if (ageDireccionesSaveDAOList == null)
                throw new InvalidSintaxisException();

            ValidateKeys.ValidarPKDuplicadas(ageDireccionesSaveDAOList,
                                             p => new { p.Id.AgePersonCodigo, 
                                                        p.Id.AgePersonAgeLicencCodigo, 
                                                        p.Id.codigo },
                                             "Id");

            List<AgeDirecciones> ageDireccioneList = new List<AgeDirecciones>();
            AgeDirecciones ageDireccione;

            foreach (AgeDireccionesSaveDAO ageDireccionesSaveDAO in ageDireccionesSaveDAOList)
            {
                ageDireccione = ValidarUpdate(_httpContextAccessor, ageDireccionesSaveDAO);
                ageDireccioneList.Add(ageDireccione);
            }

            return _repository.ActualizarVarios(ageDireccioneList);
        }

        private static Resource<AgeDireccionesDAO> GetDataWithResource(
            IHttpContextAccessor _httpContextAccessor,
            AgeDireccionesDAO ageDireccionesDAO)
        {
            string rutaSegmentoFinal = $"{ageDireccionesDAO.Id.AgePersonAgeLicencCodigo}/{ageDireccionesDAO.Id.codigo}";

            return Resource<AgeDireccionesDAO>.GetDataWithResource<AgeDireccionesController>(
                _httpContextAccessor, rutaSegmentoFinal, ageDireccionesDAO);
        }

        private AgeDirecciones ValidarInsert(
            IHttpContextAccessor _httpContextAccessor,
            AgeDireccionesSaveDAO ageDireccionesSaveDAO)
        {
            if (ageDireccionesSaveDAO == null)
                throw new InvalidSintaxisException();

            Validator.ValidateObject(ageDireccionesSaveDAO, new ValidationContext(ageDireccionesSaveDAO), true);

            FuncionesSecuencias fg = new FuncionesSecuencias(_ageLicenciatariosAplicaSecuService);

            ageDireccionesSaveDAO.Id.codigo = fg.ObtenerSecuencia(
                                                _httpContextAccessor,
                                                 ageDireccionesSaveDAO.Id.AgePersonAgeLicencCodigo,
                                                 Globales.CODIGO_SECUENCIA_DIRECCION,
                                                 ageDireccionesSaveDAO.UsuarioIngreso);

            ValidarKeys(ageDireccionesSaveDAO);

            AgeDirecciones entityObject = FromDAOToEntity(_httpContextAccessor, ageDireccionesSaveDAO);

            Validator.ValidateObject(entityObject, new ValidationContext(entityObject), true);

            return entityObject;
        }


        private AgeDirecciones ValidarUpdate(
            IHttpContextAccessor _httpContextAccessor,
            AgeDireccionesSaveDAO ageDireccionesSaveDAO)
        {
            if (ageDireccionesSaveDAO == null)
                throw new InvalidSintaxisException();

            if (ageDireccionesSaveDAO.UsuarioModificacion == null || ageDireccionesSaveDAO.UsuarioModificacion <= 0)
                throw new InvalidFieldException("El usuario de modificación debe ser un número entero mayor a cero.");

            AgeDirecciones? ageDirecciones = ConsultarCompletePorId(new AgeDireccionesPKDAO
            {
                AgePersonAgeLicencCodigo = ageDireccionesSaveDAO.Id.AgePersonAgeLicencCodigo,
                AgePersonCodigo = ageDireccionesSaveDAO.Id.AgePersonCodigo,
                codigo = ageDireccionesSaveDAO.Id.codigo
            });

            if (ageDirecciones == null)
                throw new RegisterNotFoundException("Dirección con código " + ageDireccionesSaveDAO.Id.codigo + " no existe.");

            if (!string.IsNullOrWhiteSpace(ageDireccionesSaveDAO.Estado))
            {
                ageDirecciones.Estado = ageDireccionesSaveDAO.Estado;
                ageDirecciones.FechaEstado = DateTime.Now;
            }

            if (ageDireccionesSaveDAO.AgeAgeTipLoAgePaisCodigo != null || ageDireccionesSaveDAO.AgeAgeTipLoAgePaisCodigo > 0)
                ageDirecciones.AgeAgeTipLoAgePaisCodigo = ageDireccionesSaveDAO.AgeAgeTipLoAgePaisCodigo;

            if (ageDireccionesSaveDAO.AgeTipDiCodigo > 0)
                ageDirecciones.AgeTipDiCodigo = ageDireccionesSaveDAO.AgeTipDiCodigo;

            if (ageDireccionesSaveDAO.AgeLocaliAgeTipLoCodigo != null || ageDireccionesSaveDAO.AgeLocaliAgeTipLoCodigo > 0)
                ageDirecciones.AgeLocaliAgeTipLoCodigo = ageDireccionesSaveDAO.AgeLocaliAgeTipLoCodigo;

            if (ageDireccionesSaveDAO.AgeLocaliCodigo != null || ageDireccionesSaveDAO.AgeLocaliCodigo > 0)
                ageDirecciones.AgeLocaliCodigo = ageDireccionesSaveDAO.AgeLocaliCodigo;

            if (!string.IsNullOrWhiteSpace(ageDireccionesSaveDAO.Latitud))
                ageDirecciones.Latitud = ageDireccionesSaveDAO.Latitud;

            if (!string.IsNullOrWhiteSpace(ageDireccionesSaveDAO.Longitud))
                ageDirecciones.Longitud = ageDireccionesSaveDAO.Longitud;

            if (!string.IsNullOrWhiteSpace(ageDireccionesSaveDAO.Descripcion))
                ageDirecciones.Descripcion = ageDireccionesSaveDAO.Descripcion;

            if (!string.IsNullOrWhiteSpace(ageDireccionesSaveDAO.Telefono1))
                ageDirecciones.Telefono1 = ageDireccionesSaveDAO.Telefono1;

            if (!string.IsNullOrWhiteSpace(ageDireccionesSaveDAO.Telefono2))
                ageDirecciones.Telefono2 = ageDireccionesSaveDAO.Telefono2;

            if(!string.IsNullOrWhiteSpace(ageDireccionesSaveDAO.NombreContacto))
                ageDirecciones.NombreContacto = ageDireccionesSaveDAO.NombreContacto;

            if(!string.IsNullOrWhiteSpace(ageDireccionesSaveDAO.CorreoElectronicoContacto))
                ageDirecciones.CorreoElectronicoContacto = ageDireccionesSaveDAO.CorreoElectronicoContacto;

            if (!string.IsNullOrWhiteSpace(ageDireccionesSaveDAO.CelularContacto))
                ageDirecciones.CelularContacto = ageDireccionesSaveDAO.CelularContacto;

            if (!string.IsNullOrWhiteSpace(ageDireccionesSaveDAO.ObservacionEstado))
                ageDirecciones.ObservacionEstado = ageDireccionesSaveDAO.ObservacionEstado;

        ageDirecciones.FechaModificacion = DateTime.Now;
            ageDirecciones.UbicacionModificacion = Ubicacion.getIpAdress(_httpContextAccessor);
            ageDirecciones.UsuarioModificacion = ageDireccionesSaveDAO.UsuarioModificacion;

            Validator.ValidateObject(ageDirecciones, new ValidationContext(ageDirecciones), true);

            return ageDirecciones;
        }

        private AgeDirecciones FromDAOToEntity(
        IHttpContextAccessor _httpContextAccessor,
        AgeDireccionesSaveDAO ageDireccionesSaveDAO)
        {
            return new AgeDirecciones
            {
                AgePersonCodigo = ageDireccionesSaveDAO.Id.AgePersonCodigo,
                AgePersonAgeLicencCodigo = ageDireccionesSaveDAO.Id.AgePersonAgeLicencCodigo,
                Codigo = ageDireccionesSaveDAO.Id.codigo,
                AgeAgeTipLoAgePaisCodigo = ageDireccionesSaveDAO.AgeAgeTipLoAgePaisCodigo,
                AgeLocaliAgeTipLoCodigo = ageDireccionesSaveDAO.AgeLocaliAgeTipLoCodigo,
                AgeLocaliCodigo = ageDireccionesSaveDAO.AgeLocaliCodigo,
                AgeTipDiCodigo = ageDireccionesSaveDAO.AgeTipDiCodigo,
                Descripcion = ageDireccionesSaveDAO.Descripcion,
                Telefono1 = ageDireccionesSaveDAO.Telefono1,
                Telefono2 = ageDireccionesSaveDAO.Telefono2,
                NombreContacto = ageDireccionesSaveDAO.NombreContacto,
                CorreoElectronicoContacto = ageDireccionesSaveDAO.CorreoElectronicoContacto,
                CelularContacto = ageDireccionesSaveDAO.CelularContacto,
                Latitud = ageDireccionesSaveDAO.Latitud,
                Longitud = ageDireccionesSaveDAO.Longitud,
                Estado = ageDireccionesSaveDAO.Estado,
                FechaEstado = DateTime.Now,
                FechaIngreso = DateTime.Now,
                ObservacionEstado = ageDireccionesSaveDAO.ObservacionEstado,
                UbicacionIngreso = Ubicacion.getIpAdress(_httpContextAccessor),
                UsuarioIngreso = ageDireccionesSaveDAO.UsuarioIngreso
            };
        }

        private void ValidarKeys(AgeDireccionesSaveDAO ageDireccionesSaveDAO)
        {
            ValidarPK(ageDireccionesSaveDAO.Id);

            ValidarFKLicenciatario(ageDireccionesSaveDAO.Id.AgePersonAgeLicencCodigo);

            ValidarFKPersona(ageDireccionesSaveDAO.Id.AgePersonAgeLicencCodigo, 
                             ageDireccionesSaveDAO.Id.AgePersonCodigo);

            ValidarFKTipoDireccion(ageDireccionesSaveDAO.AgeTipDiCodigo);

            if (ageDireccionesSaveDAO.AgeAgeTipLoAgePaisCodigo != null && ageDireccionesSaveDAO.AgeLocaliAgeTipLoCodigo != null
                && ageDireccionesSaveDAO.AgeLocaliCodigo != null)
            {
                ValidarFKLocalidades(ageDireccionesSaveDAO.AgeAgeTipLoAgePaisCodigo,
                                     ageDireccionesSaveDAO.AgeLocaliAgeTipLoCodigo,
                                     ageDireccionesSaveDAO.AgeLocaliCodigo);
            }
        }

        private void ValidarFKs(AgeDireccionesSaveDAO ageDireccionesSaveDAO) 
        { 
        
        }

        private void ValidarPK(AgeDireccionesPKDAO ageDireccionesPKDAO)
        {
            ValidateKeys.ValidarNoExistenciaKey(
                ageDireccionesPKDAO,
                $"Dirección con código {ageDireccionesPKDAO.codigo} ya existe.",
                _repository.ConsultarCompletePorId);
        }

        private void ValidarFKLicenciatario(int codigoLicenciatario)
        {
            ValidateKeys.ValidarExistenciaKey(
                codigoLicenciatario,
                $"Licenciatario con código {codigoLicenciatario} no existe.",
                _repositoryLicenciatario.ConsultarCompletePorId);
        }

        private void ValidarFKPersona(int codigoLicenciatario, long codigoPersona)
        {
            AgePersonasPKDAO agePersonasPKDAO = new AgePersonasPKDAO
            {
                ageLicencCodigo = codigoLicenciatario,
                codigo = codigoPersona
            };

            ValidateKeys.ValidarExistenciaKey(
                agePersonasPKDAO,
                $"Persona con código {agePersonasPKDAO.codigo} no existe.",
                _repositoryPersona.ConsultarCompletePorId);
        }

        private void ValidarFKLocalidades(int? codigoTipLoPais, int? codigoTipLo, int? codigoLocalidad)
        {
            AgeLocalidadesPKDAO ageLocalidadesPKDAO = new AgeLocalidadesPKDAO
            {
                AgeTipLoAgePaisCodigo = (int)codigoTipLoPais,
                AgeTipLoCodigo = (int)codigoTipLo,
                Codigo = (int)codigoLocalidad
            };

            ValidateKeys.ValidarExistenciaKey(
                ageLocalidadesPKDAO,
                $"Localidad con código {ageLocalidadesPKDAO.Codigo} no existe.",
                _repositoryLocalidades.ConsultarCompletePorId);
           
        }

        private void ValidarFKTipoDireccion(int codigoTipoDireccion)
        {
            ValidateKeys.ValidarExistenciaKey(
            codigoTipoDireccion,
                $"Tipo de Dirección con código {codigoTipoDireccion} no existe.",
                _repositoryTipDi.ConsultarCompletePorId);
        }


    }
}
