﻿using AGE.Entities.DAO.AgeDirecciones;
using AGE.Entities;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;

namespace AGE.Services.AgeDireccionesService
{
    public interface IAgeDireccionesService
    {
        Task<Page<AgeDireccionesDAO>> ConsultarTodos(
                int codigoLicenciatario,
                int? codigoPersona,
                int? codigo,
                int? ageTipDiCodigo,
                string? descripcion,
               Pageable pageable);
        Task<Page<AgeDireccionesDAO>> ConsultarListaFiltro(
            int codigoLicenciatario,
            string filtro,
            Pageable pageable);
        Task<AgeDireccionesDAO> ConsultarPorId(
            AgeDireccionesPKDAO ageDireccionesPKDAO);
        AgeDirecciones ConsultarCompletePorId(
            AgeDireccionesPKDAO ageDireccionesPKDAO);
        Task<Resource<AgeDireccionesDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgeDireccionesSaveDAO ageDireccionesSaveDAO);
        Task<List<Resource<AgeDireccionesDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeDireccionesSaveDAO> ageDireccionesSaveDAOList);
        Task<AgeDireccionesDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeDireccionesSaveDAO ageDireccionesSaveDAO);
        Task<List<AgeDireccionesDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeDireccionesSaveDAO> ageDireccionesSaveDAOList);
    }
}
