﻿using AGE.Entities;
using AGE.Entities.DAO.AgeUnidadesTiempos;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;

namespace AGE.Services.AgeUnidadesTiempoService
{
    public interface IAgeUnidadesTiempoService
    {
        Task<Page<AgeUnidadesTiempoDAO>> ConsultarTodos(int codigoLicenciatario,
                                                    long codigo,
                                                    string descripcion,
                                                    Pageable pageable);

        Task<AgeUnidadesTiempoDAO> ConsultarPorId(AgeUnidadesTiempoPKDAO ageUnidadesTiempo);

        Task<Page<AgeUnidadesTiempoDAO>> ConsultarListaFiltro(int codigoLicenciatario,
                                                                        string filtro,
                                                                        Pageable pageable);

        Task<Resource<AgeUnidadesTiempoDAO>> Ingresar(AgeUnidadesTiempoSaveDAO transaccion,
                                                        IHttpContextAccessor _httpContextAccessor);

        Task<List<Resource<AgeUnidadesTiempoDAO>>> IngresarVarios(List<AgeUnidadesTiempoSaveDAO> transaccion,
                                                        IHttpContextAccessor _httpContextAccessor);

        Task<AgeUnidadesTiempoDAO> Actualizar(AgeUnidadesTiempoSaveDAO transaccion,
                                                        IHttpContextAccessor _httpContextAccessor);

        Task<List<AgeUnidadesTiempoDAO>> ActualizarVarios(List<AgeUnidadesTiempoSaveDAO> transaccion,
                                                        IHttpContextAccessor _httpContextAccessor);
    }
}
