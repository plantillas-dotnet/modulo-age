﻿using AGE.Controllers;
using AGE.Entities;
using AGE.Entities.DAO.AgeUnidadesTiempos;
using AGE.Middleware.Exceptions.BadRequest;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Repositories.AgeUnidadesTiempoRepository;
using AGE.Services.AgeLicenciatariosAplicaSecuService;
using AGE.Utils;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using System.ComponentModel.DataAnnotations;
using System.Transactions;

namespace AGE.Services.AgeUnidadesTiempoService
{
    public class AgeUnidadesTiempoService : IAgeUnidadesTiempoService
    {
        private readonly IAgeUnidadesTiempoRepository _repository;
        private readonly IAgeLicenciatariosAplicaSecuService _ageLicenciatariosAplicaSecuService;

        public AgeUnidadesTiempoService(
            IAgeUnidadesTiempoRepository Repository,
            IAgeLicenciatariosAplicaSecuService ageLicenciatariosAplicaSecuService)
        {
            _repository = Repository;
            _ageLicenciatariosAplicaSecuService = ageLicenciatariosAplicaSecuService;
        }

        public Task<Page<AgeUnidadesTiempoDAO>> ConsultarTodos(int codigoLicenciatario, long codigo, string descripcion, Pageable pageable)
        {
            if (codigoLicenciatario <= 0)
                throw new InvalidFieldException("El código de licenciatario debe ser un número entero mayor a cero.");

            pageable.Validar<AgeUnidadesTiempoDAO>();

            return _repository.ConsultarTodos(codigoLicenciatario, codigo, descripcion, Globales.ESTADO_ANULADO, pageable);
        }

        public Task<AgeUnidadesTiempoDAO> ConsultarPorId(AgeUnidadesTiempoPKDAO ageUnidadesTiempo)
        {
            Validator.ValidateObject(ageUnidadesTiempo, new ValidationContext(ageUnidadesTiempo), true);

            AgeUnidadesTiempoDAO ageUnidadesTiempoDAO = _repository.ConsultarPorId(ageUnidadesTiempo).Result;

            return ageUnidadesTiempoDAO == null
                ? throw new RegisterNotFoundException("Unidad de tiempo con código: " + ageUnidadesTiempo.codigo + " no existe.")
                : Task.FromResult(ageUnidadesTiempoDAO);
        }

        private AgeUnidadesTiempo ConsultarCompletePorId(AgeUnidadesTiempoPKDAO unidadesTiempoPKDAO)
        {
            Validator.ValidateObject(unidadesTiempoPKDAO, new ValidationContext(unidadesTiempoPKDAO), true);

            AgeUnidadesTiempo unidadesTiempo = _repository.ConsultarCompletePorId(unidadesTiempoPKDAO).Result;

            return unidadesTiempo;
        }

        public Task<Page<AgeUnidadesTiempoDAO>> ConsultarListaFiltro(int codigoLicenciatario, string filtro, Pageable pageable)
        {
            if (codigoLicenciatario <= 0)
                throw new InvalidFieldException("El código de licenciatario debe ser un número entero mayor a cero.");

            if (string.IsNullOrWhiteSpace(filtro))
                throw new InvalidFieldException("El filtro es requerido.");

            pageable.Validar<AgeUnidadesTiempoDAO>();

            return _repository.ConsultarListaFiltro(codigoLicenciatario,filtro, pageable);
        }

        public async Task<Resource<AgeUnidadesTiempoDAO>> Ingresar(AgeUnidadesTiempoSaveDAO ageUnidadesTiempoSaveDAO, IHttpContextAccessor _httpContextAccessor)
        {
            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    AgeUnidadesTiempo ageUnidadesTiempo = ValidarInsert(_httpContextAccessor, ageUnidadesTiempoSaveDAO);
                    AgeUnidadesTiempoDAO ageUnidadesTiempoDAO = await _repository.Insertar(ageUnidadesTiempo);
                    
                    Resource<AgeUnidadesTiempoDAO> ageUnidadesTiempoDAOWithResource = GetDataWithResource(_httpContextAccessor, ageUnidadesTiempoDAO);

                    transactionScope.Complete();

                    return ageUnidadesTiempoDAOWithResource;

                } catch(Exception)
                {
                    throw;
                }
            }
        }

        public async Task<List<Resource<AgeUnidadesTiempoDAO>>> IngresarVarios(List<AgeUnidadesTiempoSaveDAO> ageUnidadesTiempoSaveDAO, 
                                                                                IHttpContextAccessor _httpContextAccessor)
        {
            if (ageUnidadesTiempoSaveDAO == null)
                throw new InvalidSintaxisException();
            
            List<AgeUnidadesTiempo> ageUnidadesTiemposList = new();

            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    foreach (AgeUnidadesTiempoSaveDAO ageUnidadesTiemposSaveDAO in ageUnidadesTiempoSaveDAO)
                    {
                        AgeUnidadesTiempo ageUnidadesTiempos = ValidarInsert(_httpContextAccessor, ageUnidadesTiemposSaveDAO);
                        ageUnidadesTiemposList.Add(ageUnidadesTiempos);
                    }

                    List<AgeUnidadesTiempoDAO> ageUnidadesTiemposDAOList = await _repository.InsertarVarios(ageUnidadesTiemposList);

                    List<Resource<AgeUnidadesTiempoDAO>> ageUnidadesTiempoDAOListWithResource = new();

                    foreach (AgeUnidadesTiempoDAO ageUnidadesTiempoDAO in ageUnidadesTiemposDAOList)
                    {
                        ageUnidadesTiempoDAOListWithResource.Add(GetDataWithResource(_httpContextAccessor, ageUnidadesTiempoDAO));
                    }

                    transactionScope.Complete();

                    return ageUnidadesTiempoDAOListWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public Task<AgeUnidadesTiempoDAO> Actualizar(AgeUnidadesTiempoSaveDAO ageUnidadesTiempoSaveDAO, IHttpContextAccessor _httpContextAccessor)
        {
            AgeUnidadesTiempo ageUnidadesTiempo = ValidarUpdate(_httpContextAccessor, ageUnidadesTiempoSaveDAO);

            return _repository.Actualizar(ageUnidadesTiempo);
        }

        public Task<List<AgeUnidadesTiempoDAO>> ActualizarVarios(List<AgeUnidadesTiempoSaveDAO> ageUnidadesTiempoSaveDAO, IHttpContextAccessor _httpContextAccessor)
        {
            if (ageUnidadesTiempoSaveDAO == null)
                throw new InvalidSintaxisException();

            ValidateKeys.ValidarPKDuplicadas(ageUnidadesTiempoSaveDAO,
                                             p => new { p.Id.ageLicencCodigo, p.Id.codigo },
                                             "Id");

            List<AgeUnidadesTiempo> ageUnidadesTiemposList = new();
            AgeUnidadesTiempo ageUnidadesTiempo;

            foreach (AgeUnidadesTiempoSaveDAO ageUnidadesTiempoSave in ageUnidadesTiempoSaveDAO)
            {
                ageUnidadesTiempo = ValidarUpdate(_httpContextAccessor, ageUnidadesTiempoSave);
                ageUnidadesTiemposList.Add(ageUnidadesTiempo);
            }

            return _repository.ActualizarVarios(ageUnidadesTiemposList);
        }

        private static Resource<AgeUnidadesTiempoDAO> GetDataWithResource(
            IHttpContextAccessor _httpContextAccessor,
            AgeUnidadesTiempoDAO ageUnidadesTiempoDAO)
        {
            string rutaSegmentoFinal = $"{ageUnidadesTiempoDAO.Id.ageLicencCodigo}/{ageUnidadesTiempoDAO.Id.codigo}";

            return Resource<AgeUnidadesTiempoDAO>.GetDataWithResource<AgeUnidadesTiempoController>(
                _httpContextAccessor, rutaSegmentoFinal, ageUnidadesTiempoDAO);
        }

        private AgeUnidadesTiempo ValidarInsert(
            IHttpContextAccessor _httpContextAccessor,
            AgeUnidadesTiempoSaveDAO ageUnidadesTiempoSaveDAO)
        {
            if (ageUnidadesTiempoSaveDAO == null) throw new InvalidSintaxisException();

            Validator.ValidateObject(ageUnidadesTiempoSaveDAO, new ValidationContext(ageUnidadesTiempoSaveDAO), true);

            FuncionesSecuencias fg = new(_ageLicenciatariosAplicaSecuService);

            ageUnidadesTiempoSaveDAO.Id.codigo = fg.ObtenerSecuencia(_httpContextAccessor,
                                                ageUnidadesTiempoSaveDAO.Id.ageLicencCodigo,
                                                Globales.CODIGO_SECUENCIA_UNIDAD_TIEMPO,
                                                ageUnidadesTiempoSaveDAO.UsuarioIngreso);

            ValidarPK(ageUnidadesTiempoSaveDAO.Id);

            AgeUnidadesTiempo entityObject = FromDAOToEntity(_httpContextAccessor, ageUnidadesTiempoSaveDAO);

            Validator.ValidateObject(entityObject, new ValidationContext(entityObject), true);

            return entityObject;
        }

        private AgeUnidadesTiempo ValidarUpdate(IHttpContextAccessor httpContextAccessor, AgeUnidadesTiempoSaveDAO ageUnidadesTiempoSaveDAO)
        {
            if (ageUnidadesTiempoSaveDAO == null)
                throw new InvalidSintaxisException();

            if (ageUnidadesTiempoSaveDAO.UsuarioModificacion == null || ageUnidadesTiempoSaveDAO.UsuarioModificacion <= 0)
                throw new InvalidFieldException("El usuario de modificación debe ser un número entero mayor a cero.");

            AgeUnidadesTiempo ageUnidadesTiempo = ConsultarCompletePorId(ageUnidadesTiempoSaveDAO.Id) 
                ?? throw new RegisterNotFoundException("Unidad de tiempo con código: " + ageUnidadesTiempoSaveDAO.Id.codigo + " no existe");

            ageUnidadesTiempo.Estado = !string.IsNullOrWhiteSpace(ageUnidadesTiempoSaveDAO.Estado) ? ageUnidadesTiempoSaveDAO.Estado : ageUnidadesTiempo.Estado;
            ageUnidadesTiempo.FechaEstado = !string.IsNullOrWhiteSpace(ageUnidadesTiempoSaveDAO.Estado) ? DateTime.Now : ageUnidadesTiempo.FechaEstado;
            ageUnidadesTiempo.Descripcion = !string.IsNullOrWhiteSpace(ageUnidadesTiempoSaveDAO.Descripcion) ? ageUnidadesTiempoSaveDAO.Descripcion : ageUnidadesTiempo.Descripcion;
            ageUnidadesTiempo.ObservacionEstado = !string.IsNullOrWhiteSpace(ageUnidadesTiempoSaveDAO.ObservacionEstado) ? ageUnidadesTiempoSaveDAO.ObservacionEstado : ageUnidadesTiempo.ObservacionEstado;

            ageUnidadesTiempo.FechaModificacion = DateTime.Now;
            ageUnidadesTiempo.UbicacionModificacion = Ubicacion.getIpAdress(httpContextAccessor);
            ageUnidadesTiempo.UsuarioModificacion = ageUnidadesTiempoSaveDAO.UsuarioModificacion;

            Validator.ValidateObject(ageUnidadesTiempo, new ValidationContext(ageUnidadesTiempo), true);

            return ageUnidadesTiempo;
        }

        private AgeUnidadesTiempo FromDAOToEntity(
            IHttpContextAccessor _httpContextAccessor,
            AgeUnidadesTiempoSaveDAO ageUnidadesTiempoSaveDAO)
        {

            return new AgeUnidadesTiempo()
            {
                AgeLicencCodigo = ageUnidadesTiempoSaveDAO.Id.ageLicencCodigo,
                Codigo = ageUnidadesTiempoSaveDAO.Id.codigo,
                Descripcion = ageUnidadesTiempoSaveDAO.Descripcion,
                Estado = ageUnidadesTiempoSaveDAO.Estado,
                FechaEstado = DateTime.Now,
                FechaIngreso = DateTime.Now,
                ObservacionEstado = ageUnidadesTiempoSaveDAO.ObservacionEstado,
                UbicacionIngreso = Ubicacion.getIpAdress(_httpContextAccessor),
                UsuarioIngreso = ageUnidadesTiempoSaveDAO.UsuarioIngreso
            };
        }

        private void ValidarPK(AgeUnidadesTiempoPKDAO ageUnidadesTiempoPKDAO)
        {
            ValidateKeys.ValidarNoExistenciaKey(
               ageUnidadesTiempoPKDAO,
               $"Unidad tiempo con código: {ageUnidadesTiempoPKDAO.codigo}, " +
               $" y código licenciatario: {ageUnidadesTiempoPKDAO.ageLicencCodigo} ya existe.",
               _repository.ConsultarCompletePorId);
        }
    }
}
