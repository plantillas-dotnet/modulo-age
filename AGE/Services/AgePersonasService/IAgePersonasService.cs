﻿using AGE.Entities.DAO.AgeLicenciatarios;
using AGE.Entities.DAO.AgePersonas;
using AGE.Entities;
using AGE.Middleware.Exceptions.BadRequest;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using AGE.Utils;
using System.ComponentModel.DataAnnotations;
using System.Transactions;

namespace AGE.Services.AgePersonasService
{
    public interface IAgePersonasService
    {
        Task<Page<AgePersonasDAO>> ConsultarTodos(
             int codigoLicenciatario,
             int codigo,
             string apellidos,
             string nombres,
             string numeroIdentificacion,
             Pageable pageable);
        Task<Page<AgePersonasDAO>> ConsultarListaFiltro(
            int codigoLicenciatario,
            string filtro,
            Pageable pageable);
        Task<AgePersonasDAO> ConsultarPorId(
            AgePersonasPKDAO agePersonasPKDAO);

        Task<AgePersonasDAO> ConsultarLicencYNumeroIdent(
            int codigoLicenciatario, 
            string identificacion);
    
        Task<Resource<AgePersonasDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgePersonasSaveDAO agePersonasSaveDAO);
        Task<List<Resource<AgePersonasDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgePersonasSaveDAO> agePersonasSaveDAOList);
        Task<AgePersonasDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgePersonasSaveDAO agePersonasSaveDAO);

        Task<List<AgePersonasDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgePersonasSaveDAO> agePersonasSaveDAOList);

        Task<AgePersonasDAO> ValidarYGuardarPersona(IHttpContextAccessor _httpContextAccessor,
            AgePersonasSaveDAO agePersonasSaveDAO);
    }
}
