using AGE.Controllers;
using AGE.Entities;
using AGE.Middleware.Exceptions.BadRequest;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using AGE.Utils;
using System.ComponentModel.DataAnnotations;
using System.Transactions;
using AGE.Repositories.AgePersonasRepository;
using AGE.Entities.DAO.AgePersonas;
using AGE.Services.AgeLicenciatariosAplicaSecuService;
using AGE.Repositories.AgeLicenciatariosRepository;
using AGE.Repositories.AgeTiposIdentificacioneRepository;
using AGE.Repositories.AgeLocalidadesRepository;
using AGE.Entities.DAO.AgeLocalidades;

namespace AGE.Services.AgePersonasService
{
    public class AgePersonasService : IAgePersonasService
    {
        private readonly IAgePersonasRepository _repository;
        private readonly IAgeLicenciatariosRepository _repositoryLicenciatarios;
        private readonly IAgeTiposIdentificacionesRepository _repositoryTipoIdentificacion;
        private readonly IAgeLocalidadesRepository _repositoryLocalidad;
        private readonly IAgeLicenciatariosAplicaSecuService _ageLicenciatariosAplicaSecuService;


        public AgePersonasService(
            IAgePersonasRepository Repository,
            IAgeLicenciatariosRepository RepositoryLicenciatarios,
            IAgeTiposIdentificacionesRepository RepositoryTipoIdentif,
            IAgeLocalidadesRepository RepositoryLocalidad,
            IAgeLicenciatariosAplicaSecuService ageLicenciatariosAplicaSecuService)
        {
            _repository = Repository;
            _repositoryLicenciatarios = RepositoryLicenciatarios;
            _repositoryLocalidad = RepositoryLocalidad;
            _repositoryTipoIdentificacion = RepositoryTipoIdentif;
            _ageLicenciatariosAplicaSecuService = ageLicenciatariosAplicaSecuService;
        }


        public Task<Page<AgePersonasDAO>> ConsultarTodos(
            int codigoLicenciatario,
            int codigo,
            string apellidos,
            string nombres,
            string numeroIdentificacion,
            Pageable pageable)
        {
            if (codigoLicenciatario <= 0)
                throw new InvalidFieldException("El código de licenciatario debe ser un número entero mayor a cero.");

            pageable.Validar<AgePersonasDAO>();

            return _repository.ConsultarTodos(codigoLicenciatario,codigo,apellidos, nombres,
            numeroIdentificacion, pageable);
        }


        public Task<Page<AgePersonasDAO>> ConsultarListaFiltro(
            int codigoLicenciatario,
            string filtro,
            Pageable pageable)
        {
            if (codigoLicenciatario == 0)
                throw new InvalidFieldException("El código de licenciatario debe ser un número entero mayor a cero.");

            if (string.IsNullOrWhiteSpace(filtro))
                throw new InvalidFieldException("El filtro es requerido.");

            pageable.Validar<AgePersonasDAO>();

            return _repository.ConsultarListaFiltro(codigoLicenciatario, filtro.ToLower(), pageable);
        }


        public Task<AgePersonasDAO> ConsultarPorId(
            AgePersonasPKDAO agePersonasPKDAO)
        {
            Validator.ValidateObject(agePersonasPKDAO, new ValidationContext(agePersonasPKDAO), true);

            AgePersonasDAO agePersonasDAO = _repository.ConsultarPorId(agePersonasPKDAO).Result;

            if (agePersonasDAO == null)
                throw new RegisterNotFoundException("Persona con código " + agePersonasPKDAO.codigo + " no existe");
            
            return Task.FromResult(agePersonasDAO);
        }

        public async Task<AgePersonasDAO> ConsultarLicencYNumeroIdent(
            int codigoLicenciatario,
            string identificacion)
        {
            if (codigoLicenciatario <= 0)
                throw new InvalidFieldException("El código de licenciatario debe ser un número entero mayor a cero.");

            AgePersonasDAO agePersonasDAO = await _repository.ConsultarLicencYNumeroIdent(codigoLicenciatario, identificacion);

            if (agePersonasDAO == null)
                throw new RegisterNotFoundException("Persona con número de identificación " + identificacion + " no existe");

            return agePersonasDAO;
        }

        private AgePersonas ConsultarCompletePorId(
            AgePersonasPKDAO agePersonasPKDAO)
        {
            Validator.ValidateObject(agePersonasPKDAO, new ValidationContext(agePersonasPKDAO), true);

            AgePersonas? agePersona = _repository.ConsultarCompletePorId(agePersonasPKDAO).Result;

            return agePersona;
        }

        private AgePersonasDAO ConsultarPorLicencYNumeroIdent(
            int codigoLicenciatario, string numeroIdentificacion)
        {

            AgePersonasDAO? agePersonaDAO = _repository.ConsultarPorLicencYNumeroIdent(codigoLicenciatario,numeroIdentificacion).Result;

            return agePersonaDAO;
        }


        private AgePersonas ConsultarPorIdentificacion(
            string identificacion)
        {
            return _repository.ConsultarPorIdentificacion(identificacion).Result;
        }

        public async Task<Resource<AgePersonasDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgePersonasSaveDAO agePersonasSaveDAO)
        {
            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    AgePersonas agePersona = ValidarInsert(_httpContextAccessor, agePersonasSaveDAO);

                    AgePersonasDAO agePersonasDAO = await _repository.Insertar(agePersona);

                    Resource<AgePersonasDAO> agePersonasDAOWithResource = GetDataWithResource(_httpContextAccessor, agePersonasDAO);
                    
                    transactionScope.Complete();

                    return agePersonasDAOWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }


        public async Task<List<Resource<AgePersonasDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgePersonasSaveDAO> agePersonasSaveDAOList)
        {
            if (agePersonasSaveDAOList == null)
                throw new InvalidSintaxisException();

            ValidateKeys.ValidarPKDuplicadas(agePersonasSaveDAOList,
                                             p => new { p.NumeroIdentificacion },
                                             "Número de identificación");

            List<AgePersonas> agePersonasList = new List<AgePersonas>();

            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    foreach (AgePersonasSaveDAO agePersonaSaveDAO in agePersonasSaveDAOList)
                    {
                        AgePersonas agePersona = ValidarInsert(_httpContextAccessor, agePersonaSaveDAO);
                        agePersonasList.Add(agePersona);
                    }

                    List<AgePersonasDAO> agePersonasDAOList = await _repository.InsertarVarios(agePersonasList);

                    List<Resource<AgePersonasDAO>> agePersonasDAOListWithResource = new List<Resource<AgePersonasDAO>>();

                    foreach (AgePersonasDAO agePerfilesDAO in agePersonasDAOList)
                    {
                        agePersonasDAOListWithResource.Add(GetDataWithResource(_httpContextAccessor, agePerfilesDAO));
                    }

                    transactionScope.Complete();

                    return agePersonasDAOListWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public async Task<AgePersonasDAO> ValidarYGuardarPersona(
            IHttpContextAccessor _httpContextAccessor,
            AgePersonasSaveDAO agePersonasSaveDAO)
        {

            if (agePersonasSaveDAO.Id.ageLicencCodigo <= 0)
                throw new InvalidFieldException("El código de licenciatario de la persona debe ser un número entero mayor a cero.");

            AgePersonasDAO agePersonaDAO = ConsultarPorLicencYNumeroIdent(agePersonasSaveDAO.Id.ageLicencCodigo, agePersonasSaveDAO.NumeroIdentificacion);

            if (agePersonaDAO != null)
                return agePersonaDAO;

            Resource<AgePersonasDAO> agePersonaDAOWithResource = await Insertar(_httpContextAccessor, agePersonasSaveDAO);

            return agePersonaDAOWithResource.content;
            
        }

        public Task<AgePersonasDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgePersonasSaveDAO agePersonasSaveDAO)
        {
            AgePersonas agePersona = ValidarUpdate(_httpContextAccessor, agePersonasSaveDAO);

            return _repository.Actualizar(agePersona);
        }

        public Task<List<AgePersonasDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgePersonasSaveDAO> agePersonasSaveDAOList)
        {
            if (agePersonasSaveDAOList == null)
                throw new InvalidSintaxisException();

            ValidateKeys.ValidarPKDuplicadas(agePersonasSaveDAOList,
                                             p => new { p.Id.ageLicencCodigo, p.Id.codigo },
                                             "Id");

            ValidateKeys.ValidarPKDuplicadas(agePersonasSaveDAOList,
                                             p => new { p.NumeroIdentificacion },
                                             "Número de identificación");

            List<AgePersonas> agePersonasList = new List<AgePersonas>();
            AgePersonas agePersonas;

            foreach (AgePersonasSaveDAO agePersonasSaveDAO in agePersonasSaveDAOList)
            {
                agePersonas = ValidarUpdate(_httpContextAccessor, agePersonasSaveDAO);
                agePersonasList.Add(agePersonas);
            }

            return _repository.ActualizarVarios(agePersonasList);
        }


        private static Resource<AgePersonasDAO> GetDataWithResource(
            IHttpContextAccessor _httpContextAccessor,
            AgePersonasDAO agePersonasDAO)
        {
            string rutaSegmentoFinal = $"{agePersonasDAO.Id.ageLicencCodigo}/{agePersonasDAO.Id.codigo}";

            return Resource<AgePersonasDAO>.GetDataWithResource<AgePersonasController>(
                _httpContextAccessor, rutaSegmentoFinal, agePersonasDAO);
        }


        private AgePersonas ValidarInsert(
            IHttpContextAccessor _httpContextAccessor,
            AgePersonasSaveDAO agePersonasSaveDAO)
        {

            if (agePersonasSaveDAO == null)
                throw new InvalidSintaxisException();

            Validator.ValidateObject(agePersonasSaveDAO, new ValidationContext(agePersonasSaveDAO), true);

            FuncionesSecuencias fg = new FuncionesSecuencias(_ageLicenciatariosAplicaSecuService);

            agePersonasSaveDAO.Id.codigo = fg.ObtenerSecuencia(_httpContextAccessor,
                                                agePersonasSaveDAO.Id.ageLicencCodigo,
                                                Globales.CODIGO_SECUENCIA_PERSONA,
                                                agePersonasSaveDAO.UsuarioIngreso);
            
            if (ConsultarPorIdentificacion(agePersonasSaveDAO.NumeroIdentificacion) != null)
                throw new UniqueFieldException("Identificación", "Persona con identificación " + agePersonasSaveDAO.NumeroIdentificacion + " ya existe.");

            ValidarKeys(agePersonasSaveDAO);

            AgePersonas entityObject = FromDAOToEntity(_httpContextAccessor, agePersonasSaveDAO);

            Validator.ValidateObject(entityObject, new ValidationContext(entityObject), true);

            return entityObject;
        }

        private AgePersonas ValidarUpdate(
            IHttpContextAccessor _httpContextAccessor,
            AgePersonasSaveDAO agePersonasSaveDAO)
        {
            if (agePersonasSaveDAO == null)
                throw new InvalidSintaxisException();

            if (agePersonasSaveDAO.UsuarioModificacion == null || agePersonasSaveDAO.UsuarioModificacion <= 0)
                throw new InvalidFieldException("El usuario de modificación debe ser un número entero mayor a cero.");

            if (agePersonasSaveDAO.FechaNacimiento >= DateTime.Now)
                throw new InvalidFieldException("La fecha de nacimiento debe ser menor a la actual.");
            
            AgePersonas? agePersona = ConsultarCompletePorId(new AgePersonasPKDAO
            {
                ageLicencCodigo = agePersonasSaveDAO.Id.ageLicencCodigo,
                codigo = agePersonasSaveDAO.Id.codigo
            }) ?? throw new RegisterNotFoundException("Persona con código " + agePersonasSaveDAO.Id.codigo + " no existe.");

            if (!string.IsNullOrWhiteSpace(agePersonasSaveDAO.NumeroIdentificacion)) 
            {
                if (ConsultarPorIdentificacion(agePersonasSaveDAO.NumeroIdentificacion) != null &&
                    !agePersona.NumeroIdentificacion.Equals(agePersonasSaveDAO.NumeroIdentificacion))
                    throw new UniqueFieldException("Identificación", "Persona con identificación " + agePersonasSaveDAO.NumeroIdentificacion + " ya existe.");
            }

            if (agePersonasSaveDAO.AgeLocaliCodigo != null ||
                agePersonasSaveDAO.AgeLocaliAgeTipLoCodigo != null ||
                agePersonasSaveDAO.AgeAgeTipLoAgePaisCodigo != null)
            {
                ValidarFKLocalidad(agePersonasSaveDAO.AgeLocaliCodigo ?? 0, 
                                   agePersonasSaveDAO.AgeLocaliAgeTipLoCodigo ?? 0, 
                                   agePersonasSaveDAO.AgeAgeTipLoAgePaisCodigo ?? 0);
                agePersona.AgeLocaliCodigo = agePersonasSaveDAO.AgeLocaliCodigo;
                agePersona.AgeLocaliAgeTipLoCodigo = agePersonasSaveDAO.AgeLocaliAgeTipLoCodigo;
                agePersona.AgeAgeTipLoAgePaisCodigo = agePersonasSaveDAO.AgeAgeTipLoAgePaisCodigo;
            }

            if (agePersonasSaveDAO.AgeTipIdCodigo > 0)
            {
                ValidarFKTipoIdentificacion(agePersonasSaveDAO.AgeTipIdCodigo);
                agePersona.AgeTipIdCodigo = agePersonasSaveDAO.AgeTipIdCodigo;
            }

            if (!string.IsNullOrWhiteSpace(agePersonasSaveDAO.Estado))
            {
                agePersona.Estado = agePersonasSaveDAO.Estado;
                agePersona.FechaEstado = DateTime.Now;
            }
           
            if (!string.IsNullOrWhiteSpace(agePersonasSaveDAO.Nombres))
                agePersona.Nombres = agePersonasSaveDAO.Nombres;

            if (!string.IsNullOrWhiteSpace(agePersonasSaveDAO.Apellidos))
                agePersona.Apellidos = agePersonasSaveDAO.Apellidos;

            if (!string.IsNullOrWhiteSpace(agePersonasSaveDAO.RazonSocial))
                agePersona.RazonSocial = agePersonasSaveDAO.RazonSocial;

            if (!string.IsNullOrWhiteSpace(agePersonasSaveDAO.NombreComercial))
                agePersona.NombreComercial = agePersonasSaveDAO.NombreComercial;

            if (!string.IsNullOrWhiteSpace(agePersonasSaveDAO.NumeroIdentificacion))
                agePersona.NumeroIdentificacion = agePersonasSaveDAO.NumeroIdentificacion;

            if (!string.IsNullOrWhiteSpace(agePersonasSaveDAO.CorreoElectronico))
                agePersona.CorreoElectronico = agePersonasSaveDAO.CorreoElectronico;

            if (!string.IsNullOrWhiteSpace(agePersonasSaveDAO.TelefonoPrincipal))
                agePersona.TelefonoPrincipal = agePersonasSaveDAO.TelefonoPrincipal;

            if (!string.IsNullOrWhiteSpace(agePersonasSaveDAO.TelefonoCelular1))
                agePersona.TelefonoCelular1 = agePersonasSaveDAO.TelefonoCelular1;

            if (!string.IsNullOrWhiteSpace(agePersonasSaveDAO.TelefonoCelular2))
                agePersona.TelefonoCelular2 = agePersonasSaveDAO.TelefonoCelular2;

            if (!string.IsNullOrWhiteSpace(agePersonasSaveDAO.DireccionPrincipal))
                agePersona.DireccionPrincipal = agePersonasSaveDAO.DireccionPrincipal;

            if (!string.IsNullOrWhiteSpace(agePersonasSaveDAO.EstadoCivil))
                agePersona.EstadoCivil = agePersonasSaveDAO.EstadoCivil;

            if (!string.IsNullOrWhiteSpace(agePersonasSaveDAO.Sexo))
                agePersona.Sexo = agePersonasSaveDAO.Sexo;

            if (agePersonasSaveDAO.FechaNacimiento < DateTime.Now)
                agePersona.FechaNacimiento = agePersonasSaveDAO.FechaNacimiento;

            if (!string.IsNullOrWhiteSpace(agePersonasSaveDAO.ObservacionEstado))
                agePersona.ObservacionEstado = agePersonasSaveDAO.ObservacionEstado;

            agePersona.FechaModificacion = DateTime.Now;
            agePersona.UbicacionModificacion = Ubicacion.getIpAdress(_httpContextAccessor);
            agePersona.UsuarioModificacion = agePersonasSaveDAO.UsuarioModificacion;

            Validator.ValidateObject(agePersona, new ValidationContext(agePersona), true);

            return agePersona;
        }


        private AgePersonas FromDAOToEntity(
            IHttpContextAccessor _httpContextAccessor,
            AgePersonasSaveDAO agePersonasSaveDAO)
        {
            return new AgePersonas
            {
                AgeLicencCodigo = agePersonasSaveDAO.Id.ageLicencCodigo,
                Codigo = agePersonasSaveDAO.Id.codigo,
                AgeTipIdCodigo = agePersonasSaveDAO.AgeTipIdCodigo,
                AgeLocaliAgeTipLoCodigo = agePersonasSaveDAO.AgeLocaliAgeTipLoCodigo,
                AgeLocaliCodigo = agePersonasSaveDAO.AgeLocaliCodigo,
                AgeAgeTipLoAgePaisCodigo = agePersonasSaveDAO.AgeAgeTipLoAgePaisCodigo,
                NumeroIdentificacion = agePersonasSaveDAO.NumeroIdentificacion,
                Nombres = agePersonasSaveDAO.Nombres,
                Apellidos = agePersonasSaveDAO.Apellidos,
                RazonSocial = agePersonasSaveDAO.RazonSocial,
                NombreComercial = agePersonasSaveDAO.NombreComercial,
                TelefonoCelular1 = agePersonasSaveDAO.TelefonoCelular1,
                TelefonoCelular2 = agePersonasSaveDAO.TelefonoCelular2,
                FechaNacimiento = agePersonasSaveDAO.FechaNacimiento,
                DireccionPrincipal = agePersonasSaveDAO.DireccionPrincipal,
                CorreoElectronico = agePersonasSaveDAO.CorreoElectronico,
                Sexo = agePersonasSaveDAO.Sexo,
                EstadoCivil = agePersonasSaveDAO.EstadoCivil,
                TelefonoPrincipal = agePersonasSaveDAO.TelefonoPrincipal,
                Estado = agePersonasSaveDAO.Estado,
                FechaEstado = DateTime.Now,
                FechaIngreso = DateTime.Now,
                ObservacionEstado = agePersonasSaveDAO.ObservacionEstado,
                UbicacionIngreso = Ubicacion.getIpAdress(_httpContextAccessor),
                UsuarioIngreso = agePersonasSaveDAO.UsuarioIngreso,
            };
        }


        private void ValidarKeys(AgePersonasSaveDAO agePersonasSaveDAO)
        {
            ValidarPK(agePersonasSaveDAO.Id.codigo, agePersonasSaveDAO.Id.ageLicencCodigo);

            ValidarFKLicenciatario(agePersonasSaveDAO.Id.ageLicencCodigo);

            ValidarFKTipoIdentificacion(agePersonasSaveDAO.AgeTipIdCodigo);

            if (agePersonasSaveDAO.AgeLocaliCodigo != null ||
                agePersonasSaveDAO.AgeLocaliAgeTipLoCodigo != null ||
                agePersonasSaveDAO.AgeAgeTipLoAgePaisCodigo != null)
                ValidarFKLocalidad(agePersonasSaveDAO.AgeLocaliCodigo ?? 0,
                                   agePersonasSaveDAO.AgeLocaliAgeTipLoCodigo ?? 0,
                                   agePersonasSaveDAO.AgeAgeTipLoAgePaisCodigo ?? 0);
        }

        private void ValidarPK(long codigo, int codigoLicenciatario)
        {
            AgePersonasPKDAO agePersonasPKDAO = new AgePersonasPKDAO
            {
                codigo = codigo,
                ageLicencCodigo = codigoLicenciatario
            };
            string mensaje = $"Persona con código: {agePersonasPKDAO.codigo}" +
                                $" y código licenciatario: {agePersonasPKDAO.ageLicencCodigo} no existe.";
            ValidateKeys.ValidarNoExistenciaKey(agePersonasPKDAO, mensaje, _repository.ConsultarCompletePorId);
        }

        private void ValidarFKLicenciatario(int ageSucursAgeLicencCodigo)
        {
            string mensaje = $"Licenciatario con código: {ageSucursAgeLicencCodigo} no existe.";
            ValidateKeys.ValidarExistenciaKey(ageSucursAgeLicencCodigo, mensaje, _repositoryLicenciatarios.ConsultarCompletePorId);
        }

        private void ValidarFKTipoIdentificacion(int codigo)
        {
            string mensaje = $"Tipo identificación con código: {codigo} no existe.";
            ValidateKeys.ValidarExistenciaKey(codigo, mensaje, _repositoryTipoIdentificacion.ConsultarCompletePorId);
        }

        private void ValidarFKLocalidad(int AgeLocaliCodigo, int AgeLocaliAgeTipLoCodigo,int AgeAgeTipLoAgePaisCodigo)
        {
            AgeLocalidadesPKDAO ageLocalidadPKDAO = new()
            {
                Codigo = AgeLocaliCodigo,
                AgeTipLoAgePaisCodigo = AgeAgeTipLoAgePaisCodigo,
                AgeTipLoCodigo = AgeLocaliAgeTipLoCodigo,
            };

            string mensaje = $"Localidad con código: {ageLocalidadPKDAO.Codigo}" +
                            $", código país: {ageLocalidadPKDAO.AgeTipLoAgePaisCodigo} " +
                            $" y código tipo localidad: {ageLocalidadPKDAO.AgeTipLoCodigo} no existe.";
            ValidateKeys.ValidarExistenciaKey(ageLocalidadPKDAO, mensaje, _repositoryLocalidad.ConsultarCompletePorId);
        }
    }
}
