﻿using AGE.Entities.DAO.AgeLogUsuarios;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;

namespace AGE.Services.AgeLogUsuariosService
{
    public interface IAgeLogUsuariosService
    {
        Task<Page<AgeLogUsuariosDAO>> ConsultarTodos(int ageUsuariAgeLicencCodigo,
                                                    int ageUsuariCodigo,
                                                    int codigo,
                                                    string campo,
                                                    string valorAnterior,
                                                    string valorActual,
                                                    Pageable pageable);

        Task<AgeLogUsuariosDAO> ConsultarPorId(AgeLogUsuariosPKDAO ageLogUsuariosPKDAO);

        Task<Page<AgeLogUsuariosDAO>> ConsultarListaFiltro(int codigoLicenciatario,
                                                                string filtro,
                                                                Pageable pageable);

        Task<Resource<AgeLogUsuariosDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgeLogUsuariosSaveDAO AgeLogUsuariosSaveDAO);

        Task<List<Resource<AgeLogUsuariosDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeLogUsuariosSaveDAO> AgeLogUsuariosSaveDAOList);

        Task<AgeLogUsuariosDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeLogUsuariosSaveDAO AgeLogUsuariosSaveDAO);

        Task<List<AgeLogUsuariosDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeLogUsuariosSaveDAO> AgeLogUsuariosSaveDAOList);
    }
}
