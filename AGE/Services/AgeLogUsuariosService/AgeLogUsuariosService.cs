﻿using AGE.Controllers;
using AGE.Entities.DAO.AgeLogUsuarios;
using AGE.Entities;
using AGE.Middleware.Exceptions.BadRequest;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Repositories.AgeLogUsuariosRepository;
using AGE.Services.AgeLicenciatariosAplicaSecuService;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using AGE.Utils;
using System.ComponentModel.DataAnnotations;
using System.Transactions;
using AGE.Repositories.AgeUsuarioRepository;
using AGE.Entities.DAO.AgeUsuarios;

namespace AGE.Services.AgeLogUsuariosService
{
    public class AgeLogUsuariosService : IAgeLogUsuariosService
    {
        private readonly IAgeLogUsuariosRepository _repository;
        private readonly IAgeUsuariosRepository _repositoryUsuario;
        private readonly IAgeLicenciatariosAplicaSecuService _ageLicenciatariosAplicaSecuService;

        public AgeLogUsuariosService(
            IAgeLogUsuariosRepository Repository,
            IAgeUsuariosRepository RepositoryUsuario,
            IAgeLicenciatariosAplicaSecuService ageLicenciatariosAplicaSecuService)
        {
            _repository = Repository;
            _repositoryUsuario = RepositoryUsuario;
            _ageLicenciatariosAplicaSecuService = ageLicenciatariosAplicaSecuService;
        }

        public Task<Page<AgeLogUsuariosDAO>> ConsultarTodos(int ageUsuariAgeLicencCodigo,
                                                            int ageUsuariCodigo,
                                                            int codigo,
                                                            string campo,
                                                            string valorAnterior,
                                                            string valorActual,
                                                            Pageable pageable)
        {
            if (ageUsuariAgeLicencCodigo <= 0)
                throw new InvalidFieldException("El código de licenciatario debe ser un número entero mayor a cero.");

            pageable.Validar<AgeLogUsuariosDAO>();

            return _repository.ConsultarTodos(ageUsuariAgeLicencCodigo, ageUsuariCodigo, codigo, campo, valorAnterior, valorActual, pageable);
        }

        public Task<AgeLogUsuariosDAO> ConsultarPorId(AgeLogUsuariosPKDAO ageLogUsuarios)
        {
            Validator.ValidateObject(ageLogUsuarios, new ValidationContext(ageLogUsuarios), true);

            AgeLogUsuariosDAO ageLogUsuariosDAO = _repository.ConsultarPorId(ageLogUsuarios).Result;

            return ageLogUsuariosDAO == null
                ? throw new RegisterNotFoundException("Log Usuario con código: " + ageLogUsuarios.Codigo + " no existe.")
                : Task.FromResult(ageLogUsuariosDAO);
        }

        private AgeLogUsuarios ConsultarCompletePorId(AgeLogUsuariosPKDAO ageLogUsuarioPKDAO)
        {
            Validator.ValidateObject(ageLogUsuarioPKDAO, new ValidationContext(ageLogUsuarioPKDAO), true);

            AgeLogUsuarios ageLogUsuario = _repository.ConsultarCompletePorId(ageLogUsuarioPKDAO).Result;

            return ageLogUsuario;
        }

        public Task<Page<AgeLogUsuariosDAO>> ConsultarListaFiltro(int CodigoLicenciatario, string filtro, Pageable pageable)
        {
            if (CodigoLicenciatario <= 0)
                throw new InvalidFieldException("El código de licenciatario debe ser un número entero mayor a cero.");

            if (string.IsNullOrWhiteSpace(filtro))
                throw new InvalidFieldException("El filtro es requerido.");

            pageable.Validar<AgeLogUsuariosDAO>();

            return _repository.ConsultarListaFiltro(CodigoLicenciatario, filtro, pageable);
        }

        public async Task<Resource<AgeLogUsuariosDAO>> Insertar(IHttpContextAccessor _httpContextAccessor,
                                                    AgeLogUsuariosSaveDAO AgeLogUsuariosSaveDAO)
        {
            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    AgeLogUsuarios ageLogUsuarios = ValidarInsert(_httpContextAccessor, AgeLogUsuariosSaveDAO);
                    AgeLogUsuariosDAO ageLogUsuariosDAO = await _repository.Insertar(ageLogUsuarios);

                    Resource<AgeLogUsuariosDAO> ageLogUsuariosDAOWithResource = GetDataWithResource(_httpContextAccessor, ageLogUsuariosDAO);

                    transactionScope.Complete();

                    return ageLogUsuariosDAOWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }

        }

        public async Task<List<Resource<AgeLogUsuariosDAO>>> InsertarVarios(IHttpContextAccessor _httpContextAccessor,
                                                                List<AgeLogUsuariosSaveDAO> AgeLogUsuariosSaveDAOList)
        {
            if (AgeLogUsuariosSaveDAOList == null)
                throw new InvalidSintaxisException();
            
            List<AgeLogUsuarios> ageLogUsuariosList = new();

            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    foreach (AgeLogUsuariosSaveDAO ageLogUsuariosSaveDAO in AgeLogUsuariosSaveDAOList)
                    {
                        AgeLogUsuarios ageLogUsuarios = ValidarInsert(_httpContextAccessor, ageLogUsuariosSaveDAO);
                        ageLogUsuariosList.Add(ageLogUsuarios);
                    }

                    List<AgeLogUsuariosDAO> ageLogUsuariosDAOList = await _repository.InsertarVarios(ageLogUsuariosList);

                    List<Resource<AgeLogUsuariosDAO>> ageLogUsuariosDAOListWithResource = new();

                    foreach (AgeLogUsuariosDAO ageLogUsuariosDAO in ageLogUsuariosDAOList)
                    {
                        ageLogUsuariosDAOListWithResource.Add(GetDataWithResource(_httpContextAccessor, ageLogUsuariosDAO));
                    }

                    transactionScope.Complete();

                    return ageLogUsuariosDAOListWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public Task<AgeLogUsuariosDAO> Actualizar(IHttpContextAccessor _httpContextAccessor,
            AgeLogUsuariosSaveDAO AgeLogUsuariosSaveDAO)
        {
            AgeLogUsuarios ageLogUsuarios = ValidarUpdate(_httpContextAccessor, AgeLogUsuariosSaveDAO);

            return _repository.Actualizar(ageLogUsuarios);
        }

        public Task<List<AgeLogUsuariosDAO>> ActualizarVarios(IHttpContextAccessor _httpContextAccessor,
                                                                List<AgeLogUsuariosSaveDAO> AgeLogUsuariosSaveDAOList)
        {
            if (AgeLogUsuariosSaveDAOList == null)
                throw new InvalidSintaxisException();

            ValidateKeys.ValidarPKDuplicadas(AgeLogUsuariosSaveDAOList, p => new { p.Id.AgeUsuariCodigo, p.Id.AgeUsuariAgeLicencCodigo, p.Id.Codigo }, "Id");

            List<AgeLogUsuarios> ageLogUsuariosList = new();
            AgeLogUsuarios ageLogUsuarios;

            foreach (AgeLogUsuariosSaveDAO ageLogUsuariosSave in AgeLogUsuariosSaveDAOList)
            {
                ageLogUsuarios = ValidarUpdate(_httpContextAccessor, ageLogUsuariosSave);
                ageLogUsuariosList.Add(ageLogUsuarios);
            }

            return _repository.ActualizarVarios(ageLogUsuariosList);
        }

        private static Resource<AgeLogUsuariosDAO> GetDataWithResource(
            IHttpContextAccessor _httpContextAccessor,
            AgeLogUsuariosDAO ageLogUsuariosDAO)
        {
            string rutaSegmentoFinal = $"{ageLogUsuariosDAO.Id.AgeUsuariAgeLicencCodigo}/{ageLogUsuariosDAO.Id.AgeUsuariCodigo}/{ageLogUsuariosDAO.Id.Codigo}";

            return Resource<AgeLogUsuariosDAO>.GetDataWithResource<AgeLogUsuariosController>(
                _httpContextAccessor, rutaSegmentoFinal, ageLogUsuariosDAO);
        }

        private AgeLogUsuarios ValidarInsert(
            IHttpContextAccessor _httpContextAccessor,
            AgeLogUsuariosSaveDAO ageLogUsuariosSaveDAO)
        {
            if (ageLogUsuariosSaveDAO == null) throw new InvalidSintaxisException();

            Validator.ValidateObject(ageLogUsuariosSaveDAO, new ValidationContext(ageLogUsuariosSaveDAO), true);

            FuncionesSecuencias fg = new(_ageLicenciatariosAplicaSecuService);

            ageLogUsuariosSaveDAO.Id.Codigo = fg.ObtenerSecuencia(_httpContextAccessor,
                ageLogUsuariosSaveDAO.Id.AgeUsuariAgeLicencCodigo,
                Globales.CODIGO_SECUENCIA_LOG_USUARIO,
                ageLogUsuariosSaveDAO.UsuarioIngreso);

            ValidarKeys(ageLogUsuariosSaveDAO.Id);

            AgeLogUsuarios entityObject = FromDAOToEntity(_httpContextAccessor,ageLogUsuariosSaveDAO);

            Validator.ValidateObject(entityObject, new ValidationContext(entityObject), true);

            return entityObject;
        }

        private AgeLogUsuarios ValidarUpdate(IHttpContextAccessor httpContextAccessor, AgeLogUsuariosSaveDAO ageLogUsuariosSaveDAO)
        {
            if (ageLogUsuariosSaveDAO == null)
                throw new InvalidSintaxisException();

            if (ageLogUsuariosSaveDAO.UsuarioModificacion == null || ageLogUsuariosSaveDAO.UsuarioModificacion <= 0)
                throw new InvalidFieldException("El usuario de modificación debe ser un número entero mayor a cero.");

            if (ageLogUsuariosSaveDAO.Id == null)
                throw new InvalidFieldException("El código de licenciatario y el código de modalidad no pueden ser nulos.");

            AgeLogUsuarios ageLogUsuarios = ConsultarCompletePorId(ageLogUsuariosSaveDAO.Id) 
                ?? throw new RegisterNotFoundException($"Log Usuario con código: { ageLogUsuariosSaveDAO.Id.Codigo }, " +
            $" código licenciatario: {ageLogUsuariosSaveDAO.Id.AgeUsuariAgeLicencCodigo} " +
            $"y código de usuario: {ageLogUsuariosSaveDAO.Id.AgeUsuariCodigo} no existe.");
            
            ageLogUsuarios.Estado = !string.IsNullOrWhiteSpace(ageLogUsuariosSaveDAO.Estado) ? ageLogUsuariosSaveDAO.Estado : ageLogUsuarios.Estado;
            ageLogUsuarios.FechaEstado = !string.IsNullOrWhiteSpace(ageLogUsuariosSaveDAO.Estado) ? DateTime.Now : ageLogUsuarios.FechaEstado;
            ageLogUsuarios.Campo = !string.IsNullOrWhiteSpace(ageLogUsuariosSaveDAO.Campo) ? ageLogUsuariosSaveDAO.Campo : ageLogUsuarios.Campo;
            ageLogUsuarios.ObservacionEstado = !string.IsNullOrWhiteSpace(ageLogUsuariosSaveDAO.ObservacionEstado) ? ageLogUsuariosSaveDAO.ObservacionEstado : ageLogUsuarios.ObservacionEstado;
            ageLogUsuarios.ValorActual = !string.IsNullOrWhiteSpace(ageLogUsuariosSaveDAO.ValorActual) ? ageLogUsuarios.ValorActual : ageLogUsuarios.ValorActual;
            ageLogUsuarios.ValorAnterior = !string.IsNullOrWhiteSpace(ageLogUsuariosSaveDAO.ValorAnterior) ? ageLogUsuarios.ValorAnterior : ageLogUsuarios.ValorAnterior;

            ageLogUsuarios.FechaModificacion = DateTime.Now;
            ageLogUsuarios.UbicacionModificacion = Ubicacion.getIpAdress(httpContextAccessor);
            ageLogUsuarios.UsuarioModificacion = ageLogUsuariosSaveDAO.UsuarioModificacion;

            Validator.ValidateObject(ageLogUsuarios, new ValidationContext(ageLogUsuarios), true);
            return ageLogUsuarios;
        }

        private AgeLogUsuarios FromDAOToEntity(
           IHttpContextAccessor _httpContextAccessor,
           AgeLogUsuariosSaveDAO ageLogUsuariosSaveDAO)
        {

            return new AgeLogUsuarios()
            {
                AgeUsuariAgeLicencCodigo = ageLogUsuariosSaveDAO.Id.AgeUsuariAgeLicencCodigo,
                AgeUsuariCodigo = ageLogUsuariosSaveDAO.Id.AgeUsuariCodigo,
                Codigo = ageLogUsuariosSaveDAO.Id.Codigo,
                Campo = ageLogUsuariosSaveDAO.Campo,
                ValorActual = ageLogUsuariosSaveDAO.ValorActual,
                ValorAnterior = ageLogUsuariosSaveDAO.ValorAnterior,
                Estado = ageLogUsuariosSaveDAO.Estado,
                FechaEstado = DateTime.Now,
                FechaIngreso = DateTime.Now,
                ObservacionEstado = ageLogUsuariosSaveDAO.ObservacionEstado,
                UbicacionIngreso = Ubicacion.getIpAdress(_httpContextAccessor),
                UsuarioIngreso = ageLogUsuariosSaveDAO.UsuarioIngreso
            };
        }

        private void ValidarKeys(AgeLogUsuariosPKDAO ageLogUsuariosPKDAO)
        {
            ValidarPK(ageLogUsuariosPKDAO);
            ValidarFkUsuario(new AgeUsuarioPKDAO
            {
                ageLicencCodigo = ageLogUsuariosPKDAO.AgeUsuariAgeLicencCodigo,
                codigo = ageLogUsuariosPKDAO.AgeUsuariCodigo
            });
        } 

        private void ValidarPK(AgeLogUsuariosPKDAO ageLogUsuariosPKDAO)
        {
            ValidateKeys.ValidarNoExistenciaKey(ageLogUsuariosPKDAO, $"Log Usuario con código: {ageLogUsuariosPKDAO.Codigo}, " +
                $"código licenciatario: {ageLogUsuariosPKDAO.AgeUsuariAgeLicencCodigo} " +
                $"y código de usuario: {ageLogUsuariosPKDAO.AgeUsuariCodigo}  ya existe."
                , _repository.ConsultarCompletePorId);
        }

        private void ValidarFkUsuario(AgeUsuarioPKDAO ageUsuarioPKDAO)
        {
            ValidateKeys.ValidarExistenciaKey(ageUsuarioPKDAO, $"Usuario con código: {ageUsuarioPKDAO.codigo} " +
                $"y código licenciatario: {ageUsuarioPKDAO.ageLicencCodigo} no existe."
                , _repositoryUsuario.ConsultarCompletePorId);
        }
    }
}
