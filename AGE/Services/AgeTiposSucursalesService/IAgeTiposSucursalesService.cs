﻿using AGE.Entities.DAO.AgeTiposSucursales;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;

namespace AGE.Services.AgeTiposSucursaleService
{
    public interface IAgeTiposSucursalesService
    {
        Task<Page<AgeTiposSucursaleDAO>> ConsultarTodos(
        int codigoLicenciatario,
        int codigo,
        string descripcion,
        Pageable pageable);

        Task<Page<AgeTiposSucursaleDAO>> ConsultarListaFiltro(
            int codigoLicenciatario,
            string filtro,
            Pageable pageable);

        Task<AgeTiposSucursaleDAO> ConsultarPorId(
            AgeTiposSucursalePKDAO AgeTiposSucursalePKDAO);

        Task<Resource<AgeTiposSucursaleDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgeTiposSucursaleSaveDAO AgeTiposSucursaleSaveDAO);

        Task<List<Resource<AgeTiposSucursaleDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeTiposSucursaleSaveDAO> AgeTiposSucursaleSaveDAOList);

        Task<AgeTiposSucursaleDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeTiposSucursaleSaveDAO AgeTiposSucursaleSaveDAO);

        Task<List<AgeTiposSucursaleDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeTiposSucursaleSaveDAO> AgeTiposSucursaleSaveDAOList);
    }
}
