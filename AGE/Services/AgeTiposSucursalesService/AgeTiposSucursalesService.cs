using AGE.Controllers;
using AGE.Entities;
using AGE.Middleware.Exceptions.BadRequest;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Services.AgeLicenciatariosAplicaSecuService;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using AGE.Utils;
using System.ComponentModel.DataAnnotations;
using AGE.Repositories.AgeTiposSucursaleRepository;
using AGE.Entities.DAO.AgeTiposSucursales;
using System.Transactions;
using AGE.Repositories.AgeLicenciatariosRepository;

namespace AGE.Services.AgeTiposSucursaleService
{
    public class AgeTiposSucursalesService : IAgeTiposSucursalesService
    {
        private readonly IAgeTiposSucursalesRepository _repository;

        private readonly IAgeLicenciatariosAplicaSecuService _ageLicenciatariosAplicaSecuService;
        private readonly IAgeLicenciatariosRepository _repositoryLicenciatario;


        public AgeTiposSucursalesService(
            IAgeTiposSucursalesRepository Repository,
            IAgeLicenciatariosAplicaSecuService ageLicenciatariosAplicaSecuService,
            IAgeLicenciatariosRepository repositoryLicenciatario)
        {
            _repository = Repository;
            _ageLicenciatariosAplicaSecuService = ageLicenciatariosAplicaSecuService;
            _repositoryLicenciatario = repositoryLicenciatario;
        }


        public Task<Page<AgeTiposSucursaleDAO>> ConsultarTodos(
            int codigoLicenciatario,
            int codigo,
            string descripcion,
            Pageable pageable)
        {
            if (codigoLicenciatario <= 0)
                throw new InvalidFieldException("El código de licenciatario debe ser un número entero mayor a cero.");

            pageable.Validar<AgeTiposSucursaleDAO>();

            return _repository.ConsultarTodos(codigoLicenciatario, codigo, descripcion, pageable);
        }


        public Task<Page<AgeTiposSucursaleDAO>> ConsultarListaFiltro(
            int codigoLicenciatario,
            string filtro,
            Pageable pageable)
        {
            if (codigoLicenciatario == 0)
                throw new InvalidFieldException("El código de licenciatario debe ser un número entero mayor a cero.");

            if (string.IsNullOrWhiteSpace(filtro))
                throw new InvalidFieldException("El filtro es requerido.");

            pageable.Validar<AgeTiposSucursaleDAO>();

            return _repository.ConsultarListaFiltro(codigoLicenciatario, filtro.ToLower(), pageable);
        }


        public Task<AgeTiposSucursaleDAO> ConsultarPorId(
            AgeTiposSucursalePKDAO ageTiposSucursalePKDAO)
        {
            Validator.ValidateObject(ageTiposSucursalePKDAO, new ValidationContext(ageTiposSucursalePKDAO), true);

            AgeTiposSucursaleDAO ageTiposSucursaleDAO = _repository.ConsultarPorId(ageTiposSucursalePKDAO).Result;

            if (ageTiposSucursaleDAO == null)
                throw new RegisterNotFoundException("Tipo de Sucursal con código " + ageTiposSucursalePKDAO.codigo + " no existe");

            return Task.FromResult(ageTiposSucursaleDAO);
        }


        private AgeTiposSucursales ConsultarCompletePorId(
            AgeTiposSucursalePKDAO ageTiposSucursalePKDAO)
        {
            Validator.ValidateObject(ageTiposSucursalePKDAO, new ValidationContext(ageTiposSucursalePKDAO), true);

            AgeTiposSucursales? ageTiposSucursale = _repository.ConsultarCompletePorId(ageTiposSucursalePKDAO).Result;

            return ageTiposSucursale;
        }

        public async Task<Resource<AgeTiposSucursaleDAO>> Insertar(
    IHttpContextAccessor _httpContextAccessor,
    AgeTiposSucursaleSaveDAO ageTiposSucursaleSaveDAO)
        {
            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    AgeTiposSucursales ageTiposSucursale = ValidarInsert(_httpContextAccessor, ageTiposSucursaleSaveDAO);

                    AgeTiposSucursaleDAO ageTiposSucursaleDAO = await _repository.Insertar(ageTiposSucursale);

                    Resource<AgeTiposSucursaleDAO> ageTiposSucursaleDAOWithResource = GetDataWithResource(_httpContextAccessor, ageTiposSucursaleDAO);

                    transactionScope.Complete();

                    return ageTiposSucursaleDAOWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public async Task<List<Resource<AgeTiposSucursaleDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeTiposSucursaleSaveDAO> ageTiposSucursaleSaveDAOList)
        {
            if (ageTiposSucursaleSaveDAOList == null)
            {
                throw new InvalidSintaxisException();
            }

            List<AgeTiposSucursales> ageTiposSucursaleList = new List<AgeTiposSucursales>();

            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    foreach (AgeTiposSucursaleSaveDAO ageTiposSucursaleSaveDAO in ageTiposSucursaleSaveDAOList)
                    {
                        AgeTiposSucursales ageTiposSucursale = ValidarInsert(_httpContextAccessor, ageTiposSucursaleSaveDAO);
                        ageTiposSucursaleList.Add(ageTiposSucursale);
                    }

                    List<AgeTiposSucursaleDAO> ageTiposSucursaleDAOList = await _repository.InsertarVarios(ageTiposSucursaleList);

                    List<Resource<AgeTiposSucursaleDAO>> ageTiposSucursaleDAOListWithResource = new List<Resource<AgeTiposSucursaleDAO>>();

                    foreach (AgeTiposSucursaleDAO ageTiposSucursaleDAO in ageTiposSucursaleDAOList)
                    {
                        ageTiposSucursaleDAOListWithResource.Add(GetDataWithResource(_httpContextAccessor, ageTiposSucursaleDAO));
                    }

                    transactionScope.Complete();

                    return ageTiposSucursaleDAOListWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public Task<AgeTiposSucursaleDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeTiposSucursaleSaveDAO ageTiposSucursaleSaveDAO)
        {
            AgeTiposSucursales ageTiposSucursale = ValidarUpdate(_httpContextAccessor, ageTiposSucursaleSaveDAO);

            return _repository.Actualizar(ageTiposSucursale);
        }

        public Task<List<AgeTiposSucursaleDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeTiposSucursaleSaveDAO> ageTiposSucursaleSaveDAOList)
        {
            if (ageTiposSucursaleSaveDAOList == null)
                throw new InvalidSintaxisException();

            ValidateKeys.ValidarPKDuplicadas(ageTiposSucursaleSaveDAOList,
                                             p => new { p.Id.ageLicencCodigo, p.Id.codigo },
                                             "Id");

            List<AgeTiposSucursales> ageTiposSucursaleList = new List<AgeTiposSucursales>();
            AgeTiposSucursales ageTiposSucursale;

            foreach (AgeTiposSucursaleSaveDAO ageTiposSucursaleSaveDAO in ageTiposSucursaleSaveDAOList)
            {
                ageTiposSucursale = ValidarUpdate(_httpContextAccessor, ageTiposSucursaleSaveDAO);
                ageTiposSucursaleList.Add(ageTiposSucursale);
            }

            return _repository.ActualizarVarios(ageTiposSucursaleList);
        }


        private static Resource<AgeTiposSucursaleDAO> GetDataWithResource(
            IHttpContextAccessor _httpContextAccessor,
            AgeTiposSucursaleDAO ageTiposSucursaleDAO)
        {
            string tsucursaleSegmentoFinal = $"{ageTiposSucursaleDAO.Id.ageLicencCodigo}/{ageTiposSucursaleDAO.Id.codigo}";

            return Resource<AgeTiposSucursaleDAO>.GetDataWithResource<AgeTiposSucursalesController>(
                _httpContextAccessor, tsucursaleSegmentoFinal, ageTiposSucursaleDAO);
        }

        private AgeTiposSucursales ValidarInsert(
            IHttpContextAccessor _httpContextAccessor,
            AgeTiposSucursaleSaveDAO ageTiposSucursaleSaveDAO)
        {

            if (ageTiposSucursaleSaveDAO == null)
                throw new InvalidSintaxisException();

            Validator.ValidateObject(ageTiposSucursaleSaveDAO, new ValidationContext(ageTiposSucursaleSaveDAO), true);

            FuncionesSecuencias fg = new FuncionesSecuencias(_ageLicenciatariosAplicaSecuService);

            ageTiposSucursaleSaveDAO.Id.codigo = fg.ObtenerSecuencia(_httpContextAccessor,
                                                    ageTiposSucursaleSaveDAO.Id.ageLicencCodigo,
                                                    Globales.CODIGO_SECUENCIA_TIPO_SUCURSAL,
                                                    ageTiposSucursaleSaveDAO.UsuarioIngreso);

            ValidarKeys(ageTiposSucursaleSaveDAO);

            AgeTiposSucursales entityObject = FromDAOToEntity(_httpContextAccessor, ageTiposSucursaleSaveDAO);

            Validator.ValidateObject(entityObject, new ValidationContext(entityObject), true);

            return entityObject;
        }


        private AgeTiposSucursales ValidarUpdate(
            IHttpContextAccessor _httpContextAccessor,
            AgeTiposSucursaleSaveDAO ageTiposSucursaleSaveDAO)
        {
            if (ageTiposSucursaleSaveDAO == null)
                throw new InvalidSintaxisException();

            if (ageTiposSucursaleSaveDAO.UsuarioModificacion == null || ageTiposSucursaleSaveDAO.UsuarioModificacion <= 0)
                throw new InvalidFieldException("El usuario de modificación debe ser un número entero mayor a cero.");

            AgeTiposSucursales? ageTiposSucursale = ConsultarCompletePorId(new AgeTiposSucursalePKDAO
            {
                ageLicencCodigo = ageTiposSucursaleSaveDAO.Id.ageLicencCodigo,
                codigo = ageTiposSucursaleSaveDAO.Id.codigo
            });

            if (ageTiposSucursale == null)
                throw new RegisterNotFoundException("Tipo de Sucursal con código " + ageTiposSucursaleSaveDAO.Id.codigo + " no existe");

            if (!string.IsNullOrWhiteSpace(ageTiposSucursaleSaveDAO.Estado))
            {
                ageTiposSucursale.Estado = ageTiposSucursaleSaveDAO.Estado;
                ageTiposSucursale.FechaEstado = DateTime.Now;
            }

            if (!string.IsNullOrWhiteSpace(ageTiposSucursaleSaveDAO.Descripcion))
                ageTiposSucursale.Descripcion = ageTiposSucursaleSaveDAO.Descripcion;

            if (!string.IsNullOrWhiteSpace(ageTiposSucursaleSaveDAO.ObservacionEstado))
                ageTiposSucursale.ObservacionEstado = ageTiposSucursaleSaveDAO.ObservacionEstado;

            ageTiposSucursale.FechaModificacion = DateTime.Now;
            ageTiposSucursale.UbicacionModificacion = Ubicacion.getIpAdress(_httpContextAccessor);
            ageTiposSucursale.UsuarioModificacion = ageTiposSucursaleSaveDAO.UsuarioModificacion;

            Validator.ValidateObject(ageTiposSucursale, new ValidationContext(ageTiposSucursale), true);

            return ageTiposSucursale;
        }


        private AgeTiposSucursales FromDAOToEntity(
            IHttpContextAccessor _httpContextAccessor,
            AgeTiposSucursaleSaveDAO ageTiposSucursaleSaveDAO)
        {
            return new AgeTiposSucursales
            {
                AgeLicencCodigo = ageTiposSucursaleSaveDAO.Id.ageLicencCodigo,
                Codigo = ageTiposSucursaleSaveDAO.Id.codigo,
                Descripcion = ageTiposSucursaleSaveDAO.Descripcion,
                Estado = ageTiposSucursaleSaveDAO.Estado,
                FechaEstado = DateTime.Now,
                FechaIngreso = DateTime.Now,
                ObservacionEstado = ageTiposSucursaleSaveDAO.ObservacionEstado,
                UbicacionIngreso = Ubicacion.getIpAdress(_httpContextAccessor),
                UsuarioIngreso = ageTiposSucursaleSaveDAO.UsuarioIngreso
            };
        }


        private void ValidarKeys(AgeTiposSucursaleSaveDAO ageTiposSucursaleSaveDAO)
        {
            ValidarPK(ageTiposSucursaleSaveDAO.Id);

            ValidarFKLicenciatario(ageTiposSucursaleSaveDAO.Id.ageLicencCodigo);

        }


        private void ValidarPK(AgeTiposSucursalePKDAO ageTiposSucursalePKDAO)
        {

            ValidateKeys.ValidarNoExistenciaKey(
               ageTiposSucursalePKDAO,
               $"Tipo sucursal con código {ageTiposSucursalePKDAO.codigo} ya existe.",
               _repository.ConsultarCompletePorId);
        }


        private void ValidarFKLicenciatario(int codigoLicenciatario)
        {

            ValidateKeys.ValidarExistenciaKey(
                codigoLicenciatario,
                $"Licenciatario con código {codigoLicenciatario} no existe.",
                _repositoryLicenciatario.ConsultarCompletePorId);
        }
    }
}
