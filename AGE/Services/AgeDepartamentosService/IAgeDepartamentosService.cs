﻿using AGE.Entities.DAO.AgeDepartamentos;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;

namespace AGE.Services.AgeDepartamentosService
{
    public interface IAgeDepartamentosService
    {
        Task<Page<AgeDepartamentosDAO>> ConsultarTodos(int codigoLicenciatario,
                                                        string? descripcion,
                                                        Pageable pageable);

        Task<AgeDepartamentosDAO> ConsultarPorId(AgeDepartamentosPKDAO ageDepartamentosPKDAO);

        Task<Page<AgeDepartamentosDAO>> ConsultarListaFiltro(int codigoLicenciatario,
                                                            string filtro,
                                                            Pageable pageable);

        Task<Resource<AgeDepartamentosDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgeDepartamentosSaveDAO AgeDepartamentosSaveDAO);

        Task<List<Resource<AgeDepartamentosDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeDepartamentosSaveDAO> AgeDepartamentosSaveDAOList);

        Task<AgeDepartamentosDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeDepartamentosSaveDAO AgeDepartamentosSaveDAO);

        Task<List<AgeDepartamentosDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeDepartamentosSaveDAO> AgeDepartamentosSaveDAOList);
    }
}
