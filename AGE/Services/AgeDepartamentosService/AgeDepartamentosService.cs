﻿using AGE.Controllers;
using AGE.Entities.DAO.AgeDepartamentos;
using AGE.Entities;
using AGE.Middleware.Exceptions.BadRequest;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Repositories.AgeDepartamentosRepository;
using AGE.Services.AgeLicenciatariosAplicaSecuService;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using AGE.Utils;
using System.ComponentModel.DataAnnotations;
using System.Transactions;
using AGE.Repositories.AgeLicenciatariosRepository;

namespace AGE.Services.AgeDepartamentosService
{
    public class AgeDepartamentosService : IAgeDepartamentosService
    {
        private readonly IAgeDepartamentosRepository _repository;
        private readonly IAgeLicenciatariosRepository _repositoryLicenciatarios;
        private readonly IAgeLicenciatariosAplicaSecuService _ageLicenciatariosAplicaSecuService;

        public AgeDepartamentosService(IAgeDepartamentosRepository Repository,
            IAgeLicenciatariosRepository RepositoryLicenciatarios,
            IAgeLicenciatariosAplicaSecuService ageLicenciatariosAplicaSecuService)
        {
            _repository = Repository;
            _repositoryLicenciatarios = RepositoryLicenciatarios;
            _ageLicenciatariosAplicaSecuService = ageLicenciatariosAplicaSecuService;
        }

        public Task<Page<AgeDepartamentosDAO>> ConsultarTodos(int codigoLicenciatario,
                                                                string? descripcion,
                                                                Pageable pageable)
        {
            pageable.Validar<AgeDepartamentosDAO>();

            return _repository.ConsultarTodos(codigoLicenciatario, descripcion, pageable);
        }

        public Task<AgeDepartamentosDAO> ConsultarPorId(
            AgeDepartamentosPKDAO ageDepartamentosPKDAO)
        {

            Validator.ValidateObject(ageDepartamentosPKDAO, new ValidationContext(ageDepartamentosPKDAO), true);

            AgeDepartamentosDAO ageDepartamentosDAO = _repository.ConsultarPorId(ageDepartamentosPKDAO).Result;

            return ageDepartamentosDAO == null
                ? throw new RegisterNotFoundException("Departamento con código: " + ageDepartamentosPKDAO.Codigo + " no existe.")
                : Task.FromResult(ageDepartamentosDAO);
        }

        private AgeDepartamentos ConsultarCompletePorId(AgeDepartamentosPKDAO ageDepartamentosPKDAO)
        {
            Validator.ValidateObject(ageDepartamentosPKDAO, new ValidationContext(ageDepartamentosPKDAO), true);

            AgeDepartamentos? ageDepartamento = _repository.ConsultarCompletePorId(ageDepartamentosPKDAO).Result;

            return ageDepartamento;
        }

        public Task<Page<AgeDepartamentosDAO>> ConsultarListaFiltro(int codigoLicenciatario,
                                                                    string filtro,
                                                                    Pageable pageable)
        {
            if (codigoLicenciatario <= 0)
                throw new InvalidFieldException("El código de licenciatario debe ser un número entero mayor a cero.");

            if (string.IsNullOrWhiteSpace(filtro))
                throw new InvalidFieldException("El filtro es requerido.");

            pageable.Validar<AgeDepartamentosDAO>();

            return _repository.ConsultarListaFiltro(codigoLicenciatario, filtro.ToLower(), pageable);
        }

        public async Task<Resource<AgeDepartamentosDAO>> Insertar(IHttpContextAccessor _httpContextAccessor, AgeDepartamentosSaveDAO AgeDepartamentosSaveDAO)
        {
            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    AgeDepartamentos ageDepartamento = ValidarInsert(_httpContextAccessor, AgeDepartamentosSaveDAO);
                    AgeDepartamentosDAO ageDepartamentosDAO = await _repository.Insertar(ageDepartamento);

                    Resource<AgeDepartamentosDAO> ageDepartamentoDAOWithResource = GetDataWithResource(_httpContextAccessor, ageDepartamentosDAO);

                    transactionScope.Complete();

                    return ageDepartamentoDAOWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public async Task<List<Resource<AgeDepartamentosDAO>>> InsertarVarios(IHttpContextAccessor _httpContextAccessor, List<AgeDepartamentosSaveDAO> AgeDepartamentosSaveDAOList)
        {
            if (AgeDepartamentosSaveDAOList == null)
                throw new InvalidSintaxisException();
            
            List<AgeDepartamentos> ageDepartamentosList = new();

            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    foreach (AgeDepartamentosSaveDAO ageDepartamentosSaveDAO in AgeDepartamentosSaveDAOList)
                    {
                        AgeDepartamentos ageDepartamento = ValidarInsert(_httpContextAccessor, ageDepartamentosSaveDAO);
                        ageDepartamentosList.Add(ageDepartamento);
                    }

                    List<AgeDepartamentosDAO> ageDepartamentosDAOList = await _repository.InsertarVarios(ageDepartamentosList);

                    List<Resource<AgeDepartamentosDAO>> ageDepartamentosDAOListWithResource = new();

                    foreach (AgeDepartamentosDAO ageDepartamentoDAO in ageDepartamentosDAOList)
                    {
                        ageDepartamentosDAOListWithResource.Add(GetDataWithResource(_httpContextAccessor, ageDepartamentoDAO));
                    }

                    transactionScope.Complete();

                    return ageDepartamentosDAOListWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public Task<AgeDepartamentosDAO> Actualizar(IHttpContextAccessor _httpContextAccessor, AgeDepartamentosSaveDAO AgeDepartamentosSaveDAO)
        {
            AgeDepartamentos ageDepartamento = ValidarUpdate(_httpContextAccessor, AgeDepartamentosSaveDAO);

            return _repository.Actualizar(ageDepartamento);
        }

        public Task<List<AgeDepartamentosDAO>> ActualizarVarios(IHttpContextAccessor _httpContextAccessor, List<AgeDepartamentosSaveDAO> AgeDepartamentosSaveDAOList)
        {
            if (AgeDepartamentosSaveDAOList == null)
                throw new InvalidSintaxisException();

            ValidateKeys.ValidarPKDuplicadas(AgeDepartamentosSaveDAOList, p => new { p.Id.AgeLicencCodigo, p.Id.Codigo }, "Id");

            List<AgeDepartamentos> ageDepartamentosList = new();
            AgeDepartamentos ageDepartamento;

            foreach (AgeDepartamentosSaveDAO ageDepartamentosSaveDAO in AgeDepartamentosSaveDAOList)
            {
                ageDepartamento = ValidarUpdate(_httpContextAccessor, ageDepartamentosSaveDAO);
                ageDepartamentosList.Add(ageDepartamento);
            }

            return _repository.ActualizarVarios(ageDepartamentosList);
        }

        private Resource<AgeDepartamentosDAO> GetDataWithResource(IHttpContextAccessor _httpContextAccessor, AgeDepartamentosDAO ageDepartamentosDAO)
        {
            string rutaSegmentoFinal = $"{ageDepartamentosDAO.Id.AgeLicencCodigo}/{ageDepartamentosDAO.Id.Codigo}";

            return Resource<AgeDepartamentosDAO>.GetDataWithResource<AgeDepartamentosController>(
                _httpContextAccessor, rutaSegmentoFinal, ageDepartamentosDAO);
        }

        private AgeDepartamentos ValidarInsert(IHttpContextAccessor _httpContextAccessor, AgeDepartamentosSaveDAO ageDepartamentosSaveDAO)
        {
            if (ageDepartamentosSaveDAO == null) throw new InvalidSintaxisException();

            Validator.ValidateObject(ageDepartamentosSaveDAO, new ValidationContext(ageDepartamentosSaveDAO), true);

            FuncionesSecuencias fg = new(_ageLicenciatariosAplicaSecuService);

            ageDepartamentosSaveDAO.Id.Codigo = fg.ObtenerSecuencia(_httpContextAccessor,
                ageDepartamentosSaveDAO.Id.AgeLicencCodigo,
                Globales.CODIGO_SECUENCIA_DEPARTAMENTO,
                ageDepartamentosSaveDAO.UsuarioIngreso);

            ValidarKeys(ageDepartamentosSaveDAO);

            AgeDepartamentos entityObject = FromDAOToEntity(_httpContextAccessor,ageDepartamentosSaveDAO);

            Validator.ValidateObject(entityObject, new ValidationContext(entityObject), true);

            return entityObject;
        }

        private AgeDepartamentos ValidarUpdate(IHttpContextAccessor _httpContextAccessor, AgeDepartamentosSaveDAO ageDepartamentosSaveDAO)
        {
            if (ageDepartamentosSaveDAO == null)
                throw new InvalidSintaxisException();

            if (ageDepartamentosSaveDAO.UsuarioModificacion == null || ageDepartamentosSaveDAO.UsuarioModificacion <= 0)
                throw new InvalidFieldException("El usuario de modificación debe ser un número entero mayor a cero.");

            ValidarFKS(ageDepartamentosSaveDAO);

            AgeDepartamentos? ageDepartamento = ConsultarCompletePorId(ageDepartamentosSaveDAO.Id) 
                ?? throw new RegisterNotFoundException("Departamento con código: " + ageDepartamentosSaveDAO.Id.Codigo + " no existe.");

            ageDepartamento.Estado = !string.IsNullOrWhiteSpace(ageDepartamentosSaveDAO.Estado) ? ageDepartamentosSaveDAO.Estado : ageDepartamento.Estado;
            ageDepartamento.FechaEstado = !string.IsNullOrWhiteSpace(ageDepartamentosSaveDAO.Estado) ? DateTime.Now : ageDepartamento.FechaEstado;
            ageDepartamento.Descripcion = !string.IsNullOrWhiteSpace(ageDepartamentosSaveDAO.Descripcion) ? ageDepartamentosSaveDAO.Descripcion : ageDepartamento.Descripcion;
            ageDepartamento.ObservacionEstado = !string.IsNullOrWhiteSpace(ageDepartamentosSaveDAO.ObservacionEstado) ? ageDepartamentosSaveDAO.ObservacionEstado : ageDepartamento.ObservacionEstado;

            ageDepartamento.FechaModificacion = DateTime.Now;
            ageDepartamento.UbicacionModificacion = Ubicacion.getIpAdress(_httpContextAccessor);
            ageDepartamento.UsuarioModificacion = ageDepartamentosSaveDAO.UsuarioModificacion;

            ageDepartamento.AgeDepartAgeLicencCodigo = ageDepartamentosSaveDAO.AgeDepartAgeLicencCodigo != null && ageDepartamentosSaveDAO.AgeDepartAgeLicencCodigo > 0 ? 
                ageDepartamentosSaveDAO.AgeDepartAgeLicencCodigo : ageDepartamento.AgeDepartAgeLicencCodigo;
            ageDepartamento.AgeDepartCodigo = ageDepartamentosSaveDAO.AgeDepartCodigo != null && ageDepartamentosSaveDAO.AgeDepartCodigo > 0 ?
                ageDepartamentosSaveDAO.AgeDepartCodigo : ageDepartamento.AgeDepartCodigo;

            Validator.ValidateObject(ageDepartamento, new ValidationContext(ageDepartamento), true);

            return ageDepartamento;
        }

        private AgeDepartamentos FromDAOToEntity(
           IHttpContextAccessor _httpContextAccessor,
           AgeDepartamentosSaveDAO ageDepartamentosSaveDAO)
        {

            return new AgeDepartamentos()
            {
                Codigo = ageDepartamentosSaveDAO.Id.Codigo,
                AgeLicencCodigo = ageDepartamentosSaveDAO.Id.AgeLicencCodigo,
                Descripcion = ageDepartamentosSaveDAO.Descripcion,
                AgeDepartAgeLicencCodigo = ageDepartamentosSaveDAO.AgeDepartAgeLicencCodigo,
                AgeDepartCodigo = ageDepartamentosSaveDAO.AgeDepartCodigo,
                Estado = ageDepartamentosSaveDAO.Estado,
                FechaEstado = DateTime.Now,
                FechaIngreso = DateTime.Now,
                ObservacionEstado = ageDepartamentosSaveDAO.ObservacionEstado,
                UbicacionIngreso = Ubicacion.getIpAdress(_httpContextAccessor),
                UsuarioIngreso = ageDepartamentosSaveDAO.UsuarioIngreso
            };
        }

        private void ValidarKeys(AgeDepartamentosSaveDAO ageDepartamentosSaveDAO)
        {
            ValidarPK(ageDepartamentosSaveDAO.Id);
            ValidarFKS(ageDepartamentosSaveDAO);
        }

        private void ValidarPK(AgeDepartamentosPKDAO ageDepartamentosPKDAO)
        {
            ValidateKeys.ValidarNoExistenciaKey(ageDepartamentosPKDAO,
                    $"Departamento con código: {ageDepartamentosPKDAO.Codigo}" +
                    $" y código de licenciatario: {ageDepartamentosPKDAO.AgeLicencCodigo} ya existe."
                    , _repository.ConsultarCompletePorId);
        }

        private void ValidarFKS(AgeDepartamentosSaveDAO ageDepartamentosSaveDAO)
        {
            if ((ageDepartamentosSaveDAO.AgeDepartAgeLicencCodigo > 0 && ageDepartamentosSaveDAO.AgeDepartAgeLicencCodigo != null) ||
                (ageDepartamentosSaveDAO.AgeDepartCodigo > 0 && ageDepartamentosSaveDAO.AgeDepartCodigo != null))
            {
                AgeDepartamentosPKDAO ageDepartamentosPKDAO = new AgeDepartamentosPKDAO
                {
                    AgeLicencCodigo = ageDepartamentosSaveDAO.AgeDepartAgeLicencCodigo ?? 0,
                    Codigo = ageDepartamentosSaveDAO.AgeDepartCodigo ?? 0
                };
                ValidarFKDepartamento(ageDepartamentosPKDAO);
            }

            ValidarFKLicenciatario(ageDepartamentosSaveDAO.Id.AgeLicencCodigo);
        }

        private void ValidarFKDepartamento(AgeDepartamentosPKDAO ageDepartamentosPKDAO)
        {
            ValidateKeys.ValidarExistenciaKey(ageDepartamentosPKDAO,
                    $"Departamento con código: {ageDepartamentosPKDAO.Codigo}" +
                    $" y código de licenciatario: {ageDepartamentosPKDAO.AgeLicencCodigo} no existe."
                    , _repository.ConsultarCompletePorId);
        }

        private void ValidarFKLicenciatario(int codigoLicenciatario)
        {
            ValidateKeys.ValidarExistenciaKey(codigoLicenciatario,
                $"Licenciatario con código: {codigoLicenciatario} no existe."
                , _repositoryLicenciatarios.ConsultarCompletePorId);
        }
    }
}
