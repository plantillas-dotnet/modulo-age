﻿
using AGE.Entities;
using AGE.Entities.DAO.AgeSecuenciasPrimarias;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;

namespace AGE.Services.AgeSecuenciasPrimariasService
{
    public interface IAgeSecuenciasPrimariasService
    {

        Task<AgeSecuenciasPrimariaDAO> ConsultarPorId(
            int codigoSecuencia);

        Task<AgeSecuenciasPrimaria> ConsultarCompletePorId(
            int codigoSecuencia);

        Task<Page<AgeSecuenciasPrimariaDAO>> ConsultarTodos(
            Pageable pageable);

        Task<Resource<AgeSecuenciasPrimariaDAO>> Insertar(
            IHttpContextAccessor httpContextAccessor, 
            AgeSecuenciasPrimariasSaveDAO ageSecuenciasPrimariasSaveDAO);

        Task<AgeSecuenciasPrimariaDAO> Actualizar(
            IHttpContextAccessor httpContextAccessor,
            AgeSecuenciasPrimariasSaveDAO ageSecuenciasPrimariasSaveDAO);

        Task<List<Resource<AgeSecuenciasPrimariaDAO>>> InsertarVarios(
            IHttpContextAccessor httpContextAccessor, 
            List<AgeSecuenciasPrimariasSaveDAO> ageSecuenciasPrimariasSaveDAOList);

        Task<List<AgeSecuenciasPrimariaDAO>> ActualizarVarios(
            IHttpContextAccessor httpContextAccessor, 
            List<AgeSecuenciasPrimariasSaveDAO> ageSecuenciasPrimariasSaveDAOList);

        void ActualizarSecuenciaPrimaria(
            IHttpContextAccessor _httpContextAccessor,
            AgeSecuenciasPrimaria ageSecuenciasPrimaria);
    }
}
