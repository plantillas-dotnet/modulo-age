﻿using AGE.Entities;
using AGE.Entities.DAO.AgeSecuenciasPrimarias;
using AGE.Repositories.AgeSecuenciasPrimariasRepository;
using AGE.Utils;
using AGE.Middleware.Exceptions.BadRequest;
using System.ComponentModel.DataAnnotations;
using AGE.Controllers;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using System.Transactions;

namespace AGE.Services.AgeSecuenciasPrimariasService
{
    public class AgeSecuenciasPrimariasService : IAgeSecuenciasPrimariasService
    {
        private readonly IAgeSecuenciasPrimariasRespository _repository;

        public AgeSecuenciasPrimariasService(IAgeSecuenciasPrimariasRespository Repository)
        {
            _repository = Repository;
        }

        public Task<Page<AgeSecuenciasPrimariaDAO>> ConsultarTodos(
            Pageable pageable)
        {
            pageable.Validar<AgeSecuenciasPrimariaDAO>();

            return _repository.ConsultarTodos(pageable);
        }

        public Task<AgeSecuenciasPrimariaDAO> ConsultarPorId(int codigoSecuencia)
        {
            if (codigoSecuencia <= 0)
                throw new InvalidIdException();

            AgeSecuenciasPrimariaDAO ageSecuenciasPrimariaDAO = _repository.ConsultarPorId(codigoSecuencia).Result;

            return ageSecuenciasPrimariaDAO == null
                ? throw new RegisterNotFoundException("Secuencia primaria con código: " + codigoSecuencia + " no existe.")
                : Task.FromResult(ageSecuenciasPrimariaDAO);
        }

        public Task<AgeSecuenciasPrimaria> ConsultarCompletePorId(int codigoSecuencia)
        {
            if (codigoSecuencia <= 0)
                throw new InvalidIdException();

            AgeSecuenciasPrimaria ageSecuenciasPrimaria = _repository.ConsultarCompletePorId(codigoSecuencia).Result;

            return Task.FromResult(ageSecuenciasPrimaria);
        }

        public async Task<Resource<AgeSecuenciasPrimariaDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgeSecuenciasPrimariasSaveDAO ageSecuenciasPrimariasSaveDAO)
        {

            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    AgeSecuenciasPrimaria ageSecuenciasPrimarias = ValidarInsert(_httpContextAccessor, ageSecuenciasPrimariasSaveDAO);

                    AgeSecuenciasPrimariaDAO ageSecuenciasPrimariasDAO = await _repository.Insertar(ageSecuenciasPrimarias);

                    Resource<AgeSecuenciasPrimariaDAO> ageSecuenciasPrimariasDAOWithResource = GetDataWithResource(_httpContextAccessor, ageSecuenciasPrimariasDAO);

                    transactionScope.Complete();

                    return ageSecuenciasPrimariasDAOWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }


        public async Task<List<Resource<AgeSecuenciasPrimariaDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeSecuenciasPrimariasSaveDAO> ageSecuenciasPrimariasSaveDAOList)
        {
            if (ageSecuenciasPrimariasSaveDAOList == null)
                throw new InvalidSintaxisException();

            List<AgeSecuenciasPrimaria> listAgeSecuenciasPrimaria = new List<AgeSecuenciasPrimaria>();

            ValidateKeys.ValidarPKDuplicadas(ageSecuenciasPrimariasSaveDAOList, p => new { p.Codigo }, "Codigo");

            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    foreach (AgeSecuenciasPrimariasSaveDAO ageSecuenciasPrimariasSaveDAO in ageSecuenciasPrimariasSaveDAOList) {
                        AgeSecuenciasPrimaria entityObject = ValidarInsert(_httpContextAccessor, ageSecuenciasPrimariasSaveDAO);
                        listAgeSecuenciasPrimaria.Add(entityObject);
                    }

                    List<AgeSecuenciasPrimariaDAO> ageSecuenciasPrimariasDAOList = _repository.InsertarVarios(listAgeSecuenciasPrimaria).Result;

                    List<Resource<AgeSecuenciasPrimariaDAO>> ageSecuenciasPrimariasDAOListWithResource = new List<Resource<AgeSecuenciasPrimariaDAO>>();

                    foreach (AgeSecuenciasPrimariaDAO ageSecuenciasPrimariasDAO in ageSecuenciasPrimariasDAOList)
                    {
                        ageSecuenciasPrimariasDAOListWithResource.Add(GetDataWithResource(_httpContextAccessor, ageSecuenciasPrimariasDAO));
                    }

                    transactionScope.Complete();

                    return ageSecuenciasPrimariasDAOListWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public Task<AgeSecuenciasPrimariaDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeSecuenciasPrimariasSaveDAO ageSecuenciasPrimariasSaveDAO)
        {
            AgeSecuenciasPrimaria ObjectDao = ValidarUpdate(_httpContextAccessor, ageSecuenciasPrimariasSaveDAO);
            return _repository.Actualizar(ObjectDao);
        }

        public Task<List<AgeSecuenciasPrimariaDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeSecuenciasPrimariasSaveDAO> ageSecuenciasPrimariasSaveDAOList)
        {
            ValidateKeys.ValidarPKDuplicadas(ageSecuenciasPrimariasSaveDAOList, p => new { p.Codigo }, "Codigo");

            List<AgeSecuenciasPrimaria> ageSecuenciasPrimariaDaoList = new List<AgeSecuenciasPrimaria>();

            foreach (AgeSecuenciasPrimariasSaveDAO ageSaveDAO in ageSecuenciasPrimariasSaveDAOList)
            {
                AgeSecuenciasPrimaria ObjectDao = ValidarUpdate(_httpContextAccessor, ageSaveDAO);
                ageSecuenciasPrimariaDaoList.Add(ObjectDao);
            }

            return _repository.ActualizarVarios(ageSecuenciasPrimariaDaoList);
        }

        public void ActualizarSecuenciaPrimaria(
            IHttpContextAccessor _httpContextAccessor,
            AgeSecuenciasPrimaria ageSecuenciasPrimaria)
        {
            ageSecuenciasPrimaria.FechaModificacion = DateTime.Now;
            ageSecuenciasPrimaria.UbicacionModificacion = Ubicacion.getIpAdress(_httpContextAccessor);

            Validator.ValidateObject(ageSecuenciasPrimaria, new ValidationContext(ageSecuenciasPrimaria), true);

            _repository.ActualizarSecuenciaPrimaria(ageSecuenciasPrimaria);
        }

        private static Resource<AgeSecuenciasPrimariaDAO> GetDataWithResource(IHttpContextAccessor _httpContextAccessor, AgeSecuenciasPrimariaDAO ageSecuenciasPrimariaDao)
        {
            string rutaSegmentoFinal = $"{ageSecuenciasPrimariaDao.Codigo}";

            return Resource<AgeSecuenciasPrimariaDAO>.GetDataWithResource<AgeSecuenciasPrimariasController>(
                _httpContextAccessor, rutaSegmentoFinal, ageSecuenciasPrimariaDao);
        }

        private AgeSecuenciasPrimaria ValidarInsert(
            IHttpContextAccessor _httpContextAccessor,
            AgeSecuenciasPrimariasSaveDAO ageSecuenciasPrimariasSaveDAO)
        {
            if (ageSecuenciasPrimariasSaveDAO == null)
                throw new InvalidSintaxisException();

            ValidarPK(ageSecuenciasPrimariasSaveDAO.Codigo);

            AgeSecuenciasPrimaria entityObject = FromDAOToEntity(_httpContextAccessor,ageSecuenciasPrimariasSaveDAO);

            Validator.ValidateObject(entityObject, new ValidationContext(entityObject), true);

            return entityObject;
        }

        private AgeSecuenciasPrimaria ValidarUpdate(IHttpContextAccessor _httpContextAccessor, AgeSecuenciasPrimariasSaveDAO ageSecuenciasPrimariasSaveDAO)
        {
            if (ageSecuenciasPrimariasSaveDAO == null)
                throw new InvalidSintaxisException();

            AgeSecuenciasPrimaria? ageSecuenciasPrimaria = ConsultarCompletePorId(ageSecuenciasPrimariasSaveDAO.Codigo).Result ??
                throw new RegisterNotFoundException($"Secuencia primaria con código: {ageSecuenciasPrimariasSaveDAO.Codigo} no existe.");

            if (ageSecuenciasPrimariasSaveDAO.ValorActual > 0)
                ageSecuenciasPrimaria.ValorActual = ageSecuenciasPrimariasSaveDAO.ValorActual;

            if (ageSecuenciasPrimariasSaveDAO.ValorInicial > 0)
                ageSecuenciasPrimaria.ValorInicial = ageSecuenciasPrimariasSaveDAO.ValorInicial;

            if (!string.IsNullOrWhiteSpace(ageSecuenciasPrimariasSaveDAO.Estado))
            {
                ageSecuenciasPrimaria.Estado = ageSecuenciasPrimariasSaveDAO.Estado.ToUpper().ToString();
                ageSecuenciasPrimaria.FechaEstado = DateTime.Now;
            }

            if (!string.IsNullOrWhiteSpace(ageSecuenciasPrimariasSaveDAO.Ciclica))
                ageSecuenciasPrimaria.Ciclica = ageSecuenciasPrimariasSaveDAO.Ciclica.ToUpper().ToString();

            if (!string.IsNullOrWhiteSpace(ageSecuenciasPrimariasSaveDAO.Descripcion))
                ageSecuenciasPrimaria.Descripcion = ageSecuenciasPrimariasSaveDAO.Descripcion;

            if (!string.IsNullOrWhiteSpace(ageSecuenciasPrimariasSaveDAO.ObservacionEstado))
                ageSecuenciasPrimaria.ObservacionEstado = ageSecuenciasPrimariasSaveDAO.ObservacionEstado;

            ageSecuenciasPrimaria.FechaModificacion = DateTime.Now;
            ageSecuenciasPrimaria.UbicacionModificacion = Ubicacion.getIpAdress(_httpContextAccessor);

            ageSecuenciasPrimaria.UsuarioModificacion = ageSecuenciasPrimariasSaveDAO.UsuarioModificacion;

            Validator.ValidateObject(ageSecuenciasPrimaria, new ValidationContext(ageSecuenciasPrimaria), true);

            return ageSecuenciasPrimaria;
        }

        private AgeSecuenciasPrimaria FromDAOToEntity(
            IHttpContextAccessor _httpContextAccessor,
            AgeSecuenciasPrimariasSaveDAO ageSecuenciasPrimariasSaveDAO)
        {

            return new AgeSecuenciasPrimaria
            {
                Codigo = ageSecuenciasPrimariasSaveDAO.Codigo,
                Descripcion = ageSecuenciasPrimariasSaveDAO.Descripcion,
                ValorInicial = ageSecuenciasPrimariasSaveDAO.ValorInicial,
                IncrementaEn = ageSecuenciasPrimariasSaveDAO.IncrementaEn,
                ValorActual = ageSecuenciasPrimariasSaveDAO.ValorActual,
                Ciclica = ageSecuenciasPrimariasSaveDAO.Ciclica.ToUpper().ToString(),
                Estado = ageSecuenciasPrimariasSaveDAO.Estado.ToUpper().ToString(),
                ObservacionEstado = ageSecuenciasPrimariasSaveDAO.ObservacionEstado,
                UsuarioIngreso = ageSecuenciasPrimariasSaveDAO.UsuarioIngreso,
                FechaEstado = DateTime.Now,
                FechaIngreso = DateTime.Now,
                UbicacionIngreso = Ubicacion.getIpAdress(_httpContextAccessor),
                UsuarioModificacion = null,
                FechaModificacion = null,
                UbicacionModificacion = null
            };
        }

        private void ValidarPK(int codigoSecuenciaPrimaria)
            {
                ValidateKeys.ValidarNoExistenciaKey(
                   codigoSecuenciaPrimaria,
                   $"Secuencia primaria con código: {codigoSecuenciaPrimaria} ya existe.",
                   _repository.ConsultarCompletePorId);
            }

        }
}


