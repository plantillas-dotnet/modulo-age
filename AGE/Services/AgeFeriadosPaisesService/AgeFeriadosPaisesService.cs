﻿using AGE.Controllers;
using AGE.Entities;
using AGE.Entities.DAO.AgeFeriadosPaise;
using AGE.Middleware.Exceptions.BadRequest;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Repositories.AgeDiasFeriadosRepository;
using AGE.Repositories.AgeFeriadosPaiseRepository;
using AGE.Repositories.AgePaisesRepository;
using AGE.Utils;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using System.ComponentModel.DataAnnotations;
using System.Transactions;

namespace AGE.Services.AgeFeriadosPaiseService
{
    public class AgeFeriadosPaisesService : IAgeFeriadosPaisesService
    {
        private readonly IAgeFeriadosPaisesRepository _repository;
        private readonly IAgeDiasFeriadosRepository _repositoryDiasFeriados;
        private readonly IAgePaisesRepository _repositoryPaises;

        public AgeFeriadosPaisesService(
            IAgeFeriadosPaisesRepository Repository,
            IAgeDiasFeriadosRepository repositoryDiasFeriados,
            IAgePaisesRepository repositoryPaises)
        {
            _repository = Repository;
            _repositoryDiasFeriados = repositoryDiasFeriados;
            _repositoryPaises = repositoryPaises;
        }

        public Task<Page<AgeFeriadosPaisesDAO>> ConsultarTodos(
            Pageable pageable)
        {
            pageable.Validar<AgeFeriadosPaisesDAO>();

            return _repository.ConsultarTodos(pageable);
        }

        public Task<AgeFeriadosPaisesDAO> ConsultarPorId(
            AgeFeriadosPaisesPKDAO ageFeriadosPaisePKDAO)
        {
            Validator.ValidateObject(ageFeriadosPaisePKDAO, new ValidationContext(ageFeriadosPaisePKDAO), true);

            AgeFeriadosPaisesDAO? ageFeriadosPaiseDAO = _repository.ConsultarPorId(ageFeriadosPaisePKDAO).Result;

            return ageFeriadosPaiseDAO == null
                ? throw new RegisterNotFoundException("Feriado país con código: " + ageFeriadosPaisePKDAO.AgeDiaFeCodigo + " no existe.")
                : Task.FromResult(ageFeriadosPaiseDAO);
        }

        private AgeFeriadosPaises ConsultarCompletePorId(
            AgeFeriadosPaisesPKDAO ageFeriadosPaisePKDAO)
        {
            Validator.ValidateObject(ageFeriadosPaisePKDAO, new ValidationContext(ageFeriadosPaisePKDAO), true);

            AgeFeriadosPaises? ageFeriadosPaise = _repository.ConsultarCompletePorId(ageFeriadosPaisePKDAO).Result;

            return ageFeriadosPaise;
        }

        public Task<Page<AgeFeriadosPaisesDAO>> ConsultarListaFiltro(
            string filtro, 
            Pageable pageable)
        {
            if (string.IsNullOrWhiteSpace(filtro))
                throw new InvalidFieldException("El filtro es requerido.");

            pageable.Validar<AgeFeriadosPaisesDAO>();

            return _repository.ConsultarListaFiltro(filtro.ToLower(), pageable);
        }

        public async Task<Resource<AgeFeriadosPaisesDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor, 
            AgeFeriadosPaisesSaveDAO ageFeriadosPaiseSaveDAO)
        {
            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    AgeFeriadosPaises ageFeriadosPaise = ValidarInsert(_httpContextAccessor, ageFeriadosPaiseSaveDAO);

                    AgeFeriadosPaisesDAO ageFeriadosPaiseDAO = await _repository.Insertar(ageFeriadosPaise);

                    transactionScope.Complete();

                    Resource<AgeFeriadosPaisesDAO> ageFeriadosPaiseDAOWithResource = GetDataWithResource(_httpContextAccessor, ageFeriadosPaiseDAO);

                    return ageFeriadosPaiseDAOWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public async Task<List<Resource<AgeFeriadosPaisesDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor, 
            List<AgeFeriadosPaisesSaveDAO> ageFeriadosPaiseSaveDAOList)
        {
            if (ageFeriadosPaiseSaveDAOList == null)
                throw new InvalidSintaxisException();

            ValidateKeys.ValidarPKDuplicadas(ageFeriadosPaiseSaveDAOList, p => new { p.Id.AgePaisCodigo, p.Id.AgeDiaFeCodigo }, "Id");

            List<AgeFeriadosPaises> ageFeriadosPaiseList = new List<AgeFeriadosPaises>();

            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    foreach (AgeFeriadosPaisesSaveDAO ageFeriadosPaiseSaveDAO in ageFeriadosPaiseSaveDAOList)
                    {
                        AgeFeriadosPaises ageMensaje = ValidarInsert(_httpContextAccessor, ageFeriadosPaiseSaveDAO);
                        ageFeriadosPaiseList.Add(ageMensaje);
                    }

                    List<AgeFeriadosPaisesDAO> ageFeriadosPaiseDAOList = await _repository.InsertarVarios(ageFeriadosPaiseList);

                    List<Resource<AgeFeriadosPaisesDAO>> ageFeriadosPaiseDAOListWithResource = new List<Resource<AgeFeriadosPaisesDAO>>();

                    foreach (AgeFeriadosPaisesDAO ageFeriadosPaiseDAO in ageFeriadosPaiseDAOList)
                    {
                        ageFeriadosPaiseDAOListWithResource.Add(GetDataWithResource(_httpContextAccessor, ageFeriadosPaiseDAO));
                    }

                    transactionScope.Complete();

                    return ageFeriadosPaiseDAOListWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public Task<AgeFeriadosPaisesDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor, 
            AgeFeriadosPaisesSaveDAO ageFeriadosPaiseSaveDAO)
        {
            AgeFeriadosPaises ageFeriadosPaise = ValidarUpdate(_httpContextAccessor, ageFeriadosPaiseSaveDAO);

            return _repository.Actualizar(ageFeriadosPaise);
        }

        public Task<List<AgeFeriadosPaisesDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor, 
            List<AgeFeriadosPaisesSaveDAO> ageFeriadosPaiseSaveDAOList)
        {
            if (ageFeriadosPaiseSaveDAOList == null)
                throw new InvalidSintaxisException();

            ValidateKeys.ValidarPKDuplicadas(ageFeriadosPaiseSaveDAOList,p => new { p.Id.AgePaisCodigo, p.Id.AgeDiaFeCodigo}, "Id");

            List<AgeFeriadosPaises> ageFeriadosPaiseList = new List<AgeFeriadosPaises>();
            AgeFeriadosPaises ageFeriadosPaise;

            foreach (AgeFeriadosPaisesSaveDAO ageFeriadosPaiseSaveDAO in ageFeriadosPaiseSaveDAOList)
            {
                ageFeriadosPaise = ValidarUpdate(_httpContextAccessor, ageFeriadosPaiseSaveDAO);
                ageFeriadosPaiseList.Add(ageFeriadosPaise);
            }

            return _repository.ActualizarVarios(ageFeriadosPaiseList);
        }

        private static Resource<AgeFeriadosPaisesDAO> GetDataWithResource(
            IHttpContextAccessor _httpContextAccessor,
            AgeFeriadosPaisesDAO ageFeriadosPaiseDAO)
        {
            string rutaSegmentoFinal = $"{ageFeriadosPaiseDAO.Id.AgePaisCodigo}/{ageFeriadosPaiseDAO.Id.AgeDiaFeCodigo}";

            return Resource<AgeFeriadosPaisesDAO>.GetDataWithResource<AgeFeriadosPaisesController>(
                _httpContextAccessor, rutaSegmentoFinal, ageFeriadosPaiseDAO);
        }

        private AgeFeriadosPaises ValidarInsert(
            IHttpContextAccessor _httpContextAccessor,
            AgeFeriadosPaisesSaveDAO ageFeriadosPaiseSaveDAO)
        {
            if (ageFeriadosPaiseSaveDAO == null)
                throw new InvalidSintaxisException();

            Validator.ValidateObject(ageFeriadosPaiseSaveDAO, new ValidationContext(ageFeriadosPaiseSaveDAO), true);

            if (ageFeriadosPaiseSaveDAO.FechaEjecucionDesde == default)
                throw new InvalidFieldException("La fecha ejecución desde no puede ser nula.");

            if (ageFeriadosPaiseSaveDAO.FechaEjecucionHasta == default)
                throw new InvalidFieldException("La fecha ejecución hasta no puede ser nula.");

            ValidarKeys(ageFeriadosPaiseSaveDAO.Id);

            AgeFeriadosPaises entityObject = FromDAOToEntity(_httpContextAccessor,ageFeriadosPaiseSaveDAO);

            Validator.ValidateObject(entityObject, new ValidationContext(entityObject), true);

            return entityObject;
        }

        private AgeFeriadosPaises ValidarUpdate(
            IHttpContextAccessor _httpContextAccessor,
            AgeFeriadosPaisesSaveDAO ageFeriadosPaiseSaveDAO)
        {
            if (ageFeriadosPaiseSaveDAO == null)
                throw new InvalidSintaxisException();

            if (ageFeriadosPaiseSaveDAO.UsuarioModificacion == null || ageFeriadosPaiseSaveDAO.UsuarioModificacion <= 0)
                throw new InvalidFieldException("El usuario de modificación debe ser un número entero mayor a cero.");

            AgeFeriadosPaises ageFeriadosPaise = ConsultarCompletePorId(ageFeriadosPaiseSaveDAO.Id) 
                ?? throw new RegisterNotFoundException("Feriado país con código de día feriado: " + ageFeriadosPaiseSaveDAO.Id.AgeDiaFeCodigo + 
                                                       " y código de país: " + ageFeriadosPaiseSaveDAO.Id.AgePaisCodigo + " no existe.");

            if (!string.IsNullOrWhiteSpace(ageFeriadosPaiseSaveDAO.Estado))
            {
                ageFeriadosPaise.Estado = ageFeriadosPaiseSaveDAO.Estado;
                ageFeriadosPaise.FechaEstado = DateTime.Now;
            }

            if (ageFeriadosPaiseSaveDAO.FechaEjecucionDesde != default)
                ageFeriadosPaise.FechaEjecucionDesde = ageFeriadosPaiseSaveDAO.FechaEjecucionDesde;

            if (ageFeriadosPaiseSaveDAO.FechaEjecucionHasta != default)
                ageFeriadosPaise.FechaEjecucionHasta = ageFeriadosPaiseSaveDAO.FechaEjecucionHasta;

            if (ageFeriadosPaiseSaveDAO.HoraEjecucionDesde != default && ageFeriadosPaiseSaveDAO.HoraEjecucionDesde != null)
                ageFeriadosPaise.HoraEjecucionDesde = ageFeriadosPaiseSaveDAO.HoraEjecucionDesde;

            if (ageFeriadosPaiseSaveDAO.HoraEjecucionHasta != default && ageFeriadosPaiseSaveDAO.HoraEjecucionHasta != null)
                ageFeriadosPaise.HoraEjecucionHasta = ageFeriadosPaiseSaveDAO.HoraEjecucionHasta;

            if (!string.IsNullOrWhiteSpace(ageFeriadosPaiseSaveDAO.ObservacionEstado))
                ageFeriadosPaise.ObservacionEstado = ageFeriadosPaiseSaveDAO.ObservacionEstado;

            ageFeriadosPaise.FechaModificacion = DateTime.Now;
            ageFeriadosPaise.UbicacionModificacion = Ubicacion.getIpAdress(_httpContextAccessor);
            ageFeriadosPaise.UsuarioModificacion = ageFeriadosPaiseSaveDAO.UsuarioModificacion;

            Validator.ValidateObject(ageFeriadosPaise, new ValidationContext(ageFeriadosPaise), true);

            return ageFeriadosPaise;
        }

        private AgeFeriadosPaises FromDAOToEntity(
            IHttpContextAccessor _httpContextAccessor,
            AgeFeriadosPaisesSaveDAO ageFeriadosPaiseSaveDAO)
        {

            return new AgeFeriadosPaises
            {
                AgePaisCodigo = ageFeriadosPaiseSaveDAO.Id.AgePaisCodigo,
                AgeDiaFeCodigo = ageFeriadosPaiseSaveDAO.Id.AgeDiaFeCodigo,
                FechaEjecucionDesde = ageFeriadosPaiseSaveDAO.FechaEjecucionDesde,
                FechaEjecucionHasta = ageFeriadosPaiseSaveDAO.FechaEjecucionHasta,
                HoraEjecucionDesde = ageFeriadosPaiseSaveDAO.HoraEjecucionDesde == default
                                        ? null : ageFeriadosPaiseSaveDAO.HoraEjecucionDesde,
                HoraEjecucionHasta = ageFeriadosPaiseSaveDAO.HoraEjecucionHasta == default
                                        ? null : ageFeriadosPaiseSaveDAO.HoraEjecucionHasta,
                Estado = ageFeriadosPaiseSaveDAO.Estado,
                FechaEstado = DateTime.Now,
                FechaIngreso = DateTime.Now,
                ObservacionEstado = ageFeriadosPaiseSaveDAO.ObservacionEstado,
                UbicacionIngreso = Ubicacion.getIpAdress(_httpContextAccessor),
                UsuarioIngreso = ageFeriadosPaiseSaveDAO.UsuarioIngreso
            };
        }

        private void ValidarKeys(AgeFeriadosPaisesPKDAO ageFeriadosPaisePKDAO)
        {
            ValidarPK(ageFeriadosPaisePKDAO);

            ValidarFKDiaFeriado(ageFeriadosPaisePKDAO.AgeDiaFeCodigo);

            ValidarFKPais(ageFeriadosPaisePKDAO.AgePaisCodigo);
        }

        private void ValidarPK(AgeFeriadosPaisesPKDAO ageFeriadosPaisePKDAO)
        {
            ValidateKeys.ValidarNoExistenciaKey(
               ageFeriadosPaisePKDAO,
               $"Feriado país con código de día feriado: {ageFeriadosPaisePKDAO.AgeDiaFeCodigo} " +
               $"y código país: {ageFeriadosPaisePKDAO.AgePaisCodigo} ya existe.",
               _repository.ConsultarCompletePorId);
        }

        private void ValidarFKDiaFeriado(int codigoDiaFeriado)
        {
            ValidateKeys.ValidarExistenciaKey(
               codigoDiaFeriado,
               $"Días Feriado con código: {codigoDiaFeriado} no existe.",
               _repositoryDiasFeriados.ConsultarCompletePorId);
        }

        private void ValidarFKPais(int codigoPais)
        {
            ValidateKeys.ValidarExistenciaKey(
               codigoPais,
               $"País con código: {codigoPais} no existe.",
               _repositoryPaises.ConsultarCompletePorId);
        }
    }
}
