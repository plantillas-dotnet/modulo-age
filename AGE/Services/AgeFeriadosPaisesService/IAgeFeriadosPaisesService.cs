﻿using AGE.Entities.DAO.AgeFeriadosPaise;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;

namespace AGE.Services.AgeFeriadosPaiseService
{
    public interface IAgeFeriadosPaisesService
    {

        Task<Page<AgeFeriadosPaisesDAO>> ConsultarTodos(
            Pageable pageable);

        Task<Page<AgeFeriadosPaisesDAO>> ConsultarListaFiltro(
            string filtro,
            Pageable pageable);

        Task<AgeFeriadosPaisesDAO> ConsultarPorId(
            AgeFeriadosPaisesPKDAO ageFeriadosPaisePKDAO);

        Task<Resource<AgeFeriadosPaisesDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgeFeriadosPaisesSaveDAO ageFeriadosPaiseSaveDAO);

        Task<List<Resource<AgeFeriadosPaisesDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeFeriadosPaisesSaveDAO> ageFeriadosPaiseSaveDAOList);

        Task<AgeFeriadosPaisesDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeFeriadosPaisesSaveDAO ageFeriadosPaiseSaveDAO);

        Task<List<AgeFeriadosPaisesDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeFeriadosPaisesSaveDAO> ageFeriadosPaiseSaveDAOList);


    }
}
