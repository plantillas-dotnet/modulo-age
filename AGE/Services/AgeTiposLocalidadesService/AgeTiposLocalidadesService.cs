using AGE.Controllers;
using AGE.Entities;
using AGE.Middleware.Exceptions.BadRequest;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Repositories.AgeTiposLocalidadeRepository;
using AGE.Utils;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using System.ComponentModel.DataAnnotations;
using AGE.Services.AgeSecuenciasPrimariasService;
using AGE.Entities.DAO.AgeTiposLocalidades;
using System.Transactions;
using AGE.Repositories.AgePaisesRepository;

namespace AGE.Services.AgeTiposLocalidadeService
{
    public class AgeTiposLocalidadesService : IAgeTiposLocalidadesService
    {
        private readonly IAgeTiposLocalidadesRepository _repository;
        private readonly IAgePaisesRepository _repositoryPaises;
        private readonly IAgeSecuenciasPrimariasService _ageSecuenciasPrimariasService;

        public AgeTiposLocalidadesService(
            IAgeTiposLocalidadesRepository Repository,
            IAgeSecuenciasPrimariasService ageSecuenciasPrimariasService,
            IAgePaisesRepository repositoryPaises)
        {
            _repository = Repository;
            _ageSecuenciasPrimariasService = ageSecuenciasPrimariasService;
            _repositoryPaises = repositoryPaises;
        }


        public Task<Page<AgeTiposLocalidadeDAO>> ConsultarTodos(
            int codigoPais,
            int codigo,
            int codigoAgeTipLoAgePais,
            int codigoAgeTipLo,
            string descripcion,
            Pageable pageable)
        {

            pageable.Validar<AgeTiposLocalidadeDAO>();

            return _repository.ConsultarTodos(codigoPais, codigo, codigoAgeTipLoAgePais, codigoAgeTipLo, descripcion, pageable);
        }


        public Task<Page<AgeTiposLocalidadeDAO>> ConsultarListaFiltro(
            string filtro,
            Pageable pageable)
        {

            if (string.IsNullOrWhiteSpace(filtro))
            {
                throw new InvalidFieldException("El filtro es requerido.");
            }

            pageable.Validar<AgeTiposLocalidadeDAO>();

            return _repository.ConsultarListaFiltro(filtro.ToLower(), pageable);
        }

        public Task<AgeTiposLocalidadeDAO> ConsultarPorId(
            AgeTiposLocalidadePKDAO ageTiposLocalidadePKDAO)
        {
            Validator.ValidateObject(ageTiposLocalidadePKDAO, new ValidationContext(ageTiposLocalidadePKDAO), true);

            AgeTiposLocalidadeDAO? ageTiposLocalidadeDAO = _repository.ConsultarPorId(ageTiposLocalidadePKDAO).Result;

            if (ageTiposLocalidadeDAO == null)
            {
                throw new RegisterNotFoundException("Tipo de Localidad con código " + ageTiposLocalidadePKDAO.codigo + " no existe");
            }

            return Task.FromResult(ageTiposLocalidadeDAO);
        }

        private AgeTiposLocalidades ConsultarCompletePorId(
            AgeTiposLocalidadePKDAO ageTiposLocalidadePKDAO)
        {
            Validator.ValidateObject(ageTiposLocalidadePKDAO, new ValidationContext(ageTiposLocalidadePKDAO), true);

            AgeTiposLocalidades? ageTiposLocalidade = _repository.ConsultarCompletePorId(ageTiposLocalidadePKDAO).Result;

            return ageTiposLocalidade;
        }

        public async Task<Resource<AgeTiposLocalidadeDAO>> Insertar(
    IHttpContextAccessor _httpContextAccessor,
    AgeTiposLocalidadeSaveDAO ageTiposLocalidadeSaveDAO)
        {
            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    AgeTiposLocalidades ageTiposLocalidade = ValidarInsert(_httpContextAccessor, ageTiposLocalidadeSaveDAO);

                    AgeTiposLocalidadeDAO ageTiposLocalidadeDAO = await _repository.Insertar(ageTiposLocalidade);

                    Resource<AgeTiposLocalidadeDAO> ageTiposLocalidadeDAOWithResource = GetDataWithResource(_httpContextAccessor, ageTiposLocalidadeDAO);

                    transactionScope.Complete();

                    return ageTiposLocalidadeDAOWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public async Task<List<Resource<AgeTiposLocalidadeDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeTiposLocalidadeSaveDAO> ageTiposLocalidadeSaveDAOList)
        {
            if (ageTiposLocalidadeSaveDAOList == null)
            {
                throw new InvalidSintaxisException();
            }

            List<AgeTiposLocalidades> ageTiposLocalidadeList = new List<AgeTiposLocalidades>();

            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    foreach (AgeTiposLocalidadeSaveDAO ageTiposLocalidadeSaveDAO in ageTiposLocalidadeSaveDAOList)
                    {
                        AgeTiposLocalidades ageTiposLocalidade = ValidarInsert(_httpContextAccessor, ageTiposLocalidadeSaveDAO);
                        ageTiposLocalidadeList.Add(ageTiposLocalidade);
                    }

                    List<AgeTiposLocalidadeDAO> ageTiposLocalidadeDAOList = await _repository.InsertarVarios(ageTiposLocalidadeList);

                    List<Resource<AgeTiposLocalidadeDAO>> ageTiposLocalidadeDAOListWithResource = new List<Resource<AgeTiposLocalidadeDAO>>();

                    foreach (AgeTiposLocalidadeDAO ageTiposLocalidadeDAO in ageTiposLocalidadeDAOList)
                    {
                        ageTiposLocalidadeDAOListWithResource.Add(GetDataWithResource(_httpContextAccessor, ageTiposLocalidadeDAO));
                    }

                    transactionScope.Complete();

                    return ageTiposLocalidadeDAOListWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public Task<AgeTiposLocalidadeDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeTiposLocalidadeSaveDAO ageTiposLocalidadeSaveDAO)
        {
            AgeTiposLocalidades ageTiposLocalidade = ValidarUpdate(_httpContextAccessor, ageTiposLocalidadeSaveDAO);

            return _repository.Actualizar(ageTiposLocalidade);
        }

        public Task<List<AgeTiposLocalidadeDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeTiposLocalidadeSaveDAO> ageTiposLocalidadeSaveDAOList)
        {
            if (ageTiposLocalidadeSaveDAOList == null)
                throw new InvalidSintaxisException();

            ValidateKeys.ValidarPKDuplicadas(ageTiposLocalidadeSaveDAOList,
                                             p => new { p.Id.agePaisCodigo, p.Id.codigo },
                                             "Id");

            List<AgeTiposLocalidades> ageTiposLocalidadeList = new List<AgeTiposLocalidades>();
            AgeTiposLocalidades ageTiposLocalidade;

            foreach (AgeTiposLocalidadeSaveDAO ageTiposLocalidadeSaveDAO in ageTiposLocalidadeSaveDAOList)
            {
                ageTiposLocalidade = ValidarUpdate(_httpContextAccessor, ageTiposLocalidadeSaveDAO);
                ageTiposLocalidadeList.Add(ageTiposLocalidade);
            }

            return _repository.ActualizarVarios(ageTiposLocalidadeList);
        }


        private static Resource<AgeTiposLocalidadeDAO> GetDataWithResource(
            IHttpContextAccessor _httpContextAccessor,
            AgeTiposLocalidadeDAO ageTiposLocalidadeDAO)
        {
            string idtipoSegmentoFinal = $"{ageTiposLocalidadeDAO.Id.agePaisCodigo}/{ageTiposLocalidadeDAO.Id.codigo}";

            return Resource<AgeTiposLocalidadeDAO>.GetDataWithResource<AgeTiposLocalidadesController>(
                _httpContextAccessor, idtipoSegmentoFinal, ageTiposLocalidadeDAO);
        }

        private AgeTiposLocalidades ValidarInsert(
            IHttpContextAccessor _httpContextAccessor,
            AgeTiposLocalidadeSaveDAO ageTiposLocalidadeSaveDAO)
        {

            if (ageTiposLocalidadeSaveDAO == null)
                throw new InvalidSintaxisException();
            

            Validator.ValidateObject(ageTiposLocalidadeSaveDAO, new ValidationContext(ageTiposLocalidadeSaveDAO), true);

            FuncionesSecuencias fg = new FuncionesSecuencias(_ageSecuenciasPrimariasService);

            ageTiposLocalidadeSaveDAO.Id.codigo = fg.ObtenerSecuenciaPrimaria(_httpContextAccessor, 
                                                        Globales.CODIGO_SECUENCIA_TIPO_LOCALIDAD, 
                                                        ageTiposLocalidadeSaveDAO.UsuarioIngreso);

            ValidarKeys(ageTiposLocalidadeSaveDAO);

            AgeTiposLocalidades entityObject = FromDAOToEntity(_httpContextAccessor, ageTiposLocalidadeSaveDAO); 

            Validator.ValidateObject(entityObject, new ValidationContext(entityObject), true);

            return entityObject;
        }


        private AgeTiposLocalidades ValidarUpdate(
            IHttpContextAccessor _httpContextAccessor,
            AgeTiposLocalidadeSaveDAO ageTiposLocalidadeSaveDAO)
        {
            if (ageTiposLocalidadeSaveDAO == null)
                throw new InvalidSintaxisException();

            if (ageTiposLocalidadeSaveDAO.UsuarioModificacion == null || ageTiposLocalidadeSaveDAO.UsuarioModificacion <= 0)
                throw new InvalidFieldException("El usuario de modificación debe ser un número entero mayor a cero.");

            AgeTiposLocalidades? ageTiposLocalidade = ConsultarCompletePorId(new AgeTiposLocalidadePKDAO
            {
                agePaisCodigo = ageTiposLocalidadeSaveDAO.Id.agePaisCodigo,
                codigo = ageTiposLocalidadeSaveDAO.Id.codigo
            });

            if (ageTiposLocalidade == null)
                throw new RegisterNotFoundException("Tipo Localidad con código " + ageTiposLocalidadeSaveDAO.Id.codigo + " no existe");

            if (!string.IsNullOrWhiteSpace(ageTiposLocalidadeSaveDAO.Estado))
            {
                ageTiposLocalidade.Estado = ageTiposLocalidadeSaveDAO.Estado;
                ageTiposLocalidade.FechaEstado = DateTime.Now;
            }

            if (ageTiposLocalidadeSaveDAO.AgeTipLoAgePaisCodigo != null ||
                ageTiposLocalidadeSaveDAO.AgeTipLoCodigo != null)
                ValidarFKTipoLocalidad(ageTiposLocalidadeSaveDAO.AgeTipLoAgePaisCodigo ?? 0,
                                        ageTiposLocalidadeSaveDAO.AgeTipLoCodigo ?? 0);

            if (!string.IsNullOrWhiteSpace(ageTiposLocalidadeSaveDAO.Descripcion))
                ageTiposLocalidade.Descripcion = ageTiposLocalidadeSaveDAO.Descripcion;

            if (!string.IsNullOrWhiteSpace(ageTiposLocalidadeSaveDAO.ObservacionEstado))
                ageTiposLocalidade.ObservacionEstado = ageTiposLocalidadeSaveDAO.ObservacionEstado;

            ageTiposLocalidade.FechaModificacion = DateTime.Now;
            ageTiposLocalidade.UbicacionModificacion = Ubicacion.getIpAdress(_httpContextAccessor);
            ageTiposLocalidade.UsuarioModificacion = ageTiposLocalidadeSaveDAO.UsuarioModificacion;

            Validator.ValidateObject(ageTiposLocalidade, new ValidationContext(ageTiposLocalidade), true);

            return ageTiposLocalidade;
        }


        private AgeTiposLocalidades FromDAOToEntity(
            IHttpContextAccessor _httpContextAccessor,
            AgeTiposLocalidadeSaveDAO ageTiposLocalidadeSaveDAO)
        {
            return new AgeTiposLocalidades
            {
                AgePaisCodigo = ageTiposLocalidadeSaveDAO.Id.agePaisCodigo,
                Codigo = ageTiposLocalidadeSaveDAO.Id.codigo,
                AgeTipLoAgePaisCodigo = ageTiposLocalidadeSaveDAO.AgeTipLoAgePaisCodigo,
                AgeTipLoCodigo = ageTiposLocalidadeSaveDAO.AgeTipLoCodigo,
                Descripcion = ageTiposLocalidadeSaveDAO.Descripcion,
                Estado = ageTiposLocalidadeSaveDAO.Estado,
                FechaEstado = DateTime.Now,
                FechaIngreso = DateTime.Now,
                ObservacionEstado = ageTiposLocalidadeSaveDAO.ObservacionEstado,
                UbicacionIngreso = Ubicacion.getIpAdress(_httpContextAccessor),
                UsuarioIngreso = ageTiposLocalidadeSaveDAO.UsuarioIngreso
            };
        }


        private void ValidarKeys(AgeTiposLocalidadeSaveDAO ageTiposLocalidadeSaveDAO)
        {
            ValidarPK(ageTiposLocalidadeSaveDAO.Id);

            ValidarFKPais(ageTiposLocalidadeSaveDAO.Id.agePaisCodigo);

            if ((ageTiposLocalidadeSaveDAO.AgeTipLoCodigo != null) ||
                (ageTiposLocalidadeSaveDAO.AgeTipLoAgePaisCodigo != null))
                ValidarFKTipoLocalidad(ageTiposLocalidadeSaveDAO.AgeTipLoAgePaisCodigo ?? 0,
                                        ageTiposLocalidadeSaveDAO.AgeTipLoCodigo ?? 0);
    
        }


        private void ValidarPK(AgeTiposLocalidadePKDAO ageTiposLocalidadePKDAO)
        {

            ValidateKeys.ValidarNoExistenciaKey(
               ageTiposLocalidadePKDAO,
               $"Tipo localidad con código {ageTiposLocalidadePKDAO.codigo} ya existe.",
               _repository.ConsultarCompletePorId);
        }


        private void ValidarFKPais(int codigoPais)
        {
            ValidateKeys.ValidarExistenciaKey(
                codigoPais,
               $"País con código {codigoPais} no existe.",
               _repositoryPaises.ConsultarCompletePorId);
        }


        private void ValidarFKTipoLocalidad(int codigoPais, int codigo)
        {
            ValidateKeys.ValidarExistenciaKey(
                new AgeTiposLocalidadePKDAO
                {
                    agePaisCodigo = codigoPais,
                    codigo = codigo
                },
                $"Tipo localidad con código {codigo} no existe.",
                _repository.ConsultarCompletePorId);
        }

    }
}
