﻿using AGE.Entities.DAO.AgeTiposLocalidades;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;

namespace AGE.Services.AgeTiposLocalidadeService
{
    public interface IAgeTiposLocalidadesService
    {
        Task<Page<AgeTiposLocalidadeDAO>> ConsultarTodos(
            int codigoPais,
            int codigo,
            int codigoAgeTipLoAgePais,
            int codigoAgeTipLo,
            string descripcion,
            Pageable pageable);

        Task<Page<AgeTiposLocalidadeDAO>> ConsultarListaFiltro(
            string filtro,
            Pageable pageable);

        Task<AgeTiposLocalidadeDAO> ConsultarPorId(
            AgeTiposLocalidadePKDAO AgeTiposLocalidadePKDAO);

        Task<Resource<AgeTiposLocalidadeDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgeTiposLocalidadeSaveDAO ageTiposLocalidadeSaveDAO);

        Task<List<Resource<AgeTiposLocalidadeDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeTiposLocalidadeSaveDAO> ageTiposLocalidadeSaveDAOList);

        Task<AgeTiposLocalidadeDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeTiposLocalidadeSaveDAO ageTiposLocalidadeSaveDAO);

        Task<List<AgeTiposLocalidadeDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeTiposLocalidadeSaveDAO> ageTiposLocalidadeSaveDAOList);
    }
}
