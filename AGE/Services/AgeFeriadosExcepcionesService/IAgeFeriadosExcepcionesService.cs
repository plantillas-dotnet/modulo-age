﻿using AGE.Entities.DAO.AgeFeriadosExcepciones;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;

namespace AGE.Services.AgeFeriadosExcepcionesService
{
    public interface IAgeFeriadosExcepcionesService
    {
        Task<Page<AgeFeriadosExcepcionesDAO>> ConsultarTodos(int codigoLicenciatario,
                                                            string? tipoExcepcion,
                                                            Pageable pageable);

        Task<AgeFeriadosExcepcionesDAO> ConsultarPorId(AgeFeriadosExcepcionesPKDAO ageFeriadosExcepcionesPKDAO);

        Task<Page<AgeFeriadosExcepcionesDAO>> ConsultarListaFiltro(int codigoLicenciatario,
                                                                    string filtro,
                                                                    Pageable pageable);

        Task<Resource<AgeFeriadosExcepcionesDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgeFeriadosExcepcionesSaveDAO AgeFeriadosExcepcionesSaveDAO);

        Task<List<Resource<AgeFeriadosExcepcionesDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeFeriadosExcepcionesSaveDAO> AgeFeriadosExcepcionesSaveDAOList);

        Task<AgeFeriadosExcepcionesDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeFeriadosExcepcionesSaveDAO AgeFeriadosExcepcionesSaveDAO);

        Task<List<AgeFeriadosExcepcionesDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeFeriadosExcepcionesSaveDAO> AgeFeriadosExcepcionesSaveDAOList);
    }
}
