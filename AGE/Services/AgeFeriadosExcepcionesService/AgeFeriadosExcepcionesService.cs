﻿using AGE.Controllers;
using AGE.Entities.DAO.AgeFeriadosExcepciones;
using AGE.Entities;
using AGE.Middleware.Exceptions.BadRequest;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Repositories.AgeFeriadosExcepcionesRepository;
using AGE.Repositories.AgeLicenciatariosRepository;
using AGE.Services.AgeLicenciatariosAplicaSecuService;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using AGE.Utils;
using System.ComponentModel.DataAnnotations;
using System.Transactions;

namespace AGE.Services.AgeFeriadosExcepcionesService
{
    public class AgeFeriadosExcepcionesService : IAgeFeriadosExcepcionesService
    {
        private readonly IAgeFeriadosExcepcionesRepository _repository;
        private readonly IAgeLicenciatariosRepository _repositoryLicenciatarios;
        private readonly IAgeLicenciatariosAplicaSecuService _ageLicenciatariosAplicaSecuService;
        private readonly IAgeLicenciatariosRepository _repositoryLicenciatario;

        public AgeFeriadosExcepcionesService(IAgeFeriadosExcepcionesRepository Repository,
            IAgeLicenciatariosRepository RepositoryLicenciatarios,
            IAgeLicenciatariosAplicaSecuService ageLicenciatariosAplicaSecuService,
            IAgeLicenciatariosRepository repositoryLicenciatario)
        {
            _repository = Repository;
            _repositoryLicenciatarios = RepositoryLicenciatarios;
            _ageLicenciatariosAplicaSecuService = ageLicenciatariosAplicaSecuService;
            _repositoryLicenciatario = repositoryLicenciatario;
        }

        public Task<Page<AgeFeriadosExcepcionesDAO>> ConsultarTodos(int codigoLicenciatario,
                                                                    string? tipoExcepcion,
                                                                    Pageable pageable)
        {
            pageable.Validar<AgeFeriadosExcepcionesDAO>();

            return _repository.ConsultarTodos(codigoLicenciatario, tipoExcepcion, pageable);
        }

        public Task<AgeFeriadosExcepcionesDAO> ConsultarPorId(
            AgeFeriadosExcepcionesPKDAO ageFeriadosExcepcionesPKDAO)
        {

            Validator.ValidateObject(ageFeriadosExcepcionesPKDAO, new ValidationContext(ageFeriadosExcepcionesPKDAO), true);

            AgeFeriadosExcepcionesDAO ageFeriadosExcepcionesDAO = _repository.ConsultarPorId(ageFeriadosExcepcionesPKDAO).Result;

            return ageFeriadosExcepcionesDAO == null
                ? throw new RegisterNotFoundException("Feriados Excepciones con código " + ageFeriadosExcepcionesPKDAO.Codigo + " no existe")
                : Task.FromResult(ageFeriadosExcepcionesDAO);
        }

        private AgeFeriadosExcepciones ConsultarCompletePorId(AgeFeriadosExcepcionesPKDAO ageFeriadosExcepcionesPKDAO)
        {
            Validator.ValidateObject(ageFeriadosExcepcionesPKDAO, new ValidationContext(ageFeriadosExcepcionesPKDAO), true);

            AgeFeriadosExcepciones? ageFeriadosExcepcione = _repository.ConsultarCompletePorId(ageFeriadosExcepcionesPKDAO).Result;

            return ageFeriadosExcepcione;
        }


        public Task<Page<AgeFeriadosExcepcionesDAO>> ConsultarListaFiltro(int codigoLicenciatario,
                                                                    string filtro,
                                                                    Pageable pageable)
        {
            if (codigoLicenciatario == 0)
            {
                throw new InvalidFieldException("El código de licenciatario debe ser un número entero mayor a cero.");
            }

            if (string.IsNullOrWhiteSpace(filtro))
            {
                throw new InvalidFieldException("El filtro es requerido.");
            }

            pageable.Validar<AgeFeriadosExcepcionesDAO>();

            return _repository.ConsultarListaFiltro(codigoLicenciatario, filtro.ToLower(), pageable);
        }

        public async Task<Resource<AgeFeriadosExcepcionesDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgeFeriadosExcepcionesSaveDAO ageFeriadosExcepcionesSaveDAO)
        {
            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    AgeFeriadosExcepciones ageFeriadosExcepciones = ValidarInsert(_httpContextAccessor, ageFeriadosExcepcionesSaveDAO);

                    AgeFeriadosExcepcionesDAO ageFeriadosExcepcionesDAO = await _repository.Insertar(ageFeriadosExcepciones);

                    Resource<AgeFeriadosExcepcionesDAO> ageFeriadosExcepcionesDAOWithResource = GetDataWithResource(_httpContextAccessor, ageFeriadosExcepcionesDAO);

                    transactionScope.Complete();

                    return ageFeriadosExcepcionesDAOWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public async Task<List<Resource<AgeFeriadosExcepcionesDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeFeriadosExcepcionesSaveDAO> ageFeriadosExcepcionesSaveDAOList)
        {
            if (ageFeriadosExcepcionesSaveDAOList == null)
            {
                throw new InvalidSintaxisException();
            }

            List<AgeFeriadosExcepciones> ageFeriadosExcepcionesList = new List<AgeFeriadosExcepciones>();

            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    foreach (AgeFeriadosExcepcionesSaveDAO ageFeriadosExcepcionesSaveDAO in ageFeriadosExcepcionesSaveDAOList)
                    {
                        AgeFeriadosExcepciones ageFeriadosExcepciones = ValidarInsert(_httpContextAccessor, ageFeriadosExcepcionesSaveDAO);
                        ageFeriadosExcepcionesList.Add(ageFeriadosExcepciones);
                    }

                    List<AgeFeriadosExcepcionesDAO> ageFeriadosExcepcionesDAOList = await _repository.InsertarVarios(ageFeriadosExcepcionesList);

                    List<Resource<AgeFeriadosExcepcionesDAO>> ageFeriadosExcepcionesDAOListWithResource = new List<Resource<AgeFeriadosExcepcionesDAO>>();

                    foreach (AgeFeriadosExcepcionesDAO ageFeriadosExcepcionesDAO in ageFeriadosExcepcionesDAOList)
                    {
                        ageFeriadosExcepcionesDAOListWithResource.Add(GetDataWithResource(_httpContextAccessor, ageFeriadosExcepcionesDAO));
                    }

                    transactionScope.Complete();

                    return ageFeriadosExcepcionesDAOListWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public Task<AgeFeriadosExcepcionesDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeFeriadosExcepcionesSaveDAO ageFeriadosExcepcionesSaveDAO)
        {
            AgeFeriadosExcepciones ageFeriadosExcepciones = ValidarUpdate(_httpContextAccessor, ageFeriadosExcepcionesSaveDAO);

            return _repository.Actualizar(ageFeriadosExcepciones);
        }

        public Task<List<AgeFeriadosExcepcionesDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeFeriadosExcepcionesSaveDAO> ageFeriadosExcepcionesSaveDAOList)
        {
            if (ageFeriadosExcepcionesSaveDAOList == null)
                throw new InvalidSintaxisException();

            ValidateKeys.ValidarPKDuplicadas(ageFeriadosExcepcionesSaveDAOList,
                                             p => new { p.Id.AgeLicencCodigo, p.Id.Codigo },
                                             "Id");

            List<AgeFeriadosExcepciones> ageFeriadosExcepcionesList = new List<AgeFeriadosExcepciones>();
            AgeFeriadosExcepciones ageFeriadosExcepciones;

            foreach (AgeFeriadosExcepcionesSaveDAO ageFeriadosExcepcionesSaveDAO in ageFeriadosExcepcionesSaveDAOList)
            {
                ageFeriadosExcepciones = ValidarUpdate(_httpContextAccessor, ageFeriadosExcepcionesSaveDAO);
                ageFeriadosExcepcionesList.Add(ageFeriadosExcepciones);
            }

            return _repository.ActualizarVarios(ageFeriadosExcepcionesList);
        }

        private Resource<AgeFeriadosExcepcionesDAO> GetDataWithResource(
            IHttpContextAccessor _httpContextAccessor,
            AgeFeriadosExcepcionesDAO ageFeriadosExcepcionesDAO)
        {
            string rutaSegmentoFinal = $"{ageFeriadosExcepcionesDAO.Id.AgeLicencCodigo}/{ageFeriadosExcepcionesDAO.Id.Codigo}";

            return Resource<AgeFeriadosExcepcionesDAO>.GetDataWithResource<AgeFeriadosExcepcionesController>(
                _httpContextAccessor, rutaSegmentoFinal, ageFeriadosExcepcionesDAO);
        }

        private AgeFeriadosExcepciones ValidarInsert(
            IHttpContextAccessor _httpContextAccessor,
            AgeFeriadosExcepcionesSaveDAO ageFeriadosExcepcionesSaveDAO)
        {
            if (ageFeriadosExcepcionesSaveDAO == null)
                throw new InvalidSintaxisException();

            Validator.ValidateObject(ageFeriadosExcepcionesSaveDAO, new ValidationContext(ageFeriadosExcepcionesSaveDAO), true);

            FuncionesSecuencias fg = new FuncionesSecuencias(_ageLicenciatariosAplicaSecuService);
            ageFeriadosExcepcionesSaveDAO.Id.Codigo = fg.ObtenerSecuencia(
                                                        _httpContextAccessor,
                                                        ageFeriadosExcepcionesSaveDAO.Id.AgeLicencCodigo,
                                                        Globales.CODIGO_SECUENCIA_FERIADO_EXCEPCION,
                                                        ageFeriadosExcepcionesSaveDAO.UsuarioIngreso);

            ValidarKeys(ageFeriadosExcepcionesSaveDAO);

            AgeFeriadosExcepciones entityObject = FromDAOToEntity(_httpContextAccessor, ageFeriadosExcepcionesSaveDAO);

            Validator.ValidateObject(entityObject, new ValidationContext(entityObject), true);

            return entityObject;
        }


        private AgeFeriadosExcepciones ValidarUpdate(
        IHttpContextAccessor _httpContextAccessor, 
        AgeFeriadosExcepcionesSaveDAO ageFeriadosExcepcionesSaveDAO)
        {
            if (ageFeriadosExcepcionesSaveDAO == null)
                throw new InvalidSintaxisException();

            if (ageFeriadosExcepcionesSaveDAO.UsuarioModificacion == null || ageFeriadosExcepcionesSaveDAO.UsuarioModificacion <= 0)
                throw new InvalidFieldException("El usuario de modificación debe ser un número entero mayor a cero.");
            
            AgeFeriadosExcepciones? ageFeriadosExcepcione = ConsultarCompletePorId(new AgeFeriadosExcepcionesPKDAO
            {
                AgeLicencCodigo = ageFeriadosExcepcionesSaveDAO.Id.AgeLicencCodigo,
                Codigo = ageFeriadosExcepcionesSaveDAO.Id.Codigo
            }) ?? throw new RegisterNotFoundException("Feriado Excepción con código: " + ageFeriadosExcepcionesSaveDAO.Id.Codigo + " no existe");

            if (ageFeriadosExcepcionesSaveDAO.Estado != null)
            {
                ageFeriadosExcepcione.Estado = ageFeriadosExcepcionesSaveDAO.Estado;
                ageFeriadosExcepcione.FechaEstado = DateTime.Now;
            }

            if (!string.IsNullOrWhiteSpace(ageFeriadosExcepcionesSaveDAO.ObservacionEstado))
                ageFeriadosExcepcione.ObservacionEstado = ageFeriadosExcepcionesSaveDAO.ObservacionEstado;

            ageFeriadosExcepcione.FechaModificacion = DateTime.Now;
            ageFeriadosExcepcione.UbicacionModificacion = Ubicacion.getIpAdress(_httpContextAccessor);
            ageFeriadosExcepcione.UsuarioModificacion = ageFeriadosExcepcionesSaveDAO.UsuarioModificacion;

            if (ageFeriadosExcepcionesSaveDAO.FechaDesde != default)
                ageFeriadosExcepcione.FechaDesde = ageFeriadosExcepcionesSaveDAO.FechaDesde;

            if (ageFeriadosExcepcionesSaveDAO.FechaHasta != default)
                ageFeriadosExcepcione.FechaHasta = ageFeriadosExcepcionesSaveDAO.FechaHasta;

            if (ageFeriadosExcepcionesSaveDAO.HoraDesde != null && ageFeriadosExcepcionesSaveDAO.HoraDesde != default)
                ageFeriadosExcepcione.HoraDesde = ageFeriadosExcepcionesSaveDAO.HoraDesde;

            if (ageFeriadosExcepcionesSaveDAO.HoraHasta != null && ageFeriadosExcepcionesSaveDAO.HoraHasta != default)
                ageFeriadosExcepcione.HoraHasta = ageFeriadosExcepcionesSaveDAO.HoraHasta;

            if (ageFeriadosExcepcionesSaveDAO.Observacion != null)
                ageFeriadosExcepcione.Observacion = ageFeriadosExcepcionesSaveDAO.Observacion;

            if (ageFeriadosExcepcionesSaveDAO.TipoExcepcion != null)
                ageFeriadosExcepcione.TipoExcepcion = ageFeriadosExcepcionesSaveDAO.TipoExcepcion;

            Validator.ValidateObject(ageFeriadosExcepcione, new ValidationContext(ageFeriadosExcepcione), true);

            return ageFeriadosExcepcione;
        }

        private AgeFeriadosExcepciones FromDAOToEntity(
        IHttpContextAccessor _httpContextAccessor,
        AgeFeriadosExcepcionesSaveDAO ageFeriadosExcepcionesSaveDAO)
        {
            return new AgeFeriadosExcepciones
            {
                Codigo = ageFeriadosExcepcionesSaveDAO.Id.Codigo,
                AgeLicencCodigo = ageFeriadosExcepcionesSaveDAO.Id.AgeLicencCodigo,
                Estado = ageFeriadosExcepcionesSaveDAO.Estado,
                FechaEstado = DateTime.Now,
                FechaIngreso = DateTime.Now,
                ObservacionEstado = ageFeriadosExcepcionesSaveDAO.ObservacionEstado,
                UbicacionIngreso = Ubicacion.getIpAdress(_httpContextAccessor),
                UsuarioIngreso = ageFeriadosExcepcionesSaveDAO.UsuarioIngreso,
                Observacion = ageFeriadosExcepcionesSaveDAO.Observacion,
                FechaDesde = ageFeriadosExcepcionesSaveDAO.FechaDesde,
                FechaHasta = ageFeriadosExcepcionesSaveDAO.FechaHasta,
                HoraDesde = ageFeriadosExcepcionesSaveDAO.HoraDesde,
                HoraHasta = ageFeriadosExcepcionesSaveDAO.HoraHasta,
                TipoExcepcion = ageFeriadosExcepcionesSaveDAO.TipoExcepcion
            };
        }

        private void ValidarKeys(AgeFeriadosExcepcionesSaveDAO ageFeriadosExcepcionesSaveDAO)
        {
            ValidarPK(ageFeriadosExcepcionesSaveDAO.Id);
            ValidarFKLicenciatario(ageFeriadosExcepcionesSaveDAO.Id.AgeLicencCodigo);
        }

        private void ValidarPK(AgeFeriadosExcepcionesPKDAO ageFeriadosExcepcionesPKDAO)
        {
            ValidateKeys.ValidarNoExistenciaKey(
               ageFeriadosExcepcionesPKDAO,
               $"Feriado excepción con código {ageFeriadosExcepcionesPKDAO.Codigo} ya existe.",
               _repository.ConsultarCompletePorId);
        }

        private void ValidarFKLicenciatario(int codigoLicenciatario)
        {
            ValidateKeys.ValidarExistenciaKey(
                codigoLicenciatario,
                $"Licenciatario con código {codigoLicenciatario} no existe.",
                _repositoryLicenciatario.ConsultarCompletePorId);
        }

    }
}
