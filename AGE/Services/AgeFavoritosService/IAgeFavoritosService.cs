﻿using AGE.Entities.DAO.AgeFavoritos;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;

namespace AGE.Services.AgeFavoritosService
{
    public interface IAgeFavoritosService
    {
        Task<Page<AgeFavoritosDAO>> ConsultarTodos(
            int codigoLicenciatario,
            int codigoUsuario,
            Pageable pageable);

        Task<Page<AgeFavoritosDAO>> ConsultarListaFiltro(
            string filtro,
            int codigoLicenciatario,
            int codigoUsuario,
            Pageable pageable);

        Task<AgeFavoritosDAO> ConsultarPorId(AgeFavoritosPKDAO ageFavoritosPKDAO);

        Task<Resource<AgeFavoritosDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgeFavoritosSaveDAO AgeFavoritosSaveDAO);

        Task<List<Resource<AgeFavoritosDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeFavoritosSaveDAO> AgeFavoritosSaveDAOList);

        Task<AgeFavoritosDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeFavoritosSaveDAO AgeFavoritosSaveDAO);

        Task<List<AgeFavoritosDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeFavoritosSaveDAO> AgeFavoritosSaveDAOList);
    }
}
