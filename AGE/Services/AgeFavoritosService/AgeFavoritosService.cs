﻿using AGE.Controllers;
using AGE.Entities.DAO.AgeFavoritos;
using AGE.Entities;
using AGE.Middleware.Exceptions.BadRequest;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Repositories.AgeFavoritosRepository;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using AGE.Utils;
using System.ComponentModel.DataAnnotations;
using System.Transactions;
using AGE.Repositories.AgeTransaccioneRepository;
using AGE.Entities.DAO.AgeTransacciones;
using AGE.Services.AgeLicenciatariosAplicaSecuService;
using AGE.Entities.DAO.AgeUsuarios;
using AGE.Repositories.AgeUsuarioRepository;

namespace AGE.Services.AgeFavoritosService
{
    public class AgeFavoritosService : IAgeFavoritosService
    {
        private readonly IAgeFavoritosRepository _repository;
        private readonly IAgeTransaccionesRepository _repositoryTransacciones;
        private readonly IAgeUsuariosRepository _repositoryUsuari;
        private readonly IAgeLicenciatariosAplicaSecuService _ageLicenciatariosAplicaSecuService;

        public AgeFavoritosService(
            IAgeFavoritosRepository Repository,
            IAgeTransaccionesRepository RepositoryTransacciones,
            IAgeUsuariosRepository RepositoryUsuari,
            IAgeLicenciatariosAplicaSecuService ageLicenciatariosAplicaSecuService)
        {
            _repository = Repository;
            _repositoryTransacciones = RepositoryTransacciones;
            _repositoryUsuari = RepositoryUsuari;
            _ageLicenciatariosAplicaSecuService = ageLicenciatariosAplicaSecuService;
        }

        public Task<Page<AgeFavoritosDAO>> ConsultarTodos(
            int codigoLicenciatario,
            int codigoUsuario,
            Pageable pageable)
        {

            pageable.Validar<AgeFavoritosDAO>();

            return _repository.ConsultarTodos(codigoLicenciatario, codigoUsuario, pageable);
        }

        public Task<AgeFavoritosDAO> ConsultarPorId(
            AgeFavoritosPKDAO ageFavoritosPKDAO)
        {
            Validator.ValidateObject(ageFavoritosPKDAO, new ValidationContext(ageFavoritosPKDAO), true);

            AgeFavoritosDAO ageFavoritosDAO = _repository.ConsultarPorId(ageFavoritosPKDAO).Result;

            return ageFavoritosDAO == null
                ? throw new RegisterNotFoundException("Favorito con código: "
                    + ageFavoritosPKDAO.Codigo + ", código aplicación: "
                    + ageFavoritosPKDAO.AgeTransaAgeAplicaCodigo + ", código transacción: "
                    + ageFavoritosPKDAO.AgeTransaCodigo + ", código usuario: "
                    + ageFavoritosPKDAO.AgeUsuariCodigo + " y código licenciatario: "
                    + ageFavoritosPKDAO.AgeUsuariAgeLicencCodigo + " no existe.")
                : Task.FromResult(ageFavoritosDAO);
        }

        private AgeFavoritos ConsultarCompletePorId(
            AgeFavoritosPKDAO ageFavoritosPKDAO)
        {
            Validator.ValidateObject(ageFavoritosPKDAO, new ValidationContext(ageFavoritosPKDAO), true);

            AgeFavoritos ageFavoritos = _repository.ConsultarCompletePorId(ageFavoritosPKDAO).Result;

            return ageFavoritos;
        }

        public Task<Page<AgeFavoritosDAO>> ConsultarListaFiltro(
            string filtro,
            int codigoLicenciatario,
            int codigoUsuario,
            Pageable pageable)
        {
            if (string.IsNullOrWhiteSpace(filtro))
                throw new InvalidFieldException("El filtro es requerido.");

            pageable.Validar<AgeFavoritosDAO>();

            return _repository.ConsultarListaFiltro(filtro.ToLower(), codigoLicenciatario, codigoUsuario, pageable);
        }

        public async Task<Resource<AgeFavoritosDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgeFavoritosSaveDAO ageFavoritosSaveDAO)
        {
            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    AgeFavoritos ageFavoritos = ValidarInsert(_httpContextAccessor, ageFavoritosSaveDAO);

                    AgeFavoritosDAO ageFavoritosDAO = await _repository.Insertar(ageFavoritos);

                    Resource<AgeFavoritosDAO> ageFavoritosDAOWithResource = GetDataWithResource(_httpContextAccessor, ageFavoritosDAO);

                    transactionScope.Complete();

                    return ageFavoritosDAOWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public async Task<List<Resource<AgeFavoritosDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeFavoritosSaveDAO> ageFavoritosSaveDAOList)
        {
            if (ageFavoritosSaveDAOList == null)
                throw new InvalidSintaxisException();

            List<AgeFavoritos> ageFavoritosList = new List<AgeFavoritos>();

            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    foreach (AgeFavoritosSaveDAO ageFavoritosSaveDAO in ageFavoritosSaveDAOList)
                    {
                        AgeFavoritos AgeFavorito = ValidarInsert(_httpContextAccessor, ageFavoritosSaveDAO);
                        ageFavoritosList.Add(AgeFavorito);
                    }

                    List<AgeFavoritosDAO> ageFavoritosDAOList = await _repository.InsertarVarios(ageFavoritosList);

                    List<Resource<AgeFavoritosDAO>> ageFavoritosDAOListWithResource = new List<Resource<AgeFavoritosDAO>>();

                    foreach (AgeFavoritosDAO ageFavoritosDAO in ageFavoritosDAOList)
                    {
                        ageFavoritosDAOListWithResource.Add(GetDataWithResource(_httpContextAccessor, ageFavoritosDAO));
                    }

                    transactionScope.Complete();

                    return ageFavoritosDAOListWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public Task<AgeFavoritosDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeFavoritosSaveDAO ageFavoritosSaveDAO)
        {
            AgeFavoritos ageFavoritos = ValidarUpdate(_httpContextAccessor, ageFavoritosSaveDAO);

            return _repository.Actualizar(ageFavoritos);
        }

        public Task<List<AgeFavoritosDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeFavoritosSaveDAO> ageFavoritosSaveDAOList)
        {
            if (ageFavoritosSaveDAOList == null)
                throw new InvalidSintaxisException();

            ValidateKeys.ValidarPKDuplicadas(ageFavoritosSaveDAOList, p => new
            {
                p.Id.AgeUsuariCodigo,
                p.Id.AgeTransaAgeAplicaCodigo,
                p.Id.Codigo,
                p.Id.AgeUsuariAgeLicencCodigo,
                p.Id.AgeTransaCodigo
            }, "Id");

            List<AgeFavoritos> ageFavoritosList = new List<AgeFavoritos>();
            AgeFavoritos ageFavoritos;

            foreach (AgeFavoritosSaveDAO ageFavoritosSaveDAO in ageFavoritosSaveDAOList)
            {
                ageFavoritos = ValidarUpdate(_httpContextAccessor, ageFavoritosSaveDAO);
                ageFavoritosList.Add(ageFavoritos);
            }

            return _repository.ActualizarVarios(ageFavoritosList);
        }

        private static Resource<AgeFavoritosDAO> GetDataWithResource(
            IHttpContextAccessor _httpContextAccessor,
            AgeFavoritosDAO ageFavoritosDAO)
        {
            string rutaSegmentoFinal = $"{ageFavoritosDAO.Id.AgeUsuariAgeLicencCodigo}/{ageFavoritosDAO.Id.AgeUsuariCodigo}/{ageFavoritosDAO.Id.AgeTransaAgeAplicaCodigo}/{ageFavoritosDAO.Id.AgeTransaCodigo}/{ageFavoritosDAO.Id.Codigo}";

            return Resource<AgeFavoritosDAO>.GetDataWithResource<AgeFavoritosController>(
                _httpContextAccessor, rutaSegmentoFinal, ageFavoritosDAO);
        }

        private AgeFavoritos ValidarInsert(
            IHttpContextAccessor _httpContextAccessor,
            AgeFavoritosSaveDAO ageFavoritosSaveDAO)
        {

            if (ageFavoritosSaveDAO == null)
                throw new InvalidSintaxisException();

            Validator.ValidateObject(ageFavoritosSaveDAO, new ValidationContext(ageFavoritosSaveDAO), true);

            if (ageFavoritosSaveDAO.FechaDesde == default)
                throw new InvalidFieldException("La fecha desde no puede ser nula.");

            FuncionesSecuencias fg = new(_ageLicenciatariosAplicaSecuService);

            ageFavoritosSaveDAO.Id.Codigo = fg.ObtenerSecuencia(_httpContextAccessor,
                ageFavoritosSaveDAO.Id.AgeUsuariAgeLicencCodigo,
                Globales.CODIGO_SECUENCIA_FAVORITO,
                ageFavoritosSaveDAO.UsuarioIngreso);

            ValidarKeys(ageFavoritosSaveDAO.Id);

            AgeFavoritos entityObject = FromDAOToEntity(_httpContextAccessor,ageFavoritosSaveDAO);

            Validator.ValidateObject(entityObject, new ValidationContext(entityObject), true);

            return entityObject;
        }

        private AgeFavoritos ValidarUpdate(
            IHttpContextAccessor _httpContextAccessor,
            AgeFavoritosSaveDAO ageFavoritosSaveDAO)
        {
            if (ageFavoritosSaveDAO == null)
                throw new InvalidSintaxisException();

            if (ageFavoritosSaveDAO.UsuarioModificacion == null || ageFavoritosSaveDAO.UsuarioModificacion <= 0)
                throw new InvalidFieldException("El usuario de modificación debe ser un número entero mayor a cero.");

            ValidarFKs(ageFavoritosSaveDAO.Id);

            AgeFavoritos ageFavoritos = ConsultarCompletePorId(ageFavoritosSaveDAO.Id) ??
                throw new RegisterNotFoundException($"Favorito con código: "
                                                    + $"{ageFavoritosSaveDAO.Id.Codigo}, código aplicación: "
                                                    + $"{ageFavoritosSaveDAO.Id.AgeTransaAgeAplicaCodigo}, código transacción: "
                                                    + $"{ageFavoritosSaveDAO.Id.AgeTransaCodigo}, código usuario: "
                                                    + $"{ageFavoritosSaveDAO.Id.AgeUsuariCodigo} y código licenciatario: "
                                                    + $"{ageFavoritosSaveDAO.Id.AgeUsuariAgeLicencCodigo} no existe.");

            if (!string.IsNullOrWhiteSpace(ageFavoritosSaveDAO.Estado))
            {
                ageFavoritos.Estado = ageFavoritosSaveDAO.Estado;
                ageFavoritos.FechaEstado = DateTime.Now;
            }

            if (!string.IsNullOrWhiteSpace(ageFavoritosSaveDAO.ObservacionEstado))
                ageFavoritos.ObservacionEstado = ageFavoritosSaveDAO.ObservacionEstado;

            ageFavoritos.FechaDesde = ageFavoritosSaveDAO.FechaDesde != default ? ageFavoritosSaveDAO.FechaDesde : ageFavoritos.FechaDesde;
            ageFavoritos.FechaHasta = ageFavoritosSaveDAO.FechaHasta != null && ageFavoritosSaveDAO.FechaHasta != default ? ageFavoritosSaveDAO.FechaHasta : ageFavoritos.FechaHasta;
            ageFavoritos.FechaModificacion = DateTime.Now;
            ageFavoritos.UbicacionModificacion = Ubicacion.getIpAdress(_httpContextAccessor);
            ageFavoritos.UsuarioModificacion = ageFavoritosSaveDAO.UsuarioModificacion;
            Validator.ValidateObject(ageFavoritos, new ValidationContext(ageFavoritos), true);

            return ageFavoritos;
        }

        private AgeFavoritos FromDAOToEntity(
           IHttpContextAccessor _httpContextAccessor,
           AgeFavoritosSaveDAO ageFavoritosSaveDAO)
        {

            return new AgeFavoritos
            {
                AgeUsuariAgeLicencCodigo = ageFavoritosSaveDAO.Id.AgeUsuariAgeLicencCodigo,
                AgeUsuariCodigo = ageFavoritosSaveDAO.Id.AgeUsuariCodigo,
                AgeTransaAgeAplicaCodigo = ageFavoritosSaveDAO.Id.AgeTransaAgeAplicaCodigo,
                AgeTransaCodigo = ageFavoritosSaveDAO.Id.AgeTransaCodigo,
                Codigo = ageFavoritosSaveDAO.Id.Codigo,
                FechaDesde = ageFavoritosSaveDAO.FechaDesde,
                FechaHasta = ageFavoritosSaveDAO.FechaHasta == default
                                        ? null : ageFavoritosSaveDAO.FechaHasta,
                Estado = ageFavoritosSaveDAO.Estado,
                FechaEstado = DateTime.Now,
                FechaIngreso = DateTime.Now,
                ObservacionEstado = ageFavoritosSaveDAO.ObservacionEstado,
                UbicacionIngreso = Ubicacion.getIpAdress(_httpContextAccessor),
                UsuarioIngreso = ageFavoritosSaveDAO.UsuarioIngreso
            };
        }

        private void ValidarKeys(AgeFavoritosPKDAO ageFavoritosPKDAO)
        {
            ValidarPk(ageFavoritosPKDAO);
            ValidarFKs(ageFavoritosPKDAO);
        }

        private void ValidarPk(AgeFavoritosPKDAO ageFavoritosPKDAO)
        {
            ValidateKeys.ValidarNoExistenciaKey(ageFavoritosPKDAO, $"Favorito con código: "
                    + $"{ageFavoritosPKDAO.Codigo}, código aplicación: "
                    + $"{ageFavoritosPKDAO.AgeTransaAgeAplicaCodigo}, código transacción: "
                    + $"{ageFavoritosPKDAO.AgeTransaCodigo}, código usuario: "
                    + $"{ageFavoritosPKDAO.AgeUsuariCodigo} y código licenciatario: "
                    + $"{ageFavoritosPKDAO.AgeUsuariAgeLicencCodigo} no existe."
                    , _repository.ConsultarCompletePorId);

        }

        private void ValidarFKs(AgeFavoritosPKDAO ageFavoritosPKDAO)
        {

            AgeTransaccionePKDAO ageTransaccionePKDAO = new AgeTransaccionePKDAO
            {
                ageAplicaCodigo = ageFavoritosPKDAO.AgeTransaAgeAplicaCodigo,
                codigo = ageFavoritosPKDAO.AgeTransaCodigo
            };
            ValidarFkTransaccion(ageTransaccionePKDAO);

            AgeUsuarioPKDAO ageUsuarioPK = new()
            {
                ageLicencCodigo = ageFavoritosPKDAO.AgeUsuariAgeLicencCodigo,
                codigo = ageFavoritosPKDAO.AgeUsuariCodigo
            };
            ValidarFkUsuario(ageUsuarioPK);
        }

        private void ValidarFkTransaccion(AgeTransaccionePKDAO ageTransaccionePKDAO)
        {
            ValidateKeys.ValidarExistenciaKey(ageTransaccionePKDAO,
                        $"Transacción con código: {ageTransaccionePKDAO.codigo} " +
                        $"y código de aplicación: {ageTransaccionePKDAO.ageAplicaCodigo} no existe."
                        , _repositoryTransacciones.ConsultarCompletePorId);
        }

        private void ValidarFkUsuario(AgeUsuarioPKDAO ageUsuarioPKDAO)
        {
            ValidateKeys.ValidarExistenciaKey(ageUsuarioPKDAO,
                $"Usuario con código: {ageUsuarioPKDAO.codigo} y código licenciatario: {ageUsuarioPKDAO.ageLicencCodigo} no existe."
                , _repositoryUsuari.ConsultarCompletePorId);
        }
    }
}
