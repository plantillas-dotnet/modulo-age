﻿using AGE.Controllers;
using AGE.Entities;
using AGE.Entities.DAO.AgePerfiles;
using AGE.Entities.DAO.AgeUsuarios;
using AGE.Entities.DAO.AgeUsuariosPerfiles;
using AGE.Middleware.Exceptions.BadRequest;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Repositories.AgePerfilesRepository;
using AGE.Repositories.AgeUsuarioRepository;
using AGE.Repositories.AgeUsuariosPerfileRepository;
using AGE.Services.AgeLicenciatariosAplicaSecuService;
using AGE.Utils;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using System.ComponentModel.DataAnnotations;
using System.Transactions;

namespace AGE.Services.AgeUsuariosPerfileService
{
    public class AgeUsuariosPerfilesService : IAgeUsuariosPerfilesService
    {
        private readonly IAgeUsuariosPerfilesRepository _repository;
        private readonly IAgePerfilesRepository _repositoryPerfile;
        private readonly IAgeUsuariosRepository _repositoryUsuari;
        private readonly IAgeLicenciatariosAplicaSecuService _ageLicenciatariosAplicaSecuService;

        public AgeUsuariosPerfilesService(IAgeUsuariosPerfilesRepository Repository,
            IAgePerfilesRepository RepositoryPerfile,
            IAgeUsuariosRepository RepositoryUsuario,
            IAgeLicenciatariosAplicaSecuService ageLicenciatariosAplicaSecuService)
        {
            _repository = Repository;
            _repositoryPerfile = RepositoryPerfile;
            _repositoryUsuari = RepositoryUsuario;
            _ageLicenciatariosAplicaSecuService = ageLicenciatariosAplicaSecuService;
        }

        public Task<Page<AgeUsuariosPerfileDAO>> ConsultarTodos(int codigoLicenciatario,
                                                                Pageable pageable)
        {
            if (codigoLicenciatario <= 0)
                throw new InvalidFieldException("El código de licenciatario debe ser un número entero mayor a cero.");

            pageable.Validar<AgeUsuariosPerfileDAO>();

            return _repository.ConsultarTodos(codigoLicenciatario,pageable);
        }

        public Task<AgeUsuariosPerfileDAO> ConsultarPorId(
            AgeUsuariosPerfilePKDAO ageUsuariosPerfilePKDAO)
        {
            Validator.ValidateObject(ageUsuariosPerfilePKDAO, new ValidationContext(ageUsuariosPerfilePKDAO), true);

            AgeUsuariosPerfileDAO ageUsuariosPerfilesDAO = _repository.ConsultarPorId(ageUsuariosPerfilePKDAO).Result;

            return ageUsuariosPerfilesDAO == null
                ? throw new RegisterNotFoundException("Usuario Perfil con código " + ageUsuariosPerfilePKDAO.Codigo + " no existe.")
                : Task.FromResult(ageUsuariosPerfilesDAO);
        }

        private AgeUsuariosPerfiles ConsultarCompletePorId(AgeUsuariosPerfilePKDAO ageUsuariosPerfilePKDAO)
        {
            Validator.ValidateObject(ageUsuariosPerfilePKDAO, new ValidationContext(ageUsuariosPerfilePKDAO), true);

            AgeUsuariosPerfiles? ageUsuariosPerfile = _repository.ConsultarCompletePorId(ageUsuariosPerfilePKDAO).Result;

            return ageUsuariosPerfile;
        }

        public Task<Page<AgeUsuariosPerfileDAO>> ConsultarListaFiltro(int codigoLicenciatario,
                                                                    string filtro,
                                                                    Pageable pageable)
        {
            if (codigoLicenciatario == 0)
                throw new InvalidFieldException("El código de licenciatario debe ser un número entero mayor a cero.");

            if (string.IsNullOrWhiteSpace(filtro))
                throw new InvalidFieldException("El filtro es requerido.");

            pageable.Validar<AgeUsuariosPerfileDAO>();

            return _repository.ConsultarListaFiltro(codigoLicenciatario, filtro.ToLower(), pageable);
        }


        public async Task<Resource<AgeUsuariosPerfileDAO>> Insertar(IHttpContextAccessor _httpContextAccessor, AgeUsuariosPerfileSaveDAO AgeUsuariosPerfileSaveDAO)
        {
            
            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    AgeUsuariosPerfiles ageUsuariosPerfile = ValidarInsert(_httpContextAccessor, AgeUsuariosPerfileSaveDAO);
                    AgeUsuariosPerfileDAO ageUsuariosPerfileDAO = await _repository.Insertar(ageUsuariosPerfile);
                    
                    Resource<AgeUsuariosPerfileDAO> ageUsuarioPerfileDAOWithResource = GetDataWithResource(_httpContextAccessor, ageUsuariosPerfileDAO);

                    transactionScope.Complete();

                    return ageUsuarioPerfileDAOWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }
        
        public async Task<List<Resource<AgeUsuariosPerfileDAO>>> InsertarVarios(IHttpContextAccessor _httpContextAccessor, List<AgeUsuariosPerfileSaveDAO> AgeUsuariosPerfileSaveDAOList)
        {
            if (AgeUsuariosPerfileSaveDAOList == null)
                throw new InvalidSintaxisException();

            List<AgeUsuariosPerfiles> ageUsuariosPerfileList = new();

            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    foreach (AgeUsuariosPerfileSaveDAO ageUsuariosPerfileSaveDAO in AgeUsuariosPerfileSaveDAOList)
                    {
                        AgeUsuariosPerfiles ageUsuarioPerfile = ValidarInsert(_httpContextAccessor, ageUsuariosPerfileSaveDAO);
                        ageUsuariosPerfileList.Add(ageUsuarioPerfile);
                    }

                    List<AgeUsuariosPerfileDAO> ageUsuariosPerfileDAOList = await _repository.InsertarVarios(ageUsuariosPerfileList);
                    List<Resource<AgeUsuariosPerfileDAO>> ageUsuariosPerfileDAOListWithResource = new();

                    foreach (AgeUsuariosPerfileDAO ageUsuarioPerfileDAO in ageUsuariosPerfileDAOList)
                    {
                        ageUsuariosPerfileDAOListWithResource.Add(GetDataWithResource(_httpContextAccessor, ageUsuarioPerfileDAO));
                    }

                    transactionScope.Complete();

                    return ageUsuariosPerfileDAOListWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public Task<AgeUsuariosPerfileDAO> Actualizar(IHttpContextAccessor _httpContextAccessor, AgeUsuariosPerfileSaveDAO AgeUsuariosPerfileSaveDAO)
        {
            AgeUsuariosPerfiles ageUsuarioPerfile = ValidarUpdate(_httpContextAccessor, AgeUsuariosPerfileSaveDAO);

            return _repository.Actualizar(ageUsuarioPerfile);
        }

        public Task<List<AgeUsuariosPerfileDAO>> ActualizarVarios(IHttpContextAccessor _httpContextAccessor, List<AgeUsuariosPerfileSaveDAO> AgeUsuariosPerfileSaveDAOList)
        {
            if (AgeUsuariosPerfileSaveDAOList == null)
                throw new InvalidSintaxisException();

            ValidateKeys.ValidarPKDuplicadas(AgeUsuariosPerfileSaveDAOList,
                                             p => new { p.Id.AgeUsuariCodigo, p.Id.AgeUsuariAgeLicencCodigo, p.Id.Codigo },
                                             "Id");

            List<AgeUsuariosPerfiles> ageUsuariosPerfileList = new();
            AgeUsuariosPerfiles ageUsuarioPerfile;

            foreach (AgeUsuariosPerfileSaveDAO ageUsuariosPerfileSaveDAO in AgeUsuariosPerfileSaveDAOList)
            {
                ageUsuarioPerfile = ValidarUpdate(_httpContextAccessor, ageUsuariosPerfileSaveDAO);
                ageUsuariosPerfileList.Add(ageUsuarioPerfile);
            }

            return _repository.ActualizarVarios(ageUsuariosPerfileList);
        }

        private Resource<AgeUsuariosPerfileDAO> GetDataWithResource(IHttpContextAccessor _httpContextAccessor, AgeUsuariosPerfileDAO ageUsuariosPerfileDAO)
        {
            string rutaSegmentoFinal = $"{ageUsuariosPerfileDAO.Id.AgeUsuariAgeLicencCodigo}/{ageUsuariosPerfileDAO.Id.AgeUsuariCodigo}/{ageUsuariosPerfileDAO.Id.Codigo}";

            return Resource<AgeUsuariosPerfileDAO>.GetDataWithResource<AgeUsuariosPerfilesController>(
                _httpContextAccessor, rutaSegmentoFinal, ageUsuariosPerfileDAO);
        }

        private AgeUsuariosPerfiles ValidarInsert(IHttpContextAccessor _httpContextAccessor, AgeUsuariosPerfileSaveDAO ageUsuariosPerfileSaveDAO)
        {
            if (ageUsuariosPerfileSaveDAO == null) throw new InvalidSintaxisException();

            Validator.ValidateObject(ageUsuariosPerfileSaveDAO, new ValidationContext(ageUsuariosPerfileSaveDAO), true);

            if (ageUsuariosPerfileSaveDAO.FechaDesde == default)
                throw new InvalidFieldException("La fecha desde no puede ser nula.");

            FuncionesSecuencias fg = new(_ageLicenciatariosAplicaSecuService);

            ageUsuariosPerfileSaveDAO.Id.Codigo = fg.ObtenerSecuencia(_httpContextAccessor, 
                ageUsuariosPerfileSaveDAO.Id.AgeUsuariAgeLicencCodigo, 
                Globales.CODIGO_SECUENCIA_USUARIO_PERFIL,
                ageUsuariosPerfileSaveDAO.UsuarioIngreso);

            ValidarKeys(ageUsuariosPerfileSaveDAO);

            AgeUsuariosPerfiles entityObject = FromDAOToEntity(_httpContextAccessor, ageUsuariosPerfileSaveDAO);

            Validator.ValidateObject(entityObject, new ValidationContext(entityObject), true);

            return entityObject;
        }

        private AgeUsuariosPerfiles ValidarUpdate(IHttpContextAccessor _httpContextAccessor, AgeUsuariosPerfileSaveDAO ageUsuariosPerfileSaveDAO)
        {
            if (ageUsuariosPerfileSaveDAO == null)
                throw new InvalidSintaxisException();

            if (ageUsuariosPerfileSaveDAO.UsuarioModificacion == null || ageUsuariosPerfileSaveDAO.UsuarioModificacion <= 0)
                throw new InvalidFieldException("El usuario de modificación debe ser un número entero mayor a cero.");

            // Validar PK
            AgeUsuariosPerfiles? ageUsuarioPerfile = ConsultarCompletePorId(ageUsuariosPerfileSaveDAO.Id) 
                ?? throw new RegisterNotFoundException("Usuario Perfil con código: " + ageUsuariosPerfileSaveDAO.Id.Codigo +
                                            ", código licenciatario: " + ageUsuariosPerfileSaveDAO.Id.AgeUsuariAgeLicencCodigo +
                                            " y código de usuario: " + ageUsuariosPerfileSaveDAO.Id.AgeUsuariCodigo + " no existe.");

            if (ageUsuariosPerfileSaveDAO.AgePerfilCodigo > 0 || ageUsuariosPerfileSaveDAO.AgePerfilCodigo > 0)
                ValidarFKPerfil(ageUsuariosPerfileSaveDAO);

            // Modificar campos
            ageUsuarioPerfile.Estado = !string.IsNullOrWhiteSpace(ageUsuariosPerfileSaveDAO.Estado) ? ageUsuariosPerfileSaveDAO.Estado : ageUsuarioPerfile.Estado;
            ageUsuarioPerfile.FechaEstado = !string.IsNullOrWhiteSpace(ageUsuariosPerfileSaveDAO.Estado) ? DateTime.Now : ageUsuarioPerfile.FechaEstado;
            ageUsuarioPerfile.ObservacionEstado = !string.IsNullOrWhiteSpace(ageUsuariosPerfileSaveDAO.ObservacionEstado) ? ageUsuariosPerfileSaveDAO.ObservacionEstado : ageUsuarioPerfile.ObservacionEstado;
            
            ageUsuarioPerfile.AgePerfilAgeLicencCodigo = ageUsuariosPerfileSaveDAO.AgePerfilAgeLicencCodigo > 0 ? ageUsuariosPerfileSaveDAO.AgePerfilAgeLicencCodigo : ageUsuarioPerfile.AgePerfilAgeLicencCodigo;
            ageUsuarioPerfile.AgePerfilCodigo = ageUsuariosPerfileSaveDAO.AgePerfilCodigo > 0 ? ageUsuariosPerfileSaveDAO.AgePerfilCodigo : ageUsuarioPerfile.AgePerfilCodigo;
            ageUsuarioPerfile.FechaDesde = ageUsuariosPerfileSaveDAO.FechaDesde != default ? ageUsuariosPerfileSaveDAO.FechaDesde : ageUsuarioPerfile.FechaDesde;
            ageUsuarioPerfile.FechaHasta = ageUsuariosPerfileSaveDAO.FechaHasta != null && ageUsuariosPerfileSaveDAO.FechaHasta != default ? ageUsuariosPerfileSaveDAO.FechaHasta : ageUsuarioPerfile.FechaHasta;

            ageUsuarioPerfile.FechaModificacion = DateTime.Now;
            ageUsuarioPerfile.UbicacionModificacion = Ubicacion.getIpAdress(_httpContextAccessor);
            ageUsuarioPerfile.UsuarioModificacion = ageUsuariosPerfileSaveDAO.UsuarioModificacion;

            Validator.ValidateObject(ageUsuarioPerfile, new ValidationContext(ageUsuarioPerfile), true);

            return ageUsuarioPerfile;
        }

        private AgeUsuariosPerfiles FromDAOToEntity(
            IHttpContextAccessor _httpContextAccessor,
            AgeUsuariosPerfileSaveDAO ageUsuariosPerfileSaveDAO)
        {

            return new AgeUsuariosPerfiles()
            {
                AgeUsuariAgeLicencCodigo = ageUsuariosPerfileSaveDAO.Id.AgeUsuariAgeLicencCodigo,
                Codigo = ageUsuariosPerfileSaveDAO.Id.Codigo,
                AgeUsuariCodigo = ageUsuariosPerfileSaveDAO.Id.AgeUsuariCodigo,
                AgePerfilAgeLicencCodigo = ageUsuariosPerfileSaveDAO.AgePerfilAgeLicencCodigo,
                AgePerfilCodigo = ageUsuariosPerfileSaveDAO.AgePerfilCodigo,
                FechaDesde = ageUsuariosPerfileSaveDAO.FechaDesde,
                FechaHasta = ageUsuariosPerfileSaveDAO.FechaHasta == default
                                        ? null : ageUsuariosPerfileSaveDAO.FechaHasta,
                Estado = ageUsuariosPerfileSaveDAO.Estado,
                FechaEstado = DateTime.Now,
                FechaIngreso = DateTime.Now,
                ObservacionEstado = ageUsuariosPerfileSaveDAO.ObservacionEstado,
                UbicacionIngreso = Ubicacion.getIpAdress(_httpContextAccessor),
                UsuarioIngreso = ageUsuariosPerfileSaveDAO.UsuarioIngreso
            };
        }

        private void ValidarKeys(AgeUsuariosPerfileSaveDAO ageUsuariosPerfileSaveDAO)
        {
            ValidarPK(ageUsuariosPerfileSaveDAO.Id);

            ValidarFKUsuario(ageUsuariosPerfileSaveDAO.Id);

            ValidarFKPerfil(ageUsuariosPerfileSaveDAO);
        }

        private void ValidarPK(AgeUsuariosPerfilePKDAO ageUsuariosPerfilePKDAO)
        {
            ValidateKeys.ValidarNoExistenciaKey(ageUsuariosPerfilePKDAO,
              "Usuario Perfil con código: " + ageUsuariosPerfilePKDAO.Codigo +
                ", código licenciatario: " + ageUsuariosPerfilePKDAO.AgeUsuariAgeLicencCodigo +
                " y código de usuario: " + ageUsuariosPerfilePKDAO.AgeUsuariCodigo + " no existe.",
               _repository.ConsultarCompletePorId);
        }

        private void ValidarFKUsuario(AgeUsuariosPerfilePKDAO ageUsuariosPerfilePKDAO)
        {
            ValidateKeys.ValidarExistenciaKey(new AgeUsuarioPKDAO()
            {
                ageLicencCodigo = ageUsuariosPerfilePKDAO.AgeUsuariAgeLicencCodigo,
                codigo = ageUsuariosPerfilePKDAO.AgeUsuariCodigo
            },
            $"Usuario con código: {ageUsuariosPerfilePKDAO.AgeUsuariCodigo} " +
            $"y código licenciatario: {ageUsuariosPerfilePKDAO.AgeUsuariAgeLicencCodigo} no existe."
            , _repositoryUsuari.ConsultarCompletePorId);
        }

        private void ValidarFKPerfil(AgeUsuariosPerfileSaveDAO ageUsuariosPerfileSaveDAO)
        {
            if (ageUsuariosPerfileSaveDAO.AgePerfilCodigo > 0 || ageUsuariosPerfileSaveDAO.AgePerfilCodigo > 0)
                ValidateKeys.ValidarExistenciaKey(new AgePerfilesPKDAO
                {
                    ageLicencCodigo = ageUsuariosPerfileSaveDAO.AgePerfilAgeLicencCodigo,
                    codigo = ageUsuariosPerfileSaveDAO.AgePerfilCodigo
                },
                $"Perfil con código: {ageUsuariosPerfileSaveDAO.AgePerfilCodigo} " +
                $"y código licenciatario: {ageUsuariosPerfileSaveDAO.AgePerfilAgeLicencCodigo} no existe."
                , _repositoryPerfile.ConsultarCompletePorId);
        }
    }
}
