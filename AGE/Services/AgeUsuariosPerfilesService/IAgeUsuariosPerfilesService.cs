﻿
using AGE.Entities.DAO.AgeUsuariosPerfiles;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;

namespace AGE.Services.AgeUsuariosPerfileService
{
    public interface IAgeUsuariosPerfilesService
    {
        Task<Page<AgeUsuariosPerfileDAO>> ConsultarTodos(int codigoLicenciatario,
                                                    Pageable pageable);

        Task<AgeUsuariosPerfileDAO> ConsultarPorId(AgeUsuariosPerfilePKDAO ageUsuariosPerfilePKDAO);

        Task<Page<AgeUsuariosPerfileDAO>> ConsultarListaFiltro(int codigoLicenciatario,
                                                                string filtro,
                                                                Pageable pageable);

        Task<Resource<AgeUsuariosPerfileDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgeUsuariosPerfileSaveDAO AgeUsuariosPerfileSaveDAO);

        Task<List<Resource<AgeUsuariosPerfileDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeUsuariosPerfileSaveDAO> AgeUsuariosPerfileSaveDAOList);

        Task<AgeUsuariosPerfileDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeUsuariosPerfileSaveDAO AgeUsuariosPerfileSaveDAO);

        Task<List<AgeUsuariosPerfileDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeUsuariosPerfileSaveDAO> AgeUsuariosPerfileSaveDAOList);
    }
}
