﻿using AGE.Entities.DAO.AgeTransaccionesIdioma;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;

namespace AGE.Services.AgeTransaccionesIdiomaService
{
    public interface IAgeTransaccionesIdiomasService
    {
        Task<Page<AgeTransaccionesIdiomasDAO>> ConsultarTodos(
            Pageable pageable);

        Task<Page<AgeTransaccionesIdiomasDAO>> ConsultarListaFiltro(
            string filtro, 
            Pageable pageable);

        Task<AgeTransaccionesIdiomasDAO> ConsultarPorId(
            AgeTransaccionesIdiomasPKDAO ageTransaccionesIdiomaPKDAO);

        Task<Resource<AgeTransaccionesIdiomasDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor, 
            AgeTransaccionesIdiomasSaveDAO ageTransaccionesIdiomaSaveDAO);

        Task<List<Resource<AgeTransaccionesIdiomasDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor, 
            List<AgeTransaccionesIdiomasSaveDAO> ageTransaccionesIdiomaSaveDAOList);

        Task<AgeTransaccionesIdiomasDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor, 
            AgeTransaccionesIdiomasSaveDAO ageTransaccionesIdiomaSaveDAO);

        Task<List<AgeTransaccionesIdiomasDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor, 
            List<AgeTransaccionesIdiomasSaveDAO> ageTransaccionesIdiomaSaveDAOList);
    }
}
