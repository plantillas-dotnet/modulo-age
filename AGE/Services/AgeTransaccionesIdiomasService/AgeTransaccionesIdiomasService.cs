﻿using AGE.Controllers;
using AGE.Entities;
using AGE.Entities.DAO.AgeTransacciones;
using AGE.Entities.DAO.AgeTransaccionesIdioma;
using AGE.Middleware.Exceptions.BadRequest;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Repositories.AgeIdiomaRepository;
using AGE.Repositories.AgeTransaccioneRepository;
using AGE.Repositories.AgeTransaccionesIdiomaRepository;
using AGE.Utils;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using System.ComponentModel.DataAnnotations;
using System.Transactions;

namespace AGE.Services.AgeTransaccionesIdiomaService
{
    public class AgeTransaccionesIdiomasService : IAgeTransaccionesIdiomasService
    {
        private readonly IAgeTransaccionesIdiomasRepository _repository;
        private readonly IAgeTransaccionesRepository _repositoryTransacciones;
        private readonly IAgeIdiomasRepository _repositoryIdiomas;

        public AgeTransaccionesIdiomasService(
            IAgeTransaccionesIdiomasRepository Repository,
            IAgeTransaccionesRepository repositoryTransacciones,
            IAgeIdiomasRepository repositoryIdiomas)
        {
            _repository = Repository;
            _repositoryTransacciones = repositoryTransacciones;
            _repositoryIdiomas = repositoryIdiomas;
        }


        public Task<Page<AgeTransaccionesIdiomasDAO>> ConsultarTodos(
            Pageable pageable)
        {

            pageable.Validar<AgeTransaccionesIdiomasDAO>();

            return _repository.ConsultarTodos(pageable);
        }


        public Task<Page<AgeTransaccionesIdiomasDAO>> ConsultarListaFiltro(
            string filtro,
            Pageable pageable)
        {

            if (string.IsNullOrWhiteSpace(filtro))
                throw new InvalidFieldException("El filtro es requerido.");

            pageable.Validar<AgeTransaccionesIdiomasDAO>();

            return _repository.ConsultarListaFiltro(filtro.ToLower(), pageable);
        }

        public Task<AgeTransaccionesIdiomasDAO> ConsultarPorId(
            AgeTransaccionesIdiomasPKDAO ageTransaccionesIdiomaPKDAO)
        {
            Validator.ValidateObject(ageTransaccionesIdiomaPKDAO, new ValidationContext(ageTransaccionesIdiomaPKDAO), true);

            AgeTransaccionesIdiomasDAO? ageTransaccionesIdiomaDAO = _repository.ConsultarPorId(ageTransaccionesIdiomaPKDAO).Result;

            if (ageTransaccionesIdiomaDAO == null)
                throw new RegisterNotFoundException("Transacción con código " + ageTransaccionesIdiomaPKDAO.ageTransaCodigo + " no existe");
       
            return Task.FromResult(ageTransaccionesIdiomaDAO);
        }

        private AgeTransaccionesIdiomas ConsultarCompletePorId(
            AgeTransaccionesIdiomasPKDAO ageTransaccionesIdiomaPKDAO)
        {
            Validator.ValidateObject(ageTransaccionesIdiomaPKDAO, new ValidationContext(ageTransaccionesIdiomaPKDAO), true);

            AgeTransaccionesIdiomas? ageTransaccionesIdioma = _repository.ConsultarCompletePorId(ageTransaccionesIdiomaPKDAO).Result;

            return ageTransaccionesIdioma;
        }

        public async Task<Resource<AgeTransaccionesIdiomasDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgeTransaccionesIdiomasSaveDAO ageTransaccionesIdiomaSaveDAO)
        {
            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    AgeTransaccionesIdiomas ageTransaccionesIdioma = ValidarInsert(_httpContextAccessor, ageTransaccionesIdiomaSaveDAO);

                    AgeTransaccionesIdiomasDAO ageTransaccionesIdiomaDAO = await _repository.Insertar(ageTransaccionesIdioma);

                    Resource<AgeTransaccionesIdiomasDAO> ageTransaccionesIdiomaDAOWithResource = GetDataWithResource(_httpContextAccessor, ageTransaccionesIdiomaDAO);

                    transactionScope.Complete();

                    return ageTransaccionesIdiomaDAOWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public async Task<List<Resource<AgeTransaccionesIdiomasDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeTransaccionesIdiomasSaveDAO> ageTransaccionesIdiomaSaveDAOList)
        {
            if (ageTransaccionesIdiomaSaveDAOList == null)
            {
                throw new InvalidSintaxisException();
            }

            List<AgeTransaccionesIdiomas> ageTransaccionesIdiomaList = new List<AgeTransaccionesIdiomas>();

            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    foreach (AgeTransaccionesIdiomasSaveDAO ageTransaccionesIdiomaSaveDAO in ageTransaccionesIdiomaSaveDAOList)
                    {
                        AgeTransaccionesIdiomas ageTransaccionesIdioma = ValidarInsert(_httpContextAccessor, ageTransaccionesIdiomaSaveDAO);
                        ageTransaccionesIdiomaList.Add(ageTransaccionesIdioma);
                    }

                    List<AgeTransaccionesIdiomasDAO> ageTransaccionesIdiomaDAOList = await _repository.InsertarVarios(ageTransaccionesIdiomaList);

                    List<Resource<AgeTransaccionesIdiomasDAO>> ageTransaccionesIdiomaDAOListWithResource = new List<Resource<AgeTransaccionesIdiomasDAO>>();

                    foreach (AgeTransaccionesIdiomasDAO ageTransaccionesIdiomaDAO in ageTransaccionesIdiomaDAOList)
                    {
                        ageTransaccionesIdiomaDAOListWithResource.Add(GetDataWithResource(_httpContextAccessor, ageTransaccionesIdiomaDAO));
                    }

                    transactionScope.Complete();

                    return ageTransaccionesIdiomaDAOListWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public Task<AgeTransaccionesIdiomasDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeTransaccionesIdiomasSaveDAO ageTransaccionesIdiomaSaveDAO)
        {
            AgeTransaccionesIdiomas ageTransaccionesIdioma = ValidarUpdate(_httpContextAccessor, ageTransaccionesIdiomaSaveDAO);

            return _repository.Actualizar(ageTransaccionesIdioma);
        }

        public Task<List<AgeTransaccionesIdiomasDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeTransaccionesIdiomasSaveDAO> ageTransaccionesIdiomaSaveDAOList)
        {
            if (ageTransaccionesIdiomaSaveDAOList == null)
                throw new InvalidSintaxisException();

            ValidateKeys.ValidarPKDuplicadas(ageTransaccionesIdiomaSaveDAOList,
                                             p => new { p.Id.ageIdiomaCodigo, p.Id.ageTransaAgeAplicaCodigo, p.Id.ageTransaCodigo},
                                             "Id");

            List<AgeTransaccionesIdiomas> ageTransaccionesIdiomaList = new List<AgeTransaccionesIdiomas>();
            AgeTransaccionesIdiomas ageTransaccionesIdioma;

            foreach (AgeTransaccionesIdiomasSaveDAO ageTransaccionesIdiomaSaveDAO in ageTransaccionesIdiomaSaveDAOList)
            {
                ageTransaccionesIdioma = ValidarUpdate(_httpContextAccessor, ageTransaccionesIdiomaSaveDAO);
                ageTransaccionesIdiomaList.Add(ageTransaccionesIdioma);
            }

            return _repository.ActualizarVarios(ageTransaccionesIdiomaList);
        }

        private static Resource<AgeTransaccionesIdiomasDAO> GetDataWithResource(
            IHttpContextAccessor _httpContextAccessor,
            AgeTransaccionesIdiomasDAO ageTransaccionesIdiomaDAO)
        {
            string idtipoSegmentoFinal = $"{ageTransaccionesIdiomaDAO.Id.ageTransaCodigo}/" +
                $"{ageTransaccionesIdiomaDAO.Id.ageTransaAgeAplicaCodigo}/" +
                $"{ageTransaccionesIdiomaDAO.Id.ageIdiomaCodigo}";

            return Resource<AgeTransaccionesIdiomasDAO>.GetDataWithResource<AgeTransaccionesIdiomasController>(
                _httpContextAccessor, idtipoSegmentoFinal, ageTransaccionesIdiomaDAO);
        }

        private AgeTransaccionesIdiomas ValidarInsert(
            IHttpContextAccessor _httpContextAccessor,
            AgeTransaccionesIdiomasSaveDAO ageTransaccionesIdiomaSaveDAO)
        {

            if (ageTransaccionesIdiomaSaveDAO == null)
                throw new InvalidSintaxisException();

            Validator.ValidateObject(ageTransaccionesIdiomaSaveDAO, new ValidationContext(ageTransaccionesIdiomaSaveDAO), true);

            ValidarKeys(ageTransaccionesIdiomaSaveDAO);

            AgeTransaccionesIdiomas entityObject = FromDAOToEntity(_httpContextAccessor, ageTransaccionesIdiomaSaveDAO);

            Validator.ValidateObject(entityObject, new ValidationContext(entityObject), true);

            return entityObject;
        }


        private AgeTransaccionesIdiomas ValidarUpdate(
            IHttpContextAccessor _httpContextAccessor,
            AgeTransaccionesIdiomasSaveDAO ageTransaccionesIdiomaSaveDAO)
        {
            if (ageTransaccionesIdiomaSaveDAO == null)
                throw new InvalidSintaxisException();

            if (ageTransaccionesIdiomaSaveDAO.UsuarioModificacion == null || ageTransaccionesIdiomaSaveDAO.UsuarioModificacion <= 0)
                throw new InvalidFieldException("El usuario de modificación debe ser un número entero mayor a cero.");

            AgeTransaccionesIdiomas? ageTransaccionesIdioma = ConsultarCompletePorId(new AgeTransaccionesIdiomasPKDAO
            {
                ageIdiomaCodigo = ageTransaccionesIdiomaSaveDAO.Id.ageIdiomaCodigo,
                ageTransaCodigo = ageTransaccionesIdiomaSaveDAO.Id.ageTransaCodigo,
                ageTransaAgeAplicaCodigo = ageTransaccionesIdiomaSaveDAO.Id.ageTransaAgeAplicaCodigo
            });

            if (ageTransaccionesIdioma == null)
                throw new RegisterNotFoundException("Transacción con código idioma " + ageTransaccionesIdiomaSaveDAO.Id.ageIdiomaCodigo + " no existe");

            if (!string.IsNullOrWhiteSpace(ageTransaccionesIdiomaSaveDAO.Estado))
            {
                ageTransaccionesIdioma.Estado = ageTransaccionesIdiomaSaveDAO.Estado;
                ageTransaccionesIdioma.FechaEstado = DateTime.Now;
            }

            if (!string.IsNullOrWhiteSpace(ageTransaccionesIdiomaSaveDAO.Descripcion))
                ageTransaccionesIdioma.Descripcion = ageTransaccionesIdiomaSaveDAO.Descripcion;

            if (!string.IsNullOrWhiteSpace(ageTransaccionesIdiomaSaveDAO.ObservacionEstado))
                ageTransaccionesIdioma.ObservacionEstado = ageTransaccionesIdiomaSaveDAO.ObservacionEstado;

            ageTransaccionesIdioma.FechaModificacion = DateTime.Now;
            ageTransaccionesIdioma.UbicacionModificacion = Ubicacion.getIpAdress(_httpContextAccessor);
            ageTransaccionesIdioma.UsuarioModificacion = ageTransaccionesIdiomaSaveDAO.UsuarioModificacion;

            Validator.ValidateObject(ageTransaccionesIdioma, new ValidationContext(ageTransaccionesIdioma), true);

            return ageTransaccionesIdioma;
        }


        private void ValidarKeys(AgeTransaccionesIdiomasSaveDAO ageTransaccionesIdiomaSaveDAO)
        {
            ValidarPK(ageTransaccionesIdiomaSaveDAO.Id);

            ValidarFKTransacciones(ageTransaccionesIdiomaSaveDAO.Id.ageTransaAgeAplicaCodigo, 
                                   ageTransaccionesIdiomaSaveDAO.Id.ageTransaCodigo);

            ValidarFKIdiomas(ageTransaccionesIdiomaSaveDAO.Id.ageIdiomaCodigo);

        }


        private void ValidarPK(AgeTransaccionesIdiomasPKDAO ageTransaccionesIdiomaPKDAO)
        {

            ValidateKeys.ValidarNoExistenciaKey(
               ageTransaccionesIdiomaPKDAO,
               $"Transacción con código de idioma {ageTransaccionesIdiomaPKDAO.ageIdiomaCodigo} ya existe.",
               _repository.ConsultarCompletePorId);
        }


        private void ValidarFKTransacciones(int codigoAplicacion, int codigo)
        {
            AgeTransaccionePKDAO ageTransaccionePKDAO = new AgeTransaccionePKDAO
            {
                ageAplicaCodigo = codigoAplicacion,
                codigo = codigo
            };

            ValidateKeys.ValidarExistenciaKey(
                ageTransaccionePKDAO,
                $"Transacción con código {codigo} no existe.",
                _repositoryTransacciones.ConsultarCompletePorId);
        }


        private void ValidarFKIdiomas(int codigoIdioma)
        {

            ValidateKeys.ValidarExistenciaKey(
                codigoIdioma,
                $"Idioma con código {codigoIdioma} no existe.",
                _repositoryIdiomas.ConsultarCompletePorId);
        }


        private AgeTransaccionesIdiomas FromDAOToEntity(
            IHttpContextAccessor _httpContextAccessor,
            AgeTransaccionesIdiomasSaveDAO ageTransaccionesIdiomaSaveDAO)
        {
            return new AgeTransaccionesIdiomas
            {
                AgeIdiomaCodigo = ageTransaccionesIdiomaSaveDAO.Id.ageIdiomaCodigo,
                AgeTransaAgeAplicaCodigo = ageTransaccionesIdiomaSaveDAO.Id.ageTransaAgeAplicaCodigo,
                AgeTransaCodigo = ageTransaccionesIdiomaSaveDAO.Id.ageTransaCodigo,
                Descripcion = ageTransaccionesIdiomaSaveDAO.Descripcion,
                Estado = ageTransaccionesIdiomaSaveDAO.Estado,
                FechaEstado = DateTime.Now,
                FechaIngreso = DateTime.Now,
                ObservacionEstado = ageTransaccionesIdiomaSaveDAO.ObservacionEstado,
                UbicacionIngreso = Ubicacion.getIpAdress(_httpContextAccessor),
                UsuarioIngreso = ageTransaccionesIdiomaSaveDAO.UsuarioIngreso
            };
        }

    }

}