﻿using AGE.Controllers;
using AGE.Entities.DAO.AgeModalidades;
using AGE.Entities;
using AGE.Middleware.Exceptions.BadRequest;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Repositories.AgeModalidadesRepository;
using AGE.Services.AgeLicenciatariosAplicaSecuService;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using AGE.Utils;
using System.ComponentModel.DataAnnotations;
using System.Transactions;

namespace AGE.Services.AgeModalidadesService
{
    public class AgeModalidadService : IAgeModalidadService
    {
        private readonly IAgeModalidadRepository _repository;
        private readonly IAgeLicenciatariosAplicaSecuService _ageLicenciatariosAplicaSecuService;

        public AgeModalidadService(
            IAgeModalidadRepository Repository,
            IAgeLicenciatariosAplicaSecuService ageLicenciatariosAplicaSecuService)
        {
            _repository = Repository;
            _ageLicenciatariosAplicaSecuService = ageLicenciatariosAplicaSecuService;
        }

        public Task<Page<AgeModalidadesDAO>> ConsultarTodos(int CodigoLicenciatario,
                                                        int Codigo,
                                                        string CodigoExterno,
                                                        string descripcion,
                                                        Pageable pageable)
        {
            if (CodigoLicenciatario <= 0)
                throw new InvalidFieldException("El código de licenciatario debe ser un número entero mayor a cero.");

            pageable.Validar<AgeModalidadesDAO>();

            return _repository.ConsultarTodos(CodigoLicenciatario,Codigo,CodigoExterno,descripcion, pageable);
        }

        public Task<AgeModalidadesDAO> ConsultarPorId(AgeModalidadesPKDAO ageModalidades)
        {
            Validator.ValidateObject(ageModalidades, new ValidationContext(ageModalidades), true);

            AgeModalidadesDAO ageModalidadesDAO = _repository.ConsultarPorId(ageModalidades).Result;

            return ageModalidadesDAO == null
                ? throw new RegisterNotFoundException("Modalidad con código: " + ageModalidades.Codigo + " no existe.")
                : Task.FromResult(ageModalidadesDAO);
        }

        private AgeModalidad ConsultarCompletePorId(AgeModalidadesPKDAO ageModalidadPKDAO)
        {
            Validator.ValidateObject(ageModalidadPKDAO, new ValidationContext(ageModalidadPKDAO), true);

            AgeModalidad ageModalidad = _repository.ConsultarCompletePorId(ageModalidadPKDAO).Result;

            return ageModalidad;
        }

        public Task<Page<AgeModalidadesDAO>> ConsultarListaFiltro(int CodigoLicenciatario, string filtro, Pageable pageable)
        {
            if (CodigoLicenciatario <= 0)
                throw new InvalidFieldException("El código de licenciatario debe ser un número entero mayor a cero.");

            if (string.IsNullOrWhiteSpace(filtro))
                throw new InvalidFieldException("El filtro es requerido.");

            pageable.Validar<AgeModalidadesDAO>();

            return _repository.ConsultarListaFiltro(CodigoLicenciatario, filtro, pageable);
        }

        public async Task<Resource<AgeModalidadesDAO>> Insertar(IHttpContextAccessor _httpContextAccessor,
                                                    AgeModalidadesSaveDAO AgeModalidadesSaveDAO)
        { 
            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    AgeModalidad ageModalidades = ValidarInsert(_httpContextAccessor, AgeModalidadesSaveDAO);
                    AgeModalidadesDAO ageModalidadesDAO = await _repository.Insertar(ageModalidades);
                    
                    Resource<AgeModalidadesDAO> ageModalidadesDAOWithResource = GetDataWithResource(_httpContextAccessor, ageModalidadesDAO);

                    transactionScope.Complete();

                    return ageModalidadesDAOWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }

        }

        public async Task<List<Resource<AgeModalidadesDAO>>> InsertarVarios(IHttpContextAccessor _httpContextAccessor,
                                                                List<AgeModalidadesSaveDAO> AgeModalidadesSaveDAOList)
        {
            if (AgeModalidadesSaveDAOList == null)
                throw new InvalidSintaxisException();

            List<AgeModalidad> ageModalidadesList = new();

            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    foreach (AgeModalidadesSaveDAO ageModalidadesSaveDAO in AgeModalidadesSaveDAOList)
                    {
                        AgeModalidad ageModalidades = ValidarInsert(_httpContextAccessor, ageModalidadesSaveDAO);
                        ageModalidadesList.Add(ageModalidades);
                    }

                    List<AgeModalidadesDAO> ageModalidadesDAOList = await _repository.InsertarVarios(ageModalidadesList);

                    List<Resource<AgeModalidadesDAO>> ageModalidadesDAOListWithResource = new();

                    foreach (AgeModalidadesDAO ageModalidadesDAO in ageModalidadesDAOList)
                    {
                        ageModalidadesDAOListWithResource.Add(GetDataWithResource(_httpContextAccessor, ageModalidadesDAO));
                    }

                    transactionScope.Complete();

                    return ageModalidadesDAOListWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public Task<AgeModalidadesDAO> Actualizar(IHttpContextAccessor _httpContextAccessor,
            AgeModalidadesSaveDAO AgeModalidadesSaveDAO)
        {
            AgeModalidad ageModalidades = ValidarUpdate(_httpContextAccessor, AgeModalidadesSaveDAO);

            return _repository.Actualizar(ageModalidades);
        }

        public Task<List<AgeModalidadesDAO>> ActualizarVarios(IHttpContextAccessor _httpContextAccessor,
                                                                List<AgeModalidadesSaveDAO> AgeModalidadesSaveDAOList)
        {
            if (AgeModalidadesSaveDAOList == null)
                throw new InvalidSintaxisException();
            ValidateKeys.ValidarPKDuplicadas(AgeModalidadesSaveDAOList,
                                             p => new { p.Id.AgeLicencCodigo, p.Id.Codigo },
                                             "Id");

            List<AgeModalidad> ageModalidadesList = new();
            AgeModalidad ageModalidades;

            foreach (AgeModalidadesSaveDAO ageModalidadesSave in AgeModalidadesSaveDAOList)
            {
                ageModalidades = ValidarUpdate(_httpContextAccessor, ageModalidadesSave);
                ageModalidadesList.Add(ageModalidades);
            }

            return _repository.ActualizarVarios(ageModalidadesList);
        }

        private static Resource<AgeModalidadesDAO> GetDataWithResource(
            IHttpContextAccessor _httpContextAccessor,
            AgeModalidadesDAO ageModalidadesDAO)
        {
            string rutaSegmentoFinal = $"{ageModalidadesDAO.Id.AgeLicencCodigo}/{ageModalidadesDAO.Id.Codigo}";

            return Resource<AgeModalidadesDAO>.GetDataWithResource<AgeModalidadController>(
                _httpContextAccessor, rutaSegmentoFinal, ageModalidadesDAO);
        }

        private AgeModalidad ValidarInsert(
            IHttpContextAccessor _httpContextAccessor,
            AgeModalidadesSaveDAO ageModalidadesSaveDAO)
        {
            if (ageModalidadesSaveDAO == null) throw new InvalidSintaxisException();

            Validator.ValidateObject(ageModalidadesSaveDAO, new ValidationContext(ageModalidadesSaveDAO), true);

            FuncionesSecuencias fg = new(_ageLicenciatariosAplicaSecuService);

            ageModalidadesSaveDAO.Id.Codigo = fg.ObtenerSecuencia(_httpContextAccessor,
                                                ageModalidadesSaveDAO.Id.AgeLicencCodigo,
                                                Globales.CODIGO_SECUENCIA_MODALIDAD,
                                                ageModalidadesSaveDAO.UsuarioIngreso);

            ValidarPK(ageModalidadesSaveDAO.Id);

            AgeModalidad entityObject = FromDAOToEntity(_httpContextAccessor,ageModalidadesSaveDAO);

            Validator.ValidateObject(entityObject, new ValidationContext(entityObject), true);

            return entityObject;
        }

        private AgeModalidad ValidarUpdate(IHttpContextAccessor httpContextAccessor, AgeModalidadesSaveDAO ageModalidadesSaveDAO)
        {
            if (ageModalidadesSaveDAO == null)
                throw new InvalidSintaxisException();

            if (ageModalidadesSaveDAO.UsuarioModificacion == null || ageModalidadesSaveDAO.UsuarioModificacion <= 0)
                throw new InvalidFieldException("El usuario de modificación debe ser un número entero mayor a cero.");

            AgeModalidad ageModalidades = ConsultarCompletePorId(ageModalidadesSaveDAO.Id) 
                ?? throw new RegisterNotFoundException("Modalidad con código: " + ageModalidadesSaveDAO.Id.Codigo + " no existe.");
            
            ageModalidades.Estado = !string.IsNullOrWhiteSpace(ageModalidadesSaveDAO.Estado) ? ageModalidadesSaveDAO.Estado : ageModalidades.Estado;
            ageModalidades.FechaEstado = !string.IsNullOrWhiteSpace(ageModalidadesSaveDAO.Estado) ? DateTime.Now : ageModalidades.FechaEstado;
            ageModalidades.Descripcion = !string.IsNullOrWhiteSpace(ageModalidadesSaveDAO.Descripcion) ? ageModalidadesSaveDAO.Descripcion : ageModalidades.Descripcion;
            ageModalidades.ObservacionEstado = !string.IsNullOrWhiteSpace(ageModalidadesSaveDAO.ObservacionEstado) ? ageModalidadesSaveDAO.ObservacionEstado : ageModalidades.ObservacionEstado;

            ageModalidades.CodigoExterno = !string.IsNullOrWhiteSpace(ageModalidadesSaveDAO.codigoExterno) ? ageModalidadesSaveDAO.codigoExterno : ageModalidades.CodigoExterno;
            ageModalidades.FechaModificacion = DateTime.Now;
            ageModalidades.UbicacionModificacion = Ubicacion.getIpAdress(httpContextAccessor);
            ageModalidades.UsuarioModificacion = ageModalidadesSaveDAO.UsuarioModificacion;

            Validator.ValidateObject(ageModalidades, new ValidationContext(ageModalidades), true);
            return ageModalidades;
        }

        private AgeModalidad FromDAOToEntity(
            IHttpContextAccessor _httpContextAccessor,
            AgeModalidadesSaveDAO ageModalidadesSaveDAO)
        {
            return new AgeModalidad()
            {
                AgeLicencCodigo = ageModalidadesSaveDAO.Id.AgeLicencCodigo,
                Codigo = ageModalidadesSaveDAO.Id.Codigo,
                Descripcion = ageModalidadesSaveDAO.Descripcion,
                CodigoExterno = ageModalidadesSaveDAO.codigoExterno,
                Estado = ageModalidadesSaveDAO.Estado,
                FechaEstado = DateTime.Now,
                FechaIngreso = DateTime.Now,
                ObservacionEstado = ageModalidadesSaveDAO.ObservacionEstado,
                UbicacionIngreso = Ubicacion.getIpAdress(_httpContextAccessor),
                UsuarioIngreso = ageModalidadesSaveDAO.UsuarioIngreso
            };
        }   

        private void ValidarPK(AgeModalidadesPKDAO ageModalidadesPKDAO)
        {
            ValidateKeys.ValidarNoExistenciaKey(ageModalidadesPKDAO, 
                $"Modalidad con código: {ageModalidadesPKDAO.Codigo} " +
                $"y código licenciatario: {ageModalidadesPKDAO.AgeLicencCodigo} ya existe."
                , _repository.ConsultarCompletePorId);
        }
    }
}
