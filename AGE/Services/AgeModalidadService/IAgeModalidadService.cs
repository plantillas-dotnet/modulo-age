﻿using AGE.Entities.DAO.AgeModalidades;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;

namespace AGE.Services.AgeModalidadesService
{
    public interface IAgeModalidadService
    {
        Task<Page<AgeModalidadesDAO>> ConsultarTodos(int codigoLicenciatario,
                                                    int Codigo,
                                                    string codigoExterno,
                                                    string descripcion,
                                                    Pageable pageable);

        Task<AgeModalidadesDAO> ConsultarPorId(AgeModalidadesPKDAO ageModalidadesPKDAO);

        Task<Page<AgeModalidadesDAO>> ConsultarListaFiltro(int codigoLicenciatario,
                                                                string filtro,
                                                                Pageable pageable);

        Task<Resource<AgeModalidadesDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgeModalidadesSaveDAO AgeModalidadesSaveDAO);

        Task<List<Resource<AgeModalidadesDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeModalidadesSaveDAO> AgeModalidadesSaveDAOList);

        Task<AgeModalidadesDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeModalidadesSaveDAO AgeModalidadesSaveDAO);

        Task<List<AgeModalidadesDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeModalidadesSaveDAO> AgeModalidadesSaveDAOList);
    }
}
