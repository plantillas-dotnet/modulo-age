﻿using AGE.Controllers;
using AGE.Entities;
using AGE.Entities.DAO.AgeTiposIdentificaciones;
using AGE.Middleware.Exceptions.BadRequest;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Repositories.AgeTiposIdentificacioneRepository;
using AGE.Utils;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using System.ComponentModel.DataAnnotations;
using AGE.Services.AgeSecuenciasPrimariasService;
using System.Transactions;

namespace AGE.Services.AgeTiposIdentificacioneService
{
    public class AgeTiposIdentificacionesService : IAgeTiposIdentificacionesService
    {
        private readonly IAgeTiposIdentificacionesRepository _repository;
        private readonly IAgeSecuenciasPrimariasService _ageSecuenciasPrimariasService;

        public AgeTiposIdentificacionesService(
            IAgeTiposIdentificacionesRepository Repository,
            IAgeSecuenciasPrimariasService ageSecuenciasPrimariasService)
        {
            _repository = Repository;
            _ageSecuenciasPrimariasService = ageSecuenciasPrimariasService;
        }


        public Task<Page<AgeTiposIdentificacioneDAO>> ConsultarTodos(
            int codigo,
            int codigoInstitucionControl, 
            string siglas,
            string descripcion,
            Pageable pageable)
        {

            pageable.Validar<AgeTiposIdentificacioneDAO>();

            return _repository.ConsultarTodos(codigoInstitucionControl, codigo, siglas, descripcion, pageable);
        }


        public Task<Page<AgeTiposIdentificacioneDAO>> ConsultarListaFiltro(
            string filtro,
            Pageable pageable)
        {
            if (string.IsNullOrWhiteSpace(filtro))
                throw new InvalidFieldException("El filtro es requerido.");

            pageable.Validar<AgeTiposIdentificacioneDAO>();

            return _repository.ConsultarListaFiltro(filtro.ToLower(), pageable);
        }

        public Task<AgeTiposIdentificacioneDAO> ConsultarPorId(
            int codigo)
        {

            if (codigo <= 0)
                throw new InvalidIdException();

            AgeTiposIdentificacioneDAO? ageTiposIdentificacioneDAO = _repository.ConsultarPorId(codigo).Result;

            if (ageTiposIdentificacioneDAO == null)
                throw new RegisterNotFoundException("Tipo con código " + codigo + " no existe");

            return Task.FromResult(ageTiposIdentificacioneDAO);
        }

        public async Task<Resource<AgeTiposIdentificacioneDAO>> Insertar(
    IHttpContextAccessor _httpContextAccessor,
    AgeTiposIdentificacioneSaveDAO ageTiposIdentificacioneSaveDAO)
        {
            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    AgeTiposIdentificaciones ageTiposIdentificacione = ValidarInsert(_httpContextAccessor, ageTiposIdentificacioneSaveDAO);

                    AgeTiposIdentificacioneDAO ageTiposIdentificacioneDAO = await _repository.Insertar(ageTiposIdentificacione);

                    Resource<AgeTiposIdentificacioneDAO> ageTiposIdentificacioneDAOWithResource = GetDataWithResource(_httpContextAccessor, ageTiposIdentificacioneDAO);

                    transactionScope.Complete();

                    return ageTiposIdentificacioneDAOWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public async Task<List<Resource<AgeTiposIdentificacioneDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeTiposIdentificacioneSaveDAO> ageTiposIdentificacioneSaveDAOList)
        {
            if (ageTiposIdentificacioneSaveDAOList == null)
            {
                throw new InvalidSintaxisException();
            }

            List<AgeTiposIdentificaciones> ageTiposIdentificacioneList = new List<AgeTiposIdentificaciones>();

            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    foreach (AgeTiposIdentificacioneSaveDAO ageTiposIdentificacioneSaveDAO in ageTiposIdentificacioneSaveDAOList)
                    {
                        AgeTiposIdentificaciones ageTiposIdentificacione = ValidarInsert(_httpContextAccessor, ageTiposIdentificacioneSaveDAO);
                        ageTiposIdentificacioneList.Add(ageTiposIdentificacione);
                    }

                    List<AgeTiposIdentificacioneDAO> ageTiposIdentificacioneDAOList = await _repository.InsertarVarios(ageTiposIdentificacioneList);

                    List<Resource<AgeTiposIdentificacioneDAO>> ageTiposIdentificacioneDAOListWithResource = new List<Resource<AgeTiposIdentificacioneDAO>>();

                    foreach (AgeTiposIdentificacioneDAO ageTiposIdentificacioneDAO in ageTiposIdentificacioneDAOList)
                    {
                        ageTiposIdentificacioneDAOListWithResource.Add(GetDataWithResource(_httpContextAccessor, ageTiposIdentificacioneDAO));
                    }

                    transactionScope.Complete();

                    return ageTiposIdentificacioneDAOListWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public Task<AgeTiposIdentificacioneDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeTiposIdentificacioneSaveDAO ageTiposIdentificacioneSaveDAO)
        {
            AgeTiposIdentificaciones ageTiposIdentificacione = ValidarUpdate(_httpContextAccessor, ageTiposIdentificacioneSaveDAO);

            return _repository.Actualizar(ageTiposIdentificacione);
        }

        public Task<List<AgeTiposIdentificacioneDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeTiposIdentificacioneSaveDAO> ageTiposIdentificacioneSaveDAOList)
        {
            if (ageTiposIdentificacioneSaveDAOList == null)
                throw new InvalidSintaxisException();

            ValidateKeys.ValidarPKDuplicadas(ageTiposIdentificacioneSaveDAOList,
                                             p => new { p.Codigo },
                                             "Id");

            List<AgeTiposIdentificaciones> ageTiposIdentificacioneList = new List<AgeTiposIdentificaciones>();
            AgeTiposIdentificaciones ageTiposIdentificacione;

            foreach (AgeTiposIdentificacioneSaveDAO ageTiposIdentificacioneSaveDAO in ageTiposIdentificacioneSaveDAOList)
            {
                ageTiposIdentificacione = ValidarUpdate(_httpContextAccessor, ageTiposIdentificacioneSaveDAO);
                ageTiposIdentificacioneList.Add(ageTiposIdentificacione);
            }

            return _repository.ActualizarVarios(ageTiposIdentificacioneList);
        }


        private AgeTiposIdentificaciones ConsultarCompletePorId(
            int codigo)
        {

            if (codigo <= 0)
                throw new InvalidIdException();

            return _repository.ConsultarCompletePorId(codigo).Result;
        }


        private static Resource<AgeTiposIdentificacioneDAO> GetDataWithResource(
            IHttpContextAccessor _httpContextAccessor,
            AgeTiposIdentificacioneDAO ageTiposIdentificacioneDAO)
        {
            string idtipoSegmentoFinal = $"{ageTiposIdentificacioneDAO.Codigo}";

            return Resource<AgeTiposIdentificacioneDAO>.GetDataWithResource<AgeTiposIdentificacionesController>(
                _httpContextAccessor, idtipoSegmentoFinal, ageTiposIdentificacioneDAO);
        }

        private AgeTiposIdentificaciones ValidarInsert(
            IHttpContextAccessor _httpContextAccessor,
            AgeTiposIdentificacioneSaveDAO ageTiposIdentificacioneSaveDAO)
        {

            if (ageTiposIdentificacioneSaveDAO == null)
                throw new InvalidSintaxisException();

            Validator.ValidateObject(ageTiposIdentificacioneSaveDAO, new ValidationContext(ageTiposIdentificacioneSaveDAO), true);

            FuncionesSecuencias fg = new FuncionesSecuencias(_ageSecuenciasPrimariasService);

            ageTiposIdentificacioneSaveDAO.Codigo = fg.ObtenerSecuenciaPrimaria(
                                                        _httpContextAccessor, 
                                                        Globales.CODIGO_SECUENCIA_TIPO_IDENTIFICACION, 
                                                        ageTiposIdentificacioneSaveDAO.UsuarioIngreso);

            ValidarPK(ageTiposIdentificacioneSaveDAO.Codigo);

            AgeTiposIdentificaciones entityObject = FromDAOToEntity(_httpContextAccessor, ageTiposIdentificacioneSaveDAO);

            Validator.ValidateObject(entityObject, new ValidationContext(entityObject), true);

            return entityObject;
        }


        private AgeTiposIdentificaciones ValidarUpdate(
            IHttpContextAccessor _httpContextAccessor,
            AgeTiposIdentificacioneSaveDAO ageTiposIdentificacioneSaveDAO)
        {
            if (ageTiposIdentificacioneSaveDAO == null)
                throw new InvalidSintaxisException();

            if (ageTiposIdentificacioneSaveDAO.UsuarioModificacion == null || ageTiposIdentificacioneSaveDAO.UsuarioModificacion <= 0)
                throw new InvalidFieldException("El usuario de modificación debe ser un número entero mayor a cero.");

            AgeTiposIdentificaciones ageTiposIdentificacione = ConsultarCompletePorId(ageTiposIdentificacioneSaveDAO.Codigo);

            if (ageTiposIdentificacione == null)
                throw new RegisterNotFoundException("Registro con código " + ageTiposIdentificacioneSaveDAO.Codigo + " no existe");

            if (!string.IsNullOrWhiteSpace(ageTiposIdentificacioneSaveDAO.Estado))
            {
                ageTiposIdentificacione.Estado = ageTiposIdentificacioneSaveDAO.Estado;
                ageTiposIdentificacione.FechaEstado = DateTime.Now;
            }

            if (!string.IsNullOrWhiteSpace(ageTiposIdentificacioneSaveDAO.Siglas))
                ageTiposIdentificacione.Siglas = ageTiposIdentificacioneSaveDAO.Siglas;

            if (!string.IsNullOrWhiteSpace(ageTiposIdentificacioneSaveDAO.Descripcion))
                ageTiposIdentificacione.Descripcion = ageTiposIdentificacioneSaveDAO.Descripcion;

            if (!string.IsNullOrWhiteSpace(ageTiposIdentificacioneSaveDAO.ObservacionEstado))
                ageTiposIdentificacione.ObservacionEstado = ageTiposIdentificacioneSaveDAO.ObservacionEstado;

            ageTiposIdentificacione.FechaModificacion = DateTime.Now;
            ageTiposIdentificacione.UbicacionModificacion = Ubicacion.getIpAdress(_httpContextAccessor);
            ageTiposIdentificacione.UsuarioModificacion = ageTiposIdentificacioneSaveDAO.UsuarioModificacion;

            Validator.ValidateObject(ageTiposIdentificacione, new ValidationContext(ageTiposIdentificacione), true);

            return ageTiposIdentificacione;
        }


        private void ValidarPK(int codigo)
        {

            ValidateKeys.ValidarNoExistenciaKey(
               codigo,
               $"Tipo de identificación con código {codigo} ya existe.",
               _repository.ConsultarCompletePorId);
        }


        private AgeTiposIdentificaciones FromDAOToEntity(
            IHttpContextAccessor _httpContextAccessor, 
            AgeTiposIdentificacioneSaveDAO ageTiposIdentificacioneSaveDAO)
        {
            return new AgeTiposIdentificaciones
            {
                CodigoInstitucionControl = ageTiposIdentificacioneSaveDAO.CodigoInstitucionControl,
                Codigo = ageTiposIdentificacioneSaveDAO.Codigo,
                Descripcion = ageTiposIdentificacioneSaveDAO.Descripcion,
                Siglas = ageTiposIdentificacioneSaveDAO.Siglas,
                Estado = ageTiposIdentificacioneSaveDAO.Estado,
                FechaEstado = DateTime.Now,
                FechaIngreso = DateTime.Now,
                ObservacionEstado = ageTiposIdentificacioneSaveDAO.ObservacionEstado,
                UbicacionIngreso = Ubicacion.getIpAdress(_httpContextAccessor),
                UsuarioIngreso = ageTiposIdentificacioneSaveDAO.UsuarioIngreso
            };
        }


    }
}
