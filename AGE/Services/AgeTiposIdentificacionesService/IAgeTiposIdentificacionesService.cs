﻿using AGE.Entities.DAO.AgeTiposIdentificaciones;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;

namespace AGE.Services.AgeTiposIdentificacioneService
{
    public interface IAgeTiposIdentificacionesService
    {
        Task<Page<AgeTiposIdentificacioneDAO>> ConsultarTodos(
            int codigoInstitucionControl,
            int codigo,
            string siglas,
            string descripcion,
            Pageable pageable);

        Task<Page<AgeTiposIdentificacioneDAO>> ConsultarListaFiltro(
            string filtro,
            Pageable pageable);

        Task<AgeTiposIdentificacioneDAO> ConsultarPorId(int id);

        Task<Resource<AgeTiposIdentificacioneDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgeTiposIdentificacioneSaveDAO ageTiposIdentificacioneSaveDAO);

        Task<List<Resource<AgeTiposIdentificacioneDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeTiposIdentificacioneSaveDAO> ageTiposIdentificacioneSaveDAOList);

        Task<AgeTiposIdentificacioneDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeTiposIdentificacioneSaveDAO ageTiposIdentificacioneSaveDAO);

        Task<List<AgeTiposIdentificacioneDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeTiposIdentificacioneSaveDAO> ageTiposIdentificacioneSaveDAOList);
    }
}
