﻿using AGE.Entities.DAO.AgeUsuariosPuntoEmision;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;

namespace AGE.Services.AgeUsuariosPuntoEmisionService
{
    public interface IAgeUsuariosPuntoEmisionService
    {
        Task<Page<AgeUsuariosPuntoEmisionDAO>> ConsultarTodos(int AgageSucursAgeLicencCodigo,
                                                            int agePunEmCodigo,
                                                            int agePunEmAgeSucursCodigo,
                                                            long ageUsuariCodigo,
                                                            Pageable pageable);

        Task<AgeUsuariosPuntoEmisionDAO> ConsultarPorId(int AgageSucursAgeLicencCodigo,
                                                        int agePunEmCodigo,
                                                        int agePunEmAgeSucursCodigo,
                                                        long ageUsuariCodigo);

        Task<Page<AgeUsuariosPuntoEmisionDAO>> ConsultarListaFiltro(int codigoLicenciatario,
                                                                    string filtro,
                                                                    Pageable pageable);

        Task<Resource<AgeUsuariosPuntoEmisionDAO>> Ingresar(
            IHttpContextAccessor _httpContextAccessor,
            AgeUsuariosPuntoEmisionSaveDAO AgeUsuariosPuntoEmisionSaveDAO);

        Task<List<Resource<AgeUsuariosPuntoEmisionDAO>>> IngresarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeUsuariosPuntoEmisionSaveDAO> AgeUsuariosPuntoEmisionSaveDAOList);

        Task<AgeUsuariosPuntoEmisionDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeUsuariosPuntoEmisionSaveDAO AgeUsuariosPuntoEmisionSaveDAO);

        Task<List<AgeUsuariosPuntoEmisionDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeUsuariosPuntoEmisionSaveDAO> AgeUsuariosPuntoEmisionSaveDAOList);
    }
}
