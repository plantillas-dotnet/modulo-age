﻿using AGE.Controllers;
using AGE.Entities.DAO.AgeUsuariosPuntoEmision;
using AGE.Entities;
using AGE.Middleware.Exceptions.BadRequest;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Repositories.AgeUsuariosPuntoEmisionRepository;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using AGE.Utils;
using System.ComponentModel.DataAnnotations;
using System.Transactions;
using AGE.Entities.DAO.AgeUsuarios;
using AGE.Entities.DAO.AgePuntosEmision;
using AGE.Repositories.AgeUsuarioRepository;
using AGE.Repositories.AgePuntosEmisionRepository;

namespace AGE.Services.AgeUsuariosPuntoEmisionService
{
    public class AgeUsuariosPuntoEmisionService : IAgeUsuariosPuntoEmisionService
    {
        private readonly IAgeUsuariosPuntoEmisionRepository _repository;
        private readonly IAgeUsuariosRepository _repositoryUsuari;
        private readonly IAgePuntosEmisionesRepository _repositoryPuntoEmision;

        public AgeUsuariosPuntoEmisionService(
            IAgeUsuariosPuntoEmisionRepository Repository,
            IAgeUsuariosRepository RepositoryUsuario,
            IAgePuntosEmisionesRepository RepositoryPuntoEmision)
        {
            _repository = Repository;
            _repositoryUsuari = RepositoryUsuario;
            _repositoryPuntoEmision = RepositoryPuntoEmision;
        }

        public Task<Page<AgeUsuariosPuntoEmisionDAO>> ConsultarTodos(int AgageSucursAgeLicencCodigo,
                                                            int agePunEmCodigo,
                                                            int agePunEmAgeSucursCodigo,
                                                            long ageUsuariCodigo,
                                                            Pageable pageable)
        {
            if (AgageSucursAgeLicencCodigo <= 0)
                throw new InvalidFieldException("El código de licenciatario debe ser un número entero mayor a cero.");
            pageable.Validar<AgeUsuariosPuntoEmisionDAO>();

            return _repository.ConsultarTodos(AgageSucursAgeLicencCodigo, agePunEmCodigo, agePunEmAgeSucursCodigo, ageUsuariCodigo, pageable);
        }

        public Task<AgeUsuariosPuntoEmisionDAO> ConsultarPorId(int AgageSucursAgeLicencCodigo,
                                                        int agePunEmCodigo,
                                                        int agePunEmAgeSucursCodigo,
                                                        long ageUsuariCodigo)
        {
            if (AgageSucursAgeLicencCodigo <= 0 || agePunEmCodigo <= 0 || agePunEmAgeSucursCodigo <= 0 || ageUsuariCodigo <= 0)
                throw new InvalidFieldException();

            AgeUsuariosPuntoEmisionDAO ageUsuariosPuntoEmisionSaveDAO = _repository.ConsultarPorId(AgageSucursAgeLicencCodigo, agePunEmCodigo, agePunEmAgeSucursCodigo, ageUsuariCodigo).Result;

            return ageUsuariosPuntoEmisionSaveDAO == null
                ? throw new RegisterNotFoundException("Usuario Punto Emision con código licenciatario: " + AgageSucursAgeLicencCodigo +
                                                        ", código punto emisión: " + agePunEmCodigo + ", código sucursal: " + agePunEmAgeSucursCodigo +
                                                        " y código de usuario: " + ageUsuariCodigo + " no existe.")
                : Task.FromResult(ageUsuariosPuntoEmisionSaveDAO);
        }

        private AgeUsuariosPuntoEmision ConsultarCompletePorId(int AgageSucursAgeLicencCodigo,
                                                                int agePunEmCodigo,
                                                                int agePunEmAgeSucursCodigo,
                                                                long ageUsuariCodigo)
        {

            if (AgageSucursAgeLicencCodigo <= 0 || agePunEmCodigo <= 0 || agePunEmAgeSucursCodigo <= 0 || ageUsuariCodigo <= 0)
            {
                throw new InvalidFieldException(
                    (AgageSucursAgeLicencCodigo <= 0 ? "El campo código licenciatario de sucursal es inválido. " : "") +
                    (agePunEmCodigo <= 0 ? "El campo código de punto emisión es inválido. " : "") +
                    (agePunEmAgeSucursCodigo <= 0 ? "El campo código de punto de emisión de sucursal es inválido. " : "") +
                    (ageUsuariCodigo <= 0 ? "El campo código de usuario es inválido." : "")
                );
            }

            AgeUsuariosPuntoEmision ageUsuariosPuntoEmision = _repository.ConsultarCompletePorId(AgageSucursAgeLicencCodigo, agePunEmCodigo, agePunEmAgeSucursCodigo, ageUsuariCodigo).Result;

            return ageUsuariosPuntoEmision;
        }

        public Task<Page<AgeUsuariosPuntoEmisionDAO>> ConsultarListaFiltro(int codigoLicenciatario, string filtro, Pageable pageable)
        {
            if (codigoLicenciatario <= 0)
                throw new InvalidFieldException("El código de licenciatario debe ser un número entero mayor a cero.");

            if (string.IsNullOrWhiteSpace(filtro))
                throw new InvalidFieldException("El filtro es requerido.");

            pageable.Validar<AgeUsuariosPuntoEmisionDAO>();

            return _repository.ConsultarListaFiltro(codigoLicenciatario, filtro, pageable);
        }

        public async Task<Resource<AgeUsuariosPuntoEmisionDAO>> Ingresar(IHttpContextAccessor _httpContextAccessor, AgeUsuariosPuntoEmisionSaveDAO ageUsuariosPuntoEmisionSaveDAO)
        {
            
            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    AgeUsuariosPuntoEmision ageUsuariosPuntoEmision = ValidarInsert(_httpContextAccessor, ageUsuariosPuntoEmisionSaveDAO);
                    AgeUsuariosPuntoEmisionDAO ageUsuariosPuntoEmisionDAO = await _repository.Insertar(ageUsuariosPuntoEmision);
                    
                    Resource<AgeUsuariosPuntoEmisionDAO> ageUsuariosPuntoEmisionDAOWithResource = GetDataWithResource(_httpContextAccessor, ageUsuariosPuntoEmisionDAO);

                    transactionScope.Complete();

                    return ageUsuariosPuntoEmisionDAOWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public async Task<List<Resource<AgeUsuariosPuntoEmisionDAO>>> IngresarVarios(IHttpContextAccessor _httpContextAccessor, List<AgeUsuariosPuntoEmisionSaveDAO> ageUsuariosPuntoEmisionSaveDAO)
        {
            if (ageUsuariosPuntoEmisionSaveDAO == null)
                throw new InvalidSintaxisException();

            ValidateKeys.ValidarPKDuplicadas(ageUsuariosPuntoEmisionSaveDAO,
                                             p => new {
                                                 p.Id.AgageSucursAgeLicencCodigo,
                                                 p.Id.AgeUsuariCodigo,
                                                 p.Id.AgePunEmCodigo,
                                                 p.Id.AgePunEmAgeSucursCodigo,
                                                 p.Id.AgeUsuariAgeLicencCodigo
                                             },
                                             "Id");

            List<AgeUsuariosPuntoEmision> ageUsuariosPuntoEmisionsList = new();

            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    foreach (AgeUsuariosPuntoEmisionSaveDAO ageUsuariosPuntoEmisionsSaveDAO in ageUsuariosPuntoEmisionSaveDAO)
                    {
                        AgeUsuariosPuntoEmision ageUsuariosPuntoEmisions = ValidarInsert(_httpContextAccessor, ageUsuariosPuntoEmisionsSaveDAO);
                        ageUsuariosPuntoEmisionsList.Add(ageUsuariosPuntoEmisions);
                    }

                    List<AgeUsuariosPuntoEmisionDAO> ageUsuariosPuntoEmisionsDAOList = await _repository.InsertarVarios(ageUsuariosPuntoEmisionsList);

                    List<Resource<AgeUsuariosPuntoEmisionDAO>> ageUsuariosPuntoEmisionDAOListWithResource = new();

                    foreach (AgeUsuariosPuntoEmisionDAO ageUsuariosPuntoEmisionDAO in ageUsuariosPuntoEmisionsDAOList)
                    {
                        ageUsuariosPuntoEmisionDAOListWithResource.Add(GetDataWithResource(_httpContextAccessor, ageUsuariosPuntoEmisionDAO));
                    }

                    transactionScope.Complete();

                    return ageUsuariosPuntoEmisionDAOListWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public Task<AgeUsuariosPuntoEmisionDAO> Actualizar(IHttpContextAccessor _httpContextAccessor, AgeUsuariosPuntoEmisionSaveDAO ageUsuariosPuntoEmisionSaveDAO)
        {
            AgeUsuariosPuntoEmision ageUsuariosPuntoEmision = ValidarUpdate(_httpContextAccessor, ageUsuariosPuntoEmisionSaveDAO);

            return _repository.Actualizar(ageUsuariosPuntoEmision);
        }

        public Task<List<AgeUsuariosPuntoEmisionDAO>> ActualizarVarios(IHttpContextAccessor _httpContextAccessor, List<AgeUsuariosPuntoEmisionSaveDAO> ageUsuariosPuntoEmisionSaveDAO)
        {
            if (ageUsuariosPuntoEmisionSaveDAO == null)
                throw new InvalidSintaxisException();

            ValidateKeys.ValidarPKDuplicadas(ageUsuariosPuntoEmisionSaveDAO,
                                             p => new { p.Id.AgageSucursAgeLicencCodigo, 
                                                        p.Id.AgeUsuariCodigo,
                                                        p.Id.AgePunEmCodigo,
                                                        p.Id.AgePunEmAgeSucursCodigo,
                                                        p.Id.AgeUsuariAgeLicencCodigo },
                                             "Id");

            List<AgeUsuariosPuntoEmision> ageUsuariosPuntoEmisionsList = new();
            AgeUsuariosPuntoEmision ageUsuariosPuntoEmision;

            foreach (AgeUsuariosPuntoEmisionSaveDAO ageUsuariosPuntoEmisionSave in ageUsuariosPuntoEmisionSaveDAO)
            {
                ageUsuariosPuntoEmision = ValidarUpdate(_httpContextAccessor, ageUsuariosPuntoEmisionSave);
                ageUsuariosPuntoEmisionsList.Add(ageUsuariosPuntoEmision);
            }

            return _repository.ActualizarVarios(ageUsuariosPuntoEmisionsList);
        }

        private static Resource<AgeUsuariosPuntoEmisionDAO> GetDataWithResource(
            IHttpContextAccessor _httpContextAccessor,
            AgeUsuariosPuntoEmisionDAO ageUsuariosPuntoEmisionSaveDAO)
        {
            string rutaSegmentoFinal = $"{ageUsuariosPuntoEmisionSaveDAO.Id.AgeUsuariAgeLicencCodigo}/{ageUsuariosPuntoEmisionSaveDAO.Id.AgePunEmCodigo}/{ageUsuariosPuntoEmisionSaveDAO.Id.AgePunEmAgeSucursCodigo}/{ageUsuariosPuntoEmisionSaveDAO.Id.AgeUsuariCodigo}";

            return Resource<AgeUsuariosPuntoEmisionDAO>.GetDataWithResource<AgeUsuariosPuntoEmisionController>(
                _httpContextAccessor, rutaSegmentoFinal, ageUsuariosPuntoEmisionSaveDAO);
        }

        private AgeUsuariosPuntoEmision ValidarInsert(
            IHttpContextAccessor _httpContextAccessor,
            AgeUsuariosPuntoEmisionSaveDAO ageUsuariosPuntoEmisionSaveDAO)
        {
            if (ageUsuariosPuntoEmisionSaveDAO == null) throw new InvalidSintaxisException();

            Validator.ValidateObject(ageUsuariosPuntoEmisionSaveDAO, new ValidationContext(ageUsuariosPuntoEmisionSaveDAO), true);

            if (ageUsuariosPuntoEmisionSaveDAO.FechaDesde == default)
                throw new InvalidFieldException("La fecha desde no puede ser nula.");
            
            ValidarKeys(ageUsuariosPuntoEmisionSaveDAO.Id);
             
            AgeUsuariosPuntoEmision entityObject = FromDAOToEntity(_httpContextAccessor, ageUsuariosPuntoEmisionSaveDAO);

            Validator.ValidateObject(entityObject, new ValidationContext(entityObject), true);

            return entityObject;
        }

        private AgeUsuariosPuntoEmision ValidarUpdate(IHttpContextAccessor httpContextAccessor, AgeUsuariosPuntoEmisionSaveDAO ageUsuariosPuntoEmisionSaveDAO)
        {
            if (ageUsuariosPuntoEmisionSaveDAO == null)
                throw new InvalidSintaxisException();

            if (ageUsuariosPuntoEmisionSaveDAO.UsuarioModificacion == null || ageUsuariosPuntoEmisionSaveDAO.UsuarioModificacion <= 0)
                throw new InvalidFieldException("El usuario de modificación debe ser un número entero mayor a cero.");
            
            AgeUsuariosPuntoEmision ageUsuariosPuntoEmision = ConsultarCompletePorId(ageUsuariosPuntoEmisionSaveDAO.Id.AgageSucursAgeLicencCodigo, ageUsuariosPuntoEmisionSaveDAO.Id.AgePunEmCodigo, ageUsuariosPuntoEmisionSaveDAO.Id.AgePunEmAgeSucursCodigo, ageUsuariosPuntoEmisionSaveDAO.Id.AgeUsuariCodigo)
                ?? throw new RegisterNotFoundException("Usuario Punto Emision con código licenciatario: " + ageUsuariosPuntoEmisionSaveDAO.Id.AgageSucursAgeLicencCodigo +
                ", código punto emisión: " + ageUsuariosPuntoEmisionSaveDAO.Id.AgePunEmCodigo +
                ", código sucursal: " + ageUsuariosPuntoEmisionSaveDAO.Id.AgePunEmAgeSucursCodigo +
                " y código de usuario: " + ageUsuariosPuntoEmisionSaveDAO.Id.AgeUsuariCodigo + " no existe.");

            ageUsuariosPuntoEmision.Estado = !string.IsNullOrWhiteSpace(ageUsuariosPuntoEmisionSaveDAO.Estado) ? ageUsuariosPuntoEmisionSaveDAO.Estado : ageUsuariosPuntoEmision.Estado;
            ageUsuariosPuntoEmision.FechaEstado = !string.IsNullOrWhiteSpace(ageUsuariosPuntoEmisionSaveDAO.Estado) ? DateTime.Now : ageUsuariosPuntoEmision.FechaEstado;
            ageUsuariosPuntoEmision.ObservacionEstado = !string.IsNullOrWhiteSpace(ageUsuariosPuntoEmisionSaveDAO.ObservacionEstado) ? ageUsuariosPuntoEmisionSaveDAO.ObservacionEstado : ageUsuariosPuntoEmision.ObservacionEstado;

            ageUsuariosPuntoEmision.FechaDesde = ageUsuariosPuntoEmisionSaveDAO.FechaDesde != default ? ageUsuariosPuntoEmisionSaveDAO.FechaDesde : ageUsuariosPuntoEmision.FechaDesde;
            ageUsuariosPuntoEmision.FechaHasta = ageUsuariosPuntoEmisionSaveDAO.FechaHasta != null && ageUsuariosPuntoEmisionSaveDAO.FechaHasta != default ? ageUsuariosPuntoEmisionSaveDAO.FechaHasta : ageUsuariosPuntoEmision.FechaHasta;

            ageUsuariosPuntoEmision.FechaModificacion = DateTime.Now;
            ageUsuariosPuntoEmision.UbicacionModificacion = Ubicacion.getIpAdress(httpContextAccessor);
            ageUsuariosPuntoEmision.UsuarioModificacion = ageUsuariosPuntoEmisionSaveDAO.UsuarioModificacion;

            Validator.ValidateObject(ageUsuariosPuntoEmision, new ValidationContext(ageUsuariosPuntoEmision), true);

            return ageUsuariosPuntoEmision;
        }

        private AgeUsuariosPuntoEmision FromDAOToEntity(
            IHttpContextAccessor _httpContextAccessor,
            AgeUsuariosPuntoEmisionSaveDAO ageUsuariosPuntoEmisionSaveDAO)
        {

            return new AgeUsuariosPuntoEmision()
            {
                AgageSucursAgeLicencCodigo = ageUsuariosPuntoEmisionSaveDAO.Id.AgageSucursAgeLicencCodigo,
                AgeUsuariAgeLicencCodigo = ageUsuariosPuntoEmisionSaveDAO.Id.AgeUsuariAgeLicencCodigo,
                AgePunEmCodigo = ageUsuariosPuntoEmisionSaveDAO.Id.AgePunEmCodigo,
                AgePunEmAgeSucursCodigo = ageUsuariosPuntoEmisionSaveDAO.Id.AgePunEmAgeSucursCodigo,
                AgeUsuariCodigo = ageUsuariosPuntoEmisionSaveDAO.Id.AgeUsuariCodigo,
                Estado = ageUsuariosPuntoEmisionSaveDAO.Estado,
                FechaEstado = DateTime.Now,
                FechaIngreso = DateTime.Now,
                ObservacionEstado = ageUsuariosPuntoEmisionSaveDAO.ObservacionEstado,
                UbicacionIngreso = Ubicacion.getIpAdress(_httpContextAccessor),
                UsuarioIngreso = ageUsuariosPuntoEmisionSaveDAO.UsuarioIngreso,
                FechaDesde = ageUsuariosPuntoEmisionSaveDAO.FechaDesde,
                FechaHasta = ageUsuariosPuntoEmisionSaveDAO.FechaHasta == default
                                        ? null : ageUsuariosPuntoEmisionSaveDAO.FechaHasta,
            };
        }

        private void ValidarKeys(AgeUsuariosPuntoEmisionPKDAO ageUsuariosPuntoEmisionPKDAO)
        {
            ValidarPK(ageUsuariosPuntoEmisionPKDAO);

            ValidarFKUsuario(ageUsuariosPuntoEmisionPKDAO);

            ValidarFKPuntoEmision(ageUsuariosPuntoEmisionPKDAO);
        }

        private void ValidarPK(AgeUsuariosPuntoEmisionPKDAO ageUsuariosPuntoEmisionPKDAO)
        {
            _ = ConsultarCompletePorId(ageUsuariosPuntoEmisionPKDAO.AgageSucursAgeLicencCodigo, ageUsuariosPuntoEmisionPKDAO.AgePunEmCodigo,
                               ageUsuariosPuntoEmisionPKDAO.AgePunEmAgeSucursCodigo, ageUsuariosPuntoEmisionPKDAO.AgeUsuariCodigo)
               ?? throw new RegisterNotFoundException("Usuario Punto Emision con código licenciatario: " + ageUsuariosPuntoEmisionPKDAO.AgageSucursAgeLicencCodigo +
               ", código punto emisión: " + ageUsuariosPuntoEmisionPKDAO.AgePunEmCodigo +
               ", código sucursal: " + ageUsuariosPuntoEmisionPKDAO.AgePunEmAgeSucursCodigo +
               " y código de usuario: " + ageUsuariosPuntoEmisionPKDAO.AgeUsuariCodigo + " ya existe.");
        }

        private void ValidarFKUsuario(AgeUsuariosPuntoEmisionPKDAO ageUsuariosPuntoEmisionPKDAO)
        {
            ValidateKeys.ValidarExistenciaKey(new AgeUsuarioPKDAO()
            {
                ageLicencCodigo = ageUsuariosPuntoEmisionPKDAO.AgeUsuariAgeLicencCodigo,
                codigo = ageUsuariosPuntoEmisionPKDAO.AgeUsuariCodigo
            },
            $"Usuario con código: {ageUsuariosPuntoEmisionPKDAO.AgeUsuariCodigo} " +
            $"y código licenciatario: {ageUsuariosPuntoEmisionPKDAO.AgeUsuariAgeLicencCodigo} no existe."
            , _repositoryUsuari.ConsultarCompletePorId);
        }

        private void ValidarFKPuntoEmision(AgeUsuariosPuntoEmisionPKDAO ageUsuariosPuntoEmisionPKDAO)
        {
            AgePuntosEmisionesPKDAO agePuntosEmisionPKDAO = new ()
            {
                ageSucursAgeLicencCodigo = ageUsuariosPuntoEmisionPKDAO.AgageSucursAgeLicencCodigo,
                ageSucursCodigo = ageUsuariosPuntoEmisionPKDAO.AgePunEmAgeSucursCodigo,
                codigo = ageUsuariosPuntoEmisionPKDAO.AgePunEmCodigo
            };

            ValidateKeys.ValidarExistenciaKey(agePuntosEmisionPKDAO,
            $"Punto Emisión con código: {ageUsuariosPuntoEmisionPKDAO.AgePunEmCodigo}, " +
            $" código licenciatario: {ageUsuariosPuntoEmisionPKDAO.AgageSucursAgeLicencCodigo} " +
            $" y código de sucursal: {ageUsuariosPuntoEmisionPKDAO.AgePunEmAgeSucursCodigo} no existe."
            , _repositoryPuntoEmision.ConsultarCompletePorId);
        }
    }
}
