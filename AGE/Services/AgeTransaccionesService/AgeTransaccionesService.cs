﻿using AGE.Controllers;
using AGE.Entities;
using AGE.Entities.DAO.AgeTransacciones;
using AGE.Middleware.Exceptions.BadRequest;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Repositories.AgeTransaccioneRepository;
using AGE.Services.AgeSecuenciasPrimariasService;
using AGE.Utils;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using System.ComponentModel.DataAnnotations;
using System.Transactions;

namespace AGE.Services.AgeTransaccioneService
{
    public class AgeTransaccionesService : IAgeTransaccionesService
    {
        private readonly IAgeTransaccionesRepository _repository;
        private readonly IAgeSecuenciasPrimariasService _ageSecuenciasPrimariasService;

        public AgeTransaccionesService(
            IAgeTransaccionesRepository repository,
            IAgeSecuenciasPrimariasService ageSecuenciasPrimariasService)
        {
            _repository = repository;
            _ageSecuenciasPrimariasService = ageSecuenciasPrimariasService;
        }

        public Task<Page<AgeTransaccioneDAO>> ConsultarTodos(Pageable pageable)
        {
            pageable.Validar<AgeTransaccioneDAO>();
            return _repository.ConsultarTodos(pageable);
        }

        public Task<Page<AgeTransaccioneDAO>> ConsultarPorNivel(
           short nivel,
           Pageable pageable)
        {
            if (nivel <= 0)
                throw new InvalidFieldException("El nivel debe ser un número entero mayor a cero.");

            pageable.Validar<AgeTransaccioneDAO>();

            return _repository.ConsultarPorNivel(nivel, pageable);
        }


        public Task<Page<AgeTransaccioneDAO>> ConsultarPorUsuarioLicenciatario(
            long usuarioCodigo, 
            int licenciatarioCodigo, 
            Pageable pageable)
        {

            if (usuarioCodigo <= 0)
                throw new InvalidFieldException("El código del usuario debe ser un número entero mayor a cero.");

            if (licenciatarioCodigo <= 0)
                throw new InvalidFieldException("El código del licenciatario debe ser un número entero mayor a cero.");

            pageable.Validar<AgeTransaccioneDAO>();

            return _repository.ConsultarPorUsuarioLicenciatario(usuarioCodigo, licenciatarioCodigo, pageable);
        }


        public Task<Page<AgeTransaccioneDAO>> ConsultarPorPerfilUsuarioLicenciatario(
            long perfilCodigo, 
            long usuarioCodigo, 
            int licenciatarioCodigo, 
            Pageable pageable)
        {

            if (perfilCodigo <= 0)
                throw new InvalidFieldException("El código del usuario debe ser un número entero mayor a cero.");

            if (usuarioCodigo <= 0)
                throw new InvalidFieldException("El código del usuario debe ser un número entero mayor a cero.");

            if (licenciatarioCodigo <= 0)
                throw new InvalidFieldException("El código del licenciatario debe ser un número entero mayor a cero.");

            pageable.Validar<AgeTransaccioneDAO>();

            return _repository.ConsultarPorPerfilUsuarioLicenciatario(perfilCodigo, usuarioCodigo, licenciatarioCodigo, pageable);
        }



        public Task<Page<AgeTransaccioneDAO>> ConsultarAplicacion(
            int codigoAplicacion,
            int codigo,
            int codigoExterno,
            string descripcion,
            string transaccion,
            short ordenPresentacion,
            string tipoTransaccion,
            string tipoOperacion,
            int ageTransaAgeAplicaCodigo,
            int ageTransaCodigo,
            string? opcion,
            short? nivel,
            string? url,
            string? parametros,
            Pageable pageable)
        {
            if (codigoAplicacion <= 0)
                throw new InvalidFieldException("El código de aplicacion debe ser un número entero mayor a cero.");

            pageable.Validar<AgeTransaccioneDAO>();

            return _repository.ConsultarAplicacion(codigoAplicacion, codigo, codigoExterno, descripcion, transaccion, ordenPresentacion, tipoOperacion, tipoTransaccion, ageTransaAgeAplicaCodigo,
            ageTransaCodigo, opcion, nivel, url, parametros, pageable);
        }

        public Task<Page<AgeTransaccioneDAO>> ConsultarListaFiltro(
            string filtro,
            Pageable pageable)
        {
            if (string.IsNullOrWhiteSpace(filtro))
                throw new InvalidFieldException("El filtro es requerido.");

            pageable.Validar<AgeTransaccioneDAO>();

            return _repository.ConsultarListaFiltro(filtro.ToLower(), pageable);
        }

        public Task<AgeTransaccioneDAO> ConsultarPorId(
            AgeTransaccionePKDAO ageTransaccionePKDAO)
        {
            Validator.ValidateObject(ageTransaccionePKDAO, new ValidationContext(ageTransaccionePKDAO), true);

            AgeTransaccioneDAO? ageTransaccioneDAO = _repository.ConsultarPorId(ageTransaccionePKDAO).Result;

            if (ageTransaccioneDAO == null)
                throw new RegisterNotFoundException("Aplicacion con código " + ageTransaccionePKDAO.codigo + " no existe");

            return Task.FromResult(ageTransaccioneDAO);
        }

        private AgeTransacciones ConsultarCompletePorId(AgeTransaccionePKDAO ageTransaccionePKDAO)
        {
            Validator.ValidateObject(ageTransaccionePKDAO, new ValidationContext(ageTransaccionePKDAO), true);

            AgeTransacciones ageTransaccione = _repository.ConsultarCompletePorId(ageTransaccionePKDAO).Result;

            return ageTransaccione;
        }

        public async Task<Resource<AgeTransaccioneDAO>> Insertar(IHttpContextAccessor _httpContextAccessor, AgeTransaccioneSaveDAO ageTransaccioneSaveDAO)
        {
            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    AgeTransacciones ageTransaccione = ValidarInsert(_httpContextAccessor, ageTransaccioneSaveDAO);

                    AgeTransaccioneDAO ageTransaccioneDAO = await _repository.Insertar(ageTransaccione);

                    Resource<AgeTransaccioneDAO> ageTransaccioneDAOWithResource = GetDataWithResource(_httpContextAccessor, ageTransaccioneDAO);

                    transactionScope.Complete();

                    return ageTransaccioneDAOWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public async Task<List<Resource<AgeTransaccioneDAO>>> InsertarVarios(IHttpContextAccessor _httpContextAccessor, List<AgeTransaccioneSaveDAO> ageTransaccioneSaveDAOList)
        {
            if (ageTransaccioneSaveDAOList == null)
                throw new InvalidSintaxisException();

            List<AgeTransacciones> ageTransaccioneList = new List<AgeTransacciones>();

            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    foreach (AgeTransaccioneSaveDAO ageTransaccioneSaveDAO in ageTransaccioneSaveDAOList)
                    {
                        AgeTransacciones ageTransaccione = ValidarInsert(_httpContextAccessor, ageTransaccioneSaveDAO);
                        ageTransaccioneList.Add(ageTransaccione);
                    }

                    List<AgeTransaccioneDAO> ageTransaccioneDAOList = await _repository.InsertarVarios(ageTransaccioneList);

                    List<Resource<AgeTransaccioneDAO>> ageTransaccioneDAOListWithResource = new List<Resource<AgeTransaccioneDAO>>();

                    foreach (AgeTransaccioneDAO ageTransaccioneDAO in ageTransaccioneDAOList)
                    {
                        ageTransaccioneDAOListWithResource.Add(GetDataWithResource(_httpContextAccessor, ageTransaccioneDAO));
                    }

                    transactionScope.Complete();

                    return ageTransaccioneDAOListWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public Task<AgeTransaccioneDAO> Actualizar(IHttpContextAccessor _httpContextAccessor, AgeTransaccioneSaveDAO ageTransaccioneSaveDAO)
        {
            AgeTransacciones ageTransaccione = ValidarUpdate(_httpContextAccessor, ageTransaccioneSaveDAO);

            return _repository.Actualizar(ageTransaccione);
        }

        public Task<List<AgeTransaccioneDAO>> ActualizarVarios(IHttpContextAccessor _httpContextAccessor,
            List<AgeTransaccioneSaveDAO> ageTransaccioneSaveDAOList)
        {
            if (ageTransaccioneSaveDAOList == null)
                throw new InvalidSintaxisException();

            ValidateKeys.ValidarPKDuplicadas(ageTransaccioneSaveDAOList,
                                             p => new { p.Id.ageAplicaCodigo, p.Id.codigo },
                                             "Id");

            List<AgeTransacciones> ageTransaccioneList = new List<AgeTransacciones>();

            foreach (AgeTransaccioneSaveDAO ageTransaccioneSaveDAO in ageTransaccioneSaveDAOList)
            {
                AgeTransacciones ageTransaccione = ValidarUpdate(_httpContextAccessor, ageTransaccioneSaveDAO);
                ageTransaccioneList.Add(ageTransaccione);
            }

            return _repository.ActualizarVarios(ageTransaccioneList);
        }

        private static Resource<AgeTransaccioneDAO> GetDataWithResource(IHttpContextAccessor _httpContextAccessor,
            AgeTransaccioneDAO ageTransaccioneDAO)
        {
            string transaccioneSegmentoFinal = $"{ageTransaccioneDAO.Id.ageAplicaCodigo}/{ageTransaccioneDAO.Id.codigo}";

            return Resource<AgeTransaccioneDAO>.GetDataWithResource<AgeTransaccionesController>(
                _httpContextAccessor, transaccioneSegmentoFinal, ageTransaccioneDAO);
        }


        private AgeTransacciones ValidarInsert(
            IHttpContextAccessor _httpContextAccessor, 
            AgeTransaccioneSaveDAO ageTransaccioneSaveDAO)
        {
            if (ageTransaccioneSaveDAO == null)
                throw new InvalidSintaxisException();

            Validator.ValidateObject(ageTransaccioneSaveDAO, new ValidationContext(ageTransaccioneSaveDAO), true);

            FuncionesSecuencias fg = new FuncionesSecuencias(_ageSecuenciasPrimariasService);

            ageTransaccioneSaveDAO.Id.codigo = fg.ObtenerSecuenciaPrimaria(
                                                _httpContextAccessor,
                                                Globales.CODIGO_SECUENCIA_TRANSACCION,
                                                ageTransaccioneSaveDAO.UsuarioIngreso);


            ValidarKeys(ageTransaccioneSaveDAO);

            AgeTransacciones entityObject = FromDAOToEntity(_httpContextAccessor, ageTransaccioneSaveDAO);

            Validator.ValidateObject(entityObject, new ValidationContext(entityObject), true);

            return entityObject;
        }

        private AgeTransacciones ValidarUpdate(IHttpContextAccessor _httpContextAccessor, AgeTransaccioneSaveDAO ageTransaccioneSaveDAO)
        {
            if (ageTransaccioneSaveDAO == null)
                throw new InvalidSintaxisException();

            if (ageTransaccioneSaveDAO.UsuarioModificacion == null || ageTransaccioneSaveDAO.UsuarioModificacion <= 0)
                throw new InvalidFieldException("El usuario de modificación debe ser un número entero mayor a cero.");

            AgeTransacciones ageTransaccione = ConsultarCompletePorId(new AgeTransaccionePKDAO
            {
                ageAplicaCodigo = ageTransaccioneSaveDAO.Id.ageAplicaCodigo,
                codigo = ageTransaccioneSaveDAO.Id.codigo
            });

            if (ageTransaccione == null)
                throw new RegisterNotFoundException("Transacción con código " + ageTransaccioneSaveDAO.Id.codigo + " no existe");

            if ((ageTransaccioneSaveDAO.AgeTransaAgeAplicaCodigo != null) ||
                (ageTransaccioneSaveDAO.AgeTransaCodigo != null))
                ValidarFKTransacciones(ageTransaccioneSaveDAO.AgeTransaAgeAplicaCodigo ?? 0,
                                       ageTransaccioneSaveDAO.AgeTransaCodigo ?? 0);


            if (!string.IsNullOrWhiteSpace(ageTransaccioneSaveDAO.Transaccion))
                ageTransaccione.Transaccion = ageTransaccioneSaveDAO.Transaccion;

            if (ageTransaccioneSaveDAO.OrdenPresentacion != null)
                ageTransaccione.OrdenPresentacion = ageTransaccioneSaveDAO.OrdenPresentacion;

            if (!string.IsNullOrWhiteSpace(ageTransaccioneSaveDAO.TipoTransaccion))
                ageTransaccione.TipoTransaccion = ageTransaccioneSaveDAO.TipoTransaccion;

            if (!string.IsNullOrWhiteSpace(ageTransaccioneSaveDAO.TipoOperacion))
                ageTransaccione.TipoOperacion = ageTransaccioneSaveDAO.TipoOperacion;

            if (ageTransaccioneSaveDAO.AgeTransaAgeAplicaCodigo != null &&
                ageTransaccioneSaveDAO.AgeTransaCodigo != null)
            {
                ageTransaccione.AgeTransaAgeAplicaCodigo = ageTransaccioneSaveDAO.AgeTransaAgeAplicaCodigo;
                ageTransaccione.AgeTransaCodigo = ageTransaccioneSaveDAO.AgeTransaCodigo;   
            }

            if (!string.IsNullOrWhiteSpace(ageTransaccioneSaveDAO.Opcion))
                ageTransaccione.Opcion = ageTransaccioneSaveDAO.Opcion;

            if (ageTransaccioneSaveDAO.Nivel != null)
                ageTransaccione.Nivel = ageTransaccioneSaveDAO.Nivel;

            if (!string.IsNullOrWhiteSpace(ageTransaccioneSaveDAO.Url))
                ageTransaccione.Url = ageTransaccioneSaveDAO.Url;

            if (!string.IsNullOrWhiteSpace(ageTransaccioneSaveDAO.Parametros))
                ageTransaccione.Parametros = ageTransaccioneSaveDAO.Parametros;

            if (!string.IsNullOrWhiteSpace(ageTransaccioneSaveDAO.Estado))
            {
                ageTransaccione.Estado = ageTransaccioneSaveDAO.Estado;
                ageTransaccione.FechaEstado = DateTime.Now;
            }

            if (!string.IsNullOrWhiteSpace(ageTransaccioneSaveDAO.Descripcion))
                ageTransaccione.Descripcion = ageTransaccioneSaveDAO.Descripcion;

            if (!string.IsNullOrWhiteSpace(ageTransaccioneSaveDAO.ObservacionEstado))
                ageTransaccione.ObservacionEstado = ageTransaccioneSaveDAO.ObservacionEstado;

            ageTransaccione.FechaModificacion = DateTime.Now;
            ageTransaccione.UbicacionModificacion = Ubicacion.getIpAdress(_httpContextAccessor);
            ageTransaccione.UsuarioModificacion = ageTransaccioneSaveDAO.UsuarioModificacion;

            Validator.ValidateObject(ageTransaccione, new ValidationContext(ageTransaccione), true);

            return ageTransaccione;
        }


        private void ValidarKeys(AgeTransaccioneSaveDAO ageTransaccioneSaveDAO)
        {

            ValidarPK(ageTransaccioneSaveDAO.Id);

            if ((ageTransaccioneSaveDAO.AgeTransaAgeAplicaCodigo != null) ||
                (ageTransaccioneSaveDAO.AgeTransaCodigo != null))
                ValidarFKTransacciones(ageTransaccioneSaveDAO.AgeTransaAgeAplicaCodigo ?? 0, 
                                       ageTransaccioneSaveDAO.AgeTransaCodigo ?? 0);
        }


        private void ValidarPK(AgeTransaccionePKDAO ageTransaccionePKDAO)
        {
            ValidateKeys.ValidarNoExistenciaKey(
              ageTransaccionePKDAO,
              $"Transacción con código {ageTransaccionePKDAO.codigo} ya existe.",
              _repository.ConsultarCompletePorId);
        }

        private void ValidarFKTransacciones(int codigoAplicacion, int codigo)
        {
            AgeTransaccionePKDAO ageTransaccionePKDAO = new AgeTransaccionePKDAO
            {
                ageAplicaCodigo = codigoAplicacion,
                codigo = codigo
            };

            ValidateKeys.ValidarExistenciaKey(
                ageTransaccionePKDAO,
                $"Transacción con código {codigo} no existe.",
                _repository.ConsultarCompletePorId);
        }


        private AgeTransacciones FromDAOToEntity(
            IHttpContextAccessor _httpContextAccessor,
            AgeTransaccioneSaveDAO ageTransaccioneSaveDAO)
        {
            return new AgeTransacciones
            {
                AgeAplicaCodigo = ageTransaccioneSaveDAO.Id.ageAplicaCodigo,
                Codigo = ageTransaccioneSaveDAO.Id.codigo,
                CodigoExterno = ageTransaccioneSaveDAO.CodigoExterno,
                Descripcion = ageTransaccioneSaveDAO.Descripcion,
                Transaccion = ageTransaccioneSaveDAO.Transaccion,
                OrdenPresentacion = ageTransaccioneSaveDAO.OrdenPresentacion,
                TipoTransaccion = ageTransaccioneSaveDAO.TipoTransaccion,
                TipoOperacion = ageTransaccioneSaveDAO.TipoOperacion,
                AgeTransaAgeAplicaCodigo = ageTransaccioneSaveDAO.AgeTransaAgeAplicaCodigo,
                AgeTransaCodigo = ageTransaccioneSaveDAO.AgeTransaCodigo,
                Opcion = ageTransaccioneSaveDAO.Opcion,
                Nivel = ageTransaccioneSaveDAO.Nivel,
                Url = ageTransaccioneSaveDAO.Url,
                Parametros = ageTransaccioneSaveDAO.Parametros,
                Estado = ageTransaccioneSaveDAO.Estado,
                FechaEstado = DateTime.Now,
                FechaIngreso = DateTime.Now,
                ObservacionEstado = ageTransaccioneSaveDAO.ObservacionEstado,
                UbicacionIngreso = Ubicacion.getIpAdress(_httpContextAccessor),
                UsuarioIngreso = ageTransaccioneSaveDAO.UsuarioIngreso
            };
        }

    }
}

