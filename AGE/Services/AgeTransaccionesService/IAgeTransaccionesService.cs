﻿using AGE.Entities.DAO.AgeTransacciones;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AGE.Services.AgeTransaccioneService
{
    public interface IAgeTransaccionesService
    {
        Task<Page<AgeTransaccioneDAO>> ConsultarTodos(
            Pageable pageable);

        Task<Page<AgeTransaccioneDAO>> ConsultarPorNivel(
            short nivel,
            Pageable pageable);

        Task<Page<AgeTransaccioneDAO>> ConsultarPorUsuarioLicenciatario(
            long usuarioCodigo,
            int licenciatarioCodigo,
            Pageable pageable);

        Task<Page<AgeTransaccioneDAO>> ConsultarPorPerfilUsuarioLicenciatario(
            long perfilCodigo,
            long usuarioCodigo,
            int licenciatarioCodigo,
            Pageable pageable);

        Task<Page<AgeTransaccioneDAO>> ConsultarAplicacion(
            int codigoAplicacion,
            int codigo,
            int codigoExterno, 
            string descripcion, 
            string transaccion, 
            short ordenPresentacion, 
            string tipoTransaccion, 
            string tipoOperacion, 
            int ageTransaAgeAplicaCodigo, 
            int ageTransaCodigo, 
            string? opcion, short? nivel, 
            string? url, 
            string? parametros, 
            Pageable pageable);

        Task<Page<AgeTransaccioneDAO>> ConsultarListaFiltro(
            string filtro, 
            Pageable pageable);

        Task<AgeTransaccioneDAO> ConsultarPorId(
            AgeTransaccionePKDAO ageTransaccionePKDAO);

        Task<Resource<AgeTransaccioneDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor, 
            AgeTransaccioneSaveDAO ageTransaccioneSaveDAO);

        Task<List<Resource<AgeTransaccioneDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor, 
            List<AgeTransaccioneSaveDAO> ageTransaccioneSaveDAOList);

        Task<AgeTransaccioneDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor, 
            AgeTransaccioneSaveDAO ageTransaccioneSaveDAO);

        Task<List<AgeTransaccioneDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor, 
            List<AgeTransaccioneSaveDAO> ageTransaccioneSaveDAOList);
    }
}
