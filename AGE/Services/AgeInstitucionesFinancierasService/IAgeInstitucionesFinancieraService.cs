﻿using AGE.Entities.DAO.AgeInstitucionesFinancieras;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;

namespace AGE.Services.AgeInstitucionesFinancierasService
{
    public interface IAgeInstitucionesFinancieraService
    {

        Task<Page<AgeInstitucionesFinancieraDAO>> ConsultarTodos(
            int codigoLicenciatario,
            string codigoSegunBanco,
            string descripcion,
            string siglasBanco,
            Pageable pageable);

        Task<Page<AgeInstitucionesFinancieraDAO>> ConsultarListaFiltro(
            int codigoLicenciatario,
            string filtro,
            Pageable pageable);

        Task<AgeInstitucionesFinancieraDAO> ConsultarPorId(
            AgeInstitucionesFinancieraPKDAO ageInstitucionesFinancieraPKDAO);

        Task<Resource<AgeInstitucionesFinancieraDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgeInstitucionesFinancieraSaveDAO ageInstitucionesFinancieraSaveDAO);

        Task<List<Resource<AgeInstitucionesFinancieraDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeInstitucionesFinancieraSaveDAO> ageInstitucionesFinancieraSaveDAOList);

        Task<AgeInstitucionesFinancieraDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeInstitucionesFinancieraSaveDAO ageInstitucionesFinancieraSaveDAO);

        Task<List<AgeInstitucionesFinancieraDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeInstitucionesFinancieraSaveDAO> ageInstitucionesFinancieraSaveDAOList);

    }
}
