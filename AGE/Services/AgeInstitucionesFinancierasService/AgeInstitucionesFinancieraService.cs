﻿using AGE.Controllers;
using AGE.Entities;
using AGE.Entities.DAO.AgeInstitucionesFinancieras;
using AGE.Middleware.Exceptions.BadRequest;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Repositories.AgeInstitucionesFinancierasRepository;
using AGE.Services.AgeLicenciatariosAplicaSecuService;
using AGE.Utils;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using System.ComponentModel.DataAnnotations;
using System.Transactions;
using AGE.Repositories.AgeLicenciatariosRepository;

namespace AGE.Services.AgeInstitucionesFinancierasService
{
    public class AgeInstitucionesFinancieraService : IAgeInstitucionesFinancieraService
    {

        private readonly IAgeInstitucionesFinancieraRepository _repository;

        private readonly IAgeLicenciatariosRepository _repositoryLicenciatario;

        private readonly IAgeLicenciatariosAplicaSecuService _ageLicenciatariosAplicaSecuService;


        public AgeInstitucionesFinancieraService(
            IAgeInstitucionesFinancieraRepository Repository,
            IAgeLicenciatariosRepository repositoryLicenciatario,
            IAgeLicenciatariosAplicaSecuService ageLicenciatariosAplicaSecuService)
        {
            _repository = Repository;
            _ageLicenciatariosAplicaSecuService = ageLicenciatariosAplicaSecuService;
            _repositoryLicenciatario = repositoryLicenciatario;
        }


        public Task<Page<AgeInstitucionesFinancieraDAO>> ConsultarTodos(
            int codigoLicenciatario, 
            string codigoSegunBanco, 
            string descripcion, 
            string siglasBanco, 
            Pageable pageable)
        {
            if (codigoLicenciatario <= 0)
            {
                throw new InvalidFieldException("El código de licenciatario debe ser un número entero mayor a cero.");
            }

            pageable.Validar<AgeInstitucionesFinancieraDAO>();

            return _repository.ConsultarTodos(codigoLicenciatario, codigoSegunBanco, descripcion, siglasBanco, pageable);
        }


        public Task<Page<AgeInstitucionesFinancieraDAO>> ConsultarListaFiltro(
            int codigoLicenciatario, 
            string filtro, 
            Pageable pageable)
        {
            if (codigoLicenciatario == 0)
            {
                throw new InvalidFieldException("El código de licenciatario debe ser un número entero mayor a cero.");
            }

            if (string.IsNullOrWhiteSpace(filtro))
            {
                throw new InvalidFieldException("El filtro es requerido.");
            }

            pageable.Validar<AgeInstitucionesFinancieraDAO>();

            return _repository.ConsultarListaFiltro(codigoLicenciatario, filtro.ToLower(), pageable);
        }

        public Task<AgeInstitucionesFinancieraDAO> ConsultarPorId(
            AgeInstitucionesFinancieraPKDAO ageInstitucionesFinancieraPKDAO)
        {
            Validator.ValidateObject(ageInstitucionesFinancieraPKDAO, new ValidationContext(ageInstitucionesFinancieraPKDAO), true);

            AgeInstitucionesFinancieraDAO ageInstitucionesFinancieraDAO = _repository.ConsultarPorId(ageInstitucionesFinancieraPKDAO).Result;

            if (ageInstitucionesFinancieraDAO == null)
            {
                throw new RegisterNotFoundException("Institución financiera con código " + ageInstitucionesFinancieraPKDAO.Codigo + " no existe");
            }

            return Task.FromResult(ageInstitucionesFinancieraDAO);
        }


        private AgeInstitucionesFinancieras ConsultarCompletePorId(
           AgeInstitucionesFinancieraPKDAO ageInstitucionesFinancieraPKDAO)
        {
            Validator.ValidateObject(ageInstitucionesFinancieraPKDAO, new ValidationContext(ageInstitucionesFinancieraPKDAO), true);

            AgeInstitucionesFinancieras? ageInstitucionesFinanciera = _repository.ConsultarCompletePorId(ageInstitucionesFinancieraPKDAO).Result;

            return ageInstitucionesFinanciera;
        }

        public async Task<Resource<AgeInstitucionesFinancieraDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgeInstitucionesFinancieraSaveDAO ageInstitucionesFinancieraSaveDAO)
        {
            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    AgeInstitucionesFinancieras ageInstitucionesFinanciera = ValidarInsert(_httpContextAccessor, ageInstitucionesFinancieraSaveDAO);

                    AgeInstitucionesFinancieraDAO ageInstitucionesFinancieraDAO = await _repository.Insertar(ageInstitucionesFinanciera);

                    Resource<AgeInstitucionesFinancieraDAO> ageInstitucionesFinancieraDAOWithResource = GetDataWithResource(_httpContextAccessor, ageInstitucionesFinancieraDAO);

                    transactionScope.Complete();

                    return ageInstitucionesFinancieraDAOWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public async Task<List<Resource<AgeInstitucionesFinancieraDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeInstitucionesFinancieraSaveDAO> ageInstitucionesFinancieraSaveDAOList)
        {
            if (ageInstitucionesFinancieraSaveDAOList == null)
            {
                throw new InvalidSintaxisException();
            }

            List<AgeInstitucionesFinancieras> ageInstitucionesFinancieraList = new List<AgeInstitucionesFinancieras>();

            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    foreach (AgeInstitucionesFinancieraSaveDAO ageInstitucionesFinancieraSaveDAO in ageInstitucionesFinancieraSaveDAOList)
                    {
                        AgeInstitucionesFinancieras ageInstitucionesFinanciera = ValidarInsert(_httpContextAccessor, ageInstitucionesFinancieraSaveDAO);
                        ageInstitucionesFinancieraList.Add(ageInstitucionesFinanciera);
                    }

                    List<AgeInstitucionesFinancieraDAO> ageInstitucionesFinancieraDAOList = await _repository.InsertarVarios(ageInstitucionesFinancieraList);

                    List<Resource<AgeInstitucionesFinancieraDAO>> ageInstitucionesFinancieraDAOListWithResource = new List<Resource<AgeInstitucionesFinancieraDAO>>();

                    foreach (AgeInstitucionesFinancieraDAO ageInstitucionesFinancieraDAO in ageInstitucionesFinancieraDAOList)
                    {
                        ageInstitucionesFinancieraDAOListWithResource.Add(GetDataWithResource(_httpContextAccessor, ageInstitucionesFinancieraDAO));
                    }

                    transactionScope.Complete();

                    return ageInstitucionesFinancieraDAOListWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public Task<AgeInstitucionesFinancieraDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeInstitucionesFinancieraSaveDAO ageInstitucionesFinancieraSaveDAO)
        {
            AgeInstitucionesFinancieras ageInstitucionesFinanciera = ValidarUpdate(_httpContextAccessor, ageInstitucionesFinancieraSaveDAO);

            return _repository.Actualizar(ageInstitucionesFinanciera);
        }

        public Task<List<AgeInstitucionesFinancieraDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeInstitucionesFinancieraSaveDAO> ageInstitucionesFinancieraSaveDAOList)
        {
            if (ageInstitucionesFinancieraSaveDAOList == null)
                throw new InvalidSintaxisException();

            ValidateKeys.ValidarPKDuplicadas(ageInstitucionesFinancieraSaveDAOList,
                                             p => new { p.Id.AgeLicencCodigo, p.Id.Codigo },
                                             "Id");

            List<AgeInstitucionesFinancieras> ageInstitucionesFinancieraList = new List<AgeInstitucionesFinancieras>();
            AgeInstitucionesFinancieras ageInstitucionesFinanciera;

            foreach (AgeInstitucionesFinancieraSaveDAO ageInstitucionesFinancieraSaveDAO in ageInstitucionesFinancieraSaveDAOList)
            {
                ageInstitucionesFinanciera = ValidarUpdate(_httpContextAccessor, ageInstitucionesFinancieraSaveDAO);
                ageInstitucionesFinancieraList.Add(ageInstitucionesFinanciera);
            }

            return _repository.ActualizarVarios(ageInstitucionesFinancieraList);
        }


        private static Resource<AgeInstitucionesFinancieraDAO> GetDataWithResource(
            IHttpContextAccessor _httpContextAccessor,
            AgeInstitucionesFinancieraDAO ageInstitucionesFinancieraDAO)
        {
            string tpersonaSegmentoFinal = $"{ageInstitucionesFinancieraDAO.Id.AgeLicencCodigo}/{ageInstitucionesFinancieraDAO.Id.Codigo}";

            return Resource<AgeInstitucionesFinancieraDAO>.GetDataWithResource<AgeInstitucionesFinancierasController>(
                _httpContextAccessor, tpersonaSegmentoFinal, ageInstitucionesFinancieraDAO);
        }

        private AgeInstitucionesFinancieras ValidarInsert(
        IHttpContextAccessor _httpContextAccessor,
        AgeInstitucionesFinancieraSaveDAO ageInstitucionesFinancieraSaveDAO)
        {

            if (ageInstitucionesFinancieraSaveDAO == null)
                throw new InvalidSintaxisException();

            Validator.ValidateObject(ageInstitucionesFinancieraSaveDAO, new ValidationContext(ageInstitucionesFinancieraSaveDAO), true);

            FuncionesSecuencias fg = new FuncionesSecuencias(_ageLicenciatariosAplicaSecuService);

            ageInstitucionesFinancieraSaveDAO.Id.Codigo = fg.ObtenerSecuencia(
                                                                                _httpContextAccessor,
                                                                                ageInstitucionesFinancieraSaveDAO.Id.AgeLicencCodigo,
                                                                                Globales.CODIGO_SECUENCIA_INSTITUCION_FINANCIERA,
                                                                                ageInstitucionesFinancieraSaveDAO.UsuarioIngreso);

            ValidarKeys(ageInstitucionesFinancieraSaveDAO);

            AgeInstitucionesFinancieras entityObject = FromDAOToEntity(_httpContextAccessor, ageInstitucionesFinancieraSaveDAO);

            Validator.ValidateObject(entityObject, new ValidationContext(entityObject), true);

            return entityObject;
        }



        private AgeInstitucionesFinancieras ValidarUpdate(
            IHttpContextAccessor _httpContextAccessor,
            AgeInstitucionesFinancieraSaveDAO ageInstitucionesFinancieraSaveDAO)
        {

            if (ageInstitucionesFinancieraSaveDAO == null)
                throw new InvalidSintaxisException();


            if (ageInstitucionesFinancieraSaveDAO.UsuarioModificacion == null || ageInstitucionesFinancieraSaveDAO.UsuarioModificacion <= 0)
                throw new InvalidFieldException("El usuario de modificación debe ser un número entero mayor a cero.");


            AgeInstitucionesFinancieras? ageInstitucionesFinanciera = ConsultarCompletePorId(new AgeInstitucionesFinancieraPKDAO
            {
                AgeLicencCodigo = ageInstitucionesFinancieraSaveDAO.Id.AgeLicencCodigo,
                Codigo = ageInstitucionesFinancieraSaveDAO.Id.Codigo
            });

            if (ageInstitucionesFinanciera == null)
                throw new RegisterNotFoundException("Institución financiera con código " + ageInstitucionesFinancieraSaveDAO.Id.Codigo + " no existe");


            if (!string.IsNullOrWhiteSpace(ageInstitucionesFinancieraSaveDAO.Estado))
            {
                ageInstitucionesFinanciera.Estado = ageInstitucionesFinancieraSaveDAO.Estado;
                ageInstitucionesFinanciera.FechaEstado = DateTime.Now;
            }

            if (!string.IsNullOrWhiteSpace(ageInstitucionesFinancieraSaveDAO.CodigoSegunBanco))
                ageInstitucionesFinanciera.CodigoSegunBanco = ageInstitucionesFinancieraSaveDAO.CodigoSegunBanco;

            if (!string.IsNullOrWhiteSpace(ageInstitucionesFinancieraSaveDAO.Descripcion))
                ageInstitucionesFinanciera.Descripcion = ageInstitucionesFinancieraSaveDAO.Descripcion;

            if (!string.IsNullOrWhiteSpace(ageInstitucionesFinancieraSaveDAO.SiglasBanco))
                ageInstitucionesFinanciera.SiglasBanco = ageInstitucionesFinancieraSaveDAO.SiglasBanco;

            if (!string.IsNullOrWhiteSpace(ageInstitucionesFinancieraSaveDAO.ObservacionEstado))
                ageInstitucionesFinanciera.ObservacionEstado = ageInstitucionesFinancieraSaveDAO.ObservacionEstado;

            ageInstitucionesFinanciera.FechaModificacion = DateTime.Now;
            ageInstitucionesFinanciera.UbicacionModificacion = Ubicacion.getIpAdress(_httpContextAccessor);
            ageInstitucionesFinanciera.UsuarioModificacion = ageInstitucionesFinancieraSaveDAO.UsuarioModificacion;

            Validator.ValidateObject(ageInstitucionesFinanciera, new ValidationContext(ageInstitucionesFinanciera), true);

            return ageInstitucionesFinanciera;
        }

        private AgeInstitucionesFinancieras FromDAOToEntity(
        IHttpContextAccessor _httpContextAccessor,
        AgeInstitucionesFinancieraSaveDAO ageInstitucionesFinancieraSaveDAO)
        {

            return new AgeInstitucionesFinancieras
            {
                AgeLicencCodigo = ageInstitucionesFinancieraSaveDAO.Id.AgeLicencCodigo,
                Codigo = ageInstitucionesFinancieraSaveDAO.Id.Codigo,
                Descripcion = ageInstitucionesFinancieraSaveDAO.Descripcion,
                CodigoSegunBanco = ageInstitucionesFinancieraSaveDAO.CodigoSegunBanco,
                SiglasBanco = ageInstitucionesFinancieraSaveDAO.SiglasBanco,
                Estado = ageInstitucionesFinancieraSaveDAO.Estado,
                FechaEstado = DateTime.Now,
                FechaIngreso = DateTime.Now,
                ObservacionEstado = ageInstitucionesFinancieraSaveDAO.ObservacionEstado,
                UbicacionIngreso = Ubicacion.getIpAdress(_httpContextAccessor),
                UsuarioIngreso = ageInstitucionesFinancieraSaveDAO.UsuarioIngreso
            };
        }

        private void ValidarKeys(AgeInstitucionesFinancieraSaveDAO ageInstitucionesFinancieraSaveDAO)
        {
            ValidarPK(ageInstitucionesFinancieraSaveDAO.Id);

            ValidarFKLicenciatario(ageInstitucionesFinancieraSaveDAO.Id.AgeLicencCodigo);

        }

        private void ValidarPK(AgeInstitucionesFinancieraPKDAO ageInstitucionesFinancieraPKDAO)
        {

            ValidateKeys.ValidarNoExistenciaKey(
               ageInstitucionesFinancieraPKDAO,
               $"Forma de pago institución financiera con código {ageInstitucionesFinancieraPKDAO.Codigo} ya existe.",
               _repository.ConsultarCompletePorId);
        }


        private void ValidarFKLicenciatario(int codigoLicenciatario)
        {

            ValidateKeys.ValidarExistenciaKey(
                codigoLicenciatario,
                $"Licenciatario con código {codigoLicenciatario} no existe.",
                _repositoryLicenciatario.ConsultarCompletePorId);
        }


    }
}
