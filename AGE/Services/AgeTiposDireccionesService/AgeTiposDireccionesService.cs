﻿using AGE.Controllers;
using AGE.Entities.DAO.AgeTiposDireccione;
using AGE.Entities;
using AGE.Middleware.Exceptions.BadRequest;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Repositories.AgeTiposDireccioneRepository;
using AGE.Services.AgeSecuenciasPrimariasService;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using AGE.Utils;
using System.ComponentModel.DataAnnotations;
using System.Transactions;

namespace AGE.Services.AgeTiposDireccioneService
{
    public class AgeTiposDireccionesService : IAgeTiposDireccionesService
    {
        private readonly IAgeTiposDireccionesRepository _repository;
        private readonly IAgeSecuenciasPrimariasService _ageSecuenciasPrimariasService;

        public AgeTiposDireccionesService(
            IAgeTiposDireccionesRepository Repository,
            IAgeSecuenciasPrimariasService ageSecuenciasPrimariasService)
        {
            _repository = Repository;
            _ageSecuenciasPrimariasService = ageSecuenciasPrimariasService;
        }

        public Task<Page<AgeTiposDireccionesDAO>> ConsultarTodos(
            int? codigo,
            string? descripcion,
            Pageable pageable)
        {

            pageable.Validar<AgeTiposDireccionesDAO>();

            return _repository.ConsultarTodos(codigo ?? 0, descripcion ?? "", pageable);
        }

        public Task<Page<AgeTiposDireccionesDAO>> ConsultarListaFiltro(
            string filtro,
            Pageable pageable)
        {
            if (string.IsNullOrWhiteSpace(filtro))
                throw new InvalidFieldException("El filtro es requerido.");

            pageable.Validar<AgeTiposDireccionesDAO>();

            return _repository.ConsultarListaFiltro(filtro.ToLower(), pageable);
        }

        public Task<AgeTiposDireccionesDAO> ConsultarPorId(int codigo)
        {

            if (codigo <= 0)
                throw new InvalidIdException();

            AgeTiposDireccionesDAO? ageTiposDireccioneDAO = _repository.ConsultarPorId(codigo).Result;

            if (ageTiposDireccioneDAO == null)
                throw new RegisterNotFoundException("Tipo dirección con código " + ageTiposDireccioneDAO + " no existe");

            return Task.FromResult(ageTiposDireccioneDAO);
        }

        public Task<AgeTiposDirecciones> ConsultarCompletePorId(int codigo)
        {

            if (codigo <= 0)
                throw new InvalidIdException();

            return _repository.ConsultarCompletePorId(codigo);
        }

        public async Task<Resource<AgeTiposDireccionesDAO>> Insertar(
        IHttpContextAccessor _httpContextAccessor,
        AgeTiposDireccionesSaveDAO ageTiposDireccioneSaveDAO)
        {
            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    AgeTiposDirecciones ageTiposDireccione = ValidarInsert(_httpContextAccessor, ageTiposDireccioneSaveDAO);

                    AgeTiposDireccionesDAO ageTiposDireccioneDAO = await _repository.Insertar(ageTiposDireccione);

                    Resource<AgeTiposDireccionesDAO> ageTiposDireccioneDAOWithResource = GetDataWithResource(_httpContextAccessor, ageTiposDireccioneDAO);

                    transactionScope.Complete();

                    return ageTiposDireccioneDAOWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public async Task<List<Resource<AgeTiposDireccionesDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeTiposDireccionesSaveDAO> ageTiposDireccioneSaveDAOList)
        {
            if (ageTiposDireccioneSaveDAOList == null)
            {
                throw new InvalidSintaxisException();
            }

            List<AgeTiposDirecciones> ageTiposDireccioneList = new List<AgeTiposDirecciones>();

            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    foreach (AgeTiposDireccionesSaveDAO ageTiposDireccioneSaveDAO in ageTiposDireccioneSaveDAOList)
                    {
                        AgeTiposDirecciones ageTiposDireccione = ValidarInsert(_httpContextAccessor, ageTiposDireccioneSaveDAO);
                        ageTiposDireccioneList.Add(ageTiposDireccione);
                    }

                    List<AgeTiposDireccionesDAO> ageTiposDireccioneDAOList = await _repository.InsertarVarios(ageTiposDireccioneList);

                    List<Resource<AgeTiposDireccionesDAO>> ageTiposDireccioneDAOListWithResource = new List<Resource<AgeTiposDireccionesDAO>>();

                    foreach (AgeTiposDireccionesDAO ageTiposDireccioneDAO in ageTiposDireccioneDAOList)
                    {
                        ageTiposDireccioneDAOListWithResource.Add(GetDataWithResource(_httpContextAccessor, ageTiposDireccioneDAO));
                    }

                    transactionScope.Complete();

                    return ageTiposDireccioneDAOListWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public Task<AgeTiposDireccionesDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeTiposDireccionesSaveDAO ageTiposDireccioneSaveDAO)
        {
            AgeTiposDirecciones ageTiposDireccione = ValidarUpdate(_httpContextAccessor, ageTiposDireccioneSaveDAO);

            return _repository.Actualizar(ageTiposDireccione);
        }

        public Task<List<AgeTiposDireccionesDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeTiposDireccionesSaveDAO> ageTiposDireccioneSaveDAOList)
        {
            if (ageTiposDireccioneSaveDAOList == null)
                throw new InvalidSintaxisException();

            ValidateKeys.ValidarPKDuplicadas(ageTiposDireccioneSaveDAOList,
                                             p => new { p.Codigo },
                                             "Id");

            List<AgeTiposDirecciones> ageTiposDireccioneList = new List<AgeTiposDirecciones>();
            AgeTiposDirecciones ageTiposDireccione;

            foreach (AgeTiposDireccionesSaveDAO ageTiposDireccioneSaveDAO in ageTiposDireccioneSaveDAOList)
            {
                ageTiposDireccione = ValidarUpdate(_httpContextAccessor, ageTiposDireccioneSaveDAO);
                ageTiposDireccioneList.Add(ageTiposDireccione);
            }

            return _repository.ActualizarVarios(ageTiposDireccioneList);
        }

        private static Resource<AgeTiposDireccionesDAO> GetDataWithResource(
            IHttpContextAccessor _httpContextAccessor,
            AgeTiposDireccionesDAO ageTiposDireccioneDAO)
        {
            string idtipoSegmentoFinal = $"{ageTiposDireccioneDAO.Codigo}";

            return Resource<AgeTiposDireccionesDAO>.GetDataWithResource<AgeTiposDireccionesController>(
                _httpContextAccessor, idtipoSegmentoFinal, ageTiposDireccioneDAO);
        }

        private AgeTiposDirecciones ValidarInsert(
            IHttpContextAccessor _httpContextAccessor,
            AgeTiposDireccionesSaveDAO ageTiposDireccioneSaveDAO)
        {

            if (ageTiposDireccioneSaveDAO == null)
                throw new InvalidSintaxisException();

            Validator.ValidateObject(ageTiposDireccioneSaveDAO, new ValidationContext(ageTiposDireccioneSaveDAO), true);

            FuncionesSecuencias fg = new FuncionesSecuencias(_ageSecuenciasPrimariasService);

            ageTiposDireccioneSaveDAO.Codigo = fg.ObtenerSecuenciaPrimaria(
                                                    _httpContextAccessor, 
                                                    Globales.CODIGO_SECUENCIA_TIPO_DIRECCION, 
                                                    ageTiposDireccioneSaveDAO.UsuarioIngreso);

            ValidarPK(ageTiposDireccioneSaveDAO.Codigo);

            AgeTiposDirecciones entityObject = FromDAOToEntity(_httpContextAccessor, ageTiposDireccioneSaveDAO);

            Validator.ValidateObject(entityObject, new ValidationContext(entityObject), true);

            return entityObject;
        }


        private AgeTiposDirecciones ValidarUpdate(
            IHttpContextAccessor _httpContextAccessor,
            AgeTiposDireccionesSaveDAO ageTiposDireccioneSaveDAO)
        {
            if (ageTiposDireccioneSaveDAO == null)
                throw new InvalidSintaxisException();

            if (ageTiposDireccioneSaveDAO.UsuarioModificacion == null || ageTiposDireccioneSaveDAO.UsuarioModificacion <= 0)
                throw new InvalidFieldException("El usuario de modificación debe ser un número entero mayor a cero.");

            AgeTiposDirecciones ageTiposDireccione = ConsultarCompletePorId(ageTiposDireccioneSaveDAO.Codigo).Result;

            if (ageTiposDireccione == null)
                throw new RegisterNotFoundException("Tipo dirección con código " + ageTiposDireccioneSaveDAO.Codigo + " no existe");
            
            if (!string.IsNullOrWhiteSpace(ageTiposDireccioneSaveDAO.Estado))
            {
                ageTiposDireccione.Estado = ageTiposDireccioneSaveDAO.Estado;
                ageTiposDireccione.FechaEstado = DateTime.Now;
            }

            if (!string.IsNullOrWhiteSpace(ageTiposDireccioneSaveDAO.Descripcion))
                ageTiposDireccione.Descripcion = ageTiposDireccioneSaveDAO.Descripcion;

            if (!string.IsNullOrWhiteSpace(ageTiposDireccioneSaveDAO.ObservacionEstado))
                ageTiposDireccione.ObservacionEstado = ageTiposDireccioneSaveDAO.ObservacionEstado;

            ageTiposDireccione.FechaModificacion = DateTime.Now;
            ageTiposDireccione.UbicacionModificacion = Ubicacion.getIpAdress(_httpContextAccessor);
            ageTiposDireccione.UsuarioModificacion = ageTiposDireccioneSaveDAO.UsuarioModificacion;

            Validator.ValidateObject(ageTiposDireccione, new ValidationContext(ageTiposDireccione), true);

            return ageTiposDireccione;
        }


        private void ValidarPK(int codigo)
        {

            ValidateKeys.ValidarNoExistenciaKey(
               codigo,
               $"Tipo dirección con código {codigo} ya existe.",
               _repository.ConsultarCompletePorId);
        }


        private AgeTiposDirecciones FromDAOToEntity(
            IHttpContextAccessor _httpContextAccessor,
            AgeTiposDireccionesSaveDAO ageTiposDireccioneSaveDAO)
        {
            return new AgeTiposDirecciones
            {
                Codigo = ageTiposDireccioneSaveDAO.Codigo,
                Descripcion = ageTiposDireccioneSaveDAO.Descripcion,
                Estado = ageTiposDireccioneSaveDAO.Estado,
                FechaEstado = DateTime.Now,
                FechaIngreso = DateTime.Now,
                ObservacionEstado = ageTiposDireccioneSaveDAO.ObservacionEstado,
                UbicacionIngreso = Ubicacion.getIpAdress(_httpContextAccessor),
                UsuarioIngreso = ageTiposDireccioneSaveDAO.UsuarioIngreso
            };
        }
    }
}

