﻿using AGE.Entities;
using AGE.Entities.DAO.AgeTiposDireccione;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;

namespace AGE.Services.AgeTiposDireccioneService
{
    public interface IAgeTiposDireccionesService
    {
        Task<Page<AgeTiposDireccionesDAO>> ConsultarTodos(
            int? codigo,
            string? descripcion, 
            Pageable pageable);

        Task<Page<AgeTiposDireccionesDAO>> ConsultarListaFiltro(
            string filtro, 
            Pageable pageable);

        Task<AgeTiposDireccionesDAO> ConsultarPorId(
            int codigo);

        Task<AgeTiposDirecciones> ConsultarCompletePorId(
            int codigo);

        Task<Resource<AgeTiposDireccionesDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor, 
            AgeTiposDireccionesSaveDAO ageTiposDireccioneSaveDAO);

        Task<List<Resource<AgeTiposDireccionesDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor, 
            List<AgeTiposDireccionesSaveDAO> ageTiposDireccioneSaveDAOList);

        Task<AgeTiposDireccionesDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor, 
            AgeTiposDireccionesSaveDAO ageTiposDireccioneSaveDAO);

        Task<List<AgeTiposDireccionesDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor, 
            List<AgeTiposDireccionesSaveDAO> ageTiposDireccioneSaveDAOList);
    }
}
