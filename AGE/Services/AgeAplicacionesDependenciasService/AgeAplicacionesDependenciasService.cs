﻿
using AGE.Entities.DAO.AgeAplicacionesDependencias;
using AGE.Entities;
using AGE.Middleware.Exceptions.BadRequest;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Repositories.AgeAplicacionesDependenciasRepository;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using AGE.Utils;
using System.ComponentModel.DataAnnotations;
using System.Transactions;
using AGE.Controllers;
using AGE.Repositories.AgeAplicacionesRepository;

namespace AGE.Services.AgeAplicacionesDependenciasService
{
    public class AgeAplicacionesDependenciasService : IAgeAplicacionesDependenciasService
    {
        private readonly IAgeAplicacionesDependenciasRepository _repository;
        private readonly IAgeAplicacionesRepository _repositoryAplica;

        public AgeAplicacionesDependenciasService(
            IAgeAplicacionesDependenciasRepository Repository,
            IAgeAplicacionesRepository RepositoryAplica)
        {
            _repository = Repository;
            _repositoryAplica = RepositoryAplica;
        }

        public Task<Page<AgeAplicacionesDependenciasDAO>> ConsultarTodos(
            Pageable pageable)
        {
            pageable.Validar<AgeAplicacionesDependenciasDAO>();

            return _repository.ConsultarTodos(pageable);
        }

        public Task<AgeAplicacionesDependenciasDAO> ConsultarPorId(
            AgeAplicacionesDependenciasPKDAO ageAplicacionesDependenciasPKDAO)
        {
            Validator.ValidateObject(ageAplicacionesDependenciasPKDAO, new ValidationContext(ageAplicacionesDependenciasPKDAO), true);

            AgeAplicacionesDependenciasDAO ageAplicacionesDependenciasDAO = _repository.ConsultarPorId(ageAplicacionesDependenciasPKDAO).Result;

            return ageAplicacionesDependenciasDAO == null
                ? throw new RegisterNotFoundException("Aplicación dependencia con orden: " + ageAplicacionesDependenciasPKDAO.OrdenDependencia + " no existe.")
                : Task.FromResult(ageAplicacionesDependenciasDAO);
        }

        private AgeAplicacionesDependencia ConsultarCompletePorId(
            AgeAplicacionesDependenciasPKDAO ageAplicacionesDependenciasPKDAO)
        {
            Validator.ValidateObject(ageAplicacionesDependenciasPKDAO, new ValidationContext(ageAplicacionesDependenciasPKDAO), true);

            AgeAplicacionesDependencia ageAplicacionesDependencias = _repository.ConsultarCompletePorId(ageAplicacionesDependenciasPKDAO).Result;

            return ageAplicacionesDependencias;
        }

        public Task<Page<AgeAplicacionesDependenciasDAO>> ConsultarListaFiltro(
            string filtro,
            Pageable pageable)
        {
            if (string.IsNullOrWhiteSpace(filtro))
                throw new InvalidFieldException("El filtro es requerido.");

            pageable.Validar<AgeAplicacionesDependenciasDAO>();

            return _repository.ConsultarListaFiltro(filtro.ToLower(), pageable);
        }

        public async Task<Resource<AgeAplicacionesDependenciasDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgeAplicacionesDependenciasSaveDAO ageAplicacionesDependenciasSaveDAO)
        {
            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    AgeAplicacionesDependencia ageAplicacionesDependencias = ValidarInsert(_httpContextAccessor, ageAplicacionesDependenciasSaveDAO);

                    AgeAplicacionesDependenciasDAO ageAplicacionesDependenciasDAO = await _repository.Insertar(ageAplicacionesDependencias);
                    
                    Resource<AgeAplicacionesDependenciasDAO> ageAplicacionesDependenciasDAOWithResource = GetDataWithResource(_httpContextAccessor, ageAplicacionesDependenciasDAO);

                    transactionScope.Complete();

                    return ageAplicacionesDependenciasDAOWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public async Task<List<Resource<AgeAplicacionesDependenciasDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeAplicacionesDependenciasSaveDAO> ageAplicacionesDependenciasSaveDAOList)
        {
            if (ageAplicacionesDependenciasSaveDAOList == null)
                throw new InvalidSintaxisException();

            ValidateKeys.ValidarPKDuplicadas(ageAplicacionesDependenciasSaveDAOList, p => new
            {
                p.Id.AgeAplicaCodigo,
                p.Id.AgeAplicaCodigoSer,
                p.Id.OrdenDependencia
            }, "Id");

            List<AgeAplicacionesDependencia> ageAplicacionesDependenciasList = new List<AgeAplicacionesDependencia>();

            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    foreach (AgeAplicacionesDependenciasSaveDAO ageAplicacionesDependenciasSaveDAO in ageAplicacionesDependenciasSaveDAOList)
                    {
                        AgeAplicacionesDependencia ageAplicacionesDependencia = ValidarInsert(_httpContextAccessor, ageAplicacionesDependenciasSaveDAO);
                        ageAplicacionesDependenciasList.Add(ageAplicacionesDependencia);
                    }

                    List<AgeAplicacionesDependenciasDAO> ageAplicacionesDependenciasDAOList = await _repository.InsertarVarios(ageAplicacionesDependenciasList);

                    List<Resource<AgeAplicacionesDependenciasDAO>> ageAplicacionesDependenciasDAOListWithResource = new List<Resource<AgeAplicacionesDependenciasDAO>>();

                    foreach (AgeAplicacionesDependenciasDAO ageAplicacionesDependenciasDAO in ageAplicacionesDependenciasDAOList)
                    {
                        ageAplicacionesDependenciasDAOListWithResource.Add(GetDataWithResource(_httpContextAccessor, ageAplicacionesDependenciasDAO));
                    }

                    transactionScope.Complete();

                    return ageAplicacionesDependenciasDAOListWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public Task<AgeAplicacionesDependenciasDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeAplicacionesDependenciasSaveDAO ageAplicacionesDependenciasSaveDAO)
        {
            AgeAplicacionesDependencia ageAplicacionesDependencias = ValidarUpdate(_httpContextAccessor, ageAplicacionesDependenciasSaveDAO);

            return _repository.Actualizar(ageAplicacionesDependencias);
        }

        public Task<List<AgeAplicacionesDependenciasDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeAplicacionesDependenciasSaveDAO> ageAplicacionesDependenciasSaveDAOList)
        {
            if (ageAplicacionesDependenciasSaveDAOList == null)
                throw new InvalidSintaxisException();

            ValidateKeys.ValidarPKDuplicadas(ageAplicacionesDependenciasSaveDAOList, p => new
            {
                p.Id.AgeAplicaCodigo,
                p.Id.AgeAplicaCodigoSer,
                p.Id.OrdenDependencia
            }, "Id");

            List<AgeAplicacionesDependencia> ageAplicacionesDependenciasList = new List<AgeAplicacionesDependencia>();
            AgeAplicacionesDependencia ageAplicacionesDependencias;

            foreach (AgeAplicacionesDependenciasSaveDAO ageAplicacionesDependenciasSaveDAO in ageAplicacionesDependenciasSaveDAOList)
            {
                ageAplicacionesDependencias = ValidarUpdate(_httpContextAccessor, ageAplicacionesDependenciasSaveDAO);
                ageAplicacionesDependenciasList.Add(ageAplicacionesDependencias);
            }

            return _repository.ActualizarVarios(ageAplicacionesDependenciasList);
        }

        private static Resource<AgeAplicacionesDependenciasDAO> GetDataWithResource(
            IHttpContextAccessor _httpContextAccessor,
            AgeAplicacionesDependenciasDAO ageAplicacionesDependenciasDAO)
        {
            string rutaSegmentoFinal = $"{ageAplicacionesDependenciasDAO.Id.AgeAplicaCodigo}/{ageAplicacionesDependenciasDAO.Id.AgeAplicaCodigoSer}/{ageAplicacionesDependenciasDAO.Id.OrdenDependencia}";

            return Resource<AgeAplicacionesDependenciasDAO>.GetDataWithResource<AgeAplicacionesDependenciasController>(
                _httpContextAccessor, rutaSegmentoFinal, ageAplicacionesDependenciasDAO);
        }

        private AgeAplicacionesDependencia ValidarInsert(
            IHttpContextAccessor _httpContextAccessor,
            AgeAplicacionesDependenciasSaveDAO ageAplicacionesDependenciasSaveDAO)
        {
            if (ageAplicacionesDependenciasSaveDAO == null)
                throw new InvalidSintaxisException();

            Validator.ValidateObject(ageAplicacionesDependenciasSaveDAO, new ValidationContext(ageAplicacionesDependenciasSaveDAO), true);

            ValidarKeys(ageAplicacionesDependenciasSaveDAO.Id);

            AgeAplicacionesDependencia entityObject = FromDAOToEntity(_httpContextAccessor, ageAplicacionesDependenciasSaveDAO);

            Validator.ValidateObject(entityObject, new ValidationContext(entityObject), true);

            return entityObject;
        }

        private AgeAplicacionesDependencia ValidarUpdate(
            IHttpContextAccessor _httpContextAccessor,
            AgeAplicacionesDependenciasSaveDAO ageAplicacionesDependenciasSaveDAO)
        {
            if (ageAplicacionesDependenciasSaveDAO == null)
                throw new InvalidSintaxisException();

            if (ageAplicacionesDependenciasSaveDAO.UsuarioModificacion == null || ageAplicacionesDependenciasSaveDAO.UsuarioModificacion <= 0)
                throw new InvalidFieldException("El usuario de modificación debe ser un número entero mayor a cero.");

            AgeAplicacionesDependencia ageAplicacionesDependencias = ConsultarCompletePorId(ageAplicacionesDependenciasSaveDAO.Id) 
                ?? throw new RegisterNotFoundException("Aplicación dependencia con orden: " + ageAplicacionesDependenciasSaveDAO.Id.OrdenDependencia + " no existe");
            
            ageAplicacionesDependencias.Estado = !string.IsNullOrWhiteSpace(ageAplicacionesDependenciasSaveDAO.Estado) ? ageAplicacionesDependenciasSaveDAO.Estado : ageAplicacionesDependencias.Estado;
            ageAplicacionesDependencias.FechaEstado = !string.IsNullOrWhiteSpace(ageAplicacionesDependenciasSaveDAO.Estado) ? DateTime.Now : ageAplicacionesDependencias.FechaEstado;
            ageAplicacionesDependencias.ObservacionEstado = !string.IsNullOrWhiteSpace(ageAplicacionesDependenciasSaveDAO.ObservacionEstado) ? ageAplicacionesDependenciasSaveDAO.ObservacionEstado : ageAplicacionesDependencias.ObservacionEstado;

            ageAplicacionesDependencias.FechaModificacion = DateTime.Now;
            ageAplicacionesDependencias.UbicacionModificacion = Ubicacion.getIpAdress(_httpContextAccessor);
            ageAplicacionesDependencias.UsuarioModificacion = ageAplicacionesDependenciasSaveDAO.UsuarioModificacion;

            Validator.ValidateObject(ageAplicacionesDependencias, new ValidationContext(ageAplicacionesDependencias), true);

            return ageAplicacionesDependencias;
        }

        private AgeAplicacionesDependencia FromDAOToEntity(
            IHttpContextAccessor _httpContextAccessor,
            AgeAplicacionesDependenciasSaveDAO ageAplicacionesDependenciasSaveDAO)
        {

            return new AgeAplicacionesDependencia
            {
                AgeAplicaCodigo = ageAplicacionesDependenciasSaveDAO.Id.AgeAplicaCodigo,
                AgeAplicaCodigoSer = ageAplicacionesDependenciasSaveDAO.Id.AgeAplicaCodigoSer,
                OrdenDependencia = ageAplicacionesDependenciasSaveDAO.Id.OrdenDependencia,
                Estado = ageAplicacionesDependenciasSaveDAO.Estado,
                FechaEstado = DateTime.Now,
                FechaIngreso = DateTime.Now,
                ObservacionEstado = ageAplicacionesDependenciasSaveDAO.ObservacionEstado,
                UbicacionIngreso = Ubicacion.getIpAdress(_httpContextAccessor),
                UsuarioIngreso = ageAplicacionesDependenciasSaveDAO.UsuarioIngreso
            };
        }

        private void ValidarKeys(AgeAplicacionesDependenciasPKDAO ageAplicacionesDependenciasPKDAO)
        {
            ValidarPK(ageAplicacionesDependenciasPKDAO);
            ValidarFKs(ageAplicacionesDependenciasPKDAO.AgeAplicaCodigo, "Aplicación con código:");
            ValidarFKs(ageAplicacionesDependenciasPKDAO.AgeAplicaCodigoSer, "Aplicación Ser con código:");
        }

        private void ValidarPK(AgeAplicacionesDependenciasPKDAO ageAplicacionesDependenciasPKDAO)
        {
            ValidateKeys.ValidarNoExistenciaKey(ageAplicacionesDependenciasPKDAO, 
                $"Aplicación dependencia con orden: {ageAplicacionesDependenciasPKDAO.OrdenDependencia} ya existe.",
                _repository.ConsultarCompletePorId);
        }

        private void ValidarFKs(int codigo, string mensaje)
        {
            mensaje += $" {codigo} no existe.";
            ValidateKeys.ValidarExistenciaKey(codigo, mensaje, _repositoryAplica.ConsultarCompletePorId);
        }
    }
}
