﻿using AGE.Entities.DAO.AgeAplicacionesDependencias;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;

namespace AGE.Services.AgeAplicacionesDependenciasService
{
    public interface IAgeAplicacionesDependenciasService
    {
        Task<Page<AgeAplicacionesDependenciasDAO>> ConsultarTodos(
            Pageable pageable);

        Task<Page<AgeAplicacionesDependenciasDAO>> ConsultarListaFiltro(
            string filtro,
            Pageable pageable);

        Task<AgeAplicacionesDependenciasDAO> ConsultarPorId(AgeAplicacionesDependenciasPKDAO AgeAplicacionesDependenciasPKDAO);

        Task<Resource<AgeAplicacionesDependenciasDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgeAplicacionesDependenciasSaveDAO AgeAplicacionesDependenciasSaveDAO);

        Task<List<Resource<AgeAplicacionesDependenciasDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeAplicacionesDependenciasSaveDAO> AgeAplicacionesDependenciasSaveDAOList);

        Task<AgeAplicacionesDependenciasDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeAplicacionesDependenciasSaveDAO AgeAplicacionesDependenciasSaveDAO);

        Task<List<AgeAplicacionesDependenciasDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeAplicacionesDependenciasSaveDAO> AgeAplicacionesDependenciasSaveDAOList);
    }
}
