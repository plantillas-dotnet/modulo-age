﻿using AGE.Entities;
using AGE.Entities.DAO.AgeMonedasCotizaciones;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;

namespace AGE.Services.AgeMonedasCotizacionesService
{
    public interface IAgeMonedasCotizacionesService
    {
        Task<Page<AgeMonedasCotizacionesDAO>> ConsultarTodos(
           int codigoLicenciatario,
           int codigo,
           int ageMonedaCodigo,
           int ageMonedaCodigoSerA,
           Pageable pageable);
        Task<Page<AgeMonedasCotizacionesDAO>> ConsultarListaFiltro(
            int codigoLicenciatario,
            string filtro,
            Pageable pageable);
        Task<AgeMonedasCotizacionesDAO> ConsultarPorId(
            AgeMonedasCotizacionesPKDAO ageMonedasCotizacionesPKDAO);
        AgeMonedasCotizaciones ConsultarCompletePorId(
            AgeMonedasCotizacionesPKDAO ageMonedasCotizacionesPKDAO);
        Task<Resource<AgeMonedasCotizacionesDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgeMonedasCotizacionesSaveDAO ageMonedasCotizacionesSaveDAO);
        Task<List<Resource<AgeMonedasCotizacionesDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeMonedasCotizacionesSaveDAO> ageMonedasCotizacionesSaveDAOList);
        Task<AgeMonedasCotizacionesDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeMonedasCotizacionesSaveDAO ageMonedasCotizacionesSaveDAO);
        Task<List<AgeMonedasCotizacionesDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeMonedasCotizacionesSaveDAO> ageMonedasCotizacionesSaveDAOList);
    }
}
