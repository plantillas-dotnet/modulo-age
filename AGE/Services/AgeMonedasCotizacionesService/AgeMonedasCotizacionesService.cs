﻿using AGE.Controllers;
using AGE.Entities;
using AGE.Entities.DAO.AgeMonedasCotizaciones;
using AGE.Middleware.Exceptions.BadRequest;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Repositories.AgeLicenciatariosRepository;
using AGE.Repositories.AgeMonedasCotizacionesRepository;
using AGE.Repositories.AgeMonedasRepository;
using AGE.Services.AgeLicenciatariosAplicaSecuService;
using AGE.Utils;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using System.ComponentModel.DataAnnotations;
using System.Transactions;

namespace AGE.Services.AgeMonedasCotizacionesService
{
    public class AgeMonedasCotizacionesService : IAgeMonedasCotizacionesService
    {
        private readonly IAgeMonedasCotizacionesRepository _repository;
        private readonly IAgeLicenciatariosRepository _repositoryLicenciatario;
        private readonly IAgeMonedasRepository _repositoryMonedas;
        private readonly IAgeLicenciatariosAplicaSecuService _ageLicenciatariosAplicaSecuService;

        public AgeMonedasCotizacionesService(
            IAgeMonedasCotizacionesRepository Repository,
            IAgeMonedasRepository RepositoryMoneda,
            IAgeLicenciatariosRepository RepositoryLicenciatario,
            IAgeLicenciatariosAplicaSecuService ageLicenciatariosAplicaSecuService)
        {
            _repository = Repository;
            _repositoryLicenciatario = RepositoryLicenciatario;
            _repositoryMonedas = RepositoryMoneda;
            _ageLicenciatariosAplicaSecuService = ageLicenciatariosAplicaSecuService;
        }

        public Task<Page<AgeMonedasCotizacionesDAO>> ConsultarTodos(
            int codigoLicenciatario,
            int Codigo,
            int ageMonedaCodigo,
            int ageMonedaCodigoSerA,
            Pageable pageable)
        {
            if (codigoLicenciatario <= 0)
                throw new InvalidFieldException("El código de licenciatario debe ser un número entero mayor a cero.");

            pageable.Validar<AgeMonedasCotizacionesDAO>();

            return _repository.ConsultarTodos(codigoLicenciatario, Codigo,
                ageMonedaCodigo,ageMonedaCodigoSerA, pageable);
        }

        public Task<AgeMonedasCotizacionesDAO> ConsultarPorId(
            AgeMonedasCotizacionesPKDAO ageMonedasCotizacionesPKDAO)
        {

            Validator.ValidateObject(ageMonedasCotizacionesPKDAO, new ValidationContext(ageMonedasCotizacionesPKDAO), true);

            AgeMonedasCotizacionesDAO ageMonedasCotizacionesDAO = _repository.ConsultarPorId(ageMonedasCotizacionesPKDAO).Result;

            return ageMonedasCotizacionesDAO == null
                ? throw new RegisterNotFoundException("Moneda Cotización con código: " + ageMonedasCotizacionesPKDAO.Codigo + " no existe.")
                : Task.FromResult(ageMonedasCotizacionesDAO);
        }

        public AgeMonedasCotizaciones ConsultarCompletePorId(
            AgeMonedasCotizacionesPKDAO ageMonedasCotizacionesPKDAO)
        {
            Validator.ValidateObject(ageMonedasCotizacionesPKDAO, new ValidationContext(ageMonedasCotizacionesPKDAO), true);

            AgeMonedasCotizaciones ageMonedasCotizaciones = _repository.ConsultarCompletePorId(ageMonedasCotizacionesPKDAO).Result;

            return ageMonedasCotizaciones;
        }

        public Task<Page<AgeMonedasCotizacionesDAO>> ConsultarListaFiltro(
            int codigoLicenciatario,
            string filtro,
            Pageable pageable)
        {
            if (codigoLicenciatario == 0)
                throw new InvalidFieldException("El código de licenciatario debe ser un número entero mayor a cero.");

            if (string.IsNullOrWhiteSpace(filtro))
                throw new InvalidFieldException("El filtro es requerido.");

            pageable.Validar<AgeMonedasCotizacionesDAO>();

            return _repository.ConsultarListaFiltro(codigoLicenciatario, filtro.ToLower(), pageable);
        }

        public async Task<Resource<AgeMonedasCotizacionesDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgeMonedasCotizacionesSaveDAO ageMonedasCotizacionesSaveDAO)
        {
            
            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    AgeMonedasCotizaciones ageMonedasCotizaciones = ValidarInsert(_httpContextAccessor, ageMonedasCotizacionesSaveDAO);

                    AgeMonedasCotizacionesDAO ageMonedasCotizacionesDAO = await _repository.Insertar(ageMonedasCotizaciones);
                    
                    Resource<AgeMonedasCotizacionesDAO> ageMonedasCotizacionesDAOWithResource = GetDataWithResource(_httpContextAccessor, ageMonedasCotizacionesDAO);

                    transactionScope.Complete();

                    return ageMonedasCotizacionesDAOWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public async Task<List<Resource<AgeMonedasCotizacionesDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeMonedasCotizacionesSaveDAO> ageMonedasCotizacionesSaveDAOList)
        {
            if (ageMonedasCotizacionesSaveDAOList == null)
                throw new InvalidSintaxisException();

            List<AgeMonedasCotizaciones> ageMonedasCotizacionesList = new List<AgeMonedasCotizaciones>();

            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    foreach (AgeMonedasCotizacionesSaveDAO ageMonedasCotizacionesSaveDAO in ageMonedasCotizacionesSaveDAOList)
                    {
                        AgeMonedasCotizaciones ageMonedasCotizacione = ValidarInsert(_httpContextAccessor, ageMonedasCotizacionesSaveDAO);
                        ageMonedasCotizacionesList.Add(ageMonedasCotizacione);
                    }

                    List<AgeMonedasCotizacionesDAO> ageMonedasCotizacionesDAOList = await _repository.InsertarVarios(ageMonedasCotizacionesList);

                    List<Resource<AgeMonedasCotizacionesDAO>> ageMonedasCotizacionesDAOListWithResource = new List<Resource<AgeMonedasCotizacionesDAO>>();

                    foreach (AgeMonedasCotizacionesDAO ageMonedasCotizacionesDAO in ageMonedasCotizacionesDAOList)
                    {
                        ageMonedasCotizacionesDAOListWithResource.Add(GetDataWithResource(_httpContextAccessor, ageMonedasCotizacionesDAO));
                    }

                    transactionScope.Complete();

                    return ageMonedasCotizacionesDAOListWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public Task<AgeMonedasCotizacionesDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeMonedasCotizacionesSaveDAO ageMonedasCotizacionesSaveDAO)
        {
            AgeMonedasCotizaciones ageMonedasCotizaciones = ValidarUpdate(_httpContextAccessor, ageMonedasCotizacionesSaveDAO);

            return _repository.Actualizar(ageMonedasCotizaciones);
        }

        public Task<List<AgeMonedasCotizacionesDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeMonedasCotizacionesSaveDAO> ageMonedasCotizacionesSaveDAOList)
        {
            if (ageMonedasCotizacionesSaveDAOList == null)
                throw new InvalidSintaxisException();

            ValidateKeys.ValidarPKDuplicadas(ageMonedasCotizacionesSaveDAOList, p => new { p.Id.AgeLicencCodigo, p.Id.Codigo }, "Id");

            List<AgeMonedasCotizaciones> ageMonedasCotizacionesList = new List<AgeMonedasCotizaciones>();
            AgeMonedasCotizaciones ageMonedasCotizaciones;

            foreach (AgeMonedasCotizacionesSaveDAO ageMonedasCotizacionesSaveDAO in ageMonedasCotizacionesSaveDAOList)
            {
                ageMonedasCotizaciones = ValidarUpdate(_httpContextAccessor, ageMonedasCotizacionesSaveDAO);
                ageMonedasCotizacionesList.Add(ageMonedasCotizaciones);
            }

            return _repository.ActualizarVarios(ageMonedasCotizacionesList);
        }

        private static Resource<AgeMonedasCotizacionesDAO> GetDataWithResource(
            IHttpContextAccessor _httpContextAccessor,
            AgeMonedasCotizacionesDAO ageMonedasCotizacionesDAO)
        {
            string rutaSegmentoFinal = $"{ageMonedasCotizacionesDAO.Id.AgeLicencCodigo}/{ageMonedasCotizacionesDAO.Id.Codigo}";

            return Resource<AgeMonedasCotizacionesDAO>.GetDataWithResource<AgeMonedasCotizacionesController>(
                _httpContextAccessor, rutaSegmentoFinal, ageMonedasCotizacionesDAO);
        }

        private AgeMonedasCotizaciones ValidarInsert(
            IHttpContextAccessor _httpContextAccessor,
            AgeMonedasCotizacionesSaveDAO ageMonedasCotizacionesSaveDAO)
        {

            if (ageMonedasCotizacionesSaveDAO == null)
                throw new InvalidSintaxisException();

            Validator.ValidateObject(ageMonedasCotizacionesSaveDAO, new ValidationContext(ageMonedasCotizacionesSaveDAO), true);

            if (ageMonedasCotizacionesSaveDAO.FechaDesde == default)
                throw new InvalidFieldException("La fecha desde no puede ser nula.");

            if (ageMonedasCotizacionesSaveDAO.FechaHasta == default)
                throw new InvalidFieldException("La fecha hasta no puede ser nula.");

            FuncionesSecuencias fg = new FuncionesSecuencias(_ageLicenciatariosAplicaSecuService);

            ageMonedasCotizacionesSaveDAO.Id.Codigo = fg.ObtenerSecuencia(_httpContextAccessor,
                ageMonedasCotizacionesSaveDAO.Id.AgeLicencCodigo,
                Globales.CODIGO_SECUENCIA_MONEDA_COTIZACION,
                ageMonedasCotizacionesSaveDAO.UsuarioIngreso);

            ValidarKeys(ageMonedasCotizacionesSaveDAO);

             AgeMonedasCotizaciones entityObject = FromDAOToEntity(_httpContextAccessor,ageMonedasCotizacionesSaveDAO);

            Validator.ValidateObject(entityObject, new ValidationContext(entityObject), true);

            return entityObject;
        }

        private AgeMonedasCotizaciones ValidarUpdate(
            IHttpContextAccessor _httpContextAccessor,
            AgeMonedasCotizacionesSaveDAO ageMonedasCotizacionesSaveDAO)
        {
            if (ageMonedasCotizacionesSaveDAO == null)
                throw new InvalidSintaxisException();

            if (ageMonedasCotizacionesSaveDAO.UsuarioModificacion == null || ageMonedasCotizacionesSaveDAO.UsuarioModificacion <= 0)
                throw new InvalidFieldException("El usuario de modificación debe ser un número entero mayor a cero.");

            ValidarFKS(ageMonedasCotizacionesSaveDAO);

            AgeMonedasCotizaciones ageMonedasCotizaciones = ConsultarCompletePorId(ageMonedasCotizacionesSaveDAO.Id) 
                ?? throw new RegisterNotFoundException("Moneda Cotización con código: " + ageMonedasCotizacionesSaveDAO.Id.Codigo + " no existe.");
            
            if (!string.IsNullOrWhiteSpace(ageMonedasCotizacionesSaveDAO.Estado))
            {
                ageMonedasCotizaciones.Estado = ageMonedasCotizacionesSaveDAO.Estado;
                ageMonedasCotizaciones.FechaEstado = DateTime.Now;
            }

            if(ageMonedasCotizacionesSaveDAO.AgeMonedaCodigo > 0)
                ageMonedasCotizaciones.AgeMonedaCodigo = ageMonedasCotizacionesSaveDAO.AgeMonedaCodigo;

            if (ageMonedasCotizacionesSaveDAO.AgeMonedaCodigoSerA > 0)
                ageMonedasCotizaciones.AgeMonedaCodigoSerA = ageMonedasCotizacionesSaveDAO.AgeMonedaCodigoSerA;

            if (ageMonedasCotizacionesSaveDAO.CotizacionCompra > 0)
                ageMonedasCotizaciones.CotizacionCompra = ageMonedasCotizacionesSaveDAO.CotizacionCompra;

            if (ageMonedasCotizacionesSaveDAO.CotizacionMercado > 0)
                ageMonedasCotizaciones.CotizacionMercado = ageMonedasCotizacionesSaveDAO.CotizacionMercado;

            if (ageMonedasCotizacionesSaveDAO.CotizacionVenta > 0)
                ageMonedasCotizaciones.CotizacionVenta = ageMonedasCotizacionesSaveDAO.CotizacionVenta;
            
            if (ageMonedasCotizacionesSaveDAO.FechaDesde != default) 
                ageMonedasCotizaciones.FechaDesde = ageMonedasCotizacionesSaveDAO.FechaDesde;

            if(ageMonedasCotizacionesSaveDAO.FechaHasta != default)
                ageMonedasCotizaciones.FechaHasta = ageMonedasCotizacionesSaveDAO.FechaHasta;

            if (!string.IsNullOrWhiteSpace(ageMonedasCotizacionesSaveDAO.ObservacionEstado))
                ageMonedasCotizaciones.ObservacionEstado = ageMonedasCotizacionesSaveDAO.ObservacionEstado;

            ageMonedasCotizaciones.FechaModificacion = DateTime.Now;
            ageMonedasCotizaciones.UbicacionModificacion = Ubicacion.getIpAdress(_httpContextAccessor);
            ageMonedasCotizaciones.UsuarioModificacion = ageMonedasCotizacionesSaveDAO.UsuarioModificacion;

            Validator.ValidateObject(ageMonedasCotizaciones, new ValidationContext(ageMonedasCotizaciones), true);

            return ageMonedasCotizaciones;
        }

        private AgeMonedasCotizaciones FromDAOToEntity(
           IHttpContextAccessor _httpContextAccessor,
           AgeMonedasCotizacionesSaveDAO ageMonedasCotizacionesSaveDAO)
        {

            return new AgeMonedasCotizaciones
            {
                AgeLicencCodigo = ageMonedasCotizacionesSaveDAO.Id.AgeLicencCodigo,
                Codigo = ageMonedasCotizacionesSaveDAO.Id.Codigo,
                AgeMonedaCodigo = ageMonedasCotizacionesSaveDAO.AgeMonedaCodigo,
                AgeMonedaCodigoSerA = ageMonedasCotizacionesSaveDAO.AgeMonedaCodigoSerA,
                FechaDesde = ageMonedasCotizacionesSaveDAO.FechaDesde,
                FechaHasta = ageMonedasCotizacionesSaveDAO.FechaHasta,
                CotizacionMercado = ageMonedasCotizacionesSaveDAO.CotizacionMercado,
                CotizacionCompra = ageMonedasCotizacionesSaveDAO.CotizacionCompra,
                CotizacionVenta = ageMonedasCotizacionesSaveDAO.CotizacionVenta,
                Estado = ageMonedasCotizacionesSaveDAO.Estado,
                FechaEstado = DateTime.Now,
                FechaIngreso = DateTime.Now,
                ObservacionEstado = ageMonedasCotizacionesSaveDAO.ObservacionEstado,
                UbicacionIngreso = Ubicacion.getIpAdress(_httpContextAccessor),
                UsuarioIngreso = ageMonedasCotizacionesSaveDAO.UsuarioIngreso
            };
        }

        private void ValidarKeys(AgeMonedasCotizacionesSaveDAO ageMonedasCotizacionesSaveDAO)
        {
            ValidarPK(ageMonedasCotizacionesSaveDAO.Id);
            ValidarFKS(ageMonedasCotizacionesSaveDAO);
        }

        private void ValidarPK(AgeMonedasCotizacionesPKDAO ageMonedasCotizacionesPKDAO)
        {
            ValidateKeys.ValidarNoExistenciaKey(ageMonedasCotizacionesPKDAO,
                    $"Moneda Cotización con código: {ageMonedasCotizacionesPKDAO.Codigo}" +
                    $" y código de licenciatario: {ageMonedasCotizacionesPKDAO.AgeLicencCodigo} ya existe."
                    , _repository.ConsultarCompletePorId);
        }

        private void ValidarFKS(AgeMonedasCotizacionesSaveDAO ageMonedasCotizacionesSaveDAO)
        {
            if (ageMonedasCotizacionesSaveDAO.AgeMonedaCodigo > 0)
            {
                ValidarFKMonedas(ageMonedasCotizacionesSaveDAO.AgeMonedaCodigo, "Moneda con código: ");
            }

            if (ageMonedasCotizacionesSaveDAO.AgeMonedaCodigoSerA > 0)
            {
                ValidarFKMonedas(ageMonedasCotizacionesSaveDAO.AgeMonedaCodigoSerA, "Moneda Ser con código: ");
            }

            ValidarFKLicenciatario(ageMonedasCotizacionesSaveDAO.Id.AgeLicencCodigo);
        }

        private void ValidarFKMonedas(int Codigo, string mensaje)
        {
            mensaje += $" {Codigo} no existe.";
            ValidateKeys.ValidarExistenciaKey(Codigo, mensaje, _repositoryMonedas.ConsultarCompletePorId);
        }

        private void ValidarFKLicenciatario(int codigoLicenciatario)
        {
            ValidateKeys.ValidarExistenciaKey(codigoLicenciatario,
                $"Licenciatario con código: {codigoLicenciatario} no existe."
                , _repositoryLicenciatario.ConsultarCompletePorId);
        }
    }
}
