using AGE.Controllers;
using AGE.Entities;
using AGE.Entities.DAO.AgeUsuarios;
using AGE.Entities.DAO.AgeSucursales;
using AGE.Middleware.Exceptions.BadRequest;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Repositories.AgeLicenciatariosRepository;
using AGE.Repositories.AgeSucursalesRepository;
using AGE.Repositories.AgeUsuarioRepository;
using AGE.Services.AgeLicenciatariosAplicaSecuService;
using AGE.Utils;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using System.ComponentModel.DataAnnotations;
using System.Transactions;
using AGE.Repositories.AgeTiposIdentificacioneRepository;
using AGE.Entities.DAO.AgePersonas;
using AGE.Services.AgePersonasService;

namespace AGE.Services.AgeUsuarioService
{
    public class AgeUsuariosService : IAgeUsuariosService
    {

        private readonly IAgeUsuariosRepository _repository;
        private readonly IAgeSucursalesRepository _repositorySucursales;
        private readonly IAgeLicenciatariosRepository _repositoryLicenciatarios;
        private readonly IAgeTiposIdentificacionesRepository _repositoryTipoIdentificacion;
        private readonly IAgeLicenciatariosAplicaSecuService _ageLicenciatariosAplicaSecuService;
        private readonly IAgePersonasService _agePersonasService;

        public AgeUsuariosService(
            IAgeUsuariosRepository Repository,
            IAgeSucursalesRepository RepositorySucursales,
            IAgeLicenciatariosRepository RepositoryLicenciatarios,
            IAgeTiposIdentificacionesRepository RepositoryTipoIdentif,
            IAgeLicenciatariosAplicaSecuService ageLicenciatariosAplicaSecuService,
            IAgePersonasService agePersonasService)
        {
            _repository = Repository;
            _repositoryLicenciatarios = RepositoryLicenciatarios;
            _repositorySucursales = RepositorySucursales;
            _repositoryTipoIdentificacion = RepositoryTipoIdentif;
            _ageLicenciatariosAplicaSecuService = ageLicenciatariosAplicaSecuService;
            _agePersonasService = agePersonasService;
        }

        public Task<Page<AgeUsuarioDAO>> ConsultarTodos(
            int codigoLicenciatario, 
            string codigoExterno, 
            Pageable pageable)
        {
            if (codigoLicenciatario <= 0)
                throw new InvalidFieldException("El código de licenciatario debe ser un número entero mayor a cero.");

            pageable.Validar<AgeUsuarioDAO>();

            return _repository.ConsultarTodos(codigoLicenciatario, codigoExterno, pageable);
        }

        public Task<Page<AgeUsuarioDAO>> ConsultarListaFiltro(
            int codigoLicenciatario, 
            string filtro, 
            Pageable pageable)
        {
            if (codigoLicenciatario == 0)
                throw new InvalidFieldException("El código de licenciatario debe ser un número entero mayor a cero.");

            if (string.IsNullOrWhiteSpace(filtro))
                throw new InvalidFieldException("El filtro es requerido.");

            pageable.Validar<AgeUsuarioDAO>();

            return _repository.ConsultarListaFiltro(codigoLicenciatario, filtro.ToLower(), pageable);
        }

        public Task<AgeUsuarioDAO> ConsultarPorId(
            AgeUsuarioPKDAO ageUsuarioPKDAO)
        {
            Validator.ValidateObject(ageUsuarioPKDAO, new ValidationContext(ageUsuarioPKDAO), true);

            AgeUsuarioDAO? ageUsuariosDAO = _repository.ConsultarPorId(ageUsuarioPKDAO).Result;

            if (ageUsuariosDAO == null)
                throw new RegisterNotFoundException("Usuario con código " + ageUsuarioPKDAO.codigo + " no existe");

            return Task.FromResult(ageUsuariosDAO);
        }


        private AgeUsuarios ConsultarCompletePorId(
            AgeUsuarioPKDAO ageUsuarioPKDAO)
        {
            Validator.ValidateObject(ageUsuarioPKDAO, new ValidationContext(ageUsuarioPKDAO), true);

            AgeUsuarios? ageUsuarios = _repository.ConsultarCompletePorId(ageUsuarioPKDAO).Result;

            return ageUsuarios;
        }

        private List<AgeUsuarioDAO> ConsultarPorCodigoExterno(
            string codigoExterno)
        {
            if (string.IsNullOrWhiteSpace(codigoExterno))
                throw new InvalidFieldException("El código externo no puede ser nulo.");

            return _repository.ConsultarPorCodigoExterno(codigoExterno).Result;
        }

        private AgeUsuarios ConsultarPorIdentificacion(
            string identificacion)
        {
            return _repository.ConsultarPorIdentificacion(identificacion).Result;
        }

        public async Task<Resource<AgeUsuarioDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgeUsuarioSaveDAO ageUsuarioSaveDAO)
        {
            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    AgeUsuarios ageUsuario = ValidarInsert(_httpContextAccessor, ageUsuarioSaveDAO);

                    AgeUsuarioDAO ageUsuarioDAO = await _repository.Insertar(ageUsuario);

                    Resource<AgeUsuarioDAO> ageUsuariosDAOWithResource = GetDataWithResource(_httpContextAccessor, ageUsuarioDAO);
                    
                    transactionScope.Complete();
                    
                    return ageUsuariosDAOWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public async Task<List<Resource<AgeUsuarioDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor, 
            List<AgeUsuarioSaveDAO> ageUsuarioSaveDAOList)
        {
            if (ageUsuarioSaveDAOList == null)
                throw new InvalidSintaxisException();

            ValidateKeys.ValidarPKDuplicadas(ageUsuarioSaveDAOList,
                                             p => new { p.CodigoExterno },
                                             "Código externo");

            ValidateKeys.ValidarPKDuplicadas(ageUsuarioSaveDAOList,
                                             p => new { p.NumeroIdentificacion },
                                             "Número de identificación");

            List<AgeUsuarios> ageUsuariosList = new List<AgeUsuarios>();

            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    foreach (AgeUsuarioSaveDAO ageUsuarioSaveDAO in ageUsuarioSaveDAOList)
                    {
                        AgeUsuarios ageUsuario = ValidarInsert(_httpContextAccessor, ageUsuarioSaveDAO);
                        ageUsuariosList.Add(ageUsuario);
                    }

                    List<AgeUsuarioDAO> ageUsuariosDAOList = await _repository.InsertarVarios(ageUsuariosList);

                    List<Resource<AgeUsuarioDAO>> ageUsuariosDAOListWithResource = new List<Resource<AgeUsuarioDAO>>();

                    foreach (AgeUsuarioDAO ageUsuarioDAO in ageUsuariosDAOList)
                    {
                        ageUsuariosDAOListWithResource.Add(GetDataWithResource(_httpContextAccessor, ageUsuarioDAO));
                    }

                    transactionScope.Complete();

                    return ageUsuariosDAOListWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public Task<AgeUsuarioDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor, 
            AgeUsuarioSaveDAO ageUsuarioSaveDAO)
        {
            AgeUsuarios ageUsuario = ValidarUpdate(_httpContextAccessor, ageUsuarioSaveDAO);

            return _repository.Actualizar(ageUsuario);
        }

        public Task<List<AgeUsuarioDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor, 
            List<AgeUsuarioSaveDAO> ageUsuarioSaveDAOList)
        {
            if (ageUsuarioSaveDAOList == null)
                throw new InvalidSintaxisException();

            ValidateKeys.ValidarPKDuplicadas(ageUsuarioSaveDAOList,
                                             p => new { p.Id.ageLicencCodigo, p.Id.codigo },
                                             "Id");

            ValidateKeys.ValidarPKDuplicadas(ageUsuarioSaveDAOList,
                                             p => new { p.CodigoExterno },
                                             "Código externo");

            ValidateKeys.ValidarPKDuplicadas(ageUsuarioSaveDAOList,
                                             p => new { p.NumeroIdentificacion },
                                             "Número de identificación");

            List<AgeUsuarios> ageUsuariosList = new List<AgeUsuarios>();
            AgeUsuarios ageUsuario;

            foreach (AgeUsuarioSaveDAO ageRutasSaveDAO in ageUsuarioSaveDAOList)
            {
                ageUsuario = ValidarUpdate(_httpContextAccessor, ageRutasSaveDAO);
                ageUsuariosList.Add(ageUsuario);
            }

            return _repository.ActualizarVarios(ageUsuariosList);
        }

        public Task<AgeUsuariosLoginDAO> ActualizarDatosUsuario(
            IHttpContextAccessor _httpContextAccessor,
            AgeUsuariosLoginDAO ageUsuariosLoginDAO)
        {
            AgeUsuariosLoginDAO ageUsuarios = ValidarUpdateDatosUsuario(ageUsuariosLoginDAO);

            // Faltan las validaciones y el registro de la sesión.

            ageUsuarios.Clave = GenerarClaveEncriptada(ageUsuarios.Clave);

            return Task.FromResult(ageUsuarios);
        }

        private static Resource<AgeUsuarioDAO> GetDataWithResource(
            IHttpContextAccessor _httpContextAccessor, 
            AgeUsuarioDAO ageUsuariosDAO)
        {
            string rutaSegmentoFinal = $"{ageUsuariosDAO.Id.ageLicencCodigo}/{ageUsuariosDAO.Id.codigo}";

            return Resource<AgeUsuarioDAO>.GetDataWithResource<AgeUsuariosController>(
                _httpContextAccessor, rutaSegmentoFinal, ageUsuariosDAO);
        }

        private AgeUsuarios ValidarInsert(
            IHttpContextAccessor _httpContextAccessor, 
            AgeUsuarioSaveDAO ageUsuarioSaveDAO)
        {

            if (ageUsuarioSaveDAO == null)
                throw new InvalidSintaxisException();

            Validator.ValidateObject(ageUsuarioSaveDAO, new ValidationContext(ageUsuarioSaveDAO), true);

            FuncionesSecuencias fg = new FuncionesSecuencias(_ageLicenciatariosAplicaSecuService);

            ageUsuarioSaveDAO.Id.codigo = fg.ObtenerSecuencia(_httpContextAccessor,
                                                                ageUsuarioSaveDAO.Id.ageLicencCodigo,
                                                                Globales.CODIGO_SECUENCIA_USUARIO,
                                                                ageUsuarioSaveDAO.UsuarioIngreso);

            if (ConsultarPorCodigoExterno(ageUsuarioSaveDAO.CodigoExterno).Count >= 1)
                throw new UniqueFieldException("Código externo", "Usuario " + ageUsuarioSaveDAO.CodigoExterno + " ya existe.");

            if (ConsultarPorIdentificacion(ageUsuarioSaveDAO.NumeroIdentificacion) != null)
                throw new UniqueFieldException("Identificación", "Usuario con identificación " + ageUsuarioSaveDAO.NumeroIdentificacion + " ya existe.");

            ValidarKeys(ageUsuarioSaveDAO, _httpContextAccessor);

            if ((ageUsuarioSaveDAO.AgePersonAgeLicencCodigo != null) &&
                (ageUsuarioSaveDAO.AgePersonCodigo == null || ageUsuarioSaveDAO.AgePersonCodigo == 0))
            {
                AgePersonasDAO agePersonaDAO = GuardarPersona(_httpContextAccessor, ageUsuarioSaveDAO).Result;
                ageUsuarioSaveDAO.AgePersonCodigo = agePersonaDAO.Id.codigo;
            }

            AgeUsuarios entityObject = FromDAOToEntity(_httpContextAccessor, ageUsuarioSaveDAO);

            Validator.ValidateObject(entityObject, new ValidationContext(entityObject), true);

            return entityObject;
        }

        private AgeUsuarios ValidarUpdate(
            IHttpContextAccessor _httpContextAccessor,
            AgeUsuarioSaveDAO ageUsuarioSaveDAO)
        {
            if (ageUsuarioSaveDAO == null)
                throw new InvalidSintaxisException();

            if (ageUsuarioSaveDAO.UsuarioModificacion == null || ageUsuarioSaveDAO.UsuarioModificacion <= 0)
                throw new InvalidFieldException("El usuario de modificación debe ser un número entero mayor a cero.");

            AgeUsuarios? ageUsuario = ConsultarCompletePorId(new AgeUsuarioPKDAO
            {
                ageLicencCodigo = ageUsuarioSaveDAO.Id.ageLicencCodigo,
                codigo = ageUsuarioSaveDAO.Id.codigo
            }) ?? throw new RegisterNotFoundException("Usuario con código " + ageUsuarioSaveDAO.Id.codigo + " no existe");

            if (!string.IsNullOrWhiteSpace(ageUsuarioSaveDAO.CodigoExterno))
            {
                if (ConsultarPorCodigoExterno(ageUsuarioSaveDAO.CodigoExterno).Count == 1 &&
                !ageUsuario.CodigoExterno.Equals(ageUsuarioSaveDAO.CodigoExterno))
                throw new UniqueFieldException("Código externo", "Usuario " + ageUsuarioSaveDAO.CodigoExterno + " ya existe.");
            }

            if (!string.IsNullOrWhiteSpace(ageUsuarioSaveDAO.NumeroIdentificacion))
            {
                if (ConsultarPorIdentificacion(ageUsuarioSaveDAO.NumeroIdentificacion) != null &&
                !ageUsuario.NumeroIdentificacion.Equals(ageUsuarioSaveDAO.NumeroIdentificacion))
                    throw new UniqueFieldException("Identificación", "Usuario con identificación " + ageUsuarioSaveDAO.NumeroIdentificacion + " ya existe.");
            }

            if (ageUsuarioSaveDAO.AgePersonAgeLicencCodigo != null &&
                ageUsuarioSaveDAO.AgePersonCodigo != null)
                _ = ActualizarPersona(_httpContextAccessor, ageUsuarioSaveDAO);

            if (ageUsuarioSaveDAO.AgeSucursCodigo > 0 ||
                ageUsuarioSaveDAO.AgeSucursAgeLicencCodigo > 0)
            {
                ageUsuario.AgeSucursCodigo = ageUsuarioSaveDAO.AgeSucursCodigo;
                ValidarFKSucursal(ageUsuario.AgeSucursCodigo, ageUsuario.AgeSucursAgeLicencCodigo);
            }

            if (ageUsuarioSaveDAO.AgeTipIdCodigo > 0)
                ValidarFKTipoIdentificacion(ageUsuarioSaveDAO.AgeTipIdCodigo);

            if (!string.IsNullOrWhiteSpace(ageUsuarioSaveDAO.Estado))
            {
                ageUsuario.Estado = ageUsuarioSaveDAO.Estado;
                ageUsuario.FechaEstado = DateTime.Now;
            }

            if (!string.IsNullOrWhiteSpace(ageUsuarioSaveDAO.CodigoExterno))
                ageUsuario.CodigoExterno = ageUsuarioSaveDAO.CodigoExterno;

            if (!string.IsNullOrWhiteSpace(ageUsuarioSaveDAO.ArchivoFoto))
                ageUsuario.ArchivoFoto = ageUsuarioSaveDAO.ArchivoFoto;

            if (!string.IsNullOrWhiteSpace(ageUsuarioSaveDAO.NumeroIdentificacion))
                ageUsuario.NumeroIdentificacion = ageUsuarioSaveDAO.NumeroIdentificacion;

            if (!string.IsNullOrWhiteSpace(ageUsuarioSaveDAO.Nombres))
                ageUsuario.Nombres = ageUsuarioSaveDAO.Nombres;

            if (!string.IsNullOrWhiteSpace(ageUsuarioSaveDAO.Apellidos))
                ageUsuario.Apellidos = ageUsuarioSaveDAO.Apellidos;

            if (!string.IsNullOrWhiteSpace(ageUsuarioSaveDAO.MailPrincipal))
                ageUsuario.MailPrincipal = ageUsuarioSaveDAO.MailPrincipal;

            if (!string.IsNullOrWhiteSpace(ageUsuarioSaveDAO.TelefonoCelular))
                ageUsuario.TelefonoCelular = ageUsuarioSaveDAO.TelefonoCelular;

            if (!string.IsNullOrWhiteSpace(ageUsuarioSaveDAO.TipoUsuario))
                ageUsuario.TipoUsuario = ageUsuarioSaveDAO.TipoUsuario;

            if (!string.IsNullOrWhiteSpace(ageUsuarioSaveDAO.Clave))
                ageUsuario.Clave = GenerarClaveEncriptada(ageUsuarioSaveDAO.Clave);
            
            if (!string.IsNullOrWhiteSpace(ageUsuarioSaveDAO.ObservacionEstado))
                ageUsuario.ObservacionEstado = ageUsuarioSaveDAO.ObservacionEstado;

            ageUsuario.FechaModificacion = DateTime.Now;
            ageUsuario.UbicacionModificacion = Ubicacion.getIpAdress(_httpContextAccessor);
            ageUsuario.UsuarioModificacion = ageUsuarioSaveDAO.UsuarioModificacion;

            Validator.ValidateObject(ageUsuario, new ValidationContext(ageUsuario), true);

            return ageUsuario;
        }

        private AgeUsuariosLoginDAO ValidarUpdateDatosUsuario(AgeUsuariosLoginDAO ageUsuariosLoginDAO)
        {
            if (ageUsuariosLoginDAO == null)
                throw new InvalidSintaxisException();

            Validator.ValidateObject(ageUsuariosLoginDAO, new ValidationContext(ageUsuariosLoginDAO), true);

            List<AgeUsuarioDAO> ageUsuarios = ConsultarPorCodigoExterno(ageUsuariosLoginDAO.CodigoExterno) ?? throw new InvalidaUserOrPasswordException();

            if (ageUsuarios == null || ageUsuarios.Count == 0)
                throw new InvalidaUserOrPasswordException();

            if (!BCrypt.Net.BCrypt.Verify(ageUsuariosLoginDAO.Clave, ageUsuarios[0].Clave))
                throw new InvalidaUserOrPasswordException();

            if (ageUsuarios.Count > 1)
                throw new UserDuplicateException(ageUsuarios[0].CodigoExterno);

           ageUsuariosLoginDAO.Clave = GenerarClaveEncriptada(ageUsuariosLoginDAO.Clave);

            return ageUsuariosLoginDAO;
        }


        private async Task<AgePersonasDAO> GuardarPersona(
            IHttpContextAccessor _httpContextAccessor, 
            AgeUsuarioSaveDAO ageUsuarioSaveDAO)
        {

            AgePersonasSaveDAO agePersonasSaveDAO = new AgePersonasSaveDAO
            {
                Id = new AgePersonasPKDAO
                {
                    ageLicencCodigo = ageUsuarioSaveDAO.AgePersonAgeLicencCodigo ?? 0,
                    codigo = ageUsuarioSaveDAO.AgePersonCodigo ?? 0
                },
                AgeTipIdCodigo = ageUsuarioSaveDAO.AgeTipIdCodigo,
                NumeroIdentificacion = ageUsuarioSaveDAO.NumeroIdentificacion,
                Nombres = ageUsuarioSaveDAO.Nombres,
                Apellidos = ageUsuarioSaveDAO.Apellidos,
                AgeLocaliAgeTipLoCodigo = ageUsuarioSaveDAO.AgeLocaliAgeTipLoCodigo,
                CorreoElectronico = ageUsuarioSaveDAO.MailPrincipal,
                DireccionPrincipal = ageUsuarioSaveDAO.Direccion,
                Estado = ageUsuarioSaveDAO.Estado,
                UsuarioIngreso = ageUsuarioSaveDAO.UsuarioIngreso,
                AgeLocaliCodigo = ageUsuarioSaveDAO.AgeLocaliCodigo,
                AgeAgeTipLoAgePaisCodigo = ageUsuarioSaveDAO.AgeAgeTipLoAgePaisCodigo
            };

            return await _agePersonasService.ValidarYGuardarPersona(_httpContextAccessor, agePersonasSaveDAO);
            
        }


        private async Task<AgePersonasDAO> ActualizarPersona(
            IHttpContextAccessor _httpContextAccessor,
            AgeUsuarioSaveDAO ageUsuarioSaveDAO)
        {

            AgePersonasSaveDAO agePersonasSaveDAO = new AgePersonasSaveDAO
            {
                Id = new AgePersonasPKDAO
                {
                    ageLicencCodigo = ageUsuarioSaveDAO.AgePersonAgeLicencCodigo ?? 0,
                    codigo = ageUsuarioSaveDAO.AgePersonCodigo ?? 0
                },
                DireccionPrincipal = ageUsuarioSaveDAO.Direccion,
                UsuarioModificacion = ageUsuarioSaveDAO.UsuarioModificacion,
                AgeLocaliCodigo = ageUsuarioSaveDAO.AgeLocaliCodigo,
                AgeAgeTipLoAgePaisCodigo = ageUsuarioSaveDAO.AgeAgeTipLoAgePaisCodigo,
                AgeLocaliAgeTipLoCodigo = ageUsuarioSaveDAO.AgeLocaliAgeTipLoCodigo,
            };

            return await _agePersonasService.Actualizar(_httpContextAccessor, agePersonasSaveDAO);

        }


        private void ValidarKeys(AgeUsuarioSaveDAO ageUsuarioSaveDAO, IHttpContextAccessor _httpContextAccessor)
        {
            ValidarPK(ageUsuarioSaveDAO.Id);

            ValidarFKLicenciatario(ageUsuarioSaveDAO.Id.ageLicencCodigo);

            if (ageUsuarioSaveDAO.AgePersonAgeLicencCodigo != null &&
                ageUsuarioSaveDAO.AgePersonCodigo > 0)
                ValidarFKPersona(ageUsuarioSaveDAO.AgePersonCodigo ?? 0, ageUsuarioSaveDAO.AgePersonAgeLicencCodigo ?? 0);

            ValidarFKSucursal(ageUsuarioSaveDAO.AgeSucursCodigo, ageUsuarioSaveDAO.AgeSucursAgeLicencCodigo);

            ValidarFKTipoIdentificacion(ageUsuarioSaveDAO.AgeTipIdCodigo);
        }

        private void ValidarPK(AgeUsuarioPKDAO ageUsuarioPKDAO)
        {

            ValidateKeys.ValidarNoExistenciaKey(
               ageUsuarioPKDAO,
               $"Usuario con código {ageUsuarioPKDAO.codigo} ya existe.",
               _repository.ConsultarCompletePorId);
        }

        private void ValidarFKSucursal(int codigoSucursal, int codigoLicenciatario)
        {
            AgeSucursalesPKDAO ageSucursalesPKDAO = new()
            {
                Codigo = codigoSucursal,
                AgeLicencCodigo = codigoLicenciatario
            };

            string mensaje = $"Sucursal con código: {ageSucursalesPKDAO.Codigo}" +
                            $" y código licenciatario: {ageSucursalesPKDAO.AgeLicencCodigo} no existe.";
            ValidateKeys.ValidarExistenciaKey(ageSucursalesPKDAO, mensaje, _repositorySucursales.ConsultarCompletePorId);
        }

        private void ValidarFKLicenciatario(int codigoLicenciatario)
        {
            string mensaje = $"Licenciatario con código: {codigoLicenciatario} no existe.";
            ValidateKeys.ValidarExistenciaKey(codigoLicenciatario, mensaje, _repositoryLicenciatarios.ConsultarCompletePorId);
        }

        private void ValidarFKPersona(long codigoPersona, int codigoLicenciatario)
        {
            AgePersonasPKDAO agePersonasPKDAO = new()
            {
                codigo = codigoPersona,
                ageLicencCodigo = codigoLicenciatario
            };

            string mensaje = $"Persona con código: {agePersonasPKDAO.codigo}" +
                            $" y código licenciatario: {agePersonasPKDAO.ageLicencCodigo} no existe.";
            ValidateKeys.ValidarExistenciaKey(agePersonasPKDAO, mensaje, _agePersonasService.ConsultarPorId);
        }

        private void ValidarFKTipoIdentificacion(int codigo)
        {
            string mensaje = $"Tipo identificación con código: {codigo} no existe.";
            ValidateKeys.ValidarExistenciaKey(codigo, mensaje, _repositoryTipoIdentificacion.ConsultarCompletePorId);
        }

        private AgeUsuarios FromDAOToEntity(
            IHttpContextAccessor _httpContextAccessor,
            AgeUsuarioSaveDAO ageUsuarioSaveDAO)
        {
            return new AgeUsuarios
            {
                AgeLicencCodigo = ageUsuarioSaveDAO.Id.ageLicencCodigo,
                Codigo = ageUsuarioSaveDAO.Id.codigo,
                AgePersonAgeLicencCodigo = ageUsuarioSaveDAO.AgePersonAgeLicencCodigo,
                AgePersonCodigo = ageUsuarioSaveDAO.AgePersonCodigo,
                AgeSucursAgeLicencCodigo = ageUsuarioSaveDAO.AgeSucursAgeLicencCodigo,
                AgeSucursCodigo = ageUsuarioSaveDAO.AgeSucursCodigo,
                AgeTipIdCodigo = ageUsuarioSaveDAO.AgeTipIdCodigo,
                ArchivoFoto = ageUsuarioSaveDAO.ArchivoFoto,
                CodigoExterno = ageUsuarioSaveDAO.CodigoExterno,
                Clave = GenerarClaveEncriptada(ageUsuarioSaveDAO.Clave),
                NumeroIdentificacion = ageUsuarioSaveDAO.NumeroIdentificacion,
                Nombres = ageUsuarioSaveDAO.Nombres,
                Apellidos = ageUsuarioSaveDAO.Apellidos,
                MailPrincipal = ageUsuarioSaveDAO.MailPrincipal,
                TelefonoCelular = ageUsuarioSaveDAO.TelefonoCelular,
                TipoUsuario = ageUsuarioSaveDAO.TipoUsuario,
                TipoRegistro = ageUsuarioSaveDAO.TipoRegistro ??= "R",
                PrimerIngreso = "S",
                Estado = ageUsuarioSaveDAO.Estado,
                FechaEstado = DateTime.Now,
                FechaIngreso = DateTime.Now,
                ObservacionEstado = ageUsuarioSaveDAO.ObservacionEstado,
                UbicacionIngreso = Ubicacion.getIpAdress(_httpContextAccessor),
                UsuarioIngreso = ageUsuarioSaveDAO.UsuarioIngreso,
                TokenFirebase = ageUsuarioSaveDAO.TokenFirebase,
            };
        }

        private string GenerarClaveEncriptada(string clave)
        {
            return BCrypt.Net.BCrypt.HashPassword(clave, BCrypt.Net.BCrypt.GenerateSalt(12));
        }
    }
}
