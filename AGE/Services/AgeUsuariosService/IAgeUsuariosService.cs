﻿
using AGE.Entities;
using AGE.Entities.DAO.AgePerfiles;
using AGE.Entities.DAO.AgeUsuarios;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;

namespace AGE.Services.AgeUsuarioService
{
    public interface IAgeUsuariosService
    {
        Task<Page<AgeUsuarioDAO>> ConsultarTodos(
            int codigoLicenciatario,
            string codigoExterno,
            Pageable pageable);

        Task<Page<AgeUsuarioDAO>> ConsultarListaFiltro(
            int codigoLicenciatario,
            string filtro,
            Pageable pageable);

        Task<AgeUsuarioDAO> ConsultarPorId(AgeUsuarioPKDAO AgeUsuarioPKDAO);

        Task<Resource<AgeUsuarioDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgeUsuarioSaveDAO AgeUsuarioSaveDAO);

        Task<List<Resource<AgeUsuarioDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeUsuarioSaveDAO> AgeUsuarioSaveDAOList);

        Task<AgeUsuarioDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeUsuarioSaveDAO AgeUsuarioSaveDAO);

        Task<List<AgeUsuarioDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeUsuarioSaveDAO> AgeUsuarioSaveDAOList);

        Task<AgeUsuariosLoginDAO> ActualizarDatosUsuario(
            IHttpContextAccessor _httpContextAccessor, 
            AgeUsuariosLoginDAO ageUsuariosLoginDAO);
    }
}
