﻿using AGE.Controllers;
using AGE.Entities;
using AGE.Entities.DAO.AgePaisesMonedas;
using AGE.Middleware.Exceptions.BadRequest;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Repositories.AgeMonedasRepository;
using AGE.Repositories.AgePaisesMonedasRepository;
using AGE.Repositories.AgePaisesRepository;
using AGE.Utils;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using System.ComponentModel.DataAnnotations;
using System.Transactions;

namespace AGE.Services.AgePaisesMonedasService
{
    public class AgePaisesMonedasService : IAgePaisesMonedasService
    {
        private readonly IAgePaisesMonedasRepository _repository;
        private readonly IAgePaisesRepository _repositoryPais;
        private readonly IAgeMonedasRepository _repositoryMonedas;

        public AgePaisesMonedasService(
            IAgePaisesMonedasRepository Repository,
            IAgePaisesRepository repositoryPais,
            IAgeMonedasRepository repositoryMonedas)
        {
            _repository = Repository;
            _repositoryPais = repositoryPais;
            _repositoryMonedas = repositoryMonedas;
        }


        public Task<Page<AgePaisesMonedasDAO>> ConsultarTodos(
            int? codigoMoneda,
            int? codigoPais,
            string? descripcion,
            Pageable pageable)
        {

            pageable.Validar<AgePaisesMonedasDAO>();

            return _repository.ConsultarTodos(codigoMoneda ?? 0, codigoPais ?? 0, descripcion ?? "", pageable);
        }


        public Task<Page<AgePaisesMonedasDAO>> ConsultarListaFiltro(
            string filtro,
            Pageable pageable)
        {

            if (string.IsNullOrWhiteSpace(filtro))
            {
                throw new InvalidFieldException("El filtro es requerido.");
            }

            pageable.Validar<AgePaisesMonedasDAO>();

            return _repository.ConsultarListaFiltro(filtro.ToLower(), pageable);
        }

        public Task<AgePaisesMonedasDAO> ConsultarPorId(
            AgePaisesMonedasPKDAO agePaisesMonedasPKDAO)
        {
            Validator.ValidateObject(agePaisesMonedasPKDAO, new ValidationContext(agePaisesMonedasPKDAO), true);

            AgePaisesMonedasDAO? agePaisesMonedasDAO = _repository.ConsultarPorId(agePaisesMonedasPKDAO).Result;

            if (agePaisesMonedasDAO == null)
            {
                throw new RegisterNotFoundException("Pais Moneda con código " + agePaisesMonedasPKDAO.AgeMonedaCodigo + " no existe.");
            }

            return Task.FromResult(agePaisesMonedasDAO);
        }

        public AgePaisesMonedas ConsultarCompletePorId(
            AgePaisesMonedasPKDAO agePaisesMonedasPKDAO)
        {
            Validator.ValidateObject(agePaisesMonedasPKDAO, new ValidationContext(agePaisesMonedasPKDAO), true);

            AgePaisesMonedas? agePaisesMoneda = _repository.ConsultarCompletePorId(agePaisesMonedasPKDAO).Result;

            return agePaisesMoneda;
        }

        public async Task<Resource<AgePaisesMonedasDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgePaisesMonedasSaveDAO agePaisesMonedaSaveDAO)
        {
            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    AgePaisesMonedas agePaisesMoneda = ValidarInsert(_httpContextAccessor, agePaisesMonedaSaveDAO);

                    AgePaisesMonedasDAO AgePaisesMonedasDAO = await _repository.Insertar(agePaisesMoneda);

                    Resource<AgePaisesMonedasDAO> AgePaisesMonedasDAOWithResource = GetDataWithResource(_httpContextAccessor, AgePaisesMonedasDAO);

                    transactionScope.Complete();

                    return AgePaisesMonedasDAOWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public async Task<List<Resource<AgePaisesMonedasDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgePaisesMonedasSaveDAO> agePaisesMonedasSaveDAOList)
        {
            if (agePaisesMonedasSaveDAOList == null)
            {
                throw new InvalidSintaxisException();
            }

            List<AgePaisesMonedas> agePaisesMonedaList = new List<AgePaisesMonedas>();

            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    foreach (AgePaisesMonedasSaveDAO agePaisesMonedasSaveDAO in agePaisesMonedasSaveDAOList)
                    {
                        AgePaisesMonedas agePaisMoneda = ValidarInsert(_httpContextAccessor, agePaisesMonedasSaveDAO);
                        agePaisesMonedaList.Add(agePaisMoneda);
                    }

                    List<AgePaisesMonedasDAO> AgePaisesMonedasDAOList = await _repository.InsertarVarios(agePaisesMonedaList);

                    List<Resource<AgePaisesMonedasDAO>> AgePaisesMonedasDAOListWithResource = new List<Resource<AgePaisesMonedasDAO>>();

                    foreach (AgePaisesMonedasDAO agePaisesMonedasDAO in AgePaisesMonedasDAOList)
                    {
                        AgePaisesMonedasDAOListWithResource.Add(GetDataWithResource(_httpContextAccessor, agePaisesMonedasDAO));
                    }

                    transactionScope.Complete();

                    return AgePaisesMonedasDAOListWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public Task<AgePaisesMonedasDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgePaisesMonedasSaveDAO agePaisesMonedasSaveDAO)
        {
            AgePaisesMonedas agePaisesMoneda = ValidarUpdate(_httpContextAccessor, agePaisesMonedasSaveDAO);

            return _repository.Actualizar(agePaisesMoneda);
        }

        public Task<List<AgePaisesMonedasDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgePaisesMonedasSaveDAO> agePaisesMonedasSaveDAOList)
        {
            if (agePaisesMonedasSaveDAOList == null)
                throw new InvalidSintaxisException();

            ValidateKeys.ValidarPKDuplicadas(agePaisesMonedasSaveDAOList,
                                             p => new { p.Id.AgeMonedaCodigo, p.Id.AgePaisCodigo },
                                             "Id");

            List<AgePaisesMonedas> agePaisesMonedaList = new List<AgePaisesMonedas>();
            AgePaisesMonedas agePaisesMoneda;

            foreach (AgePaisesMonedasSaveDAO agePaisesMonedasSaveDAO in agePaisesMonedasSaveDAOList)
            {
                agePaisesMoneda = ValidarUpdate(_httpContextAccessor, agePaisesMonedasSaveDAO);
                agePaisesMonedaList.Add(agePaisesMoneda);
            }

            return _repository.ActualizarVarios(agePaisesMonedaList);
        }


        private static Resource<AgePaisesMonedasDAO> GetDataWithResource(
            IHttpContextAccessor _httpContextAccessor,
            AgePaisesMonedasDAO agePaisesMonedasDAO)
        {
            string idtipoSegmentoFinal = $"{agePaisesMonedasDAO.Id.AgeMonedaCodigo}/{agePaisesMonedasDAO.Id.AgePaisCodigo}";

            return Resource<AgePaisesMonedasDAO>.GetDataWithResource<AgePaisesMonedasController>(
                _httpContextAccessor, idtipoSegmentoFinal, agePaisesMonedasDAO);
        }


        private AgePaisesMonedas ValidarInsert(
            IHttpContextAccessor _httpContextAccessor,
            AgePaisesMonedasSaveDAO agePaisesMonedaSaveDAO)
        {

            if (agePaisesMonedaSaveDAO == null)
                throw new InvalidSintaxisException();

            Validator.ValidateObject(agePaisesMonedaSaveDAO, new ValidationContext(agePaisesMonedaSaveDAO), true);

            ValidarKeys(agePaisesMonedaSaveDAO);

            AgePaisesMonedas entityObject = FromDAOToEntity(_httpContextAccessor, agePaisesMonedaSaveDAO);

            Validator.ValidateObject(entityObject, new ValidationContext(entityObject), true);

            return entityObject;
        }

        private AgePaisesMonedas ValidarUpdate(
            IHttpContextAccessor _httpContextAccessor,
            AgePaisesMonedasSaveDAO agePaisesMonedasSaveDAO)
        {
            if (agePaisesMonedasSaveDAO == null)
                throw new InvalidSintaxisException();

            if (agePaisesMonedasSaveDAO.UsuarioModificacion == null || agePaisesMonedasSaveDAO.UsuarioModificacion <= 0)
                throw new InvalidFieldException("El usuario de modificación debe ser un número entero mayor a cero.");

            AgePaisesMonedas? agePaisesMoneda = ConsultarCompletePorId(new AgePaisesMonedasPKDAO
            {
                AgeMonedaCodigo = agePaisesMonedasSaveDAO.Id.AgeMonedaCodigo,
                AgePaisCodigo = agePaisesMonedasSaveDAO.Id.AgePaisCodigo
            });

            if (agePaisesMoneda == null)
                throw new RegisterNotFoundException("Moneda con código " + agePaisesMonedasSaveDAO.Id.AgeMonedaCodigo + " no existe.");

            if (!string.IsNullOrWhiteSpace(agePaisesMonedasSaveDAO.Estado))
            {
                agePaisesMoneda.Estado = agePaisesMonedasSaveDAO.Estado;
                agePaisesMoneda.FechaEstado = DateTime.Now;
            }
            
            if (agePaisesMonedasSaveDAO.OrdenPrincipalSecundario != null && agePaisesMonedasSaveDAO.OrdenPrincipalSecundario > 0)
                 agePaisesMoneda.OrdenPrincipalSecundario = agePaisesMonedasSaveDAO.OrdenPrincipalSecundario;

            if (!string.IsNullOrWhiteSpace(agePaisesMonedasSaveDAO.TipoPrincipalSecundario))
                    agePaisesMoneda.TipoPrincipalSecundario = agePaisesMonedasSaveDAO.TipoPrincipalSecundario;

            if (!string.IsNullOrWhiteSpace(agePaisesMonedasSaveDAO.Descripcion))
                agePaisesMoneda.Descripcion = agePaisesMonedasSaveDAO.Descripcion;

            if (!string.IsNullOrWhiteSpace(agePaisesMonedasSaveDAO.ObservacionEstado))
                agePaisesMoneda.ObservacionEstado = agePaisesMonedasSaveDAO.ObservacionEstado;

            agePaisesMoneda.FechaModificacion = DateTime.Now;
            agePaisesMoneda.UbicacionModificacion = Ubicacion.getIpAdress(_httpContextAccessor);
            agePaisesMoneda.UsuarioModificacion = agePaisesMonedasSaveDAO.UsuarioModificacion;

            Validator.ValidateObject(agePaisesMoneda, new ValidationContext(agePaisesMoneda), true);

            return agePaisesMoneda;
        }

        private AgePaisesMonedas FromDAOToEntity(
        IHttpContextAccessor _httpContextAccessor,
        AgePaisesMonedasSaveDAO agePaisesMonedasSaveDAO)
        {
            return new AgePaisesMonedas
            {
                AgeMonedaCodigo = agePaisesMonedasSaveDAO.Id.AgeMonedaCodigo,
                AgePaisCodigo = agePaisesMonedasSaveDAO.Id.AgePaisCodigo,
                Descripcion = agePaisesMonedasSaveDAO.Descripcion,
                TipoPrincipalSecundario = agePaisesMonedasSaveDAO.TipoPrincipalSecundario,
                OrdenPrincipalSecundario = agePaisesMonedasSaveDAO.OrdenPrincipalSecundario,
                Estado = agePaisesMonedasSaveDAO.Estado,
                FechaEstado = DateTime.Now,
                FechaIngreso = DateTime.Now,
                ObservacionEstado = agePaisesMonedasSaveDAO.ObservacionEstado,
                UbicacionIngreso = Ubicacion.getIpAdress(_httpContextAccessor),
                UsuarioIngreso = agePaisesMonedasSaveDAO.UsuarioIngreso
            };
        }

        private void ValidarKeys(AgePaisesMonedasSaveDAO agePaisesMonedasSaveDAO)
        {
            ValidarPK(agePaisesMonedasSaveDAO.Id);
            ValidarFKMoneda(agePaisesMonedasSaveDAO.Id.AgeMonedaCodigo);
            ValidarFKPais(agePaisesMonedasSaveDAO.Id.AgePaisCodigo);

        }

        private void ValidarPK(AgePaisesMonedasPKDAO agePaisesMonedasPKDAO)
        {

            ValidateKeys.ValidarNoExistenciaKey(
               agePaisesMonedasPKDAO,
               $"Moneda de País con código de País {agePaisesMonedasPKDAO.AgePaisCodigo} " +
               $"y moneda {agePaisesMonedasPKDAO.AgeMonedaCodigo}  ya existe.",
               _repository.ConsultarCompletePorId);
        }

        private void ValidarFKPais(int codigoPais)
        {

            ValidateKeys.ValidarExistenciaKey(
                codigoPais,
                $"País con código {codigoPais} no existe.",
                _repositoryPais.ConsultarCompletePorId);
        }

        private void ValidarFKMoneda(int codigoMoneda)
        {
            ValidateKeys.ValidarExistenciaKey(
                codigoMoneda,
                $"Moneda con código {codigoMoneda} no existe.",
                _repositoryMonedas.ConsultarCompletePorId);
        }

    }

}

