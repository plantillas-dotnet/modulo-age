﻿using AGE.Entities.DAO.AgePaisesMonedas;
using AGE.Entities;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;

namespace AGE.Services.AgePaisesMonedasService
{
    public interface IAgePaisesMonedasService
    {
        Task<Page<AgePaisesMonedasDAO>> ConsultarTodos(
            int? codigoIdioma,
            int? codigoPais,
            string? descripcion,
            Pageable pageable);
        Task<Page<AgePaisesMonedasDAO>> ConsultarListaFiltro(
            string filtro,
            Pageable pageable);
        Task<AgePaisesMonedasDAO> ConsultarPorId(
            AgePaisesMonedasPKDAO agePaisesMonedasPKDAO);
        AgePaisesMonedas ConsultarCompletePorId(
            AgePaisesMonedasPKDAO agePaisesMonedasPKDAO);
        Task<Resource<AgePaisesMonedasDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgePaisesMonedasSaveDAO agePaisesMonedasSaveDAO);
        Task<List<Resource<AgePaisesMonedasDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgePaisesMonedasSaveDAO> agePaisesMonedasSaveDAOList);
        Task<AgePaisesMonedasDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgePaisesMonedasSaveDAO agePaisesMonedasSaveDAO);
        Task<List<AgePaisesMonedasDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgePaisesMonedasSaveDAO> agePaisesMonedasSaveDAOList);
    }
}
