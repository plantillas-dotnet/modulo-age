﻿using AGE.Entities.DAO.AgeMensajes;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;

namespace AGE.Services.AgeMensajesService
{
    public interface IAgeMensajeService
    {

        Task<Page<AgeMensajeDAO>> ConsultarTodos(
            long codigo,
            string accionMsg,
            Pageable pageable); 

        Task<Page<AgeMensajeDAO>> ConsultarListaFiltro(
            string filtro,
            Pageable pageable);

        Task<AgeMensajeDAO> ConsultarPorId(
            long codigo);

        Task<Resource<AgeMensajeDAO>> Insertar(
            IHttpContextAccessor _httpContextAccessor,
            AgeMensajeSaveDAO ageMensajeSaveDAO);

        Task<AgeMensajeDAO> Actualizar(
            IHttpContextAccessor _httpContextAccessor,
            AgeMensajeSaveDAO ageMensajeSaveDAO);

        Task<List<Resource<AgeMensajeDAO>>> InsertarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeMensajeSaveDAO> agePaisesSaveDAOList);

        Task<List<AgeMensajeDAO>> ActualizarVarios(
            IHttpContextAccessor _httpContextAccessor,
            List<AgeMensajeSaveDAO> agePaisesSaveDAOList);

    }
}
