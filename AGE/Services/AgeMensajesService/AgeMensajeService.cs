﻿using AGE.Controllers;
using AGE.Entities;
using AGE.Entities.DAO.AgeMensajes;
using AGE.Middleware.Exceptions.BadRequest;
using AGE.Middleware.Exceptions.NotFound;
using AGE.Repositories.AgeMensajesRepository;
using AGE.Services.AgeSecuenciasPrimariasService;
using AGE.Utils;
using AGE.Utils.Paginacion;
using AGE.Utils.WebLink;
using System.ComponentModel.DataAnnotations;
using System.Transactions;

namespace AGE.Services.AgeMensajesService
{
    public class AgeMensajeService : IAgeMensajeService
    {

        private readonly IAgeMensajeRepository _repository;
        private readonly IAgeSecuenciasPrimariasService _ageSecuenciasPrimariasService;


        public AgeMensajeService(
            IAgeMensajeRepository repository,
            IAgeSecuenciasPrimariasService ageSecuenciasPrimariasService)
        {
            _repository = repository;
            _ageSecuenciasPrimariasService = ageSecuenciasPrimariasService;
        }

        public Task<Page<AgeMensajeDAO>> ConsultarTodos(long codigo, string accionMsg, Pageable pageable)
        {
            pageable.Validar<AgeMensajeDAO>();

            return _repository.ConsultarTodos(codigo, accionMsg, pageable);
        }

        public Task<AgeMensajeDAO> ConsultarPorId(long codigo)
        {
            if (codigo <= 0)
                throw new InvalidIdException();

            AgeMensajeDAO? ageMensajeDAO = _repository.ConsultarPorId(codigo).Result;

            return ageMensajeDAO == null
                ? throw new RegisterNotFoundException("Mensaje con código: " + codigo + " no existe.")
                : Task.FromResult(ageMensajeDAO);
        }

        private AgeMensajes ConsultarCompletePorId(
            long codigo)
        {

            if (codigo <= 0)
                throw new InvalidIdException();

            return _repository.ConsultarCompletePorId(codigo).Result;
        }

        public Task<Page<AgeMensajeDAO>> ConsultarListaFiltro(string filtro, Pageable pageable)
        {
            if (string.IsNullOrWhiteSpace(filtro))
                throw new InvalidFieldException("El filtro es requerido.");

            pageable.Validar<AgeMensajeDAO>();

            return _repository.ConsultarListaFiltro(filtro, pageable);
        }

        public async Task<Resource<AgeMensajeDAO>> Insertar(IHttpContextAccessor _httpContextAccessor, AgeMensajeSaveDAO ageMensajeSaveDAO)
        {
            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    AgeMensajes ageMensaje = ValidarInsert(_httpContextAccessor, ageMensajeSaveDAO);

                    AgeMensajeDAO ageMensajeDAO = await _repository.Insertar(ageMensaje);
                    
                    Resource<AgeMensajeDAO> ageMensajeDAOWithResource = GetDataWithResource(_httpContextAccessor, ageMensajeDAO);

                    transactionScope.Complete();   

                    return ageMensajeDAOWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public async Task<List<Resource<AgeMensajeDAO>>> InsertarVarios(IHttpContextAccessor _httpContextAccessor, List<AgeMensajeSaveDAO> ageMensajeSaveDAOList)
        {
            if (ageMensajeSaveDAOList == null)
                throw new InvalidSintaxisException();

            List<AgeMensajes> ageMensajeList = new List<AgeMensajes>();

            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    foreach (AgeMensajeSaveDAO ageMensajeSaveDAO in ageMensajeSaveDAOList)
                    {
                        AgeMensajes agePerfile = ValidarInsert(_httpContextAccessor, ageMensajeSaveDAO);
                        ageMensajeList.Add(agePerfile);
                    }

                    List<AgeMensajeDAO> ageMensajeDAOList = await _repository.InsertarVarios(ageMensajeList);

                    List<Resource<AgeMensajeDAO>> ageMensajeDAOListWithResource = new List<Resource<AgeMensajeDAO>>();

                    foreach (AgeMensajeDAO ageMensajeDAO in ageMensajeDAOList)
                    {
                        ageMensajeDAOListWithResource.Add(GetDataWithResource(_httpContextAccessor, ageMensajeDAO));
                    }

                    transactionScope.Complete();

                    return ageMensajeDAOListWithResource;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public Task<AgeMensajeDAO> Actualizar(IHttpContextAccessor _httpContextAccessor, AgeMensajeSaveDAO ageMensajeSaveDAO)
        {
            AgeMensajes ageMensaje = ValidarUpdate(_httpContextAccessor, ageMensajeSaveDAO);

            return _repository.Actualizar(ageMensaje);
        }

        public Task<List<AgeMensajeDAO>> ActualizarVarios(IHttpContextAccessor _httpContextAccessor, List<AgeMensajeSaveDAO> ageMensajeSaveDAOList)
        {
            if (ageMensajeSaveDAOList == null)
                throw new InvalidSintaxisException();

            ValidateKeys.ValidarPKDuplicadas(ageMensajeSaveDAOList, p => new { p.Codigo }, "Codigo");

            List<AgeMensajes> ageMensajeList = new List<AgeMensajes>();
            AgeMensajes ageMensaje;

            foreach (AgeMensajeSaveDAO ageRutasSaveDAO in ageMensajeSaveDAOList)
            {
                ageMensaje = ValidarUpdate(_httpContextAccessor, ageRutasSaveDAO);
                ageMensajeList.Add(ageMensaje);
            }

            return _repository.ActualizarVarios(ageMensajeList);
        }

        private static Resource<AgeMensajeDAO> GetDataWithResource(
            IHttpContextAccessor _httpContextAccessor,
            AgeMensajeDAO ageMensajeDAO)
        {
            string rutaSegmentoFinal = $"{ageMensajeDAO.Codigo}";

            return Resource<AgeMensajeDAO>.GetDataWithResource<AgeMensajesController>(
                _httpContextAccessor, rutaSegmentoFinal, ageMensajeDAO);
        }

        private AgeMensajes ValidarInsert(
            IHttpContextAccessor _httpContextAccessor,
            AgeMensajeSaveDAO ageMensajeSaveDAO)
        {
            if (ageMensajeSaveDAO == null)
                throw new InvalidSintaxisException();

            Validator.ValidateObject(ageMensajeSaveDAO, new ValidationContext(ageMensajeSaveDAO), true);

            FuncionesSecuencias fg = new FuncionesSecuencias(_ageSecuenciasPrimariasService);

            ageMensajeSaveDAO.Codigo = fg.ObtenerSecuenciaPrimaria(
                        _httpContextAccessor,
                        Globales.CODIGO_SECUENCIA_MENSAJE,
                        ageMensajeSaveDAO.UsuarioIngreso);

            ValidarPK(ageMensajeSaveDAO.Codigo);

            AgeMensajes entityObject = FromDAOToEntity(_httpContextAccessor, ageMensajeSaveDAO);

            Validator.ValidateObject(entityObject, new ValidationContext(entityObject), true);

            return entityObject;
        }

        private AgeMensajes ValidarUpdate(
            IHttpContextAccessor _httpContextAccessor,
            AgeMensajeSaveDAO ageMensajeSaveDAO)
        {
            if (ageMensajeSaveDAO == null)
                throw new InvalidSintaxisException();

            if (ageMensajeSaveDAO.UsuarioModificacion == null || ageMensajeSaveDAO.UsuarioModificacion <= 0)
                throw new InvalidFieldException("El usuario de modificación debe ser un número entero mayor a cero.");

            AgeMensajes ageMensaje = ConsultarCompletePorId(ageMensajeSaveDAO.Codigo) ?? 
                throw new RegisterNotFoundException($"Mensaje con código: {ageMensajeSaveDAO.Codigo} no existe.");

            if (!string.IsNullOrWhiteSpace(ageMensajeSaveDAO.Estado))
            {
                ageMensaje.Estado = ageMensajeSaveDAO.Estado;
                ageMensaje.FechaEstado = DateTime.Now;
            }

            if (!string.IsNullOrWhiteSpace(ageMensajeSaveDAO.AccionMsg))
                ageMensaje.AccionMsg = ageMensajeSaveDAO.AccionMsg;

            if (!string.IsNullOrWhiteSpace(ageMensajeSaveDAO.ObservacionEstado))
                ageMensaje.ObservacionEstado = ageMensajeSaveDAO.ObservacionEstado;

            ageMensaje.FechaModificacion = DateTime.Now;
            ageMensaje.UbicacionModificacion = Ubicacion.getIpAdress(_httpContextAccessor);
            ageMensaje.UsuarioModificacion = ageMensajeSaveDAO.UsuarioModificacion;

            Validator.ValidateObject(ageMensaje, new ValidationContext(ageMensaje), true);

            return ageMensaje;
        }

        private AgeMensajes FromDAOToEntity(
            IHttpContextAccessor _httpContextAccessor,
            AgeMensajeSaveDAO ageMensajeSaveDAO)
        {

            return new AgeMensajes
            {
                Codigo = ageMensajeSaveDAO.Codigo,
                AccionMsg = ageMensajeSaveDAO.AccionMsg,
                Estado = ageMensajeSaveDAO.Estado.ToUpper().ToString(),
                FechaEstado = DateTime.Now,
                FechaIngreso = DateTime.Now,
                ObservacionEstado = ageMensajeSaveDAO.ObservacionEstado,
                UsuarioIngreso = ageMensajeSaveDAO.UsuarioIngreso,
                UbicacionIngreso = Ubicacion.getIpAdress(_httpContextAccessor)
            };
        }

        private void ValidarPK(long codigoMensaje)
        {
            ValidateKeys.ValidarNoExistenciaKey(
                codigoMensaje,
                $"Mensaje con código: {codigoMensaje} ya existe.",
                _repository.ConsultarCompletePorId);
        }

    }
}
